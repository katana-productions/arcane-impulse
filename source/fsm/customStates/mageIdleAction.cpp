#include "mcv_platform.h"
#include "engine.h"
#include "fsm/customStates/mageIdleAction.h"


void CMageIdleAction::update(CFSMContext& ctx, float dt) const
{
}

void CMageIdleAction::onEnter(CFSMContext& ctx) const
{
	CFSMContext t_ctx = ctx;
	TCompSkeleton* c_skeleton = t_ctx.getSkeleton();
	c_skeleton->playCycleAnimation("mage_idle", 1.0f, 1.0f);
}

void CMageIdleAction::onExit(CFSMContext& ctx) const
{

}
