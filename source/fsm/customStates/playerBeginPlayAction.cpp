#include "mcv_platform.h"
#include "fsm/customStates/playerBeginPlayAction.h"
#include "components/common/comp_fsm.h"

void CPlayerBeginAction::update(CFSMContext& ctx, float dt) const
{
	if (ctx.getWeight() == 0.f && ctx.getTimeInState() > 1.f) {
		TVariable var;
		var._name = "beginPlay";
		var._value = false;
		ctx.setVariable(var);
	}
}

void CPlayerBeginAction::onEnter(CFSMContext& ctx) const
{
	CFSMContext t_ctx = ctx;
	TCompSkeleton* c_skeleton = t_ctx.getSkeleton();
	c_skeleton->playActionAnimation("begin_play", 0.0, 0.4, 1, false);
}

void CPlayerBeginAction::onExit(CFSMContext& ctx) const
{
}
