#include "mcv_platform.h"
#include "fsm/customStates/playerIdleAction.h"

void CPlayerIdleAction::update(CFSMContext& ctx, float dt) const
{
}

void CPlayerIdleAction::onEnter(CFSMContext& ctx) const
{
	CFSMContext t_ctx = ctx;
	TCompSkeleton* c_skeleton = t_ctx.getSkeleton();
	c_skeleton->playCycleAnimation("idle", 0.5, 0.3);
}

void CPlayerIdleAction::onExit(CFSMContext& ctx) const
{

}
