#include "mcv_platform.h"
#include "fsm/customStates/playerDeathAction.h"
#include "cal3d/cal3d.h"

void CPlayerDeathAction::update(CFSMContext& ctx, float dt) const
{
}

void CPlayerDeathAction::onEnter(CFSMContext& ctx) const
{
	CFSMContext t_ctx = ctx;
	TCompSkeleton* c_skeleton = t_ctx.getSkeleton();
	c_skeleton->playActionAnimation("death", 0.0, 0.0, 1, true);
}

void CPlayerDeathAction::onExit(CFSMContext& ctx) const
{
	CFSMContext t_ctx = ctx;
	TCompSkeleton* c_skeleton = t_ctx.getSkeleton();
	auto mixer = c_skeleton->model->getMixer();
	mixer->removeAction(c_skeleton->model->getCoreModel()->getCoreAnimationId("death"));
}
