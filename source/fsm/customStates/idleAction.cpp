#include "mcv_platform.h"
#include "engine.h"
#include "fsm/customStates/idleAction.h"
#include "input/input.h"
#include "input/module_input.h"


void CIdleAction::update(CFSMContext& ctx, float dt) const
{
}

void CIdleAction::onEnter(CFSMContext& ctx) const
{
	CFSMContext t_ctx = ctx;
	TCompSkeleton* c_skeleton = t_ctx.getSkeleton();
	c_skeleton->playCycleAnimation("goblin_idle", 1.0, 1.0);
}

void CIdleAction::onExit(CFSMContext& ctx) const
{

}
