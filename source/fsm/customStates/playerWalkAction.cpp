#include "mcv_platform.h"
#include "fsm/customStates/playerWalkAction.h"
#include "components/player/handsSet.h"
#include "engine.h"

void CPlayerWalkAction::update(CFSMContext& ctx, float dt) const
{
	/*CFSMContext t_ctx = ctx;
	TVariable* pull;
	pull = t_ctx.getVariable("pull");
	if (pull != nullptr) {
		if (std::get<bool>(pull->_value) != false) {
			TVariable var;
			var._name = "speed";
			var._value = 0.0f;
			ctx.setVariable(var);
		}
	}*/
}

void CPlayerWalkAction::onEnter(CFSMContext& ctx) const
{
	CEntity* e_left_hand = getEntityByName("LeftHand");
	TCompHandSet* hand_left = e_left_hand->get<TCompHandSet>();

	CEntity* e_right_hand = getEntityByName("RightHand");
	TCompHandSet* hand_right = e_right_hand->get<TCompHandSet>();

	CFSMContext t_ctx = ctx;
	TCompSkeleton* c_skeleton = t_ctx.getSkeleton();

	TVariable* pull;
	pull = t_ctx.getVariable("pull");
	if ((pull == nullptr || std::get<bool>(pull->_value) == false)){
		hand_right->setHeight(-0.54f);
		hand_left->setHeight(-0.54f);
	}
	float speedHand = 2.5f;
	hand_right->speed = speedHand;
	hand_left->speed = speedHand;
	
	//c_skeleton->playActionAnimation("walk_start", 0.1, 0.0, 0.5, false);
}

void CPlayerWalkAction::onExit(CFSMContext& ctx) const
{
	CEntity* e_left_hand = getEntityByName("LeftHand");
	TCompHandSet* hand_left = e_left_hand->get<TCompHandSet>();

	CEntity* e_right_hand = getEntityByName("RightHand");
	TCompHandSet* hand_right = e_right_hand->get<TCompHandSet>();

	if (EnginePersistence.getGloves()) {
		hand_right->setHeight(-0.42f);
		hand_left->setHeight(-0.41f);
	}
	else {
		hand_right->setHeight(-0.345f);
		hand_left->setHeight(-0.34f);
	}
	float speedHand = 3.0f;
	hand_right->speed = speedHand;
	hand_left->speed = speedHand;

	CFSMContext t_ctx = ctx;
	TCompSkeleton* c_skeleton = t_ctx.getSkeleton();
	if (t_ctx.getTimeInState() >= 0.3f)
		c_skeleton->playActionAnimation("walk_end", 0.1f, 0.3f, 0.5f, false);
}
