#include "mcv_platform.h"
#include "fsm/customStates/playerJumpAction.h"

void CPlayerJumpAction::update(CFSMContext& ctx, float dt) const
{
}

void CPlayerJumpAction::onEnter(CFSMContext& ctx) const
{
	CFSMContext t_ctx = ctx;
	TCompSkeleton* c_skeleton = t_ctx.getSkeleton();
	c_skeleton->playActionAnimation("jump", 0.0, 0.2, 0.5, false);
}

void CPlayerJumpAction::onExit(CFSMContext& ctx) const
{
	TVariable var;
	var._name = "jump";
	var._value = false;
	ctx.setVariable(var);
}
