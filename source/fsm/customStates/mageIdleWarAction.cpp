#include "mcv_platform.h"
#include "engine.h"
#include "fsm/customStates/mageIdleWarAction.h"


void CMageIdleWarAction::update(CFSMContext& ctx, float dt) const
{
}

void CMageIdleWarAction::onEnter(CFSMContext& ctx) const
{
	CFSMContext t_ctx = ctx;
	TCompSkeleton* c_skeleton = t_ctx.getSkeleton();
	c_skeleton->playCycleAnimation("mage_idle", 1.0f, 1.0f);
}

void CMageIdleWarAction::onExit(CFSMContext& ctx) const
{

}
