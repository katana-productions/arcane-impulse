#include "mcv_platform.h"
#include "fsm/customStates/goblinTauntAction.h"

void CGoblinTauntAction::update(CFSMContext& ctx, float dt) const
{
}

void CGoblinTauntAction::onEnter(CFSMContext& ctx) const
{
	CFSMContext t_ctx = ctx;
	TCompSkeleton* c_skeleton = t_ctx.getSkeleton();
	c_skeleton->playCycleAnimation("goblin_idle_war", 1, 1.0f);
}

void CGoblinTauntAction::onExit(CFSMContext& ctx) const
{
}