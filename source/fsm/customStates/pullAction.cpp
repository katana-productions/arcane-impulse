#include "mcv_platform.h"
#include "fsm/customStates/pullAction.h"
#include "components/player/handsSet.h"
#include "engine.h"

void CPullAction::update(CFSMContext& ctx, float dt) const
{
}

void CPullAction::onEnter(CFSMContext& ctx) const
{
	CFSMContext t_ctx = ctx;
	TCompSkeleton* c_skeleton = t_ctx.getSkeleton();
	c_skeleton->playActionAnimation("pull_start", 0.0, 0.2, 1, false);
	EngineAudio.playEvent("event:/Spells/Pull_spell", 1);

	CEntity* e_right_hand = getEntityByName("RightHand");
	TCompHandSet* hand_right = e_right_hand->get<TCompHandSet>();
	hand_right->setFrontOffset(0.13f);
}

void CPullAction::onExit(CFSMContext& ctx) const
{
	CEntity* e_right_hand = getEntityByName("RightHand");
	TCompHandSet* hand_right = e_right_hand->get<TCompHandSet>();
	hand_right->setFrontOffset(0.1f);
}
