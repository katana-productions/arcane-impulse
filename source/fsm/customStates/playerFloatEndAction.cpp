#include "mcv_platform.h"
#include "fsm/customStates/playerFloatEndAction.h"

void CPlayerFloatEndAction::update(CFSMContext& ctx, float dt) const
{
}

void CPlayerFloatEndAction::onEnter(CFSMContext& ctx) const
{
	CFSMContext t_ctx = ctx;
	TCompSkeleton* c_skeleton = t_ctx.getSkeleton();
	c_skeleton->playActionAnimation("float_end", 0.0, 0.1, 0.4, false);
}

void CPlayerFloatEndAction::onExit(CFSMContext& ctx) const
{
}
