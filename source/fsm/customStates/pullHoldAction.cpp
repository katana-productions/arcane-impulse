#include "mcv_platform.h"
#include "fsm/customStates/pullHoldAction.h"
#include "components/player/handsSet.h"

void CPullHoldAction::update(CFSMContext& ctx, float dt) const
{
}

void CPullHoldAction::onEnter(CFSMContext& ctx) const
{
	CFSMContext t_ctx = ctx;
	TCompSkeleton* c_skeleton = t_ctx.getSkeleton();
	c_skeleton->playCycleAnimation("pull_hold", 1, 0.0);

	CEntity* e_right_hand = getEntityByName("RightHand");
	TCompHandSet* hand_right = e_right_hand->get<TCompHandSet>();
	hand_right->setFrontOffset(0.14f);
}

void CPullHoldAction::onExit(CFSMContext& ctx) const
{
	CEntity* e_right_hand = getEntityByName("RightHand");
	TCompHandSet* hand_right = e_right_hand->get<TCompHandSet>();
	hand_right->setFrontOffset(0.1f);

	CFSMContext t_ctx = ctx;
	TCompSkeleton* c_skeleton = t_ctx.getSkeleton();
	c_skeleton->playActionAnimation("pull_end", 0.0, 0.5, 1, false);
}
