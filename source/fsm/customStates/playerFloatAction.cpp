#include "mcv_platform.h"
#include "fsm/customStates/playerFloatAction.h"


void CPlayerFloatAction::update(CFSMContext& ctx, float dt) const
{
	CFSMContext t_ctx = ctx;
	TVariable* floating;
	floating = t_ctx.getVariable("floating");
	if ((floating == nullptr) || (std::get<bool>(floating->_value) == false)) {
		TVariable var;
		var._name = "floating";
		var._value = true;
		ctx.setVariable(var);
		var._value = false;
		ctx.setVariable(var);
	}
}

void CPlayerFloatAction::onEnter(CFSMContext& ctx) const
{
	CFSMContext t_ctx = ctx;
	TCompSkeleton* c_skeleton = t_ctx.getSkeleton();
	c_skeleton->playActionAnimation("float_start", 0.0, 0.1, 0.2, false);
	c_skeleton->playCycleAnimation("float_loop", 0.3, 0.1);
}

void CPlayerFloatAction::onExit(CFSMContext& ctx) const
{
}
