#include "mcv_platform.h"
#include "fsm/customStates/goblinWalkAction.h"

void CGoblinWalkAction::update(CFSMContext& ctx, float dt) const
{
}

void CGoblinWalkAction::onEnter(CFSMContext& ctx) const
{
	CFSMContext t_ctx = ctx;
	TCompSkeleton* c_skeleton = t_ctx.getSkeleton();
	c_skeleton->playCycleAnimation("goblin_walk", 1, 1);
}

void CGoblinWalkAction::onExit(CFSMContext& ctx) const
{

}
