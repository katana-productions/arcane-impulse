#include "mcv_platform.h"
#include "fsm/customStates/goblinRunAction.h"
#include "components\controllers\comp_char_controller.h"

void CGoblinRunAction::update(CFSMContext& ctx, float dt) const
{
	CFSMContext t_ctx = ctx;
	TCompSkeleton* c_skeleton = t_ctx.getSkeleton();
	float weight = t_ctx.getWeight();
	c_skeleton->setWeight("goblin_walk_war", "goblin_run", weight);
}

void CGoblinRunAction::onEnter(CFSMContext& ctx) const
{
	CFSMContext t_ctx = ctx;
	TCompSkeleton* c_skeleton = t_ctx.getSkeleton();
	c_skeleton->blendCycleAnimations("goblin_walk_war", "goblin_run", 0.25f);
}

void CGoblinRunAction::onExit(CFSMContext& ctx) const
{
}