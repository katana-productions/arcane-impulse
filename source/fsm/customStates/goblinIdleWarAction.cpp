#include "mcv_platform.h"
#include "fsm/customStates/goblinIdleWarAction.h"

void CGoblinIdleWarAction::update(CFSMContext& ctx, float dt) const
{
}

void CGoblinIdleWarAction::onEnter(CFSMContext& ctx) const
{
	CFSMContext t_ctx = ctx;
	TCompSkeleton* c_skeleton = t_ctx.getSkeleton();
	c_skeleton->playCycleAnimation("goblin_idle_war", 1, 0.25f);
}

void CGoblinIdleWarAction::onExit(CFSMContext& ctx) const
{

}