#include "mcv_platform.h"
#include "fsm/customStates/playerLeftRunAction.h"
#include "components/player/handsSet.h"
#include "cal3d/cal3d.h"
#include "components/common/comp_fsm.h"

void CPlayerLeftRunAction::update(CFSMContext& ctx, float dt) const
{
}

void CPlayerLeftRunAction::onEnter(CFSMContext& ctx) const
{
	CFSMContext t_ctx = ctx;
	TCompSkeleton* c_skeleton = t_ctx.getSkeleton();
	TVariable* pull;
	c_skeleton->playCycleAnimation("run_loop", 0.7f, 0.0f);
	
	//Get left hand animation time to synchronize
	CEntity* e_left_hand = getEntityByName("RightHand");
	TCompFSM* cFsm_left = e_left_hand->get<TCompFSM>();
	CFSMContext left_cont = cFsm_left->getContext();
	TCompSkeleton* left_skel = left_cont.getSkeleton();
	auto left_mixer = left_skel->model->getMixer();
	auto mixer = c_skeleton->model->getMixer();
	float anim_time = left_mixer->getAnimationTime();
	std::string anim_name = left_cont.getContextAnim();
	if(anim_name == "run" && anim_time != 0.0f)
		mixer->setAnimationTime(anim_time);
}

void CPlayerLeftRunAction::onExit(CFSMContext& ctx) const
{
}
