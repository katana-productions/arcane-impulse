#include "mcv_platform.h"
#include "engine.h"
#include "fsm/customStates/mageRunAction.h"


void CMageRunAction::update(CFSMContext& ctx, float dt) const
{
}

void CMageRunAction::onEnter(CFSMContext& ctx) const
{
	CFSMContext t_ctx = ctx;
	TCompSkeleton* c_skeleton = t_ctx.getSkeleton();
	c_skeleton->playCycleAnimation("mage_walk_straight", 1.0f, 1.0f);
}

void CMageRunAction::onExit(CFSMContext& ctx) const
{

}
