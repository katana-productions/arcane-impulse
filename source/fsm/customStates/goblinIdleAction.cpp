#include "mcv_platform.h"
#include "engine.h"
#include "fsm/customStates/goblinIdleAction.h"


void CGoblinIdleAction::update(CFSMContext& ctx, float dt) const
{
}

void CGoblinIdleAction::onEnter(CFSMContext& ctx) const
{
	CFSMContext t_ctx = ctx;
	TCompSkeleton* c_skeleton = t_ctx.getSkeleton();
	c_skeleton->playCycleAnimation("goblin_idle", 1.0f, 1.0f);
}

void CGoblinIdleAction::onExit(CFSMContext& ctx) const
{

}
