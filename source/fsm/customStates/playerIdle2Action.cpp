#include "mcv_platform.h"
#include "fsm/customStates/playerIdle2Action.h"

void CPlayerIdle2Action::update(CFSMContext& ctx, float dt) const
{
}

void CPlayerIdle2Action::onEnter(CFSMContext& ctx) const
{
	CFSMContext t_ctx = ctx;
	TCompSkeleton* c_skeleton = t_ctx.getSkeleton();
	c_skeleton->playCycleAnimation("idle2", 0.5, 0.0);
}

void CPlayerIdle2Action::onExit(CFSMContext& ctx) const
{

}
