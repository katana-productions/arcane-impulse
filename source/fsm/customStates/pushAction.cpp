#include "mcv_platform.h"
#include "fsm/customStates/pushAction.h"
#include "engine.h"

void CPushAction::update(CFSMContext& ctx, float dt) const
{
}

void CPushAction::onEnter(CFSMContext& ctx) const
{
	CFSMContext t_ctx = ctx;
	TCompSkeleton* c_skeleton = t_ctx.getSkeleton();
	c_skeleton->playActionAnimation("push", 0.0f, 0.5f, 1.0f, false);
	EngineAudio.playEvent("event:/Spells/Push_spell", 1);
}

void CPushAction::onExit(CFSMContext& ctx) const
{

}
