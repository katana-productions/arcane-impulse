#include "mcv_platform.h"
#include "fsm/customStates/playerRunAction.h"

void CPlayerRunAction::update(CFSMContext& ctx, float dt) const
{
}

void CPlayerRunAction::onEnter(CFSMContext& ctx) const
{
	CFSMContext t_ctx = ctx;
	TCompSkeleton* c_skeleton = t_ctx.getSkeleton();
	TVariable* pull;
	c_skeleton->playCycleAnimation("run_loop", 0.7f, 0.0f);
}

void CPlayerRunAction::onExit(CFSMContext& ctx) const
{
}
