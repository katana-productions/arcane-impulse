#include "mcv_platform.h"
#include "engine.h"
#include "fsm/customStates/goblinFlyAction.h"


void CGoblinFlyAction::update(CFSMContext& ctx, float dt) const
{
}

void CGoblinFlyAction::onEnter(CFSMContext& ctx) const
{
	CFSMContext t_ctx = ctx;
	TCompSkeleton* c_skeleton = t_ctx.getSkeleton();
	c_skeleton->playCycleAnimation("goblin_flying", 1, 1);
}

void CGoblinFlyAction::onExit(CFSMContext& ctx) const
{
}