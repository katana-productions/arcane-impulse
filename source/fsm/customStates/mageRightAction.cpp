#include "mcv_platform.h"
#include "engine.h"
#include "fsm/customStates/mageRightAction.h"


void CMageRightAction::update(CFSMContext& ctx, float dt) const
{
}

void CMageRightAction::onEnter(CFSMContext& ctx) const
{
	CFSMContext t_ctx = ctx;
	TCompSkeleton* c_skeleton = t_ctx.getSkeleton();
	c_skeleton->playCycleAnimation("mage_walk_left", 1.0f, 1.0f);
}

void CMageRightAction::onExit(CFSMContext& ctx) const
{

}
