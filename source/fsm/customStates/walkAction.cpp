#include "mcv_platform.h"
#include "fsm/customStates/walkAction.h"

void CWalkAction::update(CFSMContext& ctx, float dt) const
{
}

void CWalkAction::onEnter(CFSMContext& ctx) const
{
	CFSMContext t_ctx = ctx;
	TCompSkeleton* c_skeleton = t_ctx.getSkeleton();
	c_skeleton->playCycleAnimation("goblin_run", 1, 1);
}

void CWalkAction::onExit(CFSMContext& ctx) const
{

}
