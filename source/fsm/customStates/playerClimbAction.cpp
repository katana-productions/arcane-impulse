#include "mcv_platform.h"
#include "fsm/customStates/playerClimbAction.h"
#include "components/player/handsSet.h"
#include "engine.h"

void CPlayerClimbAction::update(CFSMContext& ctx, float dt) const
{
	/*CFSMContext t_ctx = ctx;
	if ((t_ctx.getWeight() == 0) && (!looping)) {
		TCompSkeleton* c_skeleton = t_ctx.getSkeleton();
		float weight = t_ctx.getWeight();
		c_skeleton->playCycleAnimation("climb_loop", 0.7, 0.1);
		looping == true;
	}*/
}

void CPlayerClimbAction::onEnter(CFSMContext& ctx) const
{
	CEntity* e_left_hand = getEntityByName("LeftHand");
	TCompHandSet* hand_left = e_left_hand->get<TCompHandSet>();

	CEntity* e_right_hand = getEntityByName("RightHand");
	TCompHandSet* hand_right = e_right_hand->get<TCompHandSet>();

	float heightLow = -1.5f;
	if (EnginePersistence.getGloves()) {
		hand_right->setFrontOffset(0.11f);
		hand_left->setFrontOffset(0.11f);
		hand_right->setHeight(heightLow);
		hand_left->setHeight(heightLow);
	}
	else {
		hand_right->setFrontOffset(0.17f);
		hand_left->setFrontOffset(0.17f);
		hand_right->setHeight(heightLow);
		hand_left->setHeight(heightLow);
	}
	float speedHand = 0.5f;
	hand_right->speed = speedHand;
	hand_left->speed = speedHand;

	CFSMContext t_ctx = ctx;
	TCompSkeleton* c_skeleton = t_ctx.getSkeleton();
	c_skeleton->playActionAnimation("climb", 0.0f, 0.1f, 1.f, false);
}

void CPlayerClimbAction::onExit(CFSMContext& ctx) const
{
	CEntity* e_left_hand = getEntityByName("LeftHand");
	TCompHandSet* hand_left = e_left_hand->get<TCompHandSet>();

	CEntity* e_right_hand = getEntityByName("RightHand");
	TCompHandSet* hand_right = e_right_hand->get<TCompHandSet>();

	if (EnginePersistence.getGloves()) {
		hand_right->setFrontOffset(0.1f);
		hand_left->setFrontOffset(0.1f);
		hand_right->setHeight(-0.42f);
		hand_left->setHeight(-0.41f);
	}
	else {
		hand_right->setFrontOffset(0.16f);
		hand_left->setFrontOffset(0.16f);
		hand_right->setHeight(-0.345f);
		hand_left->setHeight(-0.34f);
	}
	float speedHand = 3.0f;
	hand_right->speed = speedHand;
	hand_left->speed = speedHand;
}
