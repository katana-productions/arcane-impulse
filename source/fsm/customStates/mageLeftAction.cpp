#include "mcv_platform.h"
#include "engine.h"
#include "fsm/customStates/mageLeftAction.h"


void CMageLeftAction::update(CFSMContext& ctx, float dt) const
{
}

void CMageLeftAction::onEnter(CFSMContext& ctx) const
{
	CFSMContext t_ctx = ctx;
	TCompSkeleton* c_skeleton = t_ctx.getSkeleton();
	c_skeleton->playCycleAnimation("mage_walk_right", 1.0f, 1.0f);
}

void CMageLeftAction::onExit(CFSMContext& ctx) const
{

}
