#include "mcv_platform.h"
#include "fsm/module_fsm.h"
#include "fsm/state.h"
#include "fsm/transition.h"

#include "fsm/customStates/playerIdleAction.h"
#include "fsm/customStates/playerIdle2Action.h"
#include "fsm/customStates/walkAction.h"
#include "fsm/customStates/idleAction.h"
#include "fsm/customStates/jumpAction.h"
#include "fsm/customStates/goblinIdleAction.h"
#include "fsm/customStates/goblinWalkAction.h"
#include "fsm/customStates/goblinRunAction.h"
#include "fsm/customStates/goblinTauntAction.h"
#include "fsm/customStates/goblinIdleWarAction.h"
#include "fsm/customStates/goblinFlyAction.h"
#include "fsm/customStates/playerJumpAction.h"
#include "fsm/customStates/playerFloatAction.h"
#include "fsm/customStates/pushAction.h"
#include "fsm/customStates/pullAction.h"
#include "fsm/customStates/pullHoldAction.h"
#include "fsm/customStates/playerWalkAction.h"
#include "fsm/customStates/playerDeathAction.h"
#include "fsm/customStates/playerBeginPlayAction.h"
#include "fsm/customStates/playerClimbAction.h"
#include "fsm/customStates/playerRunAction.h"
#include "fsm/customStates/playerLeftRunAction.h"
#include "fsm/customStates/playerRightRunAction.h"
#include "fsm/customStates/playerFloatEndAction.h"

#include "fsm/customStates/mageIdleAction.h"
#include "fsm/customStates/mageIdleWarAction.h"
#include "fsm/customStates/mageLeftAction.h"
#include "fsm/customStates/mageRightAction.h"
#include "fsm/customStates/mageRunAction.h"
#include "fsm/customStates/mageWalkAction.h"


#include "fsm/customTransitions/checkVariable.h"
#include "fsm/customTransitions/checkTime.h"

namespace FSM
{
  CModuleFSM::CModuleFSM(const std::string& name)
    : IModule(name)
  {
  }

  bool CModuleFSM::start()
  {
    _stateFactories["idleAction"] = new StateFactory<CIdleAction>();
    _stateFactories["walkAction"] = new StateFactory<CWalkAction>();
    _stateFactories["jumpAction"] = new StateFactory<CJumpAction>();
	_stateFactories["goblinIdleAction"] = new StateFactory<CGoblinIdleAction>();
	_stateFactories["goblinWalkAction"] = new StateFactory<CGoblinWalkAction>();
	_stateFactories["goblinRunAction"] = new StateFactory<CGoblinRunAction>();
	_stateFactories["goblinTauntAction"] = new StateFactory<CGoblinTauntAction>();
	_stateFactories["goblinIdleWarAction"] = new StateFactory<CGoblinIdleWarAction>();
	_stateFactories["goblinFlyAction"] = new StateFactory<CGoblinFlyAction>();
	_stateFactories["playerIdleAction"] = new StateFactory<CPlayerIdleAction>();
	_stateFactories["playerIdle2Action"] = new StateFactory<CPlayerIdle2Action>();
    _stateFactories["playerJumpAction"] = new StateFactory<CPlayerJumpAction>();
	_stateFactories["playerFloatAction"] = new StateFactory<CPlayerFloatAction>();
	_stateFactories["playerFloatEndAction"] = new StateFactory<CPlayerFloatEndAction>();
	_stateFactories["playerWalkAction"] = new StateFactory<CPlayerWalkAction>();
	_stateFactories["playerRunAction"] = new StateFactory<CPlayerRunAction>();
	_stateFactories["playerLeftRunAction"] = new StateFactory<CPlayerLeftRunAction>();
	_stateFactories["playerRightRunAction"] = new StateFactory<CPlayerRightRunAction>();
	_stateFactories["playerDeathAction"] = new StateFactory<CPlayerDeathAction>();
	_stateFactories["pushAction"] = new StateFactory<CPushAction>();
	_stateFactories["pullAction"] = new StateFactory<CPullAction>();
	_stateFactories["pullHoldAction"] = new StateFactory<CPullHoldAction>();
	_stateFactories["playerBeginPlayAction"] = new StateFactory<CPlayerBeginAction>();
	_stateFactories["playerClimbAction"] = new StateFactory<CPlayerClimbAction>();

	_stateFactories["mageIdleAction"] = new StateFactory<CMageIdleAction>();
	_stateFactories["mageIdleWarAction"] = new StateFactory<CMageIdleWarAction>();
	_stateFactories["mageLeftAction"] = new StateFactory<CMageLeftAction>();
	_stateFactories["mageRightAction"] = new StateFactory<CMageRightAction>();
	_stateFactories["mageRunAction"] = new StateFactory<CMageRunAction>();
	_stateFactories["mageWalkAction"] = new StateFactory<CMageWalkAction>();


    _transitionFactories["checkVariable"] = new TransitionFactory<CCheckVariable>();
    _transitionFactories["checkTime"] = new TransitionFactory<CCheckTime>();

    return true;
  }

  IState* CModuleFSM::createState(const std::string& type)
  {
    const auto& factory = _stateFactories.find(type);
    if(factory == _stateFactories.end())
    {
      return nullptr;
    }
    IState* st = factory->second->create();
    st->_type = factory->first;

    return st;
  }

  ITransition* CModuleFSM::createTransition(const std::string& type)
  {
    const auto& factory = _transitionFactories.find(type);
    if (factory == _transitionFactories.end())
    {
      return nullptr;
    }
    ITransition* tr = factory->second->create();
    tr->_type = factory->first;

    return tr;
  }

  void CModuleFSM::renderInMenu()
  {
    /*const auto printButton = [](const TButton& button, const char* label, bool centerValue = false)
    {
      float value = centerValue ? 0.5f + button.value * 0.5f : button.value;
      ImGui::ProgressBar(value, ImVec2(-1, 0), label);
    };

    if (ImGui::TreeNode("Input"))
    {
      if (ImGui::TreeNode("keyboard"))
      {
        for(int i = 0; i< Input::BT_KEYBOARD_COUNT; ++i)
        {
          printButton(_keyboard._buttons[i], getButtonName(INTERFACE_KEYBOARD, i).c_str());
        }
        ImGui::TreePop();
      }
      if (ImGui::TreeNode("mouse"))
      {
        ImGui::Text("Current position: %.0f %.0f", _mouse._currPosition.x, _mouse._currPosition.y);
        ImGui::Text("Previous position: %.0f %.0f", _mouse._prevPosition.x, _mouse._prevPosition.y);
        for (int i = 0; i< Input::BT_MOUSE_COUNT; ++i)
        {
          printButton(_mouse._buttons[i], getButtonName(INTERFACE_MOUSE, i).c_str());
        }
        ImGui::TreePop();
      }
      if (ImGui::TreeNode("pad"))
      {
        ImGui::Text("Connected: %s", _gamepad._connected ? "YES" : "no");
        for (int i = 0; i< Input::BT_GAMEPAD_COUNT; ++i)
        {
          bool isAnalog = i == Input::BT_LANALOG_X || i == Input::BT_LANALOG_Y || i == Input::BT_RANALOG_X || i == Input::BT_RANALOG_Y;
          printButton(_gamepad._buttons[i], getButtonName(INTERFACE_GAMEPAD, i).c_str(), isAnalog);
        }
        ImGui::TreePop();
      }
      if (ImGui::TreeNode("Mapping"))
      {
        for (auto& bt : _mapping._buttons)
        {
          printButton(bt.second.result, bt.first.c_str());
        }
        ImGui::TreePop();
      }
      ImGui::TreePop();
    }*/
  }
}
