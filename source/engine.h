#pragma once
#include "modules/module_manager.h"
#include "modules/module_entities.h"
#include "physics/module_physics.h"
#include "modules/module_multithreading.h"
#include "modules/AlarmClock.h"
#include "modules/module_navmesh.h"
#include "modules/module_enemy_manager.h"
#include "modules/Basics.h"
#include "modules/module_persistence.h"
#include "modules/module_settings.h"
#include "modules/module_lua.h"
#include "modules/module_console.h"
#include "modules/module_worker.h"
#include "modules/module_scene_loader.h"
#include "render/module_gpu_culling.h"
#include "modules/module_audio.h"
#include "modules/module_decals.h"
#include "particles/module_particles.h"


#define DECL_MODULE(__CLASS__, __MEMBER__, __GETTER__) \
  public: __CLASS__& __GETTER__() const { return *__MEMBER__; } \
  private: __CLASS__* __MEMBER__ = nullptr;

namespace Input
{
  class CModuleInput;
}
class CModuleRender;
class CModuleGPUCullingRender;
class CModuleCameraMixer;
namespace UI
{
  class CModuleUI;
}
namespace FSM
{
  class CModuleFSM;
}

class CEngine
{
  CModuleManager   _modules;

public:
  CEngine();
  ~CEngine();

  static CEngine& get();

  void start();
  void stop();
  void update(float dt);
  void renderDebug();
  void registerResourceTypes();

  CModuleManager& getModules();

  DECL_MODULE(Input::CModuleInput, _input, getInput);
  DECL_MODULE(CModuleRender, _render, getRender);
  DECL_MODULE(CModuleEntities, _entities, getEntities);
  DECL_MODULE(CModulePhysics, _physics, getPhysics);
  DECL_MODULE(CAlarmClock, _alarms, getAlarms);
  DECL_MODULE(CNavMesh , _navMesh, getNavMesh);
  DECL_MODULE(CEnemyManager, _enemyManager, getEnemyManager);
  DECL_MODULE(CBasics, _basics, getBasics);
  DECL_MODULE(CModuleCameraMixer, _cameraMixer, getCameraMixer);
  DECL_MODULE(MultithreadingModule, _multithreading, getMultithreading);
  DECL_MODULE(CModuleGPUCulling, _gpu_culling, getGPUCulling);
  DECL_MODULE(UI::CModuleUI, _ui, getUI);
  DECL_MODULE(FSM::CModuleFSM, _fsm, getFSM);
  DECL_MODULE(CModulePersistence, _persistence, getPersistence);
  DECL_MODULE(CModuleSettings, _settings, getSettings);
  DECL_MODULE(CModuleLua, _lua, getLua);
  DECL_MODULE(CModuleGameConsole, _console, getConsole);
  DECL_MODULE(CModuleBkgWorker, _worker, getWorker);
  DECL_MODULE(CModuleSceneLoader, _sceneLoader, getSceneLoader);
  DECL_MODULE(CModuleAudio, _audio, getAudio);
  DECL_MODULE(CModuleParticles, _moduleParticles, getParticles);
  DECL_MODULE(CModuleDecals, _decals, getDecals);
};

#define Engine CEngine::get()
#define EngineInput CEngine::get().getInput() 
#define EngineRender CEngine::get().getRender()
#define EnginePhysics CEngine::get().getPhysics()
#define EngineAlarms CEngine::get().getAlarms()
#define EngineNavMesh CEngine::get().getNavMesh()
#define EngineEnemyManager CEngine::get().getEnemyManager()
#define EngineBasics CEngine::get().getBasics()
#define EnginePersistence CEngine::get().getPersistence()
#define EngineSettings CEngine::get().getSettings()
#define EngineAudio CEngine::get().getAudio()
#define EngineLua CEngine::get().getLua()
#define EngineConsole CEngine::get().getConsole()
#define EngineWorker CEngine::get().getWorker()
#define EngineSceneLoader CEngine::get().getSceneLoader()
#define EngineGpuCulling CEngine::get().getGPUCulling()
#define EngineDecals CEngine::get().getDecals()
#define EngineParticles CEngine::get().getParticles()


