#include "mcv_platform.h"
#include "navmesh_input.h"
#include "components\common\comp_aabb.h"
#include "components\common\comp_transform.h"
#include "physics/module_physics.h"
#include <assert.h>

using namespace physx;

CNavmeshInput::CNavmeshInput( )
  : verts( nullptr )
  , tris( nullptr )
  , nverts( 0 )
  , ntris( 0 )
  , nverts_total( 0 )
  , ntris_total( 0 )
{ }

// ---------------------------------------------------
void CNavmeshInput::addInput(std::vector<CHandle> inputs) {
	handleInputs = inputs;
	for (size_t i = 0; i < handleInputs.size(); ++i) {
		CEntity* e = handleInputs[i];
		TCompCollider* c_collider = e->get<TCompCollider>();
		PxRigidActor* actor = c_collider->actor;
		static const PxU32 max_shapes = 8;
		PxShape* shapes[max_shapes];
		PxU32 nshapes = actor->getNbShapes();
		// Even when the buffer is small, it writes all the shape pointers
		PxU32 shapes_read = actor->getShapes(shapes, sizeof(shapes), 0);
		// An actor can have several shapes
		for (PxU32 i = 0; i < nshapes; ++i) {
			PxShape* shape = shapes[i];
			assert(shape);
			// Combine physics local offset with world transform of the entity
			switch (shape->getGeometryType()) {
	
			case PxGeometryType::eBOX: {
				PxBoxGeometry box;
				if (shape->getBoxGeometry(box)) {
					nverts_total += 8;
					ntris_total += 10;
				}
				break;
			}
	
			case PxGeometryType::eTRIANGLEMESH: {
				PxTriangleMeshGeometry trimesh;
				if (shape->getTriangleMeshGeometry(trimesh)) {
					PxTriangleMesh* triangleMesh = trimesh.triangleMesh;

					nverts_total += triangleMesh->getNbVertices();
					ntris_total += triangleMesh->getNbTriangles();
				}
				break;
			}
			default:
				break;
			}
		}

	}
}


void CNavmeshInput::clearInput( ) {
  handleInputs.clear( );
  nverts_total = 0;
  ntris_total = 0;
}

// ---------------------------------------------------
void CNavmeshInput::prepareInput( const CHandle input ) {
  unprepareInput( );

	CEntity* e = input;
	TCompTransform* trans = e->get<TCompTransform>();
	TCompCollider* c_collider = e->get<TCompCollider>();

	PxRigidActor* actor = c_collider->actor;
	static const PxU32 max_shapes = 8;
	PxShape* shapes[max_shapes];
	PxU32 nshapes = actor->getNbShapes();

	// Even when the buffer is small, it writes all the shape pointers
	PxU32 shapes_read = actor->getShapes(shapes, sizeof(shapes), 0);

	// An actor can have several shapes
	for (PxU32 i = 0; i < nshapes; ++i) {
		PxShape* shape = shapes[i];
		assert(shape);

		// Combine physics local offset with world transform of the entity
		MAT44 world = toTransform(shape->getLocalPose()).asMatrix() * trans->asMatrix();

		switch (shape->getGeometryType()) {
		case PxGeometryType::eSPHERE: {
			PxSphereGeometry sphere;
			if (shape->getSphereGeometry(sphere))
			break;
		}
		case PxGeometryType::eBOX: {
			PxBoxGeometry box;
			if (shape->getBoxGeometry(box)) {
				VEC3 corners[8];
				VEC3 half = PXVEC3_TO_VEC3(box.halfExtents);
				corners[0] = VEC3::Transform(VEC3(-half.x, -half.y, half.z), world);
				corners[1] = VEC3::Transform(VEC3(half.x, -half.y, half.z), world);
				corners[2] = VEC3::Transform(VEC3(half.x, half.y, half.z), world);
				corners[3] = VEC3::Transform(VEC3(-half.x, half.y, half.z), world);
				corners[4] = VEC3::Transform(VEC3(-half.x, -half.y, -half.z), world);
				corners[5] = VEC3::Transform(VEC3(half.x, -half.y, -half.z), world);
				corners[6] = VEC3::Transform(VEC3(half.x, half.y, -half.z), world);
				corners[7] = VEC3::Transform(VEC3(-half.x, half.y, -half.z), world);

				nverts = 8;
				ntris = 10;

				verts = new float[nverts * 3];
				tris = new int[ntris * 3];

				memset(verts, 0, nverts * 3 * sizeof(float));
				memset(tris, 0, ntris * 3 * sizeof(int));

				//indices
				static const int idxs[6][4] = {
				{ 6, 2, 3, 7 }
				, { 1, 2, 6, 5 }
				, { 0, 3, 2, 1 }
				, { 5, 1, 0, 4 }
				, { 4, 0, 3, 7 }
				, { 4, 5, 6, 7 }
				};
				//pones los vertices en un array verts
				for (int i = 0; i < 8; ++i) {
					VEC3 p = corners[i];
					int idx = i * 3;
					verts[idx] = p.x;
					verts[idx + 1] = p.y;
					verts[idx + 2] = p.z;
				}
				//selecciona las 3 pos de vertice para hacer un tris
				int idx = 0;
				for (int i = 0; i < 5; ++i) {
					tris[idx++] = idxs[i][0];
					tris[idx++] = idxs[i][2];
					tris[idx++] = idxs[i][1];

					tris[idx++] = idxs[i][0];
					tris[idx++] = idxs[i][3];
					tris[idx++] = idxs[i][2];
				}
				assert(idx == ntris * 3);
			}
			break;
		}

		case PxGeometryType::ePLANE: {
			PxPlaneGeometry plane;
			if (shape->getPlaneGeometry(plane))
			break;
		}
		case PxGeometryType::eCAPSULE: {
			PxCapsuleGeometry capsule;
			if (shape->getCapsuleGeometry(capsule))
			break;
		}
		case PxGeometryType::eTRIANGLEMESH: {
			PxTriangleMeshGeometry trimesh;
			if (shape->getTriangleMeshGeometry(trimesh)) {
				PxTriangleMesh* triangleMesh = trimesh.triangleMesh;

				nverts = triangleMesh->getNbVertices();
				ntris = triangleMesh->getNbTriangles();

				verts = new float[nverts * 3];
				tris = new int[ntris * 3];

				memset(verts, 0, nverts * 3 * sizeof(float));
				memset(tris, 0, ntris * 3 * sizeof(int));

				const PxVec3* vertices = triangleMesh->getVertices();
				for (int i = 0; i < nverts; i++) {
					VEC3 p = PXVEC3_TO_VEC3(vertices[i]);
					p = VEC3::Transform(p, world);
					int idx = i * 3;
					verts[idx] = p.x;
					verts[idx + 1] = p.y;
					verts[idx + 2] = p.z;
				}

				PxTriangleMeshFlags triangleMeshFlag = triangleMesh->getTriangleMeshFlags();
				const void* triangleIndices = triangleMesh->getTriangles();

				// Grab triangle indices
				uint32_t I0, I1, I2;
				int tris_ind;

				for (uint32_t TriIndex = 0; TriIndex < ntris; ++TriIndex)
				{
					if (triangleMesh->getTriangleMeshFlags() & PxTriangleMeshFlag::e16_BIT_INDICES)
					{
						PxU16* P16BitIndices = (PxU16*)triangleIndices;
						I0 = P16BitIndices[(TriIndex * 3) + 0];
						I1 = P16BitIndices[(TriIndex * 3) + 1];
						I2 = P16BitIndices[(TriIndex * 3) + 2];
					}
					else
					{
						PxU32* P32BitIndices = (PxU32*)triangleIndices;
						I0 = P32BitIndices[(TriIndex * 3) + 0];
						I1 = P32BitIndices[(TriIndex * 3) + 1];
						I2 = P32BitIndices[(TriIndex * 3) + 2];
					}

					tris_ind = TriIndex * 3;
					// Local position
					tris[tris_ind] = I0;
					tris[tris_ind+1] = I1;
					tris[tris_ind+2] = I2;
				}
			}
			break;
		}
		default:
			break;
		}
	}
}

void CNavmeshInput::unprepareInput( ) {
  delete [] verts;
  delete [] tris;
  verts = 0;
  tris = 0;
}

void CNavmeshInput::computeBoundaries( ) {
  
  aabb_min = VEC3( 0, 0, 0 );
  aabb_max = VEC3( 0, 0, 0 );

  for( auto& handle_input : handleInputs ) {
	CEntity* e = handle_input;
	TCompTransform* trans = e->get<TCompTransform>();
	TCompCollider* c_collider = e->get<TCompCollider>();

	PxRigidActor* actor = c_collider->actor;
	static const PxU32 max_shapes = 8;
	PxShape* shapes[max_shapes];
	PxU32 nshapes = actor->getNbShapes();

	// Even when the buffer is small, it writes all the shape pointers
	PxU32 shapes_read = actor->getShapes(shapes, sizeof(shapes), 0);

	// An actor can have several shapes
	for (PxU32 i = 0; i < nshapes; ++i) {
		PxShape* shape = shapes[i];
		assert(shape);

		// Combine physics local offset with world transform of the entity
		MAT44 world = toTransform(shape->getLocalPose()).asMatrix() * trans->asMatrix();

		switch (shape->getGeometryType()) {
		case PxGeometryType::eSPHERE: {
			PxSphereGeometry sphere;
			if (shape->getSphereGeometry(sphere))
				break;
		}
		case PxGeometryType::eBOX: {
			PxBoxGeometry box;
			if (shape->getBoxGeometry(box)) {
				VEC3 corners[8];
				int num_verts = 0;

				VEC3 half = PXVEC3_TO_VEC3(box.halfExtents);
				corners[0] = VEC3::Transform(VEC3(-half.x, -half.y, half.z), world);
				corners[1] = VEC3::Transform(VEC3(half.x, -half.y, half.z), world);
				corners[2] = VEC3::Transform(VEC3(half.x, half.y, half.z), world);
				corners[3] = VEC3::Transform(VEC3(-half.x, half.y, half.z), world);
				corners[4] = VEC3::Transform(VEC3(-half.x, -half.y, -half.z), world);
				corners[5] = VEC3::Transform(VEC3(half.x, -half.y, -half.z), world);
				corners[6] = VEC3::Transform(VEC3(half.x, half.y, -half.z), world);
				corners[7] = VEC3::Transform(VEC3(-half.x, half.y, -half.z), world);
				num_verts = 8;

				for (int j = 0; j < num_verts; j++) {
					if (corners[j].x < aabb_min.x)   aabb_min.x = corners[j].x;
					if (corners[j].y < aabb_min.y)   aabb_min.y = corners[j].y;
					if (corners[j].z < aabb_min.z)   aabb_min.z = corners[j].z;
					if (corners[j].x > aabb_max.x)   aabb_max.x = corners[j].x;
					if (corners[j].y > aabb_max.y)   aabb_max.y = corners[j].y;
					if (corners[j].z > aabb_max.z)   aabb_max.z = corners[j].z;
				}
			}
			break;
		}

		case PxGeometryType::ePLANE: {
			PxPlaneGeometry plane;
			if (shape->getPlaneGeometry(plane))
				break;
		}
		case PxGeometryType::eCAPSULE: {
			PxCapsuleGeometry capsule;
			if (shape->getCapsuleGeometry(capsule))
				break;
		}
		case PxGeometryType::eTRIANGLEMESH: {
			PxTriangleMeshGeometry trimesh;
			if (shape->getTriangleMeshGeometry(trimesh)) {
				PxTriangleMesh* triangleMesh = trimesh.triangleMesh;
				int num_verts = triangleMesh->getNbVertices();
				const PxVec3* vertices = triangleMesh->getVertices();
				for (int i = 0; i < num_verts; i++) {
					VEC3 vec3Vertice = PXVEC3_TO_VEC3(vertices[i]);
					vec3Vertice = VEC3::Transform(vec3Vertice, world);
					if (vec3Vertice.x < aabb_min.x)   aabb_min.x = vec3Vertice.x;
					if (vec3Vertice.y < aabb_min.y)   aabb_min.y = vec3Vertice.y;
					if (vec3Vertice.z < aabb_min.z)   aabb_min.z = vec3Vertice.z;
					if (vec3Vertice.x > aabb_max.x)   aabb_max.x = vec3Vertice.x;
					if (vec3Vertice.y > aabb_max.y)   aabb_max.y = vec3Vertice.y;
					if (vec3Vertice.z > aabb_max.z)   aabb_max.z = vec3Vertice.z;
				}
			}
				break;
		}
		default:
			break;
		}
	}
  }
}
