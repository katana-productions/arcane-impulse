#ifndef _NAVMESH_INPUT_INC
#define _NAVMESH_INPUT_INC

#include <vector>

class CNavmeshInput {
public:

  static const int MAX_INPUTS = 1024;

public:
  std::vector<CHandle>        handleInputs;
  VEC3           aabb_min;
  VEC3           aabb_max;
  
  //TODO
  float*                verts;
  int*                  tris;
  int                   nverts;
  int                   ntris;
  int                   nverts_total;
  int                   ntris_total;

public:
  CNavmeshInput( );

  void addInput(std::vector<CHandle> inputs);
  void clearInput( );
  void prepareInput( const CHandle input );
  void unprepareInput( );
  void computeBoundaries(  );
};

#endif
