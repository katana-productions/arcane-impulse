#pragma once

#include <vector>
#include "NavMesh\recast\include\Recast.h"
#include "NavMesh\recast\include\DetourNavMesh.h"
#include "NavMesh\recast\include\DetourNavMeshQuery.h"
#include "NavMesh\recast\include\DetourCrowd.h"
#include "navmesh_builder.h"
#include "navmesh_input.h"
#include "navmesh_render.h"

class CNavmesh {
public:

  enum {
    FLAG_WALK = 0x01
  , FLAG_SWIM = 0x02
  , FLAG_DISABLED = 0x10
  , ALL_FLAGS = 0xffff
  };

  enum EDrawMode {
      NAVMESH_DRAW_NONE = 0
    , NAVMESH_DRAW_TRANS
    , NAVMESH_DRAW_BVTREE
    , NAVMESH_DRAW_NODES
    , NAVMESH_DRAW_INVIS
    , NAVMESH_DRAW_MESH
    //, NAVMESH_DRAW_VOXELS
    //, NAVMESH_DRAW_VOXELS_WALKABLE
    //, NAVMESH_DRAW_COMPACT
    //, NAVMESH_DRAW_COMPACT_DISTANCE
    //, NAVMESH_DRAW_COMPACT_REGIONS
    //, NAVMESH_DRAW_REGION_CONNECTIONS
    //, NAVMESH_DRAW_RAW_CONTOURS
    //, NAVMESH_DRAW_BOTH_CONTOURS
    , NAVMESH_DRAW_COUNTOURS
    , NAVMESH_DRAW_POLYMESH
    , NAVMESH_DRAW_POLYMESH_DETAILS
    , NAVMESH_DRAW_TYPE_COUNT
  };

public:
  dtNavMesh*            m_navMesh;
  dtNavMeshQuery*       m_navQuery;
  dtCrowd*				m_navCrowd;
  DebugDrawGL           m_draw;

private:
  rcHeightfield*        m_solid;
  rcCompactHeightfield* m_chf;
  rcContourSet*         m_cset;
  rcPolyMesh*           m_pmesh;
  rcConfig              m_cfg;
  rcPolyMeshDetail*     m_dmesh;
  rcBuildContext*       m_ctx;
  unsigned char*        m_triareas;

  rcBuildContext        m_context;

  float m_agentRadius = 0.8f;
  float m_agentHeight = 2.0;
  float m_agentMaxClimb = 0.1;


public:
  CNavmeshInput         m_input;
  EDrawMode             m_draw_mode;

public:
  CNavmesh( );
  void prepareQueries();
  void prepareCrowd();
  void destroy();
  void render();

  void build( );
  dtNavMesh* create( const rcConfig& cfg );
  void dumpLog( );

  virtual float getAgentRadius() { return m_agentRadius; }
  virtual float getAgentHeight() { return m_agentHeight; }
  virtual float getAgentClimb() { return m_agentMaxClimb; }
};


