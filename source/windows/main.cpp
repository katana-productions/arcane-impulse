#include "mcv_platform.h"
#include "render/render.h"
#include "windows/app.h"
#include "render/primitives.h"
#include "render/module_render.h"
#include "components/common/comp_tags.h"
#include "audio/inc/fmod.hpp"
#include "engine.h"

using namespace FMOD;

void CApplication::generateFrame() {
  PROFILE_FRAME_BEGINS();
  PROFILE_FUNCTION("App::generateFrame");

  // This allow use of Imgui anytime during update/render
  auto& mod_render = CEngine::get().getRender();
  mod_render.beginImgui();

  // Update
  float elapsed = time_since_last_render.elapsedAndReset();
  Time.set(Time.current + elapsed);
  CEngine::get().update(Time.delta);

  // Render
  mod_render.generateFrame();

  // The last thing we do is render imgui menus
  mod_render.endImgui();
  Render.swapChain();
}

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow) {
  PROFILE_SET_NFRAMES(3);
  PROFILE_FRAME_BEGINS();

  Remotery* rmt;
  rmt_CreateGlobalInstance(&rmt);

  json cfg = loadJson("data/config.json");
  const json& cfg_render = cfg["render"];
  int render_width = cfg_render.value("width", 1280);
  int render_height = cfg_render.value("height", 800);
  bool fullScreen = cfg_render.value("fullscreen", false);
	int vsync = cfg_render.value("vsync", 0);

  CApplication app;
  if (!app.create(hInstance, nCmdShow, render_width, render_height))
    return -1;

  if (!Render.create( app.getHandle(), render_width, render_height))
    return -2;

  Render.fullscreen = fullScreen;
	Render.vsync = vsync;

  CEngine::get().registerResourceTypes();

  app.runMainLoop();

  Render.destroy();

  rmt_DestroyGlobalInstance(rmt);

	return 0;
}
