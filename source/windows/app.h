#pragma once

class CApplication {
  HWND             hWnd;
  CTimer           time_since_last_render;
  bool             has_focus = false;

  HRESULT InitWindow(HINSTANCE hInstance, int nCmdShow);

  static CApplication* the_app;
  static LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

  bool cursorVisible = false;
  VEC4 windowRect = VEC4(0, 0, 0, 0);
  VEC2 windowSize = VEC2(0, 0);
  VEC2 windowPos = VEC2(0, 0);

public:
  HWND getHandle() const { return hWnd; }
  static CApplication& get();

  bool create(HINSTANCE hInstance, int nCmdShow, int w, int h);
  void runMainLoop();
  void generateFrame();
  bool getFocus() const { return has_focus; }
  void setFocus(bool state) { has_focus = state; }

  void toggleCursorVisible()
  {
    cursorVisible = !cursorVisible;
    ShowCursor(cursorVisible);
  }

  void setCursorVisible(bool isVisible){
	  cursorVisible = isVisible;
	  ShowCursor(cursorVisible);
  }

  bool getCursorVisible() { return cursorVisible; }

  void setWindowRect(float left, float top, float right, float bottom)
  {
    windowRect.x = left;
    windowRect.y = top;
    windowRect.z = right;
    windowRect.w = bottom;
  }
  VEC4 getWindowRect() { return windowRect; }

  void setWindowSize(float w, float h)
  {
    windowSize.x = w;
    windowSize.y = h;
  }
  VEC2 getWindowSize() { return windowSize; }

  void setWindowPosition(float left, float top)
  {
    windowPos.x = left;
    windowPos.y = top;
  }
  VEC2 getWindowPosition() { return windowPos; }

  void ExitGame();
};
