#include "mcv_platform.h"
#include "engine.h"

#include "resources/resource.h"
#include "physics/tribuffer.h"
#include "render/module_render.h"
#include "render/textures/texture.h"
#include "render/textures/material.h"
#include "render/meshes/mesh.h"
#include "render/meshes/collision_mesh.h"
#include "render/shaders/technique.h"
#include "render/compute/compute_shader.h"
#include "skeleton/game_core_skeleton.h"
#include "skeleton/rigid_animation.h"
#include "geometry/curve.h"
#include "utils/json_resource.h"
#include "windows/app.h"
#include "render/module_render.h"
#include "render/module_gpu_culling.h"
#include "input/module_input.h"
#include "modules/module_camera_mixer.h"
#include "ui/module_ui.h"
#include "fsm/fsm.h"
#include "fsm/module_fsm.h"
#include "particles/module_particles.h"
#include "particles/particle_emitter.h"

#include "modules/module_pause.h"
#include "modules/module_ui_in_game.h"
#include "modules/test/module_intro.h"
#include "modules/test/module_splash.h"
#include "modules/test/module_reset.h"
#include "modules/test/module_main_menu.h"
#include "modules/module_credits.h"
#include "modules/module_title_game.h"
#include "modules/module_pre_intro.h"
#include "modules/test/module_gameplay.h"
#include "modules/test/module_sample_objs.h"
#include "modules/test/module_test_cameras.h"
#include "modules/module_boot.h"

#include "input/devices/device_keyboard.h"
#include "input/devices/device_mouse.h"
#include "input/devices/device_pad_xbox.h"
#include "utils/directory_watcher.h"

CDirectoyWatcher dir_watcher_data;

CEngine::CEngine()
{
}

CEngine::~CEngine()
{
#pragma message("TODO: delete modules!!!")
}

CEngine& CEngine::get()
{
  static CEngine engine;
  return engine;
}

void CEngine::registerResourceTypes() {
  PROFILE_FUNCTION("registerResourceTypes");
  Resources.registerResourceType(getResourceTypeFor<CMesh>());
  Resources.registerResourceType(getResourceTypeFor<CJson>());
  Resources.registerResourceType(getResourceTypeFor<CTexture>());
  Resources.registerResourceType(getResourceTypeFor<CTechnique>());
  Resources.registerResourceType(getResourceTypeFor<CMaterial>());
  Resources.registerResourceType(getResourceTypeFor<CCollisionMesh>());
  Resources.registerResourceType(getResourceTypeFor<CGameCoreSkeleton>());
  Resources.registerResourceType(getResourceTypeFor<CCurve>());
  Resources.registerResourceType(getResourceTypeFor<CTransCurve>());
  Resources.registerResourceType(getResourceTypeFor<CFSM>());
  Resources.registerResourceType(getResourceTypeFor<CRigidAnimation>());
  Resources.registerResourceType(getResourceTypeFor<CComputeShader>());
  Resources.registerResourceType(getResourceTypeFor<CTriBuffer>());
  Resources.registerResourceType(getResourceTypeFor<particles::TEmitter>());
}

void CEngine::start()
{
  PROFILE_FUNCTION("Engine::Start");
  _render = new CModuleRender("render");
  _entities = new CModuleEntities("entities");
  _physics = new CModulePhysics("physics");
  _alarms = new CAlarmClock("alarms");
  _enemyManager = new CEnemyManager("enemy_manager");
  _navMesh = new CNavMesh("navigation_mesh");
  _basics = new CBasics("basics");
  _cameraMixer = new CModuleCameraMixer("camera_mixer");
  _multithreading = new MultithreadingModule("multihtreading");
  _ui = new UI::CModuleUI("ui");
  _fsm = new FSM::CModuleFSM("fsm");
  _persistence = new CModulePersistence("persistence");
  _settings = new CModuleSettings("settings");
  _lua = new CModuleLua("lua");
  _console = new CModuleGameConsole("console");
  _gpu_culling = new CModuleGPUCulling();
  _worker = new CModuleBkgWorker("worker");
  _sceneLoader = new CModuleSceneLoader("sceneLoader");
  _audio = new CModuleAudio("audio");
  _decals = new CModuleDecals("decals");
  _moduleParticles = new CModuleParticles("module_particles");


  {
    PROFILE_FUNCTION("Input");
    _input = new Input::CModuleInput("input_1");
    _input->registerDevice(new CDeviceKeyboard("keyboard"));
    _input->registerDevice(new CDeviceMouse("mouse"));
    _input->registerDevice(new CDevicePadXbox("gamepad", 0));
    _input->assignMapping("data/input/mapping.json");
  }

  {
    PROFILE_FUNCTION("Sys Modules");
  _modules.registerSystemModule(_render);
  _modules.registerSystemModule(_physics);
  _modules.registerSystemModule(_entities);
  _modules.registerSystemModule(_input);
  _modules.registerSystemModule(_alarms);
  _modules.registerSystemModule(_navMesh);
  _modules.registerSystemModule(_enemyManager);
  _modules.registerSystemModule(_basics);
  _modules.registerSystemModule(_cameraMixer);
  _modules.registerSystemModule(_multithreading);
  _modules.registerSystemModule(_fsm);
  _modules.registerSystemModule(_persistence);
  _modules.registerSystemModule(_settings);
  _modules.registerSystemModule(_lua);
  _modules.registerSystemModule(_console);
  _modules.registerSystemModule(_ui);
  _modules.registerSystemModule(_gpu_culling);
  _modules.registerSystemModule(_worker);
  _modules.registerSystemModule(_sceneLoader);
  _modules.registerSystemModule(_audio);
  _modules.registerSystemModule(_decals);
  _modules.registerSystemModule(_moduleParticles);
 }

  _modules.registerGameModule(new CModuleBoot("boot"));
  _modules.registerGameModule(new CModuleSplash("splash"));
  _modules.registerGameModule(new CModuleIntro("intro"));
  _modules.registerGameModule(new CModuleMainMenu("main_menu"));
  _modules.registerGameModule(new CModuleTitleGame("title_game"));
  _modules.registerGameModule(new CModuleCredits ("credits"));
  _modules.registerGameModule(new CModulePreIntro("pre_intro"));

  _modules.registerGameModule(new CModuleMenuPause("pause_menu"));
  _modules.registerGameModule(new CModuleUiInGame ("ui_in_game"));
  _modules.registerGameModule(new CModuleGameplay("gameplay"));
  _modules.registerGameModule(new CModuleSampleObjs("sample_objs"));
  _modules.registerGameModule(new CModuleReset("reset"));
  _modules.registerGameModule(new CModuleTestCameras("test_cameras"));
  //_modules.registerGameModule(new CModuleTestInstancing("test_instancing"));
  
  _modules.start();

  dir_watcher_data.start("data", CApplication::get().getHandle());
}

void CEngine::stop()
{
  _modules.stop();
}

void CEngine::update(float dt)
{
  _modules.update(dt);
}

void CEngine::renderDebug()
{
  _modules.renderDebug();
}

CModuleManager& CEngine::getModules()
{
  return _modules;
}
