#pragma once

#include "modules/module.h"
#include "audio/SoundEvent.h"


class CModuleCredits : public IModule
{
public:
	CModuleCredits(const std::string& name);
	bool start() override;
	void stop() override;
	void update(float dt) override;

private:
	void setPause(bool p);
};