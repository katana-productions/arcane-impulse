#pragma once

#include "modules/module.h"
#include "audio/SoundEvent.h"


class CModuleTitleGame : public IModule
{
public:
	CModuleTitleGame(const std::string& name);
	bool start() override;
	void stop() override;
	void update(float dt) override;

private:
	void onOptionStart();

	void setPause(bool p);

	CSoundEvent music;

};