#pragma once

#include "modules/module.h"
#include "components/common/comp_transform.h"
#include "SLB/include/SLB/SLB.hpp"

// Modules
#include "modules/module_console.h"
#include "modules/test/module_gameplay.h"
#include "modules/module_enemy_manager.h"

struct ConsoleResult {
  bool success;
  std::string resultMsg;
};

struct CommandVariable {
  void* variable;
};

struct DelayedScript {
  std::string script;
  float remainingTime;
};

class CModuleLua : public IModule {
private:
  bool LuaLoaded = false;
  bool gameStart = false;

  SLB::Manager* m;
  SLB::Script* s;
  void BootLuaSLB();
  void publishClasses();
  void loadScriptsInFolder(const char* path);

public:
  bool isPaused = false;
  std::vector<std::string> log;
  std::vector<DelayedScript> delayedScripts;
  std::vector<DelayedScript> delayedSystemScripts;

  CModuleLua(const std::string& name);

  bool start() override;
  void stop() override;
  void update(float delta) override;
  void renderDebug() override;
  void renderInMenu() override;

  /*********** LUA ************/
  bool isLuaLoaded() { return LuaLoaded; }
  void printLog();
  void doString(std::string c) { s->doString(c); }

  void runFile(const char* filename);
  void runCode(const char* code);

  static ConsoleResult execScript(const std::string& script);
  static bool execScriptDelayed(const std::string& script, float delay);
  static bool execSystemScriptDelayed(const std::string& script, float delay);

  /*****************************/

  /******* Aux Functions *******/
  // Module getters
  static CModuleGameConsole* getConsole();
  static CModuleGameplay* getGameplay();
  // TODO: change this
  static CEnemyManager* getEnemyManager();

  // Utils
  //static int getJsonSize(std::string& filename);
  static void write(std::string n);
  static std::string VEC3tostring(VEC3 v);
  static CHandle getPlayer();
  static VEC3 getEntityPos(std::string n);
  static void setEntityPos(std::string n, VEC3 pos);
  static VEC3 getEntityAngles(std::string n);
  static void setEntityAngles(std::string n, VEC3 angles);
  static VEC3 getEntityFront(std::string n);
  static VEC3 getEntityLeft(std::string n);
  static VEC3 getEntityUp(std::string n);
  static void destroyEntity(std::string n);

  // Gamestates
  static void togglePauseGame();
  static void changeGameState(std::string n);
  static void changeGameStateFinal(std::string n);
  static void resetGame();
  static void setMainMenuCamera(bool b);

  // Enemies
  static void togglePauseEnemies();
  static void ragdollEnemies();
  static void resetEnemies();
  static void spawn();
  static void setCinematicControl(std::string n, bool c);
  static void setEnemieViewPlayer(std::string n, bool c);
  static void moveToPositionCinematic(std::string n, VEC3 pos);
  static void setBerserkMode(std::string n, bool c);

  // Player
  static void toggleInput();
  static void setInputEnabled(bool b);
  static void toggleImmortal();
  static void setImmortal(bool b);
  static void toggleInfMagic();
  static void setPlayerSpeed(float s);
  static void setPlayerGravity(float g);
  static void togglePlayerInvisible();
  static void activateMagic();
  static void deactivateMagic();
  static void teleport(VEC3 &pos, VEC3 &front);
  static void teleportYPR(VEC3 &pos, float yaw, float pitch, float roll);
  static void damagePlayer(float dmg);
  static void healPlayer(float hp);
  static void launchCinematicDeath();
  static VEC3 getCameraPosition();
  static VEC3 getCameraFront();
  static void setLeftHandLockCamera(bool b);
  static void setRightHandLockCamera(bool b);
  static void fadeInToBlack(float time);
  static void fadeOutToBlack(float time);
  static void fadeInToWhite(float time);
  static void fadeOutToWhite(float time);
  static void setVisibleDeathUI(bool b);
  static void launchPlayerDeath();
  static void launchPlayerDeathEnd();
  static void setGlovesRandGlow(bool b);
  static void blockPlayer();
  static void unlockPlayer();
  static void setPlayerVisible(bool b);

  static void setRandomCamShake(float intensity, float duration, float roughness);
  static void stopPlayerAnimations();


  //Scenes
  static void loadSceneNoBlock(const std::string& s);
  static void unloadSceneNoBlock(const std::string& s);
  static void loadSceneBlock(const std::string& s);
  static void unloadScene(const std::string& s);
  static void createNavMesh();
  static void pausePhysx(bool b);

  // Render
  static void activateCompleteMode();
  static void activateCheapMode();
  static void activateNormalMode();
  static void launchParticleAtTransform(std::string p, VEC3 pos, VEC3 angles);

  // Sounds
  static void playSoundEvent(std::string c);
  static void playSFX(std::string c, std::string d);
	static void playDialogue(int e);
	static void stopVoices();

  // Cinematics
  static void launchCinematic(std::string c);

  // GUI
  static void showUIMessage(std::string c);
  static void hideUIMessage(std::string c);
  static void hideUIinGame(std::string c);
  static void showUIinGame(std::string c);
  static void setUIVisible(bool b);

  static void activateSubs();
  static void deactivateSubs();
  static void skipIntroDialogue();
  static void skipDialogue();

  static void setSubsText(std::string t);
  static void setSubsLine(int sub_line);
  static void setSubsLanguage(int l);
  static void changeSubBackgroundSize();
  static void setUIScrollSpeed(std::string m, float speed_x, float speed_y);

  static void lerpUIScrollSpeed(std::string m, float speed_x, float speed_y, float t);
  static void setInitImageUV(std::string m);
  static void setImageAlpha(std::string m, float desiredAlpha);
  static void lerpImageAlpha(std::string m, float desiredAlpha, float t);
  static void lerpImageBurnAmount(std::string m, float desiredBurnAmout, float t);

  static void enabledTriggerTutorial(std::string m, bool isEnabled);
  static void stateWidget(std::string m, bool enable);

  // Converters
  CEntity* toEntity(CHandle h);
  TCompTransform* toTransform(CHandle h);

  // Events
  static void launchEvent(int e);
  static void setOpenDoor(std::string n, bool o);
  static void setOpenDoorFinal(bool o);
  static void loadFinalMessage();
  static void sendToMenu();
  static void loadScenesPersistance(std::string scene);
  static void loadStartEvent(std::string scene);
  static void loadFinishEvent();

  //Instantiate
  static void instantiateTrigger(std::string name);
  /*****************************/
};
