#pragma once

#include "modules/module.h"
#include "components/common/comp_transform.h"

class CEnemyManager : public IModule {

	struct StrategicPosition {
		VEC3 position;
		bool busy;
		CHandle owner;
	};
	struct MeleeFighter {
		CHandle fighter;
		bool inCombat;
		bool attacking;
		bool isProtected;
	};
	struct MageFighter {
		CHandle fighter;
		bool inCombat;
	};

	std::vector<MeleeFighter> meleeFightersSBB;
	std::vector<MageFighter> magesFightersSBB;
	std::vector<StrategicPosition> strategicCombatPositionsSBB;


	float timeOneGnomeDeath = 0;
	bool canDieByOtherGnome = true;
public:
  CEnemyManager(const std::string& name);

	bool start() override;
	void stop() override;
	void update(float delta) override;
	void renderDebug() override;
	void renderInMenu() override;

  // TODO: add a way to retrieve all enemies

	void addMageFighterSBB(CHandle new_fighter);
	void eraseMageFighterSBB(CHandle erase_fighter);

	//void InCombatMageSBB(CHandle new_fighter);
	//void StopCombatMageSBB(CHandle new_fighter);

	CHandle ProtectMeleeFighterSBB();
	void UnProtectMeleeFighterSBB(CHandle fighter);

	void addMeleeFighterSBB(CHandle new_fighter);
	void EraseMeleeFighterSBB(CHandle erase_fighter);

	void InCombatMeleeFighterSBB(CHandle new_fighter);
	void StopCombatMeleeFighterSBB(CHandle new_fighter);
	//bool AttackingMeleeFighterSBB(CHandle new_fighter);
	//void stopAttackingMeleeFighterSBB(CHandle new_fighter);
	bool oneGoblinToGoblinDeath();


	VEC3 OccupyStrategicPositionSBB(CHandle owner, float acceptable_distance); 
	void SetFreeStrategicPositionSSB(CHandle owner);
	void InsertStrategicPositionSBB(VEC3 position); 

	void ClearSBB();
	
	void WarnOtherEnemies();
	void IncreaseInCombatFighters();
	void DecreaseInCombatFighters();

	//void CheckPanic();

private:
	int numMaxSlots = 2;
	int numSlotsBusy = 0;

	int numFightersInCombat = 0;
	int numMaxFightersInCombat = 0;

	float timeCDWarn = 2;

	bool cdWarnOtherEnemies = true;
};