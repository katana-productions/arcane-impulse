#pragma once

#include "modules/module.h"
#include "audio/SoundEvent.h"


class CModuleMenuPause : public IModule
{
public:
	CModuleMenuPause(const std::string& name);
	bool start() override;
	void stop() override;

private:
	void onOptionContinue();
	void onOptionExit();
	void onOptionExitDesktop();
	void setPause(bool p);

	CSoundEvent music;
};