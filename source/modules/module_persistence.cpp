#include "mcv_platform.h"
#include "engine.h"
#include "handle/handle.h"
#include "modules/module_persistence.h"
#include "input\input.h"
#include "components\controllers\comp_char_controller.h"
#include "components\controllers\comp_camera_fp.h"
#include "components\player\comp_player_magic.h"
#include "components/player/comp_player_stats.h"
#include "components/player/comp_player_move.h"
#include "components/common/comp_fsm.h"
#include "components/other/comp_player_spawn.h"
#include "components/postfx/comp_render_vignette.h"
#include "components/postfx/comp_render_outlines.h"
#include "components/postfx/comp_color_grading.h"
#include "ui/module_ui.h"
#include "ui/widgets/ui_progress.h"
#include "ui/widgets/ui_image.h"
#include "render\module_render.h"


CModulePersistence::CModulePersistence(const std::string& name)
	: IModule(name)
{}

bool CModulePersistence::start() {
	load();
	return true;
}

void CModulePersistence::stop() {
	brokenPieces.clear();
}

void CModulePersistence::update(float delta) {

}

void CModulePersistence::renderDebug() {}

void CModulePersistence::renderInMenu() {
	if (ImGui::TreeNode("Persistence"))
	{
		ImGui::DragInt("Id", &spawnPointId, 0, 1, 20);
		ImGui::DragFloat("globalAmbient", &globalAmbient, 0, 0.05, 5);
		ImGui::DragFloat("globalExposure", &globalExposure, 0, 0.05, 5);
		ImGui::DragFloat("vigExtend", &vigExtend, 0, 0.05, 5);
		ImGui::DragFloat("vigIntensity", &vigIntensity, 0, 0.05, 50);
		ImGui::DragFloat("fogDepthFade", &fogDepthFade, 0, 0.05, 5);
		ImGui::DragFloat("maxFog", &maxFog, 0, 0.05, 5);
		ImGui::ColorEdit3("Fog Color", (float*)&fogColor.x);
		ImGui::Checkbox("pullWallUnlocked", &pullWallUnlocked);
		ImGui::Checkbox("pushWallUnlocked", &pushWallUnlocked);
		ImGui::Checkbox("pullObjUnlocked", &pullObjUnlocked);
		ImGui::Checkbox("pushObjUnlocked", &pushObjUnlocked);
		ImGui::Text("oldScene = %s", oldScene);
		ImGui::Text("loadScene = %s", sceneToLoad);
		ImGui::LabelText("Core Puzzles: ", "%i/%i", coresDeactivated, totalCores);
		ImGui::DragInt("EnemyCounter", &enemyCounter, 0, 1, 20);
		ImGui::Checkbox("finalMusic", &finalMusic);

		ImGui::TreePop();
	}
}


void CModulePersistence::resetPlayerPos() {
	bool spawn_point = false;
	TCompPlayerSpawn* spawnComp = nullptr;

	CEntity* player = getEntityByName("Player");
	TCompCharController* charcontroller = player->get<TCompCharController>();
	charcontroller->SetFootPos(pos[spawnPointId]);
	TCompPlayerStats* c_p_stats = player->get<TCompPlayerStats>();
	c_p_stats->heal(c_p_stats->getMaxHealth());

	CEntity* camera = getEntityByName("Camera");
	TCompTransform* cameraFP = camera->get<TCompTransform>();
	cameraFP->setAngles(rot[spawnPointId].x, rot[spawnPointId].y, 0);
}

void CModulePersistence::setMagic(bool pullObj, bool pullWall, bool pushObj, bool pushWall) {
	pullWallUnlocked = pullWall;
	pullObjUnlocked = pullObj;
	pushWallUnlocked = pushWall;
	pushObjUnlocked = pushObj;
	setPlayerMagic();
}

void CModulePersistence::setPlayerMagic() {
	CEntity* player = getEntityByName("Player");
	TCompPlayerMagic* magic = player->get<TCompPlayerMagic>();

	magic->setPullObjUnlocked(pullObjUnlocked);
	magic->setPullWallUnlocked(pullWallUnlocked);
	magic->setPushObjUnlocked(pushObjUnlocked);
	magic->setPushWallUnlocked(pushWallUnlocked);
}

void CModulePersistence::setId(int id) {
	if (id > spawnPointId) spawnPointId = id;
}

void CModulePersistence::setGloves(bool glove) {
	if (glove != gloves) {
		gloves = glove;

		setPlayerGloves();
	}
}

void CModulePersistence::setUIMagicPower(bool enable) {
	if (currentUIMagicPowerState != enable || firstFrameUIGloves) {
		UI::CWidget* uiPowerCristal = dynamic_cast<UI::CWidget*>(Engine.getUI().getWidgetByAlias("power_cristal"));
		if (enable) {
			uiPowerCristal->getParams()->visible = true;
		}
		else {
			uiPowerCristal->getParams()->visible = false;
		}
		currentUIMagicPowerState = enable;
		firstFrameUIGloves = false;
	}
}

void CModulePersistence::setPlayerGloves() {
	CEntity* handRight = getEntityByName("LeftHand");
	CEntity* handLeft = getEntityByName("RightHand");

	if (gloves) {
		TEntityParseContext ctx;
		parseSceneRecursive("data/prefabs/hands.json", ctx);
		if (handRight) CHandle(handRight).getOwner().destroy();
		if (handLeft) CHandle(handLeft).getOwner().destroy();

		CEntity* e_left_hand = getEntityByName("LeftHand");
		TCompFSM* cFsm_left = e_left_hand->get<TCompFSM>();
		CEntity* e_right_hand = getEntityByName("RightHand");
		TCompFSM* cFsm_right = e_right_hand->get<TCompFSM>();
		if (cFsm_right && cFsm_left) {
			cFsm_right->changeVariable("beginPlay", 1, true);
			cFsm_left->changeVariable("beginPlay", 1, true);
		}
		CHandle glovesHandle = getEntityByName("Gloves");
		if (glovesHandle.isValid())
		{
			glovesHandle.destroy();
		}
	}
	else {
		TEntityParseContext ctx;
		parseSceneRecursive("data/prefabs/gloveless_hands.json", ctx);
		//if (handRight) CHandle(handRight).getOwner().destroy();
		//if (handLeft) CHandle(handLeft).getOwner().destroy();
	}

}

void CModulePersistence::setPowers() {
	pullWallUnlocked = true;
	pullObjUnlocked = true;
	pushWallUnlocked = true;
	pushObjUnlocked = true;
	setPlayerMagic();
}

void CModulePersistence::setCheckpoint() {
	if (spawnPointId < pos.size() - 1) ++spawnPointId;
	else spawnPointId = 0;
	resetPlayerPos();
}

void CModulePersistence::setPostProcess(const float& gAmbient, const float& gExposure, const float& vExtend,
	const float& vIntensity, const float& fDepthFade, const float& mFog, const float& fContrast,
	const float4& fColor, const std::string& lutN) {
	globalAmbient = gAmbient;
	globalExposure = gExposure;
	vigExtend = vExtend;
	vigIntensity = vIntensity;
	fogDepthFade = fDepthFade;
	maxFog = mFog;
	fogContrast = fContrast;
	fogColor = fColor;
	lutName = lutN;
	lutHasBeenLoaded = true;
}

void CModulePersistence::blockPlayer(bool isPaused) {
	CEntity* player = getEntityByName("Player");
	if (!player) return;
	TCompPlayerMove* c_playermove = player->get<TCompPlayerMove>();
	c_playermove->setPaused(isPaused);

	TCompPlayerMagic* c_magic = player->get<TCompPlayerMagic>();
	c_magic->setPaused(isPaused);
}



void CModulePersistence::load()
{
	//Checkpoints
	json json = loadJson("data/persistence/checkpoints.json");
	if (json != NULL) {
		pos = loadVectorVEC3(json, "pos", 17);
		rot = loadVectorVEC3(json, "rot", 17);
	}
	else {
		pos[0] = VEC3(0, 0, 0);
		rot[0] = VEC3(0, 0, 0);
	}

	//Scenes
	scenes = loadJson("data/boot.json");

	//Persistence Vars
	json = loadJson("data/persistence/persistence_vars.json");

	//Powers
	pullWallUnlocked = json.value("pullWallUnlocked", pullWallUnlocked);
	pullObjUnlocked = json.value("pullObjUnlocked", pullObjUnlocked);
	pushWallUnlocked = json.value("pushWallUnlocked", pushWallUnlocked);
	pushObjUnlocked = json.value("pushObjUnlocked", pushObjUnlocked);
	gloves = json.value("gloves", gloves);

	//Old scene
	sceneToLoad = json.value("sceneToLoad", sceneToLoad);
	oldScene = json.value("oldScene", oldScene);

	//Total cores
	totalCores = json.value("totalCores", totalCores);

}

void CModulePersistence::loadPostProcess() {
	json json = loadJson("data/persistence/persistence_vars.json");

	auto& j_scene = json[sceneToLoad];
	if (j_scene.size() > 0) {
		globalAmbient = j_scene.value("globalAmbient", globalAmbient);
		globalExposure = j_scene.value("globalExposure", globalExposure);
		vigExtend = j_scene.value("vigExtend", vigExtend);
		vigIntensity = j_scene.value("vigIntensity", vigIntensity);
		fogDepthFade = j_scene.value("fogDepthFade", fogDepthFade);
		maxFog = j_scene.value("maxFog", maxFog);
		fogContrast = j_scene.value("fogContrast", fogContrast);
		fogColor = loadVEC4(j_scene, "fogColor");
		lutName = j_scene.value("lut", "data/textures/lut/neutralLut.dds");
	}

	updatePostProcess();
}

void CModulePersistence::updatePostProcess() {
	EngineRender.globalAmbient = globalAmbient;
	EngineRender.globalExposure = globalExposure;

	CEntity* camera = getEntityByName("CameraShaker");
	TCompVignette* vignette = camera->get<TCompVignette>();
	TCompOutlines* outlines = camera->get<TCompOutlines>();
	TCompColorGrading* colGrading = camera->get<TCompColorGrading>();

	vignette->vigExtend = vigExtend;
	vignette->vigIntensity = vigIntensity;

	outlines->fogDepthFade = fogDepthFade;
	outlines->maxFog = maxFog;
	outlines->fogColor = fogColor;
	outlines->fogContrast = fogContrast;

	if(lutHasBeenLoaded) colGrading->CreateLutTexture(lutName);

  camera = getEntityByName("Camera Debug");
  vignette = camera->get<TCompVignette>();
  outlines = camera->get<TCompOutlines>();
  colGrading = camera->get<TCompColorGrading>();

  vignette->vigExtend = vigExtend;
  vignette->vigIntensity = vigIntensity;

  outlines->fogDepthFade = fogDepthFade;
  outlines->maxFog = maxFog;
  outlines->fogColor = fogColor;
  outlines->fogContrast = fogContrast;

  if (lutHasBeenLoaded) colGrading->CreateLutTexture(lutName);
}

void CModulePersistence::loadScenes() {

	blockPlayer(true);

	auto prefabs = scenes[sceneToLoad].get< std::vector< std::string > >();
	auto unload = scenes[oldScene].get<std::vector <std::string>>();

	for (auto& s : unload) {
		//EngineSceneLoader.unloadSceneNoBlock(s);
		EngineSceneLoader.unloadScene(s);
	}

	EngineEnemyManager.ClearSBB();

	for (auto& p : prefabs) {
		EngineSceneLoader.loadSceneNoBlock(p);
		//EngineSceneLoader.loadSceneBlock(p);
	}

	prefabs.clear();
	unload.clear();

	for (auto& piece : brokenPieces) {
		if (piece.isValid())
			piece.destroy();
	}

	brokenPieces.clear();

	blockPlayer(false);

	EngineNavMesh.buildNavMesh();
	EngineLua.loadFinishEvent();
}

void CModulePersistence::setScene(std::string scene) {
	if (oldScene != sceneToLoad) {
		oldScene = sceneToLoad;
		sceneToLoad = scene;
		EngineGpuCulling.stop();
		loadScenes();
	}
}

void CModulePersistence::setOldScene(std::string scene) {
	oldScene = scene;
}

void CModulePersistence::setCheckpointId(int id) {
	spawnPointId = id;
}

void CModulePersistence::setEnemyCounter(int cont) {
	if ((enemyCounter + cont) >= 0) enemyCounter = enemyCounter + cont;
}

void CModulePersistence::deactivateCore() {
	coresDeactivated++;
}

void CModulePersistence::resetCores() {
	coresDeactivated = 0;
}

void CModulePersistence::resetGame() {
	//Delete when new UI
	CEntity* e_ui = getEntityByName("UI");
	TCompUI* c_ui = e_ui->get<TCompUI>();
	c_ui->setVisible(false);
	//
	setCheckpointId(0);
	sceneToLoad = "Initial";
	load();
	loadPostProcess();
	gameState = Engine.getModules().getCurrentGamestate();
	if (gameState == "gs_credits")
		gameState = "gs_title_game";

	endGame = 0;
	EngineAudio.stop();
	EngineLua.skipDialogue();
	Engine.getModules().changeToGamestate("gs_reset");
}
