#pragma once

#include "modules/module.h"
#include "navmesh_engine/navmesh.h"
#include "navmesh_engine/navmesh_query.h"
#include "navmesh_engine/navmesh_crowd.h"

class CNavMesh : public IModule {
public:
	CNavMesh(const std::string& name);

	bool start() override;
	void stop() override;
	void update(float delta) override;
	void renderDebug() override;
	void renderInMenu() override;

	void addHandle(CHandle handle);
	void buildNavMesh();

	int addAgent(VEC3 pos, float speed, float radius);
	void removeAgent(int idx);
	VEC3 getAgentVel(int idx);
	void changeAgentSpeed(int idx, float speed);
	void setAgentState(int idx, bool isActive);
	void moveCrowd(VEC3 pos);
	void moveAgentCrowd(int idx, VEC3 pos);
	void setAgentPos(int idx, VEC3 pos);
	void setAgentDirection(int idx, VEC3 normalizedDir);
	VEC3 getAgentPosition(int idx);
	bool findPath(VEC3 src, VEC3 dst, std::vector<VEC3> *path);

private:
	CNavmesh nav;
	CNavmeshQuery nav_query;
	CrowdToolState nav_crowd;
	bool navMeshBuilded = false;
	std::vector<CHandle> handlesNavMesh;
};