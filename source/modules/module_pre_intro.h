#pragma once

#include "modules/module.h"
#include "audio/SoundEvent.h"


class CModulePreIntro : public IModule
{
public:
	CModulePreIntro(const std::string& name);
	bool start() override;
	void stop() override;
	void update(float dt) override;

private:

};