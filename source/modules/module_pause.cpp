#include "mcv_platform.h"
#include "module_pause.h"
#include "engine.h"
#include "input/input.h"
#include "ui/module_ui.h"
#include "ui/widgets/ui_progress.h"
#include "ui/controllers/ui_menu_controller.h"
#include "ui/widgets/ui_button.h"
#include "components/player/comp_ui.h"
#include "audio/SoundEvent.h"


CModuleMenuPause::CModuleMenuPause(const std::string& name)
	: IModule(name)
{}

bool CModuleMenuPause::start()
{
	setPause(true);
	EngineAudio.pauseSounds();
	EnginePersistence.blockPlayer(true);
	EngineSettings.setEnabled(false);
	EngineSettings.setInPause(true);
	CApplication::get().setCursorVisible(true);

	if (!EngineAudio.musics.isValid()) {
		EngineAudio.musics = EngineAudio.playEvent("event:/Music/MusicMenu", 0);
	}

	UI::CModuleUI& ui = Engine.getUI();
	ui.activateWidget("pause_menu");

	UI::CMenuController* menu = new UI::CMenuController;

	menu->registerOption(dynamic_cast<UI::CButton*>(ui.getWidgetByAlias("bt_continue_pause")), std::bind(&CModuleMenuPause::onOptionContinue, this));
	menu->registerOption(dynamic_cast<UI::CButton*>(ui.getWidgetByAlias("bt_exit_pause")), std::bind(&CModuleMenuPause::onOptionExit, this));
	menu->registerOption(dynamic_cast<UI::CButton*>(ui.getWidgetByAlias("bt_exit_desktop")), std::bind(&CModuleMenuPause::onOptionExitDesktop, this));

	menu->setCurrentOption(0);

	ui.registerController(menu);
	return true;
}

void CModuleMenuPause::onOptionContinue()
{
	EngineAudio.resumeSounds();
	EngineAudio.musics.Stop();
	Engine.getModules().changeToGamestate("gs_gameplay");
	printf("CONTINUE");
}

void CModuleMenuPause::onOptionExit()
{	
	EngineAudio.musics.Stop();
	Engine.getUI().deactivateWidget("pause_menu");
	EnginePersistence.resetGame();
	printf("EXIT");
}

void CModuleMenuPause::onOptionExitDesktop()
{
    CApplication::get().ExitGame();
}

void CModuleMenuPause::stop()
{
	setPause(false);
	EngineAudio.resumeSounds();
	EngineAudio.musics.Stop();

	Engine.getSettings().setInGame(true);
	Engine.getPersistence().blockPlayer(false);
	Engine.getSettings().setEnabled(true);
	CApplication::get().setCursorVisible(false);

	EngineSettings.setInPause(false);
	Engine.getUI().unregisterControllers();
	Engine.getUI().deactivateWidget("pause_menu");
}

void CModuleMenuPause::setPause(bool p)
{
	EnginePhysics.isPaused = p;
	if (EnginePhysics.isPaused) Time.scale_factor = 0.0f;
	else Time.scale_factor = 1.0f;
}