#include "mcv_platform.h"
#include "module_credits.h"
#include "engine.h"
#include "input/input.h"
#include "ui/module_ui.h"
#include "ui/widgets/ui_progress.h"
#include "ui/controllers/ui_menu_controller.h"
#include "ui/widgets/ui_button.h"
#include "components/player/comp_ui.h"
#include "audio/SoundEvent.h"


CModuleCredits::CModuleCredits(const std::string& name)
	: IModule(name)
{}

bool CModuleCredits::start()
{
	EngineAudio.stop();
	EngineAudio.start();

	if (!EngineAudio.musics.isValid()) {
		EngineAudio.musics = EngineAudio.playEvent("event:/Music/MusicCredits", 0);
	}
	else {
		EngineAudio.musics.Restart();
	}

	UI::CModuleUI& ui = Engine.getUI();

	ui.activateWidget("credits");

	EngineLua.launchEvent(10);

	Engine.getSettings().setInGame(false);
	Engine.getPersistence().blockPlayer(true);

	return true;
}

void CModuleCredits::stop()
{
	Engine.getUI().deactivateWidget("credits");
	EngineAudio.musics.Stop();
}

void CModuleCredits::update(float dt)
{
}

void CModuleCredits::setPause(bool p)
{
	EnginePhysics.isPaused = p;
	if (EnginePhysics.isPaused) Time.scale_factor = 0.0f;
	else Time.scale_factor = 1.0f;
}
