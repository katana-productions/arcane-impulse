#include "mcv_platform.h"

// Data structures
#include <experimental/filesystem>

// Components
#include "components/player/comp_player_stats.h"
#include "components/player/comp_player_magic.h"
#include "components/controllers/comp_char_controller.h"
#include "components/controllers/comp_camera_fp.h"
#include "components/controllers/camShake.h"
#include "components/controllers/camMovement.h"
#include "components/controllers/comp_camera_transition.h"
#include "components/ai/bt_gnome.h"
#include "components/ai/bt_mage.h"
#include "components/puzzles/door_baby_puzzle.h"
#include "components/puzzles/door_enemies.h"
#include "components/puzzles/door_timed.h"
#include "components/puzzles/door_player_goto.h"
#include "components/puzzles/door_controller.h"
#include "components/player/handsSet.h"
#include "components/other/comp_glove_glow.h"
#include "components/other/comp_tutorial_trigger.h"
#include "components/common/comp_fsm.h"

// Other classes
#include "handle/handle.h"
#include "engine.h"
#include "ui/module_ui.h"
#include "ui/effects/ui_fx_animate_uv.h"
#include "ui/widgets/ui_progress.h"
#include "ui/widgets/ui_image.h"
#include "ui/widgets/ui_text.h"
#include "input/input.h"


CModuleLua::CModuleLua(const std::string& name)
  : IModule(name)
{}

bool CModuleLua::start() {
  m = new SLB::Manager;
  s = new SLB::Script(m);
  BootLuaSLB();
  return true;
}

void CModuleLua::stop() 
{
  delete(s);
  delete(m);
  LuaLoaded = false;
}

void CModuleLua::update(float dt)
{
  if (!gameStart)
  {
    // Run onGameStart scripts
    std::string c = "onGameStart()";
    s->doString(c);
    gameStart = true;
  }

  // Delayed system scripts
  // can't be paused
  //for (int i = delayedSystemScripts.size() - 1; i >= 0; i--) {
  // TODO: turn this into lists for more efficient delete maybe
  for (int i = 0; i < delayedSystemScripts.size(); ++i)
  {
    delayedSystemScripts[i].remainingTime -= Time.delta_unscaled;
    if (delayedSystemScripts[i].remainingTime <= 0) {
      std::string aux_call = delayedSystemScripts[i].script;
      delayedSystemScripts.erase(delayedSystemScripts.begin() + i);
      execScript(aux_call);
    }
  }
  // Delayed scripts
  if (!isPaused) {
    for (int i = 0; i < delayedScripts.size(); ++i)
    {
      delayedScripts[i].remainingTime -= dt;
      if (delayedScripts[i].remainingTime <= 0) {
        std::string aux_call_delayed = delayedScripts[i].script;
        delayedScripts.erase(delayedScripts.begin() + i);
        execScript(aux_call_delayed);
      }
    }
  }

  // Update Sys Coroutines
  // can't be paused
  char c1[64];
  sprintf(c1, "updateSysCoroutines(%f)", Time.delta_unscaled);
  s->doString(c1);

  // Update Coroutines
  char c2[64];
  sprintf(c2, "updateCoroutines(%f)", dt);
  if (!isPaused)
    s->doString(c2);
}

void CModuleLua::renderDebug() {}

void CModuleLua::renderInMenu() 
{
  if (ImGui::TreeNode("Logic")) {

    if (ImGui::TreeNode("Delayed Scripts")) {
      for (int i = 0; i < delayedScripts.size(); i++) {
        ImGui::Text("%s - %f", delayedScripts[i].script.c_str(), delayedScripts[i].remainingTime);
      }
      ImGui::TreePop();
    }

    if (ImGui::TreeNode("System Delayed Scripts")) {
      for (int i = 0; i < delayedSystemScripts.size(); i++) {
        ImGui::Text("%s - %f", delayedSystemScripts[i].script.c_str(), delayedSystemScripts[i].remainingTime);
      }
      ImGui::TreePop();
    }
    ImGui::TreePop();
  }
}

/******************** LUA ***********************/

void CModuleLua::printLog()
{
  dbg("Printing log\n");
  for (int i = 0; i < log.size(); i++) {
    dbg("%s\n", log[i].c_str());
  }
  dbg("End printing log\n");
}

void CModuleLua::runFile(const char* filename)
{
  s->doFile(filename);
}

void CModuleLua::runCode(const char* code)
{
  s->doString(code);
}

void CModuleLua::BootLuaSLB()
{
  publishClasses();
  loadScriptsInFolder("data/scripts");
  //Mark LUA as started
  LuaLoaded = true;
}

//Function to publish all classes needed for use in scripting
void CModuleLua::publishClasses()
{
  // Modules
  SLB::Class< CModuleLua, SLB::Instance::NoCopyNoDestroy >("Logic", m)
    .comment("Wrapper of the logic class")

    // Utils
    .set("write", CModuleLua::write)
    .set("VEC3tostring", CModuleLua::VEC3tostring)
    .set("execScriptDelayed", CModuleLua::execScriptDelayed)
    .set("execSysScriptDelayed", CModuleLua::execSystemScriptDelayed)
    .set("execScript", CModuleLua::execScript)
    .set("setEntityPos", CModuleLua::setEntityPos)
    .set("getEntityPos", CModuleLua::getEntityPos)
    .set("getEntityAngles", CModuleLua::getEntityAngles)
    .set("setEntityAngles", CModuleLua::setEntityAngles)
    .set("getEntityFront", CModuleLua::getEntityFront)
    .set("getEntityLeft", CModuleLua::getEntityLeft)
    .set("getEntityUp", CModuleLua::getEntityUp)
	  .set("destroyEntity", CModuleLua::destroyEntity)
	  
    // Gamestates
    .set("pause", CModuleLua::togglePauseGame)
    .set("changeGameState", CModuleLua::changeGameState)
    .set("changeGameStateFinal", CModuleLua::changeGameStateFinal)
    .set("resetGame", CModuleLua::resetGame)
    .set("setMainMenuCamera", CModuleLua::setMainMenuCamera)

    // Player
    .set("immortal", CModuleLua::toggleImmortal)
    .set("setImmortal", CModuleLua::setImmortal)
    .set("infMagic", CModuleLua::toggleInfMagic)
    .set("activateMagic", CModuleLua::activateMagic)
    .set("deactivateMagic", CModuleLua::deactivateMagic)
    .set("teleportPlayer", CModuleLua::teleport)
    .set("teleportYPR", CModuleLua::teleportYPR)
    .set("damagePlayer", CModuleLua::damagePlayer)
    .set("healPlayer", CModuleLua::healPlayer)
    .set("launchCinematicDeath", CModuleLua::launchCinematicDeath)
    .set("setLeftHandLockCamera", CModuleLua::setLeftHandLockCamera)
    .set("setRightHandLockCamera", CModuleLua::setRightHandLockCamera)
    .set("fadeInToBlack", CModuleLua::fadeInToBlack)
    .set("fadeOutToBlack", CModuleLua::fadeOutToBlack)
    .set("fadeInToWhite", CModuleLua::fadeInToWhite)
    .set("fadeOutToWhite", CModuleLua::fadeOutToWhite)
    .set("setGlovesRandGlow", CModuleLua::setGlovesRandGlow)
    .set("blockPlayer", CModuleLua::blockPlayer)
	  .set("unlockPlayer", CModuleLua::unlockPlayer)
    .set("setRandomCamShake", CModuleLua::setRandomCamShake)
	  .set("stopPlayerAnimations", CModuleLua::stopPlayerAnimations)
    .set("setInputEnabled", CModuleLua::setInputEnabled)
	  .set("setPlayerVisible", CModuleLua::setPlayerVisible)

    // Enemies
    .set("setCinematicControl", CModuleLua::setCinematicControl)
	  .set("setEnemieViewPlayer", CModuleLua::setEnemieViewPlayer)
    .set("moveToPositionCinematic", CModuleLua::moveToPositionCinematic)
	  .set("setBerserkMode", CModuleLua::setBerserkMode)

    // Scenes
    .set("loadSceneNoBlock", &CModuleLua::loadSceneNoBlock)
    .set("unloadSceneNoBlock", &CModuleLua::unloadSceneNoBlock)
    .set("loadSceneBlock", &CModuleLua::loadSceneBlock)
    .set("unloadScene", &CModuleLua::unloadScene)
    .set("getJsonSize", &getJsonSize)
    .set("setCommonCtx", &setCommonCtx)
    .set("partialLoadEntities", &partialLoadEntities)
    .set("partialUnloadEntities", &partialUnloadEntities)
    .set("createNavMesh", &createNavMesh)
    .set("pausePhysx", &CModuleLua::pausePhysx)

    // Render
    .set("activateCheapMode", CModuleLua::activateCheapMode)
    .set("activateCompleteMode", CModuleLua::activateCompleteMode)
    .set("launchParticleAtTransform", CModuleLua::launchParticleAtTransform)

	 // Sounds
    .set("playSoundEvent", CModuleLua::playSoundEvent)
    .set("playSFX", CModuleLua::playSFX)
    .set("stopVoices", &CModuleLua::stopVoices)

    // Cinematics
    .set("launchCinematic", CModuleLua::launchCinematic)

	  // GUI
    .set("showUIMessage", CModuleLua::showUIMessage)
    .set("hideUIMessage", CModuleLua::hideUIMessage)
    .set("hideUIinGame", CModuleLua::hideUIinGame)
    .set("showUIinGame", CModuleLua::showUIinGame)
    .set("setUIScrollSpeed", CModuleLua::setUIScrollSpeed)
    .set("showUIMessage", CModuleLua::showUIMessage)
    .set("hideUIMessage", CModuleLua::hideUIMessage)
    .set("hideUIinGame", CModuleLua::hideUIinGame)
    .set("showUIinGame", CModuleLua::showUIinGame)
    .set("setUIScrollSpeed", CModuleLua::setUIScrollSpeed)
    .set("activateSubs", CModuleLua::activateSubs)
    .set("deactivateSubs", CModuleLua::deactivateSubs)
    .set("skipIntroDialogue", CModuleLua::skipIntroDialogue)
    .set("skipDialogue", CModuleLua::skipDialogue)
    .set("setSubsText", CModuleLua::setSubsText)
    .set("setSubsLine", CModuleLua::setSubsLine)
    .set("setSubsLanguage", CModuleLua::setSubsLanguage)
    .set("lerpUIScrollSpeed", CModuleLua::lerpUIScrollSpeed)

    .set("setInitImageUV", CModuleLua::setInitImageUV)
    .set("setImageAlpha", CModuleLua::setImageAlpha)
    .set("lerpImageAlpha", CModuleLua::lerpImageAlpha)
	  .set("lerpImageBurnAmount", CModuleLua::lerpImageBurnAmount)
	  .set("enabledTriggerTutorial", CModuleLua::enabledTriggerTutorial)
		.set("stateWidget", CModuleLua::stateWidget)


		
    // Events
    .set("setOpenDoor", CModuleLua::setOpenDoor)
    .set("setOpenDoorFinal", CModuleLua::setOpenDoorFinal)
    .set("loadScenesPersistance", CModuleLua::loadScenesPersistance)
    .set("loadStartEvent", CModuleLua::loadStartEvent)
    .set("loadFinishEvent", CModuleLua::loadFinishEvent)

    // Instantiate
    .set("instantiateTrigger", CModuleLua::instantiateTrigger)

    //Final
    .set("loadFinalMessage", CModuleLua::loadFinalMessage)
    .set("sendToMenu", CModuleLua::sendToMenu);


  // Misc classes
  SLB::Class< VEC3 >("VEC3", m)
    .constructor<float, float, float>()
    .comment("This is our wrapper of the VEC3 class")
    .property("x", &VEC3::x)
    .property("y", &VEC3::y)
    .property("z", &VEC3::z);

  SLB::Class < TEntityParseContext >("entityParseCtx", m)
    .comment("Wrapper for the entity parse context")
    .constructor();

  SLB::Class< json >("json", m)
    .comment("Wrapper for the json class")
    .constructor();

  SLB::Class< CHandle >("CHandle", m)
    .comment("Wrapper for the CHandle class")
    .constructor();
}

void CModuleLua::loadScriptsInFolder(const char* path)
{
  if (std::experimental::filesystem::exists(path) && std::experimental::filesystem::is_directory(path)) {

    std::experimental::filesystem::recursive_directory_iterator iter(path);
    std::experimental::filesystem::recursive_directory_iterator end;

    while (iter != end) {
      std::string fileName = iter->path().string();
      if (iter->path().extension().string() == ".lua" &&
        !std::experimental::filesystem::is_directory(iter->path())) {
        dbg("File : %s loaded\n", fileName.c_str());
        s->doFile(fileName);
      }
      std::error_code ec;
      iter.increment(ec);
      if (ec) {
        fatal("Error while accessing %s: %s\n", iter->path().string().c_str(), ec.message().c_str());
      }
    }
  }
  else {
    fatal("Exception %s while loading scripts at path %s\n", path);
  }
}

ConsoleResult CModuleLua::execScript(const std::string& script) {
  if (!EngineLua.isLuaLoaded())
    return ConsoleResult{ false, "" };

  std::string scriptLogged = script;
  std::string errMsg = "";
  bool success = false;

  try {
    EngineLua.doString(script);
    scriptLogged = scriptLogged + " - Success";
    success = true;
  }
  catch (std::exception e) {
    scriptLogged = scriptLogged + " - Failed";
    scriptLogged = scriptLogged + "\n" + e.what();
    errMsg = e.what();
  }

  dbg("Script %s\n", scriptLogged.c_str());
  EngineLua.log.push_back(scriptLogged);
  return ConsoleResult{ success, errMsg };
}

bool CModuleLua::execScriptDelayed(const std::string & script, float delay)
{
  EngineLua.delayedScripts.push_back(DelayedScript{ script, delay });
  return true;
}

bool CModuleLua::execSystemScriptDelayed(const std::string & script, float delay)
{
  EngineLua.delayedSystemScripts.push_back(DelayedScript{ script, delay });
  return true;
}

/******* Aux Functions *******/
// Module getters
CModuleGameConsole* CModuleLua::getConsole()
{
  return EngineConsole.getConsole();
}

CModuleGameplay* CModuleLua::getGameplay()
{
  CModuleGameplay *m = (CModuleGameplay*)Engine.getModules().getModule("gameplay");
  return m;
}

CEnemyManager* CModuleLua::getEnemyManager()
{
  CEnemyManager *m = (CEnemyManager*)Engine.getModules().getModule("enemy_manager");
  return m;
}

// Utils
void CModuleLua::write(std::string n)
{
  dbg(n.c_str());
}

std::string CModuleLua::VEC3tostring(VEC3 v)
{
  return stringify(v);
}

CHandle CModuleLua::getPlayer()
{
  return getEntityByName("Player");
}

VEC3 CModuleLua::getEntityPos(std::string n)
{
  CHandle c = getEntityByName(n);
	if (!c.isValid()) return VEC3(0, 0, 0);
  TCompTransform *t = ((CEntity*)c)->get <TCompTransform>();
  return t->getPosition();
}

void CModuleLua::setEntityPos(std::string n, VEC3 pos)
{
  CHandle c = getEntityByName(n);
	if (!c.isValid()) return;
  TCompTransform *t = ((CEntity*)c)->get <TCompTransform>();
  t->setPosition(pos);
}

VEC3 CModuleLua::getEntityAngles(std::string n)
{
  CHandle c = getEntityByName(n);
	if (!c.isValid()) return VEC3(0, 0, 0);
  TCompTransform *t = ((CEntity*)c)->get <TCompTransform>();
  float y, p, r;
  t->getAngles(&y, &p, &r);
  return VEC3(y, p, r);
}

void CModuleLua::setEntityAngles(std::string n, VEC3 angles)
{
  CHandle c = getEntityByName(n);
	if (!c.isValid()) return;
  TCompTransform *t = ((CEntity*)c)->get <TCompTransform>();
  t->setAngles(angles.x, angles.y, angles.z);
}

VEC3 CModuleLua::getEntityFront(std::string n)
{
  CHandle c = getEntityByName(n);
	if (!c.isValid()) return {};
  TCompTransform *t = ((CEntity*)c)->get <TCompTransform>();
  return t->getFront();
}

VEC3 CModuleLua::getEntityLeft(std::string n)
{
  CHandle c = getEntityByName(n);
	if (!c.isValid()) return VEC3(0, 0, 0);
  TCompTransform *t = ((CEntity*)c)->get <TCompTransform>();
  return t->getLeft();
}

VEC3 CModuleLua::getEntityUp(std::string n)
{
  CHandle c = getEntityByName(n);
	if (!c.isValid()) return VEC3(0, 0, 0);
  TCompTransform *t = ((CEntity*)c)->get <TCompTransform>();
  return t->getUp();
}

void CModuleLua::destroyEntity(std::string n)
{
	CHandle c = getEntityByName(n);
	if (c.isValid()) c.destroy();
}

// Gamestates
void CModuleLua::togglePauseGame()
{
  //EngineLua.isPaused = !EngineLua.isPaused;
  //getGameplay()->setPause(EngineLua.isPaused);
}

void CModuleLua::changeGameState(std::string n) {
	Engine.getModules().changeToGamestate(n);
}

void CModuleLua::changeGameStateFinal(std::string n) {
	EnginePersistence.gameState = n;
}

void CModuleLua::resetGame() {
	EnginePersistence.resetGame();
}

void CModuleLua::setMainMenuCamera(bool b)
{
  if (b)
  {
    std::string n = "data/prefabs/cinematics/menu_camera.json";
    CEntity *cin = EngineBasics.Instantiate(n, VEC3(0, 0, 0), 0, 0, 0);
    TCompCameraTransition *camTrans = cin->get< TCompCameraTransition >();
    camTrans->startInMenu();
  }
  else
  {
    CHandle cam = getEntityByName("menu_camera_transitions");
    TCompCameraTransition *camTrans = ((CEntity*)cam)->get <TCompCameraTransition>();
    camTrans->end();
  }
}

// Enemies
void CModuleLua::togglePauseEnemies() {}
void CModuleLua::ragdollEnemies() {}
void CModuleLua::resetEnemies() {}
void CModuleLua::spawn() {}
void CModuleLua::setCinematicControl(std::string n, bool c)
{
  CEntity *enemy = getEntityByName(n);
  if (!enemy) return;
  bt_gnome *btg = enemy->get< bt_gnome >();
  bt_mage *btm = enemy->get< bt_mage >();
  if (btg != nullptr)
    btg->setGnomeCinematicController(c);
  else if (btm != nullptr)
    btm->setMageCinematicController(c);

}

void CModuleLua::setEnemieViewPlayer(std::string n, bool c)
{
	CEntity *enemy = getEntityByName(n);
	if (!enemy) return;
	bt_gnome *btg = enemy->get< bt_gnome >();
	bt_mage *btm = enemy->get< bt_mage >();
	if (btg != nullptr)
		btg->setViewPlayer(c);
	else if (btm != nullptr)
		btm->setViewPlayer(c);

}

void CModuleLua::moveToPositionCinematic(std::string n, VEC3 pos)
{
  CEntity *enemy = getEntityByName(n);
  if (!enemy) return;
  bt_gnome *btg = enemy->get< bt_gnome >();
  bt_mage *btm = enemy->get< bt_mage >();
  if (btg != nullptr)
    btg->moveGnomeCinematic(pos);
  else if (btm != nullptr)
    btm->moveMageCinematic(pos);
}

void CModuleLua::setBerserkMode(std::string n, bool c)
{
	CEntity *enemy = getEntityByName(n);
	if (!enemy) return;
	bt_gnome *btg = enemy->get< bt_gnome >();
	bt_mage *btm = enemy->get< bt_mage >();
	if (btg != nullptr)
		btg->setBerserkMode(c);
	else if (btm != nullptr)
		btm->setBerserkMode(c);
}

// Player
void CModuleLua::toggleInput()
{
  CHandle p = getPlayer();
  TCompPlayerMagic *plMagic = ((CEntity*)p)->get <TCompPlayerMagic>();
  plMagic->toggleEnabled();
  TCompCharController *plController = ((CEntity*)p)->get <TCompCharController>();
  plController->toggleEnabled();
  CHandle c = getEntityByName("Camera");
  TCompCameraFP *plCam = ((CEntity*)c)->get <TCompCameraFP>();
  plCam->toggleEnabled();
  CHandle cam = getEntityByName("CameraShaker");
  TCompCamMove *movCam = ((CEntity*)cam)->get <TCompCamMove>();
  movCam->toggleEnabled();
}

void CModuleLua::setInputEnabled(bool b)
{
  CHandle p = getPlayer();
  TCompPlayerMagic *plMagic = ((CEntity*)p)->get <TCompPlayerMagic>();
  plMagic->setEnabled(b);
  TCompCharController *plController = ((CEntity*)p)->get <TCompCharController>();
  plController->setEnabled(b);
  CHandle c = getEntityByName("Camera");
  TCompCameraFP *plCam = ((CEntity*)c)->get <TCompCameraFP>();
  plCam->setEnabled(b);
  CHandle cam = getEntityByName("CameraShaker");
  TCompCamMove *movCam = ((CEntity*)cam)->get <TCompCamMove>();
  movCam->setEnabled(b);
}

void CModuleLua::setPlayerVisible(bool b)
{
	CEntity* h_right_hand = getEntityByName("RightHand");
	CEntity* h_left_hand = getEntityByName("LeftHand");

	if (!h_right_hand || !h_left_hand) return;

	TCompRender* c_render_right = h_right_hand->get<TCompRender>();
	TCompRender* c_render_left = h_left_hand->get<TCompRender>();
	c_render_right->setVisible(b);
	c_render_left->setVisible(b);
}

void CModuleLua::toggleInfMagic() 
{
  CHandle p = getPlayer();
  TCompPlayerStats *plStats = ((CEntity*)p)->get <TCompPlayerStats>();
  plStats->setInfMagic(!plStats->getInfMagic());
}
void CModuleLua::toggleImmortal() 
{
  CHandle p = getPlayer();
  TCompPlayerStats *plStats = ((CEntity*)p)->get <TCompPlayerStats>();
  plStats->setImmortal(!plStats->getImmortal());
}

void CModuleLua::setImmortal(bool b)
{
  CHandle p = getPlayer();
  TCompPlayerStats *plStats = ((CEntity*)p)->get <TCompPlayerStats>();
  plStats->setImmortal(b);
}

void CModuleLua::setPlayerSpeed(float s) {}
void CModuleLua::setPlayerGravity(float g) {}
void CModuleLua::togglePlayerInvisible() {}
void CModuleLua::activateMagic() 
{
  CHandle p = getPlayer();
  if(p.isValid()){
	 TCompPlayerMagic *plMagic = ((CEntity*)p)->get <TCompPlayerMagic>();
	plMagic->activateMagic();
  }
}
void CModuleLua::deactivateMagic() 
{
  CHandle p = getPlayer();
  TCompPlayerMagic *plMagic = ((CEntity*)p)->get <TCompPlayerMagic>();
  plMagic->deactivateMagic();
}
void CModuleLua::teleport(VEC3 &pos, VEC3 &front) 
{
  CHandle p = getPlayer();
  TCompCharController *charCon = ((CEntity*)p)->get <TCompCharController>();
  charCon->teleport(pos, front);
}

void CModuleLua::teleportYPR(VEC3 &pos, float yaw, float pitch, float roll)
{
	CEntity* player = getEntityByName("Player");
	TCompCharController* charcontroller = player->get<TCompCharController>();
	charcontroller->SetFootPos(pos);

	CEntity* camera = getEntityByName("Camera");
	TCompTransform* cameraFP = camera->get<TCompTransform>();
	cameraFP->setAngles(yaw, pitch, roll);
}

void CModuleLua::damagePlayer(float dmg)
{
  CHandle p = getPlayer();
	TCompPlayerStats* stats = ((CEntity*)p)->get <TCompPlayerStats>();
	EngineAudio.playEvent("event:/PlayerVoice/Hurt", 1);
	if (stats->getHealth() - dmg <= 0) return;
  TMsgDamage msg;
  msg.damage = dmg;
  msg.vel = VEC3(0.0f, 0.0f, 0.0f);
  p.sendMsg(msg);
}

void CModuleLua::healPlayer(float hp)
{
  CHandle p = getPlayer();
  TMsgHeal msg;
  msg.hp = hp;
  p.sendMsg(msg);
}

void CModuleLua::launchCinematicDeath()
{
  std::string n = "data/prefabs/cinematics/death.json";
  CEntity *camera = getEntityByName("Camera");
  TCompTransform *cTrans = camera->get<TCompTransform>();
  CEntity *player = getEntityByName("Player");
  TCompTransform *plTrans = player->get<TCompTransform>();
  float y, p, r;
  cTrans->getAngles(&y, &p, &r);
  CEntity *cin = EngineBasics.Instantiate(n, cTrans->getPosition(), y, p, r);
  TCompCameraTransition *camTrans = cin->get< TCompCameraTransition >();

  // Put everything in respect to the player
  std::vector< CamTransition > transitions = camTrans->getTransitions();
  for (int i = 0; i < transitions.size(); ++i)
  {
    CEntity *cam = getEntityByName(transitions[i].camName);
    TCompTransform *t = cam->get<TCompTransform>();
    float x = t->getPosition().x;
    float y = t->getPosition().y;
    float z = t->getPosition().z;
    VEC3 newPos = cTrans->getPosition() + plTrans->getLeft() * x + plTrans->getUp() * y + plTrans->getFront() * z;
    t->setPosition(newPos);
    t->setRotation(t->getRotation() * plTrans->getRotation());
  }
  camTrans->start();
}

VEC3 CModuleLua::getCameraPosition()
{
  CHandle c = getEntityByName("CameraShaker");
  TCompCamera *t = ((CEntity*)c)->get <TCompCamera>();
  return t->getPosition();
}

VEC3 CModuleLua::getCameraFront()
{
  CHandle c = getEntityByName("CameraShaker");
  TCompCamera *t = ((CEntity*)c)->get <TCompCamera>();
  return t->getFront();
}

void CModuleLua::setLeftHandLockCamera(bool b)
{
  CHandle c = getEntityByName("LeftHand");
  TCompHandSet *hs = ((CEntity*)c)->get <TCompHandSet>();
  hs->setLockCamera(b);
}

void CModuleLua::setRightHandLockCamera(bool b)
{
  CHandle c = getEntityByName("RightHand");
  TCompHandSet *hs = ((CEntity*)c)->get <TCompHandSet>();
  hs->setLockCamera(b);
}

void CModuleLua::fadeInToBlack(float time)
{
  CEntity *camera = (CEntity *)getEntityByName("CameraShaker");
  TCompOverlay *render_overlay = camera->get<TCompOverlay>(); //Get Overlays component
  render_overlay->black = 0.0f; //Set Black to 0 (should be 0 anyways)
  EngineBasics.LerpElement(&render_overlay->black, 1.0f, time);
}

void CModuleLua::fadeOutToBlack(float time)
{
  CEntity *camera = (CEntity *)getEntityByName("CameraShaker");
  TCompOverlay *render_overlay = camera->get<TCompOverlay>(); //Get Overlays component
  render_overlay->black = 1.0f; //Set Black to 1
  EngineBasics.LerpElement(&render_overlay->black, 0.0f, time);
}

void CModuleLua::fadeInToWhite(float time)
{
  CEntity *camera = (CEntity *)getEntityByName("CameraShaker");
  TCompOverlay *render_overlay = camera->get<TCompOverlay>(); //Get Overlays component
  render_overlay->white = 0.0f; //Set White to 0 (should be 0 anyways)
  EngineBasics.LerpElement(&render_overlay->white, 1.0f, time);
}

void CModuleLua::fadeOutToWhite(float time)
{
  CEntity *camera = (CEntity *)getEntityByName("CameraShaker");
  TCompOverlay *render_overlay = camera->get<TCompOverlay>(); //Get Overlays component
  render_overlay->white = 1.0f; //Set White to 1
  EngineBasics.LerpElement(&render_overlay->white, 0.0f, time);
}

void CModuleLua::launchPlayerDeath()
{
  stateWidget("ui_in_game", false);
  CEntity *player = getEntityByName("Player");
  TCompCharController *c = player->get<TCompCharController>();
  if (c->IsGrounded())
  {
    launchEvent(7);
    CEntity *ui = getEntityByName("UI");
    TCompUI *cui = ui->get<TCompUI>();
    cui->setTimer(6);
  }
  else
  {
    CEntity *ui = getEntityByName("UI");
    TCompUI *cui = ui->get<TCompUI>();
    cui->setTimer(3);
    CEntity *camera = (CEntity *)getEntityByName("CameraShaker");
    TCompOverlay *render_overlay = camera->get<TCompOverlay>();
    EngineBasics.LerpElement(&render_overlay->black, 1.0f, 0.25f);
  }   
}

void CModuleLua::launchPlayerDeathEnd()
{
  launchEvent(8);
}

void CModuleLua::setGlovesRandGlow(bool b)
{
  CEntity *player = getEntityByName("Player");
  TCompGloveGlow *gl = player->get<TCompGloveGlow>();
  gl->dialogue = b;
}

void CModuleLua::blockPlayer()
{
  Engine.getSettings().setInGame(false);
  EnginePersistence.blockPlayer(true);
}

void CModuleLua::unlockPlayer() {
	Engine.getSettings().setInGame(true);
	EnginePersistence.blockPlayer(false);
}

void CModuleLua::setRandomCamShake(float intensity, float duration, float roughness)
{
  CHandle c = getEntityByName("CameraShaker");
  TCompCamShake *shaker = ((CEntity*)c)->get <TCompCamShake>();
  shaker->startShake(intensity, duration, roughness);
}

void CModuleLua::stopPlayerAnimations() {
	CEntity* e_left_hand = getEntityByName("LeftHand");
	TCompFSM* cFsm_left = e_left_hand->get<TCompFSM>();

	CEntity* e_right_hand = getEntityByName("RightHand");
	TCompFSM* cFsm_right = e_right_hand->get<TCompFSM>();

	cFsm_right->changeVariable("running", 0,true);
	cFsm_right->changeVariable("speed", 0.0f, false);
	cFsm_right->changeVariable("floating", 0, true);
	cFsm_right->changeVariable("pull", 0, true);
	cFsm_right->changeVariable("push", 0, true);

	cFsm_left->changeVariable("running", 0, true);
	cFsm_left->changeVariable("speed", 0.0f, false);
	cFsm_left->changeVariable("floating", 0, true);
	cFsm_left->changeVariable("pull", 0, true);
	cFsm_left->changeVariable("push", 0, true);

	CEntity* camera = getEntityByName("CameraShaker");
	TCompCamMove* cam_move = camera->get< TCompCamMove >();
	cam_move->SetState(cam_move->idle);
}

// Scenes

void CModuleLua::loadSceneNoBlock(const std::string& filename)
{
  std::string c = "loadSceneNoBlock(\"" + filename + "\")";
  EngineLua.execScript(c);
}

void CModuleLua::unloadSceneNoBlock(const std::string& filename)
{
  std::string c = "unloadSceneNoBlock(\"" + filename + "\")";
  EngineLua.execScript(c);
}

void CModuleLua::loadSceneBlock(const std::string& filename)
{
  EngineSceneLoader.loadSceneBlock(filename);
  EngineNavMesh.buildNavMesh();
}

void CModuleLua::unloadScene(const std::string& filename)
{
  EngineSceneLoader.unloadScene(filename);
}

void CModuleLua::createNavMesh()
{
	EngineNavMesh.buildNavMesh();
}

void CModuleLua::pausePhysx(bool b)
{
  EnginePhysics.isPaused = b;

  if (EnginePhysics.isPaused) Time.scale_factor = 0.0f;
  else Time.scale_factor = 1.0f;
}

// Render
void CModuleLua::activateCompleteMode() 
{
}

void CModuleLua::activateCheapMode() 
{
}

void CModuleLua::activateNormalMode() {}

void CModuleLua::launchParticleAtTransform(std::string p, VEC3 pos, VEC3 angles)
{
  CTransform trans(pos, QUAT::CreateFromYawPitchRoll(angles.x, angles.y, angles.z));
  EngineParticles.CreateLogicSeparatedSystemParticles(p.c_str(), trans);
}

// Sounds
void CModuleLua::playSoundEvent(std::string c)
{
	EngineAudio.playEvent(c, 3);
}

void CModuleLua::playSFX(std::string c, std::string d)
{
	CEntity* ent = getEntityByName(d);
	TCompTransform* trans = ent->get<TCompTransform>();
	EngineAudio.playEvent3D(c, trans, 0);
}

void CModuleLua::playDialogue(int e)
{
	std::string c = "PlayDialogue(" + std::to_string(e) + ")";
	execScript(c);
}

void CModuleLua::stopVoices()
{
	EngineAudio.stopVoices();
}

// Cinematics

void CModuleLua::launchCinematic(std::string c) 
{
  std::string n = "data/prefabs/cinematics/" + c + ".json";
  CEntity *cin = EngineBasics.Instantiate(n, VEC3(0, 0, 0), 0, 0, 0);
  TCompCameraTransition *camTrans = cin->get< TCompCameraTransition >();
  camTrans->start();
}

// GUI
void CModuleLua::showUIMessage(std::string m)
{
	UI::CImage* imageTutorial = dynamic_cast<UI::CImage*>(Engine.getUI().getWidgetByAlias(m));
	if (imageTutorial) {
		imageTutorial->getParams()->visible = true;
		imageTutorial->setImageAlpha(0.8);
	}
}

void CModuleLua::hideUIMessage(std::string m)
{
	UI::CImage* imageTutorial = dynamic_cast<UI::CImage*>(Engine.getUI().getWidgetByAlias(m));
	if (imageTutorial) {
		imageTutorial->getParams()->visible = false;
		imageTutorial->setImageAlpha(0);
	}
}

void CModuleLua::hideUIinGame(std::string c) {
	UI::CWidget* uiInGame = dynamic_cast<UI::CWidget*>(Engine.getUI().getWidgetByAlias("rootUIInGame"));
	uiInGame->getParams()->visible = false;
}

void CModuleLua::showUIinGame(std::string c) {
	UI::CWidget* uiInGame = dynamic_cast<UI::CWidget*>(Engine.getUI().getWidgetByAlias("rootUIInGame"));
	uiInGame->getParams()->visible = true;
}

void CModuleLua::setUIVisible(bool b)
{
  //Show UI elements
  UI::CWidget* uiInGame = dynamic_cast<UI::CWidget*>(Engine.getUI().getWidgetByAlias("Cursor"));
  uiInGame->getParams()->visible = b;

  uiInGame = dynamic_cast<UI::CWidget*>(Engine.getUI().getWidgetByAlias("LifeAndPower"));
  uiInGame->getParams()->visible = b;
}

void CModuleLua::setUIScrollSpeed(std::string m, float speed_x, float speed_y)
{
	UI::CWidget* ui_item = Engine.getUI().getWidgetByAlias(m);
	if (ui_item) {
		std::vector<UI::CEffect*> effects = ui_item->getEffects();
		if (effects.size() > 0) {
			for (UI::CEffect* eff : effects) {
				UI::CFXAnimateUV* UV = (UI::CFXAnimateUV*)eff;
				UV->setSpeed(speed_x, speed_y);
			}
		}
	}
}

void CModuleLua::activateSubs()
{
  Engine.getUI().activateWidget("subtitles");
}

void CModuleLua::deactivateSubs()
{
  Engine.getUI().deactivateWidget("subtitles");
}

void CModuleLua::skipIntroDialogue()
{
  execScript("skipIntroDialogue()");
}

void CModuleLua::skipDialogue()
{
  execScript("skipDialogue()");
}

void CModuleLua::setSubsText(std::string t)
{
  UI::CText *sub = dynamic_cast<UI::CText*>(Engine.getUI().getWidgetByAlias("sub"));
  sub->setText(t);
}

void CModuleLua::setSubsLine(int sub_line)
{
  UI::CText *sub = dynamic_cast<UI::CText*>(Engine.getUI().getWidgetByAlias("sub"));
  sub->setText(sub_line);
  changeSubBackgroundSize();
}

void CModuleLua::setSubsLanguage(int l)
{
  UI::Language lang;
  if (l == 0) lang = UI::Language::English;
  else lang = UI::Language::Spanish;
  UI::CText *sub = dynamic_cast<UI::CText*>(Engine.getUI().getWidgetByAlias("sub"));
  sub->setLanguage(lang);
  changeSubBackgroundSize();
}

void CModuleLua::changeSubBackgroundSize()
{
  UI::CText *sub = dynamic_cast<UI::CText*>(Engine.getUI().getWidgetByAlias("sub"));
  UI::TTextParams *textParams = sub->getTextParams();
  int nlines = 0;
  for (int i = 0; i < textParams->text.size(); ++i)
    if (textParams->text[i] == '\n') ++nlines;
  UI::CImage *background = dynamic_cast<UI::CImage*>(Engine.getUI().getWidgetByAlias("subBkg"));
  UI::TImageParams *p = background->getImageParams();
  p->size = VEC2(p->size.x, ((nlines + 1) * textParams->size.y) + 15);
}

void CModuleLua::lerpUIScrollSpeed(std::string m, float speed_x, float speed_y, float t)
{
	UI::CWidget* ui_item = Engine.getUI().getWidgetByAlias(m);
	if (ui_item) {
		std::vector<UI::CEffect*> effects = ui_item->getEffects();
		if (effects.size() > 0) {
			for (UI::CEffect* eff : effects) {
				UI::CFXAnimateUV* UV = (UI::CFXAnimateUV*)eff;
				UV->LerpSpeed(speed_x, speed_y, t);
			}
		}
	}
}

void CModuleLua::setInitImageUV(std::string m)
{
	UI::CImage* ui_item = dynamic_cast<UI::CImage*>(Engine.getUI().getWidgetByAlias(m));
	if (ui_item) {
		ui_item->setInitUv();
	}
}

void CModuleLua::setImageAlpha(std::string m, float desiredAlpha) {
	UI::CWidget* ui_item = Engine.getUI().getWidgetByAlias(m);
	if (ui_item) {
		ui_item->setImageAlpha(desiredAlpha);
	}
}


void CModuleLua::lerpImageAlpha(std::string m, float desiredAlpha, float t) {
	UI::CWidget* ui_item = Engine.getUI().getWidgetByAlias(m);
	if (ui_item) {
		ui_item->lerpImageAlpha(desiredAlpha, t);
	}
}

void CModuleLua::lerpImageBurnAmount(std::string m, float desiredBurnAmout, float t) {
	UI::CImage* ui_item = dynamic_cast<UI::CImage*>(Engine.getUI().getWidgetByAlias(m));
	if (ui_item) {
		ui_item->lerpImageBurnAmount(desiredBurnAmout, t);
	}
}

void CModuleLua::enabledTriggerTutorial(std::string m, bool isEnabled) {
	CEntity* e = getEntityByName(m);
	if (!e) return;
	TCompTutorialTrigger* c_tut_trigger = e->get<TCompTutorialTrigger>();
	if (!c_tut_trigger) return;

	c_tut_trigger->setEnabled(isEnabled);
}

void CModuleLua::stateWidget(std::string m, bool enable) {
	if(enable) Engine.getUI().activateWidget(m);
	else Engine.getUI().deactivateWidget(m);
}

// Converters
CEntity* CModuleLua::toEntity(CHandle h) 
{
  return CHandle();
}
TCompTransform* CModuleLua::toTransform(CHandle h) 
{
  return CHandle();
}

// Events
void CModuleLua::launchEvent(int e)
{
  std::string c = "launchEvent(" + std::to_string(e) + ")";
  execScript(c);
}

void CModuleLua::setOpenDoor(std::string n, bool o)
{
  CEntity *d = getEntityByName(n);
  if (d == nullptr) return;
  TCompDoorBabyPuzzle *bp = d->get< TCompDoorBabyPuzzle >();
  TCompDoorEnemies *de = d->get< TCompDoorEnemies >();
  TCompDoorPlayerGoto *dg = d->get< TCompDoorPlayerGoto >();
  //TCompDoorTimed *dt = d->get< TCompDoorTimed >();
  TCompDoorController *dc = d->get< TCompDoorController >();
  TCompVerticalDoor *vd = d->get< TCompVerticalDoor >();
  TCompHorizontalDoor *hd = d->get< TCompHorizontalDoor >();
  if (bp != nullptr)
    bp->setOpenDoor(o);
  else if (de != nullptr)
    de->setOpenDoor(o);
  else if (dg != nullptr)
    dg->setOpenDoor(o);
  //else if (dt != nullptr)
    //dt->setOpenDoor(o);
  else if (dc != nullptr)
    dc->setOpenDoor(o);
  
  if (vd != nullptr)
	  vd->SetDoorState(o);
  else if (dc != nullptr)
	  hd->SetDoorState(o);
}

void CModuleLua::setOpenDoorFinal(bool o)
{
  CEntity *d = getEntityByName("FinalDoor");
  if (d == nullptr) return;
  TCompDoorEnemies *de = d->get< TCompDoorEnemies >();
  de->setOpenDoor(o);
}

//Instantiate
void CModuleLua::instantiateTrigger(std::string name) {
	TEntityParseContext ctx;
	instantiateAtCurrentScene(name, ctx);
}


//Hardcore
void CModuleLua::loadFinalMessage() {
	UI::CImage* imageTutorial = dynamic_cast<UI::CImage*>(Engine.getUI().getWidgetByAlias("tutorialMessage8"));
	if (imageTutorial)
		imageTutorial->setImageAlpha(1.0);
}

//PlaceHolder Final Milestone3
void CModuleLua::sendToMenu() {
	UI::CImage* imageTutorial = dynamic_cast<UI::CImage*>(Engine.getUI().getWidgetByAlias("tutorialMessage8"));
	if (imageTutorial)
		imageTutorial->setImageAlpha(0.0);

	EnginePersistence.gameState = "";
	EnginePersistence.setCheckpointId(0);
	EnginePersistence.sceneToLoad = "Initial";
	EnginePersistence.load();
	EnginePersistence.loadPostProcess();

	Engine.getModules().changeToGamestate("gs_reset");


	//CApplication::get().ExitGame();
}

void CModuleLua::loadScenesPersistance(std::string scene)
{
  EnginePersistence.setScene(scene);
}

void CModuleLua::loadStartEvent(std::string scene)
{
  std::string script = "loadStartEvent(\"" + scene + "\")";
  execScript(script);
}

void CModuleLua::loadFinishEvent()
{
  std::string script = "loadFinishEvent()";
  execScript(script);
}
/*****************************/
