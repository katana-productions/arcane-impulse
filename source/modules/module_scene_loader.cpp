#include "mcv_platform.h"
#include "module_scene_loader.h"
#include "entity/entity_parser.h"
#include "profiling/profiling.h"
#include "engine.h"

bool CModuleSceneLoader::isSceneLoaded(const std::string& s)
{
  if (all_loaded_scenes.find(s) != all_loaded_scenes.end()) return true;
  return false;
}

// This is called by the parser to add the scene to the map when it's finished
void CModuleSceneLoader::addSceneToMap(const std::string& s, CHandle h_scene)
{
  all_loaded_scenes[s] = h_scene;
}

// This adds other scenes as a dependency of a scene. When one scene is deleted, so are all it's dependencies
void CModuleSceneLoader::addSceneDependency(const std::string& s, CHandle h_dep)
{
  if (scene_dependency[s].empty())
    scene_dependency[s] = std::vector<CHandle>();
  scene_dependency[s].push_back(h_dep);
}

// Note: this loads the whole file in the same frame, game might freeze because of this
// Returns if the scene loaded properly
bool CModuleSceneLoader::loadSceneBlock(const std::string& s)
{
  /* Profiling
  
  PROFILE_SET_NFRAMES(2);
  PROFILE_FRAME_BEGINS();
  PROFILE_FUNCTION("parseScene");*/

  TEntityParseContext ctx;
  return parseScene(s, ctx);
}

/* Launches lua coroutine that repeats this process to load the scene:
    - Loads some entities one frame.
    - Skip some frames doing nothing.

    This way the game does not freeze. Use this for really big scenes
*/
void CModuleSceneLoader::loadSceneNoBlock(const std::string& s)
{
  //Profiling

  PROFILE_SET_NFRAMES(2);
  PROFILE_FRAME_BEGINS();
  PROFILE_FUNCTION("parseScene");

  const std::string c = s;
  EngineLua.loadSceneNoBlock(c);
}

void CModuleSceneLoader::unloadSceneNoBlock(const std::string& s) {
	const std::string c = s;
	EngineLua.unloadSceneNoBlock(c);

	std::vector<CHandle> v = scene_dependency[s];
	for (auto h : v)
		h.destroy();

	all_loaded_scenes.erase(s);
	scene_dependency.erase(s);

	EngineEnemyManager.ClearSBB();
}

bool CModuleSceneLoader::unloadScene(const std::string& s)
{
  auto it = all_loaded_scenes.find(s);
  if (it == all_loaded_scenes.end()) return false;
  CHandle h_scene = it->second;
  h_scene.destroy();

  // We have to destroy it's dependencies too
  std::vector<CHandle> v = scene_dependency[s];
  for (auto h : v)
    h.destroy();

  all_loaded_scenes.erase(s);
  scene_dependency.erase(s);
  EngineEnemyManager.ClearSBB();
  return true;
}

bool CModuleSceneLoader::start()
{
  all_loaded_scenes = std::map<std::string, CHandle>();
  return true;
}

void CModuleSceneLoader::stop()
{
  // Destroy all scenes
  for (auto it : all_loaded_scenes)
  {
    CHandle h_scene = it.second;
    h_scene.destroy();
  }
  all_loaded_scenes.clear();
}

void CModuleSceneLoader::renderInMenu()
{
  ImGui::Text("Number of loaded scenes: %d", all_loaded_scenes.size());
}
