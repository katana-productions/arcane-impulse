#pragma once

#include "modules/module.h"
#include "entity/entity.h"
#include <queue>

// Examples
/*current_scenes[s] = CHandle();std::string& s;
  std::function<void()> f = [&]() { asdf(s, current_scenes[s]); };
  EngineWorker.addJob(f, false);*/

struct Job
{
  bool isDone;
  std::function<void()> exec;
};

class CModuleBkgWorker : public IModule
{
private:
  std::thread t;
  std::queue<Job> main_worker_jobs;
  std::queue<Job> parallel_worker_jobs;
  std::queue<Job> pending_jobs;

public:

  CModuleBkgWorker(const std::string& aname) : IModule(aname) { }
  void addJob(std::function<void()>& f, bool thread_safe);
  bool start() override;
  void stop() override;
  void update(float delta) override;
  void renderInMenu() override;
};
