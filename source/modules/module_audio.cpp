#include "mcv_platform.h"
#include "audio/inc/fmod_studio.hpp"
#include "audio/inc/fmod.hpp"
#include "engine.h"
#include "handle/handle.h"
#include "modules/module_audio.h"
#include "input/input.h"
#include <algorithm>
#include <iostream>
#include <vector>


CModuleAudio::CModuleAudio(const std::string& name)
	: IModule(name)
{}

bool CModuleAudio::start() {

	FMOD_RESULT res;

	// arranco FMOD
	res = Studio::System::create(&system);
	if (res != FMOD_OK) {
		dbg("Failed to create FMOD System: %d", res);
		return false;
	}
	assert(system);
	res = system->getCoreSystem(&lowLevelSystem);
	res = system->initialize(1024, FMOD_STUDIO_INIT_NORMAL, FMOD_INIT_NORMAL, extraDriverData);

	load();

	json cfg = loadJson("data/config.json");
	const json& cfg_settings = cfg["settings"];
	musicVol = cfg_settings.value("music", 1);
	voiceVol = cfg_settings.value("voice", 1);
	sfxVol = cfg_settings.value("sfx", 1);

	setBusVolume("bus:/Music", musicVol);
	setBusVolume("bus:/Voice", voiceVol);
	setBusVolume("bus:/SFX", sfxVol);

	return true;
}

void CModuleAudio::stop() {
	for (std::map<std::string, Studio::Bank*>::iterator it = mBanks.begin(); it != mBanks.end(); ++it) {
		it->second->unload();
	}

	mBanks.clear();
	mEvents.clear();
	mEventInstances.clear();
	mBuses.clear();

	environment.clear();
	enemies.clear();
	objects.clear();
	voice.clear();

	music.clear();
	menu.clear();
	player.clear();
	voicesPandS.clear();

	system->release();
}

void CModuleAudio::update(float delta) {
		std::vector<int> done;
		std::vector<Studio::EventInstance*> doneEvent;
		for (auto& iter : mEventInstances) {
			Studio::EventInstance* eInstance = iter.second;

			FMOD_STUDIO_PLAYBACK_STATE eState;
			eInstance->getPlaybackState(&eState);

			if (eState == FMOD_STUDIO_PLAYBACK_STOPPED) {
				eInstance->release();
				done.emplace_back(iter.first);
				doneEvent.emplace_back(iter.second);
			}
		}

		for (auto id : done) {
			mEventInstances.erase(id);
		}

		for (auto event : doneEvent) {
			cleanDoneSounds(event);
		}

		playerPos = setListener();

		controlMusic();
		busControlVoice();

		if (playerPos != nullptr) {
			update3D();
		}
		system->update();
}

TCompTransform* CModuleAudio::setListener() {
	CEntity* player = getEntityByName("Player");
	if (!player) return nullptr;
	TCompTransform* trans = player->get<TCompTransform>();
	VEC3 front = trans->getFront();
	front.Normalize();
	VEC3 up = trans->getUp();
	up.Normalize();

	FMOD_3D_ATTRIBUTES listener;
	listener.position = Vec3toFmodVect(trans->getPosition());
	listener.forward = Vec3toFmodVect(front);
	listener.up = Vec3toFmodVect(up);
	listener.velocity = { 0, 0, 0 };

	system->setListenerAttributes(0, &listener);

	return trans;
}

CSoundEvent CModuleAudio::playEvent(const std::string& name, int type, Studio::EventInstance* eventInstance) {
	int retID = 0;
	auto iter = mEvents.find(name);

	if (eventInstance == nullptr) {
		if (iter != mEvents.end()) {
			iter->second->createInstance(&eventInstance);

			if (eventInstance) {
				eventInstance->start();

				nextID++;
				retID = nextID;
				mEventInstances.emplace(retID, eventInstance);

				switch (type)
				{
					//Music
				case 0:
					music.emplace_back(eventInstance);
					break;

					//Player
				case 1:
					player.emplace_back(eventInstance);
					break;

					//Menu
				case 2:
					menu.emplace_back(eventInstance);
					break;

					//Voice (Player / Spirit)
				case 3:
					for (auto event : voicesPandS) {
						event->stop(FMOD_STUDIO_STOP_IMMEDIATE);
					}
					voicesPandS.emplace_back(eventInstance);

					//Voice Actions Player
				case 4:
					if (voiceIsPlaying()) {
						eventInstance->stop(FMOD_STUDIO_STOP_IMMEDIATE);
						break;
					}
					else {
							player.emplace_back(eventInstance);
					}
					break;

				default:
					break;
				}
			}
		}
	}
	else {
		if (eventInstance) {
			eventInstance->start();

			nextID++;
			retID = nextID;
			mEventInstances.emplace(retID, eventInstance);

			switch (type)
			{
				//Music
			case 0:
				music.emplace_back(eventInstance);
				break;

				//Player
			case 1:
				player.emplace_back(eventInstance);
				break;

				//Menu
			case 2:
				menu.emplace_back(eventInstance);
				break;

				//Voice (Player / Spirit)
			case 3:
				for (auto event : voicesPandS) {
					event->stop(FMOD_STUDIO_STOP_IMMEDIATE);
				}
				voicesPandS.emplace_back(eventInstance);

				//Voice Actions Player
			case 4:
				if (voiceIsPlaying()) {
						eventInstance->stop(FMOD_STUDIO_STOP_IMMEDIATE);
						break;
				}
				else {
						player.emplace_back(eventInstance);
				}
				break;

			default:
				break;
			}
		}
	}
	return CSoundEvent(this, retID);
}

CSoundEvent CModuleAudio::playEvent3D(const std::string& name, TCompTransform* trans, int type, Studio::EventInstance* eventInstance) {
	int retID = 0;
	auto iter = mEvents.find(name);

	if (eventInstance == nullptr) {
		if (iter != mEvents.end()) {
			iter->second->createInstance(&eventInstance);

			if (eventInstance) {
				eventInstance->start();

				nextID++;
				retID = nextID;
				mEventInstances.emplace(retID, eventInstance);

				switch (type)
				{
					//Environment
				case 0:
					environment.emplace(eventInstance, trans);
					break;

					//Enemies
				case 1:
					enemies.emplace(eventInstance, trans);
					break;

					//Objects
				case 2:
					objects.emplace(eventInstance, trans);
					break;

					//Voice
				case 3:
					for (auto event : voice) {
						if (event.second == trans) event.first->stop(FMOD_STUDIO_STOP_IMMEDIATE);
					}
					voice.emplace(eventInstance, trans);
					break;

				default:
					break;
				}
			}
		}
	}
	else {
		if (eventInstance) {
			eventInstance->start();

			switch (type)
			{
				//Environment
			case 0:
				environment.emplace(eventInstance, trans);
				break;

				//Enemies
			case 1:
				enemies.emplace(eventInstance, trans);
				break;

				//Objects
			case 2:
				objects.emplace(eventInstance, trans);
				break;

				//Voice
			case 3:
				for (auto event : voice) {
					if (event.second == trans) event.first->stop(FMOD_STUDIO_STOP_IMMEDIATE);
				}
				voice.emplace(eventInstance, trans);
				break;

			default:
				break;
			}
		}

	}

	set3DAttributes(eventInstance, trans);

	return CSoundEvent(this, retID);
}

Studio::EventInstance* CModuleAudio::createEvent(std::string name) {
	int retID = 0;
	auto iter = mEvents.find(name);
	Studio::EventInstance* eventInstance = nullptr;

	if (iter != mEvents.end()) {
		iter->second->createInstance(&eventInstance);

		if (eventInstance) {
			nextID++;
			retID = nextID;
			mEventInstances.emplace(retID, eventInstance);
		}
	}

	return eventInstance;
}

void CModuleAudio::set3DAttributes(Studio::EventInstance* event, TCompTransform* trans) {
	VEC3 pos = trans->getPosition();
	VEC3 front = trans->getFront();
	front.Normalize();
	VEC3 up = trans->getUp();
	up.Normalize();

	FMOD_3D_ATTRIBUTES attributes;
	attributes.position = EngineAudio.Vec3toFmodVect(pos);
	attributes.forward = EngineAudio.Vec3toFmodVect(front);
	attributes.up = EngineAudio.Vec3toFmodVect(up);
	attributes.velocity = { 0, 0, 0 };

	event->set3DAttributes(&attributes);
}

void CModuleAudio::update3D() {
	for (auto& event : environment) {
		if (event.second != nullptr) set3DAttributes(event.first, event.second);
	}
	
	for (auto& event : enemies) {
		if (event.second != nullptr) 
			set3DAttributes(event.first, event.second);
	}
	
	for (auto& event : voice) {
		if (event.second != nullptr) 
			set3DAttributes(event.first, event.second);
	}
	
	for (auto& event : objects) {
		if (event.second != nullptr) set3DAttributes(event.first, event.second);
	}
}

void CModuleAudio::pauseSounds() {
	for (auto& event : environment) {
		FMOD_RESULT res = event.first->setPaused(true);
	}

	for (auto& event : enemies) {
		FMOD_RESULT res = event.first->setPaused(true);
	}

	for (auto& event : objects) {
		FMOD_RESULT res = event.first->setPaused(true);
	}
	
	for (auto& event : voice) {
		FMOD_RESULT res = event.first->setPaused(true);
	}

	for (auto& event : music) {
		FMOD_RESULT res = event->setPaused(true);
	}

	for (auto& event : player) {
		FMOD_RESULT res = event->setPaused(true);
	}
	
	for (auto& event : voicesPandS) {
		FMOD_RESULT res = event->setPaused(true);
	}

}

void CModuleAudio::resumeSounds() {
	for (auto& event : environment) {
		event.first->setPaused(false);
	}

	for (auto& event : enemies) {
		event.first->setPaused(false);
	}

	for (auto& event : objects) {
		event.first->setPaused(false);
	}
	
	for (auto& event : voice) {
		event.first->setPaused(false);
	}

	for (auto& event : music) {
		event->setPaused(false);
	}

	for (auto& event : player) {
		event->setPaused(false);
	}
	
	for (auto& event : voicesPandS) {
		event->setPaused(false);
	}
}

void CModuleAudio::stopVoices() {
	for (auto& iter : voicesPandS) {
		iter->stop(FMOD_STUDIO_STOP_IMMEDIATE);
	}
}

void CModuleAudio::renderDebug() {}

void CModuleAudio::renderInMenu() {

}

Studio::EventInstance* CModuleAudio::getEventInstance(int id) {
	for (auto& iter : mEventInstances) {
		if (id == iter.first) return iter.second;
	}
	return nullptr;
}

void CModuleAudio::load()
{
	json j = loadJson("./data/audio/banks/banks.json");
	auto banks = j["banks"].get< std::vector< std::string > >();
	for (auto& b : banks) {
		loadBank(b);
	}
}

void CModuleAudio::loadBank(std::string bankName) {
	if (mBanks.find(bankName) != mBanks.end()) return;
	assert(system);

	Studio::Bank* bank = nullptr;
	FMOD_RESULT res = system->loadBankFile(bankName.c_str(), FMOD_STUDIO_LOAD_BANK_NORMAL, &bank);

	const int maxPathLength = 512;

	if (res == FMOD_OK) {
		mBanks.emplace(bankName, bank);
		bank->loadSampleData();
		int numEvents = 0;
		bank->getEventCount(&numEvents);
		
		if (numEvents > 0) {
			std::vector<Studio::EventDescription*> events(numEvents);
			bank->getEventList(events.data(), numEvents, &numEvents);

			char eventName[maxPathLength];

			for (int i = 0; i < numEvents; i++) {
				Studio::EventDescription* e = events[i];
				e->getPath(eventName, maxPathLength, nullptr);
				mEvents.emplace(eventName, e);
			}
		}

		int numBuses = 0;
		bank->getBusCount(&numBuses);

		if (numBuses > 0) {
			std::vector<Studio::Bus*> buses(numBuses);
			bank->getBusList(buses.data(), numBuses, &numBuses);

			char busName[maxPathLength];

			for (int i = 0; i < numBuses; i++) {
				Studio::Bus* b = buses[i];
				b->getPath(busName, maxPathLength, nullptr);
				mBuses.emplace(busName, b);
			}
		}
	}
}

void CModuleAudio::cleanDoneSounds(Studio::EventInstance* instance) {

	auto aux = environment.find(instance);
	if (aux != environment.end()) environment.erase(aux);	

	aux = enemies.find(instance);
	if (aux != enemies.end()) enemies.erase(aux);	

	aux = objects.find(instance);
	if (aux != objects.end()) objects.erase(aux);	

	aux = voice.find(instance);
	if (aux != voice.end()) voice.erase(aux);	

	auto auxVec = std::find(menu.begin(), menu.end(), instance);
	if (auxVec != menu.end()) menu.erase(auxVec);
	
	auxVec = std::find(music.begin(), music.end(), instance);
	if (auxVec != music.end()) music.erase(auxVec);
	
	auxVec = std::find(player.begin(), player.end(), instance);
	if (auxVec != player.end()) player.erase(auxVec);
	
	auxVec = std::find(voicesPandS.begin(), voicesPandS.end(), instance);
	if (auxVec != voicesPandS.end()) voicesPandS.erase(auxVec);
}

//Bus
float CModuleAudio::getBusVolume(const std::string& name) const {
	auto iter = mBuses.find(name);
	float aux = 0;

	if (iter != mBuses.end()) iter->second->getVolume(&aux);
	
	return aux;
}

bool CModuleAudio::getBusPaused(const std::string& name) const {
	auto iter = mBuses.find(name);
	bool aux = false;

	if (iter != mBuses.end()) iter->second->getPaused(&aux);

	return aux;
}

void CModuleAudio::setBusVolume(const std::string& name, float vol) {
	auto iter = mBuses.find(name);

	if (iter != mBuses.end()) iter->second->setVolume(vol);
}

void CModuleAudio::setBusPaused(const std::string& name, bool pause) {
	auto iter = mBuses.find(name);

	if (iter != mBuses.end()) iter->second->setPaused(pause);
}

void CModuleAudio::busControlVoice() {
	if (voiceIsPlaying()) {
		if (sfxVol > 0.4) setBusVolume("bus:/SFX", 0.4);
		if (musicVol > 0.4) setBusVolume("bus:/Music", 0.4);
	}
	else {
		setBusVolume("bus:/SFX", sfxVol);
		setBusVolume("bus:/Music", musicVol);
	}
}

bool CModuleAudio::voiceIsPlaying() {
	for (auto event : voicesPandS) {
		FMOD_STUDIO_PLAYBACK_STATE status;
		event->getPlaybackState(&status);
		if (status == FMOD_STUDIO_PLAYBACK_PLAYING)
			return true;
	}

	return false;
}

void CModuleAudio::controlMusic() {
	if (EnginePersistence.finalMusic) {
		if (EnginePersistence.endGame < 1) {
			EnginePersistence.musicGame.setParameter("EndGame", EnginePersistence.endGame);
			EnginePersistence.endGame = EnginePersistence.endGame + 0.003f;
		}
		if (EnginePersistence.endGame > 1) {
			EnginePersistence.endGame = 1;
			EnginePersistence.musicGame.setParameter("EndGame", EnginePersistence.endGame);
		}
	}
	else {
		if (EnginePersistence.getEnemyCounter() >= 1) {
			if (combat < 1) {
				EnginePersistence.musicGame.setParameter("Combat", combat);
				combat = combat + 0.003f;
			}
			else if (combat > 1) {
				combat = 1;
				EnginePersistence.musicGame.setParameter("Combat", combat);
			}
		}
		else {
			if (combat < 0) {
				combat = 0;
				EnginePersistence.musicGame.setParameter("Combat", combat);
			}
			else if (combat > 0) {
				EnginePersistence.musicGame.setParameter("Combat", combat);
				combat = combat - 0.003f;
			}
		}

		if (EnginePersistence.sceneToLoad == "Castle") {
			if (scene < 1) {
				EnginePersistence.musicGame.setParameter("Escena", scene);
				scene = scene + 0.003f;
			}
			if (scene > 1) {
				scene = 1;
				EnginePersistence.musicGame.setParameter("Escena", scene);
			}
		}
	}
}

//Helpers
FMOD_VECTOR CModuleAudio::Vec3toFmodVect(VEC3 vec) {
	FMOD_VECTOR auxVec;
	auxVec.x = vec.x;
	auxVec.y = vec.y;
	auxVec.z = vec.z;

	return auxVec;
}
