#include "mcv_platform.h"
#include "engine.h"
#include "modules/module_enemy_manager.h"
#include "components/controllers/comp_char_controller.h"
#include "components/ai/bt_gnome.h"
#include "handle/handle.h"

CEnemyManager::CEnemyManager(const std::string& name)
	: IModule(name)
{}

bool CEnemyManager::start() { 
	return true;
}

void CEnemyManager::stop() {
	ClearSBB();
}

void CEnemyManager::update(float delta) {
	timeCDWarn+= delta;
	if (timeCDWarn > 1) cdWarnOtherEnemies = true;

	timeOneGnomeDeath += delta;
	if (timeOneGnomeDeath > 1) canDieByOtherGnome = true;
}

void CEnemyManager::renderDebug() {}

void CEnemyManager::renderInMenu() {}

void CEnemyManager::addMageFighterSBB(CHandle new_fighter) {
	MageFighter new_mage;
	new_mage.fighter = new_fighter;
	new_mage.inCombat = false;
	magesFightersSBB.push_back(new_mage);
}

void CEnemyManager::eraseMageFighterSBB(CHandle erase_fighter) {
	for (int i = 0; i < magesFightersSBB.size(); i++) {
		if (magesFightersSBB[i].fighter == erase_fighter) {
			if (magesFightersSBB[i].inCombat) {
				magesFightersSBB[i].inCombat = false;
				DecreaseInCombatFighters();
			}
			magesFightersSBB.erase(magesFightersSBB.begin() + i);
			break;
		}
	}
}

void CEnemyManager::addMeleeFighterSBB(CHandle new_fighter) {
	MeleeFighter melee_fighter;
	melee_fighter.fighter = new_fighter;
	melee_fighter.isProtected = false;
	melee_fighter.inCombat = false;
	melee_fighter.attacking = false;
	meleeFightersSBB.push_back(melee_fighter);
}

void CEnemyManager::EraseMeleeFighterSBB(CHandle erase_fighter) {
	PROFILE_FUNCTION("EraseMeleeFighterSBB");
	for (int i = 0; i < meleeFightersSBB.size(); i++) {
		if (meleeFightersSBB[i].fighter.getExternalIndex() == erase_fighter.getExternalIndex()) {
			if (meleeFightersSBB[i].attacking) {
				numSlotsBusy--;
				meleeFightersSBB[i].attacking = false;
			}
			if (meleeFightersSBB[i].inCombat) {
				DecreaseInCombatFighters();
			}
			meleeFightersSBB.erase(meleeFightersSBB.begin() + i);
			break;
		}
	}
}

void CEnemyManager::InCombatMeleeFighterSBB(CHandle new_fighter) {
	for (int i = 0; i < meleeFightersSBB.size(); i++) {
		if (meleeFightersSBB[i].fighter== new_fighter) {
			if (!meleeFightersSBB[i].inCombat) {
				meleeFightersSBB[i].inCombat = true;
			}
			break;
		}
	}
}

void CEnemyManager::StopCombatMeleeFighterSBB(CHandle fighter) {
	for (int i = 0; i < meleeFightersSBB.size(); i++) {
		if (meleeFightersSBB[i].fighter == fighter) {
			if (meleeFightersSBB[i].inCombat) {
				meleeFightersSBB[i].inCombat = false;
			}
			break;
		}
	}
}

bool CEnemyManager::oneGoblinToGoblinDeath() {
	if (!canDieByOtherGnome)
		return false;

	timeOneGnomeDeath = 0;
	canDieByOtherGnome = false;
	return true;
}

CHandle CEnemyManager::ProtectMeleeFighterSBB() {
	PROFILE_FUNCTION("ProtectMeleeFighterSBB");
	for (int i = 0; i < meleeFightersSBB.size(); i++) {
		if (meleeFightersSBB[i].inCombat && !meleeFightersSBB[i].isProtected) {
			meleeFightersSBB[i].isProtected = true;
			CEntity* e_gnome = meleeFightersSBB[i].fighter;
			TMsgMagicProtection msg;
			msg.isProtected = true;
			e_gnome->sendMsg(msg);
			return meleeFightersSBB[i].fighter;
		}
	}
	return CHandle();
}

void CEnemyManager::UnProtectMeleeFighterSBB(CHandle fighter){
	PROFILE_FUNCTION("LiberateMeleeFighterSBB");
	for (int i = 0; i < meleeFightersSBB.size(); i++) {
		if (meleeFightersSBB[i].fighter == fighter) {
			meleeFightersSBB[i].isProtected = false;

			CEntity* e_gnome = fighter;
			TMsgMagicProtection msg;
			msg.isProtected = false;
			e_gnome->sendMsg(msg);
			break;
		}
	}
}

VEC3 CEnemyManager::OccupyStrategicPositionSBB(CHandle owner, float acceptable_distance) {
	PROFILE_FUNCTION("ApplyStrategicPositionSBB");
	CEntity* entity = owner;
	TCompTransform* c_trans = entity->get<TCompTransform>();
	for (int i = 0; i < strategicCombatPositionsSBB.size(); i++) {
		if (!strategicCombatPositionsSBB[i].busy && c_trans->accurateDistance(strategicCombatPositionsSBB[i].position) < acceptable_distance) {
			strategicCombatPositionsSBB[i].owner = owner;
			strategicCombatPositionsSBB[i].busy = true;
			return strategicCombatPositionsSBB[i].position;
		}
	}
	return VEC3(0, 0, 0);
}

void CEnemyManager::SetFreeStrategicPositionSSB(CHandle owner) {
	PROFILE_FUNCTION("SetFreeStrategicPositionSSB");
	for (int i = 0; i < strategicCombatPositionsSBB.size(); i++) {
		if (strategicCombatPositionsSBB[i].owner == owner) {
			strategicCombatPositionsSBB[i].owner = CHandle();
			strategicCombatPositionsSBB[i].busy = false;
		}
	}
}

void CEnemyManager::InsertStrategicPositionSBB(VEC3 position) {
	PROFILE_FUNCTION("InsertStrategicPositionSBB");
	StrategicPosition strategicPosition;
	strategicPosition.position = position;
	strategicPosition.busy = false;
	strategicPosition.owner = CHandle();
	strategicCombatPositionsSBB.push_back(strategicPosition);
}

void CEnemyManager::ClearSBB() {
	meleeFightersSBB.clear();
	magesFightersSBB.clear();
	strategicCombatPositionsSBB.clear();
	numFightersInCombat = 0;
	numMaxFightersInCombat = 0;
	numSlotsBusy = 0;
}

void CEnemyManager::WarnOtherEnemies() {
	if (!cdWarnOtherEnemies)
		return;

	timeCDWarn = 0;
	cdWarnOtherEnemies = false;

	for (int i = 0; i < meleeFightersSBB.size(); i++) {
		if (!meleeFightersSBB[i].inCombat) {
			TMsgDetectPlayer msg;
			CEntity* fighter = meleeFightersSBB[i].fighter;
			if (!fighter) continue;
			fighter->sendMsg(msg);
		}
	}

	for (int i = 0; i < magesFightersSBB.size(); i++) {
		if (!magesFightersSBB[i].inCombat)
		{
			TMsgDetectPlayer msg;
			CEntity* fighter = magesFightersSBB[i].fighter;
			if (!fighter) continue;
			fighter->sendMsg(msg);
		}
	}
}





	//void CEnemyManager::InCombatMageSBB(CHandle new_fighter) {
	//	for (int i = 0; i < magesFightersSBB.size(); i++) {
	//		if (magesFightersSBB[i].fighter == new_fighter) {
	//			if (!magesFightersSBB[i].inCombat) {
	//				magesFightersSBB[i].inCombat = true;
	//				IncreaseInCombatFighters();
	//			}
	//			break;
	//		}
	//	}
	//	if (cdWarnOtherEnemies) WarnOtherEnemies();
	//}
	//
	//void CEnemyManager::StopCombatMageSBB(CHandle new_fighter){
	//	for (int i = 0; i < magesFightersSBB.size(); i++) {
	//		if (magesFightersSBB[i].fighter == new_fighter) {
	//			if (magesFightersSBB[i].inCombat) {
	//				magesFightersSBB[i].inCombat = false;
	//				DecreaseInCombatFighters();
	//			}
	//			break;
	//		}
	//	}
	//}


//bool cenemymanager::attackingmeleefightersbb(chandle new_fighter) {
//	profile_function("applymeleefightersbb");
//	if (numslotsbusy >= nummaxslots)
//		return false;
//
//	for (int i = 0; i < meleefighterssbb.size(); i++) {
//		if (!meleefighterssbb[i].attacking && meleefighterssbb[i].fighter == new_fighter) {
//			meleefighterssbb[i].attacking = true;
//			numslotsbusy++;
//			return true;
//		}
//	}
//	return false;
//}
//
//void cenemymanager::stopattackingmeleefightersbb(chandle erase_fighter) {
//	for (int i = 0; i < meleefighterssbb.size(); i++) {
//		if (meleefighterssbb[i].fighter.getexternalindex() == erase_fighter.getexternalindex()) {
//			if (meleefighterssbb[i].attacking) {
//				numslotsbusy--;
//				meleefighterssbb[i].attacking = false;
//				break;
//			}
//		}
//	}
//}
//


//void CEnemyManager::CheckPanic() {
//	if ((float)numFightersInCombat / (float)numMaxFightersInCombat <= 0.25) {
//		for (int i = 0; i < meleeFightersSBB.size(); i++) {
//			if (meleeFightersSBB[i].inCombat && !meleeFightersSBB[i].isProtected) {
//				TMsgPanic msgPanic;
//				CEntity* fighter = meleeFightersSBB[i].fighter;
//				fighter->sendMsg(msgPanic);
//				if (meleeFightersSBB[i].attacking) {
//					meleeFightersSBB[i].attacking = false;
//					numSlotsBusy--;
//				}
//				meleeFightersSBB[i].inCombat = false;
//				DecreaseInCombatFighters();
//				break;
//			}
//		}
//	}
//}

void CEnemyManager::IncreaseInCombatFighters() {
	numFightersInCombat++;
	if (numFightersInCombat > numMaxFightersInCombat) numMaxFightersInCombat = numFightersInCombat;
}

void CEnemyManager::DecreaseInCombatFighters() {
	numFightersInCombat--;
	if (numFightersInCombat == 0) numMaxFightersInCombat = 0;
}