#pragma once

#include "modules/module.h"

class CMeshInstanced;

class CModuleDecals : public IModule {

  struct TGPUInfo {
    MAT44 world;
    VEC4  color;
  };

  struct TInstanceInfo
  {
    std::string name;
    std::string type;
    float ttl;
    int gpu_info_pos;
  };

  // Holds the name of every decal entity and the position on the decal info, holds the true number of decals
  std::unordered_map<std::string, TInstanceInfo> info_by_name; // name, instance info

  // Every decal gpu info, stored by type
  std::unordered_map<std::string, std::vector< TGPUInfo > > gpu_info_by_type;  // type, vector with info on all decals of that type

  // Every type of decal, holds all unique groups of decals
  std::unordered_map<std::string, CMeshInstanced* > decal_types;  // type, pointer with the instanced_mesh

public:
  CModuleDecals(const std::string& name);
  bool start() override;
  void renderDebug() override;
  void renderInMenu() override;
  void update(float dt) override;
  // If TTL == -1 -> TTL = inf
  void addDecal(std::string name, std::string type, VEC3 pos, QUAT rotation = QUAT(), float scale = 1, VEC4 color = VEC4(1, 1, 1, 1), float TTL = -1.0f);
  void deleteDecal(std::string name);
  void modifyDecal(std::string name, VEC3 pos, QUAT rotation = QUAT(), float scale = 1);
  void modifyDecal(std::string name, VEC4 color);
};