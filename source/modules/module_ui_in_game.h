#pragma once

#include "modules/module.h"

class CModuleUiInGame : public IModule
{
public:
	CModuleUiInGame(const std::string& name);
	bool start() override;
	void stop() override;

private:

};