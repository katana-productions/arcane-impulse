#pragma once
#include "modules/module.h"

class CModuleSettings : public IModule
{
private:
	bool cursorVisibilityEnabled = true;
	bool resChangedPending = false;
	bool inGame = false;
	bool inPause = false;
	float timeResChange;
  bool showDebug = true;
  bool using_keyboard = true; // false-> using gamepad

public:
	CModuleSettings(const std::string& name);

	bool start() override;
	void stop() override;
	void update(float delta) override;
	void renderDebug() override;
	void renderInMenu() override;

	void setEnabled(bool newCursorVisibilityEnabled) { cursorVisibilityEnabled = newCursorVisibilityEnabled; }
	void setInGame(bool newInGame) { inGame = newInGame; }
	void setInPause(bool newInPause) { inPause = newInPause; }
	bool getCursorVisibilityEnabled() { return cursorVisibilityEnabled; }
  void setShowDebugWindow(bool b) { showDebug = b; }
	bool getShowDebugWindow() { return showDebug; }
	bool usingKeyboard() { return using_keyboard; }

	void LoadFirstFrameTextures();

};
