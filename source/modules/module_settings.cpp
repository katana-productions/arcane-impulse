#include "mcv_platform.h"
#include "engine.h"
#include "handle/handle.h"
#include "modules/module_settings.h"
#include "render\module_render.h"
#include "components/common/comp_buffers.h"
#include "render/compute/gpu_buffer.h"
#include "input\input.h"
#include "components/controllers/comp_char_controller.h"

CModuleSettings::CModuleSettings(const std::string& name)
	: IModule(name)
{}

bool CModuleSettings::start() {
  json cfg = loadJson("data/config.json");
  const json& cfg_render = cfg["render"];
  showDebug = cfg_render.value("debugWindow", false);

  //LoadFirstFrameTextures();
  return true;
}

void CModuleSettings::stop() {

}

void CModuleSettings::update(float delta) {
	if (resChangedPending && Time.current > timeResChange)
	{
		Engine.getRender().onResolutionUpdated();
		resChangedPending = false;
	}

	//FullScreen
	if (EngineInput["f1"].justPressed()) {
		Render.fullscreen = !Render.fullscreen;
		timeResChange = Time.current + 0.05f;
		resChangedPending = true;
	}

	//Cursor Visibility Toggle
	if (EngineInput["f6"].justPressed()) cursorVisibilityEnabled = !cursorVisibilityEnabled;

	//Reset
	if (EngineInput["Reset"].justPressed()) {
		EnginePersistence.gameState = CEngine::get().getModules().getCurrentGamestate();
		CEngine::get().getModules().changeToGamestate("gs_reset");
	//EnginePersistence.resetPlayerPos();
	}

    //Debug Cam
	if (EngineInput["DebugCam"].justPressed())
	{
		CHandle debugCamH = getEntityByName("Camera Debug");
		CHandle playerH = getEntityByName("Player");
		if (debugCamH.isValid() && playerH.isValid())
		{
			CEntity* debugCamE = (CEntity*)debugCamH;
			CEntity* playerE = (CEntity*)playerH;
			TCompTransform* debugCamTrans = debugCamE->get<TCompTransform>();
			TCompTransform* playerTrans = playerE->get<TCompTransform>();
			debugCamTrans->setPosition(playerTrans->getPosition() + VEC3(0, 5, 0));
		}
		EngineRender.switchDebugCam();
	}

	//Activate cursor
	if (EngineInput["f5"].justPressed()) CApplication::get().toggleCursorVisible();

	//Pause
	if (EngineInput["f7"].justPressed())
	{
		EnginePhysics.isPaused = !EnginePhysics.isPaused;
		if (EnginePhysics.isPaused) Time.scale_factor = 0.0f;
		else Time.scale_factor = 1.0f;
	}

	//Activate powers
	if (EngineInput["f8"].justPressed()) {
		EnginePersistence.setGloves(true);
		EnginePersistence.setPowers();
	}

	//Change checkpoint
	if (EngineInput["f9"].justPressed()) EnginePersistence.setCheckpoint();

	//VSync
	if (EngineInput["f4"].justPressed()) {
		if (Render.vsync < 2) ++Render.vsync;
		else Render.vsync = 0;
	}

	//Toggle Gameplay/Pause
	if (EngineInput["Pause"].justPressed() && inGame) {
		if (!inPause)
			CEngine::get().getModules().changeToGamestate("gs_pause");
		if (inPause)
			CEngine::get().getModules().changeToGamestate("gs_gameplay");
	}

	//MOVE PLAYER TO DEBUG CAM POS
	if (EngineInput["control"].isPressed() && EngineInput["1"].justPressed()) {
		CHandle playerH = getEntityByName("Player");
		CHandle camera = getEntityByName("Camera Debug");

		if (playerH.isValid() && camera.isValid())
		{
			CEntity* playerE = (CEntity*)playerH;
			TCompCharController* playerCC = playerE->get<TCompCharController>();
			CEntity* cameraE = (CEntity*)camera;
			TCompTransform* cameraPos = cameraE->get<TCompTransform>();
			playerCC->SetFootPos(cameraPos->getPosition());
		}
	}

	// Show/Hide debug window
	if (EngineInput["control"].isPressed() && EngineInput["3"].justPressed())
	{
		showDebug = !showDebug;
	}

	//Set for testing scenes
	/*if (EngineInput["control"].isPressed() && EngineInput["2"].justPressed()) {
		EnginePersistence.setTesting(!EnginePersistence.getTesting());
		CEngine::get().getModules().changeToGamestate("reset");
	}
*/

	if (EngineInput["control"].isPressed() && EngineInput["2"].justPressed()) {
		EngineSceneLoader.loadSceneNoBlock("data/scenes/Sewer_Claudia.json");
	}

  if (EngineInput["any_keyboard"].justPressed()) using_keyboard = true;
  if (EngineInput["any_gamepad"].justPressed()) using_keyboard = false;
}

void CModuleSettings::renderDebug() {
}

void CModuleSettings::renderInMenu() {
	ImGui::DragInt("VSync: 0 = NoVs / 1 = VSync / 2 = Half Refresh", &Render.vsync, 1, 1, 3);
  ImGui::Checkbox("Using Keyboard", &using_keyboard);
}

void CModuleSettings::LoadFirstFrameTextures()
{
	json json = loadJson("data/FirstFrameTextures.json");
	auto& jEntities = json["Dummy"];
	auto& jEntityToModify = jEntities[0];
	auto& jEntity = jEntityToModify["entity"];
	auto& jRender = jEntity["render"];


	CEntity* e_dummy = EngineBasics.CreateEntityFromJson(jEntities, "firstFrameEntity", VEC3(0,0,0), 0, 0, 0);
	TCompRender* c_render = e_dummy->get<TCompRender>();
	TCompBuffers* c_buffer = e_dummy->get<TCompBuffers>();
	auto cte_buffer = c_buffer->getCteByName("TCtesParticles");

	// Read all textures
	auto& textures = json["MaterialToLoad"];
	for (auto it = textures.begin(); it != textures.end(); ++it) {
		PROFILE_FUNCTION("MaterialToLoad");
		assert(it->is_object());

		auto& texture = *it;
		std::string texture_name = texture["FileName"];
		jRender["material"] = texture_name;

		c_render->readMesh(jRender);
		c_render->showMeshesWithState(c_render->curr_state);

		std::string mesh_name = jRender["mesh"];
		for (auto gpu_buffer : c_buffer->gpu_buffers) {
			if (gpu_buffer->name == "indirect_draw") {
				const CMesh* mesh = Resources.get(mesh_name)->as<CMesh>();
				auto& g = mesh->getGroups();
				assert(g.size() > 0);
				uint32_t args[5] = { g[0].num_indices, 0, g[0].first_idx, 0, 0 };
				gpu_buffer->copyCPUtoGPUFrom(args);
			}
		}
		cte_buffer->updateGPU();
	}

	CHandle(e_dummy).destroy();
}
