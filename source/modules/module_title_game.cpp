#include "mcv_platform.h"
#include "module_title_game.h"
#include "engine.h"
#include "input/input.h"
#include "ui/module_ui.h"
#include "ui/widgets/ui_progress.h"
#include "ui/controllers/ui_menu_controller.h"
#include "ui/widgets/ui_button.h"
#include "components/player/comp_ui.h"
#include "audio/SoundEvent.h"


CModuleTitleGame::CModuleTitleGame(const std::string& name)
	: IModule(name)
{}

bool CModuleTitleGame::start()
{
	EngineAudio.pauseSounds();

	if (!EngineAudio.musics.isValid()) {
		EngineAudio.musics = EngineAudio.playEvent("event:/Music/MusicMenu", 0);
	}
	else {
		EngineAudio.musics.Restart();
	}

	Engine.getSettings().setInGame(false);
	Engine.getPersistence().blockPlayer(true);
	Engine.getSettings().setEnabled(true);
	CApplication::get().setCursorVisible(true);

	EngineLua.launchEvent(15);

	UI::CModuleUI& ui = Engine.getUI();

	ui.activateWidget("title_game");
	
	UI::CMenuController* menu = new UI::CMenuController;
  menu->setAnyKey(true);

	menu->registerOption(dynamic_cast<UI::CButton*>(ui.getWidgetByAlias("title_game_click_to_start")), std::bind(&CModuleTitleGame::onOptionStart, this));

	menu->setCurrentOption(0);

	ui.registerController(menu);
	return true;
}

void CModuleTitleGame::onOptionStart()
{
	Engine.getUI().unregisterControllers();

	//Activa Lua Change Gamestate
	EngineLua.launchEvent(16);

	printf("START");
}


void CModuleTitleGame::stop()
{
	Engine.getUI().deactivateWidget("title_game");
}

void CModuleTitleGame::update(float dt)
{

}

void CModuleTitleGame::setPause(bool p)
{
	EnginePhysics.isPaused = p;
	if (EnginePhysics.isPaused) Time.scale_factor = 0.0f;
	else Time.scale_factor = 1.0f;
}
