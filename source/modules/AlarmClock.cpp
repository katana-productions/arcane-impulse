#include "mcv_platform.h"
#include "engine.h"
#include "modules/AlarmClock.h"
#include "handle/handle.h"

CAlarmClock::CAlarmClock(const std::string& name)
	: IModule(name)
{}

bool CAlarmClock::start() { return true; }

void CAlarmClock::stop()
{
	alarmList.clear();
}

void CAlarmClock::AddAlarm(float alarmT, int id, CHandle alarmReciever)
{
	Alarm newAlarm;
	newAlarm.alarmT = alarmT + globalTime;
	newAlarm.id = id;
	newAlarm.alarmReciever = alarmReciever;
	alarmList.push_back(newAlarm);
}

void CAlarmClock::DeleteAlarms(const int& id, CHandle handleOfEmisor)
{
	std::list<Alarm>::iterator it = alarmList.begin();
	while (it != alarmList.end())
	{
		if (!it->alarmReciever.isValid() || !handleOfEmisor.isValid()) break;
		CEntity* e1 = (CEntity*)it->alarmReciever.getOwner();
		CEntity* e2 = (CEntity*)handleOfEmisor.getOwner();
		if (it->id == id && e1 == e2) alarmList.erase(it++);
		else ++it;
	}
}

void CAlarmClock::update(float delta)
{
	globalTime += delta;

	std::list<Alarm>::iterator it = alarmList.begin();
	while (it != alarmList.end())
	{
		currEntity = (CEntity*)it->alarmReciever.getOwner();
		if (!it->alarmReciever.isValid())
		{
			alarmList.erase(it++);
		}
		else if (it->alarmT <= globalTime)
		{
			TMsgAlarmClock msg;
			msg.id = it->id;
			it->alarmReciever.getOwner().sendMsg(msg);
			alarmList.erase(it++);
		}
		else ++it;
	}
}

void CAlarmClock::renderDebug() {}

void CAlarmClock::renderInMenu()
{
	if (ImGui::TreeNode("Alarms")) {
		ImGui::LabelText("Current time is: ", "%f", globalTime);

		std::list<Alarm>::iterator it;
		for (it = alarmList.begin(); it != alarmList.end(); ++it) {
			currEntity = (CEntity*)it->alarmReciever.getOwner();
			ImGui::LabelText(currEntity->getName(), "t:%f, id:%d", it->alarmT, it->id);
		}
		ImGui::TreePop();
	}
}