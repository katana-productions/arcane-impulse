#include "mcv_platform.h"
#include "engine.h"
#include "modules/module_navmesh.h"
#include "handle/handle.h"

CNavMesh::CNavMesh(const std::string& name)
	: IModule(name)
{}

bool CNavMesh::start() {
	nav_query.setNavMesh(&nav);
	return true;
}

void CNavMesh::addHandle(CHandle handle) {
	CEntity* e = handle;
	TCompCollider* c_collider = e->get<TCompCollider>();
	if(c_collider)
		handlesNavMesh.push_back(handle);
}

void CNavMesh::buildNavMesh()
{
	nav.destroy();
	if (handlesNavMesh.size() == 0) return;

	nav.m_input.clearInput();
	nav.m_input.addInput(handlesNavMesh);
	nav.m_input.computeBoundaries();
	nav.build();

	nav_crowd.init(&nav);
	handlesNavMesh.clear();
}

int CNavMesh::addAgent(VEC3 pos, float speed, float radius) {
	float position[] = { pos.x, pos.y, pos.z };
	int idx = nav_crowd.addAgent(position, speed, radius);
	return idx;
}

void CNavMesh::removeAgent(int idx) {
	nav_crowd.removeAgent(idx);
}

VEC3 CNavMesh::getAgentVel(int idx) {
	if (!nav.m_navMesh)
		return VEC3(0,0,0);
	const dtCrowdAgent* ag = nav.m_navCrowd->getAgent(idx);
	return VEC3(ag->vel[0], ag->vel[1], ag->vel[2]);
}

void CNavMesh::changeAgentSpeed(int idx, float speed) {
	if (!nav.m_navMesh)
		return;

	nav_crowd.updateAgentParams(idx, speed);
}

void CNavMesh::setAgentState(int idx, bool isActive) {
	if (!nav.m_navMesh)
		return;

	dtCrowdAgent* ag = nav.m_navCrowd->getEditableAgent(idx);
	ag->active = isActive;
}

void CNavMesh::moveCrowd(VEC3 pos) {
	if (!nav.m_navMesh)
		return;

	float position[] = { pos.x, pos.y, pos.z };
	nav_crowd.setMoveTarget(position, false);
}

void CNavMesh::moveAgentCrowd(int idx, VEC3 pos) {
	if (!nav.m_navMesh)
		return;
	float position[] = { pos.x, pos.y, pos.z };
	nav_crowd.setMoveAgent(position, idx);
}

void CNavMesh::setAgentPos(int idx, VEC3 pos) {
	if (!nav.m_navMesh)
		return;
	dtCrowdAgent* ag = nav.m_navCrowd->getEditableAgent(idx);
	ag->npos[0] = pos.x;
	ag->npos[1] = pos.y;
	ag->npos[2] = pos.z;	

}

void CNavMesh::setAgentDirection(int idx, VEC3 normalizedDir) {
	if (!nav.m_navMesh)
		return;
	dtCrowdAgent* ag = nav.m_navCrowd->getEditableAgent(idx);
	ag->vel[0] = normalizedDir.x;
	ag->vel[1] = normalizedDir.y;
	ag->vel[2] = normalizedDir.z;
}


VEC3 CNavMesh::getAgentPosition(int idx) {
	if (!nav.m_navMesh)
		return VEC3(0, 0, 0);
	const dtCrowdAgent* ag = nav.m_navCrowd->getAgent(idx);
	return VEC3(ag->npos[0], ag->npos[1], ag->npos[2]);
}

bool CNavMesh::findPath(VEC3 src, VEC3 dst, std::vector<VEC3> *path) {
	nav_query.findTPos(src, dst);
	nav_query.findPath(nav_query.p1, nav_query.p2);
	std::vector nav_path = nav_query.getPath();
	if (nav_path.size() > 1) {
		*path = nav_path;
		return true;
	}
	return false;
}

void CNavMesh::update(float delta)
{
	if (!nav.m_navMesh)
		return;
	nav_crowd.handleUpdate(delta);
}

void CNavMesh::stop()
{
	handlesNavMesh.clear();
}

void CNavMesh::renderInMenu()
{

}

void CNavMesh::renderDebug() {
 	nav.render();
}