#include "mcv_platform.h"
#include "engine.h"
#include "modules/Basics.h"
#include "handle/handle.h"
#include "components/controllers/camShake.h"
#include "components/common/explosionMesh.h"

using namespace physx;

CBasics::CBasics(const std::string& name) : IModule(name){}
bool CBasics::start() {return false;}
void CBasics::stop(){}

void CBasics::update(float delta) {
  if (delta >= 0.05f) return;
	LerpUpdate(delta);
}

void CBasics::LerpUpdate(float delta)
{
	if (elements_to_lerp.size() > 0) {
		std::list<LerpingElement>::iterator it = elements_to_lerp.begin();
		while (it != elements_to_lerp.end()) {
			if ((*it).current_time >= (*it).time_before_start) {
				if ((*it).first_frame) {
					(*it).max_element_to_lerp = *(*it).element_to_lerp;
					(*it).first_frame = false;
				}
				float diff = (*it).target_value - (*it).max_element_to_lerp;
				float percentage = OurClamp(((((*it).current_time - (*it).time_before_start)) / (*it).lerp_duration), 0.0f, 1.0f);
				*(*it).element_to_lerp = (*it).max_element_to_lerp + (diff * percentage);
			}
			(*it).current_time += delta;

			if (((*it).current_time - (*it).time_before_start) >= (*it).lerp_duration) {
				*(*it).element_to_lerp = (*it).target_value;
				it = elements_to_lerp.erase(it);
			}
			else {
				it++;
			}
		}
	}
}

void CBasics::LerpElement(float* element_to_lerp, const float& target_value, 
	const float& lerp_duration, const float& time_before_start) {
	LerpingElement new_lerp_element;
	new_lerp_element.element_to_lerp = element_to_lerp;
	new_lerp_element.max_element_to_lerp = *element_to_lerp;
	new_lerp_element.target_value = target_value;
	new_lerp_element.current_time = 0.0f;
	new_lerp_element.lerp_duration = lerp_duration;
	new_lerp_element.time_before_start = time_before_start;
	elements_to_lerp.push_back(new_lerp_element);
}

void CBasics::ShakeCamera(const float& intensity, const float& duration) {
	CEntity* entity = (CEntity*)TCompCamShake::shakerHandle;
	TCompCamShake* cShake = entity->get<TCompCamShake>();
	cShake->startShake(intensity, duration);
}

CEntity* CBasics::InstantiateAtParent(const std::string& name, const VEC3& pos, const float& yaw, const float& pitch, const float& roll, const CHandle creatorH) {
  CEntity* entityParent = creatorH.getOwner();
  assert(entityParent);
  TCompTransform* transformParent = entityParent->get<TCompTransform>();
  TEntityParseContext ctx;
  ctx.root_transform = *transformParent;
  instantiateAtCurrentScene(name, ctx);
  CEntity* entity = (CEntity *)ctx.entities_loaded[0];
  TCompCollider* collider = entity->get<TCompCollider>();
  if (collider) {
    PxTransform pxTrans(VEC3_TO_PXVEC3(pos), QUAT_TO_PXQUAT(QUAT::CreateFromYawPitchRoll(yaw, pitch, roll)));
    collider->actor->setGlobalPose(pxTrans);
  }
  TCompTransform* entityTransform = entity->get<TCompTransform>();
  entityTransform->setPosition(pos);
  entityTransform->setAngles(yaw, pitch, roll);
  return entity;

}

CEntity* CBasics::Instantiate(const std::string& name, const VEC3& pos, const float& yaw, const float& pitch, const float& roll) {
  TEntityParseContext ctx;
  instantiateAtCurrentScene(name, ctx);
  CEntity* entity = (CEntity *)ctx.entities_loaded[0];
  TCompCollider* collider = entity->get<TCompCollider>();
  if (collider) {
    PxTransform pxTrans(VEC3_TO_PXVEC3(pos), QUAT_TO_PXQUAT(QUAT::CreateFromYawPitchRoll(yaw, pitch, roll)));
    collider->actor->setGlobalPose(pxTrans);
  }
  TCompTransform* entityTransform = entity->get<TCompTransform>();
  entityTransform->setPosition(pos);
  entityTransform->setAngles(yaw, pitch, roll);
  return entity;

}

//TODO INSTANCIATE SINGLE ENTITY FROM JSON FILE
CEntity* CBasics::CreateEntityFromJson(const json & j, const std::string& name, const VEC3& pos, const float& yaw, const float& pitch, const float& roll) {
	TEntityParseContext ctx;
	instantiateEntityAtCurrentSceneFromJson(j, name, ctx);
	CEntity* entity = (CEntity *)ctx.entities_loaded[0];
	TCompCollider* collider = entity->get<TCompCollider>();
	if (collider) {
		PxTransform pxTrans(VEC3_TO_PXVEC3(pos), QUAT_TO_PXQUAT(QUAT::CreateFromYawPitchRoll(yaw, pitch, roll)));
		collider->actor->setGlobalPose(pxTrans);
	}
	TCompTransform* entityTransform = entity->get<TCompTransform>();
	entityTransform->setPosition(pos);
	entityTransform->setAngles(yaw, pitch, roll);
	return entity;

}

void CBasics::CreateExplosionMeshAt(const float & radius, const VEC3& pos) {
	TEntityParseContext ctx;
	parseScene("data/prefabs/explosionMesh.json", ctx);
	CEntity* e_explosion = (CEntity *)ctx.entities_loaded[0];
	TCompTransform* explosionPos = e_explosion->get<TCompTransform>();
	explosionPos->setPosition(pos);
	TCompExplosionMesh* explosionComp = e_explosion->get<TCompExplosionMesh>();
	explosionComp->setRadius(radius);
}

/*void CBasics::switchMesh(TCompRender* c_render, std::string mesh_name, std::string material_name) {
	PROFILE_FUNCTION("switchMesh");
	c_render->switchMesh(mesh_name, material_name);
}*/

void CBasics::switchRenderState(TCompRender* c_render, int new_state) {
	PROFILE_FUNCTION("switchRenderState");
	int curr_state = c_render->getCurrentState();
	if(curr_state != new_state) c_render->showMeshesWithState(new_state);
}

void CBasics::ChangeColor(TCompRender* c_render, VEC4 color) {
	c_render->color = color;
}

void CBasics::LoadWaypointsFromJSON(const json & j, std::vector<VEC3>* waypoints, int *nWaypoints)
{
	json jwaypoints = j["waypoints"];
	VEC3 v;
	for (json::iterator it = jwaypoints.begin(); it != jwaypoints.end(); ++it) {
		(*nWaypoints)++;
		auto val = it.value().get<std::string>();
		sscanf(val.c_str(), "%f %f %f", &v.x, &v.y, &v.z);
		waypoints->push_back(v);
	}
}

void CBasics::AdaptPatrolToOrigin(TCompTransform* c_trans, std::vector<VEC3>* waypoints) {
	VEC3 origin_pos = c_trans->getPosition();
	for (std::vector<VEC3>::iterator it = waypoints->begin(); it != waypoints->end(); ++it)
		*it += origin_pos;
}

void CBasics::SetGateKeeperPosition(TCompTransform* c_trans, VEC3 *gate_postion, VEC3 *gate_looking) {
	*gate_postion = c_trans->getPosition();
	*gate_looking = c_trans->getPosition() + (c_trans->getFront() * 10);
}

void CBasics::FixToGround(TCompTransform* c_trans, VEC3 *src, VEC3 *dst) {
	float maxDist = 1000.0;
	PxQueryFilterData fd = PxQueryFilterData();
	fd.data.word0 = ~(CModulePhysics::FilterGroup::Triggers | CModulePhysics::FilterGroup::Characters | CModulePhysics::FilterGroup::Player);
	PxRaycastHit hit;
	//src
	bool ray_cast_src = EnginePhysics.Raycast(*src, -c_trans->getUp(), maxDist, hit, (physx::PxQueryFlag::eSTATIC | physx::PxQueryFlag::eDYNAMIC), physx::PxHitFlag::eDEFAULT, fd);
	if (ray_cast_src) {
		*src = PXVEC3_TO_VEC3(hit.position);
	}
	//dst
	std::vector<PxRaycastHit> array_hits(5);
	std::vector<bool> ray_cast_dst(5);

	ray_cast_dst[0] = EnginePhysics.Raycast(*dst, -c_trans->getUp(), maxDist, array_hits[0], (physx::PxQueryFlag::eSTATIC | physx::PxQueryFlag::eDYNAMIC), physx::PxHitFlag::eDEFAULT, fd);
	ray_cast_dst[1] = EnginePhysics.Raycast(*dst + VEC3(1, 0, 0), -c_trans->getUp(), maxDist, array_hits[1], (physx::PxQueryFlag::eSTATIC | physx::PxQueryFlag::eDYNAMIC), physx::PxHitFlag::eDEFAULT, fd);
	ray_cast_dst[2] = EnginePhysics.Raycast(*dst + VEC3(-1, 0, 0), -c_trans->getUp(), maxDist, array_hits[2], (physx::PxQueryFlag::eSTATIC | physx::PxQueryFlag::eDYNAMIC), physx::PxHitFlag::eDEFAULT, fd);
	ray_cast_dst[3] = EnginePhysics.Raycast(*dst + VEC3(0, 0, 1), -c_trans->getUp(), maxDist, array_hits[3], (physx::PxQueryFlag::eSTATIC | physx::PxQueryFlag::eDYNAMIC), physx::PxHitFlag::eDEFAULT, fd);
	ray_cast_dst[4] = EnginePhysics.Raycast(*dst + VEC3(0, 0, -1), -c_trans->getUp(), maxDist, array_hits[4], (physx::PxQueryFlag::eSTATIC | physx::PxQueryFlag::eDYNAMIC), physx::PxHitFlag::eDEFAULT, fd);

	bool first_ray = true;
	for (int i = 0; i < ray_cast_dst.size(); i++) {
		if (first_ray && ray_cast_dst[i]) {
			*dst = PXVEC3_TO_VEC3(array_hits[i].position);
			first_ray = false;
		}
		else if (ray_cast_dst[i] && PXVEC3_TO_VEC3(array_hits[i].position).y > (*dst).y)
			*dst = PXVEC3_TO_VEC3(array_hits[i].position);
	}
}

bool CBasics::SteeringSeek(TCompTransform* c_trans, VEC3 *dst) {
	VEC3 src = c_trans->getPosition();
	src += VEC3(0, 1, 0);
	float maxDist = 2;

	PxQueryFilterData fd = PxQueryFilterData();
	fd.data.word0 = ~(CModulePhysics::FilterGroup::Triggers | CModulePhysics::FilterGroup::Characters | CModulePhysics::FilterGroup::Player);

	PxRaycastHit central_hit;
	PxRaycastHit left_hit;
	PxRaycastHit right_hit;

	////primer rayo miramos si colisi�n
	VEC3 direction = *dst - src;
	direction.y = 0;
	direction.Normalize();

	VEC3 front = c_trans->getFront();
	front -= src;
	front.y = 0;
	front.Normalize();
	VEC3 left = c_trans->getLeft();
	left -= src;
	left.y = 0;
	left.Normalize();

	bool no_view_player =  EnginePhysics.Raycast(src, direction, maxDist, central_hit, (physx::PxQueryFlag::eSTATIC | physx::PxQueryFlag::eDYNAMIC), physx::PxHitFlag::eDEFAULT, fd);
	bool central_ray = EnginePhysics.Raycast(src, direction, maxDist, central_hit, (physx::PxQueryFlag::eSTATIC | physx::PxQueryFlag::eDYNAMIC), physx::PxHitFlag::eDEFAULT, fd);
	bool left_ray = EnginePhysics.Raycast(src + left * 0.5, front, maxDist, left_hit, (physx::PxQueryFlag::eSTATIC | physx::PxQueryFlag::eDYNAMIC), physx::PxHitFlag::eDEFAULT, fd);
	bool right_ray= EnginePhysics.Raycast(src - left * 0.5, front, maxDist, right_hit, (physx::PxQueryFlag::eSTATIC | physx::PxQueryFlag::eDYNAMIC), physx::PxHitFlag::eDEFAULT, fd);
	if(no_view_player && central_ray)
	{
		std::string name;
		if (central_hit.actor) {
			CHandle handle;
			handle.fromVoidPtr(central_hit.actor->userData);
			handle = handle.getOwner();
			CEntity* e = handle;
			std::string name = e->getName();
		}
		VEC3 ahead = PXVEC3_TO_VEC3(central_hit.position) - src;
		int side = 1;
		//Comprobamos limite colision
		int MAX_ITERATIONS = 100;
		PxRaycastHit hit;
		for (int i = 0; i < MAX_ITERATIONS; i++) {
			if (!EnginePhysics.Raycast(src - side * i * left, front, maxDist, hit, (physx::PxQueryFlag::eSTATIC | physx::PxQueryFlag::eDYNAMIC), physx::PxHitFlag::eDEFAULT, fd))
			{
				//Calculamos nuevo punto de destino
				*dst = (src - side * i * left) + ahead;
				break;
			}
		}
		return true;
	}
	return false;
}

VEC3 CBasics::GetPlayerPos(CHandle playerHandle) {
	if (!playerHandle.isValid())
		return VEC3(0, 0, 0);

	CEntity* player = playerHandle;
	TCompTransform* trans_player = player->get<TCompTransform>();
	return trans_player->getPosition();
}

CEntity* CBasics::GetPlayerEntity() {
	CHandle playerHandle = getEntityByName("Player");
	if (playerHandle.getOwner().isValid())
	{
		return (CEntity*)playerHandle;
	}
	return nullptr;
}

CHandle CBasics::Create(const char* name, TCompTransform* c_trans, VEC3 pos) {
	TEntityParseContext ctx;
	ctx.root_transform = *c_trans;
	if(pos != VEC3(0,0,0))
		ctx.root_transform.setPosition(pos);

	parseScene(name, ctx);
	int aux = ctx.entities_loaded.size();
	return ctx.entities_loaded[0];
}

std::vector<CHandle> CBasics::CreateGroup(const char* name, TCompTransform* c_trans, VEC3 pos) {
	TEntityParseContext ctx;
	ctx.root_transform = *c_trans;
	if (pos != VEC3(0, 0, 0))
		ctx.root_transform.setPosition(pos);

	parseScene(name, ctx);
	int aux = ctx.entities_loaded.size();

	return ctx.entities_loaded;
}

std::vector<CHandle> CBasics::CreateGroup(const char* name, CTransform &c_trans, VEC3 pos) {
  TEntityParseContext ctx;
  ctx.root_transform = c_trans;
  if (pos != VEC3(0, 0, 0))
    ctx.root_transform.setPosition(pos);

  parseScene(name, ctx);
  int aux = ctx.entities_loaded.size();

  return ctx.entities_loaded;
}

void CBasics::Destroy(CHandle destroyed) {
	destroyed.destroy();
}

void CBasics::rotate_vector_by_quaternion(const Vector3& v, const Quaternion& q, Vector3& vprime)
{
	// Extract the vector part of the quaternion
	Vector3 u(q.x, q.y, q.z);

	// Extract the scalar part of the quaternion
	float s = q.w;

	// Do the math
	vprime = 2.0f * u.Dot(v) * u
		+ (s*s - u.Dot(u)) * v
		+ 2.0f * s * u.Cross(v);
}

void CBasics::PlayParticle(std::string filename, TCompTransform* c_trans)
{
	CreateGroup(filename.c_str(), c_trans);
}