#pragma once
#include "modules/module.h"
#include "audio/inc/fmod_studio.hpp"
#include "audio/inc/fmod.hpp"
#include "audio/SoundEvent.h"

using namespace FMOD;

class CModuleAudio : public IModule
{
private:

	//Maps System Audio
	std::map<std::string, Studio::Bank*> mBanks;
	std::map<std::string, Studio::EventDescription*> mEvents;
	std::map<int, Studio::EventInstance*> mEventInstances;
	std::map<std::string, Studio::Bus*> mBuses;

	//Audio 2D
	std::vector<Studio::EventInstance*> music;
	std::vector<Studio::EventInstance*> player;
	std::vector<Studio::EventInstance*> voicesPandS;
	std::vector<Studio::EventInstance*> menu;

	//Audio 3D
	std::map<Studio::EventInstance*, TCompTransform*> environment;
	std::map<Studio::EventInstance*, TCompTransform*> enemies;
	std::map<Studio::EventInstance*, TCompTransform*> objects;
	std::map<Studio::EventInstance*, TCompTransform*> voice;


	TCompTransform* playerPos = nullptr;

	int nextID = 0;

	FMOD_RESULT res;
	FMOD_STUDIO_PLAYBACK_STATE *state = nullptr;

	void *extraDriverData = NULL;

	Studio::System* system = NULL;
	System* lowLevelSystem = NULL;

	//Music controls
	float combat = 0;
	float scene = 0;

	//Bus controls
	float musicVol = 1;
	float voiceVol = 1;
	float sfxVol = 1;

public:
	CModuleAudio(const std::string& name);

	bool start() override;
	void stop() override;
	void update(float delta) override;
	void renderDebug() override;
	void renderInMenu() override;

	//Json load
	void load();

	//Bank and Bus initial set
	void loadBank(std::string);

	//Setting listener (player pos)
	TCompTransform* setListener();

	//Set attrubutes for 3DSound
	void set3DAttributes(Studio::EventInstance*, TCompTransform*);

	//Update position of each sound in the scene
	void update3D();

	//Pause all the sounds
	void pauseSounds();

	//Resume all the sounds
	void resumeSounds();

	//Cleaning maps and vectors of stopped sounds
	void cleanDoneSounds(Studio::EventInstance*);

	//Getter instance by id
	Studio::EventInstance* getEventInstance(int);

	//Bus controllers
	float getBusVolume(const std::string&) const;
	bool getBusPaused(const std::string&) const;
	void setBusVolume(const std::string&, float);
	void setBusPaused(const std::string&, bool);
	void busControlVoice();

	//Play & create events
	CSoundEvent playEvent3D(const std::string&, TCompTransform*, int, Studio::EventInstance* eventInstance = nullptr);
	CSoundEvent playEvent(const std::string&, int, Studio::EventInstance* eventInstance = nullptr);
	Studio::EventInstance* createEvent(std::string);

	//Stop vector sounds
	void stopVoices();

	//Voice is playing
	bool voiceIsPlaying();

	//Music
	CSoundEvent musics;
	void controlMusic();

	//Helpers
	FMOD_VECTOR Vec3toFmodVect(VEC3);
};
