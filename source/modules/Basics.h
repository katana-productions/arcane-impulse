#pragma once
#include "modules/module.h"
#include "entity/entity_parser.h"
#include "components/common/comp_render.h"
#include "components/common/comp_transform.h"

class CBasics : public IModule {
public:
	CBasics(const std::string& name);

	void update(float delta) override;
	bool start() override;
	void stop() override;

	void ShakeCamera(const float& intensity, const float& duration);
	CEntity* InstantiateAtParent(const std::string& name, const VEC3& pos, const float& yaw, const float& pitch, const float& roll, const CHandle creatorH);
	CEntity* Instantiate(const std::string& name, const VEC3& pos, const float& yaw, const float& pitch, const float& roll);
	CEntity* CreateEntityFromJson(const json & j, const std::string& name, const VEC3& pos, const float& yaw, const float& pitch, const float& roll);
	void CreateExplosionMeshAt(const float& radius, const VEC3& pos);

	//void switchMesh(TCompRender* c_render, std::string mesh_name, std::string material_name);
	void switchRenderState(TCompRender* c_render, int new_state);
	void ChangeColor(TCompRender* c_render, VEC4 color);

	void LoadWaypointsFromJSON(const json & j, std::vector<VEC3>* waypoints, int *nWaypoints);
	void AdaptPatrolToOrigin(TCompTransform* c_trans, std::vector<VEC3>* waypoints);
	void SetGateKeeperPosition(TCompTransform* c_trans, VEC3 *gate_postion, VEC3 *gate_looking);
	void FixToGround(TCompTransform* c_trans, VEC3 *src, VEC3 *dst);
	bool SteeringSeek(TCompTransform* c_trans, VEC3 *dst);

	VEC3 GetPlayerPos(CHandle playerHandle);
	CEntity * GetPlayerEntity();
	CEntity* GetInstancedItem(int index) { return instancedEnt[index]; }
	std::vector<CEntity *> instancedEnt;

	CHandle Create(const char* name, TCompTransform* c_trans, VEC3 pos = VEC3(0,0,0));
	std::vector<CHandle> CreateGroup(const char* name, TCompTransform* c_trans, VEC3 pos = VEC3(0, 0, 0));
	std::vector<CHandle> CreateGroup(const char* name, CTransform &c_trans, VEC3 pos = VEC3(0, 0, 0));

	void Destroy(CHandle destroyed);

	void LerpElement(float* element_to_lerp, const float& target_value,
		const float& lerp_duration, const float& time_before_start = 0.0f);


	void rotate_vector_by_quaternion(const Vector3& v, const Quaternion& q, Vector3& vprime);


	void PlayParticle(std::string filename, TCompTransform* c_trans);


private:
	struct LerpingElement {

		float *element_to_lerp;
		float max_element_to_lerp;
		float target_value;
		float time_before_start = 0.0;
		float lerp_duration;
		float current_time = 0.0;
		bool first_frame = true;
	};
	void LerpUpdate(float delta);
	std::list<LerpingElement> elements_to_lerp;
};