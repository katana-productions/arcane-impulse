#include "mcv_platform.h"
#include "module_boot.h"
#include "engine.h"
#include "entity/entity_parser.h"
#include  <iomanip>
#include  <fstream>

CModuleBoot::CModuleBoot(const std::string& name)
	: IModule(name)
{}

bool CModuleBoot::start()
{
  json jsonDoc = loadJson("data/boot.json");
  std::string sceneToLoad = jsonDoc["alwaysScene"];
  auto prefabs = jsonDoc[sceneToLoad].get< std::vector< std::string > >();
  for (auto& p : prefabs) {
    TFileContext fc(p);
    /*TEntityParseContext ctx;
    PROFILE_FUNCTION_COPY_TEXT( p.c_str() );
    dbg("Parsing boot prefab %s\n", p.c_str());
    parseScene(p, ctx);*/
    EngineSceneLoader.loadSceneBlock(p);
  }

	prefabs.clear();

  if (EnginePersistence.firstTime) {

	  //std::ifstream in("data/boot.json");
	  //json file = json::parse(in);

	  ////std::string scene = file.at("sceneToLoad");

	  //std::ofstream o("pretty.json");
	  //o << std::setw(4) << file << std::endl;

	  //json j;
	  //j = file;

	  //std::ofstream out("data/boot.json");
	  //j["sceneToLoad"] = EnginePersistence.getScene();
	  //out << std::setw(4) << j << std::endl;

	  //in.close();
	  //out.close();
	  //o.close();

	  sceneToLoad = jsonDoc["sceneToLoad"];

	  auto scenePrefabs = jsonDoc[sceneToLoad].get< std::vector< std::string > >();
	  for (auto& p : scenePrefabs) {
		  TFileContext fc(p);
		  EngineSceneLoader.loadSceneBlock(p);
	  }

		scenePrefabs.clear();

	  EnginePersistence.sceneToLoad = sceneToLoad;
	  EnginePersistence.firstTime = false;
		EngineAudio.pauseSounds();
  }
  else {
	  sceneToLoad = EnginePersistence.getScene();

	  auto scenePrefabs = jsonDoc[sceneToLoad].get< std::vector< std::string > >();
	  for (auto& p : scenePrefabs) {
		  TFileContext fc(p);
		  EngineSceneLoader.loadSceneBlock(p);
	  }

		scenePrefabs.clear();
  }
  EngineNavMesh.buildNavMesh();
	EnginePersistence.loadPostProcess();

  //EnginePersistence.setOldScene("hola");
  //std::string aux = json.value("sceneToLoad", EnginePersistence.getOldScene());
 /* if (aux == EnginePersistence.getOldScene()) EnginePersistence.setScene(aux);
  else EnginePersistence.setScene(EnginePersistence.getOldScene());*/

  return true;
}


