#pragma once
#include "modules/module.h"
#include "audio/SoundEvent.h"

class CModulePersistence : public IModule
{
private:
	std::vector<VEC3> pos;
	std::vector<VEC3> rot;
	int spawnPointId = 0;

	bool pullWallUnlocked = false;
	bool pullObjUnlocked = false;
	bool pushWallUnlocked = false;
	bool pushObjUnlocked = false;
	bool gloves = false;
	bool currentUIMagicPowerState = false;
	bool firstFrameUIGloves = true;

	//Cores
	int coresDeactivated = 0;
	int totalCores = 4;

	//Scenes
	json jsonLoadScenes = NULL;
	json jsonUnloadScenes = NULL;
	json scenes = NULL;

	std::string oldScene = "";

	//Audio
	int enemyCounter = 0;

	//PostProcess
	float globalAmbient = 0.2;
	float globalExposure = 1;
	float vigExtend = 0.25;
	float vigIntensity = 25;
	float fogDepthFade = 0.8;
	float maxFog = 0.07;
	float fogContrast = 0.07;
	float4 fogColor = VEC4(0.05, 0.05, 0.05, 1);
	std::string lutName = "data/textures/lut/neutralLut.dds";
	bool lutHasBeenLoaded = false;

public:
	CModulePersistence(const std::string& name);

	bool start() override;
	void stop() override;
	void update(float delta) override;
	void renderDebug() override;
	void renderInMenu() override;

	//Load + load postprocess
	void load();
	void loadPostProcess();

	//Reset pos in a checkpoint position
	void resetPlayerPos();

	//Magic power manager
	void setMagic(bool, bool, bool, bool);
	void setPlayerMagic();
	void setPowers();

	//Gloves manager
	void setGloves(bool);
	bool getGloves() { return gloves; };
	void setPlayerGloves();

	//UI magic controller
	bool getUIMagicPower() { return currentUIMagicPowerState; }
	void setUIMagicPower(bool enable);


	//Checkpoint 
	void setCheckpoint();
	void setCheckpointId(int id);
	void setId(int id);

	//Block player move
	void blockPlayer(bool isPaused);

	//Audio combat controller
	CSoundEvent musicGame;
	void setEnemyCounter(int cont);
	int getEnemyCounter() { return enemyCounter; }
	bool finalMusic = false;

	//PostProcess
	void setPostProcess(const float& gAmbient, const float& gExposure, const float& vExtend,
		const float& vIntensity, const float& fDepthFade, const float& mFog, const float& fContrast, 
		const float4& fColor, const std::string& lutN);
	void updatePostProcess();
	float getVigExtend() { return vigExtend; }
	float getVigIntensity() { return vigIntensity; }
	float getFogDepthFade() { return fogDepthFade; }
	float getMaxFog() { return maxFog; }
	float4 getFogColor() { return fogColor; }

	//Scenes controllers
	void setScene(std::string);
	std::string getScene() { return sceneToLoad; }
	void setOldScene(std::string);
	std::string getOldScene() { return oldScene; }
	void loadScenes();
	void resetGame();
	
	//Cores controller
	void deactivateCore();
	void resetCores();

	//Helper load - unload scenes
	bool firstTime = true;
	std::string sceneToLoad = "";
	std::string gameState = "";

	//Vector pieces objects
	std::vector<CHandle> brokenPieces = {};

	//Triggers checkpoints
	bool oneShoot = false;
	bool oneShot = false;

	//Final room music
	float endGame = 0;
};
