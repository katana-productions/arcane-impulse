#include "mcv_platform.h"
#include "module_main_menu.h"
#include "engine.h"
#include "input/input.h"
#include "ui/module_ui.h"
#include "ui/widgets/ui_progress.h"
#include "ui/controllers/ui_menu_controller.h"
#include "ui/widgets/ui_button.h"
#include "components/player/comp_ui.h"
#include "audio/SoundEvent.h"


CModuleMainMenu::CModuleMainMenu(const std::string& name)
  : IModule(name)
{}

bool CModuleMainMenu::start()
{
	//Delete when new UI
	CEntity* e_ui = getEntityByName("UI");
	if (e_ui) {
		TCompUI* c_ui = e_ui->get<TCompUI>();
		c_ui->setVisible(false);
	}
	//
	/*if (!music.isValid()) {
		music = EngineAudio.playEvent("event:/Music/MusicMenu", 0);
	}
	else {
		music.Restart();
	}*/

  CApplication::get().setCursorVisible(true);

  EngineLua.launchEvent(13);

  UI::CModuleUI& ui = Engine.getUI();

  ui.activateWidget("main_menu");
  
  UI::CMenuController* menu = new UI::CMenuController;
  
  menu->registerOption(dynamic_cast<UI::CButton*>(ui.getWidgetByAlias("bt_start")), std::bind(&CModuleMainMenu::onOptionStart, this));
  menu->registerOption(dynamic_cast<UI::CButton*>(ui.getWidgetByAlias("bt_exit")), std::bind(&CModuleMainMenu::onOptionExit, this));

  menu->setCurrentOption(0);

  ui.registerController(menu);
  return true;
}

void CModuleMainMenu::onOptionStart()
{
	//Delete when new UI
	CEntity* e_ui = getEntityByName("UI");
	if (e_ui) {
		TCompUI* c_ui = e_ui->get<TCompUI>();
		c_ui->setVisible(true);
	}
	//

  Engine.getUI().unregisterControllers();

	EngineAudio.musics.Stop();

  EnginePersistence.resetPlayerPos();

  CApplication::get().setCursorVisible(false);
  //Activa Lua Change Gamestate
  EngineLua.launchEvent(11);

  printf("START");
}

void CModuleMainMenu::onOptionExit()
{
	CApplication::get().ExitGame();
	printf("EXIT");
}

void CModuleMainMenu::stop()
{
	Engine.getUI().deactivateWidget("main_menu");
}

void CModuleMainMenu::update(float dt)
{

}

void CModuleMainMenu::setPause(bool p)
{
	EnginePhysics.isPaused = p;
	if (EnginePhysics.isPaused) Time.scale_factor = 0.0f;
	else Time.scale_factor = 1.0f;
}
