#include "mcv_platform.h"
#include "module_reset.h"
#include "engine.h"
#include "windows/app.h"

CModuleReset::CModuleReset(const std::string& name)
  : IModule(name)
{}

bool CModuleReset::start()
{
	Resources.ClearRTResources();
	//Destroy all entities
	IModule* mod_ent = CEngine::get().getModules().getModule("entities");
	mod_ent->stop();
	EngineEnemyManager.stop();
	EngineGpuCulling.stop();
	EngineAudio.stop();
	EngineAudio.start();
	//Resources.destroyAll();
	//EngineGpuCulling.start();
	EnginePersistence.setOldScene("");
	EnginePersistence.setEnemyCounter(-EnginePersistence.getEnemyCounter());
	EnginePersistence.endGame = 0;
	EnginePhysics.totalTime = 0.0f;
	return true;
}

void CModuleReset::stop()
{
}

void CModuleReset::update(float dt)
{
	if (EnginePersistence.gameState == "gs_credits") {
		CEngine::get().getModules().changeToGamestate("gs_credits");
	}
	else if (EnginePersistence.gameState == "gs_gameplay") {
		CEngine::get().getModules().changeToGamestate("gs_gameplay");
	}
	else  {
		CEngine::get().getModules().changeToGamestate("gs_title_game");
	}
}
