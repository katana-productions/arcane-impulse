#include "mcv_platform.h"
#include "module_gameplay.h"
#include "engine.h"
#include "input/input.h"
#include "render/module_render.h"
#include "windows/app.h"
#include "modules/module_camera_mixer.h"
#include "components/other/comp_player_spawn.h"

/*DECL_MSG(CModuleGameplay, TMsgEntityTriggerEnter, onTriggerEnter);
DECL_MSG(CModuleGameplay, TMsgEntityTriggerExit, onTriggerExit);*/

CModuleGameplay::CModuleGameplay(const std::string& name)
	: IModule(name)
{}

bool CModuleGameplay::start()
{
	EnginePersistence.resetPlayerPos();
	EnginePersistence.blockPlayer(false);
	EnginePersistence.setPlayerMagic();
	EnginePersistence.setPlayerGloves();
	EnginePersistence.setUIMagicPower(EnginePersistence.getUIMagicPower());
	Engine.getCameraMixer().setDefaultCamera(getEntityByName("CameraShaker"));
	Engine.getCameraMixer().setOutputCamera(getEntityByName("CameraShaker"));
	//EnginePersistence.loadScenes();
	EnginePersistence.updatePostProcess();
	return true;
}


void CModuleGameplay::stop()
{
}

void CModuleGameplay::update(float dt)
{
	const float kBlendTime = 1.f;
	static Interpolator::TQuadInOutInterpolator quadInt;

	/*Input::CModuleInput& input = CEngine::get().getInput();
	Input::TRumbleData rumble;
	rumble.leftVibration = input[Input::BT_LTRIGGER].value;
	rumble.rightVibration = input[Input::BT_RTRIGGER].value;
	input.feedback(rumble);*/
}

void CModuleGameplay::togglePauseGame()
{
	EnginePhysics.isPaused = !EnginePhysics.isPaused;
	if (EnginePhysics.isPaused) Time.scale_factor = 0.0f;
	else Time.scale_factor = 1.0f;
}

void CModuleGameplay::setPause(bool p)
{
	EnginePhysics.isPaused = p;
	if (EnginePhysics.isPaused) Time.scale_factor = 0.0f;
	else Time.scale_factor = 1.0f;
}
