#include "mcv_platform.h"
#include "module_intro.h"
#include "engine.h"
#include "ui/module_ui.h"
#include "input/input.h"
#include "ui/widgets/ui_progress.h"

CModuleIntro::CModuleIntro(const std::string& name)
  : IModule(name)
{}

bool CModuleIntro::start()
{
  _timer = 3.f;
  UI::CModuleUI& ui = Engine.getUI();

  ui.activateWidget("intro");
  Engine.getUI().activateWidget("skip_cinematic");

  EngineLua.launchEvent(0);

  return true;
}

void CModuleIntro::stop()
{
	EngineAudio.resumeSounds();
	EngineSettings.setEnabled(true);
	EngineSettings.setInGame(true);
	EnginePersistence.resetPlayerPos();

	Engine.getUI().deactivateWidget("intro");
  Engine.getUI().deactivateWidget("skip_cinematic");
}

void CModuleIntro::update(float dt)
{
  if (EngineInput["any_key"].isPressed())
  {
    time += dt;
    if (time >= max_time)
    {
      time = 0.0f;
      EngineLua.skipIntroDialogue();
    }
  }
  else time = 0.0f;

  UI::CProgress* skipProgress = dynamic_cast<UI::CProgress*>(Engine.getUI().getWidgetByAlias("skip_progress"));
  skipProgress->setRatio(time / max_time);
}
