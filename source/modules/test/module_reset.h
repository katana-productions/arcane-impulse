#pragma once
#include "modules/module.h"

class CModuleReset : public IModule
{
public:
  CModuleReset(const std::string& name);
  bool start() override;
  void stop() override;
  void update(float dt) override;

private:
  float _timer = 3.f;
};