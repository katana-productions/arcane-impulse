#pragma once
#include "modules/module.h"

class CModuleIntro : public IModule
{
public:
	CModuleIntro(const std::string& name);
	bool start() override;
	void stop() override;
	void update(float dt) override;

private:
	float _timer = 0.f;
  float time = 0.0f;
  float max_time = 2.0f;
};