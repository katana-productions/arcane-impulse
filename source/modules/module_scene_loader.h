#pragma once

#include "modules/module.h"
#include "entity/entity.h"
#include <queue>
#include <map>

class CModuleSceneLoader : public IModule
{
private:
  std::string current_scene;
  std::map<std::string, CHandle> all_loaded_scenes;
  std::map<std::string, std::vector< CHandle > > scene_dependency;

public:
  CModuleSceneLoader(const std::string& aname) : IModule(aname) { }
  std::string getCurrentScene() { return current_scene; };
  CHandle getCurrentSceneHandle() { return all_loaded_scenes[current_scene]; };
  void setCurrentScene(const std::string& s) { current_scene = s; };
  bool isSceneLoaded(const std::string& s);
  void addSceneToMap(const std::string& s, CHandle h_scene);
  void addSceneDependency(const std::string& s, CHandle h_dep);
  bool loadSceneBlock(const std::string& s);
  void loadSceneNoBlock(const std::string& s);
  void unloadSceneNoBlock(const std::string& s);
  bool unloadScene(const std::string& s);
  bool start() override;
  void stop() override;
  void renderInMenu() override;
};
