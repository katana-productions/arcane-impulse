#include "mcv_platform.h"
#include "module_worker.h"

#include <time.h>


void processJobs(std::queue<Job> &jobs)
{
  while (true)
  {
    while (!jobs.empty())
    {
      Job job = jobs.front();
      jobs.pop();
      job.exec();
      job.isDone = true;
    }
  }
}

void CModuleBkgWorker::addJob(std::function<void()>& f, bool thread_safe)
{
  Job j;
  j.exec = f;
  if (thread_safe)
    parallel_worker_jobs.push(j);
  else
    pending_jobs.push(j);
}

bool CModuleBkgWorker::start()
{
  t = std::thread(&processJobs, std::ref(parallel_worker_jobs));
  return true;
}

void CModuleBkgWorker::update(float delta)
{
  // Push our pending jobs to the worker
  while (!pending_jobs.empty())
  {
    Job job = pending_jobs.front();
    pending_jobs.pop();
    main_worker_jobs.push(job);
  }

  // Execute the jobs, if we didn't take too much time, execute more
  const clock_t begin_time = clock();
  float secs = float(clock() - begin_time) / CLOCKS_PER_SEC;

  while (secs < delta and !main_worker_jobs.empty())
  {
    Job job = main_worker_jobs.front();
    job.exec();
    job.isDone = true;
    secs = float(clock() - begin_time) / CLOCKS_PER_SEC;
    main_worker_jobs.pop();
  }
}

void CModuleBkgWorker::stop()
{
}

void CModuleBkgWorker::renderInMenu()
{
  ImGui::Text("Number of pending jobs: %d", pending_jobs.size());
  ImGui::Text("Number of main thread jobs: %d", main_worker_jobs.size());
  ImGui::Text("Number of parallel thread jobs: %d", parallel_worker_jobs.size());
}
