#include "mcv_platform.h"
#include "module_decals.h"
#include "render/meshes/mesh_instanced.h"

void renderMATT44InMenu(MAT44& world) {
  VEC3 scale, trans;
  QUAT rot;
  world.Decompose(scale, rot, trans);
  CTransform tmx;
  tmx.setRotation(rot);
  tmx.setPosition(trans);
  tmx.setScale(scale.x);
  if (tmx.renderInMenu())
    world = tmx.asMatrix();
}

CModuleDecals::CModuleDecals(const std::string& name)
  : IModule(name)
{}

bool CModuleDecals::start() {
  // Instances groups might be useful?
  /*{
    sphere_instances_mesh = (CMeshInstanced*) Resources.get("data/meshes/figures_GeoSphere002.instanced_mesh")->as<CMesh>();
    // Split instances in two groups: 4 + 6
    VMeshGroups groups;
    groups.resize(2);
    groups[0].first_idx = 0;
    groups[0].num_indices = 4;
    groups[1].first_idx = 4;
    groups[1].num_indices = 6;
    sphere_instances_mesh->setGroups(groups);
    sphere_instances.resize(10);
    for (int i = 0; i < (int)sphere_instances.size(); ++i)
      sphere_instances[i].world = MAT44::CreateTranslation(VEC3((float)i * 6.0f, (float)i, -10.0f));
  }*/

  // -----------------------
  /*{
    auto rmesh = Resources.get("data/meshes/blood.instanced_mesh")->as<CMesh>();
    blood_instances_mesh = (CMeshInstanced*)rmesh;
  }*/

  return true;
}

void CModuleDecals::update(float dt)
{
  // Update the ttls
  for (auto it1 = info_by_name.begin(), next_it = it1; it1 != info_by_name.end(); it1 = next_it)
  {
    ++next_it;
    if (it1->second.ttl != -1.0f)
    {
      it1->second.ttl -= dt;
      if (it1->second.ttl <= 0.0f)
      {
        deleteDecal(it1->first);
      }
    }
  }

  // Update GPU meshes
  for (auto it2 = decal_types.begin(); it2 != decal_types.end(); ++it2)
  {
    std::string type = it2->first;
    it2->second->setInstancesData(gpu_info_by_type[type].data(), gpu_info_by_type[type].size(), sizeof(TGPUInfo));
  }
}

void CModuleDecals::renderInMenu() {
  /*if (ImGui::TreeNode("Sphere Instances")) {
    for (auto& p : sphere_instances) {
      ImGui::PushID(&p);
      renderMATT44InMenu(p.world);
      ImGui::PopID();
    }
    ImGui::TreePop();
  }

  // ----------------------------------------------
  if (ImGui::TreeNode("Decals")) {
    static int num = 3;
    static float sz = 30.0f;
    ImGui::DragFloat("Size", &sz, 0.01f, -50.f, 50.f);
    ImGui::DragInt("Num", &num, 0.1f, 1, 10);
    ImGui::Text("Num Instances: %ld / %ld", blood_instances.size(), blood_instances.capacity());
    if (ImGui::Button("Add")) {
      for (int i = 0; i < num; ++i) {
        TInstanceBlood new_instance;
        new_instance.world =
          MAT44::CreateRotationY(randomFloat((float)-M_PI, (float)M_PI))
          *
          MAT44::CreateScale(randomFloat(2.f, 10.f))
          *
          MAT44::CreateTranslation(VEC3(randomFloat(-sz, sz), 0, randomFloat(-sz, sz)));
        new_instance.color.x = unitRandom();
        new_instance.color.y = unitRandom();
        new_instance.color.z = 1 - new_instance.color.x - new_instance.color.y;
        if (new_instance.color.z < 0.f) new_instance.color.z = 0.0f;
        new_instance.color.w = 1;
        blood_instances.push_back(new_instance);
      }
    }
    ImGui::SameLine();
    if (ImGui::Button("Del") && !blood_instances.empty())
      blood_instances.pop_back();
    if (ImGui::TreeNode("Instances")) {
      for (auto& p : blood_instances) {
        ImGui::PushID(&p);
        renderMATT44InMenu(p.world);
        ImGui::ColorEdit4("Color", &p.color.x);
        ImGui::PopID();
      }
      ImGui::TreePop();
    }
    ImGui::TreePop();
  }*/
}

void CModuleDecals::renderDebug()
{

}

void CModuleDecals::addDecal(std::string name, std::string type, VEC3 pos, QUAT rotation, float scale, VEC4 color, float ttl)
{
  // Add new decal type if needed
  if (decal_types.find(type) == decal_types.end())
  {
    auto rmesh = Resources.get("data/decals/" + type + ".instanced_mesh")->as<CMesh>();
    assert(type != "");
    assert(rmesh);
    decal_types[type] = (CMeshInstanced*)rmesh;
  }

  // Add new instance gpu info to the map containing all gpu info of that type of instance
  TGPUInfo gpu_info;
  gpu_info.world =
    MAT44::CreateFromQuaternion(rotation)
    *
    MAT44::CreateScale(scale)
    *
    MAT44::CreateTranslation(pos);
  gpu_info.color = color;

  gpu_info_by_type[type].push_back(gpu_info);

  // Add new instance to the map containing all instances by name
  TInstanceInfo info;
  info.name = name;
  info.type = type;
  info.ttl = ttl;
  info.gpu_info_pos = gpu_info_by_type[type].size() - 1;

  info_by_name[name] = info;
}

void CModuleDecals::deleteDecal(std::string name)
{
  if (info_by_name.find(name) == info_by_name.end())
    return;

  TInstanceInfo info = info_by_name[name];

  // Delete the gpu info
  gpu_info_by_type[info.type].erase(gpu_info_by_type[info.type].begin() + info.gpu_info_pos);

  // Delete the type if no more instances of that type
  if (gpu_info_by_type[info.type].size() == 0)
  {
    gpu_info_by_type.erase(info.type);
    decal_types.erase(info.type);
  }

  // Finally delete info of instance
  info_by_name.erase(name);

  // Delete the entity too
  CHandle e = getEntityByName(name);
  if (e.isValid())
    e.destroy();
}

void CModuleDecals::modifyDecal(std::string name, VEC3 pos, QUAT rotation, float scale)
{
  if (info_by_name.find(name) == info_by_name.end())
    return;

  TInstanceInfo info = info_by_name[name];
  gpu_info_by_type[info.type][info.gpu_info_pos].world =
    MAT44::CreateFromQuaternion(rotation)
    *
    MAT44::CreateScale(scale)
    *
    MAT44::CreateTranslation(pos);
}

void CModuleDecals::modifyDecal(std::string name, VEC4 color)
{
  if (info_by_name.find(name) == info_by_name.end())
    return;

  TInstanceInfo info = info_by_name[name];
  gpu_info_by_type[info.type][info.gpu_info_pos].color = color;
}
