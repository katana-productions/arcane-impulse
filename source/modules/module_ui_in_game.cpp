#include "mcv_platform.h"
#include "module_ui_in_game.h"
#include "engine.h"
#include "input/input.h"
#include "ui/module_ui.h"
#include "ui/widgets/ui_progress.h"
#include "ui/widgets/ui_button.h"
#include "components/player/comp_ui.h"


CModuleUiInGame::CModuleUiInGame(const std::string& name)
	: IModule(name)
{}

bool CModuleUiInGame::start()
{
	UI::CModuleUI& ui = Engine.getUI();
	ui.activateWidget("ui_in_game");
	EngineLua.setUIVisible(true);
  //ui.activateWidget("subtitles");

	EnginePersistence.musicGame = EngineAudio.playEvent("event:/Music/MusicGameplay", 0);
	EnginePersistence.musicGame.setPaused(true);
	EnginePersistence.musicGame.setVolume(0.15f);
	EnginePersistence.musicGame.setPaused(false);

	return true;
}

void CModuleUiInGame::stop()
{
	Engine.getUI().deactivateWidget("ui_in_game");
	//Engine.getUI().deactivateWidget("subtitles");
}

