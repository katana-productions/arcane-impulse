#include "mcv_platform.h"
#include "module_pre_intro.h"
#include "engine.h"
#include "input/input.h"
#include "ui/module_ui.h"
#include "ui/widgets/ui_progress.h"
#include "ui/controllers/ui_menu_controller.h"
#include "ui/widgets/ui_button.h"
#include "components/player/comp_ui.h"
#include "audio/SoundEvent.h"


CModulePreIntro::CModulePreIntro(const std::string& name)
	: IModule(name)
{}

bool CModulePreIntro::start()
{
	UI::CModuleUI& ui = Engine.getUI();

	ui.activateWidget("pre_intro");

	EngineLua.launchEvent(12);

	return true;
}

void CModulePreIntro::stop()
{
	Engine.getUI().deactivateWidget("pre_intro");
}

void CModulePreIntro::update(float dt)
{
}
