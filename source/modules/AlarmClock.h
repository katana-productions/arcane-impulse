#pragma once

#include "modules/module.h"

class CAlarmClock : public IModule {
public:
	CAlarmClock(const std::string& name);

	bool start() override;
	void stop() override;
	void update(float delta) override;
	void renderDebug() override;
	void renderInMenu() override;

	void AddAlarm(float alarmT, int id, CHandle alarmReciever);
	void DeleteAlarms(const int& id, CHandle handleOfEmisor);

private:
	float globalTime = 0.0f;
	struct Alarm
	{
		float alarmT;
		int id;
		CHandle alarmReciever;
	};
	std::list<Alarm> alarmList;
	CEntity* currEntity;
};