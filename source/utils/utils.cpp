#include "mcv_platform.h"
#include <fstream>
#include "utils.h"
#include "file_context.h"
#include <time.h> 
#include "windows/app.h"
#include "components/ai/bt_mage.h"

#define E_ROUND(f) (f * 100) / 100

TElapsedTime Time;
std::vector< const std::string* > TFileContext::files;

bool isPressed(int key)
{
  return ::GetAsyncKeyState(key) & 0x8000;
}

void dbg(const char* format, ...) {
  va_list argptr;
  va_start(argptr, format);
  char dest[1024 * 16];
  _vsnprintf(dest, sizeof(dest), format, argptr);
  va_end(argptr);
  ::OutputDebugString(dest);
}

bool fatal(const char* format, ...) {
  va_list argptr;
  va_start(argptr, format);
  char dest[1024 * 16];
  _vsnprintf(dest, sizeof(dest), format, argptr);
  va_end(argptr);
  ::OutputDebugString(dest);

  std::string file_context = TFileContext::getFileContextStack();
  if (!file_context.empty()) {
    strcat(dest, "Files context:\n");
    strcat(dest, file_context.c_str());
  }

  if (MessageBox(nullptr, dest, "Error!", MB_RETRYCANCEL) == IDCANCEL)
    exit(-1);
  return false;
}

// --------------------------------------------------------
json loadJson(const std::string& filename) {
  PROFILE_FUNCTION_COPY_TEXT(filename.c_str());

  json j;

  while (true) {

    std::ifstream ifs(filename.c_str());
    if (!ifs.is_open()) {
      fatal("Failed to open json file %s\n", filename.c_str());
      continue;
    }

#ifdef NDEBUG

    j = json::parse(ifs, nullptr, false);
    if (j.is_discarded()) {
      ifs.close();
      fatal("Failed to parse json file %s\n", filename.c_str());
      continue;
    }

#else

    try
    {
      // parsing input with a syntax error
      j = json::parse(ifs);
    }
    catch (json::parse_error& e)
    {
      ifs.close();
      // output exception information
      fatal("Failed to parse json file %s\n%s\nAt offset: %d\n"
        , filename.c_str(), e.what(), e.byte);
      continue;
    }

#endif

    // The json is correct, we can leave the while loop
    break;
  }

  return j;
}


// generate a hash from the input buffer
uint32_t getID(const void* buff, size_t nbytes) {
  uint32_t seed = 0;
  uint32_t out_value = 0;
  MurmurHash3_x86_32(buff, (uint32_t)nbytes, seed, &out_value);
  assert(out_value != 0);
  return out_value;
}

uint32_t getID(const char* txt) {
  return getID(txt, strlen(txt));
}

bool fileExists(const char* filename) {
  FILE *f = fopen(filename, "rb");
  if (!f)
    return false;
  fclose(f);
  return true;
}

void toEulerAngle(const QUAT& q, float& roll, float& pitch, float& yaw)
{
	// roll (x-axis rotation)
	float sinr_cosp = +2.0 * (q.w * q.x + q.y * q.z);
	float cosr_cosp = +1.0 - 2.0 * (q.x * q.x + q.y * q.y);
	roll = atan2(sinr_cosp, cosr_cosp);

	// pitch (y-axis rotation)
	float sinp = +2.0 * (q.w * q.y - q.z * q.x);
	if (fabs(sinp) >= 1)
		pitch = copysign(M_PI / 2, sinp); // use 90 degrees if out of range
	else
		pitch = asin(sinp);

	// yaw (z-axis rotation)
	float siny_cosp = +2.0 * (q.w * q.z + q.x * q.y);
	float cosy_cosp = +1.0 - 2.0 * (q.y * q.y + q.z * q.z);
	yaw = atan2(siny_cosp, cosy_cosp);
}

float magnitude(VEC2 v)
{
	return sqrt(pow(v.x, 2) + pow(v.y, 2));
}

float magnitude(VEC3 v)
{
	return sqrt(pow(v.x, 2) + pow(v.y, 2) + pow(v.z, 2));
}

float manhattanDistance(VEC2 v1, VEC2 v2)
{
	return std::abs(v2.x - v1.x) + std::abs(v2.y - v1.y);
}

float manhattanDistance(VEC3 v1, VEC3 v2)
{
	return std::abs(v2.x - v1.x) + std::abs(v2.y - v1.y) + std::abs(v2.z - v1.z);
}

float euclideanDistance(VEC2 v1, VEC2 v2)
{
	VEC2 v = v2 - v1;
	return magnitude(v);
}

float euclideanDistance(VEC3 v1, VEC3 v2)
{
	VEC3 v = v2 - v1;
	return magnitude(v);
}

float randomValue() {
	static bool first = true;
	if (first)
	{
		srand(time(NULL));
		first = false;
	}
	return static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
}

float negPosUnitRandom() {
	return (randomValue() * 2.0f) - 1.0f;
}

float randomRange(const float& min, const float& max) {
	static bool first = true;
	if (first)
	{
		srand(time(NULL));
		first = false;
	}
	return min + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (max - min)));
}

int randomInt(int min, int max)
{
	static bool first = true;
	if (first)
	{
		srand(time(NULL));
		first = false;
	}
	return min + rand() % ((max + 1) - min);
}

float randomFloat(float vmin, float vmax) {
	return vmin + (vmax - vmin) * randomValue();
}

float clampFloat(const float& value, const float& min, const float& max)
{
	return std::max(min, std::min(value, max));
}

float clampFloat01(const float & value)
{
	return std::max(0.0f, std::min(value, 1.0f));
}

float truncFloat(float value, int decimals) {
	value *= pow(10,decimals);
	value = trunc(value);
	return value /= pow(10, decimals);
}

float lerpFloat(float a, float b, float f)
{
	return a + f * (b - a);
}

float moveTowardsFloat(float a, float b, float f)
{
	if (abs(b - a) <= f)
	{
		return b;
	}
	return a + (1 * (b - a)) * f;
}

int strnicmp(const char * str1, const char * str2, int n)
{
  int d = 0;
  while (n > 0 && (d = toupper(*str2) - toupper(*str1)) == 0 && *str1) {
    str1++; str2++; n--;
  }
  return d;
}

std::string stringify(VEC2 pos) {

	return std::to_string(E_ROUND(pos.x)) + " " +
		std::to_string(E_ROUND(pos.y));
}


std::string stringify(VEC3 pos) {

	return std::to_string(E_ROUND(pos.x)) + " " +
		std::to_string(E_ROUND(pos.y)) + " " +
		std::to_string(E_ROUND(pos.z));
}

std::string stringify(VEC4 pos) {

	return std::to_string(E_ROUND(pos.x)) + " " +
		std::to_string(E_ROUND(pos.y)) + " " +
		std::to_string(E_ROUND(pos.z)) + " " +
		std::to_string(E_ROUND(pos.w));
}

const char* stringToCharArray(const std::string& s)
{
	return s.c_str();
}

bool isPointInRectangle(VEC2 point, VEC2 rectangle_size, VEC2 rectangle_position) {
	VEC2 point2d = point - rectangle_position;
	return point2d.x >= 0.f && point2d.x <= rectangle_size.x && point2d.y >= 0.f && point2d.y <= rectangle_size.y;
}

bool isSurface(CHandle targetHandle)
{
  CEntity *targetEnt = targetHandle;
  TCompCollider *targetCol = targetEnt->get<TCompCollider>();
  physx::PxCapsuleController* char_controller = targetCol->controller;
  physx::PxRigidDynamic *targetRb = targetCol->actor->is<physx::PxRigidDynamic>();

  if (targetRb == nullptr or targetRb->getRigidBodyFlags().isSet(physx::PxRigidBodyFlag::eKINEMATIC) and char_controller == nullptr) return true;
  else return false;
}

bool isObject(CHandle targetHandle)
{
  CEntity *targetEnt = targetHandle;
  TCompCollider *targetCol = targetEnt->get<TCompCollider>();
  physx::PxRigidDynamic *targetRb = targetCol->actor->is<physx::PxRigidDynamic>();
  TCompEnemyStats *targetEnemy = targetEnt->get<TCompEnemyStats>();

  if (targetRb != nullptr and (!targetRb->getRigidBodyFlags().isSet(physx::PxRigidBodyFlag::eKINEMATIC) 
	  or targetCol->getAwakePower()) and targetEnemy == nullptr) return true;
  else return false;
}

bool isEnemy(CHandle targetHandle)
{
  CEntity *targetEnt = targetHandle;
  TCompEnemyStats *targetEnemy = targetEnt->get<TCompEnemyStats>();
  bt_mage* isMage = targetEnt->get<bt_mage>();

  if (targetEnemy != nullptr && !isMage) return true;
  else return false;
}

bool isFireball(CHandle targetHandle)
{
  CEntity *targetEnt = targetHandle;
  TCompFireballController *fireball = targetEnt->get<TCompFireballController>();

  if (fireball != nullptr) return true;
  else return false;
}

bool isInteractable(CHandle targetHandle)
{
  CEntity *targetEnt = targetHandle;
  TCompName *targetName = targetEnt->get<TCompName>();

  if (targetName->getTag() == Tag::Interactable) return true;
  else return false;
}

VEC2 getMouseInWindow() {
	POINT cursorPos;
	GetCursorPos(&cursorPos);

	ScreenToClient(CApplication::get().getHandle(), &cursorPos);
	return VEC2(cursorPos.x, cursorPos.y);
}

bool pointInRectangle(VEC2 point, VEC2 rectangle_size, VEC2 rectangle_position) {
	VEC2 point2d = point - rectangle_position;
	return point2d.x >= 0.f && point2d.x <= rectangle_size.x && point2d.y >= 0.f && point2d.y <= rectangle_size.y;
}

void dbgFloat(float f, int id)
{
	dbg("%d: %f, T:%f\n", id, f, Time.current);
}

void dbgString(std::string s, int id)
{
	dbg("%d: %s, T:%f\n", id, s.c_str(), Time.current);
}

void dbgVEC2(VEC2 vec, int id)
{
	dbg("%d: %f, %f  , T:%f\n", id, vec.x, vec.y, Time.current);
}

void dbgVEC3(VEC3 vec, int id)
{
	dbg("%d: %f, %f, %f  , T:%f\n", id, vec.x, vec.y, vec.z, Time.current);
}

void dbgVEC4(VEC4 vec, int id)
{
	dbg("%d: %f, %f, %f, %f  , T:%f\n", id, vec.x, vec.y, vec.z, vec.w, Time.current);
}