#pragma once

#include "data_provider.h"

// -----------------------------------------
#include "murmur3/murmur3.h"
uint32_t getID(const char* txt);
uint32_t getID(const void* buff, size_t nbytes);

bool isPressed(int key);

json loadJson(const std::string& filename);

bool fatal(const char* fmt, ...);
bool fileExists(const char* filename);

//MATH-------------------------------------------
void toEulerAngle(const QUAT& q, float& roll, float& pitch, float& yaw);
float magnitude(VEC2 v);
float magnitude(VEC3 v);
float manhattanDistance(VEC2 v1, VEC2 v2);
float manhattanDistance(VEC3 v1, VEC3 v2);
float euclideanDistance(VEC2 v1, VEC2 v2);
float euclideanDistance(VEC3 v1, VEC3 v2);
float randomValue();
float negPosUnitRandom();
float randomRange(const float& min, const float& max);
int randomInt(int min, int max);
float randomFloat(float vmin, float vmax);
float clampFloat(const float& value, const float& min, const float& max);
float clampFloat01(const float& value);
float truncFloat(float value, int decimals);
float lerpFloat(float a, float b, float f);
float moveTowardsFloat(float a, float b, float f);
template <typename T>
T OurClamp(const T& n, const T& lower, const T& upper) {
	return std::max(lower, std::min(n, upper));
}

//STRINGS-------------------------------------------
int strnicmp(const char * str1, const char * str2, int n);
std::string stringify(VEC2 pos);
std::string stringify(VEC3 pos);
std::string stringify(VEC4 pos);
const char* stringToCharArray(const std::string& s);

bool isPointInRectangle(VEC2 point, VEC2 rectangle_size, VEC2 rectangle_position);

#include "named_values.h"
#include "utils/time.h"
#include "utils/file_context.h"

//OTHER--------------------------------------------
#include "render/meshes/mesh.h"
#include "entity/entity.h"
#include "components/common/comp_name.h"
#include "components/common/comp_collider.h"
#include "components/common/comp_fireball.h"
#include "components/ai/comp_enemy_stats.h"

bool isSurface(CHandle targetHdl);
bool isObject(CHandle targetHdl);
bool isEnemy(CHandle targetHdl);
bool isFireball(CHandle targetHdl);
bool isInteractable(CHandle targetHdl);
VEC2 getMouseInWindow();
bool pointInRectangle(VEC2 point, VEC2 rectangle_size, VEC2 rectangle_position);

//DEBUG-------------------------------------------
void dbg(const char* fmt, ...);
void dbgFloat(float f, int id);
void dbgString(std::string s, int id);
void dbgVEC2(VEC2 vec, int id);
void dbgVEC3(VEC3 vec, int id);
void dbgVEC4(VEC4 vec, int id);