#include "mcv_platform.h"
#include "comp_bone_tracker.h"
#include "components/common/comp_transform.h"
#include "skeleton/comp_skeleton.h"
#include "cal3d/cal3d.h"
#include "cal3d2engine.h"
#include "entity/entity_parser.h"

DECL_OBJ_MANAGER("bone_tracker", TCompBoneTracker);

void TCompBoneTracker::load(const json& j, TEntityParseContext& ctx) {
  bone_name   = j.value("bone", "");
  parent_name = j.value("parent", "");
  if (j.count("offsetPosition")) offsetPosition = loadVEC3(j, "offsetPosition");
  if (j.count("offsetRotation")) offsetRotation = loadVEC4(j, "offsetRotation");
  offsetRotation.Normalize();

  assert(!bone_name.empty());
  assert(!parent_name.empty());

  CEntity* e_parent = ctx.findEntityByName(parent_name);
  if (e_parent) 
    h_skeleton = e_parent->get<TCompSkeleton>();
}

void TCompBoneTracker::update(float dt) {
  TCompSkeleton* c_skel = h_skeleton;

  if (c_skel == nullptr) {
    // Search the parent entity by name
    CEntity* e_entity = getEntityByName(parent_name);
    if (!e_entity)
      return;
    // The entity I'm tracking should have an skeleton component.
    h_skeleton = e_entity->get<TCompSkeleton>();
    assert(h_skeleton.isValid());
    c_skel = h_skeleton;
  }

  if (bone_id == -1) {
    bone_id = c_skel->model->getCoreModel()->getCoreSkeleton()->getCoreBoneId(bone_name);
    // The skeleton don't have the bone with name 'bone_name'
    assert(bone_id != -1);
  }
  
  // Access to the bone 'bone_id' of the skeleton
  auto cal_bone = c_skel->model->getSkeleton()->getBone(bone_id);
  QUAT rot = Cal2DX(cal_bone->getRotationAbsolute());
  VEC3 pos = Cal2DX(cal_bone->getTranslationAbsolute());

  MAT44 mtx;
  mtx = MAT44::CreateFromQuaternion(rot) * MAT44::CreateTranslation(pos);


  //Apply offset
  pos += mtx.Left() * offsetPosition.x  + mtx.Up() * offsetPosition.y +  mtx.Forward()  * offsetPosition.z;
  rot = Quaternion::Concatenate(rot, offsetRotation);


  // Apply the cal3d pos&rot to my entity owner
  TCompTransform* tmx = get<TCompTransform>();
  if (!tmx) return;
  tmx->setPosition(pos);
  tmx->setRotation(rot);
}

void TCompBoneTracker::debugInMenu() {
	ImGui::Separator();
	bool changed = false;
	ImGui::DragFloat("OffsetPosition: x", &offsetPosition.x, 0.01f, -10.f, 10.f);
	ImGui::DragFloat("OffsetPosition: y", &offsetPosition.y, 0.01f, -10.f, 10.f);
	ImGui::DragFloat("OffsetPosition: z", &offsetPosition.z, 0.01f, -10.f, 10.f);
	ImGui::Separator();
	changed |= ImGui::DragFloat("OffsetRotation: x", &offsetRotation.x, 0.01f, -1.f, 1.f);
	changed |= ImGui::DragFloat("OffsetRotation: y", &offsetRotation.y, 0.01f, -1.f, 1.f);
	changed |= ImGui::DragFloat("OffsetRotation: z", &offsetRotation.z, 0.01f, -1.f, 1.f);
	changed |= ImGui::DragFloat("OffsetRotation: w", &offsetRotation.w, 0.01f, -1.f, 1.f);
	if (changed) offsetRotation.Normalize();
	ImGui::Separator();

}
