#include "mcv_platform.h"
#include "rigid_animation.h"
#include "utils/json_resource.h"

class CRigidAnimationResourceType : public CResourceType {
public:
  const char* getExtension(int idx) const override { return "rigid_anim"; }
  const char* getName() const override {
    return "Rigid Animation";
  }
  IResource* create(const std::string& name) const override {
    CRigidAnimation* new_res = new CRigidAnimation();
    bool is_ok = new_res->create(name);
    if (!is_ok)
      return nullptr;
    new_res->setNameAndType(name, this);
    return new_res;
  }
};

template<>
const CResourceType* getResourceTypeFor<CRigidAnimation>() {
  static CRigidAnimationResourceType resource_type;
  return &resource_type;
}

// ----------------------------------------------------------
bool CRigidAnimation::create(const std::string& name) {
  json j = loadJson(name);
  return create(j);
}

// ----------------------------------------------------------
bool CRigidAnimation::create(const json& j) {

	if (j.count("positions")) {
		json jpositions = j["positions"];
		VEC3 v;
		for (json::iterator it = jpositions.begin(); it != jpositions.end(); ++it) {
			auto val = it.value().get<std::string>();
			sscanf(val.c_str(), "%f %f %f", &v.x, &v.y, &v.z);
			positions.push_back(v);
		}

	}
  return true;
}

void CRigidAnimation::onFileChanged(const std::string& filename) {
  if (filename == getName())
    create(filename);
}



void CRigidAnimation::renderInMenu() {
  /*category_names.debugInMenu("Category", category);
  ImGui::DragInt("Priority", &priority, 0.2f, 1, 200);
  ((CTechnique*)tech)->renderInMenu();
  ImGui::LabelText("Tech", "%s", tech->getName().c_str());

  for (int i = 0; i < max_textures; ++i) {
    const CTexture* t = textures[i];
    ImGui::LabelText(texture_slot_names.nth(i)->name, "%s", t->getName().c_str());
    if (t)
      ((CTexture*)t)->renderInMenu();
  }
  */

}

