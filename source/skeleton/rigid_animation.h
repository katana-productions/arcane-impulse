#pragma once

class CRigidAnimation : public IResource {
	bool loop;
	int total_frames;
	std::vector <VEC3> positions;
	std::vector<VEC3> rotations;
	std::vector<float> scales;
public:
	bool create(const json& j);
	bool create(const std::string& filename);
	void renderInMenu() override;
	void onFileChanged(const std::string& filename);

	bool enabled;

};
