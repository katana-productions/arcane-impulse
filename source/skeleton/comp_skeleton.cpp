#include "mcv_platform.h"
#include "comp_skeleton.h"
#include "cal3d/cal3d.h"
#include "game_core_skeleton.h"
#include "render/primitives.h"      // for the max_skeletons_ctes
#include "components/common/comp_transform.h"
#include "render/primitives.h"      // for the max_skeletons_ctes

// Changed name from skeleton to force to be parsed before the comp_mesh
DECL_OBJ_MANAGER("armature", TCompSkeleton);

// ---------------------------------------------------------------------------------------
// Cal2DX conversions, VEC3 are the same, QUAT must change the sign of w
CalVector DX2Cal(VEC3 p) {
  return CalVector(p.x, p.y, p.z);
}
CalQuaternion DX2Cal(QUAT q) {
  return CalQuaternion(q.x, q.y, q.z, -q.w);
}
VEC3 Cal2DX(CalVector p) {
  return VEC3(p.x, p.y, p.z);
}
QUAT Cal2DX(CalQuaternion q) {
  return QUAT(q.x, q.y, q.z, -q.w);
}
MAT44 Cal2DX(CalVector trans, CalQuaternion rot) {
  return
    MAT44::CreateFromQuaternion(Cal2DX(rot))
    * MAT44::CreateTranslation(Cal2DX(trans))
    ;
}

// ---------------------------------------------------------------------------------------
void TCompSkeleton::load(const json& j, TEntityParseContext& ctx) {

  std::string src = j["src"];
  auto core = Resources.get(src)->as<CGameCoreSkeleton>();
  int startCycleAnimation = 0;

  model = new CalModel((CalCoreModel*)core);
  model->getMixer()->blendCycle(startCycleAnimation, 1.0f, 0.f);
  lastIDCycleAnimation = startCycleAnimation;

  // Do a time zero update just to have the bones in a correct place
  model->update(0.f);
}

TCompSkeleton::TCompSkeleton() 
: cb_bones(CTE_BUFFER_SLOT_SKIN_BONES )
{
  bool is_ok = cb_bones.create("Bones");
  assert(is_ok);
}

TCompSkeleton::~TCompSkeleton() {
  if (model)
    delete model;
  cb_bones.destroy();
  model = nullptr;
}

void TCompSkeleton::update(float dt) {
  PROFILE_FUNCTION("updateSkel");
  assert(model);
  TCompTransform* tmx = get<TCompTransform>();
  if (!tmx)
	  return;

  VEC3 pos = tmx->getPosition();
  QUAT rot = tmx->getRotation();
  model->getMixer()->setWorldTransform(DX2Cal(pos), DX2Cal(rot));
  model->update(dt);
  VEC3 root_motion = Cal2DX(model->getMixer()->getAndClearDeltaRootMotion());
  tmx->setPosition(pos + root_motion);
}

//Insert an Animation Cycle and clear the others
void TCompSkeleton::playCycleAnimation(const char* animation_name, float weight, float delay) {
	auto mixer = model->getMixer();
	auto core = (CGameCoreSkeleton*)model->getCoreModel();

	for (auto a : mixer->getAnimationCycle()) {
		int id = core->getCoreAnimationId(a->getCoreAnimation()->getName());
		mixer->clearCycle(id, delay);
	}

	lastIDCycleAnimation = core->getCoreAnimationId(animation_name);
	mixer->blendCycle(lastIDCycleAnimation, weight, delay);
	mixer->setAnimationTime(0.0f);
}

void TCompSkeleton::blendCycleAnimations(const char* first_animation_name, const char* second_animation_name, float delay) {
	auto mixer = model->getMixer();
	auto core = (CGameCoreSkeleton*)model->getCoreModel();

	for (auto a : mixer->getAnimationCycle()) {
		int id = core->getCoreAnimationId(a->getCoreAnimation()->getName());
		mixer->clearCycle(id, delay);
	}

	firstBlendCycleAnimation = core->getCoreAnimationId(first_animation_name);
	mixer->blendCycle(firstBlendCycleAnimation, 1.0f, delay);
	secondBlendCycleAnimation = core->getCoreAnimationId(second_animation_name);
	mixer->blendCycle(secondBlendCycleAnimation, 1.0f, delay);
}

void TCompSkeleton::playActionAnimation(const char* animation_name, float in_delay, float out_delay, float weight, bool auto_lock) {
	auto core = (CGameCoreSkeleton*)model->getCoreModel();
	int id = core->getCoreAnimationId(animation_name);
	model->getMixer()->executeAction(id, in_delay, out_delay, weight, auto_lock);
}

void TCompSkeleton::setWeight(const char* first_animation_name, const char* second_animation_name, float weight) {
	weight = clampFloat(weight, 0.0f, 1.0f);

	auto mixer = model->getMixer();
	auto core = (CGameCoreSkeleton*)model->getCoreModel();

	firstBlendCycleAnimation = core->getCoreAnimationId(first_animation_name);
	mixer->blendCycle(firstBlendCycleAnimation, (1.0f- weight), 1.0f);
	secondBlendCycleAnimation = core->getCoreAnimationId(second_animation_name);
	mixer->blendCycle(secondBlendCycleAnimation, weight, 1.0f);
}

void TCompSkeleton::debugInMenu() {
  static int anim_id = 0;
  static float in_delay = 0.3f;
  static float out_delay = 0.3f;
  static bool auto_lock = false;

  // Play action/cycle from the menu
  ImGui::DragInt("Anim Id", &anim_id, 0.1f, 0, model->getCoreModel()->getCoreAnimationCount() - 1);
  auto core_anim = model->getCoreModel()->getCoreAnimation(anim_id);
  if (core_anim)
    ImGui::Text("%s", core_anim->getName().c_str());
  ImGui::DragFloat("In Delay", &in_delay, 0.01f, 0, 1.f);
  ImGui::DragFloat("Out Delay", &out_delay, 0.01f, 0, 1.f);
  ImGui::Checkbox("Auto lock", &auto_lock);
  if (ImGui::SmallButton("As Cycle")) {
    model->getMixer()->blendCycle(anim_id, 1.0f, in_delay);
  }
  if (ImGui::SmallButton("As Action")) {
    model->getMixer()->executeAction(anim_id, in_delay, out_delay, 1.0f, auto_lock);
  }

  // Dump Mixer
  auto mixer = model->getMixer();
  for (auto a : mixer->getAnimationActionList()) {
    ImGui::Text("Action %s S:%d W:%1.2f Time:%1.4f/%1.4f"
      , a->getCoreAnimation()->getName().c_str()
      , a->getState()
      , a->getWeight()
      , a->getTime()
      , a->getCoreAnimation()->getDuration()
    );
    ImGui::SameLine();
    if (ImGui::SmallButton("X")) {
      auto core = (CGameCoreSkeleton*)model->getCoreModel();
      int id = core->getCoreAnimationId(a->getCoreAnimation()->getName());
      if (a->getState() == CalAnimation::State::STATE_STOPPED)
        mixer->removeAction(id);
      else
        a->remove(out_delay);
    }
  }

  for (auto a : mixer->getAnimationCycle()) {
    ImGui::PushID(a);
    ImGui::Text("Cycle %s S:%d W:%1.2f Time:%1.4f"
      , a->getCoreAnimation()->getName().c_str()
      , a->getState()
      , a->getWeight()
      , a->getCoreAnimation()->getDuration()
    );
    ImGui::SameLine();
    if (ImGui::SmallButton("X")) {
      auto core = (CGameCoreSkeleton*)model->getCoreModel();
      int id = core->getCoreAnimationId(a->getCoreAnimation()->getName());
      mixer->clearCycle(id, out_delay);
    }
    ImGui::PopID();
  }

  if (mixer->getAnimationCycle().size() == 2) {
    static bool unit_weight = false;
    ImGui::Checkbox("Unit Weight", &unit_weight);
    if (unit_weight) {
      static float w1 = 0.5f;
      auto anim1 = mixer->getAnimationCycle().front();
      auto anim2 = mixer->getAnimationCycle().back();
      float wsmall = 1e-3f;
      if (ImGui::DragFloat("Weight", &w1, 0.005f, wsmall, 1.0f - wsmall)) {
        int anim1_id = model->getCoreModel()->getCoreAnimationId(anim1->getCoreAnimation()->getName());
        int anim2_id = model->getCoreModel()->getCoreAnimationId(anim2->getCoreAnimation()->getName());
        model->getMixer()->blendCycle(anim1_id, w1, 0.1f);
        model->getMixer()->blendCycle(anim2_id, 1.0f-w1, 0.1f);
      }
    }
  }

  // Show Skeleton Resource
  if (ImGui::TreeNode("Core")) {
    auto core_skel = (CGameCoreSkeleton*)model->getCoreModel();
    if (core_skel)
      core_skel->renderInMenu();
    ImGui::TreePop();
  }

}

void TCompSkeleton::renderDebug() {
  assert(model);

  VEC3 lines[MAX_SUPPORTED_BONES][2];
  int nrLines = model->getSkeleton()->getBoneLines(&lines[0][0].x);
  TCompTransform* transform = get<TCompTransform>();
  float scale = transform->getScale();
  for (int currLine = 0; currLine < nrLines; currLine++)
    drawLine(lines[currLine][0] * scale, lines[currLine][1] * scale, VEC4(1, 1, 1, 1));

  // Show list of bones
  auto mesh = Resources.get("axis.mesh")->as<CMesh>();
  auto core = (CGameCoreSkeleton*)model->getCoreModel();
  auto& bones_to_debug = core->bone_ids_to_debug;
  for (auto it : bones_to_debug) {
    CalBone* cal_bone = model->getSkeleton()->getBone(it);
    QUAT rot = Cal2DX(cal_bone->getRotationAbsolute());
    VEC3 pos = Cal2DX(cal_bone->getTranslationAbsolute());
    MAT44 mtx;
    mtx = MAT44::CreateFromQuaternion(rot) * MAT44::CreateTranslation(pos);
    drawMesh(mesh, mtx, VEC4(1, 1, 1, 1));
  }
}

void TCompSkeleton::updateCtesBones() {
  PROFILE_FUNCTION("updateCtesBones");

  // Pointer to the first float of the array of matrices
  float* fout = &cb_bones.Bones[0]._11;

  CalSkeleton* skel = model->getSkeleton();
  auto& cal_bones = skel->getVectorBone();
  assert(cal_bones.size() < MAX_SUPPORTED_BONES);

  // For each bone from the cal model
  for (size_t bone_idx = 0; bone_idx < cal_bones.size(); ++bone_idx) {
    CalBone* bone = cal_bones[bone_idx];

    const CalMatrix& cal_mtx = bone->getTransformMatrix();
    const CalVector& cal_pos = bone->getTranslationBoneSpace();

    *fout++ = cal_mtx.dxdx;
    *fout++ = cal_mtx.dydx;
    *fout++ = cal_mtx.dzdx;
    *fout++ = 0.f;
    *fout++ = cal_mtx.dxdy;
    *fout++ = cal_mtx.dydy;
    *fout++ = cal_mtx.dzdy;
    *fout++ = 0.f;
    *fout++ = cal_mtx.dxdz;
    *fout++ = cal_mtx.dydz;
    *fout++ = cal_mtx.dzdz;
    *fout++ = 0.f;
    *fout++ = cal_pos.x;
    *fout++ = cal_pos.y;
    *fout++ = cal_pos.z;
    *fout++ = 1.f;
  }

  cb_bones.updateGPU();
}


