#ifndef INC_COMPONENT_SKELETON_H_
#define INC_COMPONENT_SKELETON_H_

#include "geometry/geometry.h"
#include "components/common/comp_base.h"
#include "entity/entity.h"

class CGameCoreSkeleton;
class CalModel;

struct TCompSkeleton : public TCompBase {

  TCompSkeleton();
  ~TCompSkeleton();

  CalModel*                 model = nullptr;
  CCteBuffer<TCteSkinBones> cb_bones;

  void updateCtesBones();
  void renderDebug();
  void debugInMenu();
  void update(float dt);
  void load(const json& j, TEntityParseContext& ctx);

  DECL_SIBLING_ACCESS();

public:
	void playCycleAnimation(const char* animation_name, float weight, float delay);
	void blendCycleAnimations(const char* first_animation_name, const char* second_animation_name, float delay);
	void playActionAnimation(const char* animation_name, float in_delay, float out_delay, float weight, bool auto_lock = false);
	void setWeight(const char* first_animation_name, const char* second_animation_name, float weight);

private:
	int lastIDCycleAnimation;

	int firstBlendCycleAnimation;
	int secondBlendCycleAnimation;
};


#endif
