#ifndef INC_COMMON_ENTITY_MSGS_
#define INC_COMMON_ENTITY_MSGS_

#include "msgs.h"
#include "geometry/transform.h"

/***** BULLETS ******/
struct TMsgAssignBulletOwner {
  CHandle h_owner;
  VEC3    source;
  VEC3    front;
  DECL_MSG_ID();
};
/********************/

//ParticleEditor
struct TMsgParticleEditorInfo 
{
	json jsonTCtesParticles;
	DECL_MSG_ID();
};

/**** CONTROLLER ****/
struct TMsgOnControllerHit
{
	CHandle h_controller;
	CHandle h_other;
	DECL_MSG_ID();
};

struct TMsgDestroy
{
	CHandle h_object;
	DECL_MSG_ID();
};

struct TMsgDamage {
  CHandle h_sender;
  CHandle h_bullet;
  float damage;
  VEC3 vel;
  VEC3 force;
  DECL_MSG_ID();
};

struct TMsgHeal {
  float hp;
  DECL_MSG_ID();
};

struct TMsgExplosion
{
	DECL_MSG_ID();
};
/********************/

/******** LOGIC ********/
struct TMsgDetectPlayer
{
	DECL_MSG_ID();
};

struct TMsgMagicProtection
{
	bool isProtected;
	DECL_MSG_ID();
};

struct TMsgPanic
{
	DECL_MSG_ID();
};

struct TMsgOnAddedCrown
{
	int idx;
	DECL_MSG_ID();
};

/********************/

/******** UI ********/
struct TMsgInitUI {
	float max_health;
	float pull_cooldown;
  float push_cooldown;
	DECL_MSG_ID();
};

struct TMsgChangeUIStats {
	float health;
  bool pull_used;
  bool push_used;
	DECL_MSG_ID();
};

struct TMsgMessageUI {
  std::string message;
  bool show;
  DECL_MSG_ID();
};
/********************/

/***** WIN/LOSE *****/
struct TMsgLoseState {
  DECL_MSG_ID();
};

struct TMsgWinState {
  DECL_MSG_ID();
};
/********************/

/****** ALARMS ******/
struct TMsgAlarmClock {
  int id;
  DECL_MSG_ID();
};
/********************/

/***** CONTACT *****/
struct TMsgOnContact {
	CHandle source;
	VEC3 pos;
	VEC3 vel;
	VEC3 normal;
	DECL_MSG_ID();
};

struct TMsgOnLostContact {
  CHandle source;
  VEC3 pos;
  DECL_MSG_ID();
};
/********************/

/****** PUZZLES *****/
struct TMsgPuzzleTriggerEntered {
  CHandle h_trigger;
  CHandle h_entity;
  DECL_MSG_ID();
};

struct TMsgPuzzleTriggerExited {
  CHandle h_trigger;
  CHandle h_entity;
  DECL_MSG_ID();
};

struct TMsgEntityCall {
  bool solved;
  DECL_MSG_ID();
};

/********************/

/***** COLLIDER *****/
struct TMsgEntityTriggerEnter {
  CHandle h_entity;
  DECL_MSG_ID();
};

struct TMsgEntityTriggerExit {
  CHandle h_entity;
  DECL_MSG_ID();
};
/********************/

/* INTERACTABLE STATE */
enum ObjState
{
  Normal,   // Defined as no interaction, gravity on
  Pulled,   // Obj is pulled by player, gravity off
  Pushed,    // Obj is pushed by player until it contacts another obj, gravity on
  Pieces,    // Obj pieces
};
struct TMsgIntObjState {
  ObjState state;
  DECL_MSG_ID();
};
/********************/

/***** ENTITIES *****/
// Sent to all entities from a parsed file once all the entities
// in that file has been created. Used to link entities between them
struct TEntityParseContext;
struct TMsgEntitiesGroupCreated {
  const TEntityParseContext& ctx;
  DECL_MSG_ID();
};

struct TMsgEntityCreated {
  CHandle h_entity;
  DECL_MSG_ID();
};

struct TMsgDefineLocalAABB {
  AABB* aabb = nullptr;
  DECL_MSG_ID();
};

struct TMsgSetVisible {
  bool visible;
  DECL_MSG_ID();
};

struct TMsgSceneCreated {
  CHandle h_scene;
  CHandle h_entity;
  DECL_MSG_ID();
};
#endif

