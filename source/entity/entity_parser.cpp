#include "mcv_platform.h"
#include "handle/handle.h"
#include "entity.h"
#include "entity_parser.h"
#include "common_msgs.h"
#include "utils/json_resource.h"
#include "components/common/comp_group.h"
#include "modules/module_scene_loader.h"
#include "render/module_gpu_culling.h"
#include "engine.h"
#include <queue>

TEntityParseContext common_parse_context;
std::queue< CHandle > instanced_handles;

// Find in the current list of entities created, the first entity matching
// the given name
CHandle TEntityParseContext::findEntityByName(const std::string& name) const {

  // Search linearly in the list of entity currently loaded
  for (auto h : entities_loaded) {
    CEntity* e = h;
    if (e->getName() == name)
      return h;
  }

  // Delegate it to my parent
  if (parent)
    return parent->findEntityByName(name);

  return getEntityByName(name);
}

TEntityParseContext::TEntityParseContext(TEntityParseContext& another_ctx, const CTransform& delta_transform) {
  parent = &another_ctx;
  recursion_level = another_ctx.recursion_level + 1;
  entity_starting_the_parse = another_ctx.entity_starting_the_parse;
  root_transform = another_ctx.root_transform.combineWith(delta_transform);
  //VEC3 p = root_transform.getPosition(); float y, pitch; root_transform.getYawPitchRoll(&y, &pitch);
  //dbg("New root transform is Pos:%f %f %f Yaw: %f\n", p.x, p.y, p.z, rad2deg(y));
}


bool loadEntity(const json& j_item, TEntityParseContext& ctx)
{
  assert(j_item.is_object());

  if (j_item.count("entity")) {
    auto& j_entity = j_item["entity"];

    CHandle h_e;

    // Do we have the prefab key in the json?
    if (j_entity.count("prefab")) {

      // Get the src/id of the prefab
      std::string prefab_src = j_entity["prefab"];
      assert(!prefab_src.empty());

      // Get delta transform where we should instantiate this transform
      CTransform delta_transform;
      if (j_entity.count("transform"))
        delta_transform.load(j_entity["transform"]);

      // Parse the prefab, if any other child is created they will inherit our ctx transform
      TEntityParseContext prefab_ctx(ctx, delta_transform);
      prefab_ctx.parsing_prefab = true;
      if (!parseSceneRecursive(prefab_src, prefab_ctx))
        return false;

      assert(!prefab_ctx.entities_loaded.empty());

      // Create a new fresh entity
      h_e = prefab_ctx.entities_loaded[0];

      // Cast to entity object
      CEntity* e = h_e;

      // We give an option to 'reload' the prefab by modifying existing components, 
      // like changing the name, add other components, etc, but we don't want to parse again 
      // the comp_transform, because it was already parsed as part of the root
      // As the json is const as it's a resouce, we make a copy of the prefab section and
      // remove the transform
      json j_entity_without_transform = j_entity;
      j_entity_without_transform.erase("transform");

      // Do the parse now outside the 'prefab' context
      prefab_ctx.parsing_prefab = false;
      e->load(j_entity_without_transform, prefab_ctx);

      // TODO: erase this, emergency fix for milestone 3
      sendInitMsg(prefab_ctx);

    }
    else {

      // Create a new fresh entity
      h_e.create< CEntity >();

      // Cast to entity object
      CEntity* e = h_e;

      // Do the parse
      e->load(j_entity, ctx);

    }

    ctx.entities_loaded.push_back(h_e);
  }
  return true;
}



bool partialLoadEntities(const std::string& filename, int i, int max)
{
  TEntityParseContext ctx = common_parse_context;
  if (i == 0)
  {
    EngineGpuCulling.clearReferenceBuffer();

    ctx.filename = filename;
    EngineSceneLoader.setCurrentScene(filename);
  }

  const json& j_scene = Resources.get(filename)->as<CJson>()->getJson();
  assert(j_scene.is_array());

  int j;
  int count = 0;

  // For each item in the array...
  for (j = i; j < j_scene.size() and count < max; ++j)
  {
    auto j_item = j_scene[j];
    bool b = loadEntity(j_item, ctx);
    if (!b) return false;
    ++count;
  }

  // store the common context for next iteration
  common_parse_context = ctx;

  if (j == j_scene.size())
  {
    createGroup(ctx);
    sendGroupMsg(ctx);

    EngineSceneLoader.addSceneToMap(filename, ctx.entities_loaded[0]);
    // Build Navmesh if there's any
    //EngineNavMesh.buildNavMesh();

    sendInitMsg(ctx);
    TMsgSceneCreated msg;
    msg.h_scene = ctx.entities_loaded[0];

    // Add pending instanced handles as dependencies of this scene if there are any
    while (!instanced_handles.empty())
    {
      CHandle h = instanced_handles.front();
      EngineSceneLoader.addSceneDependency(filename, h);
      instanced_handles.pop();

      // Activate instanced handle too
      msg.h_entity = h;
      h.sendMsg(msg);
    }

    // Delete common context
    setCommonCtx();
  }
  return true;
}

bool partialUnloadEntities(const std::string& filename, int i, int max)
{
  TEntityParseContext ctx = common_parse_context;
  if (i == 0)
  {
    EngineGpuCulling.clearReferenceBuffer();

    ctx.filename = filename;
  }

  const json& j_scene = Resources.get(filename)->as<CJson>()->getJson();
  assert(j_scene.is_array());

  int j;
  int count = 0;

  // For each item in the array...
  for (j = i; j < j_scene.size() and count < max; ++j)
  {
    auto j_item = j_scene[j];
    bool b = unloadEntity(j_item, ctx);
    if (!b) return false;
    ++count;
  }

	common_parse_context = ctx;

  return true;
}

bool unloadEntity(const json& j_item, TEntityParseContext& ctx) {
	assert(j_item.is_object());

	if (j_item.count("entity")) {
		auto& j_entity = j_item["entity"];

		if (j_entity.count("name")) {

			// Get the src/id of the prefab
			auto& j_name = j_entity["name"];
			if (j_name.size() >= 2) {
				auto& j_name2 = j_entity["name"];
				auto& name = j_name2["entityName"];

				CEntity* ent = getEntityByName(name);
				if (ent) {
					CHandle h(ent);
					h.destroy();
				}
			}
			else {
				CEntity* ent = getEntityByName(j_name);
				if (ent) {
					CHandle h(ent);
					h.destroy();
				}
			}

			//CHandleManager::destroyAllPendingObjects();

			return true;
		}
	}
}

bool parseSceneRecursive(const std::string& filename, TEntityParseContext& ctx)
{
  ctx.filename = filename;

  const json& j_scene = Resources.get(filename)->as<CJson>()->getJson();
  assert(j_scene.is_array());

  // For each item in the array...
  for (int i = 0; i < j_scene.size(); ++i)
  {
    auto j_item = j_scene[i];
    bool b = loadEntity(j_item, ctx);
    if (!b) return false;
  }

  createGroup(ctx);
  sendGroupMsg(ctx);

  return true;
}

bool parseScene(const std::string& filename, TEntityParseContext& ctx)
{
  EngineGpuCulling.clearReferenceBuffer();

  ctx.filename = filename;
  EngineSceneLoader.setCurrentScene(filename);

  const json& j_scene = Resources.get(filename)->as<CJson>()->getJson();
  assert(j_scene.is_array());

  // For each item in the array...
  for (int i = 0; i < j_scene.size(); ++i) 
  {
    auto j_item = j_scene[i];
    bool b = loadEntity(j_item, ctx);
    if (!b) return false;
  }

  createGroup(ctx);
  sendGroupMsg(ctx);

  EngineSceneLoader.addSceneToMap(filename, ctx.entities_loaded[0]);
  // Build Navmesh if there's any
  //EngineNavMesh.buildNavMesh();

  sendInitMsg(ctx);
  TMsgSceneCreated msg;
  msg.h_scene = ctx.entities_loaded[0];

  // Add pending instanced handles as dependencies of this scene if there are any
  while (!instanced_handles.empty())
  {
    CHandle h = instanced_handles.front();
    EngineSceneLoader.addSceneDependency(filename, h);
    instanced_handles.pop();

    // Activate instanced handle too
    msg.h_entity = h;
    h.sendMsg(msg);
  }

  return true;
}

// Used to instantiate something and add it to the current scene. Don't use this for big files, consider making another scene then
bool instantiateAtCurrentScene(const std::string& filename, TEntityParseContext& ctx)
{
  ctx.filename = filename;

  const json& j_scene = Resources.get(filename)->as<CJson>()->getJson();
  assert(j_scene.is_array());

  // For each item in the array...
  for (int i = 0; i < j_scene.size(); ++i)
  {
    auto j_item = j_scene[i];
    bool b = loadEntity(j_item, ctx);
    if (!b) return false;
  }

  createGroup(ctx);
  sendGroupMsg(ctx);
  
  // Add the group as a scene dependency to the current scene
  std::string scene = EngineSceneLoader.getCurrentScene();

  if (EngineSceneLoader.isSceneLoaded(scene))
  {
    EngineSceneLoader.addSceneDependency(scene, ctx.entities_loaded[0]);

    // Activate all instances
    sendInitMsg(ctx);
  }
  // The scene might not have finished loading, then just put the handles in a queue to be added later
  else
  {
    for (size_t i = 0; i < ctx.entities_loaded.size(); ++i)
      instanced_handles.push(ctx.entities_loaded[i]);
  }
  return true;
}

bool instantiateEntityAtCurrentSceneFromJson(const json & j, const std::string& filename, TEntityParseContext& ctx)
{
	ctx.filename = filename;

	const json& j_scene = j;

	assert(j_scene.is_array());

	// For each item in the array...
	for (int i = 0; i < j_scene.size(); ++i)
	{
		auto j_item = j_scene[i];
		bool b = loadEntity(j_item, ctx);
		if (!b) return false;
	}

	createGroup(ctx);
	sendGroupMsg(ctx);

	// Add the group as a scene dependency to the current scene
	std::string scene = EngineSceneLoader.getCurrentScene();

	if (EngineSceneLoader.isSceneLoaded(scene))
	{
		EngineSceneLoader.addSceneDependency(scene, ctx.entities_loaded[0]);

		// Activate all instances
		sendInitMsg(ctx);
	}
	// The scene might not have finished loading, then just put the handles in a queue to be added later
	else
	{
		for (size_t i = 0; i < ctx.entities_loaded.size(); ++i)
			instanced_handles.push(ctx.entities_loaded[i]);
	}
	return true;
}

int getJsonSize(const std::string& filename)
{
  const json& j_scene = Resources.get(filename)->as<CJson>()->getJson();
  assert(j_scene.is_array());
  return j_scene.size();
}

void createGroup(TEntityParseContext& ctx)
{
  // Create a comp_group automatically if there is more than one entity
  if (ctx.entities_loaded.size() > 1) {
    // The first entity becomes the head of the group. He is NOT in the group
    CHandle h_root_of_group = ctx.entities_loaded[0];
    CEntity* e_root_of_group = h_root_of_group;
    assert(e_root_of_group);
    // Create a new instance of the TCompGroup
    CHandle h_group = getObjectManager<TCompGroup>()->createHandle();
    // Add it to the entity
    e_root_of_group->set(h_group.getType(), h_group);
    // Now add the rest of entities created to the group, starting at 1 because 0 is the head
    TCompGroup* c_group = h_group;
    for (size_t i = 1; i < ctx.entities_loaded.size(); ++i)
      c_group->add(ctx.entities_loaded[i]);
  }
}

void sendGroupMsg(TEntityParseContext& ctx)
{
  // Notify each entity created that we have finished processing this file
  TMsgEntitiesGroupCreated msg = { ctx };
  for (auto h : ctx.entities_loaded)
    h.sendMsg(msg);
}

void sendInitMsg(TEntityParseContext& ctx)
{
  // Notify each entity created that we have finished processing this file
  TMsgSceneCreated msg;
  msg.h_scene = ctx.entities_loaded[0];
  for (auto h : ctx.entities_loaded)
  {
    msg.h_entity = h;
    h.sendMsg(msg);
  }
}

void setCommonCtx()
{
  common_parse_context = TEntityParseContext();
}
