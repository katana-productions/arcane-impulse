#pragma once

#include "modules/module.h"
#include "render/deferred_renderer.h"

class CModuleRender : public IModule
{
  VEC4 clear_color = VEC4(0.142f, 0.142f, 0.142f, 1);
  void renderEntities();

  CHandle h_camera;
  CCamera camera;
  CCamera camera_ortho;
  bool debug_cam = false;
  bool beginTrace = false;
  float config_exposure = 0.0f;
  float config_ambient = 0.0f;

  CDeferredRenderer* deferred = nullptr;
  CRenderToTexture* deferred_output = nullptr;

  void uploadSkinMatricesToGPU();
  void parsePipelines(const std::string& filename);
  bool setupDeferredOutput();

public:
  CModuleRender(const std::string& name);
  bool start() override;
  void stop() override;
  void renderInMenu() override;
  void switchDebugCam();
  void update(float dt) override;

  bool isDebug() { return debug_cam; }
  void beginImgui();
  void endImgui();
  void generateFrame();
  void onResolutionUpdated();

  float globalAmbient = 0.2f;
  float globalExposure = 1.0f;
};
