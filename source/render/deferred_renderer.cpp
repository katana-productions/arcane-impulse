#include "mcv_platform.h"
#include "deferred_renderer.h"
#include "render_manager.h"
#include "render_utils.h"
#include "resources/resource.h"
#include "render/primitives.h"
#include "components/lightning/comp_light_dir.h"
#include "components/lightning/comp_light_point.h"
#include "components/lightning/comp_light_point_shadows.h"
#include "components/postfx/comp_render_ao.h"
#include "render/intel/ASSAO.h"

CDeferredRenderer::CDeferredRenderer() {
	dbg("Creating CDeferredRenderer\n");
}


void CDeferredRenderer::renderGBuffer() {
	CGpuScope gpu_scope("Deferred.GBuffer");

	// Disable the gbuffer textures as we are going to update them
	// Can't render to those textures and have them active in some slot...
	CTexture::setNullTexture(TS_DEFERRED_ALBEDOS);
	CTexture::setNullTexture(TS_DEFERRED_NORMALS);
	CTexture::setNullTexture(TS_DEFERRED_LINEAR_DEPTH);
	CTexture::setNullTexture(TS_DEFERRED_EMISSIVE);
	CTexture::setNullTexture(TS_DEFERRED_OUTLINES);
	CTexture::setNullTexture(TS_DEFERRED_DISTORSIONS);
	CTexture::setNullTexture(TS_DEFERRED_OUTLINEMASK);

	// Activate el multi-render-target MRT
	//This are the SV_TargetsN of the PBR PS!!!
	const int nrender_targets = 7;
	ID3D11RenderTargetView* rts[nrender_targets] = {
	  rt_albedos->getRenderTargetView(),
	  rt_normals->getRenderTargetView(),
	  rt_depth->getRenderTargetView(),
	  rt_emissive->getRenderTargetView(),
	  rt_outlines->getRenderTargetView(),
	  rt_distorsions->getRenderTargetView(),
	  rt_outlineMask->getRenderTargetView()
	};

	// We use our 3 rt's and the Zbuffer of the backbuffer
	Render.ctx->OMSetRenderTargets(nrender_targets, rts, Render.depth_stencil_view);
	rt_albedos->activateViewport();   // Any rt will do...

	// Clear output buffers, some can be removed if we intend to fill all the screen
	// with new data.
	rt_albedos->clear(VEC4(0, 0, 0, 1));
	rt_normals->clear(VEC4(0, 0, 1, 1));
	rt_depth->clear(VEC4(1, 1, 1, 1));
	rt_emissive->clear(VEC4(0, 0, 0, 0));
	rt_outlines->clear(VEC4(0, 0, 0, 1));
	rt_distorsions->clear(VEC4(0, 0, 0, 1));
	rt_outlineMask->clear(VEC4(0, 0, 0, 1));

	// Clear ZBuffer with the value 1.0 (far)
	Render.ctx->ClearDepthStencilView(Render.depth_stencil_view, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	// Render the solid objects that output to the G-Buffer
	CRenderManager::get().render(eRenderCategory::CATEGORY_SOLIDS);

	// Disable rendering to all render targets.
	ID3D11RenderTargetView* rt_nulls[nrender_targets];
	for (int i = 0; i < nrender_targets; ++i) rt_nulls[i] = nullptr;
	Render.ctx->OMSetRenderTargets(nrender_targets, rt_nulls, nullptr);

	// Activate the gbuffer textures to other shaders
	rt_albedos->activate(TS_DEFERRED_ALBEDOS);
	rt_normals->activate(TS_DEFERRED_NORMALS);
	rt_depth->activate(TS_DEFERRED_LINEAR_DEPTH);
	rt_emissive->activate(TS_DEFERRED_EMISSIVE);
	rt_outlines->activate(TS_DEFERRED_OUTLINES);
	rt_distorsions->activate(TS_DEFERRED_DISTORSIONS);
	rt_outlineMask->activate(TS_DEFERRED_OUTLINEMASK);
}

void CDeferredRenderer::renderGBufferTransparentsWrite() {
	CGpuScope gpu_scope("Deferred.Transparents.GBuffer");

	// Disable the gbuffer textures as we are going to update them
	// Can't render to those textures and have them active in some slot...
	CTexture::setNullTexture(TS_DEFERRED_ACC_LIGHTS);
	CTexture::setNullTexture(TS_DEFERRED_NORMALS);
	CTexture::setNullTexture(TS_DEFERRED_LINEAR_DEPTH);
	CTexture::setNullTexture(TS_DEFERRED_EMISSIVE);
	CTexture::setNullTexture(TS_DEFERRED_OUTLINES);
	CTexture::setNullTexture(TS_DEFERRED_DISTORSIONS);
	CTexture::setNullTexture(TS_DEFERRED_OUTLINEMASK);

	// Activate el multi-render-target MRT
	//This are the SV_TargetsN of the PBR PS!!!
	const int nrender_targets = 7;
	ID3D11RenderTargetView* rts[nrender_targets] = {
	  rt_acc_light->getRenderTargetView(),
	  rt_normals->getRenderTargetView(),
	  rt_depth->getRenderTargetView(),
	  rt_emissive->getRenderTargetView(),
	  rt_outlines->getRenderTargetView(),
	  rt_distorsions->getRenderTargetView(),
	  rt_outlineMask->getRenderTargetView()
	};

	// We use our 3 rt's and the Zbuffer of the backbuffer
	Render.ctx->OMSetRenderTargets(nrender_targets, rts, Render.depth_stencil_view);
	rt_albedos->activateViewport();   // Any rt will do...

	CRenderManager::get().render(eRenderCategory::CATEGORY_TRANSPARENTS);

	// Disable rendering to all render targets.
	ID3D11RenderTargetView* rt_nulls[nrender_targets];
	for (int i = 0; i < nrender_targets; ++i) rt_nulls[i] = nullptr;
	Render.ctx->OMSetRenderTargets(nrender_targets, rt_nulls, nullptr);

	// Activate the gbuffer textures to other shaders
	rt_albedos->activate(TS_DEFERRED_ACC_LIGHTS);
	rt_normals->activate(TS_DEFERRED_NORMALS);
	rt_depth->activate(TS_DEFERRED_LINEAR_DEPTH);
	rt_emissive->activate(TS_DEFERRED_EMISSIVE);
	rt_outlines->activate(TS_DEFERRED_OUTLINES);
	rt_distorsions->activate(TS_DEFERRED_DISTORSIONS);
	rt_outlineMask->activate(TS_DEFERRED_OUTLINEMASK);
}

void CDeferredRenderer::renderGBufferTransparentsRead() {
	CGpuScope gpu_scope("Deferred.Transparents.GBuffer");

	CTexture::setNullTexture(TS_DEFERRED_ACC_LIGHTS);

	const int nrender_targets = 1;
	ID3D11RenderTargetView* rts[nrender_targets] = {
	  rt_acc_light->getRenderTargetView()
	};

	Render.ctx->OMSetRenderTargets(nrender_targets, rts, Render.depth_stencil_view);
	rt_albedos->activateViewport();   // Any rt will do...

	CRenderManager::get().render(eRenderCategory::CATEGORY_TRANSPARENTS2);

	ID3D11RenderTargetView* rt_nulls[nrender_targets];
	for (int i = 0; i < nrender_targets; ++i) rt_nulls[i] = nullptr;
	Render.ctx->OMSetRenderTargets(nrender_targets, rt_nulls, nullptr);

	rt_albedos->activate(TS_DEFERRED_ACC_LIGHTS);
}

// --------------------------------------------------------------
void CDeferredRenderer::renderGBufferDecals() {
	CGpuScope gpu_scope("Deferred.GBuffer.Decals");

	// Disable the gbuffer textures as we are going to update them
	// Can't render to those textures and have them active in some slot...
	CTexture::setNullTexture(TS_DEFERRED_ALBEDOS);
	CTexture::setNullTexture(TS_DEFERRED_NORMALS);

	// Activate el multi-render-target MRT
	const int nrender_targets = 2;
	ID3D11RenderTargetView* rts[nrender_targets] = {
	  rt_albedos->getRenderTargetView(),
	  rt_normals->getRenderTargetView()
	  // No Z as we need to read to reconstruct the position
	};

	// We use our 3 rt's and the Zbuffer of the backbuffer
	Render.ctx->OMSetRenderTargets(nrender_targets, rts, Render.depth_stencil_view);
	rt_albedos->activateViewport();   // Any rt will do...

	// Render blending layer on top of gbuffer before adding lights
	CRenderManager::get().render(eRenderCategory::CATEGORY_DECALS);

	// Disable rendering to all render targets.
	ID3D11RenderTargetView* rt_nulls[nrender_targets];
	for (int i = 0; i < nrender_targets; ++i) rt_nulls[i] = nullptr;
	Render.ctx->OMSetRenderTargets(nrender_targets, rt_nulls, nullptr);

	// Activate the gbuffer textures to other shaders
	rt_albedos->activate(TS_DEFERRED_ALBEDOS);
	rt_normals->activate(TS_DEFERRED_NORMALS);
}

// -----------------------------------------------------------------
void CDeferredRenderer::destroy() {
	if (rt_albedos) {
		rt_albedos->destroy();
		rt_normals->destroy();
		rt_depth->destroy();
		rt_acc_light->destroy();
		rt_emissive->destroy();
		rt_outlines->destroy();
		rt_distorsions->destroy();
		rt_outlineMask->destroy();
	}

	if (assao_fx) {
		ASSAO_Effect::DestroyInstance(assao_fx);
		assao_fx = nullptr;
	}
}

// -----------------------------------------------------------------
bool CDeferredRenderer::create(int new_xres, int new_yres) {

	xres = new_xres;
	yres = new_yres;

	dbg("Initializing deferred to %dx%d\n", xres, yres);

	destroy();

	if (!rt_albedos) {
		rt_albedos = new CRenderToTexture;
		rt_normals = new CRenderToTexture;
		rt_depth = new CRenderToTexture;
		rt_acc_light = new CRenderToTexture;
		rt_emissive = new CRenderToTexture;
		rt_outlines = new CRenderToTexture;
		rt_distorsions = new CRenderToTexture;
		rt_outlineMask = new CRenderToTexture;
	}

	if (!rt_albedos->createRT("g_albedos.dds", xres, yres, DXGI_FORMAT_R8G8B8A8_UNORM))
		return false;

	if (!rt_normals->createRT("g_normals.dds", xres, yres, DXGI_FORMAT_R16G16B16A16_FLOAT))
		return false;

	if (!rt_depth->createRT("g_depths.dds", xres, yres, DXGI_FORMAT_R32_FLOAT))
		return false;

	if (!rt_acc_light->createRT("acc_light.dds", xres, yres, DXGI_FORMAT_R16G16B16A16_FLOAT, DXGI_FORMAT_UNKNOWN, true))
		return false;

	if (!rt_emissive->createRT("g_emissive.dds", xres, yres, DXGI_FORMAT_R16G16B16A16_FLOAT))
		return false;

	if (!rt_outlines->createRT("g_outlines.dds", xres, yres, DXGI_FORMAT_R8G8B8A8_UNORM))
		return false;

	if (!rt_distorsions->createRT("g_distorsions.dds", xres, yres, DXGI_FORMAT_R8G8B8A8_UNORM))
		return false;

	if (!rt_outlineMask->createRT("g_outlineMask.dds", xres, yres, DXGI_FORMAT_R8G8B8A8_UNORM))
		return false;

	if (assao_fx) {
		ASSAO_Effect::DestroyInstance(assao_fx);
		assao_fx = nullptr;
	}

	CMemoryDataProvider mdp("data/shaders/ASSAO.hlsl");
	ASSAO_CreateDescDX11 desc(Render.device, mdp.data(), mdp.size());
	assao_fx = ASSAO_Effect::CreateInstance(&desc);

	return true;
}

// -----------------------------------------------------------------

void CDeferredRenderer::renderAmbientPass() {
	CGpuScope gpu_scope("renderAmbientPass");
	drawFullScreenQuad("pbr_ambient.tech", nullptr);
}

// -------------------------------------------------------------------------
void CDeferredRenderer::renderSkyBox() const {
	CGpuScope gpu_scope("renderSkyBox");
	drawFullScreenQuad("pbr_skybox.tech", nullptr);
}

// -------------------------------------------------------------------------
void CDeferredRenderer::renderAccLight() {
	CGpuScope gpu_scope("Deferred.AccLight");
	rt_acc_light->activateRT();
	rt_acc_light->clear(VEC4(0, 0, 0, 0));
	renderAmbientPass();
	renderPointLights();
	renderDirectionalLights();
	renderPointLightsShadows();
	renderSkyBox();
}

// -------------------------------------------------------------------------
void CDeferredRenderer::renderPointLights() {
	CGpuScope gpu_scope("renderPointLights");

	// Activate tech for the light dir
	auto* tech = Resources.get("pbr_point_lights.tech")->as<CTechnique>();
	tech->activate();

	// All light directional use the same mesh
	auto* mesh = Resources.get("data/meshes/UnitSphere.mesh")->as<CMesh>();
	mesh->activate();

	// Para todas las luces... pintala
	getObjectManager<TCompLightPoint>()->forEach([mesh](TCompLightPoint* c) {

		// subir las contantes de la posicion/dir
		// activar el shadow map...
		c->activate();

		activateObject(c->getWorld(), VEC4(1, 1, 1, 1));

		// mandar a pintar una geometria que refleje los pixeles que potencialmente
		// puede iluminar esta luz.... El Frustum solido
		mesh->render();
	});
}

// -------------------------------------------------------------------------
void CDeferredRenderer::renderPointLightsShadows() {
	CGpuScope gpu_scope("renderPointShadowLights");

	// Activate tech for the light dir
	auto* tech = Resources.get("pbr_dir_lights.tech")->as<CTechnique>();
	tech->activate();

	// All light directional use the same mesh
	auto* mesh = Resources.get("unit_frustum_solid.mesh")->as<CMesh>();
	mesh->activate();

	// Para todas las luces... pintala
	getObjectManager<TCompLightPointShadows>()->forEach([mesh](TCompLightPointShadows* c) {

		// subir las contantes de la posicion/dir
		// activar el shadow map...
		// for all sides of the cube
		for (int i = 0; i < c->getNSides(); ++i)
		{
			c->activateSide(i);

			activateObject(c->getShadowView(i)->getViewProjection().Invert(), VEC4(1, 1, 1, 1));

			// mandar a pintar una geometria que refleje los pixeles que potencialmente
			// puede iluminar esta luz.... El Frustum solido
			mesh->render();
		}
	});
}

// -------------------------------------------------------------------------
void CDeferredRenderer::renderDirectionalLights() {
	CGpuScope gpu_scope("renderDirectionalLights");

	// Activate tech for the light dir
	auto* tech = Resources.get("pbr_dir_lights.tech")->as<CTechnique>();
	tech->activate();

	// All light directional use the same mesh
	auto* mesh = Resources.get("unit_frustum_solid.mesh")->as<CMesh>();
	mesh->activate();

	// Para todas las luces... pintala
	getObjectManager<TCompLightDir>()->forEach([mesh](TCompLightDir* c) {

		// subir las contantes de la posicion/dir
		// activar el shadow map...
		c->activate();

		activateObject(c->getViewProjection().Invert(), VEC4(1, 1, 1, 1));

		// mandar a pintar una geometria que refleje los pixeles que potencialmente
		// puede iluminar esta luz.... El Frustum solido
		mesh->render();
	});
}

// --------------------------------------
void CDeferredRenderer::renderAO(CHandle h_camera) const {

	CEntity* e_camera = h_camera;
	if (!e_camera)
	{
		return;
	}

	TCompRenderAO* comp_ao = e_camera->get<TCompRenderAO>();
	if (!comp_ao) {
		// As there is no comp AO, use a white texture as substitute
		const CTexture* white_texture = Resources.get("data/textures/white.dds")->as<CTexture>();
		white_texture->activate(TS_DEFERRED_AO);
		return;
	}
	// As we are going to update the RenderTarget AO
	// it can NOT be active as a texture while updating it.
	CTexture::setNullTexture(TS_DEFERRED_AO);

	auto ao = comp_ao->compute(rt_depth, rt_normals, assao_fx);
	// Activate the updated AO texture so everybody else can use it
	// Like the AccLight (Ambient pass or the debugger)
	ao->activate(TS_DEFERRED_AO);
}

// --------------------------------------
void CDeferredRenderer::render(CRenderToTexture* rt_destination, CHandle h_camera) {
	PROFILE_FUNCTION("Deferred");

	assert(rt_destination);
	assert(xres);
	assert(rt_destination->getWidth() == xres);
	assert(rt_destination->getHeight() == yres);

	renderGBuffer();
	renderGBufferDecals();
	renderAO(h_camera);

	// Do the same with the acc light
	CTexture::setNullTexture(TS_DEFERRED_ACC_LIGHTS);
	renderAccLight();

	// Render blending layer on top of gbuffer before adding lights
	renderGBufferTransparentsWrite();
	renderGBufferTransparentsRead();

	// Now dump contents to the destination buffer.
	rt_destination->activateRT();
	rt_acc_light->activate(TS_DEFERRED_ACC_LIGHTS);

	// Combine the results
	drawFullScreenQuad("gbuffer_resolve.tech", nullptr);
}