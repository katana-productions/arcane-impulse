#include "mcv_platform.h"
#include "render/render.h"
#include "module_render.h"
#include "imgui/imgui_impl_win32.h"
#include "imgui/imgui_impl_dx11.h"
#include "windows/app.h"
#include "render/primitives.h"
#include "engine.h"
#include "components/common/comp_render.h"
#include "components/common/comp_transform.h"
#include "components/common/comp_camera.h"
#include "components/lightning/comp_light_dir.h"
#include "components/lightning/comp_light_point_shadows.h"
#include "components/postfx/comp_render_blur.h"
#include "components/postfx/comp_render_focus.h"
#include "components/postfx/comp_render_bloom.h"
#include "components/postfx/comp_color_grading.h"
#include "components/postfx/comp_render_outlines.h"
#include "components/postfx/comp_render_vignette.h"
#include "components/postfx/comp_render_motionBlur.h"
#include "components/postfx/comp_render_antialias.h"
#include "components/postfx/comp_render_overlay.h"
#include "components/postfx/comp_camera_magic_outlines.h"
#include "components/postfx/comp_distorsions.h"
#include "skeleton/comp_skeleton.h"
#include "render/render.h"
#include "render/meshes/mesh.h"
#include "render/textures/material.h"
#include "render/render_manager.h"
#include "ui/module_ui.h"
#include "render/compute/compute_shader.h"
#include "input/input.h"
#include "particles/module_particles.h"

CModuleRender::CModuleRender(const std::string & name)
	: IModule(name)
{}

bool CModuleRender::start() {

	auto& app = CApplication::get();

	deferred = new CDeferredRenderer();
	deferred_output = new CRenderToTexture();

	setupDeferredOutput();

	createRenderUtils();
	parsePipelines("data/shaders/techniques.json");
	createRenderPrimitives();

	// Setup Platform/Renderer bindings
	{
		PROFILE_FUNCTION("Imgui");
		ImGui::CreateContext();
		ImGui_ImplWin32_Init(app.getHandle());
		ImGui_ImplDX11_Init(Render.device, Render.ctx);
	}

  json cfg = loadJson("data/config.json");
  const json& cfg_settings = cfg["settings"];
  config_ambient = cfg_settings.value("ambient_boost", 0.0f);
  config_exposure = cfg_settings.value("exposure", 0.0f);

	// Initial values
	ctes_shared.GlobalRenderOutput = RO_COMPLETE;
	ctes_shared.GlobalAmbientBoost = globalAmbient + config_ambient;
	ctes_shared.GlobalExposureAdjustment = globalExposure + config_exposure;


	onResolutionUpdated();
	return true;
}

void CModuleRender::onResolutionUpdated() {
	assert(deferred);
	deferred->create(Render.width, Render.height);

	if (!debug_cam) {
		h_camera = getEntityByName("CameraShaker");
	}
	else {
		h_camera = getEntityByName("Camera Debug");
	}
	if (!h_camera.isValid()) return;
	CEntity* e_camera = h_camera;

	TCompOutlines* render_outlines = e_camera->get<TCompOutlines>();
	if (render_outlines) render_outlines->CreateRT();

	TCompRenderBlur* render_blur = e_camera->get<TCompRenderBlur>();
	if (render_blur) render_blur->CreateRT();

	TCompRenderFocus* render_focus = e_camera->get<TCompRenderFocus>();
	if (render_focus) render_focus->CreateRT();

	TCompOverlay* render_overlay = e_camera->get<TCompOverlay>();
	if (render_overlay) render_overlay->CreateRT();

	TCompRenderBloom* render_bloom = e_camera->get<TCompRenderBloom>();
	if (render_bloom) render_bloom->CreateRT();

	TCompVignette* render_vignette = e_camera->get<TCompVignette>();
	if (render_vignette) render_vignette->CreateRT();

	TCompMotionBlur* render_motionBlur = e_camera->get<TCompMotionBlur>();
	if (render_motionBlur) render_motionBlur->CreateRT();

	TCompAntiAlias* render_antialias = e_camera->get<TCompAntiAlias>();
	if (render_antialias) render_antialias->CreateRT();

	TCompCameraMagicOutlines* magic_outlines = e_camera->get<TCompCameraMagicOutlines>();
	if (magic_outlines) magic_outlines->CreateRT();

	TCompDistorsions* distorisons = e_camera->get<TCompDistorsions>();
	if (magic_outlines) distorisons->CreateRT();
}

void CModuleRender::parsePipelines(const std::string& filename) {
	PROFILE_FUNCTION("parsePipelines");
	TFileContext fc(filename);

	json j = loadJson(filename);

	for (auto it : j.items()) {
		PROFILE_FUNCTION_COPY_TEXT(it.key().c_str());
		TFileContext fce(it.key());

		const json& j = it.value();

		const CResourceType* res_type = nullptr;
		IResource* new_res = nullptr;
		if (j.count("cs")) {
			res_type = getResourceTypeFor<CComputeShader>();
			CComputeShader* cs = new CComputeShader();
			if (cs->create(j))
				new_res = cs;
		}
		else {
			assert(j.count("vdecl") > 0);
			res_type = getResourceTypeFor<CTechnique>();
			CTechnique* tech = new CTechnique();
			if (tech->create(j))
				new_res = tech;
		}

		std::string suffix = std::string(".") + res_type->getExtension();
		std::string name = it.key() + suffix;

		if (new_res == nullptr) {
			fatal("Failed to create %s %s\n", res_type->getName(), name.c_str());
			continue;
		}

		// Create a new tech, configure it and register it as resource
		new_res->setNameAndType(name, res_type);
		dbg("Registering %s %s\n", res_type->getName(), name.c_str());
		Resources.registerResource(new_res);
	}
}

void CModuleRender::update(float dt) {
}

void CModuleRender::stop() {

	// Render targets are owned by the Resources obj
	deferred = nullptr;
	deferred_output = nullptr;
	//outline_data = nullptr;

	ImGui_ImplDX11_Shutdown();
	ImGui_ImplWin32_Shutdown();
	ImGui::DestroyContext();

	destroyRenderPrimitives();
	destroyRenderUtils();
}

void CModuleRender::beginImgui() {

	// Start the Dear ImGui frame
	ImGui_ImplDX11_NewFrame();
	ImGui_ImplWin32_NewFrame();
	ImGui::NewFrame();

}

// Rendering ImGui
void CModuleRender::endImgui() {
	PROFILE_FUNCTION("endImgui");
	CGpuScope gpu_scope("imgui");
	ImGui::Render();
	ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
}

void CModuleRender::renderInMenu() {

	static int nframes = 5;
	ImGui::DragInt("NumFrames To Capture", &nframes, 0.1f, 1, 20);
	beginTrace = ImGui::SmallButton("Start CPU Trace");
	if (beginTrace) {
		PROFILE_SET_NFRAMES(nframes);
		beginTrace = false;
	}

	// 1. Show the big demo window (Most of the sample code is in ImGui::ShowDemoWindow()! You can browse its code to learn more about Dear ImGui!).
	static bool show_demo_window = false;
	if (show_demo_window) ImGui::ShowDemoWindow(&show_demo_window);
	ImGui::Checkbox("Demo Window", &show_demo_window);      // Edit bools storing our window open/close state
	ImGui::ColorEdit3("clear color", (float*)&clear_color.x); // Edit 3 floats representing a color
	ImGui::Text("Application average %.3f ms/frame (%.1f FPS) %f", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate, Time.current);

	if (ImGui::TreeNode("Render Control")) {
		ImGui::DragFloat("Exposure Adjustment", &globalExposure, 0.01f, 0.1f, 32.f);
		ImGui::DragFloat("Ambient Boost", &globalAmbient, 0.01f, 0.0f, 2.f);

		// Must be in the same order as the RO_* ctes
		static const char* render_output_str =
			"Complete\0"
			"Albedo\0"        // RO_ALBEDO
			"Normals\0"
			"Normals ViewSpace\0"
			"Specular\0"
			"World Pos\0"
			"Linear Depth\0"
			"AO\0"
			"Emissive\0"
			"Outlines\0"
			"Distorsions\0"
			"Outline Mask\0"
			"\0";
		ImGui::Combo("Output", &ctes_shared.GlobalRenderOutput, render_output_str);
		ImGui::TreePop();
	}
}

void CModuleRender::renderEntities() {
	CRenderManager::get().render(eRenderCategory::CATEGORY_SOLIDS);
}

void CModuleRender::switchDebugCam() {
	debug_cam = !debug_cam;

	if (debug_cam) {
    EngineLua.setUIVisible(false);
		CHandle h_camera = getEntityByName("Camera Debug");
		EngineGpuCulling.setCamera(h_camera);
		ctes_shared.GlobalRenderOutput = 0; //COMPLETE
	}
	else {
    EngineLua.setUIVisible(true);
		CHandle h_camera = getEntityByName("CameraShaker");
		EngineGpuCulling.setCamera(h_camera);
		ctes_shared.GlobalRenderOutput = 0; //COMPLETE
	}
}

void CModuleRender::uploadSkinMatricesToGPU() {
	PROFILE_FUNCTION("uploadSkinMatricesToGPU");
	getObjectManager<TCompSkeleton>()->forEach([](TCompSkeleton* cs) {
		cs->updateCtesBones();
	});
}

// ------------------------------------------
bool CModuleRender::setupDeferredOutput() {
	assert(deferred_output);
	if (deferred_output->getWidth() != Render.width || deferred_output->getHeight() != Render.height) {
		if (!deferred_output->createRT("g_deferred_output.dds", Render.width, Render.height, DXGI_FORMAT_R16G16B16A16_FLOAT, DXGI_FORMAT_UNKNOWN, true))
			return false;
	}
	return true;
}

// ----------------------------------------------
void CModuleRender::generateFrame() {
	CGpuScope gpu_trace("Module::Render");
	PROFILE_FUNCTION("CModuleRender::generateFrame");

	ctes_shared.xScreenRes = Render.width;
	ctes_shared.yScreenRes = Render.height;
	ctes_shared.GlobalWorldTime = (float)Time.current;
	ctes_shared.GlobalDeltaTime = Time.delta;
	ctes_shared.GlobalDeltaUnscaledTime = Time.delta_unscaled;
	ctes_shared.GlobalAmbientBoost = globalAmbient + config_ambient;
	ctes_shared.GlobalExposureAdjustment = globalExposure + config_exposure;
	ctes_shared.updateGPU();

	activateObject(MAT44::Identity, VEC4(1, 1, 1, 1));

	uploadSkinMatricesToGPU();

	// Activate all the lights... but only the last one will be used!!!!
	getObjectManager<TCompLightDir>()->forEach([](TCompLightDir* cs) {
		cs->generateShadowMap();
	});

	//Activate all point lights with shadows
	getObjectManager<TCompLightPointShadows>()->forEach([](TCompLightPointShadows* cs) {
		for (int i = 0; i < cs->getNSides(); ++i)
			cs->generateShadowMapSide(i);
	});

	if (!debug_cam) {
		h_camera = getEntityByName("CameraShaker");
	}
	else {
		h_camera = getEntityByName("Camera Debug");
	}
	if (h_camera.isValid()) {
		CEntity* e_camera = h_camera;
		TCompCamera* c_camera = e_camera->get<TCompCamera>();
		assert(c_camera);
		// Here we are updating the Viewport of the given camera
		c_camera->setViewport(0, 0, Render.width, Render.height);
		// Copy current state of the given camera 
		camera = *c_camera;
		CRenderManager::get().setEntityCamera(h_camera);
	}
	else {
		camera.setViewport(0, 0, Render.width, Render.height);
	}

	activateCamera(camera, Render.width, Render.height);

	setupDeferredOutput();

	deferred->render(deferred_output, h_camera);

	CTexture* current_output = deferred_output;
	CTexture* blurred_output = deferred_output;

	CRenderManager::get().render(eRenderCategory::CATEGORY_DISTORSIONS);

	// Apply post FX effects
	CEntity* e_camera = h_camera;
	if (e_camera) {
		TCompRenderBlur* render_blur = e_camera->get<TCompRenderBlur>();
		if (render_blur) {
			blurred_output = render_blur->apply(current_output);
			blurred_output->activate(TS_ALBEDO1);
		}

		//TCompRenderFocus* render_focus = e_camera->get<TCompRenderFocus>();
		//if (render_focus && render_focus->enabled && render_blur) current_output = render_focus->apply(deferred_output, blurred_output);

		TCompOutlines* render_outlines = e_camera->get<TCompOutlines>();
		if (render_outlines) {
			CTexture* outline_maks = deferred->rt_outlineMask;
			current_output = render_outlines->apply(current_output, outline_maks);
		}

		TCompCameraMagicOutlines* magic_outlines = e_camera->get<TCompCameraMagicOutlines>();
		if (magic_outlines && render_blur) {
			CTexture* pre_blur_outlines = deferred->rt_outlines;
			CTexture* current_outlines = render_blur->apply(deferred->rt_outlines);
			current_output = magic_outlines->apply(current_output, current_outlines, pre_blur_outlines);
		}

		TCompCamera* c_camera = e_camera->get<TCompCamera>();
		TCompMotionBlur* render_motionBlur = e_camera->get<TCompMotionBlur>();
		if (render_motionBlur) current_output = render_motionBlur->apply(current_output, *c_camera);


		TCompRenderBloom* render_bloom = e_camera->get<TCompRenderBloom>();
		if (render_bloom) {
			render_bloom->generateHighlights(deferred_output);
			render_bloom->addBloom();
		}

		TCompDistorsions* distorsions = e_camera->get<TCompDistorsions>();
		if (magic_outlines) {
			CTexture* distorsion_data = deferred->rt_distorsions;
			current_output = distorsions->apply(current_output, distorsion_data);
		}

		TCompColorGrading* color_grading = e_camera->get<TCompColorGrading>();
		ctes_shared.GlobalLUTAmount = color_grading ? color_grading->getAmount() : 0.f;
		ctes_shared.updateGPU();
		if (color_grading) color_grading->lut1->activate(TS_LUT_COLOR_GRADING);

		TCompVignette* render_vignette = e_camera->get<TCompVignette>();
		if (render_vignette && render_blur) current_output = render_vignette->apply(current_output, blurred_output);

		TCompOverlay* render_overlay = e_camera->get<TCompOverlay>();
		if (render_overlay) current_output = render_overlay->apply(current_output);

		TCompAntiAlias* render_antialias = e_camera->get<TCompAntiAlias>();
		if (render_antialias) current_output = render_antialias->apply(current_output);
	}

	Render.startRenderingBackBuffer();

	assert(current_output);
	{
		drawFullScreenQuad("presentation.tech", current_output);
	}

	{
		PROFILE_FUNCTION("RenderInMenu");
		CEngine::get().getModules().renderInMenu();
		Resources.renderInMenu();
	}

	// call render method of all active modules... Probably for debug only
	if (EngineSettings.getShowDebugWindow() and debug_cam)
		CEngine::get().renderDebug();

	// Render UI section using an ortho camera
	// Vertical size is 1
	// Horizontal size is aspect ratio. Top/left is 0,0
	camera_ortho.setOrthoParams(false, 0.0f, (float)Render.width / (float)Render.height, 0.f, 1.0f, -1.0f, 1.0f);
	activateCamera(camera_ortho, Render.width, Render.height);
	CRenderManager::get().render(eRenderCategory::CATEGORY_UI);
	CEngine::get().getUI().render();
}