#ifndef INC_RENDER_DEFERRED_RENDERER_H_
#define INC_RENDER_DEFERRED_RENDERER_H_

#include "render/textures/render_to_texture.h"
#include "intel/ASSAO.h"

class CDeferredRenderer {

public:

  int               xres = 0, yres = 0;
  CRenderToTexture* rt_normals = nullptr;
  CRenderToTexture* rt_albedos = nullptr;
  CRenderToTexture* rt_depth = nullptr;
  CRenderToTexture* rt_acc_light = nullptr;
  CRenderToTexture* rt_emissive = nullptr;
  CRenderToTexture* rt_outlines = nullptr;
  CRenderToTexture* rt_distorsions = nullptr;
  CRenderToTexture* rt_outlineMask = nullptr;
  ASSAO_Effect*     assao_fx = nullptr;

  void renderGBuffer();
  void renderGBufferTransparentsWrite();
  void renderGBufferTransparentsRead();
  void renderGBufferDecals();
  void renderAccLight();
  void renderAmbientPass();
  void renderDirectionalLights();
  void renderPointLights();
  void renderPointLightsShadows();
  void renderSkyBox() const;
  void renderAO(CHandle h_camera) const;

public:

  CDeferredRenderer();
  bool create( int xres, int yres );
  void destroy();
  void render( CRenderToTexture* rt_destination, CHandle h_camera );
};


#endif

