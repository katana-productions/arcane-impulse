#include "mcv_platform.h"
#include "transform.h"

MAT44 CTransform::asMatrix() const {
	return
		MAT44::CreateScale(scale)
		* MAT44::CreateFromQuaternion(rotation)
		* MAT44::CreateTranslation(position)
		;
}

MAT44 CTransform::asRenderMatrix() const {
	return
		MAT44::CreateScale(scale)
		* MAT44::CreateFromQuaternion(rotation)
		* MAT44::CreateTranslation(position + renderOffset)
		;
}

// ---------------------------
CTransform CTransform::combineWith(const CTransform& delta_transform) const {
	CTransform new_t;
	new_t.rotation = delta_transform.rotation * rotation;

	VEC3 delta_pos_rotated = VEC3::Transform(delta_transform.position, rotation);
	new_t.position = position + (delta_pos_rotated * scale);

	new_t.scale = scale * delta_transform.scale;
	return new_t;
}


VEC3 CTransform::getFront() const {
	return -MAT44::CreateFromQuaternion(rotation).Forward();
}

VEC3 CTransform::getUp() const {
	return MAT44::CreateFromQuaternion(rotation).Up();
}

VEC3 CTransform::getLeft() const {
	return -MAT44::CreateFromQuaternion(rotation).Left();
}

VEC3 CTransform::getDown() const {
	return MAT44::CreateFromQuaternion(rotation).Down();
}

void CTransform::fromMatrix(MAT44 mtx) {
	VEC3 scale3;
	bool is_valid = mtx.Decompose(scale3, rotation, position);
	scale = scale3.x;
}

void CTransform::lookAt(VEC3 eye, VEC3 target, VEC3 up_aux) {
	position = eye;
	VEC3 front = target - eye;
	float yaw, pitch;
	vectorToYawPitch(front, &yaw, &pitch);
	setAngles(yaw, pitch, 0.f);
}

void CTransform::getAngles(float* yaw, float* pitch, float* roll) {
	VEC3 front = getFront();
	vectorToYawPitch(front, yaw, pitch);

	// If requested...
	if (roll) {
		VEC3 roll_zero_left = VEC3(0, 1, 0).Cross(getFront());
		VEC3 roll_zero_up = VEC3(getFront()).Cross(roll_zero_left);
		VEC3 my_real_left = getLeft();
		float rolled_left_on_up = my_real_left.Dot(roll_zero_up);
		float rolled_left_on_left = my_real_left.Dot(roll_zero_left);
		*roll = atan2f(rolled_left_on_up, rolled_left_on_left);
	}

}

void CTransform::setAngles(float yaw, float pitch, float roll) {
	rotation = QUAT::CreateFromYawPitchRoll(yaw, pitch, roll);
}

bool CTransform::isInFront(VEC3 p) const {
	VEC3 delta = p - position;
	return delta.Dot(getFront()) > 0.f;
}

bool CTransform::isInLeft(VEC3 p)const {
	VEC3 delta = p - position;
	return delta.Dot(getLeft()) > 0.f;
}

bool CTransform::renderInMenu() {
	VEC3 front = getFront();
	VEC3 up = getUp();
	VEC3 left = getLeft();

	bool changed_pos = ImGui::DragFloat3("Pos", &position.x, 0.025f, -50.f, 50.f);

	bool changed = false;
	float yaw, pitch, roll = 0.f;
	getAngles(&yaw, &pitch, &roll);
	float yaw_deg = rad2deg(yaw);
	if (ImGui::DragFloat("Yaw", &yaw_deg, 0.1f, -180.0f, 180.0f)) {
		changed = true;
		yaw = deg2rad(yaw_deg);
	}

	float pitch_deg = rad2deg(pitch);
	float max_pitch = 90.0f - 1e-3f;
	if (ImGui::DragFloat("Pitch", &pitch_deg, 0.1f, -max_pitch, max_pitch)) {
		changed = true;
		pitch = deg2rad(pitch_deg);
	}

	float roll_deg = rad2deg(roll);
	if (ImGui::DragFloat("Roll", &roll_deg, 0.1f, -180.0f, 180.0f)) {
		changed = true;
		roll = deg2rad(roll_deg);
	}

	if (changed)
		setAngles(yaw, pitch, roll);

	ImGui::DragFloat("Scale", &scale, 0.01f, 0.001f, 10.f);
	ImGui::LabelText("Front", "%f %f %f", front.x, front.y, front.z);
	ImGui::LabelText("Up", "%f %f %f", up.x, up.y, up.z);
	ImGui::LabelText("Left", "%f %f %f", left.x, left.y, left.z);
	ImGui::LabelText("Offset", "%f %f %f", renderOffset.x, renderOffset.y, renderOffset.z);
	return changed | changed_pos;
}

float CTransform::getDeltaYawToAimTo(VEC3 target) const {
	VEC3 dir_to_target = target - position;
	float dot_left = getLeft().Dot(dir_to_target);
	float dot_front = getFront().Dot(dir_to_target);
	return atan2f(dot_left, dot_front);
}

bool CTransform::load(const json& j) {

	if (j.count("pos"))
		position = loadVEC3(j, "pos");

	if (j.count("rotation")) {
		setRotation(loadQUAT(j, "rotation"));
	}
	else
	{
		if (j.count("yawPitchRoll")) {
			VEC3 euler = loadVEC3(j, "yawPitchRoll");
			setRotation(QUAT::CreateFromYawPitchRoll(euler.x,
				euler.y, euler.z));
		}
	}

	if (j.count("lookat"))
		lookAt(getPosition(), loadVEC3(j, "lookat"));

	if (j.count("axis")) {
		VEC3 axis = loadVEC3(j, "axis");
		float angle_deg = j.value("angle", 0.f);
		float angle_rad = deg2rad(angle_deg);
		setRotation(QUAT::CreateFromAxisAngle(axis, angle_rad));
	}
	if (j.count("scale"))
		scale = j.value("scale", 1.0f);
	return true;
}
VEC3 CTransform::getVectorFromAngle(float yaw) {
	float actual_angle = vectorToYaw(getFront());
	return yawToVector(yaw + actual_angle);
}

bool CTransform::getConeVision(VEC3 target, float degree, float distance) {
	float degree_to_target = getDeltaYawToAimTo(target);
	float distance_to_target = abs((target - position).x) + abs((target - position).y) + abs((target - position).z);
	if (distance_to_target < distance) {
		if (abs(degree_to_target) < degree)
			return true;
	}
	return false;
}

void CTransform::followPoint(VEC3 point, float speed, float rotation_speed, float dt) {
	VEC3 position = getPosition();
	float yaw, pitch;
	getAngles(&yaw, &pitch);

	if (isInLeft(point)) {
		yaw += rotation_speed * dt;
	}
	else {
		yaw -= rotation_speed * dt;
	}
	position = position + getFront() * speed * dt;

	setPosition(position);
	setAngles(yaw, pitch, 0);
}

float CTransform::manhattanDistance(VEC3 p) {
	return abs(position.x - p.x) + abs(position.z - p.z);
}

float CTransform::accurateDistance(VEC3 p)
{
	return sqrt((position.x - p.x) * (position.x - p.x)
		+ (position.y - p.y) * (position.y - p.y)
		+ (position.z - p.z) * (position.z - p.z));
}
