#pragma once

class CTransform
{
	QUAT  rotation = QUAT::Identity;
	VEC3  position = VEC3::Zero;
	float scale = 1.0f;

public:
	VEC3  renderOffset;

	CTransform() = default;
	CTransform(VEC3 new_pos, QUAT new_rot, float new_scale = 1.0f)
		: rotation(new_rot)
		, position(new_pos)
		, scale(new_scale)
	{}

	const VEC3& getPosition() const { return position; }
	const QUAT& getRotation() const { return rotation; }
	float getScale() const { return scale; }
	void setPosition(VEC3 new_position) { position = new_position; }
	void setRotation(QUAT new_rotation) { rotation = new_rotation; }
	void setScale(const float& new_scale) { 
		scale = new_scale; 
	}

	MAT44 asMatrix() const;
	MAT44 asRenderMatrix() const;
	void fromMatrix(MAT44 mtx);

  VEC3 getFront() const;
  VEC3 getUp() const;
  VEC3 getLeft() const;
  VEC3 getDown() const;

	void getAngles(float* yaw, float* pitch, float* roll = nullptr);
	void setAngles(float yaw, float pitch, float roll = 0.0f);

	bool isInFront(VEC3 p) const;
	bool isInLeft(VEC3 p)const;

	void lookAt(VEC3 eye, VEC3 target, VEC3 up_aux = VEC3(0, 1, 0));
	CTransform combineWith(const CTransform& delta_transform) const;

	float getDeltaYawToAimTo(VEC3 target) const;

	bool load(const json& j);

	bool renderInMenu();

	VEC3 getVectorFromAngle(float yaw);

	void followPoint(VEC3 point, float speed, float rotation, float dt);

	bool getConeVision(VEC3 target, float degree, float distance);

	float manhattanDistance(VEC3 p);
	float accurateDistance(VEC3 p);
};
