#pragma once

#include "mcv_platform.h"
#include "ui/ui_utils.h"
#include "engine.h"
#include "ui/module_ui.h"
#include "render/textures/texture.h"

namespace UI
{
  void renderBitmap(const MAT44& world, const CTexture* texture, const VEC2& minUV, const VEC2& maxUV, const VEC4& color, const bool& additive, const CTexture* texture2, const CTexture* texture3, const bool& isBurn, const float& burnAmount)
  {
	UI::CModuleUI& ui = Engine.getUI();


    MAT44 adjust = MAT44::CreateScale(1.920 / Render.width, 1.080 / Render.height, 1.f);

	MAT44 adjustScreen = MAT44::CreateScale(((float)Render.width / (float)Render.height) / (ui.uiResolution.x / ui.uiResolution.y) *((float)Render.width / ui.uiResolution.x), ((float)Render.height / ui.uiResolution.y), 1.f);

    activateObject(world * adjust * adjustScreen , color);
    
    ctes_shared.UIminUV = minUV;
    ctes_shared.UImaxUV = maxUV;
    ctes_shared.UItint = color;
    ctes_shared.uiBurn = burnAmount;
    ctes_shared.updateGPU();

	std::string tech_name = "ui.tech";
	if (additive)  tech_name = "ui_additive.tech";
	else if (isBurn) tech_name = "ui_burn.tech";

    auto* tech = Resources.get(tech_name)->as<CTechnique>();
    assert(tech);
    tech->activate();
    if (texture) texture->activate(TS_ALBEDO);
	if (isBurn && texture2 && texture3) {
		texture2->activate(TS_NORMAL);
		texture3->activate(TS_EMISSIVE);
	}

    auto* mesh = Resources.get("unit_plane_xy.mesh")->as<CMesh>();
    mesh->activateAndRender();
  }

  void renderText(const MAT44& transform, const CTexture* texture, const std::string& text, const VEC2& size)
  {
    // assuming texture has the characters in ASCII order
    constexpr char firstCh = ' ';
    constexpr int numRows = 12;
    constexpr int numCols = 8;
    constexpr VEC2 cellSize(1.f / numCols, 1.f / numRows);

    MAT44 world = transform;
    MAT44 tr = MAT44::CreateTranslation(size.x, 0.f, 0.f);
    int charLineCount = 0;

    for (const char& ch : text)
    {
      if (ch == '\n')
      {
        MAT44 nl = MAT44::CreateTranslation(-charLineCount * size.x, size.y, 0.f);
        world = world * nl;
        charLineCount = 0;
      }
      else
      {
        const char cellCh = ch - firstCh;
        const int row = cellCh / numCols;
        const int col = cellCh % numCols;
        const VEC2 minUV(static_cast<float>(col) / static_cast<float>(numCols), static_cast<float>(row) / static_cast<float>(numRows));
        const VEC2 maxUV = minUV + cellSize;

        renderBitmap(world, texture, minUV, maxUV);

        world = world * tr;
        ++charLineCount;
      }
    }
  }
}
