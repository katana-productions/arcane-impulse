#pragma once

#include "mcv_platform.h"
#include "ui/widgets/ui_button.h"
#include "ui/controllers/ui_menu_controller.h"
#include "engine.h"
#include "input/input.h"

namespace UI
{

	void CMenuController::update(float dt)
	{

		//Just for Debug
		//displayCursorPos();

		VEC2 mousePos = getMouseInWindow();
		if (mousePos != lastPosMouse || EngineInput["Push"].justPressed()) {
			lastPosMouse = mousePos;
			if (_currentOption != getPosInButton(mousePos)) setCurrentOption(getPosInButton(mousePos));
		}

		if ((EngineInput["menu_confirm"].justPressed() or (anyKey and EngineInput["any_key"].justPressed())) && _currentOption >= 0)
		{
			_options[_currentOption].callback();
			//Posar so tecla BUTTON_Click_Fan_Plastic_Metallic_Turn_01_stereo
			EngineAudio.playEvent("event:/Menu/ClickButton", 2);
		}
		if (EngineInput["menu_next"].justPressed())
		{
			if (_currentOption == -1)
				setCurrentOption(0);
			else if (_currentOption < _options.size() - 1)
				setCurrentOption(++_currentOption);
		}
		if (EngineInput["menu_prev"].justPressed())
		{
			if (_currentOption == -1)
				setCurrentOption(0);
			else if (_currentOption > 0)
				setCurrentOption(--_currentOption);
		}
		if (EngineInput["menu_confirm"].justPressed() && _currentOption >= 0)
		{
			_options[_currentOption].callback();
			//Posar so tecla BUTTON_Click_Fan_Plastic_Metallic_Turn_01_stereo
			EngineAudio.playEvent("event:/Menu/ClickButton", 2);
		}
	}

	void CMenuController::registerOption(CButton* button, Callback callback)
	{
		_options.emplace_back(TOption{ button, callback });
	}

	void CMenuController::setCurrentOption(int idx)
	{
		_currentOption = clamp(idx, -1, static_cast<int>(_options.size()) - 1);

		for (auto option : _options)
		{
			option.button->setCurrentState("enabled");
			cont = 0;
		}

		if (_currentOption >= 0 && _currentOption < _options.size())
		{
			_options.at(_currentOption).button->setCurrentState("selected");
			EngineAudio.playEvent("event:/Menu/OverButton", 2);
		}
	}

	int CMenuController::getPosInButton(VEC2 mouse_pos)
	{
		int nbutton = -1;
		for (int i = 0; i < _options.size(); i++) {
			VEC2 size = _options[i].button->getCurrentState()->imageParams.size;

			VEC3 bscale = VEC3::Zero;
			QUAT brot = QUAT();
			VEC3 bpos = VEC3::Zero;

			_options[i].button->getCorrection().Decompose(bscale, brot, bpos);

			VEC2 sizeCorrected = _options[i].button->getSizeCorrection();

			if (pointInRectangle(mouse_pos, sizeCorrected, VEC2(bpos.x, bpos.y))) {
				nbutton = i;
				break;
			}
		}
		return nbutton;
	}

	void CMenuController::displayCursorPos() {
		ImGuiStyle * style = &ImGui::GetStyle();
		// Main body starts here.
		if (!ImGui::Begin(""))
		{
			// Early out if the window is collapsed, as an optimization.
			ImGui::End();
			return;
		}

		std::string position = "Mouse Position: x: " + std::to_string(lastPosMouse.x) + ", " + "y: " + std::to_string(lastPosMouse.y);
		const char *c = position.c_str();
		ImGui::Text(c);
		//End code
		ImGui::End();
	}
}