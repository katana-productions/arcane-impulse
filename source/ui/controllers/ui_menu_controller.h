#pragma once

#include "mcv_platform.h"
#include "ui/ui_controller.h"

namespace UI
{
  class CButton;

  class CMenuController : public CController
  {
  public:
    using Callback = std::function<void()>;

    void update(float dt) override;

    void registerOption(CButton* button, Callback callback);
    void setCurrentOption(int idx);
	  int getPosInButton(VEC2 mouse_pos);
    void setAnyKey(bool b) { anyKey = b; };

  private:
	void displayCursorPos();
    struct TOption
    {
      CButton* button = nullptr;
      Callback callback = nullptr;
    };
    std::vector<TOption> _options;
		VEC2 lastPosMouse = VEC2(0,0);
    int _currentOption = -1;
		int cont = 0;
    bool anyKey = false;
  };
}
