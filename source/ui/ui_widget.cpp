#pragma once

#include "mcv_platform.h"
#include "ui/ui_widget.h"
#include "ui/ui_effect.h"


namespace UI
{
  void CWidget::start()
  {
    for (auto fx : _effects)
    {
      fx->start();
    }

    for (auto& child : _children)
    {
      child->start();
    }
  }

  void CWidget::stop()
  {
    for (auto fx : _effects)
    {
      fx->stop();
    }

    for (auto& child : _children)
    {
      child->stop();
    }
  }

  void CWidget::update(float dt)
  {
    for (auto fx : _effects)
    {
      fx->update(dt);
    }

    for (auto& child : _children)
    {
      child->update(dt);
    }
  }

  void CWidget::doRender()
  {
    if (!_params.visible)
    {
      return;
    }

    render();

    for (auto& child : _children)
    {
      child->doRender();
    }
  }

  void CWidget::updateTransform()
  {
    computeAbsolute();

    for (auto& child : _children)
    {
      child->updateTransform();
    }
  }

  void CWidget::computePivot()
  {
    _pivot = MAT44::Identity * MAT44::CreateTranslation(-_params.pivot.x, -_params.pivot.y, 0.f);
  }

  void CWidget::computeLocal()
  {
    computePivot();

    MAT44 tr = MAT44::CreateTranslation(_params.position.x, _params.position.y, 0.f);
    MAT44 sc = MAT44::CreateScale(_params.scale.x, _params.scale.y, 0.f);
    MAT44 rot = MAT44::CreateRotationZ(_params.rotation);
    
    _local = rot * sc * tr;
  }

  void CWidget::computeAbsolute()
  {
    computeLocal();

    _absolute = _parent ? _local * _parent->_absolute : _local;
  }

  void CWidget::setParent(CWidget* parent)
  {
    removeFromParent();

    if (!parent)
    {
      return;
    }

    _parent = parent;
    _parent->_children.push_back(this);
  }

  void CWidget::removeFromParent()
  {
    if (!_parent)
    {
      return;
    }

    auto it = std::find(_parent->_children.begin(), _parent->_children.end(), this);
    assert(it != _parent->_children.end());
    _parent->_children.erase(it);
    _parent = nullptr;
  }

  void CWidget::renderMenu()
  {
	  ImGui::Separator();
	  ImGui::Text(_name.c_str());
	  renderEditor();
	  std::string debugNameTree = " Son of: " + _name;
	  if (ImGui::TreeNode(debugNameTree.c_str())) {
		  for (auto child : _children)
		  {
			  child->renderMenu();
		  }
		  ImGui::TreePop();
	  }
  }


  void CWidget::renderEditor()
  {
	  std::string editName = "Edit " + _name;
	  if (ImGui::TreeNode(editName.c_str())) {

			bool changed = false;
			float limit = 2000;
		  TParams* w_params = getParams();

		  std::string debugName = "Visible " + _name;
		  changed |= ImGui::Checkbox(debugName.c_str(), &w_params->visible);

		  debugName = "Pos " + _name;
		  changed |= ImGui::DragFloat2(debugName.c_str(), &w_params->position.x, 0.1, -limit, limit);

		  debugName = "Scale " + _name;
		  changed |= ImGui::DragFloat2(debugName.c_str(), &w_params->scale.x, 0.1, -limit, limit);

		  debugName = "Rotation " + _name;
		  changed |= ImGui::DragFloat(debugName.c_str(), &w_params->rotation, 0.1, -limit, limit);

		  debugName = "Pivot " + _name;
		  changed |= ImGui::DragFloat2(debugName.c_str(), &w_params->pivot.x, 0.1, -limit, limit);

		  TImageParams* w_image_params = getImageParams();
		  if (w_image_params) {
			  debugName = "minUV " + _name;
			  changed |= ImGui::DragFloat2(debugName.c_str(), &w_image_params->minUV.x, 0.0001, 0, 1);

			  debugName = "maxUV " + _name;
			  changed |= ImGui::DragFloat2(debugName.c_str(), &w_image_params->maxUV.x, 0.0001, 0, 1);

			  debugName = "Size " + _name;
			  changed |= ImGui::DragFloat2(debugName.c_str(), &w_image_params->size.x, 1.0, 0, 2000);
		  }
		  if (changed) {
			  updateTransform();
		  }
		  ImGui::TreePop();
	  }
  }
}

