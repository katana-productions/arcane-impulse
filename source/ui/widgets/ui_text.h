#pragma once

#include "ui/ui_widget.h"

namespace UI
{
  class CText : public CWidget
  {
  public:
    void render() override;
    void setText(std::string t);
    void setText(int sub_line);
    void setLanguage(Language l);
    void getSubInfo();
    TTextParams* getTextParams(); 

  private:
    TTextParams _textParams;
    std::vector<std::vector<std::string> > subs_info;
    friend class CParser;
  };
}
