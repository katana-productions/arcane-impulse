#pragma once

#include "mcv_platform.h"
#include "ui/widgets/ui_button.h"
#include "ui/ui_utils.h"
#include "render/textures/texture.h"
#include "ui/module_ui.h"
#include "engine.h"

namespace UI
{
  void CButton::render()
  {
    if (!_currentState)
    {
      return;
    }

    MAT44 imageWorld = _pivot * MAT44::CreateScale(_currentState->imageParams.size.x, _currentState->imageParams.size.y, 1.f) * _absolute;
    renderBitmap(imageWorld, _currentState->imageParams.texture, VEC2::Zero, VEC2::One, _currentState->imageParams.color, _currentState->imageParams.additive);
    
    MAT44 textWorld = _pivot * MAT44::CreateScale(_currentState->textParams.size.x, _currentState->textParams.size.y, 1.f) * _absolute;
    renderText(textWorld, _currentState->textParams.texture, _currentState->textParams.text, _currentState->textParams.size);
  }

  void CButton::setCurrentState(const std::string& stateName)
  {
    auto it = _states.find(stateName);
    _currentState = it != _states.end() ? &it->second : nullptr;
  }

  MAT44 CButton::getCorrection() {
	  UI::CModuleUI& ui = Engine.getUI();

	  MAT44 world = _pivot * MAT44::CreateScale(_currentState->imageParams.size.x, _currentState->imageParams.size.y , 1.f) * _absolute;

	  MAT44 adjust = MAT44::CreateScale(1.088, 1.071, 1.f); //Correct arbitrary adjust made in renderBitmap

	  MAT44 adjustScreen = MAT44::CreateScale(Render.width / ui.uiResolution.x, Render.height / ui.uiResolution.y, 1.f);

	  return world * adjust * adjustScreen;
  }

  VEC2 CButton::getSizeCorrection() {
	  UI::CModuleUI& ui = Engine.getUI();

	  return VEC2((Render.width/ ui.uiResolution.x) * 1.05 * _currentState->imageParams.size.x, (Render.height / ui.uiResolution.y) * _currentState->imageParams.size.y);
  }

  void CButton::lerpImageAlpha(float newAlpha, float t) {
	  for (auto& state : _states) {
		  EngineBasics.LerpElement(&state.second.imageParams.color.w, newAlpha, t);
	  }
  }

}
