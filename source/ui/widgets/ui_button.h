#pragma once

#include "ui/ui_widget.h"

namespace UI
{
  class CButton : public CWidget
  {
	struct TState
	{	
	  TImageParams imageParams;
	  TTextParams textParams;
	};

  public:
    void render() override;
    void setCurrentState(const std::string& stateName);
	TState* getCurrentState() { return _currentState; }
	MAT44 getCorrection();
	VEC2 getSizeCorrection();
	void lerpImageAlpha(float newAlpha, float t) override;
	void setImageAlpha(float alpha) override {
		for (auto& state : _states) {
			state.second.imageParams.color.w = alpha;
		}
	}

  private:
 
    std::map<std::string, TState> _states;
    TState* _currentState = nullptr;
    friend class CParser;
  };
}
