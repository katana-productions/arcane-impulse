#pragma once

#include "ui/ui_widget.h"

namespace UI
{
  class CProgress : public CWidget
  {
  public:
    void render() override;
    void setRatio(float newRatio);

	void setImageAlpha(float alpha) override {
		_imageParams.color.w = alpha;
	}
	void lerpImageAlpha(float newAlpha, float t) override;

  private:
    TImageParams _imageParams;
    TProgressParams _progressParams;

    friend class CParser;
  };
}
