#pragma once

#include "mcv_platform.h"
#include "ui/widgets/ui_text.h"
#include "ui/ui_utils.h"

#include <fstream>
#include <sstream>

#include "engine.h"

namespace UI
{
  void CText::render()
  {
    MAT44 world = _pivot * MAT44::CreateScale(_textParams.size.x, _textParams.size.y, 1.f) * _absolute;
    renderText(world, _textParams.texture, _textParams.text, _textParams.size);
  };

  void CText::setText(std::string t)
  {
    _textParams.text = t;
  };

  void CText::setText(int sub_line)
  {
    _textParams.text = subs_info[sub_line][_textParams.lang];
  };

  void CText::setLanguage(Language l)
  {
    _textParams.lang = l;
  };

  void CText::getSubInfo()
  {
    std::ifstream ifs;
    ifs.open("data/ui/" + _textParams.sub_info + ".tsv", std::ifstream::in);
    
    const char delim = ';';

    while (ifs.good())
    {
      std::vector<std::string> row;
      std::string line;
      getline(ifs, line);

      std::stringstream ss;
      ss.str(line);

      for (int i = 0; i < 2; ++i) // n languages
      {
        std::string chara, dialog;
        getline(ss, chara, delim);
        getline(ss, dialog, delim);

        std::string res;
        if (chara != "0") res = chara + " " + dialog;
        else res = dialog;

        // process result to fit subtitle size and capacity
        int nchars = 67; //chars until an endl
        int pos = nchars;
        char c, front;
        while (pos <= res.size())
        {
          c = res[pos];
          front = res[pos - 1];

          if (c == ' ' and (front == '.' or front != ' '))
          {
            res.erase(res.begin() + pos);
            res.insert(res.begin() + pos, '\n');
            pos += nchars;
          }
          else --pos;
        }
        row.push_back(res);
      }
      subs_info.push_back(row);
    }
  };

  TTextParams* CText::getTextParams()
  {
    return &_textParams;
  }
}
