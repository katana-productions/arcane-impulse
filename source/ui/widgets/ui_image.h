#pragma once

#include "mcv_platform.h"
#include "ui/ui_widget.h"

namespace UI
{
  class CImage : public CWidget
  {
  public:
	void render() override;
    TImageParams* getImageParams() override { return &_imageParams; }

	void setInitUv() {
		_imageParams.minUV = _imageParams.initMinUV;
		_imageParams.maxUV = _imageParams.initMaxUV;
	}

	void setImageAlpha(float alpha) override { 
		_imageParams.color.w = alpha;
	}

	void lerpImageAlpha(float newAlpha, float t) override;
	void lerpImageBurnAmount(float newBurnAmount, float t);

  private:
    TImageParams _imageParams;

    friend class CParser;
  };
}