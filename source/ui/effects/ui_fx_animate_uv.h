#pragma once

#include "mcv_platform.h"
#include "ui/ui_effect.h"

namespace UI
{
  class CFXAnimateUV : public CEffect
  {
  public:
    void update(float dt) override;
	void setSpeed(float speed_x, float speed_y);
	void LerpSpeed(float desired_speed_x, float desired_speed_y, float time);

  private:
    VEC2 _speed = VEC2::Zero;
	VEC2 _desiredSpeed = VEC2::Zero;
	float _factorSpeed = 0;

    friend class CParser;
  };
}
