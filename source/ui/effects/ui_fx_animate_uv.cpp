#pragma once

#include "mcv_platform.h"
#include "ui/effects/ui_fx_animate_uv.h"
#include "ui/ui_widget.h"
#include "engine.h"

namespace UI
{
  void CFXAnimateUV::update(float dt)
  {
    assert(_owner);

    TImageParams* imageParams = _owner->getImageParams();
    if (!imageParams)
    {
      return;
    }

    imageParams->minUV += _speed * dt;
    imageParams->maxUV += _speed * dt;

	//if (imageParams->color.w != imageParams->desiredLerpAlpha) {
	//	imageParams->color.w = EngineBasics.LerpElement(&imageParams->color.w, imageParams->desiredLerpAlpha, imageParams->lerpAlphaFactor);
	//}
  }

  void CFXAnimateUV::setSpeed(float speed_x, float speed_y){
	  _speed.x = speed_x;
	  _speed.y = speed_y;

	  _desiredSpeed = _speed;
  }

  void CFXAnimateUV::LerpSpeed(float desired_speed_x, float desired_speed_y, float time) {
	  EngineBasics.LerpElement(&_speed.x, desired_speed_x, time);
	  EngineBasics.LerpElement(&_speed.y, desired_speed_y, time);
  }

}
