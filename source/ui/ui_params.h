#pragma once

#include "mcv_platform.h"

namespace UI
{
  struct TParams
  {
    VEC2 pivot = VEC2::Zero;
    VEC2 position = VEC2::Zero;
    VEC2 scale = VEC2::One;
    float rotation = 0.f;
    bool visible = true;
  };

  struct TImageParams
  {
    const CTexture* texture = nullptr;
    const CTexture* texture2 = nullptr;
    const CTexture* texture3 = nullptr;
    VEC2 size = VEC2::One;
    bool additive = false;
    VEC4 color = VEC4::One;
    VEC2 minUV = VEC2::Zero;
    VEC2 maxUV = VEC2::One;
    VEC2 initMinUV = VEC2::Zero;
    VEC2 initMaxUV = VEC2::Zero;
    float desiredLerpAlpha = 0;
    float lerpAlphaFactor = 1;
    bool isBurn = false;
    float burnAmount = 0.0f;
  };

  enum Language
  {
    English,
    Spanish
  };

  struct TTextParams
  {
    std::string text;
    std::string sub_info;
    Language lang = English;
    const CTexture* texture = nullptr;
    VEC2 size = VEC2::One;
  };

  struct TProgressParams
  {
    float ratio = 1.f;
    //const std::string varName;
  };
}
