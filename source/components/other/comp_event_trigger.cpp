#include "mcv_platform.h"
#include "comp_event_trigger.h"
#include "components/common/comp_name.h"
#include "engine.h"

DECL_OBJ_MANAGER("event_trigger", TCompEventTrigger);

void TCompEventTrigger::registerMsgs()
{
  DECL_MSG(TCompEventTrigger, TMsgEntityTriggerEnter, objEntered);
  DECL_MSG(TCompEventTrigger, TMsgAlarmClock, onAlarmRecieve);
}

void TCompEventTrigger::objEntered(const TMsgEntityTriggerEnter& msg)
{
  CHandle e = msg.h_entity;
  CEntity *ent = e;
  if (!ent) return;
  TCompName *entName = ent->get<TCompName>();
  if (!entName) return;
  std::string name = entName->getName();

  // Display UI message
  if (name == "Player")
  {
		if (dialogueEvent) {
			EngineLua.playDialogue(eventToLaunch);
		}
    else EngineLua.launchEvent(eventToLaunch);
    if (oneShot)
    {
      CHandle myHandle = CHandle(this);
      EngineAlarms.AddAlarm(0.1f, -765897, myHandle);
    }
  }
}

void TCompEventTrigger::onAlarmRecieve(const TMsgAlarmClock& msg)
{
  if (msg.id == -765897)
    CHandle(this).getOwner().destroy();
}

void TCompEventTrigger::load(const json& j, TEntityParseContext& ctx)
{
  oneShot = j.value("oneShot", true);
  eventToLaunch = j.value("event", 0);
  assert(eventToLaunch != 0);
	dialogueEvent = j.value("dialogueEvent", dialogueEvent);
}

void TCompEventTrigger::debugInMenu()
{
  ImGui::Text("Event to load %d", eventToLaunch);
}

void TCompEventTrigger::renderDebug()
{
}
