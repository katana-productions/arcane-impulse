#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

class TCompLoadTrigger : public TCompBase
{
private:
  // LoadScene on enter
  bool oneShot = false;
  std::vector< std::string > scenesToLoad;
  std::vector< std::string > scenesToUnload;

  void objEntered(const TMsgEntityTriggerEnter& msg);

public:
  DECL_SIBLING_ACCESS();
  static void registerMsgs();
  void load(const json& j, TEntityParseContext& ctx);
  void debugInMenu();
  void renderDebug();
};
