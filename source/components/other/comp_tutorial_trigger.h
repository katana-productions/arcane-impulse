#pragma once

#include "components/common/comp_base.h"
#include "components/common/comp_transform.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "ui/widgets/ui_image.h"

class TCompTutorialTrigger : public TCompBase
{
private:
  // Displays a UI message to the player
  std::string message;
  std::string imageAlias;
  std::string imageAliasGamepad;
  float imageAlpha = 1;
  bool finalTrigger = false;
  bool enabled = true;
  bool lerping = false;
  float time = 0.0f;
  float lerpTime = 0.25f;
  UI::CImage* imageTutorial;

  void objEntered(const TMsgEntityTriggerEnter& msg);
  void objExited(const TMsgEntityTriggerExit& msg);
  void onAlarmRecieve(const TMsgAlarmClock& msg);


public:
  DECL_SIBLING_ACCESS();
  static void registerMsgs();
  void load(const json& j, TEntityParseContext& ctx);
  void update(float dt);
  void debugInMenu();
  void renderDebug();
  void setEnabled(bool isEnabled) { enabled = isEnabled; }


  void EndGame();

};
