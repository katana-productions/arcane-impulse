#include "mcv_platform.h"
#include "comp_trigger_call.h"
#include "physics/module_physics.h"
#include "components/common/comp_collider.h"

DECL_OBJ_MANAGER("trigger_call", TCompTriggerCall);


void TCompTriggerCall::load(const json& j, TEntityParseContext& ctx)
{
  json c = j["call_entities"];
  for (auto t : c)
  {
    call_entities.push_back(t);
  }
}

void TCompTriggerCall::debugInMenu()
{
  ImGui::Text("Entity to call: %s", call_entities[0].c_str());
}

void TCompTriggerCall::registerMsgs()
{
  DECL_MSG(TCompTriggerCall, TMsgEntityTriggerEnter, objEntered);
  //DECL_MSG(TCompTriggerCall, TMsgEntityTriggerExit, objExited);
}

void TCompTriggerCall::objEntered(const TMsgEntityTriggerEnter& msg)
{
  TMsgPuzzleTriggerEntered message;
  //Which trigger sends the message
  message.h_trigger = CHandle(this).getOwner();
  //Which object has entered the trigger
  message.h_entity = msg.h_entity;

  CEntity *ent = msg.h_entity;
  if (!ent) return;
  TCompName *entName = ent->get<TCompName>();
  if (!entName) return;
  std::string s = entName->getName();

  //TODO: filter out particles at a later date
  if (s == "Player")
  {
    for (auto e : call_entities)
    {
      CEntity *ent = (CEntity *)getEntityByName(e);
      if (ent != nullptr)
        ent->sendMsg(message);
    }
  }
}

void TCompTriggerCall::objExited(const TMsgEntityTriggerExit& msg)
{
  TMsgEntityTriggerExit message;
  //Which trigger sends the message
  //message.h_trigger = CHandle(this).getOwner();
  //Which object has entered the trigger
  message.h_entity = msg.h_entity;

  CEntity *ent = msg.h_entity;
  if (!ent) return;
  TCompName *entName = ent->get<TCompName>();
  if (!entName) return;
  std::string s = entName->getName();

  //TODO: filter out particles at a later date
  if (s == "Player")
  {
    for (auto e : call_entities)
    {
      CEntity *ent = (CEntity *)getEntityByName(e);
      ent->sendMsg(message);
    }
  }
}

//void TCompTriggerCall::update(float dt) {}
//void TCompTriggerCall::renderDebug() {}
