#pragma once

#include "components/common/comp_base.h"
#include "components/common/comp_transform.h"
#include "components/common/comp_render.h"

class TCompGloveGlow : public TCompBase
{
private:
  bool sign = false;
  float f = 0.2f;
  float cycletime = 0.8f;
  float time = 0.0f;
  bool firstTime = false;
  float frequency = 2;
  float pullMaxGlow;
  float pushMaxGlow;

  CHandle pullHdl;
  CHandle pushHdl;

  void onEntityCreated(const TMsgEntityCreated& msg);

public:
  bool dialogue = false;
  DECL_SIBLING_ACCESS();
  static void registerMsgs();
  void load(const json& j, TEntityParseContext& ctx);
  void update(float dt);
  void debugInMenu();
  void renderDebug();
  void setCurrentGlowPull(float g);
  void setCurrentGlowPush(float g);
};