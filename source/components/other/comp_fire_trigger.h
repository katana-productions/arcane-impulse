#pragma once

#include "components/common/comp_base.h"
#include "components/common/comp_transform.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

class TCompFireTrigger : public TCompBase
{
private:
  bool playerInside = false;
  float timeToDamage = 0.5f;
  float time = 0.0f;
  float dmg;
  float knockbackForce;

  void objEntered(const TMsgEntityTriggerEnter& msg);
  void objExited(const TMsgEntityTriggerExit& msg);
  void sendFireMsg();

public:
  DECL_SIBLING_ACCESS();
  static void registerMsgs();
  void load(const json& j, TEntityParseContext& ctx);
  void debugInMenu();
  void renderDebug();
  void update(float dt);
};
