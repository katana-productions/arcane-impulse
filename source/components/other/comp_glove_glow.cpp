#include "mcv_platform.h"
#include "comp_glove_glow.h"
#include "components/shaders/comp_water.h"

DECL_OBJ_MANAGER("glove_glow", TCompGloveGlow);

void TCompGloveGlow::registerMsgs() {
  DECL_MSG(TCompGloveGlow, TMsgEntityCreated, onEntityCreated);
}
void TCompGloveGlow::onEntityCreated(const TMsgEntityCreated& msg)
{

}

void TCompGloveGlow::load(const json& j, TEntityParseContext& ctx)
{
	frequency = j.value("frequency", frequency);
}

void TCompGloveGlow::update(float dt)
{
  if (!firstTime and !EnginePersistence.getGloves())
  {
    CEntity *altarGloves = getEntityByName("Gloves");
	if (!altarGloves) return;
    //TCompWater *glovesRender = ((CEntity*)altarGloves)->get<TCompWater>();
    TCompRender *glovesRender = ((CEntity*)altarGloves)->get<TCompRender>();

    if (altarGloves != nullptr and glovesRender != nullptr and dialogue)
    {
      time += dt;
      //float f = (sin(time * frequency * 2 * M_PI) + 1) / 2 + 0.2f;

      if (sign) f = time / cycletime;
      else f = 1.2f - time / cycletime;

      if (f > 1.2f) f = 1.2f;
      if (f < 0.2f) f = 0.2f;

      glovesRender->glowAmount = f * 25;

      if (time >= cycletime)
      {
        time = 0.0f;
        cycletime = randomFloat(0.1f, 0.4f);
        sign = !sign;
      }
    }
    else if (altarGloves != nullptr and glovesRender != nullptr and !dialogue)
      glovesRender->glowAmount = 25;
  }
	else if (!firstTime and EnginePersistence.getGloves())
	{
		CEntity *rightHand = getEntityByName("RightHand");
		pullHdl = ((CEntity*)rightHand)->get<TCompWater>();
		CEntity *leftHand = getEntityByName("LeftHand");
		pushHdl = ((CEntity*)leftHand)->get<TCompWater>();

		TCompWater *pullRender = (TCompWater*)pullHdl;
		TCompWater *pushRender = (TCompWater*)pushHdl;

		if (pullRender->water_glow != 0 and pushRender->water_glow != 0)
		{
			pullMaxGlow = pullRender->water_glow;
			pushMaxGlow = pushRender->water_glow;
			firstTime = true;
		}
	}
	if (firstTime and dialogue)
	{
		time += dt;
		//float f = (sin(time * frequency * 2 * M_PI) + 1) / 2 + 0.2f;

    if (sign) f = time / cycletime;
    else f = 1.2f - time / cycletime;

    if (f > 1.2f) f = 1.2f;
    if (f < 0.2f) f = 0.2f;

		TCompWater *pullRender = (TCompWater*)pullHdl;
		TCompWater *pushRender = (TCompWater*)pushHdl;
		pullRender->water_glow = f * pullMaxGlow;
		pushRender->water_glow = f * pushMaxGlow;

    if (time >= cycletime)
    {
      time = 0.0f;
      cycletime = randomFloat(0.1f, 0.4f);
      sign = !sign;
    }
	}
	else if (firstTime)	time = 0.0f;
}

void TCompGloveGlow::debugInMenu()
{
	ImGui::Checkbox("Dialogue", &dialogue);
	ImGui::DragFloat("Frequency", &frequency, 0.1f, 0.0f, 10.0f);
}

void TCompGloveGlow::renderDebug()
{

}

void TCompGloveGlow::setCurrentGlowPull(float g)
{
	if (!firstTime or dialogue) return;
	if (g < 0.05f)
	{
		TCompWater *pullRender = (TCompWater*)pullHdl;
		pullRender->water_glow = g * pullMaxGlow;
	}
	if (g > 0.7f)
	{
		TCompWater *pullRender = (TCompWater*)pullHdl;
		pullRender->water_glow = ((g - 0.7f) / (1.0f - 0.7f)) * pullMaxGlow;
	}
}

void TCompGloveGlow::setCurrentGlowPush(float g)
{
	if (!firstTime or dialogue) return;
	if (g < 0.05f)
	{
		TCompWater *pushRender = (TCompWater*)pushHdl;
		pushRender->water_glow = g * pushMaxGlow;
	}
	else if (g > 0.7f)
	{
		TCompWater *pushRender = (TCompWater*)pushHdl;
		pushRender->water_glow = ((g - 0.7f)/(1.0f - 0.7f)) * pushMaxGlow;
	}
}
