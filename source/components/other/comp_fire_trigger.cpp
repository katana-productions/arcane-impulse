#include "mcv_platform.h"
#include "comp_fire_trigger.h"

DECL_OBJ_MANAGER("fire_trigger", TCompFireTrigger);

void TCompFireTrigger::registerMsgs()
{
  DECL_MSG(TCompFireTrigger, TMsgEntityTriggerEnter, objEntered);
  DECL_MSG(TCompFireTrigger, TMsgEntityTriggerExit, objExited);
}

void TCompFireTrigger::objEntered(const TMsgEntityTriggerEnter& msg)
{
  CHandle e = msg.h_entity;
  CEntity *ent = e;
  if (!ent) return;
  TCompName *entName = ent->get<TCompName>();
  if (!entName) return;
  std::string name = entName->getName();


  if (name == "Player")
  {
    playerInside = true;
    sendFireMsg();
  }
}

void TCompFireTrigger::objExited(const TMsgEntityTriggerExit& msg)
{
  CHandle e = msg.h_entity;
  CEntity *ent = e;
  if (!ent) return;
  TCompName *entName = ent->get<TCompName>();
  if (!entName) return;
  std::string name = entName->getName();


  if (name == "Player")
  {
    playerInside = false;
  }
}

void TCompFireTrigger::sendFireMsg()
{
  CEntity *player = getEntityByName("Player");
  TMsgDamage msg;
  msg.h_sender = (CHandle)this;
  msg.damage = dmg;

  TCompTransform *myTrans = get<TCompTransform>();
  TCompTransform *playerTrans = player->get<TCompTransform>();
  VEC3 dir = playerTrans->getPosition() - myTrans->getPosition();
  dir.Normalize();
  msg.force = dir * knockbackForce;

  player->sendMsg(msg);
}

void TCompFireTrigger::load(const json& j, TEntityParseContext& ctx)
{
  dmg = j.value("damage", dmg);
  knockbackForce = j.value("knockbackForce", knockbackForce);
  timeToDamage = j.value("timeToDamage", timeToDamage);
}

void TCompFireTrigger::debugInMenu()
{
  ImGui::DragFloat("Damage", &dmg, 1.0f, 0.0f, 1000.0f);
  ImGui::DragFloat("Knockback", &knockbackForce, 1.0f, 0.0f, 1000.0f);
  ImGui::DragFloat("Time2Damage", &timeToDamage, 0.01f, 0.1f, 3.0f);
}

void TCompFireTrigger::renderDebug()
{
}

void TCompFireTrigger::update(float dt)
{
  if (playerInside)
  {
    time += dt;
    if (time >= timeToDamage)
    {
      sendFireMsg();
      time = 0.0f;
    }
  }
  else
    time = 0.0f;
}
