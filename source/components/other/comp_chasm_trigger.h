#pragma once

#include "components/common/comp_base.h"
#include "components/common/comp_transform.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

class TCompChasmTrigger : public TCompBase
{
private:
  // Kills the player, teleports named entities

  std::vector<std::string> teleportEntities;
  VEC3 teleportTarget;

  void objEntered(const TMsgEntityTriggerEnter& msg);

public:
  DECL_SIBLING_ACCESS();
  static void registerMsgs();
  void load(const json& j, TEntityParseContext& ctx);
  void debugInMenu();
  void renderDebug();
};