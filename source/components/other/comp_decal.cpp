#include "mcv_platform.h"
#include "comp_decal.h"
#include "engine.h"

#include "components/common/comp_name.h"

DECL_OBJ_MANAGER("decal", TCompDecal);

void TCompDecal::registerMsgs() {
  DECL_MSG(TCompDecal, TMsgEntityCreated, onEntityCreated);
}
void TCompDecal::onEntityCreated(const TMsgEntityCreated& msg)
{
  myTrans = get< TCompTransform >();
  TCompName *myName = get< TCompName >();
  worldPos = myTrans->getPosition();
  scale = myTrans->getScale();
  EngineDecals.addDecal(myName->getName(), type, myTrans->getPosition(), myTrans->getRotation(), myTrans->getScale(), color, ttl);
}

void TCompDecal::load(const json& j, TEntityParseContext& ctx)
{
  type = j.value("type", "");
  color = loadColor(j, "color", VEC4(1, 1, 1, 1));
  ttl = j.value("ttl", ttl);
}

void TCompDecal::debugInMenu()
{
  ImGui::DragFloat3("WorldPos", &worldPos.x, 0.1f, -1000.0f, 1000.0f);
  ImGui::ColorEdit4("Color", &color.x);
  ImGui::DragFloat("Scale", &scale, 0.1f, 0.0f, 10.f);
  bool x = ImGui::DragFloat("AngleX", &angleX, 1.f, -180.f, 180.f);
  bool y = ImGui::DragFloat("AngleY", &angleY, 1.f, -180.f, 180.f);
  bool z = ImGui::DragFloat("AngleZ", &angleZ, 1.f, -180.f, 180.f);

  myTrans->setPosition(worldPos);

  if (x or y or z) myTrans->setRotation(QUAT::CreateFromYawPitchRoll(deg2rad(angleY), deg2rad(angleX), deg2rad(angleZ)));

  myTrans->setScale(scale);
  TCompName *myName = get< TCompName >();
  EngineDecals.modifyDecal(myName->getName(), color);
  EngineDecals.modifyDecal(myName->getName(), myTrans->getPosition(), myTrans->getRotation(), myTrans->getScale());
}

void TCompDecal::renderDebug()
{

}
