#pragma once

#include "components/common/comp_base.h"
#include "entity/common_msgs.h"

class TCompPlayerSpawn : public TCompBase
{
private:
  VEC3 target;
  void onEntityCreated(const TMsgEntitiesGroupCreated& msg);

public:
  DECL_SIBLING_ACCESS();
  static void registerMsgs();
  void load(const json& j, TEntityParseContext& ctx);
  void spawnPlayer();


  void debugInMenu();
  void renderDebug();
};