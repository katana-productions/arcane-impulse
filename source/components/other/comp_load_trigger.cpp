#include "mcv_platform.h"
#include "comp_load_trigger.h"
#include "components/common/comp_name.h"
#include "engine.h"

DECL_OBJ_MANAGER("load_trigger", TCompLoadTrigger);

void TCompLoadTrigger::registerMsgs()
{
  DECL_MSG(TCompLoadTrigger, TMsgEntityTriggerEnter, objEntered);
}

void TCompLoadTrigger::objEntered(const TMsgEntityTriggerEnter& msg)
{
  CHandle e = msg.h_entity;
  CEntity *ent = e;
  if (!ent) return;
  TCompName *entName = ent->get<TCompName>();
  if (!entName) return;
  std::string name = entName->getName();

  if (name == "Player")
  {
    for (auto s : scenesToUnload)
    {
      EngineSceneLoader.unloadScene(s);
    }
    for (auto s : scenesToLoad)
    {
      //if (!EngineSceneLoader.isSceneLoaded(s))
			EngineSceneLoader.unloadScene(s);
      EngineSceneLoader.loadSceneBlock(s);
    }
	EngineNavMesh.buildNavMesh();
    if (EnginePersistence.oneShot)
      CHandle(this).getOwner().destroy();
  }
}

void TCompLoadTrigger::load(const json& j, TEntityParseContext& ctx)
{
	EnginePersistence.oneShot = j.value("oneShot", true);
  json s = j["scenesToLoad"];
  for (json::iterator it = s.begin(); it != s.end(); ++it) {
    std::string scene = it.value().get<std::string>();
    scenesToLoad.push_back(scene);
  }
  s = j["scenesToUnload"];
  for (json::iterator it = s.begin(); it != s.end(); ++it) {
    std::string scene = it.value().get<std::string>();
    scenesToUnload.push_back(scene);
  }
}

void TCompLoadTrigger::debugInMenu()
{
  ImGui::Text("Scenes to load:");
  for (auto s : scenesToLoad)
    ImGui::Text(s.c_str());
}

void TCompLoadTrigger::renderDebug()
{
}
