#include "mcv_platform.h"
#include "comp_player_spawn.h"
#include "components/controllers/comp_char_controller.h"
#include "components/common/comp_transform.h"


DECL_OBJ_MANAGER("player_spawn", TCompPlayerSpawn);


void TCompPlayerSpawn::registerMsgs() {
	DECL_MSG(TCompPlayerSpawn, TMsgEntitiesGroupCreated, onEntityCreated);
}

void TCompPlayerSpawn::onEntityCreated(const TMsgEntitiesGroupCreated& msg) {
	//spawnPlayer();
}

void TCompPlayerSpawn::spawnPlayer() {
	TCompTransform* myTransform = get<TCompTransform>();
	target = myTransform->getPosition();

	CEntity* e_player = getEntityByName("Player");

	TCompCharController* c_charcontroller = e_player->get<TCompCharController>();
	c_charcontroller->SetFootPos(target);
}

void TCompPlayerSpawn::load(const json& j, TEntityParseContext& ctx)
{
}

void TCompPlayerSpawn::debugInMenu()
{

}

void TCompPlayerSpawn::renderDebug()
{
}
