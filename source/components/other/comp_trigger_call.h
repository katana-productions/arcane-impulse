#pragma once

#include "components/common/comp_base.h"
#include "components/common/comp_transform.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

class TCompTriggerCall : public TCompBase
{
private:
  void objEntered(const TMsgEntityTriggerEnter& msg);
  void objExited(const TMsgEntityTriggerExit& msg);

public:
  DECL_SIBLING_ACCESS();
  std::vector<std::string> call_entities;
  static void registerMsgs();
  //void update(float dt);
  void load(const json& j, TEntityParseContext& ctx);
  void debugInMenu();
  //void renderDebug();
};