#include "mcv_platform.h"
#include "comp_chasm_trigger.h"
#include "components/player/comp_player_stats.h"
#include "components/ai/comp_enemy_stats.h"
#include "components/common/comp_name.h"
#include "components/common/comp_collider.h"

DECL_OBJ_MANAGER("chasm_trigger", TCompChasmTrigger);

void TCompChasmTrigger::registerMsgs()
{
  DECL_MSG(TCompChasmTrigger, TMsgEntityTriggerEnter, objEntered);
}

void TCompChasmTrigger::objEntered(const TMsgEntityTriggerEnter& msg)
{
  CHandle e = msg.h_entity;
  CEntity *ent = e;
  if (!ent) return;
  TCompName *entName = ent->get<TCompName>();
  if (!entName) return;
  std::string name = entName->getName();

  bool found = false;
  for (int i = 0; i < teleportEntities.size() and found == false; ++i)
  {
    if (teleportEntities[i] == name) found = true;
  }
  // Teleport entity to the target
  if (found)
  {
    TCompCollider *col = ent->get<TCompCollider>();
    physx::PxRigidDynamic *rb = col->actor->is<physx::PxRigidDynamic>();
    rb->setGlobalPose(physx::PxTransform(physx::PxVec3(teleportTarget.x, teleportTarget.y, teleportTarget.z)));
    rb->setLinearVelocity(physx::PxZero);
  }
  TCompEnemyStats *e_stats = ent->get<TCompEnemyStats>();
  TCompPlayerStats *p_stats = ent->get<TCompPlayerStats>();
  if (e_stats != nullptr or p_stats != nullptr)
  {
    // Kill entity
    TMsgDamage msg;
    msg.damage = 1000;
    msg.h_sender = ((CHandle)this).getOwner();
    msg.h_bullet = ((CHandle)this).getOwner();
    ent->sendMsg(msg);
  }
}

void TCompChasmTrigger::load(const json& j, TEntityParseContext& ctx)
{
  teleportTarget = loadVEC3(j,"teleport_target");

  // Load teleport entities
  if (j.count("teleport_entities"))
  {
    json t = j["teleport_entities"];
    for (json::iterator it = t.begin(); it != t.end(); ++it) {
      std::string ent = it.value().get<std::string>();
      teleportEntities.push_back(ent);
    }
  }
}

void TCompChasmTrigger::debugInMenu()
{

}

void TCompChasmTrigger::renderDebug()
{
}
