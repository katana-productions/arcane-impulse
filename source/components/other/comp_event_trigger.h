#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "engine.h"

class TCompEventTrigger : public TCompBase
{
private:
  bool oneShot = false;
  int eventToLaunch;
	bool dialogueEvent = false;

  void objEntered(const TMsgEntityTriggerEnter& msg);
  void onAlarmRecieve(const TMsgAlarmClock& msg);

public:
  DECL_SIBLING_ACCESS();
  static void registerMsgs();
  void load(const json& j, TEntityParseContext& ctx);
  void debugInMenu();
  void renderDebug();
};
