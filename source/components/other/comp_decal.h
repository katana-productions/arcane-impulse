#pragma once

#include "components/common/comp_base.h"
#include "components/common/comp_transform.h"

class TCompDecal : public TCompBase
{
private:
  std::string type;
  VEC4 color;
  float ttl = -1.0f;

  VEC3 worldPos;
  float angleX, angleY, angleZ;
  float scale;
  TCompTransform *myTrans;

  void onEntityCreated(const TMsgEntityCreated& msg);

public:
  DECL_SIBLING_ACCESS();
  static void registerMsgs();
  void load(const json& j, TEntityParseContext& ctx);
  void debugInMenu();
  void renderDebug();
};