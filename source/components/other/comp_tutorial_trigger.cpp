#include "mcv_platform.h"
#include "comp_tutorial_trigger.h"
#include "components/player/comp_player_stats.h"
#include "components/ai/comp_enemy_stats.h"
#include "components/common/comp_name.h"
#include "components/common/comp_collider.h"
#include "engine.h"
#include "ui/module_ui.h"
#include "ui/widgets/ui_progress.h"

DECL_OBJ_MANAGER("tutorial_trigger", TCompTutorialTrigger);

void TCompTutorialTrigger::registerMsgs()
{
  DECL_MSG(TCompTutorialTrigger, TMsgEntityTriggerEnter, objEntered);
  DECL_MSG(TCompTutorialTrigger, TMsgEntityTriggerExit, objExited);
  DECL_MSG(TCompTutorialTrigger, TMsgAlarmClock, onAlarmRecieve);

}

void TCompTutorialTrigger::onAlarmRecieve(const TMsgAlarmClock& msg) {
	if (msg.id == 101) {
		EndGame();
	}
}

void TCompTutorialTrigger::objEntered(const TMsgEntityTriggerEnter& msg)
{
  CHandle e = msg.h_entity;
  CEntity *ent = e;
  if (!ent) return;
  TCompName *entName = ent->get<TCompName>();
  if (!entName) return;
  std::string name = entName->getName();

  // Display UI message
  //if (name == "Player")
  //{
  //  TMsgMessageUI msg;
  //  msg.message = message;
  //  msg.show = true;
  //  CEntity *ui = getEntityByName("UI");
  //  ui->sendMsg(msg);
  //}

  if (name == "Player" && imageAlias != "") {
    lerping = true;
  }

  if(finalTrigger)
	  EngineAlarms.AddAlarm(3, 101, CHandle(this));

}

void TCompTutorialTrigger::objExited(const TMsgEntityTriggerExit& msg)
{
  CHandle e = msg.h_entity;
  CEntity *ent = e;
  if (!ent) return;
  TCompName *entName = ent->get<TCompName>();
  if (!entName) return;
  std::string name = entName->getName();

  // Hide UI message
  //if (name == "Player")
  //{
  //  TMsgMessageUI msg;
  //  msg.message = "";
  //  msg.show = false;
  //  CEntity *ui = getEntityByName("UI");
  //  ui->sendMsg(msg);
  //}

  if (name == "Player" && imageAlias != "") {
    lerping = false;
  }
}

void TCompTutorialTrigger::load(const json& j, TEntityParseContext& ctx)
{
  message = j.value("message", "");
  imageAlias = j.value("imageAlias", "");
  imageAliasGamepad = j.value("imageAliasGamepad", "");
  imageAlpha = j.value("imageAlpha", 1.0);
  finalTrigger = j.value("finalTrigger", false);
  enabled = j.value("enabled", enabled);
}

void TCompTutorialTrigger::update(float dt)
{
  if (lerping) time += dt;
  else time -= dt;

  if (time < 0.0f) time = 0.0f;
  if (time >= lerpTime) time = lerpTime;

  if (!enabled) return;

  if (imageAliasGamepad != "" && !EngineSettings.usingKeyboard()) {
    if (imageTutorial) imageTutorial->setImageAlpha(0);
	  imageTutorial = dynamic_cast<UI::CImage*>(Engine.getUI().getWidgetByAlias(imageAliasGamepad));
  }else {
    if (imageTutorial) imageTutorial->setImageAlpha(0);
	  imageTutorial = dynamic_cast<UI::CImage*>(Engine.getUI().getWidgetByAlias(imageAlias));
  }
  if (imageTutorial)
    imageTutorial->setImageAlpha(time / lerpTime * imageAlpha);
}

void TCompTutorialTrigger::debugInMenu()
{
  ImGui::Text(message.c_str());
}

void TCompTutorialTrigger::renderDebug()
{
}

void TCompTutorialTrigger::EndGame(){
}
