#ifndef INC_COMPONENT_RENDER_VIGNETTE_H_
#define INC_COMPONENT_RENDER_VIGNETTE_H_

#include <vector>
#include "components/common/comp_base.h"

class CTexture;
class CRenderToTexture;

struct TCompVignette : public TCompBase {
	CRenderToTexture*       rt = nullptr;
	const CTechnique*       tech = nullptr;
	const CMesh*            mesh = nullptr;

	TCompVignette();
	~TCompVignette();

	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	CTexture* apply(CTexture* in_texture, CTexture* blur_texture);

public:
	bool enabled;
	float vigExtend, vigIntensity, chromAberrationAmount;

	CCteBuffer<TCtePostFx> cte_postFx;
	static CHandle vignetteHandle;

	void CreateRT();
	void ChromAberrAnim(const float & newChromAberr, const float& time);
};
#endif