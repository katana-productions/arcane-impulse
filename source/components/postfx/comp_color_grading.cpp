#include "mcv_platform.h"
#include "comp_color_grading.h"
#include "render/textures/render_to_texture.h"
#include "resources/resource.h"

DECL_OBJ_MANAGER("color_grading", TCompColorGrading);

// ---------------------
void TCompColorGrading::debugInMenu() {
  ImGui::Checkbox("Enabled", &enabled);
  ImGui::DragFloat("Amount", &amount, 0.01f, 0.0f, 1.0f);
  ImGui::Text("Lut Texture: %s", lut_name.c_str());
}

void TCompColorGrading::load(const json& j, TEntityParseContext& ctx) {
  enabled = j.value("enabled", true);
  amount= j.value( "amount", 1.0f);
  lut_name = j.value("lut", "data/textures/lut/neutralLut.dds");
  CreateLutTexture(lut_name);
}

void TCompColorGrading::CreateLutTexture(const std::string& textureName)
{
	lut_name = textureName;
	lut1 = Resources.get(lut_name)->as<CTexture>();
}