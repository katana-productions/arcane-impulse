#ifndef INC_COMPONENT_RENDER_MOTIONBLUR_H_
#define INC_COMPONENT_RENDER_MOTIONBLUR_H_

#include <vector>
#include "components/common/comp_base.h"
#include "components/common/comp_camera.h"

class CTexture;
class CRenderToTexture;

struct TCompMotionBlur : public TCompBase {
	CRenderToTexture*       rt = nullptr;
	const CTechnique*       tech = nullptr;
	const CMesh*            mesh = nullptr;
	MAT44 prevViewProjection;

	TCompMotionBlur();
	~TCompMotionBlur();

	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	CTexture* apply(CTexture* in_texture, const TCompCamera& currCam);

public:
	bool enabled;
	float mblur_blur_samples;
	float mblur_blur_speed;
	float mblur_samples;

	CCteBuffer<TCtePostFx> cte_motionBlur;

	void CreateRT();
};
#endif