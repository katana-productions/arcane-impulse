#include "mcv_platform.h"
#include "comp_camera_magic_outlines.h"
#include "resources/resource.h"
#include "render/textures/render_to_texture.h"
#include "entity/entity_parser.h"

DECL_OBJ_MANAGER("camera_magic_outlines", TCompCameraMagicOutlines);

void TCompCameraMagicOutlines::debugInMenu() {
	ImGui::Checkbox("Enabled", &enabled);
}

void TCompCameraMagicOutlines::load(const json& j, TEntityParseContext& ctx) {
	enabled = j.value("enabled", true);

	tech = Resources.get("outline_compose.tech")->as<CTechnique>();
	mesh = Resources.get("unit_quad_xy.mesh")->as<CMesh>();

	rt = new CRenderToTexture;
	CreateRT();
}

void TCompCameraMagicOutlines::CreateRT()
{
	bool is_ok = rt->createRT("RT_MagicOutlines", Render.width, Render.height, DXGI_FORMAT_R16G16B16A16_FLOAT);
	assert(is_ok);
}

CTexture* TCompCameraMagicOutlines::apply(CTexture* in_deferred, CTexture* in_outline_data, CTexture* in_pre_outline_data) {
	if (!enabled) return in_deferred;
	CGpuScope gpu_scope("magic_outlines");

	assert(rt);
	assert(mesh);
	assert(tech);

	rt->activateRT();

	in_deferred->activate(TS_ALBEDO);
	in_outline_data->activate(TS_ALBEDO2);
	in_pre_outline_data->activate(TS_ALBEDO3);

	tech->activate();
	mesh->activateAndRender();

	return rt;
}
