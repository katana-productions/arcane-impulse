#ifndef INC_COMPONENT_RENDER_FOCUS_H_
#define INC_COMPONENT_RENDER_FOCUS_H_

#include <vector>
#include "components/common/comp_base.h"

class CTexture;
class CRenderToTexture;

// ------------------------------------
struct TCompRenderFocus : public TCompBase {
	bool                     enabled;
	CCteBuffer< TCteFocus > cte_focus;
	CRenderToTexture*        rt = nullptr;
	const CTechnique*        tech = nullptr;
	const CMesh*             mesh = nullptr;

	TCompRenderFocus();
	~TCompRenderFocus();

	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	CTexture* apply(CTexture* focus_texture, CTexture* blur_texture);

public:
	void CreateRT();

	float focus_z_center_in_focus;
	float focus_z_margin_in_focus;
	float focus_transition_distance;
	float focus_modifier;
};

#endif
