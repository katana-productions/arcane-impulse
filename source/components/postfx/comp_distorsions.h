#ifndef INC_COMP_CAMERA_DISTORSIONS_H_
#define INC_COMP_CAMERA_DISTORSIONS_H_

#include "components/common/comp_base.h"

class CTexture;
class CRenderToTexture;

struct TCompDistorsions : public TCompBase {
	CRenderToTexture*       rt = nullptr;
	const CTechnique*       tech = nullptr;
	const CMesh*            mesh = nullptr;

	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	CTexture* apply(CTexture* in_deferred, CTexture* distorsion_data);

public:
	bool enabled;
	void CreateRT();
};

#endif