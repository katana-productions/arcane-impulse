#include "mcv_platform.h"
#include "comp_render_motionBlur.h"
#include "resources/resource.h"
#include "render/textures/render_to_texture.h"
#include "entity/entity_parser.h"

DECL_OBJ_MANAGER("render_motionBlur", TCompMotionBlur);

TCompMotionBlur::TCompMotionBlur()
	: cte_motionBlur(CTE_BUFFER_SLOT_POSTFX)
{
	bool is_ok = cte_motionBlur.create("MotionBlur");
	assert(is_ok);
	cte_motionBlur.activate();
}

TCompMotionBlur::~TCompMotionBlur() {
	cte_motionBlur.destroy();
}

void TCompMotionBlur::debugInMenu() {
	ImGui::Checkbox("Enabled", &enabled);
	ImGui::DragFloat("Blur Samples", &mblur_blur_samples, 0.1f, 0.f, 1000.f);
	ImGui::DragFloat("Blur speed", &mblur_blur_speed, 0.1f, 0.f, 300.f);
	ImGui::DragFloat("Samples", &mblur_samples, 0.1f, 0.f, 1000.f);
}

void TCompMotionBlur::load(const json& j, TEntityParseContext& ctx) {
	enabled = j.value("enabled", true);
	mblur_blur_samples = j.value("motionBlur_samples", 8.f);
	mblur_blur_speed = j.value("motionBlur_speed", 0.35f);
	mblur_samples = j.value("mBsamples", 8.f);

	tech = Resources.get("motionBlur.tech")->as<CTechnique>();
	mesh = Resources.get("unit_quad_xy.mesh")->as<CMesh>();


	rt = new CRenderToTexture;
	CreateRT();
}

void TCompMotionBlur::CreateRT() {
	bool is_ok = rt->createRT("RT_MotionBlur", Render.width, Render.height, DXGI_FORMAT_R16G16B16A16_FLOAT);
	assert(is_ok);
}

CTexture* TCompMotionBlur::apply(CTexture* in_texture, const TCompCamera& currCam) {
	if (!enabled) return in_texture;
	CGpuScope gpu_scope("motionBlur");

	assert(rt);

	cte_motionBlur.mblur_blur_samples = mblur_blur_samples;
	cte_motionBlur.mblur_blur_speed = mblur_blur_speed;
	cte_motionBlur.PrevViewProjection = prevViewProjection;

	cte_motionBlur.updateGPU();
	cte_motionBlur.activate();

	assert(mesh);
	assert(tech);
	assert(in_texture);

	rt->activateRT();

	in_texture->activate(TS_ALBEDO);

	tech->activate();
	mesh->activateAndRender();

	prevViewProjection = currCam.getViewProjection();

	return rt;
}