#include "mcv_platform.h"
#include "comp_render_overlay.h"
#include "resources/resource.h"
#include "render/textures/render_to_texture.h"
#include "entity/entity_parser.h"

DECL_OBJ_MANAGER("render_overlay", TCompOverlay);

CHandle TCompOverlay::vignetteHandle;

TCompOverlay::TCompOverlay()
	: cte_postFx(CTE_BUFFER_SLOT_POSTFX)
{
	bool is_ok = cte_postFx.create("Overlay");
	assert(is_ok);
	cte_postFx.activate();
}

TCompOverlay::~TCompOverlay() {
	cte_postFx.destroy();
}

void TCompOverlay::debugInMenu() {
	ImGui::Checkbox("Enabled", &enabled);

	ImGui::DragFloat("iluminance", &iluminance, .01f, -1.0f, 1.0f);
	ImGui::DragFloat("intensity", &intensity, 0.5f, 0.0f, 500.0f);
	ImGui::DragFloat("colorShift", &colorShift, 0.01f, 0.0f, 10.0f);
	ImGui::DragFloat("vignette", &vignette, 0.01f, 0.0f, 15.0f);
	ImGui::DragFloat("hitAmount", &hitAmount, 0.01f, 0.0f, 1.0f);
	ImGui::DragFloat("black", &black, 0.01f, 0.0f, 1.0f);
}

void TCompOverlay::load(const json& j, TEntityParseContext& ctx) {
	enabled = j.value("enabled", true);

	paletteFileName = j.value("paletteFileName", paletteFileName);
	paletteTexture = Resources.get(paletteFileName)->as<CTexture>();

	iluminance = j.value("iluminance", iluminance);
	intensity = j.value("intensity", intensity);
	colorShift = j.value("colorShift", colorShift);
	vignette = j.value("vignette", vignette);
	hitAmount = j.value("hitAmount", hitAmount);
	black = j.value("black", black);

	tech = Resources.get("overlays.tech")->as<CTechnique>();
	mesh = Resources.get("unit_quad_xy.mesh")->as<CMesh>();

	vignetteHandle = ctx.current_entity;

	rt = new CRenderToTexture;
	CreateRT();
}

void TCompOverlay::CreateRT() {
	bool is_ok = rt->createRT("RT_Overlay", Render.width, Render.height, DXGI_FORMAT_R16G16B16A16_FLOAT);
	assert(is_ok);
}

CTexture* TCompOverlay::apply(CTexture* in_texture) {
	if (!enabled) return in_texture;
	CGpuScope gpu_scope("Overlay");

	assert(rt);

	cte_postFx.iluminance = iluminance;
	cte_postFx.intensity = intensity;
	cte_postFx.colorShift = colorShift;
	cte_postFx.vignette = vignette;
	cte_postFx.hitAmount = hitAmount;
	cte_postFx.black = black;
  cte_postFx.white = white;
	cte_postFx.updateGPU();
	cte_postFx.activate();

	assert(mesh);
	assert(tech);
	assert(in_texture);

	rt->activateRT();

	in_texture->activate(TS_ALBEDO);
	paletteTexture->activate(TS_ALBEDO2);

	tech->activate();
	mesh->activateAndRender();

	return rt;
}