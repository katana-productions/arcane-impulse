#ifndef INC_COMPONENT_RENDER_ANTIALIAS_H_
#define INC_COMPONENT_RENDER_ANTIALIAS_H_

#include <vector>
#include "components/common/comp_base.h"

class CTexture;
class CRenderToTexture;

struct TCompAntiAlias : public TCompBase {
	CRenderToTexture*       rt = nullptr;
	const CTechnique*       tech = nullptr;
	const CMesh*            mesh = nullptr;

	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	CTexture* apply(CTexture* in_texture);

public:
	bool enabled;
	void CreateRT();
};
#endif