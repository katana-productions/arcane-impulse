#include "mcv_platform.h"
#include "comp_render_antialias.h"
#include "resources/resource.h"
#include "render/textures/render_to_texture.h"
#include "entity/entity_parser.h"

DECL_OBJ_MANAGER("render_antialias", TCompAntiAlias);


void TCompAntiAlias::debugInMenu() {
	ImGui::Checkbox("Enabled", &enabled);
}

void TCompAntiAlias::load(const json& j, TEntityParseContext& ctx) {
	enabled = j.value("enabled", true);

	tech = Resources.get("antialias.tech")->as<CTechnique>();
	mesh = Resources.get("unit_quad_xy.mesh")->as<CMesh>();

	rt = new CRenderToTexture;
	CreateRT();
}

void TCompAntiAlias::CreateRT() {
	bool is_ok = rt->createRT("RT_Antialias", Render.width, Render.height, DXGI_FORMAT_R16G16B16A16_FLOAT);
	assert(is_ok);
}

CTexture* TCompAntiAlias::apply(CTexture* in_texture) {
	if (!enabled) return in_texture;
	CGpuScope gpu_scope("antialiasing");

	assert(rt);

	assert(mesh);
	assert(tech);
	assert(in_texture);

	rt->activateRT();

	in_texture->activate(TS_ALBEDO);

	tech->activate();
	mesh->activateAndRender();

	return rt;
}