#ifndef INC_COMPONENT_RENDER_OVERLAY_H_
#define INC_COMPONENT_RENDER_OVERLAY_H_

#include <vector>
#include "components/common/comp_base.h"

class CTexture;
class CRenderToTexture;

struct TCompOverlay : public TCompBase {
	CRenderToTexture*       rt = nullptr;
	const CTechnique*       tech = nullptr;
	const CMesh*            mesh = nullptr;

	std::string paletteFileName;
	const CTexture* paletteTexture;

	TCompOverlay();
	~TCompOverlay();

	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	CTexture* apply(CTexture* in_texture);

public:
	bool enabled;
	float iluminance = 0.0f;
	float intensity = 1.0f;
	float colorShift = 0.2f;
	float vignette = 0.9f;
	float hitAmount = 0.0f;
	float black = 0.0f;
  float white = 0.0f;

	CCteBuffer<TCtePostFx> cte_postFx;
	static CHandle vignetteHandle;

	void CreateRT();
};
#endif