#ifndef INC_COMP_CAMERA_MAGIC_OUTLINES_H_
#define INC_COMP_CAMERA_MAGIC_OUTLINES_H_

#include "components/common/comp_base.h"

class CTexture;
class CRenderToTexture;

// ------------------------------------
struct TCompCameraMagicOutlines : public TCompBase {
	CRenderToTexture*       rt = nullptr;
	const CTechnique*       tech = nullptr;
	const CMesh*            mesh = nullptr;

	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	CTexture* apply(CTexture* in_deferred, CTexture* in_outline_data, CTexture* in_pre_outline_data);

public:
	bool enabled;
	void CreateRT();
};

#endif