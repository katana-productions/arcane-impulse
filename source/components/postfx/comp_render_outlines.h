#ifndef INC_COMPONENT_RENDER_OUTLINES_H_
#define INC_COMPONENT_RENDER_OUTLINES_H_

#include <vector>
#include "components/common/comp_base.h"

class CTexture;
class CRenderToTexture;

// ------------------------------------
struct TCompOutlines : public TCompBase {
	CRenderToTexture*       rt = nullptr;
	const CTechnique*       tech = nullptr;
	const CMesh*            mesh = nullptr;

	TCompOutlines();
	~TCompOutlines();

	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	CTexture* apply(CTexture* in_texture, CTexture* outline_mask);

public:
	bool enabled;
	float depthFade;
	float outlineThreshold;
	float maxOutline;
	float fogDepthFade;
	float maxFog;
	float fogContrast;
	VEC4 fogColor = VEC4(0.0f, 0.0f, 0.0f, 1.0f);

	CCteBuffer< TCtePostFx > cte_outlines;

	static CHandle outlinesHandle;

	void CreateRT();
};

#endif
