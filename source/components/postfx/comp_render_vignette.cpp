#include "mcv_platform.h"
#include "engine.h"
#include "comp_render_vignette.h"
#include "resources/resource.h"
#include "render/textures/render_to_texture.h"
#include "entity/entity_parser.h"

DECL_OBJ_MANAGER("render_vignette", TCompVignette);

CHandle TCompVignette::vignetteHandle;

TCompVignette::TCompVignette()
	: cte_postFx(CTE_BUFFER_SLOT_POSTFX)
{
	bool is_ok = cte_postFx.create("Vignette");
	assert(is_ok);
	cte_postFx.activate();
}

TCompVignette::~TCompVignette() {
	cte_postFx.destroy();
}

void TCompVignette::debugInMenu() {
	ImGui::Checkbox("Enabled", &enabled);
	ImGui::DragFloat("Vig Intens", &vigIntensity, 0.5f, 0.0f, 50.0f);
	ImGui::DragFloat("Vig Ext", &vigExtend, 0.05f, 0.0f, 1.0f);
	ImGui::DragFloat("CAb Amnt", &chromAberrationAmount, 0.01f, -1.0f, 0.0f);
}

void TCompVignette::load(const json& j, TEntityParseContext& ctx) {
	enabled = j.value("enabled", true);
	vigIntensity = j.value("VigIntens", 25.0f);
	vigExtend = j.value("VigExt", 0.25f);
	chromAberrationAmount = j.value("ChromAberrationAmount", 0.0f);

	tech = Resources.get("vignette.tech")->as<CTechnique>();
	mesh = Resources.get("unit_quad_xy.mesh")->as<CMesh>();

	vignetteHandle = ctx.current_entity;

	rt = new CRenderToTexture;
	CreateRT();
}

void TCompVignette::CreateRT() {
	bool is_ok = rt->createRT("RT_Vignette", Render.width, Render.height, DXGI_FORMAT_R16G16B16A16_FLOAT);
	assert(is_ok);
}

CTexture* TCompVignette::apply(CTexture* in_texture, CTexture* blur_texture) {
	if (!enabled) return in_texture;
	CGpuScope gpu_scope("vignette");

	assert(rt);

	cte_postFx.vigIntensity = vigIntensity;
	cte_postFx.vigExtend = vigExtend;
	cte_postFx.chromAberrationAmount = chromAberrationAmount;
	cte_postFx.updateGPU();
	cte_postFx.activate();

	assert(mesh);
	assert(tech);
	assert(in_texture);
	assert(blur_texture);

	rt->activateRT();

	in_texture->activate(TS_ALBEDO);
	blur_texture->activate(TS_ALBEDO2);

	tech->activate();
	mesh->activateAndRender();

	return rt;
}

void TCompVignette::ChromAberrAnim(const float& newChromAberr, const float& time) {
	chromAberrationAmount = newChromAberr;
	EngineBasics.LerpElement(&chromAberrationAmount, 0.0f, time);
}