#include "mcv_platform.h"
#include "comp_render_outlines.h"
#include "resources/resource.h"
#include "render/textures/render_to_texture.h"
#include "entity/entity_parser.h"

DECL_OBJ_MANAGER("render_outlines", TCompOutlines);

CHandle TCompOutlines::outlinesHandle;

TCompOutlines::TCompOutlines()
	: cte_outlines(CTE_BUFFER_SLOT_POSTFX)
{
	bool is_ok = cte_outlines.create("Outlines");
	assert(is_ok);
}

TCompOutlines::~TCompOutlines() {
	cte_outlines.destroy();
}

void TCompOutlines::debugInMenu() {
	ImGui::Checkbox("Enabled", &enabled);
	ImGui::DragFloat("Depth Fade", &depthFade, 0.001f, 0.01f, 5.0f);
	ImGui::DragFloat("Max Outline", &maxOutline, 0.001f, 0.0f, 1.0f);
	ImGui::DragFloat("Normal Threshold", &outlineThreshold, 0.025f, 0.0f, 5.0f);
	ImGui::DragFloat("Fog Fade", &fogDepthFade, 0.001f, 0.01f, 10.0f);
	ImGui::DragFloat("Max Fog", &maxFog, 0.001f, 0.0f, 1.0f);
	ImGui::DragFloat("FogContrast", &fogContrast, 0.1f, 0.0f, 50.0f);
	ImGui::ColorEdit3("Fog Color", (float*)&fogColor.x);
}

void TCompOutlines::load(const json& j, TEntityParseContext& ctx) {
	enabled = j.value("enabled", true);
	depthFade = j.value("DepthFade", 0.05f);
	maxOutline = j.value("MaxOutline", 1.0f);
	outlineThreshold = j.value("outlineThreshold", 0.4f);
	fogDepthFade = j.value("fogDepthFade", 0.05f);
	maxFog = j.value("maxFog", 1.0f);
	fogContrast = j.value("fogContrast", 0.0f);
	fogColor = loadVEC4(j, "fogColor");

	tech = Resources.get("outlines.tech")->as<CTechnique>();
	mesh = Resources.get("unit_quad_xy.mesh")->as<CMesh>();

	outlinesHandle = ctx.current_entity;

	rt = new CRenderToTexture;
	CreateRT();
}

void TCompOutlines::CreateRT()
{
	bool is_ok = rt->createRT("RT_Outlines", Render.width, Render.height, DXGI_FORMAT_R16G16B16A16_FLOAT);
	assert(is_ok);
}

CTexture* TCompOutlines::apply(CTexture* in_texture, CTexture* outline_mask) {
	if (!enabled) return in_texture;
	CGpuScope gpu_scope("outlines");

	assert(rt);

	cte_outlines.depthFade = depthFade;
	cte_outlines.maxOutline = maxOutline;
	cte_outlines.outlineThreshold = outlineThreshold;
	cte_outlines.fogDepthFade = fogDepthFade;
	cte_outlines.maxFog = maxFog;
	cte_outlines.fogContrast = fogContrast;
	cte_outlines.fogColor = fogColor;
	cte_outlines.updateGPU();
	cte_outlines.activate();

	assert(mesh);
	assert(tech);

	rt->activateRT();

	in_texture->activate(TS_ALBEDO);
	outline_mask->activate(TS_ALBEDO2);

	tech->activate();
	mesh->activateAndRender();

	return rt;
}