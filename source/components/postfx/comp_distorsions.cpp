#include "mcv_platform.h"
#include "comp_distorsions.h"
#include "resources/resource.h"
#include "render/textures/render_to_texture.h"
#include "entity/entity_parser.h"

DECL_OBJ_MANAGER("camera_distorsions", TCompDistorsions);

void TCompDistorsions::debugInMenu() {
	ImGui::Checkbox("Enabled", &enabled);
}

void TCompDistorsions::load(const json& j, TEntityParseContext& ctx) {
	enabled = j.value("enabled", true);

	tech = Resources.get("screen_distorsions.tech")->as<CTechnique>();
	mesh = Resources.get("unit_quad_xy.mesh")->as<CMesh>();

	rt = new CRenderToTexture;
	CreateRT();
}

void TCompDistorsions::CreateRT()
{
	bool is_ok = rt->createRT("RT_Distorsions", Render.width, Render.height, DXGI_FORMAT_R16G16B16A16_FLOAT);
	assert(is_ok);
}

CTexture* TCompDistorsions::apply(CTexture* in_deferred, CTexture* distorsion_data) {
	if (!enabled) return in_deferred;
	CGpuScope gpu_scope("screen_distorsions");

	assert(rt);
	assert(mesh);
	assert(tech);

	rt->activateRT();

	in_deferred->activate(TS_ALBEDO);
	distorsion_data->activate(TS_ALBEDO2);

	tech->activate();
	mesh->activateAndRender();

	return rt;
}