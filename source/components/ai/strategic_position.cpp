#include "mcv_platform.h"
#include "strategic_position.h"
#include "components/common/comp_transform.h"
#include "entity/entity_parser.h"
#include "engine.h"

DECL_OBJ_MANAGER("strategic_position", TCompStrategicPosition);

void TCompStrategicPosition::registerMsgs() {
	DECL_MSG(TCompStrategicPosition, TMsgEntityCreated, onEntityCreated);
}

void TCompStrategicPosition::onEntityCreated(const TMsgEntityCreated& msg) {
	TCompTransform* c_trans = get<TCompTransform>();
	EngineEnemyManager.InsertStrategicPositionSBB(c_trans->getPosition());
	//TODO: Destroy
}




