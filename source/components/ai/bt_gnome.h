#pragma once
#include "bt.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "components\common\comp_transform.h"


class bt_gnome :public bt
{

	enum typeAttack {
		ShortAttack = 0,
		LongAttack = 1
	};

	std::vector<VEC3> path;
	int crowdIdx;
	bool crowdMovement = false;
	float timeCheckPath = 2;
	bool checkPath = true;
	bool walkable = true;

	std::vector<VEC3> waypoints;
	VEC3 gatePosition;
	VEC3 lookAtKeepingGate;

	std::string prefabSword;
	std::vector<CHandle> swords;
	std::string fireball_explosion_particles = "data/particles/prefabs_particles/GoblinDeath3.json";

	bool cinematicController = false;
	bool cinematicMove = false;
	VEC3 cinematicDestination = VEC3(0, 0, 0);

	bool BerserkMode = false;

	bool explosiveGnome = false;
	bool view = false;
	bool forget = true;
	bool powerAffected = false;
	bool isPushed = false;
	bool gnomeKilled = false;
	bool isProtected = false;
	bool isTaunt = false;
	ObjState state = ObjState::Normal;
	ObjState lastState = ObjState::Normal;

	bool firstDeathFrame = true;
	bool canDisappear = false;
	bool ragdollActived = false;
	float timeToDisappear = 3;
	float velToDieByGnome = 1;
	float attenuationFactor = 0.75;

	float tauntingTime = 2.0f;
	bool firstTauntFrame = true;

	float degreesVision = 180;
	float vision_distance = 20;
	float forget_distance = 10;
	bool visionState = false;
	float timeNoVision = 0;
	float timeToForget = 5;

	bool viewingPlayer = false;
	float timeToViewPlayer = 1;

	float attackDistance = 1;
	float shortAttackDistance = 0.9;
	typeAttack currentAttackType;

	float damageAttack = 5;
	bool cdAttack = true;
	bool attacking = false;
	float timeShortAttacking = 0.8; //not move during attacking
	float timeLongAttacking = 1.; //not move during attacking
	float cdAttackTime = 3;
	float knockbackForce = 10;

	float timeToImpactFirstShort = 0.2;
	float timeToImpactSecondShort = 0.6;
	float timeToImpactFirstLong = 0.3;
	float timeToImpactSecondLong = 0.9;

	bool inIdleWar = false;

	float damageExplosion = 10;
	float forceExplosion = 50;
	float explosionRadius = 10.0f;

	float minKnockBack = 0.25f;

	CHandle player;

	int nWaypoints = 0;
	int currWaypoint = 0;

	VEC3 tauntPosition;
	VEC3 panicPosition;
	bool firstPanic = true;

	VEC3 lastDst = VEC3(0, 0, 0);

	VEC3 debugDst = VEC3(0, 0, 0);
	VEC3 debugPosCrowd = VEC3(0, 0, 0);
	const char* target;
	physx::PxRaycastBuffer hit;

	//Audio
	bool isStep = true;
	bool isTalk = true;
	bool isPulled = true;

	DECL_SIBLING_ACCESS();
	void onImpact(const TMsgDamage & msg);
	void onContact(const TMsgOnContact & msg);
	void onPowerMessage(const TMsgIntObjState& msg);
	void onEntityCreated(const TMsgEntityCreated& msg);
	void onDetectPlayer(const TMsgDetectPlayer& msg);
	void onGnomeProtected(const TMsgMagicProtection& msg);
	void onAlarmRecieve(const TMsgAlarmClock& msg);
	void onControllerHit(const TMsgOnControllerHit& msg);
	void onAddedCrown(const TMsgOnAddedCrown& msg);

	void DeathChecks(CHandle sourceImpact, VEC3 velImpact);

	int cont = 0;

	//Auxiliar enemyCounter
	bool auxFight = false;

public:
	bt_gnome();

	void load(const json& j, TEntityParseContext& ctx);
	void renderDebug();
	void debugInMenu();
	void drawConeVision(float near_distance, float far_distance, VEC4 color);

	void create(string); //Init

	int actionCinematicMove();
	int actionCinematicIdle();
	int actionExplodeGnome();
	int actionDying();
	int actionRecoverFromPower();
	int actionPowerAffected();
	int actionTaunt();
	int actionShortAttack();
	int actionLongAttack();
	int actionIdleWar();
	int actionChase();
	int actionPatrol();
	int actionChaseGate();
	int actionKeepGate();

	bool conditionCinematicController();
	bool conditionCinematicMove();
	bool conditionGnomeKilled();
	bool conditionExplodeGnome();
	bool conditionTakePlayerRefence();
	bool conditionRecoverFromPower();
	bool conditionPowerAffected();
	bool conditionTaunt();
	bool conditionView();
	bool conditionAttack();
	bool conditionShortAttack();
	bool conditionIdleWar();
	bool conditionPatrol();
	bool conditionChaseGate();

	static void registerMsgs();

	void Move(VEC3 dst);
	void SendDamage();
	void ActivateRagdoll();
	void LeaveSwords();
	void changeVariable(const char* varName, float value, bool isBool);
	bool CheckViewPlayer();
	void CheckDiedByOtherGnome(VEC3 velImpact);
	void setAgentState(bool state);
	VEC3 getVelAgent() { return velAgent; }

	bool GetPowerAffected();
	bool GetRagdollActivated();
	bool GetInIdleWar() { return inIdleWar; }

	void BlendSpeedAnimation();
	void ChangeSpeed(float desiredSpeed);

	void WatchingPlayer();

	float relaxSpeed = 2.5f;
	float walkSpeed = 5.f;
	float runSpeed = 15.f;
	float actualSpeed = 1.0f;

	VEC3 velAgent = VEC3(0, 0, 0);
	float agentRadius = 0.6;
	float lastWeight = 0;
	float weightDebug = 0;
	float speedDebug = 0;

	float forceSpeed = 3;
	bool setSpeed = false;

	bool getIsRagdoll() { return ragdollActived; }

	//LUA
	void setGnomeCinematicController(bool newCinematicController) {
		cinematicController = newCinematicController;
		if (!cinematicController) resetState = true;
	}
	void moveGnomeCinematic(VEC3 destination);
	void setViewPlayer(bool newView);
	void setBerserkMode(bool mode) { 
		BerserkMode = mode; 
	}
	//
	~bt_gnome();
};