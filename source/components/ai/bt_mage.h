#pragma once
#include "bt.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "PxPhysicsAPI.h"
#include "audio/SoundEvent.h"
#include "engine.h"

class bt_mage :public bt
{
	CHandle currentFireBall;

	std::vector<VEC3> path;
	int crowdIdx;
	bool crowdMovement = false;

	std::vector<VEC3> waypoints;
	VEC3 gatePosition;
	VEC3 strategicPosition;
	VEC3 lookAtKeepingGate;

	bool cinematicController = false;
	bool cinematicMove = false;
	VEC3 cinematicDestination = VEC3(0, 0, 0);
	bool BerserkMode = false;

	bool fireMage = false;
	bool supportMage = false;
	bool death = false;
	bool damaged = false;
	bool view = false;
	bool forget = true;
	bool covered = false;
	bool firstMoveDone = false;
	bool walking = false;
	bool loadingSpell = false;
	bool correctAimDirection = true;
	bool canRecieveDamage = true;
	bool canChangeColor = true;
	bool canSummonGnome = true;
	bool protectingGnome = false;

	float degree_vision = 30;
	float vision_distance = 5;
	float spell_distance = 3;
	float close_distance = 1;
	float forget_distance = 10;

	bool visionState = false;
	float timeNoVision = 0;
	float timeToForget = 5;

	bool canPush = true;
	float timeCDpushspell = 2.0f;

	float time_moving_comb = 2;
	float time_rand_move = 3;
	float time_load_spell_id1 = 2;
	float cdSummonGnome = 10;
	float timeProtectingGnome = 10;
	float timeToDisappear = 3;

	bool firstDeathFrame = true;
	bool ragdollActived = false;
	bool canDisappear = false;
	float force_push_spell = 1;
	float push_damage = 5;
	float force_cooldown = 0.5f;
	float impacted_timer = 0.5f;

	float prob_push_spell = 0;
	float prob_step_back = 0;

	bool cdFireball = true;
	float timeCDFireball = 5.0;

	float randDir = 1;

	float rand_dir = 0;

	float knockBackOnHit = 20.0f;
	float velToDieByGnome = 5.0f;

	bool adapted = false;
	std::string renderMaterial = "data/materials/standard.material";
	std::string prefabBook;
	std::string charge_spell_particle = "data/particles/prefabs_particles/default_particle.json";
	std::string fireball_explosion_particles = "data/particles/prefabs_particles/GoblinDeath3.json";
	std::string mage_push_particle = "data/particles/prefabs_particles/MagePushWave.json";

	std::vector<CHandle> objectsDropped;

	bool viewingplayer = false;
	float timeviewingplayer = 1.0;
	bool hitted = false;
	float timehitted = 1.0;
	bool pushingplayer = false;
	float timepushingplayer = 1.0f;
	bool throwingspell = false;
	float timethrowingspell = 1.0f;
	float timetospawnfireball = 1.0f;
	float timetoshootfireball = 1.0f;
	float timetopushplayer = 1.0f;

	TMsgDamage lastMsg;
	physx::PxRaycastBuffer hit;
	VEC3 lastHitVel = VEC3(0, 0, 0);

	//to render debug
	bool editorMode = false;
	bool going_back = false;
	float timeColorChanged = 1;

	VEC3 debugDst = VEC3(0, 0, 0);
	VEC3 debugPosCrowd = VEC3(0, 0, 0);
	const char* target;

	CHandle player;
	CHandle gnomeProtected;

	int nWaypoints = 0;
	int currWaypoint = 0;

	Studio::EventInstance* walkSound = EngineAudio.createEvent("event:/Movement/MageSteps");
	Studio::EventInstance* whispers = EngineAudio.createEvent("event:/Mage/MageWhispers");

	DECL_SIBLING_ACCESS();
	void rcvDmgMsg(const TMsgDamage& msg);
	void onAlarmRecieve(const TMsgAlarmClock& msg);
	void onEntityCreated(const TMsgEntityCreated& msg);
	void onDetectPlayer(const TMsgDetectPlayer& msg);
	void onAddedCrown(const TMsgOnAddedCrown& msg);
	void onContact(const TMsgOnContact & msg);

	//Auxiliar enemyCounter
	bool auxFight = false;

public:
	bt_mage();

	void load(const json& j, TEntityParseContext& ctx);
	void renderDebug();
	void debugInMenu();

	void create(string); //Init

	int actionCinematicMove();
	int actionCinematicIdle();
	int actionFreeCoverageByDeath();
	int actionDying();
	int actionTakeDamageMage();
	int actionFillCoveragePosition();
	int actionChaseCoverage();
	int actionChase();
	int actionPatrol();
	int actionRandomMove();
	int actionStepBack();
	int actionPushSpell();
	int actionLoadSpell();
	int actionShootFireSpell();
	int actionWait();
	int actionLoadSummonSpell();
	int actionSummonGnomeSpell();
	int actionChaseGnome();
	int actionChaseProtectGnome();
	int actionRandomMoveSupport();
	int actionWaitSupport();
	int actionFreeCoverageByForget();
	int actionChaseGate();
	int actionKeepGate();

	bool conditionCinematicController();
	bool conditionCinematicMove();
	bool conditionDeath();
	bool conditionTakeDamageMage();
	bool conditionCombat();
	bool conditionFillCoveragePosition();
	bool conditionChaseCoverage();
	bool conditionChase();
	bool conditionTakeDistance();
	bool conditionStepBack();
	bool conditionFireMage();
	bool conditionSupportMage();
	bool conditionShoot();
	bool conditionRandomMove();
	bool conditionSummonGnome();
	bool conditionProtect();
	bool conditionRandomMoveSupport();
	bool conditionFreeCoverageByForget();
	bool conditionPatrol();
	bool conditionChaseGate();

	static void registerMsgs();

	void drawConeVision(float near_distance, float far_distance, VEC4 color);
	void changeVariable(const char* varName, float value, bool isBool);
	bool CheckViewPlayer();
	bool ViewShoot();
	int genericLoadSpell();
	void unprotectGnome();
	void chaseProtectedGnome(bool isCovered);
	int genericWait();
	void LeaveBook();

	bool Move(VEC3 dst, VEC3* lookAt = nullptr);
	void setAgentState(bool state);
	void ChangeSpeed(float desiredSpeed);
	void CheckDiedByOtherGnome(VEC3 velImpact);
	void BlendSpeedAnimation();
	void ActivateRagdoll();
	void WatchingPlayer();

	void SpawnFireball();
	void ShootFireball();
	void PushingSpell();

	float walkSpeed = 5.f;
	float runSpeed = 15.f;
	float actualSpeed = 1.0f;

	VEC3 velAgent = VEC3(0, 0, 0);
	float agentRadius = 0.6;
	float lastWeight = 0;
	float weightDebug = 0;
	float speedDebug = 0;

	//LUA
	void setMageCinematicController(bool newCinematicController) {
		cinematicController = newCinematicController;
		if (!cinematicController) resetState = true;
	}
	void moveMageCinematic(VEC3 destination);
	void setBerserkMode(bool mode) { BerserkMode = mode; }
	void setViewPlayer(bool newView);

	//

	~bt_mage();
};