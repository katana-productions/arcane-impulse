#include "mcv_platform.h"
#include "bt_mage.h"
#include "comp_enemy_stats.h"
#include "components/common/comp_transform.h"
#include "components/player/comp_player_stats.h"
#include "comp_enemy_stats.h"
#include "physics/module_physics.h"
#include "entity/entity_parser.h"
#include "engine.h"
#include "components/common/comp_render.h"
#include "components/common/comp_damage_by_objects.h"
#include "components/controllers/comp_char_controller.h"
#include "components/common/comp_fireball.h"
#include "render/meshes/collision_mesh.h"
#include "components/common/comp_fsm.h"
#include "comp_waypoints.h"
#include "skeleton/comp_skel_lookat.h"
#include "components/shaders/comp_dissolve.h"
#include "components/common/comp_group.h"
#include "skeleton/comp_skeleton.h"
#include "components/common/comp_particle_move.h"


DECL_OBJ_MANAGER("bt_mage", bt_mage);

using namespace physx;

bt_mage::bt_mage(){}

void bt_mage::registerMsgs() {
	DECL_MSG(bt_mage, TMsgDamage, rcvDmgMsg);
	DECL_MSG(bt_mage, TMsgAlarmClock, onAlarmRecieve);
	DECL_MSG(bt_mage, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(bt_mage, TMsgDetectPlayer, onDetectPlayer);
	DECL_MSG(bt_mage, TMsgOnAddedCrown, onAddedCrown);
	DECL_MSG(bt_mage, TMsgOnContact, onContact);
}

void bt_mage::rcvDmgMsg(const TMsgDamage& msg) {
	if (!canRecieveDamage) return;
	canRecieveDamage = false;
	EngineAlarms.AddAlarm(impacted_timer, 2, CHandle(this));
	TCompTransform* trans = get<TCompTransform>();


	EngineAudio.playEvent3D("event:/Mage/MageHurt", trans, 1);

	//alert AI where player is
	if(!view) WatchingPlayer();

	lastMsg = msg;
	lastHitVel = msg.vel;
	damaged = true;
	forget = false;
	firstMoveDone = false;
	resetState = true;

	if (currentFireBall.isValid()) {
		CEntity* fb = currentFireBall;
		TCompFireballController* fb_controller = fb->get< TCompFireballController>();
		if (!fb_controller->GetIsActivate()) fb_controller->ExplodeFireball();
	}

	CEntity* e_bullet = msg.h_bullet;
	if (e_bullet) {
		TCompCollider* c_bullet = e_bullet->get<TCompCollider>();
		if (c_bullet) {
			PxRigidDynamic* rb_src = c_bullet->actor->is<PxRigidDynamic>();
			if (rb_src) {
				lastHitVel = PXVEC3_TO_VEC3(rb_src->getLinearVelocity());
			}
		}
	}
}

void bt_mage::onContact(const TMsgOnContact& msg) {
	CHandle myHandle = CHandle(this);
	if (!myHandle.getOwner().isValid()) return;

	//Die By Ragdoll
	if (!msg.source.getOwner().isValid()) {
		CheckDiedByOtherGnome(msg.vel);
		return;
	}
}

void bt_mage::onDetectPlayer(const TMsgDetectPlayer& msg) {
	TCompTransform* c_trans = get<TCompTransform>();
	VEC3 player_pos = EngineBasics.GetPlayerPos(player);
	if (c_trans->accurateDistance(player_pos) < forget_distance) {
		WatchingPlayer();
	}
}

void bt_mage::onAlarmRecieve(const TMsgAlarmClock& msg) {
	if (msg.id == 1) {
		canPush = true;
	}
	else if (msg.id == 2) {
		canRecieveDamage = true;
	}
	else if (msg.id == 3) {
		walking = false;
	}
	else if (msg.id == 4) {
 		loadingSpell = false;
	}
	else if (msg.id == 5) {
		canChangeColor = true;
	}
	else if (msg.id == 6) {
		canSummonGnome = true;
	}
	else if (msg.id == 7) { 
		protectingGnome = false;
	}
	else if (msg.id == 8) {
		canDisappear = true;
	}
	else if (msg.id == 9) {
		cdFireball = true;
	}
	else if (msg.id == 10) {
		viewingplayer = false;
	}
	else if (msg.id == 11) {
		hitted = false;
	}
	else if (msg.id == 12) {
		pushingplayer = false;
	}
	else if (msg.id == 13) {
		throwingspell = false;
	}
	else if (msg.id == 14) {
		SpawnFireball();
	}
	else if (msg.id == 215) {
		ShootFireball();
	}
	else if (msg.id == 16) {
		PushingSpell();
	}
	else if (msg.id == 17) {
		correctAimDirection = false;
	}
	else if (msg.id == 111) {
		TCompSystemParticleWorldMove* part = get<TCompSystemParticleWorldMove>();
		if (part) part->ActivateParticles();
	}
}

void bt_mage::onAddedCrown(const TMsgOnAddedCrown& msg) {
	crowdIdx = msg.idx;
}

void bt_mage::onEntityCreated(const TMsgEntityCreated& msg) {
	TCompTransform* c_trans = get<TCompTransform>();
	
	TCompWaypoints* c_waypoints = get<TCompWaypoints>();

	EngineAudio.playEvent3D("event:/Mage/MageWhispers", c_trans, 1, whispers);

	if (c_waypoints != nullptr) {
		waypoints = c_waypoints->getAllWaypoints();
		nWaypoints = c_waypoints->getWaypointNumber();
	}

	if(nWaypoints > 0) EngineBasics.AdaptPatrolToOrigin(c_trans, &waypoints);
	else EngineBasics.SetGateKeeperPosition(c_trans, &gatePosition, &lookAtKeepingGate);

	player = getEntityByName("Player");
	EngineEnemyManager.addMageFighterSBB(CHandle(this).getOwner());

	create("bt_mage");
}

void bt_mage::load(const json& j, TEntityParseContext& ctx) {
	degree_vision = j.value("degree_vision", degree_vision);
	vision_distance = j.value("vision_distance", vision_distance);
	spell_distance = j.value("spell_distance", spell_distance);
	close_distance = j.value("close_distance", close_distance);
	forget_distance = j.value("forget_distance", forget_distance);
	time_moving_comb = j.value("time_moving_comb", time_moving_comb);
	time_load_spell_id1 = j.value("time_load_spell_id1", time_load_spell_id1);
	force_push_spell = j.value("force_push_spell", force_push_spell);
	force_cooldown = j.value("timeCDpushspell", force_cooldown);
	prob_push_spell = j.value("prob_push_spell", prob_push_spell);
	prob_step_back = j.value("prob_step_back", prob_step_back);
	knockBackOnHit = j.value("knockBackOnHit", knockBackOnHit);
	push_damage = j.value("push_damage", push_damage);
	fireMage = j.value("fireMage", false);
	supportMage = j.value("supportMage", false);
	cdSummonGnome = j.value("cdSummonGnome", cdSummonGnome);
	timeProtectingGnome = j.value("timeProtectingGnome", timeProtectingGnome);
	timeToForget = j.value("timeToForget", timeToForget);
	walkSpeed = j.value("walkSpeed", walkSpeed);
	runSpeed = j.value("runSpeed", runSpeed);
	agentRadius = j.value("agentRadius", agentRadius);
	velToDieByGnome = j.value("velToDieByGnome", velToDieByGnome);
	timeToDisappear = j.value("timeToDisappear", timeToDisappear);
	prefabBook = j.value("prefabBook", prefabBook);
	timeCDFireball = j.value("timeCDFireball", timeCDFireball);
	timeviewingplayer = j.value("timeviewingplayer", timeviewingplayer);
	timehitted = j.value("timehitted", timehitted);
	timepushingplayer = j.value("timepushingplayer", timepushingplayer);
	timethrowingspell = j.value("timethrowingspell", timethrowingspell);
	timetospawnfireball = j.value("timetospawnfireball", timetospawnfireball);
	timetoshootfireball = j.value("timetoshootfireball", timetoshootfireball);
	timetopushplayer = j.value("timetopushplayer", timetopushplayer);
	editorMode = j.value("editorMode", editorMode);
	charge_spell_particle = j.value("charge_spell_particle", charge_spell_particle);
	fireball_explosion_particles = j.value("fireball_explosion_particles", fireball_explosion_particles);
	timeCDpushspell = j.value("timeCDpushspell", timeCDpushspell);
	EngineBasics.LoadWaypointsFromJSON(j, &waypoints, &nWaypoints);
}

void bt_mage::create(string s)
{
	name = s;
	//Llamadas createRoot y addChild tienen que hacerse en orden de recorrido del arbol.
	createRoot("mage", PRIORITY, NULL, NULL);

	addChild("mage", "cinematicController", PRIORITY, (btcondition)&bt_mage::conditionCinematicController, NULL);
	addChild("mage", "freeCoverageByDeath", DECORATOR, (btcondition)&bt_mage::conditionDeath, (btaction)&bt_mage::actionFreeCoverageByDeath);
	addChild("mage", "takeDamage", ACTION, (btcondition)&bt_mage::conditionTakeDamageMage, (btaction)&bt_mage::actionTakeDamageMage); player = getEntityByName("Player");
	addChild("mage", "combat", PRIORITY, (btcondition)&bt_mage::conditionCombat, NULL);
	addChild("mage", "freeCoverageByForget", ACTION, (btcondition)&bt_mage::conditionFreeCoverageByForget, (btaction)&bt_mage::actionFreeCoverageByForget);
	addChild("mage", "idle", PRIORITY, NULL, NULL);

	addChild("cinematicController", "cinematicMove", ACTION, (btcondition)&bt_mage::conditionCinematicMove, (btaction)&bt_mage::actionCinematicMove);
	addChild("cinematicController", "cinematicIdle", ACTION, NULL, (btaction)&bt_mage::actionCinematicIdle);

	addChild("freeCoverageByDeath", "Dying", ACTION, NULL, (btaction)&bt_mage::actionDying);

	addChild("combat", "fillCoveragePosition", ACTION, (btcondition)&bt_mage::conditionFillCoveragePosition, (btaction)&bt_mage::actionFillCoveragePosition); 
	addChild("combat", "chaseCoverage", ACTION, (btcondition)&bt_mage::conditionChaseCoverage, (btaction)&bt_mage::actionChaseCoverage);
	addChild("combat", "FireMage", PRIORITY, (btcondition)&bt_mage::conditionFireMage, NULL);
	addChild("combat", "SupportMage", PRIORITY, (btcondition)&bt_mage::conditionSupportMage, NULL);


	//FireMage
	addChild("FireMage", "chase", ACTION, (btcondition)&bt_mage::conditionChase, (btaction)&bt_mage::actionChase);
	addChild("FireMage", "takeDistance", PRIORITY, (btcondition)&bt_mage::conditionTakeDistance, NULL);
	addChild("FireMage", "combatDecision", PRIORITY, NULL, NULL);

	addChild("takeDistance", "stepBack", ACTION, (btcondition)&bt_mage::conditionStepBack, (btaction)&bt_mage::actionStepBack);
	addChild("takeDistance", "pushSpell", ACTION, NULL, (btaction)&bt_mage::actionPushSpell);

	addChild("combatDecision", "shoot", SEQUENCE, (btcondition)&bt_mage::conditionShoot, NULL);
	addChild("combatDecision", "displacement", PRIORITY, NULL, NULL);

	addChild("shoot", "loadSpell", ACTION, NULL, (btaction)&bt_mage::actionLoadSpell);
	addChild("shoot", "shootFireSpell", ACTION, NULL, (btaction)&bt_mage::actionShootFireSpell);

	addChild("displacement", "randomMove", ACTION, (btcondition)&bt_mage::conditionRandomMove, (btaction)&bt_mage::actionRandomMove);
	addChild("displacement", "wait", ACTION, NULL, (btaction)&bt_mage::actionWait);

	//SupportMage
	addChild("SupportMage", "summon", SEQUENCE, (btcondition)&bt_mage::conditionSummonGnome, NULL);
	addChild("SupportMage", "protect", SEQUENCE, (btcondition)&bt_mage::conditionProtect, NULL);
	addChild("SupportMage", "randomMoveSupport", ACTION, (btcondition)&bt_mage::conditionRandomMoveSupport, (btaction)&bt_mage::actionRandomMoveSupport);
	addChild("SupportMage", "waitSupport", ACTION, NULL, (btaction)&bt_mage::actionWaitSupport);

	addChild("summon", "loadSummonSpell", ACTION, NULL, (btaction)&bt_mage::actionLoadSummonSpell);
	addChild("summon", "summoGnomeSpell", ACTION, NULL, (btaction)&bt_mage::actionSummonGnomeSpell);

	addChild("protect", "chaseGnome", ACTION, NULL, (btaction)&bt_mage::actionChaseGnome);
	addChild("protect", "chaseProtectGnome", ACTION, NULL, (btaction)&bt_mage::actionChaseProtectGnome);

	addChild("idle", "patrol", ACTION, (btcondition)&bt_mage::conditionPatrol, (btaction)&bt_mage::actionPatrol);
	addChild("idle", "gateKeeper", PRIORITY, NULL, NULL);

	addChild("gateKeeper", "chaseGate", ACTION, (btcondition)&bt_mage::conditionChaseGate, (btaction)&bt_mage::actionChaseGate);
	addChild("gateKeeper", "keepGate", ACTION, NULL, (btaction)&bt_mage::actionKeepGate);
}

int bt_mage::actionCinematicMove() {
	cinematicMove = false;
	TCompTransform* c_trans = get< TCompTransform>();
	ChangeSpeed(walkSpeed);
	Move(cinematicDestination);
	float debugDSt = c_trans->accurateDistance(cinematicDestination);
	if (c_trans->accurateDistance(cinematicDestination) > 2)  return STAY;
	else return LEAVE;
}

int bt_mage::actionCinematicIdle() {
	changeVariable("mage_speed", 0, false);
	TCompTransform* c_trans = get<TCompTransform>();
	TCompCharController* enemy_controller = get<TCompCharController>();
	VEC3 player_pos = EngineBasics.GetPlayerPos(player);

	enemy_controller->lookAt(player_pos);
	ChangeSpeed(0);

	return LEAVE;
}

int bt_mage::actionFreeCoverageByDeath() {
	if (covered) {
		covered = false;
		EngineEnemyManager.SetFreeStrategicPositionSSB(CHandle(this).getOwner());
	}
	return LEAVE;
}

int bt_mage::actionDying() {

	if (firstDeathFrame) {
		firstDeathFrame = false;
		if (gnomeProtected.isValid()) {
			unprotectGnome();
		}

		whispers->stop(FMOD_STUDIO_STOP_ALLOWFADEOUT);
		TCompTransform* trans = get<TCompTransform>();
		EngineAudio.playEvent3D("event:/Mage/MageDeath", trans, 3);

		EngineEnemyManager.eraseMageFighterSBB(CHandle(this).getOwner());
		EngineAlarms.AddAlarm(timeToDisappear * 0.7, 8, CHandle(this));
		//Comptador de musica
		if (auxFight) {
			EnginePersistence.setEnemyCounter(-1);
			auxFight = false;
		}

		if (!ragdollActived)
			ActivateRagdoll();

		// Activate magic dissolve
		TCompDissolve *d = get< TCompDissolve >();
		if (d) {
			d->setDissolveTime(timeToDisappear);
			d->setEnabled(true);
		}
	}

	if (canDisappear) {
		TCompTransform* trans = get<TCompTransform>();
		VEC3 tPos = trans->getPosition();
		VEC3 upVec = trans->getUp();
		upVec.Normalize();
		upVec *= 0.5f;
		trans->setPosition(tPos + upVec + VEC3(0.0f, 1.0f, 0.0f));
		EngineParticles.CreateOneShootSystemParticles(fireball_explosion_particles.c_str(), trans);
		EngineAudio.playEvent3D("event:/Enemy/RagdollExplosion", trans, 3);
		EngineNavMesh.removeAgent(crowdIdx);
		EngineEnemyManager.EraseMeleeFighterSBB(CHandle(this).getOwner());
		EngineBasics.Destroy(CHandle(this).getOwner());
	}
	return LEAVE;
}

int bt_mage::actionTakeDamageMage() {
	TCompCharController* enemy_controller = get<TCompCharController>();
	CEntity* e_player = player;
	TCompTransform* player_trans = e_player->get<TCompTransform>();
	TCompTransform* c_trans = get<TCompTransform>();

	if (gnomeProtected.isValid()) {
		unprotectGnome();
	}

	VEC3 dir = lastHitVel;
	dir.Normalize();
	if (enemy_controller) enemy_controller->addExternForce(dir * knockBackOnHit);

	TCompEnemyStats* e_stats = get<TCompEnemyStats>();
	if (!e_stats->isAlive())
		death = true;

	TCompSkeleton* c_skeleton = get< TCompSkeleton>();
	c_skeleton->playActionAnimation("mage_hit", 0.4, 0.0, 1.0);
	EngineAlarms.AddAlarm(timehitted, 11, CHandle(this));
	hitted = true;

	changeVariable("mage_left", 0, true);
	changeVariable("mage_right", 0, true);
	changeVariable("mage_speed", 0, false);

	return LEAVE;
}

int bt_mage::actionFillCoveragePosition() {
	covered = true;
	return LEAVE;
}

int bt_mage::actionChaseCoverage() {
	Move(strategicPosition);

	return LEAVE;
}

int bt_mage::actionChase() 
{
	if (firstMoveDone)
		firstMoveDone = false;

	VEC3 player_pos = EngineBasics.GetPlayerPos(player);
	Move(player_pos);
	return LEAVE;
}

int bt_mage::actionRandomMove() {

	TCompCharController* enemy_controller = get<TCompCharController>();
	TCompTransform* c_trans = get<TCompTransform>();

	VEC3 player_pos = EngineBasics.GetPlayerPos(player);

	float distanceToPlayer = c_trans->accurateDistance(player_pos);
	if (distanceToPlayer < close_distance || distanceToPlayer > spell_distance) {
		going_back = false;
		firstMoveDone = false;
		EngineNavMesh.setAgentPos(crowdIdx, c_trans->getPosition());
		EngineNavMesh.setAgentDirection(crowdIdx, c_trans->getFront());

		if (randDir == 1.0)
			changeVariable("mage_left", 0, true);
		else
			changeVariable("mage_right", 0, true);

		return LEAVE;
	}

	if (!firstMoveDone) {
		walking = true;
		firstMoveDone = true;
		randDir = randomInt(0, 1) * 2 - 1.0f;
		if(randDir == 1.0)
			changeVariable("mage_left", 1, true);
		else
			changeVariable("mage_right", 1, true);

		EngineAlarms.AddAlarm(time_rand_move, 3, CHandle(this));
	}

	if (walking) {
		going_back = true;

		VEC3 direction = c_trans->getPosition() + c_trans->getLeft() * randDir * 5;
		if (!Move(direction, &player_pos)) {
			randDir *= -1;
			if (randDir == 1.0) {
				changeVariable("mage_right", 0, true);
				changeVariable("mage_left", 1, true);
			}
			else {
				changeVariable("mage_left", 0, true);
				changeVariable("mage_right", 1, true);
			}
		}

		return STAY;
	}
	else {
		going_back = false;
		firstMoveDone = false;
		EngineNavMesh.setAgentPos(crowdIdx, c_trans->getPosition());
		EngineNavMesh.setAgentDirection(crowdIdx, c_trans->getFront());

		if (randDir == 1.0)
			changeVariable("mage_left", 0, true);
		else
			changeVariable("mage_right", 0, true);

		return LEAVE;
	}
}

int bt_mage::actionStepBack() {
	TCompCharController* enemy_controller = get<TCompCharController>();
	TCompTransform* c_trans = get<TCompTransform>();

	VEC3 player_pos = EngineBasics.GetPlayerPos(player);

	if ((c_trans->accurateDistance(player_pos) < close_distance && canPush) || c_trans->accurateDistance(player_pos) > spell_distance)
	{
		walking = false;
		firstMoveDone = false;
		going_back = false;

		EngineNavMesh.setAgentPos(crowdIdx, c_trans->getPosition());
		EngineNavMesh.setAgentDirection(crowdIdx, c_trans->getFront());
		return LEAVE;
	}

	if (!firstMoveDone) {
		walking = true;
		firstMoveDone = true;
		EngineAlarms.AddAlarm(time_moving_comb, 3, CHandle(this));
	}

	if (walking) {
		going_back = true;
		VEC3 direction = c_trans->getPosition() - c_trans->getFront() * 5;
		Move(direction, &player_pos);
		return STAY;
	}
	else {
		going_back = false;
		firstMoveDone = false;
		EngineNavMesh.setAgentPos(crowdIdx, c_trans->getPosition());
		EngineNavMesh.setAgentDirection(crowdIdx, c_trans->getFront());
		return LEAVE;
	}
}

int bt_mage::actionPushSpell() {	
	TCompSkeleton* c_skeleton = get< TCompSkeleton>();
	c_skeleton->playActionAnimation("mage_wind", 0.1, 0.25, 1.0);

	pushingplayer = true;
	canPush = false;
	EngineAlarms.AddAlarm(timeCDpushspell, 1, CHandle(this));
	EngineAlarms.AddAlarm(timepushingplayer, 12, CHandle(this));
	EngineAlarms.AddAlarm(timetopushplayer, 16, CHandle(this));

	return LEAVE;
}

int bt_mage::actionLoadSpell() {
	return genericLoadSpell();
}

int bt_mage::actionShootFireSpell() {
	TCompSkeleton* c_skeleton = get< TCompSkeleton>();
	c_skeleton->playActionAnimation("mage_fireball_throw", 0.4, 0.2, 2.0);
	
	EngineAlarms.AddAlarm(timetoshootfireball, 215, CHandle(this));
	EngineAlarms.AddAlarm(timethrowingspell, 13, CHandle(this));
	throwingspell = true;

	return LEAVE;
}

int bt_mage::actionWait() {
	return genericWait();
}

int bt_mage::actionLoadSummonSpell() {
	return genericLoadSpell();
}

int bt_mage::actionSummonGnomeSpell() {
	canSummonGnome = false;
	EngineAlarms.AddAlarm(cdSummonGnome, 6, CHandle(this));

	TCompTransform* c_trans = get<TCompTransform>();
	VEC3 pos_loaded = c_trans->getPosition() + c_trans->getFront() * 3;
	CHandle h_loaded = EngineBasics.Create("data/prefabs/enemies/gnome.json", c_trans, pos_loaded);
	CEntity* e_loaded = h_loaded;
	
	TMsgDetectPlayer msg;
	e_loaded->sendMsg(msg);

	return LEAVE;
}

int bt_mage::actionChaseGnome() {
	if (!gnomeProtected.isValid() || covered)
		return LEAVE;

	TCompTransform* c_trans = get<TCompTransform>();

	CEntity* e_gnome = gnomeProtected;
	TCompTransform* gnome_trans = e_gnome->get<TCompTransform>();
	VEC3 gnome_pos = gnome_trans->getPosition();

	Move(gnome_pos);

	if (c_trans->accurateDistance(gnome_pos) < spell_distance) {
		return LEAVE;
	}
	return STAY;
}

int bt_mage::actionChaseProtectGnome() {
	if (!gnomeProtected.isValid()) {
		protectingGnome = false;
		firstMoveDone = false;
		return LEAVE;
	}

	CEntity* e_gnome = gnomeProtected;
		chaseProtectedGnome(covered);
	
	if (!firstMoveDone) {
		protectingGnome = true;
		firstMoveDone = true;

		EngineAlarms.AddAlarm(timeProtectingGnome, 7, CHandle(this));
	}

	if (protectingGnome) {
		return STAY;
	}
	else {
		firstMoveDone = false;
		unprotectGnome();
		return LEAVE;
	}
}

int bt_mage::actionRandomMoveSupport() {
	VEC3 player_pos = EngineBasics.GetPlayerPos(player);

	Move(player_pos);
	return LEAVE;
}

int bt_mage::actionWaitSupport() {
	return genericWait();
}

int bt_mage::actionPatrol() {
	if (EnginePhysics.isPaused) return LEAVE;

	TCompTransform* c_trans = get<TCompTransform>();

	Move(waypoints[currWaypoint]);

	if (c_trans->manhattanDistance(waypoints[currWaypoint]) < 1.5f) {
		currWaypoint = (currWaypoint + 1) % nWaypoints;
	}

	return LEAVE;
}

int bt_mage::actionChaseGate() {
	if (EnginePhysics.isPaused) return LEAVE;

	Move(gatePosition);

	return LEAVE;
}

int bt_mage::actionFreeCoverageByForget() {
	covered = false;
	EngineEnemyManager.SetFreeStrategicPositionSSB(CHandle(this).getOwner());
	return LEAVE;
}

int bt_mage::actionKeepGate() {
	TCompTransform* c_trans = get< TCompTransform>();

	Move(c_trans->getPosition(), &lookAtKeepingGate);

	return LEAVE;
}

bool bt_mage::conditionCinematicController() {
	return cinematicController;
}

bool bt_mage::conditionCinematicMove() {
	return cinematicMove;
}

bool bt_mage::conditionDeath() {
	return death;
}

bool bt_mage::conditionTakeDamageMage() {
	if (damaged) {
		damaged = false;
		return true;
	}
	return false;
}

bool bt_mage::conditionCombat() {
	TCompTransform* c_trans = get<TCompTransform>();
	VEC3 player_pos = EngineBasics.GetPlayerPos(player);
	visionState = CheckViewPlayer();

	if ((!view && c_trans->getConeVision(player_pos, deg2rad(degree_vision), vision_distance) && visionState) || BerserkMode && !view) {
		view = true;
		EngineEnemyManager.WarnOtherEnemies();
		WatchingPlayer();
	}

	if (visionState == false) {
		timeNoVision += deltaT;
	}
	else {
		timeNoVision = 0;
	}

	if (((view && timeNoVision > timeToForget) || c_trans->accurateDistance(player_pos) > forget_distance) && !BerserkMode)
	{
		if (view) {
			if (auxFight) {
				EnginePersistence.setEnemyCounter(-1);
				auxFight = false;
			}
		}

		view = false;
		if (crowdMovement) {
			setAgentState(false);
		}
	}

	return view;
}

bool bt_mage::conditionFillCoveragePosition() {
	if (covered || !optimization)
		return false;

	strategicPosition = EngineEnemyManager.OccupyStrategicPositionSBB(CHandle(this).getOwner(), spell_distance);
	if (strategicPosition == VEC3(0, 0, 0))
		return false;
	return true;
}

bool bt_mage::conditionChaseCoverage() {
	TCompTransform* c_trans = get<TCompTransform>();

	if (covered && c_trans->accurateDistance(strategicPosition) >2.5) return true;
	else return false;
}

bool bt_mage::conditionChase() {
	TCompTransform* c_trans = get<TCompTransform>();
	VEC3 player_pos = EngineBasics.GetPlayerPos(player);

	return (!covered && (c_trans->accurateDistance(player_pos) > spell_distance || !ViewShoot()));
}

bool bt_mage::conditionTakeDistance() {
	TCompTransform* c_trans = get<TCompTransform>();
	VEC3 player_pos = EngineBasics.GetPlayerPos(player);

	return c_trans->accurateDistance(player_pos) < close_distance && CheckViewPlayer() && !viewingplayer && !hitted && !pushingplayer && !throwingspell;
}

bool bt_mage::conditionStepBack() {
	TCompTransform* c_trans = get<TCompTransform>();
	VEC3 player_pos = EngineBasics.GetPlayerPos(player);

	return !canPush;
}


bool bt_mage::conditionFireMage() {
	return fireMage;
}

bool bt_mage::conditionSupportMage() {
	return supportMage;
}

bool bt_mage::conditionShoot() {
	if (cdFireball && !viewingplayer && !hitted && !pushingplayer && !throwingspell) {
		float var = randomFloat(-0.25 * timeCDFireball, 0.25 * timeCDFireball);
		EngineAlarms.AddAlarm(timeCDFireball + var, 9, CHandle(this));
		cdFireball = false;
		return true;
	}
	return false;
}

bool bt_mage::conditionRandomMove() {
	return !covered && !viewingplayer && !hitted && !pushingplayer && !throwingspell;
}

bool bt_mage::conditionSummonGnome() {
	return  canSummonGnome;
}

bool bt_mage::conditionProtect() {
	gnomeProtected = EngineEnemyManager.ProtectMeleeFighterSBB();
	
	if (gnomeProtected.isValid()) {
		return true;
	}
	return false;
}

bool bt_mage::conditionRandomMoveSupport() {
	return !covered;
}

bool bt_mage::conditionFreeCoverageByForget() {
	return covered;
}

bool bt_mage::conditionPatrol() {
	if (nWaypoints > 0) return true;
	return false;
}

bool bt_mage::conditionChaseGate() {
	TCompTransform* c_trans = get<TCompTransform>();
	if (c_trans->manhattanDistance(gatePosition) > 1.5) return true;
	return false;
}

void bt_mage::debugInMenu() {
	ImGui::DragFloat("DegreeVision", &degree_vision, 0.1f, 0.f, 360.f);
	ImGui::DragFloat("CloseDistance", &close_distance, 0.1f, 0.f, 100);
	ImGui::DragFloat("SpellDistance", &spell_distance, 0.1f, 0.f, 100);
	ImGui::DragFloat("VisionDistance", &vision_distance, 0.1f, 0.f, 100.f);
	ImGui::DragFloat("ForgetDistance", &forget_distance, 0.1f, 0.f, 200.f);
	ImGui::DragFloat("TimeMovingComb", &time_moving_comb, 0.1f, 0.f, 10.f);
	ImGui::DragFloat("TimeLoadSpellId1", &time_load_spell_id1, 0.1f, 0.f, 10.f);
	ImGui::DragFloat("ForcePushSpell", &force_push_spell, 1.f, 0.f, 100.f);
	ImGui::DragFloat("PushDamage", &push_damage, 0.1f, 0.f, 100.f);
	ImGui::DragFloat("ProbPushSpell", &prob_push_spell, 1.f, 0.f, 100.f);
	ImGui::DragFloat("ProbStepBack", &prob_step_back, 1.f, 0.f, 100.f);
	ImGui::DragFloat("knockBackOnHit", &knockBackOnHit, 1.f, 0.f, 100.f);
	ImGui::DragFloat("cdSummonGnome", &cdSummonGnome, 1.f, 0.f, 100.f);
	ImGui::DragFloat("timeProtectingGnome", &timeProtectingGnome, 1.f, 0.f, 100.f);

	ImGui::Text("RayCast view: %s", target);
}

void bt_mage::renderDebug() {
	TCompTransform* c_trans = get<TCompTransform>();
	VEC3 c_pos = c_trans->getPosition();

	//draw vision distances
	drawCircle(c_pos, close_distance, VEC4(0, 1, 0, 1));
	drawCircle(c_pos, spell_distance, VEC4(1, 0, 0, 1));
	drawCircle(c_pos, forget_distance, VEC4(0.5, 0.5, 0.5, 1));
	drawConeVision(0, vision_distance, VEC4(1, 1, 0, 1));

	//draw states and directions
	if (nWaypoints > 0) drawCircle(waypoints[currWaypoint], 3.0f, VEC4(1, 1, 1, 1));
	else drawCircle(gatePosition, 3.0f, VEC4(1, 1, 1, 1));
	if (rand_dir != NULL)	drawLine(c_pos, c_pos + c_trans->getVectorFromAngle(deg2rad(rand_dir)) * 10, VEC4(1, 0, 0, 1));
	if (going_back)	drawLine(c_pos, c_pos + c_trans->getFront()* -1 * 10, VEC4(0, 1, 0, 1));
	if (loadingSpell) {
		drawCircle(c_pos, 1, VEC4(1, 0, 1, 1));
		drawCircle(c_pos, 2, VEC4(0.8, 0.2, 0.2, 1));
		drawCircle(c_pos, 3, VEC4(0.2, 0.8, 0, 1));
		drawCircle(c_pos, 4, VEC4(0.1, 0.3, 0.9, 1));
		drawCircle(c_pos, 5, VEC4(0.2, 0, 0.7, 1));
	}

	drawCircle(debugPosCrowd, 0.7, VEC4(1, 0, 1, 1));
}

void bt_mage::drawConeVision(float near_distance, float far_distance, VEC4 color)
{
	TCompTransform* c_trans = get<TCompTransform>();
	VEC3 c_pos = c_trans->getPosition();
	drawLine(c_pos + c_trans->getVectorFromAngle(deg2rad(degree_vision))*(near_distance / cos(deg2rad(degree_vision))), c_pos + c_trans->getVectorFromAngle(deg2rad(degree_vision))*(far_distance / cos(deg2rad(degree_vision))), color);
	drawLine(c_pos + c_trans->getVectorFromAngle(deg2rad(-degree_vision))*(near_distance / cos(deg2rad(degree_vision))), c_pos + c_trans->getVectorFromAngle(deg2rad(-degree_vision))*(far_distance / cos(deg2rad(degree_vision))), color);
	drawLine(c_pos + c_trans->getVectorFromAngle(deg2rad(degree_vision))*(far_distance / cos(deg2rad(degree_vision))), c_pos + c_trans->getVectorFromAngle(deg2rad(-degree_vision))*(far_distance / cos(deg2rad(degree_vision))), color);
}

void bt_mage::changeVariable(const char* varName, float value, bool isBool) {
	TCompFSM* cFsm = get<TCompFSM>();
	if (cFsm)
	{
		cFsm->changeVariable(varName, value, isBool);
	}
}

bool bt_mage::CheckViewPlayer() {
	TCompTransform* c_trans = get<TCompTransform>();
	VEC3 pos = c_trans->getPosition();

	VEC3 player_pos = EngineBasics.GetPlayerPos(player);

	VEC3 offset = VEC3(0, 1.8, 0);
	VEC3 player_offset = VEC3(0, 1.0, 0);

	VEC3 raycast_direction = (player_pos + player_offset) - (pos + offset);
	raycast_direction.Normalize();

	physx::PxScene* scene = EnginePhysics.getPhysxScene();

	PxQueryFilterData filter_data = PxQueryFilterData();
	filter_data.data.word0 = ~(CModulePhysics::FilterGroup::Objects | CModulePhysics::FilterGroup::Triggers | CModulePhysics::FilterGroup::Enemy);

	hit = PxRaycastBuffer();

	const PxHitFlags outputFlags =
		PxHitFlag::eDISTANCE
		| PxHitFlag::ePOSITION
		| PxHitFlag::eNORMAL
		;

	pos += offset;
	bool ray_cast_impact = scene->raycast(
		VEC3_TO_PXVEC3(pos),
		VEC3_TO_PXVEC3(raycast_direction),
		forget_distance,
		hit,
		outputFlags,
		filter_data
	);

	bool viewPlayer = false;
	if (ray_cast_impact) {
		CHandle targetHdl;
		targetHdl.fromVoidPtr(hit.block.actor->userData);
		if (targetHdl.getOwner().isValid())
		{
			CEntity *targetEntity = targetHdl.getOwner();
			target = targetEntity->getName();
			if (std::string(target) == "Player")
				viewPlayer = true;
		}
	}
	return viewPlayer;
}

bool bt_mage::ViewShoot() {
	TCompTransform* c_trans = get<TCompTransform>();
	VEC3 pos = c_trans->getPosition();

	VEC3 player_pos = EngineBasics.GetPlayerPos(player);

	VEC3 offset = VEC3(0, 1.8, 0);
	VEC3 player_offset = VEC3(0, 1.0, 0);
	VEC3 raycast_direction = (player_pos + player_offset) - (pos + offset);

	VEC3 raycast_direction_right = (player_pos + player_offset) - (pos - c_trans->getLeft() + offset);
	VEC3 raycast_direction_left = (player_pos + player_offset) - (pos + c_trans->getLeft() + offset);

	raycast_direction.Normalize();
	raycast_direction_right.Normalize();
	raycast_direction_left.Normalize();

	physx::PxScene* scene = EnginePhysics.getPhysxScene();

	PxQueryFilterData filter_data = PxQueryFilterData();
	filter_data.data.word0 = ~(CModulePhysics::FilterGroup::Objects | CModulePhysics::FilterGroup::Triggers | CModulePhysics::FilterGroup::Enemy);

	physx::PxRaycastBuffer hit_right = PxRaycastBuffer();
	physx::PxRaycastBuffer hit_left = PxRaycastBuffer();

	hit = PxRaycastBuffer();
	hit_right = PxRaycastBuffer();
	hit_left = PxRaycastBuffer();

	const PxHitFlags outputFlags =
		PxHitFlag::eDISTANCE
		| PxHitFlag::ePOSITION
		| PxHitFlag::eNORMAL
		;

	pos += offset;
	VEC3 pos_right = pos - c_trans->getLeft();
	VEC3 pos_left = pos + c_trans->getLeft();

	bool ray_cast_impact = scene->raycast(
		VEC3_TO_PXVEC3(pos),
		VEC3_TO_PXVEC3(raycast_direction),
		forget_distance,
		hit,
		outputFlags,
		filter_data
	);

	bool ray_cast_impact_right = scene->raycast(
		VEC3_TO_PXVEC3(pos_right),
		VEC3_TO_PXVEC3(raycast_direction_right),
		forget_distance,
		hit_right,
		outputFlags,
		filter_data
	);

	bool ray_cast_impact_left = scene->raycast(
		VEC3_TO_PXVEC3(pos_left),
		VEC3_TO_PXVEC3(raycast_direction_left),
		forget_distance,
		hit_left,
		outputFlags,
		filter_data
	);

	bool enable_attack = false;
	if (ray_cast_impact && ray_cast_impact_right && ray_cast_impact_left) {
		CHandle targetHdl;
		CHandle targetHdlright;
		CHandle targetHdlleft;

		targetHdl.fromVoidPtr(hit.block.actor->userData);
		targetHdlright.fromVoidPtr(hit_right.block.actor->userData);
		targetHdlleft.fromVoidPtr(hit_left.block.actor->userData);


		if (targetHdl.getOwner().isValid() && targetHdlright.getOwner().isValid() && targetHdlleft.getOwner().isValid())
		{
			CEntity *targetEntity = targetHdl.getOwner();
			target = targetEntity->getName();

			CEntity *targetEntity_right = targetHdlright.getOwner();
			const char* target_right = targetEntity_right->getName();

			CEntity *targetEntity_left = targetHdlleft.getOwner();
			const char* target_left = targetEntity_left->getName();

			if (std::string(target) == "Player" && std::string(target_right) == "Player" && std::string(target_left) == "Player")
				enable_attack = true;
		}
	}
	return enable_attack;
}

int bt_mage::genericLoadSpell() {
	TCompCharController* enemy_controller = get<TCompCharController>();
	TCompTransform* c_trans = get<TCompTransform>();

	VEC3 player_pos = EngineBasics.GetPlayerPos(player);

	Move(c_trans->getPosition(), &player_pos);


	if (!firstMoveDone) {
		currentFireBall = CHandle();
		loadingSpell = true;
		correctAimDirection = true;
		firstMoveDone = true;
		EngineAlarms.AddAlarm(time_load_spell_id1, 4, CHandle(this));
		EngineAlarms.AddAlarm(time_load_spell_id1 * 0.8, 17, CHandle(this));
		EngineAlarms.AddAlarm(timetospawnfireball, 14, CHandle(this));

		TCompSkeleton* c_skeleton = get< TCompSkeleton>();
		c_skeleton->playActionAnimation("mage_fireball_cast", 0.2, 0.2, 1.0);
	}

	if (loadingSpell) {

		if (currentFireBall.isValid() && correctAimDirection) {
			CEntity* fb = currentFireBall;
			TCompTransform* fb_trans = fb->get< TCompTransform>();
			VEC3 currentPos = fb_trans->getPosition();

			TCompTransform* c_trans = get<TCompTransform>();
			VEC3 pos = c_trans->getPosition();
			VEC3 player_pos = EngineBasics.GetPlayerPos(player);

			VEC3 dir = player_pos - pos;
			dir.Normalize();

			VEC3 newPos = pos + (-c_trans->getLeft() * 0.5) + dir * 2.5 + VEC3(0, 3, 0);
			newPos = VEC3::Lerp(currentPos, newPos, deltaT * 10);

			fb_trans->setPosition(newPos);
		}
		return STAY;
	}
	else {
		firstMoveDone = false;
		return LEAVE;
	}
}

void bt_mage::unprotectGnome() {
	EngineEnemyManager.UnProtectMeleeFighterSBB(gnomeProtected);
	gnomeProtected = CHandle();
}

void bt_mage::chaseProtectedGnome(bool isCovered) {
	TCompTransform* c_trans = get<TCompTransform>();
	TCompCharController* enemy_controller = get<TCompCharController>();
	VEC3 pos = c_trans->getPosition();

	CEntity* e_gnome = gnomeProtected;
	TCompTransform* gnome_trans = e_gnome->get<TCompTransform>();
	VEC3 gnome_pos = gnome_trans->getPosition();

	if (!isCovered) {
		if (c_trans->accurateDistance(gnome_pos) > spell_distance / 2) {;
			Move(gnome_pos, &gnome_pos);
		}
		else if (c_trans->accurateDistance(gnome_pos) < spell_distance / 2) {
			VEC3 direction = pos - gnome_pos;
			direction.Normalize();
			Move(direction, &gnome_pos);
		}
	}
	else {
		Move(c_trans->getPosition(), &gnome_pos);
	}
}

int bt_mage::genericWait() {
	TCompCharController* enemy_controller = get<TCompCharController>();
	TCompTransform* c_trans = get<TCompTransform>();

	VEC3 player_pos = EngineBasics.GetPlayerPos(player);
	Move(c_trans->getPosition(), &player_pos);
	return LEAVE;
}

void bt_mage::CheckDiedByOtherGnome(VEC3 velImpact) {
	PxShape* shapes_controller[1];
	TCompCollider* c_collider = get<TCompCollider>();
	c_collider->actor->getShapes(shapes_controller, sizeof(shapes_controller), 0);
	PxFilterData simulationQueryDataController = shapes_controller[0]->getQueryFilterData();
	if (simulationQueryDataController.word1 & CModulePhysics::FilterGroup::Bones) {	
		TMsgDamage damage_msg;
		damage_msg.h_sender = CHandle(this).getOwner();
		damage_msg.h_bullet = CHandle(this).getOwner();
		damage_msg.damage = 10;
		damage_msg.vel = velImpact;

		CEntity* objective = CHandle(this).getOwner();
		objective->sendMsg(damage_msg);
	}
	
}

bool bt_mage::Move(VEC3 dst, VEC3* lookAt) {
	TCompTransform* c_trans = get<TCompTransform>();
	TCompCharController* enemy_controller = get<TCompCharController>();
	VEC3 pos = c_trans->getPosition();
	VEC3 moveDst = dst;
	bool existPath = true;
	bool paused = false;
	EngineAudio.playEvent3D("event:/Movement/MageSteps", c_trans, 1, walkSound);
	walkSound->getPaused(&paused);

	if (crowdMovement) {
		VEC3 agentPos = EngineNavMesh.getAgentPosition(crowdIdx);
		if (VEC3::Distance(pos, agentPos) > 3.0 || VEC3::Distance(agentPos, dst) < 2.0) {
			EngineNavMesh.setAgentPos(crowdIdx, pos);
		}

		ChangeSpeed(runSpeed);
		EngineNavMesh.moveAgentCrowd(crowdIdx, dst);
		VEC3 posCrowd = EngineNavMesh.getAgentPosition(crowdIdx);
		debugPosCrowd = posCrowd;
		moveDst = posCrowd;
		BlendSpeedAnimation();

		//check if exist path
		bool pathfinding = EngineNavMesh.findPath(pos, dst, &path);
		if (pathfinding && path.size() > 1)
			existPath = true;
		else
			existPath = false;
	}
	else {
		EngineBasics.FixToGround(c_trans, &pos, &dst);
		bool pathfinding = EngineNavMesh.findPath(pos, dst, &path);
		if (pathfinding && path.size() > 1)
			moveDst = path[1];
	}

	if (c_trans->accurateDistance(dst) > 2) {
		VEC3 direction;
		direction = moveDst - pos;
		changeVariable("mage_speed", 1, false);
		if (direction.Length() > 1)
			direction.Normalize();

		if (paused) 
			walkSound->setPaused(false);
		
		if (!lookAt)
			enemy_controller->newMoveDirection(direction, true);
		else
			enemy_controller->newMoveAndLook(direction, *lookAt);
	}
	else {
		if (!paused) 
			walkSound->setPaused(true);
		
		changeVariable("mage_speed", 0, false);
		if (!lookAt)
			enemy_controller->newMoveDirection(VEC3(0, 0, 0));
		else
			enemy_controller->newMoveAndLook(VEC3(0, 0, 0), *lookAt);

		if (crowdMovement) {
			EngineNavMesh.setAgentPos(crowdIdx, pos);
			EngineNavMesh.setAgentDirection(crowdIdx, c_trans->getFront());
		}
	}
	return existPath;
}

void bt_mage::setAgentState(bool state) {
	TCompTransform* c_trans = get<TCompTransform>();
	TCompCharController* c_controller = get<TCompCharController>();
	TCompSkelLookAt* c_lookAt = get< TCompSkelLookAt>();
	if (state) {
		crowdMovement = true;
		crowdIdx = EngineNavMesh.addAgent(c_trans->getPosition(), runSpeed, agentRadius);
		EngineNavMesh.setAgentPos(crowdIdx, c_trans->getPosition());
		EngineNavMesh.setAgentDirection(crowdIdx, c_trans->getFront());
		if(c_lookAt)
			c_lookAt->setAmount(1);
		ChangeSpeed(runSpeed);
		changeVariable("mage_combat", 1, true);
	}
	else {
		crowdMovement = false;
		EngineNavMesh.setAgentState(crowdIdx, false);
		if(c_lookAt)
			c_lookAt->setAmount(0);
		ChangeSpeed(walkSpeed);
		changeVariable("mage_combat", 0, true);
	}
}

void bt_mage::ChangeSpeed(float desiredSpeed) {
	if (actualSpeed != desiredSpeed) {
		TCompCharController* c_controller = get<TCompCharController>();
		EngineNavMesh.changeAgentSpeed(crowdIdx, desiredSpeed);
		c_controller->setMaxSpeed(desiredSpeed);
		actualSpeed = desiredSpeed;
	}
}

void bt_mage::ActivateRagdoll() {
	TCompRagdoll* c_ragdoll = get<TCompRagdoll>();
	if (c_ragdoll) {
		c_ragdoll->activateRagdoll();
		ragdollActived = true;

		EngineAlarms.AddAlarm(1.75f, 111, CHandle(this));
	}
	LeaveBook();
}

void bt_mage::LeaveBook() {
	TCompGroup* c_group = get<TCompGroup>();
	std::vector<CHandle> handles = c_group->handles;

	for (int i = 0; i < handles.size(); i++) {
		CEntity* e = handles[i];
		TCompTransform* c_trans = e->get<TCompTransform>();
		VEC3 pos = c_trans->getPosition();

		float yaw, pitch, roll;
		c_trans->getAngles(&yaw, &pitch, &roll);

		CEntity* ebook= EngineBasics.Instantiate(prefabBook, pos, yaw, pitch, roll);
		objectsDropped.push_back(ebook);

		TCompCollider* collider = ebook->get<TCompCollider>();
		if (collider) {
			PxRigidDynamic* rb = collider->actor->is<PxRigidDynamic>();
			rb->addTorque(VEC3_TO_PXVEC3(VEC3(randomRange(-100, 100), randomRange(-100, 100), randomRange(-100, 100))));
		}

		handles[i].destroy();
	}
}

void bt_mage::WatchingPlayer() {
	if (!auxFight) {
		EnginePersistence.setEnemyCounter(1);
		auxFight = true;
	}

	view = true;
	viewingplayer = true;
	EngineAlarms.AddAlarm(timeviewingplayer, 10, CHandle(this));

	TCompSkeleton* c_skeleton = get< TCompSkeleton>();
	c_skeleton->playActionAnimation("mage_spotplayer", 0.0, 0.5, 1.0);
	if (!crowdMovement) {
		setAgentState(true);
	}
}


void bt_mage::SpawnFireball() {
	TCompTransform* c_trans = get<TCompTransform>();
	VEC3 pos = c_trans->getPosition();
	VEC3 player_pos = EngineBasics.GetPlayerPos(player);
	
	VEC3 dir = player_pos - pos;
	dir.Normalize();

	VEC3 currentPos = pos +  (- c_trans ->getLeft() * 0.65) +  dir * 2.5 + VEC3(0, 3, 0);

	EngineAudio.playEvent3D("event:/Mage/MageSpell", c_trans, 1);

	std::vector<CHandle> v_particle_systems = EngineParticles.CreateOneShootSystemParticles(charge_spell_particle.c_str(), c_trans, currentPos);
	for (auto particle_system : v_particle_systems) {
			CEntity* e_loaded = particle_system;
			TCompBuffers* c_buffer = e_loaded->get<TCompBuffers>();
			if (!c_buffer) continue;
			auto cte_buffer = c_buffer->getCteByName("TCtesParticles");
			TCtesParticles* particles_buffer = dynamic_cast<TCtesParticles*>(cte_buffer);

			EngineParticles.UpdateConstantBuffer(cte_buffer);
	}

	currentFireBall = EngineBasics.Create("data/prefabs/fireball.json", c_trans, currentPos);
}

void bt_mage::ShootFireball() {
	if (editorMode) {
		currentFireBall.destroy();
		return;
	}

	TCompTransform* c_trans = get<TCompTransform>();
	CEntity* e_loaded = currentFireBall;
	if (!e_loaded) return;

	TMsgAssignBulletOwner msg;
	msg.h_owner = CHandle(this).getOwner();
	msg.source = c_trans->getPosition();
	msg.front = VEC3(0,0,0);

	e_loaded->sendMsg(msg);
}

void bt_mage::PushingSpell() {
	TCompTransform* c_trans = get<TCompTransform>();
	VEC3 c_pos = c_trans->getPosition();

	CEntity* e_player = player;
	TCompCharController* player_controller = e_player->get<TCompCharController>();
	VEC3 player_pos = EngineBasics.GetPlayerPos(player);

	VEC3 force_vector = (player_pos - c_pos);
	force_vector.Normalize();
	force_vector *= force_push_spell;

	player_controller->addExternForce(force_vector);

	TMsgDamage damage_msg;
	damage_msg.h_sender = CHandle(this).getOwner();
	damage_msg.h_bullet = CHandle(this).getOwner();
	damage_msg.damage = push_damage;
	e_player->sendMsg(damage_msg);

	TCompTransform* trans = get<TCompTransform>();
	VEC3 tPos = trans->getPosition();
	VEC3 upVec = trans->getUp();
	upVec.Normalize();
	upVec *= 0.5f;
	tPos = tPos + upVec + VEC3(0.0f, 1.0f, 0.0f);
	EngineParticles.CreateOneShootSystemParticles(mage_push_particle.c_str(), trans, tPos);
}

void bt_mage::BlendSpeedAnimation() {

}

void bt_mage::moveMageCinematic(VEC3 destination) {
	cinematicMove = true;
	cinematicDestination = destination;
}

void bt_mage::setViewPlayer(bool newView) {
	if (newView) WatchingPlayer();
	else view = newView;
}

bt_mage::~bt_mage() {}
