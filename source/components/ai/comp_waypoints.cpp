#include "mcv_platform.h"
#include "comp_waypoints.h"
#include "components/common/comp_transform.h"
#include "entity/entity_parser.h"
#include "engine.h"

DECL_OBJ_MANAGER("comp_waypoints", TCompWaypoints);

void TCompWaypoints::registerMsgs() {
}

void TCompWaypoints::load(const json& j, TEntityParseContext& ctx) {
	EngineBasics.LoadWaypointsFromJSON(j, &waypoints, &nWaypoints);
	//Load names
	json jNames = j["names"];
	for (json::iterator it = jNames.begin(); it != jNames.end(); ++it) {
		std::string n = it.value().get<std::string>();
		names.push_back(n);
	}

}

TCompWaypoints::~TCompWaypoints() {
	CHandle ent;
	for (auto it = names.begin(); it != names.end(); ++it) {
		std::string n = *it;
		ent = getEntityByName(n);
		ent.destroy();
	}
}




