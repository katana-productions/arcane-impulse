#include "mcv_platform.h"
#include "comp_enemy_stats.h"
#include "entity/entity_parser.h"

DECL_OBJ_MANAGER("enemy_stats", TCompEnemyStats);


void TCompEnemyStats::debugInMenu() {
	ImGui::DragFloat("Health", &health, 0.1f, 0.f, max_health);
}

void TCompEnemyStats::load(const json& j, TEntityParseContext& ctx) {
	max_health = j.value("max_health", max_health);
	health = max_health;
}

void TCompEnemyStats::registerMsgs() {
	DECL_MSG(TCompEnemyStats, TMsgDamage, rcvDmgMsg);
}

void TCompEnemyStats::rcvDmgMsg(const TMsgDamage& msg) {
	health -= msg.damage;
}

bool TCompEnemyStats::isAlive() {
	if (health > 0)
		return true;
	else
		return false;
}


