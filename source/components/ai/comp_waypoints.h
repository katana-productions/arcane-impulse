#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

class TCompWaypoints : public TCompBase
{
	std::vector<std::string> names;
	std::vector<VEC3> waypoints;

	int nWaypoints = 0;
	

public:
	static void registerMsgs();
	void load(const json& j, TEntityParseContext& ctx);

	std::vector<VEC3> getAllWaypoints() { return waypoints; }
	VEC3 getWaypoint(int index) { return waypoints[index]; }

	std::vector<std::string> getAllNames() { return names; }
	std::string getName(int index) { return names[index]; }

	int getWaypointNumber() { return nWaypoints; }

	~TCompWaypoints();
};