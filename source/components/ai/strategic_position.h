#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

class TCompStrategicPosition : public TCompBase
{
	DECL_SIBLING_ACCESS();
	void onEntityCreated(const TMsgEntityCreated& msg);

public:
	static void registerMsgs();
};