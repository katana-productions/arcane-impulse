#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

class TCompEnemyStats : public TCompBase
{
	float max_health = 100;
	float health = 100;

	DECL_SIBLING_ACCESS();
	void rcvDmgMsg(const TMsgDamage& msg);

public:
	void debugInMenu();
	void load(const json& j, TEntityParseContext& ctx);
	bool isAlive();

	static void registerMsgs();

};