#include "mcv_platform.h"
#include "bt_gnome.h"
#include "components/common/comp_transform.h"
#include "components/common/comp_name.h"
#include "components/player/comp_player_stats.h"
#include "physics/module_physics.h"
#include "entity/entity_parser.h"
#include "engine.h"
#include "components/common/comp_render.h"
#include "components/common/comp_damage_by_objects.h"
#include "components/controllers/comp_char_controller.h"
#include "components/common/comp_fireball.h"
#include "render/meshes/collision_mesh.h"
#include "components/common/comp_fsm.h"
#include "skeleton/comp_skeleton.h"
#include "comp_waypoints.h"
#include "physics/comp_ragdoll.h"
#include "components/common/comp_group.h"
#include "components/common/comp_particle_move.h"
#include "skeleton/comp_skel_lookat.h"

#include "components/shaders/comp_magic_outline.h"
#include "components/shaders/comp_dissolve.h"

DECL_OBJ_MANAGER("bt_gnome", bt_gnome);

bt_gnome::bt_gnome() {}

void bt_gnome::registerMsgs() {
	DECL_MSG(bt_gnome, TMsgDamage, onImpact);
	DECL_MSG(bt_gnome, TMsgOnContact, onContact);
	DECL_MSG(bt_gnome, TMsgIntObjState, onPowerMessage);
	DECL_MSG(bt_gnome, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(bt_gnome, TMsgDetectPlayer, onDetectPlayer);
	DECL_MSG(bt_gnome, TMsgMagicProtection, onGnomeProtected);
	DECL_MSG(bt_gnome, TMsgAlarmClock, onAlarmRecieve);
	DECL_MSG(bt_gnome, TMsgOnControllerHit, onControllerHit);
	DECL_MSG(bt_gnome, TMsgOnAddedCrown, onAddedCrown);
}

void bt_gnome::onAlarmRecieve(const TMsgAlarmClock& msg) {
	if (msg.id == 101) {
		cdAttack = true;
	}
	else if (msg.id == 102) {
		canDisappear = true;
	}
	else if (msg.id == 103) {
		SendDamage();
	}
	else if (msg.id == 104) {
		isTaunt = false;
		firstTauntFrame = true;
		changeVariable("goblin_taunt", 0, true);
	}
	else if (msg.id == 105) {
		attacking = false;
	}
	else if (msg.id == 106) {
		viewingPlayer = false;
	}
	else if (msg.id == 107) {
		isStep = true;
	}
	else if (msg.id == 108) {
		isTalk = true;
	}
	else if (msg.id == 109) {
		isPulled = true;
	}
	else if (msg.id == 110) {
		checkPath = true;
	}
	else if (msg.id == 111) {
		TCompSystemParticleWorldMove* part = get<TCompSystemParticleWorldMove>();
		if (part) part->ActivateParticles();
	}
}

void bt_gnome::onImpact(const TMsgDamage& msg) {
	if (isProtected || gnomeKilled) return;

	CEntity* e_bullet = msg.h_bullet;
  TCompName *name = e_bullet->get<TCompName>();

	if (name != nullptr and name->getName() == "sword" and !EngineEnemyManager.oneGoblinToGoblinDeath())
		return;

	gnomeKilled = true;
	resetState = true;

	if (e_bullet) {
		TCompCollider* c_bullet = e_bullet->get<TCompCollider>();
		if (c_bullet) {
			PxRigidDynamic* rb_src = c_bullet->actor->is<PxRigidDynamic>();
			if (rb_src) {
				TCompCharController* c_controller = get<TCompCharController>();
				c_controller->addExternForce(PXVEC3_TO_VEC3(rb_src->getLinearVelocity()) * attenuationFactor);
			}
		}
	}
}

void bt_gnome::onContact(const TMsgOnContact& msg) {
	CHandle myHandle = CHandle(this);
	if (!myHandle.getOwner().isValid()) return;

	DeathChecks(msg.source, msg.vel);
}

void bt_gnome::onControllerHit(const TMsgOnControllerHit& msg) {
	CEntity* e = msg.h_other;
	if (!e)
		return;

	bt_gnome* other_gnome = e->get<bt_gnome>();
	if (!other_gnome || ragdollActived)
		return;

	TCompCharController* c_controller = get<TCompCharController>();
	TCompCharController* other_controller = other_gnome->get<TCompCharController>();

	if (other_gnome->GetPowerAffected() && !powerAffected) {
		c_controller->addExternForce(other_controller->GetExternForce());
		powerAffected = true;
		lastState = state;
		state = Pushed;
		resetState = true;
		return;
	}

	if (other_gnome->GetInIdleWar() && !powerAffected && CHandle(other_gnome).getOwner() != CHandle(this).getOwner()) {
		isTaunt = true;
		return;
	}
}

void bt_gnome::onPowerMessage(const TMsgIntObjState& msg) {
	if (isProtected)
		return;

	powerAffected = true;
	lastState = state;
	state = msg.state;

	resetState = true;
}

void bt_gnome::DeathChecks(CHandle sourceImpact, VEC3 velImpact)
{
	if (isProtected)
		return;

	if (ragdollActived) {
		gnomeKilled = true;
		resetState = true;
		return;
	}

	if (!sourceImpact.getOwner().isValid()) {
		CheckDiedByOtherGnome(velImpact);
		return;
	}

	CEntity* hitEntity = (CEntity*)sourceImpact.getOwner();
	TCompFireballController* fireball = hitEntity->get<TCompFireballController>();
	if (fireball) {
		gnomeKilled = true;
		resetState = true;
		return;
	}
}

void bt_gnome::onDetectPlayer(const TMsgDetectPlayer& msg) {
	TCompTransform* c_trans = get<TCompTransform>();
	VEC3 player_pos = EngineBasics.GetPlayerPos(player);

	if (c_trans->manhattanDistance(player_pos) < forget_distance) {
		WatchingPlayer();
	}
}

void bt_gnome::onGnomeProtected(const TMsgMagicProtection& msg) {
	isProtected = msg.isProtected;
	TCompName *name = get<TCompName>();
	if (isProtected) name->setTag(Tag::NoInteractable);
	else name->setTag(Tag::Interactable);
}

void bt_gnome::onAddedCrown(const TMsgOnAddedCrown& msg) {
	crowdIdx = msg.idx;
}

void bt_gnome::onEntityCreated(const TMsgEntityCreated& msg) {
	TCompTransform* c_trans = get<TCompTransform>();
	TCompWaypoints* c_waypoints = get<TCompWaypoints>();

	if (c_waypoints != nullptr) {
		waypoints = c_waypoints->getAllWaypoints();
		nWaypoints = c_waypoints->getWaypointNumber();
	}

	if (nWaypoints == 0)
		EngineBasics.SetGateKeeperPosition(c_trans, &gatePosition, &lookAtKeepingGate);

	player = getEntityByName("Player");
	EngineEnemyManager.addMeleeFighterSBB(CHandle(this).getOwner());
	create("bt_gnome");
}

void bt_gnome::load(const json& j, TEntityParseContext& ctx) {
	degreesVision = j.value("degree_vision", degreesVision);
	vision_distance = j.value("vision_distance", vision_distance);
	forget_distance = j.value("forget_distance", forget_distance);
	damageExplosion = j.value("damageExplosion", damageExplosion);
	forceExplosion = j.value("force_explosion", forceExplosion);
	explosionRadius = j.value("explosionRadius", explosionRadius);
	walkSpeed = j.value("walkSpeed", walkSpeed);
	runSpeed = j.value("runSpeed", runSpeed);
	relaxSpeed = j.value("relaxSpeed", relaxSpeed);
	minKnockBack = j.value("minknockback", minKnockBack);
	explosiveGnome = j.value("explosiveGnome", explosiveGnome);
	attackDistance = j.value("attackDistance", attackDistance);
	shortAttackDistance = j.value("shortAttackDistance", shortAttackDistance);
	damageAttack = j.value("damageAttack", damageAttack);
	cdAttackTime = j.value("cdAttackTime", cdAttackTime);
	timeToDisappear = j.value("timeToDisappear", timeToDisappear);
	prefabSword = j.value("prefabSword", prefabSword);
	velToDieByGnome = j.value("velToDieByGnome", velToDieByGnome);
	timeToForget = j.value("timeToForget", timeToForget);
	tauntingTime = j.value("tauntingTime", tauntingTime);
	agentRadius = j.value("agentRadius", agentRadius);
	timeToViewPlayer = j.value("timeToViewPlayer", timeToViewPlayer);
	knockbackForce = j.value("knockbackForce", knockbackForce);
	fireball_explosion_particles = j.value("fireball_explosion_particles", fireball_explosion_particles);
}

void bt_gnome::create(string s)
{
	name = s;
	//Llamadas createRoot y addChild tienen que hacerse en orden de recorrido del arbol.
	createRoot("gnome", PRIORITY, NULL, NULL);

	addChild("gnome", "cinematicController", PRIORITY, (btcondition)&bt_gnome::conditionCinematicController, NULL);
	addChild("gnome", "die", PRIORITY, (btcondition)&bt_gnome::conditionGnomeKilled, NULL);
	addChild("gnome", "recoverFromPower", ACTION, (btcondition)&bt_gnome::conditionRecoverFromPower, (btaction)&bt_gnome::actionRecoverFromPower);
	addChild("gnome", "powerAffected", ACTION, (btcondition)&bt_gnome::conditionPowerAffected, (btaction)&bt_gnome::actionPowerAffected);
	addChild("gnome", "taunt", ACTION, (btcondition)&bt_gnome::conditionTaunt, (btaction)&bt_gnome::actionTaunt);
	addChild("gnome", "combat", PRIORITY, (btcondition)&bt_gnome::conditionView, NULL);
	addChild("gnome", "idle", PRIORITY, NULL, NULL);

	addChild("cinematicController", "cinematicMove", ACTION, (btcondition)&bt_gnome::conditionCinematicMove, (btaction)&bt_gnome::actionCinematicMove);
	addChild("cinematicController", "cinematicIdle", ACTION, NULL, (btaction)&bt_gnome::actionCinematicIdle);

	addChild("die", "dyingExplodeGnome", ACTION, (btcondition)&bt_gnome::conditionExplodeGnome, (btaction)&bt_gnome::actionExplodeGnome);
	addChild("die", "dyingGnome", ACTION, NULL, (btaction)&bt_gnome::actionDying);

	addChild("combat", "chooseAttack", PRIORITY, (btcondition)&bt_gnome::conditionAttack, NULL);
	addChild("combat", "idleWar", ACTION, (btcondition)&bt_gnome::conditionIdleWar, (btaction)&bt_gnome::actionIdleWar);
	addChild("combat", "chase", ACTION, NULL, (btaction)&bt_gnome::actionChase);

	addChild("chooseAttack", "shortAttack", ACTION, (btcondition)&bt_gnome::conditionShortAttack, (btaction)&bt_gnome::actionShortAttack);
	addChild("chooseAttack", "longAttack", ACTION, NULL, (btaction)&bt_gnome::actionLongAttack);

	addChild("idle", "patrol", ACTION, (btcondition)&bt_gnome::conditionPatrol, (btaction)&bt_gnome::actionPatrol);
	addChild("idle", "gateKeeper", PRIORITY, NULL, NULL);

	addChild("gateKeeper", "chaseGate", ACTION, (btcondition)&bt_gnome::conditionChaseGate, (btaction)&bt_gnome::actionChaseGate);
	addChild("gateKeeper", "keepGate", ACTION, NULL, (btaction)&bt_gnome::actionKeepGate);

	srand(time(NULL));
}

int bt_gnome::actionCinematicMove() {
	cinematicMove = false;
	TCompTransform* c_trans = get< TCompTransform>();
	ChangeSpeed(relaxSpeed);
	Move(cinematicDestination);
	float debugDSt = c_trans->accurateDistance(cinematicDestination);
	if (c_trans->accurateDistance(cinematicDestination) > 3)  return STAY;
	else return LEAVE;
}

int bt_gnome::actionCinematicIdle() {
	TCompTransform* c_trans = get<TCompTransform>();
	TCompCharController* enemy_controller = get<TCompCharController>();

	enemy_controller->lookAt(c_trans->getFront());
	ChangeSpeed(0);
	changeVariable("goblin_speed", 0, false);

	return LEAVE;
}

int bt_gnome::actionExplodeGnome() {
	TCompTransform* c_trans = get<TCompTransform>();
	if (c_trans->accurateDistance(EngineBasics.GetPlayerPos(player)) < 100.0f) EngineBasics.ShakeCamera(0.5f, 0.2f);
	EngineBasics.CreateExplosionMeshAt(explosionRadius, c_trans->getPosition());
	EnginePhysics.AddExplosion(c_trans->getPosition(), explosionRadius, forceExplosion, damageExplosion, minKnockBack);
	EngineNavMesh.removeAgent(crowdIdx);
	EngineEnemyManager.EraseMeleeFighterSBB(CHandle(this).getOwner());
	EngineBasics.Destroy(CHandle(this).getOwner());
	return LEAVE;
}

int bt_gnome::actionDying() {
	if (firstDeathFrame) {

		if (auxFight) {
			EnginePersistence.setEnemyCounter(-1);
			auxFight = false;
		}

		firstDeathFrame = false;
		inIdleWar = false;

		TCompTransform* trans = get<TCompTransform>();
		EngineAudio.playEvent3D("event:/ScreamEnemy/Goblin_death", trans, 3);

		EngineAlarms.AddAlarm(timeToDisappear * 0.7, 102, CHandle(this));
		if (!ragdollActived)
			ActivateRagdoll();

		// Activate magic dissolve
		TCompDissolve *d = get< TCompDissolve >();
		if (d) {
			d->setDissolveTime(timeToDisappear);
			d->setEnabled(true);
		}
	}

	TCompMagicOutline *m = get< TCompMagicOutline >();
	if (m != nullptr)
		m->setEnabled(false);

	if (canDisappear) {
		TCompTransform* trans = get<TCompTransform>();
		VEC3 tPos = trans->getPosition();
		VEC3 upVec = trans->getUp();
		upVec.Normalize();
		upVec *= 0.5f;
		trans->setPosition(tPos + upVec + VEC3(0.0f, 1.0f, 0.0f));
		EngineParticles.CreateOneShootSystemParticles(fireball_explosion_particles.c_str(), trans);
		EngineAudio.playEvent3D("event:/Enemy/RagdollExplosion", trans, 3);

		TCompSystemParticleWorldMove* part = get<TCompSystemParticleWorldMove>();
		if (part) part->SetEnabled(false);

		EngineNavMesh.removeAgent(crowdIdx);
		EngineEnemyManager.EraseMeleeFighterSBB(CHandle(this).getOwner());
		EngineBasics.Destroy(CHandle(this).getOwner());
	}
	return LEAVE;
}

int bt_gnome::actionRecoverFromPower() {
	TCompCharController* enemy_controller = get<TCompCharController>();
	TCompTransform* c_trans = get<TCompTransform>();
	powerAffected = false;

	EngineNavMesh.setAgentPos(crowdIdx, c_trans->getPosition());
	changeVariable("goblin_fly", 0, true);

	//TCompRagdoll* c_ragdoll = get<TCompRagdoll>();
	//if (!ragdollActived && lastState == ObjState::Pulled && (state == ObjState::Pushed /*|| state == ObjState::Normal*/)) {
	//	ActivateRagdoll();
	//}
	return LEAVE;
}

int bt_gnome::actionPowerAffected() {
	TCompRagdoll* c_ragdoll = get<TCompRagdoll>();
	TCompCharController* enemy_controller = get<TCompCharController>();
	CEntity* e_player = player;
	VEC3 player_pos = EngineBasics.GetPlayerPos(player);
	TCompTransform* c_trans = get<TCompTransform>();

	if (isPulled) {
		EngineAudio.playEvent3D("event:/ScreamEnemy/Goblin_power_scream", c_trans, 3);
		isPulled = false;
		EngineAlarms.AddAlarm(7, 109, CHandle(this));
	}

	enemy_controller->lookAt(player_pos);
	changeVariable("goblin_speed", 0, false);
	if (state == ObjState::Pulled) {
		changeVariable("goblin_fly", 1, true);
		EngineEnemyManager.StopCombatMeleeFighterSBB(CHandle(this).getOwner());
	}
	if (!ragdollActived && lastState == ObjState::Pulled && (state == ObjState::Pushed/* || state == ObjState::Normal*/)) {
		ActivateRagdoll();
	}
	inIdleWar = false;
	return LEAVE;
}

int bt_gnome::actionTaunt() {
	changeVariable("goblin_taunt", 1, true);

	if (firstTauntFrame) {
		firstTauntFrame = false;
		EngineAlarms.AddAlarm(tauntingTime, 104, CHandle(this));
	}

	TCompTransform* c_trans = get<TCompTransform>();
	TCompCharController* enemy_controller = get<TCompCharController>();
	VEC3 player_pos = EngineBasics.GetPlayerPos(player);
	enemy_controller->lookAt(player_pos);
	EngineNavMesh.setAgentPos(crowdIdx, c_trans->getPosition());
	EngineNavMesh.setAgentDirection(crowdIdx, c_trans->getFront());

	ChangeSpeed(0);

	VEC3 posCrowd = EngineNavMesh.getAgentPosition(crowdIdx);
	debugPosCrowd = posCrowd;

	return LEAVE;
}

int bt_gnome::actionShortAttack() {
	TCompCharController* enemy_controller = get<TCompCharController>();
	VEC3 player_pos = EngineBasics.GetPlayerPos(player);

	enemy_controller->lookAt(player_pos);

	TCompSkeleton* c_skeleton = get< TCompSkeleton>();
	c_skeleton->playActionAnimation("goblin_attack_short", 0.0, 0.5, 1.0);

	TCompTransform* trans = get<TCompTransform>();
	EngineAudio.playEvent3D("event:/ScreamEnemy/Goblin_attack_scream", trans, 3);

	cdAttack = false;
	attacking = true;
	EngineAlarms.AddAlarm(cdAttackTime, 101, CHandle(this));

	EngineAlarms.AddAlarm(timeToImpactFirstShort, 103, CHandle(this));
	EngineAlarms.AddAlarm(timeToImpactSecondShort, 103, CHandle(this));

	EngineAlarms.AddAlarm(timeShortAttacking, 105, CHandle(this));

	currentAttackType = typeAttack::ShortAttack;

	return LEAVE;
}

int bt_gnome::actionLongAttack() {

	TCompCharController* enemy_controller = get<TCompCharController>();
	VEC3 player_pos = EngineBasics.GetPlayerPos(player);

	enemy_controller->lookAt(player_pos);

	TCompTransform* trans = get<TCompTransform>();
	EngineAudio.playEvent3D("event:/ScreamEnemy/Goblin_attack_scream", trans, 3);

	enemy_controller->newMoveAndLook(VEC3(0, 0, 0), player_pos);
	TCompFSM* cFsm = get<TCompFSM>();
	if (cFsm)
	{
		cFsm->changeVariable("goblin_speed", 0, false);
	}

	TCompSkeleton* c_skeleton = get< TCompSkeleton>();
	c_skeleton->playActionAnimation("goblin_attack_long", 0.0, 0.5, 1.0);

	cdAttack = false;
	attacking = true;
	EngineAlarms.AddAlarm(cdAttackTime, 101, CHandle(this));

	EngineAlarms.AddAlarm(timeToImpactFirstLong, 103, CHandle(this));
	EngineAlarms.AddAlarm(timeToImpactSecondLong, 103, CHandle(this));

	EngineAlarms.AddAlarm(timeLongAttacking, 105, CHandle(this));

	currentAttackType = typeAttack::LongAttack;

	return LEAVE;
}

int bt_gnome::actionIdleWar() {
	inIdleWar = true;
	TCompTransform* c_trans = get<TCompTransform>();
	TCompCharController* enemy_controller = get<TCompCharController>();
	VEC3 player_pos = EngineBasics.GetPlayerPos(player);
	enemy_controller->lookAt(player_pos);
	EngineNavMesh.setAgentPos(crowdIdx, c_trans->getPosition());
	EngineNavMesh.setAgentDirection(crowdIdx, c_trans->getFront());

	if (isTalk) {
		EngineAudio.playEvent3D("event:/ScreamEnemy/Goblin_idle_scream", c_trans, 3);
		isTalk = false;
		int conTime = rand() % 5 + 3;
		EngineAlarms.AddAlarm(conTime, 108, CHandle(this));
	}

	ChangeSpeed(0);
	changeVariable("goblin_speed", 0, false);

	VEC3 posCrowd = EngineNavMesh.getAgentPosition(crowdIdx);
	debugPosCrowd = posCrowd;

	return LEAVE;
}

int bt_gnome::actionChase()
{
	if (EnginePhysics.isPaused) return STAY;
	changeVariable("goblin_combat", 1, true);
	CheckViewPlayer();

	TCompTransform* c_trans = get<TCompTransform>();
	VEC3 player_pos = EngineBasics.GetPlayerPos(player);
	Move(player_pos);

	if (isStep) {
		EngineAudio.playEvent3D("event:/Movement/GoblinSteps", c_trans, 1);
		isStep = false;
		EngineAlarms.AddAlarm(0.2, 107, CHandle(this));
	}

	inIdleWar = false;
	return LEAVE;
}

int bt_gnome::actionPatrol()
{
	PROFILE_FUNCTION("actionPatrol");
	if (EnginePhysics.isPaused) return LEAVE;
	TCompTransform* c_trans = get<TCompTransform>();

	Move(waypoints[currWaypoint]);

	if (isStep) {
		EngineAudio.playEvent3D("event:/Movement/GoblinSteps", c_trans, 1);
		isStep = false;
		EngineAlarms.AddAlarm(0.5, 107, CHandle(this));
	}

	if (c_trans->manhattanDistance(waypoints[currWaypoint]) < 5.0f) {
		currWaypoint = (currWaypoint + 1) % nWaypoints;
	}

	return LEAVE;
}

int bt_gnome::actionChaseGate() {
	if (EnginePhysics.isPaused) return LEAVE;

	EngineBasics.switchRenderState(get<TCompRender>(), 0);

	TCompTransform* c_trans = get<TCompTransform>();
	Move(gatePosition);

	return LEAVE;
}

int bt_gnome::actionKeepGate() {
	PROFILE_FUNCTION("actionKeepGate");
	TCompRender* render = get<TCompRender>();
	if (render == nullptr) return -1;

	EngineBasics.switchRenderState(get<TCompRender>(), 0);

	changeVariable("goblin_speed", 0, false);

	TCompCharController* enemy_controller = get<TCompCharController>();
	enemy_controller->lookAt(lookAtKeepingGate);

	return LEAVE;
}

bool bt_gnome::conditionCinematicController() {
	return cinematicController;
}

bool bt_gnome::conditionCinematicMove() {
	return cinematicMove;
}

bool bt_gnome::conditionGnomeKilled() {
	return gnomeKilled;
}

bool bt_gnome::conditionExplodeGnome() {
	return explosiveGnome;
}

bool bt_gnome::conditionTakePlayerRefence() {
	if (optimization && !player.isValid()) return true;
	return false;
}

bool bt_gnome::conditionRecoverFromPower() {
	TCompCharController* enemy_controller = get<TCompCharController>();
	if (powerAffected && (state == Pushed && enemy_controller->GetExternForce().Length() < 0.1 || state == Normal)) //Recover when has been just Push (NOT PULL)
		return true;
	return false;
}

bool bt_gnome::conditionPowerAffected() {
	return powerAffected || ragdollActived;
}

bool bt_gnome::conditionTaunt() {
	return isTaunt;
}

bool bt_gnome::conditionView()
{
	TCompTransform* c_trans = get<TCompTransform>();
	if (c_trans == nullptr) return false;

	VEC3 player_pos = EngineBasics.GetPlayerPos(player);

	visionState = CheckViewPlayer();

	if (!view && c_trans->getConeVision(player_pos, deg2rad(degreesVision), vision_distance) && visionState || BerserkMode && !view) {
		changeVariable("goblin_combat", 1, true);
		EngineEnemyManager.WarnOtherEnemies();
		WatchingPlayer();
	}

	if (visionState == false) {
		timeNoVision += deltaT;
	}
	else {
		timeNoVision = 0;
	}

	if (((view && timeNoVision > timeToForget) || c_trans->accurateDistance(player_pos) > forget_distance) && !BerserkMode)
	{
		if (view) {
			if (auxFight) {
				EnginePersistence.setEnemyCounter(-1);
				auxFight = false;
			}
		}

			view = false;
		inIdleWar = false;
		changeVariable("goblin_combat", 0, true);
		if (crowdMovement) {
			setAgentState(false);
		}
	}

	return view;
}

bool bt_gnome::conditionAttack() {
	TCompTransform* c_trans = get<TCompTransform>();
	VEC3 player_pos = EngineBasics.GetPlayerPos(player);

	return(!explosiveGnome && c_trans->accurateDistance(player_pos) < attackDistance && cdAttack);
}

bool bt_gnome::conditionShortAttack() {
	TCompTransform* c_trans = get<TCompTransform>();
	VEC3 player_pos = EngineBasics.GetPlayerPos(player);

	return (c_trans->accurateDistance(player_pos) < shortAttackDistance);
}

bool bt_gnome::conditionIdleWar() {
	TCompTransform* c_trans = get<TCompTransform>();
	VEC3 player_pos = EngineBasics.GetPlayerPos(player);
	return(c_trans->accurateDistance(player_pos) < attackDistance || attacking || viewingPlayer);
}

bool bt_gnome::conditionPatrol() {
	if (nWaypoints > 0) return true;
	return false;
}

bool bt_gnome::conditionChaseGate() {
	TCompTransform* c_trans = get<TCompTransform>();
	if (c_trans == nullptr) return false;

	if (c_trans->manhattanDistance(gatePosition) > 1.5) return true;
	return false;
}

void bt_gnome::debugInMenu() {
	ImGui::DragFloat("DegreeVision", &degreesVision, 0.1f, 0.f, 360.f);
	ImGui::DragFloat("VisionDistance", &vision_distance, 0.1f, 0.f, 100.f);
	ImGui::DragFloat("ForgetDistance", &forget_distance, 0.1f, 0.f, 100.f);
	ImGui::DragFloat("ForceExplosion", &forceExplosion, 1.f, 0.f, 400.f);
	ImGui::DragFloat("explosionRadius", &explosionRadius, 0.5f, 0.f, 100.0f);
	ImGui::DragFloat("DamageExplosion", &damageExplosion, 1.f, 0.f, 400.f);
	ImGui::DragFloat("walkSpeed", &walkSpeed, 0.1f, 1.f, 10.f);
	ImGui::DragFloat("runSpeed", &runSpeed, 0.1f, 1.f, 10.f);
	ImGui::DragFloat("relaxSpeed", &relaxSpeed, 0.1f, 1.f, 10.f);
	ImGui::DragFloat("attackDistance", &attackDistance, 0.1f, 1.f, 10.f);
	ImGui::DragFloat("shortAttackDistance", &shortAttackDistance, 0.1f, 1.f, 10.f);
	ImGui::DragFloat("damageAttack", &damageAttack, 0.1f, 1.f, 10.f);
	ImGui::DragFloat("cdAttackTime", &cdAttackTime, 0.1f, 1.f, 10.f);
	ImGui::DragFloat("timeToDisappear", &timeToDisappear, 0.1f, 1.f, 10.f);

	ImGui::Checkbox("ForcingDebugSpeed?", &setSpeed);
	ImGui::DragFloat("speed", &forceSpeed, 0.1f, 1.f, 10.f);


	ImGui::Text("RayCast view: %s", target);
	ImGui::Text("Speed: %f", speedDebug);
	ImGui::Text("Weight: %f", weightDebug);
	ImGui::Text("isTaunt: %d", isTaunt);

}

void bt_gnome::renderDebug() {
	TCompTransform* c_trans = get<TCompTransform>();
	VEC3 c_pos = c_trans->getPosition();
	drawConeVision(0, vision_distance, VEC4(1, 1, 0, 1));
	drawCircle(c_pos, forget_distance, VEC4(0.5, 0.5, 0.5, 1));
	if (nWaypoints > 0) drawCircle(waypoints[currWaypoint], 3.0f, VEC4(1, 1, 1, 1));
	else drawCircle(gatePosition, 3.0f, VEC4(1, 1, 1, 1));

	if (path.size() > 0) {
		for (int i = 0; i < path.size() - 1; i++) {
			drawLine(path[i], path[i + 1], VEC4(0, 1, 1, 1));
		}
	}

	float maxDist = 8.0;
	c_pos += VEC3(0, 0.5, 0);
	drawLine(c_pos + c_trans->getLeft()*0.5, c_pos + c_trans->getLeft()*0.5 + c_trans->getFront() * maxDist, VEC4(1, 0, 0, 1));
	drawLine(c_pos - c_trans->getLeft()*0.5, c_pos - c_trans->getLeft()*0.5 + c_trans->getFront() * maxDist, VEC4(1, 0, 0, 1));
	drawCircle(debugDst, 1, VEC4(0, 1, 0, 1));

	drawCircle(debugPosCrowd, 0.7, VEC4(1, 0, 1, 1));
}

void bt_gnome::drawConeVision(float near_distance, float far_distance, VEC4 color)
{
	TCompTransform* c_trans = get<TCompTransform>();
	VEC3 c_pos = c_trans->getPosition();
	drawLine(c_pos + c_trans->getVectorFromAngle(deg2rad(degreesVision))*(near_distance / cos(deg2rad(degreesVision))), c_pos + c_trans->getVectorFromAngle(deg2rad(degreesVision))*(far_distance / cos(deg2rad(degreesVision))), color);
	drawLine(c_pos + c_trans->getVectorFromAngle(deg2rad(-degreesVision))*(near_distance / cos(deg2rad(degreesVision))), c_pos + c_trans->getVectorFromAngle(deg2rad(-degreesVision))*(far_distance / cos(deg2rad(degreesVision))), color);
	drawLine(c_pos + c_trans->getVectorFromAngle(deg2rad(degreesVision))*(far_distance / cos(deg2rad(degreesVision))), c_pos + c_trans->getVectorFromAngle(deg2rad(-degreesVision))*(far_distance / cos(deg2rad(degreesVision))), color);
}

void bt_gnome::Move(VEC3 dst) {
	TCompTransform* c_trans = get<TCompTransform>();
	TCompCharController* enemy_controller = get<TCompCharController>();
	VEC3 pos = c_trans->getPosition();
	VEC3 moveDst = dst;

	if (crowdMovement) {
		//if agent Pos has arrived destination, reset crowdIdx
		VEC3 agentPos = EngineNavMesh.getAgentPosition(crowdIdx);
		if (VEC3::Distance(pos, agentPos) > 3.0  || VEC3::Distance(agentPos, dst) < 2.0) {
			EngineNavMesh.setAgentPos(crowdIdx, pos);
		}
		ChangeSpeed(runSpeed);

		//MoveAgent
		EngineNavMesh.moveAgentCrowd(crowdIdx, moveDst);
		VEC3 posCrowd = EngineNavMesh.getAgentPosition(crowdIdx);
		debugPosCrowd = posCrowd;
		//Our current Destination is our Agent
		moveDst = posCrowd;

		BlendSpeedAnimation();

		//Every TimeCheckPath, We check if Exist some path to Dst
		if (checkPath) {
			checkPath = false;
			EngineAlarms.AddAlarm(timeCheckPath, 110, CHandle(this));
			EngineBasics.FixToGround(c_trans, &pos, &dst);
			bool pathfinding = EngineNavMesh.findPath(pos, dst, &path);
			walkable = pathfinding & path.size() > 5;
		}
	}
	else {
		EngineBasics.FixToGround(c_trans, &pos, &moveDst);
		bool pathfinding = EngineNavMesh.findPath(pos, moveDst, &path);
		if (pathfinding && path.size() > 1) {
			moveDst = path[1];
			walkable = true;
		}
	}

	//If exist Path and it's far enough to Dst, Move.
	float debugDSt = c_trans->accurateDistance(dst);
	walkable &= c_trans->accurateDistance(dst) > 3;
	if (walkable) {
		VEC3 direction;
		direction = moveDst - pos;
		if (direction.Length() > 1)
			direction.Normalize();
		enemy_controller->newMoveDirection(direction, true);
		changeVariable("goblin_speed", 1, false);
	}
	else {
		enemy_controller->newMoveDirection(VEC3(0, 0, 0));
		changeVariable("goblin_speed", 0, false);
		TCompFSM* fsm = get<TCompFSM>();
		if (fsm->getCurrentAnim() == "goblin_idle") {
			if (isTalk) {
				EngineAudio.playEvent3D("event:/ScreamEnemy/Goblin_idle_scream", c_trans, 3);
				isTalk = false;
				int conTime = rand() % 5 + 3;
				EngineAlarms.AddAlarm(5, 108, CHandle(this));
			}
		}
		if (crowdMovement) {
			EngineNavMesh.setAgentPos(crowdIdx, pos);
			EngineNavMesh.setAgentDirection(crowdIdx, c_trans->getFront());
		}
	}
}

void bt_gnome::SendDamage() {
	CEntity* e_player = getEntityByName("Player");
	TCompTransform* c_trans = get<TCompTransform>();
	VEC3 player_pos = EngineBasics.GetPlayerPos(player);

	TMsgDamage damage_msg;
	damage_msg.h_sender = CHandle(this).getOwner();
	damage_msg.h_bullet = CHandle(this).getOwner();
	damage_msg.damage = damageAttack;
	damage_msg.force = c_trans->getFront() * knockbackForce;

	if (currentAttackType == typeAttack::ShortAttack && c_trans->accurateDistance(player_pos) < shortAttackDistance) {
		e_player->sendMsg(damage_msg);
		EngineAudio.playEvent("event:/HitTypes/GoblinHit", 1);
	}
	else if (currentAttackType == typeAttack::LongAttack && c_trans->accurateDistance(player_pos) < attackDistance) {
		e_player->sendMsg(damage_msg);
		EngineAudio.playEvent("event:/HitTypes/GoblinHit", 1);
	}
}

void bt_gnome::ActivateRagdoll() {
	TCompRagdoll* c_ragdoll = get<TCompRagdoll>();
	if (c_ragdoll && !ragdollActived) {
		c_ragdoll->activateRagdoll();
		ragdollActived = true;

		EngineAlarms.AddAlarm(1.75f, 111, CHandle(this));
	}
	LeaveSwords();
}

void bt_gnome::LeaveSwords() {
	TCompGroup* c_group = get<TCompGroup>();
	std::vector<CHandle> handles = c_group->handles;

	for (int i = 0; i < handles.size(); i++) {
		CEntity* e = handles[i];
		TCompTransform* c_trans = e->get<TCompTransform>();
		VEC3 pos = c_trans->getPosition();

		float yaw, pitch, roll;
		c_trans->getAngles(&yaw, &pitch, &roll);

		CEntity* e_sword = EngineBasics.Instantiate(prefabSword, pos, yaw, pitch, roll);
		swords.push_back(e_sword);

		TCompCollider* collider = e_sword->get<TCompCollider>();
		if (collider) {
			PxRigidDynamic* rb = collider->actor->is<PxRigidDynamic>();
			rb->addTorque(VEC3_TO_PXVEC3(VEC3(randomRange(-100, 100), randomRange(-100, 100), randomRange(-100, 100))));
		}

		handles[i].destroy();
	}
}

void bt_gnome::changeVariable(const char* varName, float value, bool isBool) {
	TCompFSM* cFsm = get<TCompFSM>();
	if (cFsm)
	{
		cFsm->changeVariable(varName, value, isBool);
	}
}

bool bt_gnome::CheckViewPlayer() {
	TCompTransform* c_trans = get<TCompTransform>();
	if (c_trans == nullptr) return false;
	VEC3 pos = c_trans->getPosition();

	VEC3 player_pos = EngineBasics.GetPlayerPos(player);

	VEC3 offset = VEC3(0, 1, 0);
	VEC3 raycast_direction = player_pos - pos;
	raycast_direction.Normalize();

	physx::PxScene* scene = EnginePhysics.getPhysxScene();

	PxQueryFilterData filter_data = PxQueryFilterData();
	filter_data.data.word0 = ~(CModulePhysics::FilterGroup::Triggers | CModulePhysics::FilterGroup::Enemy);

	hit = PxRaycastBuffer();

	const PxHitFlags outputFlags =
		PxHitFlag::eDISTANCE
		| PxHitFlag::ePOSITION
		| PxHitFlag::eNORMAL
		;

	pos += offset;
	bool ray_cast_impact = scene->raycast(
		VEC3_TO_PXVEC3(pos),
		VEC3_TO_PXVEC3(raycast_direction),
		forget_distance,
		hit,
		outputFlags,
		filter_data
	);

	bool enable_attack = false;
	if (ray_cast_impact) {
		CHandle targetHdl;
		targetHdl.fromVoidPtr(hit.block.actor->userData);
		if (targetHdl.getOwner().isValid())
		{
			CEntity *targetEntity = targetHdl.getOwner();
			target = targetEntity->getName();
			if (std::string(target) == "Player")
				enable_attack = true;
		}
	}
	return enable_attack;
}

void bt_gnome::CheckDiedByOtherGnome(VEC3 velImpact) {
	if (velImpact.Length() > velToDieByGnome) {
		PxShape* shapes_controller[1];
		TCompCollider* c_collider = get<TCompCollider>();
		c_collider->actor->getShapes(shapes_controller, sizeof(shapes_controller), 0);
		PxFilterData simulationQueryDataController = shapes_controller[0]->getQueryFilterData();
		if (simulationQueryDataController.word1 & CModulePhysics::FilterGroup::Bones) {
			if (EngineEnemyManager.oneGoblinToGoblinDeath()) {
				gnomeKilled = true;
				resetState = true;
			}
		}
	}
}

void bt_gnome::setAgentState(bool state) {
	TCompTransform* c_trans = get<TCompTransform>();
	TCompCharController* c_controller = get<TCompCharController>();
	TCompSkelLookAt* c_lookAt = get< TCompSkelLookAt>();
	if (state) {
		crowdMovement = true;
		crowdIdx = EngineNavMesh.addAgent(c_trans->getPosition(), runSpeed, agentRadius);
		EngineNavMesh.setAgentPos(crowdIdx, c_trans->getPosition());
		EngineNavMesh.setAgentDirection(crowdIdx, c_trans->getFront());
		EngineEnemyManager.InCombatMeleeFighterSBB(CHandle(this).getOwner());
		if (c_lookAt)
			c_lookAt->setAmount(1);
		ChangeSpeed(runSpeed);
	}
	else {
		crowdMovement = false;
		EngineNavMesh.setAgentState(crowdIdx, false);
		EngineEnemyManager.StopCombatMeleeFighterSBB(CHandle(this).getOwner());
		if (c_lookAt)
			c_lookAt->setAmount(0);
		ChangeSpeed(walkSpeed);
	}
}

bool bt_gnome::GetPowerAffected() {
	return powerAffected;
}

bool bt_gnome::GetRagdollActivated() {
	return ragdollActived;
}

void bt_gnome::BlendSpeedAnimation() {
	velAgent = EngineNavMesh.getAgentVel(crowdIdx);
	float speed = velAgent.Length();
	speed = clampFloat(speed, 0.0f, runSpeed);

	TCompFSM* cFsm = get<TCompFSM>();
	if (cFsm)
	{
		float weight;
		if (speed < walkSpeed) {
			weight = 0.0f;
			if (abs(lastWeight - weight) >= 0.1)
				cFsm->setWeight(weight);
		}
		else {
			weight = (speed - walkSpeed) / (runSpeed - walkSpeed);
			weight = truncFloat(weight, 1);
			if (abs(lastWeight - weight) >= 0.1)
				cFsm->setWeight(weight);
		}
		weightDebug = weight;
		lastWeight = weight;
	}
	speedDebug = speed;
}

void bt_gnome::ChangeSpeed(float desiredSpeed) {
	if (setSpeed)//debug
		desiredSpeed = forceSpeed;

	if (actualSpeed != desiredSpeed) {
		TCompCharController* c_controller = get<TCompCharController>();
		EngineNavMesh.changeAgentSpeed(crowdIdx, desiredSpeed );
		c_controller->setMaxSpeed(desiredSpeed);
		actualSpeed = desiredSpeed;
	}
}

void bt_gnome::WatchingPlayer() {
	if (!auxFight) {
		EnginePersistence.setEnemyCounter(1);
		auxFight = true;
	}
	view = true;
	viewingPlayer = true;
	TCompSkeleton* c_skeleton = get< TCompSkeleton>();
	EngineAlarms.AddAlarm(timeToViewPlayer, 106, CHandle(this));
	c_skeleton->playActionAnimation("goblin_viewplayer", 0.0, 0.4, 1.0);
	if (!crowdMovement) {
		setAgentState(true);
	}
}

void bt_gnome::moveGnomeCinematic(VEC3 destination) {
	cinematicMove = true;
	cinematicDestination = destination;
}

void bt_gnome::setViewPlayer(bool newView) {
	if (newView) WatchingPlayer();
	else view = newView;
}

bt_gnome::~bt_gnome() {}