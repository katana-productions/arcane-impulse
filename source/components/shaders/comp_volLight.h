#pragma once

#include "components/shaders/shader_controller.h"
#include "engine.h"

class TCompVolLight : public CShaderController {
private:
  VEC4 lightCol;
  float volEmissive = 7.5f;
  float volDepth = 3.0f;
  float volRim = 1.5f;
  
  void onEntityCreated(const TMsgEntityCreated& msg);

public:

  DECL_SIBLING_ACCESS();
  static void registerMsgs();
  void load(const json& j, TEntityParseContext& ctx);
  void debugInMenu();
  void apply();
};
