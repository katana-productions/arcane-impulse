#include "mcv_platform.h"
#include "comp_fire.h"
#include "components/common/comp_render.h"
#include "entity/entity_parser.h"

DECL_OBJ_MANAGER("magic_fire", TCompFire);

void TCompFire::registerMsgs() {
  DECL_MSG(TCompFire, TMsgEntityCreated, onEntityCreated);
}
void TCompFire::onEntityCreated(const TMsgEntityCreated& msg)
{
  TCompRender *myRender = get<TCompRender>();
  myRender->setShaderController(ShaderController::Fire);

  TCompTransform *myTrans = get<TCompTransform>();
  myTrans->setAngles(0, 0, 0);
}

void TCompFire::load(const json& j, TEntityParseContext& ctx)
{
  enabled = j.value("enabled", true);

  distorsion_ammount = j.value("distorsion_ammount", 0.0f);
  fire_glow = j.value("fire_glow", 0.0f);
  inner_fire_color = loadColor(j, "inner_fire_color");
  outer_fire_color = loadColor(j, "outer_fire_color");

  random_time = randomFloat(0.0f, 10.0f);
}

void TCompFire::debugInMenu() 
{
  ImGui::Checkbox("Enabled", &enabled);
  ImGui::DragFloat("Distorsion Ammount", &distorsion_ammount, 0.1f, 0.0f, 5.0f);
  ImGui::DragFloat("Fire Glow", &fire_glow, 1.f, 0.0f, 200.0f);
  ImGui::ColorEdit4("Inner Fire Color", &inner_fire_color.x);
  ImGui::ColorEdit4("Outer Fire Color", &outer_fire_color.x);
}

void TCompFire::apply()
{
  ctes_shaderfx.fire_enabled = enabled;
  ctes_shaderfx.fire_distorsion_ammount = distorsion_ammount;
  ctes_shaderfx.fire_glow = fire_glow;
  ctes_shaderfx.inner_fire_color = inner_fire_color;
  ctes_shaderfx.outer_fire_color = outer_fire_color;
  ctes_shaderfx.random_time = random_time;

  ctes_shaderfx.updateGPU();
}
