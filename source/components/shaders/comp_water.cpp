#include "mcv_platform.h"
#include "comp_water.h"
#include "components/common/comp_render.h"
#include "components/shaders/comp_magic_outline.h"
#include "entity/entity_parser.h"

DECL_OBJ_MANAGER("magic_water", TCompWater);

void TCompWater::registerMsgs() {
  DECL_MSG(TCompWater, TMsgEntityCreated, onEntityCreated);
}

void TCompWater::onEntityCreated(const TMsgEntityCreated& msg)
{
  TCompRender *myRender = get<TCompRender>();
  myRender->setShaderController(ShaderController::Water);
  TCompMagicOutline *m = get< TCompMagicOutline>();
  if (m != nullptr)
    myRender->setShaderController(ShaderController::WaterAndOutlines);
}

void TCompWater::load(const json& j, TEntityParseContext& ctx)
{
  enabled = j.value("enabled", true);
  black_color = loadColor(j,"black_color");
  white_color = loadColor(j, "white_color");
  dist_ammount = j.value("dist_ammount", 0.0f);
  water_glow = j.value("water_glow", 0.0f);
  water_speed = j.value("water_speed", 0.0f);
  water_stretch = j.value("water_stretch", 0.0f);
  dist_speed = j.value("dist_speed", 0.0f);
  dist_stretch = j.value("dist_stretch", 0.0f);
  contrast = j.value("contrast", 0.0f);
  posterize = j.value("posterize", false);
  posterize_bands = j.value("posterize_bands", 0);

  foam_intensity = j.value("foam_intensity", foam_intensity);
  foamMaxDist = j.value("foamMaxDist", foamMaxDist);
  foamMinDist = j.value("foamMinDist", foamMinDist);
  foam_scale = j.value("foam_scale", foam_scale);
  borderVisibility = j.value("borderVisibility", borderVisibility);
  foam_color = loadColor(j, "foam_color");
}

void TCompWater::debugInMenu() 
{
  ImGui::Checkbox("Enabled:", &enabled);
  ImGui::ColorEdit4("Black Color:", &black_color.x);
  ImGui::ColorEdit4("White Color:", &white_color.x);
  ImGui::Checkbox("Posterize:", &posterize);
  ImGui::DragInt("Post Bands:", &posterize_bands);
  ImGui::DragFloat("Water Glow:", &water_glow, 1.5f, 0.0f, 2000.0f);
  ImGui::DragFloat("Water Speed:", &water_speed, 0.01f, 0.0f, 5.0f);
  ImGui::DragFloat("Water Stretch:", &water_stretch, 0.01f, 0.0f, 5.0f);
  ImGui::DragFloat("Dist Ammount:", &dist_ammount, 0.01f, 0.0f, 5.0f);
  ImGui::DragFloat("Dist Speed:", &dist_speed, 0.01f, 0.0f, 5.0f);
  ImGui::DragFloat("Dist Stretch:", &dist_stretch, 0.01f, 0.0f, 5.0f);
  ImGui::DragFloat("Contrast:", &contrast, 0.1f, 0.0f, 100.0f);

  ImGui::DragFloat("foam_intensity:", &foam_intensity, 1.5f, 0.0f, 2000.0f);
  ImGui::DragFloat("foamMaxDist:", &foamMaxDist, 0.001f, 0.0f, 0.1f);
  ImGui::DragFloat("foamMinDist:", &foamMinDist, 0.01f, 0.0f, 0.1f);
  ImGui::DragFloat("foam_scale:", &foam_scale, 0.01f, 0.0f, 20.0f);
  ImGui::DragFloat("borderVisibility:", &borderVisibility, 0.01f, 0.0f, 20.0f);
  ImGui::ColorEdit4("foam_color:", &foam_color.x);
}

void TCompWater::apply()
{
  ctes_shaderfx.water_enabled = enabled;
  ctes_shaderfx.black_color = VEC4(black_color.x, black_color.y, black_color.z, 1.0f);
  ctes_shaderfx.white_color = VEC4(white_color.x, white_color.y, white_color.z, 1.0f);
  ctes_shaderfx.posterize_enabled = posterize;
  ctes_shaderfx.posterize_bands = posterize_bands;
  ctes_shaderfx.water_glow = water_glow;
  ctes_shaderfx.water_speed = water_speed;
  ctes_shaderfx.water_stretch = water_stretch;
  ctes_shaderfx.dist_ammount = dist_ammount;
  ctes_shaderfx.dist_speed = dist_speed;
  ctes_shaderfx.dist_stretch = dist_stretch;
  ctes_shaderfx.contrast = contrast;

  ctes_shaderfx.foam_intensity = foam_intensity;
  ctes_shaderfx.foamMaxDist = foamMaxDist;
  ctes_shaderfx.foamMinDist = foamMinDist;
  ctes_shaderfx.foam_scale = foam_scale;
  ctes_shaderfx.borderVisibility = borderVisibility;
  ctes_shaderfx.foam_color = VEC4(foam_color.x, foam_color.y, foam_color.z, 1.0f);

  ctes_shaderfx.updateGPU();
}
