#include "mcv_platform.h"
#include "alpha_erosion.h"
#include "components/common/comp_render.h"
#include "entity/entity_parser.h"

DECL_OBJ_MANAGER("alpha_erosion", TCompAlphaErosion);

void TCompAlphaErosion::registerMsgs() {
  DECL_MSG(TCompAlphaErosion, TMsgEntityCreated, onEntityCreated);
}

void TCompAlphaErosion::onEntityCreated(const TMsgEntityCreated& msg)
{
  TCompRender *myRender = get<TCompRender>();
  myRender->setShaderController(ShaderController::Erosion);
}

void TCompAlphaErosion::load(const json& j, TEntityParseContext& ctx)
{
	alphaErosion = j.value("alphaErosion", alphaErosion);
	baseAlpha = j.value("baseAlpha", baseAlpha);
}

void TCompAlphaErosion::debugInMenu()
{
  ImGui::DragFloat("alphaErosion:", &alphaErosion, 0.01f, 0.0f, 1.0f);
  ImGui::DragFloat("baseAlpha:", &baseAlpha, 0.01f, 0.0f, 100.0f);
}

void TCompAlphaErosion::apply()
{
  ctes_shaderfx.alphaErosion = alphaErosion;
  ctes_shaderfx.baseAlpha = baseAlpha;

  ctes_shaderfx.updateGPU();
}