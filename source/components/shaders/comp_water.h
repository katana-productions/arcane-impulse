#pragma once

#include "components/shaders/shader_controller.h"
#include "engine.h"

class TCompWater : public CShaderController {
private:
	void onEntityCreated(const TMsgEntityCreated& msg);

public:
	VEC4 black_color;
	VEC4 white_color;
	bool posterize;
	int posterize_bands;
	float water_glow;
	float water_speed;
	float water_stretch;
	float dist_ammount;
	float dist_speed;
	float dist_stretch;
	float contrast;

	float foam_intensity = 2;
	float foamMaxDist = 0.0075;
	float foamMinDist = 0.00075;
	float foam_scale = 13;
	float borderVisibility = 3;
	VEC4 foam_color;

	DECL_SIBLING_ACCESS();
	static void registerMsgs();
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void apply();
};
