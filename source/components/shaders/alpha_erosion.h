#pragma once

#include "components/shaders/shader_controller.h"
#include "engine.h"

class TCompAlphaErosion : public CShaderController {
private:
  VEC4 lightCol;
  float alphaErosion = 0.0f;
  float baseAlpha = 1.0f;
  
  void onEntityCreated(const TMsgEntityCreated& msg);

public:

  DECL_SIBLING_ACCESS();
  static void registerMsgs();
  void load(const json& j, TEntityParseContext& ctx);
  void debugInMenu();
  void apply();
};
