#include "mcv_platform.h"
#include "comp_volLight.h"
#include "components/common/comp_render.h"
#include "entity/entity_parser.h"

DECL_OBJ_MANAGER("vol_light", TCompVolLight);

void TCompVolLight::registerMsgs() {
  DECL_MSG(TCompVolLight, TMsgEntityCreated, onEntityCreated);
}

void TCompVolLight::onEntityCreated(const TMsgEntityCreated& msg)
{
  TCompRender *myRender = get<TCompRender>();
  myRender->setShaderController(ShaderController::VolLight);
}

void TCompVolLight::load(const json& j, TEntityParseContext& ctx)
{
  lightCol = loadColor(j,"lightCol");
  volEmissive = j.value("volEmissive", volEmissive);
  volDepth = j.value("volDepth", volDepth);
  volRim = j.value("volRim", volRim);
}

void TCompVolLight::debugInMenu()
{
  ImGui::ColorEdit4("lightCol:", &lightCol.x);
  ImGui::DragFloat("emissive:", &volEmissive, 0.1f, 0.0f, 2000.0f);
  ImGui::DragFloat("volDepth:", &volDepth, 0.1f, 0.0f, 2000.0f);
  ImGui::DragFloat("volRim:", &volRim, 0.05f, 0.0f, 50.0f);
}

void TCompVolLight::apply()
{
  ctes_shaderfx.lightCol = lightCol;
  ctes_shaderfx.volEmissive = volEmissive;
  ctes_shaderfx.volDepth = volDepth;
  ctes_shaderfx.volRim = volRim;

  ctes_shaderfx.updateGPU();
}