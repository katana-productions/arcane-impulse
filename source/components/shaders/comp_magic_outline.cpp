#include "mcv_platform.h"
#include "comp_magic_outline.h"
#include "components/common/comp_render.h"
#include "components/shaders/comp_water.h"
#include "components/ai/bt_gnome.h"
#include "entity/entity_parser.h"

DECL_OBJ_MANAGER("magic_outline", TCompMagicOutline);

void TCompMagicOutline::registerMsgs() {
  DECL_MSG(TCompMagicOutline, TMsgEntityCreated, onEntityCreated);
}

void TCompMagicOutline::onEntityCreated(const TMsgEntityCreated& msg)
{
  TCompRender *myRender = get<TCompRender>();
  myRender->setShaderController(ShaderController::Outlines);
  bt_gnome *bt = get<bt_gnome>();
  TCompWater *w = get<TCompWater>();
  if (bt != nullptr)
    myRender->setShaderController(ShaderController::DissolveAndOutlines);
  else if (w != nullptr)
    myRender->setShaderController(ShaderController::WaterAndOutlines);
}

void TCompMagicOutline::load(const json& j, TEntityParseContext& ctx)
{
  enabled = j.value("enabled", false);
  outline_color = loadColor(j, "outline_color");
  outline_glow = j.value("outline_glow", outline_glow);
}

void TCompMagicOutline::debugInMenu() 
{
  ImGui::Checkbox("Enabled", &enabled);
  ImGui::ColorEdit4("Outline Color", &outline_color.x);
  ImGui::DragFloat("Outline Glow", &outline_glow, 0.01f, 0.0f, 10.0f);
}

void TCompMagicOutline::update(float dt)
{
  if (enabled)
  {
    if (time / lerp_time < 1.0f) time += dt;
    if (time / lerp_time > 1.0f) time = lerp_time;
  }
  else
  {
    if (time / lerp_time > 0.0f) time -= dt;
    if (time / lerp_time < 0.0f) time = 0.0f;
  }
}

void TCompMagicOutline::apply()
{
  current_color = outline_color * time / lerp_time;
  current_color.w = 1.0f;

  ctes_shaderfx.outline_color = current_color;
  ctes_shaderfx.outline_glow = outline_glow;

  ctes_shaderfx.updateGPU();
}
