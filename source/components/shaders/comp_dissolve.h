#pragma once

#include "components/shaders/shader_controller.h"
#include "engine.h"

// Controls the dissolve.fx shader
class TCompDissolve : public CShaderController {
private:
  float   ratio = 0.0f;
  float   time = 0.0f;
  float   dissolve_time;
  float   edge_thickness;
  float   edge_glow;
  VEC4    edge_color;

  void onEntityCreated(const TMsgEntityCreated& msg);

public:

  DECL_SIBLING_ACCESS();
  static void registerMsgs();
  void load(const json& j, TEntityParseContext& ctx);
  void debugInMenu();
  void update(float dt);
  void apply();

  void setDissolveTime(float t) { dissolve_time = t; }
};
