#include "mcv_platform.h"
#include "comp_dissolve.h"
#include "components/common/comp_render.h"
#include "components/ai/bt_gnome.h"
#include "components/ai/bt_mage.h"
#include "entity/entity_parser.h"

DECL_OBJ_MANAGER("magic_dissolve", TCompDissolve);

void TCompDissolve::registerMsgs() {
  DECL_MSG(TCompDissolve, TMsgEntityCreated, onEntityCreated);
}

void TCompDissolve::onEntityCreated(const TMsgEntityCreated& msg)
{
  TCompRender *myRender = get<TCompRender>();
  myRender->setShaderController(ShaderController::Dissolve);

  bt_gnome *gnome = get<bt_gnome>();
  bt_mage *mage = get<bt_mage>();

  if (gnome != nullptr)
    myRender->setShaderController(ShaderController::DissolveAndOutlines);
  else if (mage != nullptr)
	myRender->setShaderController(ShaderController::Dissolve);

}

void TCompDissolve::load(const json& j, TEntityParseContext& ctx)
{
  enabled = j.value("enabled", true);
  dissolve_time = j.value("dissolve_time", 0.0f);
  edge_thickness = j.value("edge_thickness", 0.0f);
  edge_glow = j.value("edge_glow", 0.0f);
  edge_color = loadColor(j, "edge_color");
}

void TCompDissolve::debugInMenu() 
{
  ImGui::Checkbox("Enabled", &enabled);
  ImGui::DragFloat("Loop Time", &dissolve_time, 0.1f, 0.0f, 10.0f);

  ImGui::DragFloat("Edge Thickness", &edge_thickness, 0.01f, 0.0f, 1.0f);
  ImGui::DragFloat("Edge Glow", &edge_glow, 1.f, 0.0f, 200.0f);
  ImGui::ColorEdit4("Edge Color", &edge_color.x);
}

void TCompDissolve::update(float dt)
{
  if (!enabled) {
    return;
  }

  time += dt;
  
  ratio = time / dissolve_time;
}

void TCompDissolve::apply()
{
  ctes_shaderfx.dissolve_enabled = enabled;
  ctes_shaderfx.dissolve_ratio = Interpolator::quadInOut(0.f, 1.f, ratio);
  ctes_shaderfx.edge_color = edge_color;
  ctes_shaderfx.edge_glow = edge_glow;
  ctes_shaderfx.edge_thickness = edge_thickness;

  ctes_shaderfx.updateGPU();
}
