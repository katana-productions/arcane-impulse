#pragma once

#include "components/shaders/shader_controller.h"
#include "engine.h"

// Controls the fire.fx shader
class TCompFire : public CShaderController {
private:
  float   distorsion_ammount;
  float   fire_glow;
  VEC4    inner_fire_color;
  VEC4    outer_fire_color;
  float   random_time;

  void onEntityCreated(const TMsgEntityCreated& msg);

public:

  DECL_SIBLING_ACCESS();
  static void registerMsgs();
  void load(const json& j, TEntityParseContext& ctx);
  void debugInMenu();
  void apply();
};
