#pragma once

#include "components/common/comp_base.h"

/************************************
** HOW TO CREATE SHADER CONTROLERS **
*************************************
*
* 1. Create a class that has as base class TCompBase and CShaderController.
*    Class Signature should look something like this:

      class TCompDissolve : public TCompBase, public CShaderController

* 2. This class has to have onEntityCreated(const TMsgEntityCreated& msg) method, registerMsgs()
*    with at least onEntityCreated registered 
*
* 3. Add a new enum to the enum of ShaderController (below) coresponding to your shader. If your shader has to be used with other shaders, create a new enum for each combination
*
* 4. onEntityCreated has to look like this (with your shader enum):

      void TCompDissolve::onEntityCreated(const TMsgEntityCreated& msg)
      {
        TCompRender *myRender = get<TCompRender>();
        myRender->setShaderController(ShaderController::VolLight);
      }

* 5. Redefine the base class method apply(). This is where you will update the constant buffer for the render manager.
*    It should look something like this:

      void TCompDissolve::apply()
      {
        ctes_shaderfx.dissolve_enabled = enabled;
        ctes_shaderfx.dissolve_time = time;
        ctes_shaderfx.edge_color = edgeColor;
        ctes_shaderfx.edge_glow = edgeGlow;
        ctes_shaderfx.edge_thickness = edgeThickness;

        ctes_shaderfx.updateGPU();
      }

* 5. The rest of the class behaves like a normal component, meaning that you can use update() to control the shaders parameters
* 
*
*/

// Add new Shader Controllers here

enum ShaderController
{
  Nothing,
  Dissolve,
  Outlines,
  DissolveAndOutlines,
  Erosion,
  Fire,
  VolLight,
  Water,
  WaterAndOutlines
};

// Shader Controller base class serves as interface
class CShaderController : public TCompBase
{
protected:
  bool enabled;

public:

  void toggleEnabled() { enabled = !enabled; }
  bool getEnabled() { return enabled; }
  void setEnabled(bool e) { enabled = e; }
  virtual void apply() {};
};