#pragma once

#include "components/shaders/shader_controller.h"
#include "engine.h"

// Controls the fire.fx shader
class TCompMagicOutline : public CShaderController {
private:
  VEC4    outline_color;
  VEC4    current_color = VEC4(0,0,0,1); // Black for disabled
  float   outline_glow = 2.0f;
  float   lerp_time = 0.1f;
  float   time = 0.0f;

  void onEntityCreated(const TMsgEntityCreated& msg);

public:

  DECL_SIBLING_ACCESS();
  static void registerMsgs();
  void load(const json& j, TEntityParseContext& ctx);
  void debugInMenu();
  void update(float dt);
  void apply();
};
