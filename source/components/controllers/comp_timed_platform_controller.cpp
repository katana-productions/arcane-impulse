#include "mcv_platform.h"
#include "comp_timed_platform_controller.h"
#include "components/common/comp_group.h"
#include "components/controllers/comp_char_controller.h"
#include "components/other/comp_trigger_call.h"
#include "components/player/comp_player_stats.h"
#include "physics/module_physics.h"

DECL_OBJ_MANAGER("timed_platform_controller", TCompTimedPlatfController);

void TCompTimedPlatfController::registerMsgs() {
  DECL_MSG(TCompTimedPlatfController, TMsgEntityCreated, onEntityCreated);
  DECL_MSG(TCompTimedPlatfController, TMsgSceneCreated, onSceneCreated);
  DECL_MSG(TCompTimedPlatfController, TMsgPuzzleTriggerEntered, keyEntered);
  DECL_MSG(TCompTimedPlatfController, TMsgDamage, onDamageMsg);
}

void TCompTimedPlatfController::onSceneCreated(const TMsgSceneCreated& msg)
{
  myTrans = get<TCompTransform>();
  myCol = get<TCompCollider>();
  TCompName *myName = get<TCompName>();

  origTrans = *myTrans;

  float y, p, r;
  myTrans->getAngles(&y, &p, &r);

  // Instantiate trigger  
  if (affectedByPlayer)
  {
    VEC3 pos = myTrans->getPosition() + (myTrans->getUp() * 10.0f);
    CEntity *t = EngineBasics.Instantiate("data/prefabs/plat_trigger_instance.json", pos, y, p, r);
    h_trigger = t;
    TCompName *tName = t->get<TCompName>();
    myTriggerName = (std::string)(myName->getName()) + "_trigger";
    tName->setName(myTriggerName.c_str());
    TCompTriggerCall *pTrigger = t->get<TCompTriggerCall>();
    pTrigger->call_entities.push_back(myName->getName());

    TCompTransform *triggerTrans = t->get<TCompTransform>();
    triggerOrigTrans = *triggerTrans;

    // Add trigger to my platform group so it moves around with me
    TCompGroup *myGroup = get<TCompGroup>();
    myGroup->add((CHandle)t);
    isTrigger = true;
  }
}

void TCompTimedPlatfController::onEntityCreated(const TMsgEntityCreated& msg)
{
  myTrans = get<TCompTransform>();
  myCol = get<TCompCollider>();
  TCompName *myName = get<TCompName>();

  origTrans = *myTrans;

  float y, p, r;
  myTrans->getAngles(&y, &p, &r);

  // Instantiate trigger  
  if (affectedByPlayer)
  {
    VEC3 pos = myTrans->getPosition() + (myTrans->getUp() * 10.0f);
    CEntity *t = EngineBasics.Instantiate("data/prefabs/plat_trigger_instance.json", pos, y, p, r);
    h_trigger = t;
    TCompName *tName = t->get<TCompName>();
    myTriggerName = (std::string)(myName->getName()) + "_trigger";
    tName->setName(myTriggerName.c_str());
    TCompTriggerCall *pTrigger = t->get<TCompTriggerCall>();
    pTrigger->call_entities.push_back(myName->getName());

    TCompTransform *triggerTrans = t->get<TCompTransform>();
    triggerOrigTrans = *triggerTrans;

    // Add trigger to my platform group so it moves around with me
    TCompGroup *myGroup = get<TCompGroup>();
    myGroup->add((CHandle)t);
		isTrigger = true;
  }
}

void TCompTimedPlatfController::keyEntered(const TMsgPuzzleTriggerEntered& msg)
{
  CHandle trig = msg.h_trigger;
  CHandle key = msg.h_entity;

  if (trig.isValid() and key.isValid())
  {
    CEntity *trigEnt = trig;
    CEntity *keyEnt = key;
    TCompName *trigName = trigEnt->get<TCompName>();
    TCompName *keyName = keyEnt->get<TCompName>();
    std::string tName = trigName->getName();
    std::string kName = keyName->getName();

    if (tName == triggerSwitch and kName == "Player")
    {
      enabled = true;
    }
    if (enabled == true and tName == myTriggerName and kName == "Player")
    {
      playerIsOn = true;
    }
  }
}

void TCompTimedPlatfController::onDamageMsg(const TMsgDamage& msg)
{
  if (enabled)
    destroy();
}

void TCompTimedPlatfController::update(float dt)
{
  if (enabled)
  {
    if (dt > 0.1f) dt = 0.1f;
    if (playerIsOn)
    {
      time += dt;
      if (time >= ttl)
      {
        destroy();
        time = 0.0f;
      }
    }
    else if (disappeared == true)
    {
      time += dt;
      if (time >= time_to_regenerate)
      {
        // Teleport platform
        //myTrans->setPosition(origTrans.getPosition());
        physx::PxTransform point(VEC3_TO_PXVEC3(origTrans.getPosition()));
        physx::PxRigidDynamic *rb = myCol->actor->is<physx::PxRigidDynamic>();
        rb->setKinematicTarget(point);

        // Teleport trigger
        if (affectedByPlayer)
        {
          physx::PxTransform point2(VEC3_TO_PXVEC3(triggerOrigTrans.getPosition()));
          CEntity *triggerEnt = h_trigger;
          TCompCollider *triggerCol = triggerEnt->get<TCompCollider>();
          rb = triggerCol->actor->is<physx::PxRigidDynamic>();
          rb->setKinematicTarget(point2);
        }

        disappeared = false;
        time = 0.0f;
      }
    }
  }
}

void TCompTimedPlatfController::destroy()
{
	TCompName *myName = get<TCompName>();
	std::string name = myName->getName();
	CEntity* player = getEntityByName("Player");
	TCompPlayerStats* playerStats = player->get<TCompPlayerStats>();
  // TODO: implement proper destruction later
  // If oneshot, destroy myself
  if (oneShot)
  {
		/*if (name == "Trap") {
			EnginePersistence.blockPlayer(true);
			playerStats->setTrap(true);
		}*/

    CHandle(this).getOwner().destroy();
    //h_trigger.destroy();
  }
  // If regenerate, teleport myself the fuck outta here and come back in a bit
  else
  {
    // Get the player's position
    CEntity *pl = getEntityByName("Player");
    TCompTransform *plTrans = pl->get<TCompTransform>();
    VEC3 plPos = plTrans->getPosition();
    VEC3 plFront = plTrans->getFront();

    // Teleport player to not make contact with the platform
    TCompCharController *plController = pl->get<TCompCharController>();
    plController->teleport(plPos + VEC3(0, 0.1f, 0), plFront);

    // Teleport platform
    //myTrans->setPosition(VEC3(10000, 10000, 10000));
    physx::PxTransform point(VEC3_TO_PXVEC3(VEC3(10000, 10000, 10000)));
    physx::PxRigidDynamic *rb = myCol->actor->is<physx::PxRigidDynamic>();
    rb->setKinematicTarget(point);

    // Teleport trigger
    if (affectedByPlayer)
    {
      CEntity *triggerEnt = h_trigger;
      TCompCollider *triggerCol = triggerEnt->get<TCompCollider>();
      rb = triggerCol->actor->is<physx::PxRigidDynamic>();
      rb->setKinematicTarget(point);
    }

    disappeared = true;
    playerIsOn = false;
  }
}



void TCompTimedPlatfController::debugInMenu() {
  if (oneShot)  ImGui::Text("Type: OneShot\n");
  else ImGui::Text("Type: Regenerative");
  ImGui::Text("Trigger: %s", myTriggerName.c_str());
  ImGui::Text("Time to live: %d", ttl);
	ImGui::Checkbox("isTrigger", &isTrigger);
}

void TCompTimedPlatfController::load(const json& j, TEntityParseContext& ctx)
{
  std::string s = j.value("type", "oneShot");
  if (s == "oneShot") oneShot = true;
  else oneShot = false;

  triggerSwitch = j.value("triggerSwitch", "");
  if (triggerSwitch == "") enabled = true;

  affectedByPlayer = j.value("affectedByPlayer", false);
  ttl = j.value("time_to_live", ttl);
  time_to_regenerate = j.value("time_to_regenerate", time_to_regenerate);
}

void TCompTimedPlatfController::renderDebug()
{

}
