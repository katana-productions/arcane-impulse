#pragma once
#include "components/common/comp_base.h"

class TCompEntityDestController : public TCompBase
{
private:
	bool enabled = true;
	bool entitiesCreated = false;
	bool eventVoice = false;
	std::vector<std::string> entities;
	int event;
	bool all = true;

	void init(const TMsgEntityCreated &msg);

	bool allEntitiesOk();
	bool allEntitiesDestroyed();
	bool anyEntityDestroyed();

public:
	DECL_SIBLING_ACCESS();
	static void registerMsgs();
	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);
	void renderDebug();
	void debugInMenu();
};

