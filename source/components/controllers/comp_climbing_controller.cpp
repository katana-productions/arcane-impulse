#include "mcv_platform.h"
#include "components/ai/comp_enemy_stats.h"
#include "comp_climbing_controller.h"
#include "comp_char_controller.h"
#include "components\player\comp_player_magic.h"
#include "engine.h"
#include "input/module_input.h"
#include "components\player\comp_player_magic.h"
#include "entity/common_msgs.h"
#include "components/common/comp_collider.h"
#include "components/common/comp_transform.h"
#include "physics\module_physics.h"
#include "entity/entity.h"
#include "components/common/comp_base.h"
#include "components/common/comp_fsm.h"

DECL_OBJ_MANAGER("climbing_controller", TCompClimbingController);

void TCompClimbingController::load(const json& j, TEntityParseContext& ctx)
{
	SetUpPhysXFilters();
	offsetFrontRay = j.value("offsetFrontRay", offsetFrontRay);
	distFrontRay = j.value("distFrontRay", distFrontRay);

	offsetDownRayX = j.value("offsetDownRayX", offsetDownRayX);
	offsetDownRayY = j.value("offsetDownRayY", offsetDownRayY);
	distDownRay = j.value("distDownRay", distDownRay);

	distHeadRay = j.value("distHeadRay", distHeadRay);
	distCheck = j.value("distCheck", distCheck);

	maxDistPush = j.value("maxDistPush", maxDistPush);
}

void TCompClimbingController::registerMsgs()
{
	DECL_MSG(TCompClimbingController, TMsgEntityCreated, init);
}

void TCompClimbingController::init(const TMsgEntityCreated& msg)
{
	characterController = get<TCompCharController>();
	assert(characterController);

	playerMove = get<TCompPlayerMove>();
	assert(playerMove);

	playerMagic = get<TCompPlayerMagic>();
	assert(playerMagic);

	myTrans = get<TCompTransform>();
	assert(myTrans);

	enabled = true;
}

void TCompClimbingController::update(float scaled_dt)
{
	if (!enabled || EnginePhysics.isPaused) return;

	CheckClimbRay();
	if (IsClimbing) HandlePlayerClimb(scaled_dt);
}

void TCompClimbingController::CheckClimbRay()
{
	if (!playerMove->getHasJumped() and !playerMagic->getIsPlayerPullPush())
		return;

	VEC3 pos = myTrans->getPosition();
	VEC3 front = myTrans->getFront();
	VEC3 down = myTrans->getDown();
	VEC3 up = myTrans->getUp();

	// Do all Raycast
	headRaycast1 = EnginePhysics.Raycast(pos, up, distHeadRay, headRaycastHit,
		physx::PxQueryFlag::eSTATIC | physx::PxQueryFlag::eDYNAMIC, physx::PxHitFlag::eDEFAULT, groundDetectionFilter);

	headRaycast2 = EnginePhysics.Raycast(pos + 0.3f * front, up, distHeadRay, headRaycastHit,
		physx::PxQueryFlag::eSTATIC | physx::PxQueryFlag::eDYNAMIC, physx::PxHitFlag::eDEFAULT, groundDetectionFilter);

	headRaycast3 = EnginePhysics.Raycast(pos - 0.3f * front, up, distHeadRay, headRaycastHit,
		physx::PxQueryFlag::eSTATIC | physx::PxQueryFlag::eDYNAMIC, physx::PxHitFlag::eDEFAULT, groundDetectionFilter);

	frontRaycast = EnginePhysics.Raycast(pos + VEC3(0, offsetFrontRay, 0), front, distFrontRay, frontRaycastHit,
		physx::PxQueryFlag::eSTATIC | physx::PxQueryFlag::eDYNAMIC, physx::PxHitFlag::eDEFAULT, groundDetectionFilter);

	downRaycast = EnginePhysics.Raycast(pos + up * offsetDownRayY + front * offsetDownRayX, down, distDownRay, downRaycastHit,
		physx::PxQueryFlag::eSTATIC | physx::PxQueryFlag::eDYNAMIC, physx::PxHitFlag::eDEFAULT, groundDetectionFilter);

	VEC3 origin = PXVEC3_TO_VEC3(downRaycastHit.position) + myTrans->getUp();
	VEC3 leftDir = myTrans->getLeft() + front;
	leftDir.Normalize();
	VEC3 rightDir = -myTrans->getLeft() + front;
	rightDir.Normalize();

	bool checkLeft = EnginePhysics.Raycast(origin, leftDir, distCheck, headRaycastHit, physx::PxQueryFlag::eSTATIC, physx::PxHitFlag::eDEFAULT, groundDetectionFilter);
	bool checkRight = EnginePhysics.Raycast(origin, rightDir, distCheck, headRaycastHit, physx::PxQueryFlag::eSTATIC, physx::PxHitFlag::eDEFAULT, groundDetectionFilter);

	bool rampCheck = EnginePhysics.Raycast(pos, down, 1.0f, footRaycastHit, physx::PxQueryFlag::eSTATIC, physx::PxHitFlag::eDEFAULT, groundDetectionFilter);

	// Nothing over the player's head
	if (!headRaycast1 and !headRaycast2 and !headRaycast3)
	{
		// Basic case
		if (!frontRaycast and downRaycast and !checkLeft and !checkRight and !IsClimbing)
		{
			CHandle hDown;
			hDown.fromVoidPtr(downRaycastHit.actor->userData);
			hDown = hDown.getOwner();
			if (!playerMagic->pullingObj or (playerMagic->pulledObjHandle.isValid() and hDown != playerMagic->pulledObjHandle))
			{
				// Ramp check
				float d = VEC3::Distance(PXVEC3_TO_VEC3(footRaycastHit.position), pos);
				if ((rampCheck and d >= 0.8f) or !rampCheck)
				{
					CEntity* e_left_hand = getEntityByName("LeftHand");
					TCompFSM* cFsm_left = e_left_hand->get<TCompFSM>();

					CEntity* e_right_hand = getEntityByName("RightHand");
					TCompFSM* cFsm_right = e_right_hand->get<TCompFSM>();

					if (cFsm_right && cFsm_left) {
						cFsm_right->changeVariable("climbing", 1, true);
						cFsm_left->changeVariable("climbing", 1, true);
					}
					IsClimbing = true;
					playerMove->SetActive(false);

					float distRayDown2ground = VEC3::Distance(PXVEC3_TO_VEC3(downRaycastHit.position), (pos + up * offsetDownRayY + front * offsetDownRayX));
					maxDistUp = offsetDownRayY - distRayDown2ground;
					originalOrientation = CTransform(pos, myTrans->getRotation());

					prevPos = myTrans->getPosition();

					// Platform check
					if (downRaycast)
					{
						platRb = downRaycastHit.actor->is<PxRigidBody>();
						if (platRb != nullptr and platRb->getRigidBodyFlags().isSet(physx::PxRigidBodyFlag::eKINEMATIC))
						{
							isPlatform = true;
						}
						else
						{
							isPlatform = false;
							platRb = nullptr;
						}
					}
				}
			}
		}
	}
}

void TCompClimbingController::HandlePlayerClimb(float dt)
{
	if (!climbSound) {
		EngineAudio.playEvent("event:/PlayerVoice/ClimbVoice", 4);
		climbSound = true;
	}

	// First we gotta pull the player up until the foot is over the edge
	if (IsPullingUp)
	{
		if (distUp <= (maxDistUp + 0.1f))
		{
			if (isPlatform)
			{
				VEC3 platVel = PXVEC3_TO_VEC3(platRb->getLinearVelocity());
				VEC3 platDir = platVel;
				platDir.Normalize();
				float platDist = platVel.Length() * dt;
				SetFootPos(myTrans->getPosition() + (platDir * platDist) + (myTrans->getUp() * stepByDelta * dt));
			}
			else
				SetFootPos(myTrans->getPosition() + myTrans->getUp() * stepByDelta * dt);
			distUp += stepByDelta * dt;
		}
		else
		{
			IsPullingUp = false;
			distUp = 0.0f;
		}
	}
	// we gotta push the player forward until it's grounded
	else
	{
		CEntity* e_left_hand = getEntityByName("LeftHand");
		TCompFSM* cFsm_left = e_left_hand->get<TCompFSM>();

		CEntity* e_right_hand = getEntityByName("RightHand");
		TCompFSM* cFsm_right = e_right_hand->get<TCompFSM>();

		if (cFsm_right && cFsm_left) {
			cFsm_right->changeVariable("climbing", 0, true);
			cFsm_left->changeVariable("climbing", 0, true);
		}
		if (distPush <= maxDistPush and !characterController->IsGrounded()) {
			if (isPlatform)
			{
				VEC3 platVel = PXVEC3_TO_VEC3(platRb->getLinearVelocity());
				VEC3 platDir = platVel;
				platDir.Normalize();
				float platDist = platVel.Length() * dt;
				SetFootPos(myTrans->getPosition() + (platDir * platDist) + (originalOrientation.getFront() * stepByDelta * dt));
			}
			else
				SetFootPos(myTrans->getPosition() + originalOrientation.getFront() * stepByDelta * dt);
			distPush += stepByDelta * dt;
		}
		else {
			IsPullingUp = true;
			IsClimbing = false;
			playerMove->setHasJumped(false);
			playerMove->SetActive(true);
			distPush = 0;
			isPlatform = false;
			platRb = nullptr;
			climbSound = false;
		}
	}
	prevPos = myTrans->getPosition();
}

void TCompClimbingController::SetFootPos(VEC3 new_pos)
{
	TCompCollider* comp_collider = get<TCompCollider>();
	comp_collider->controller->setFootPosition(VEC3_TO_PXEXTENDEDVEC3(new_pos));
}

void TCompClimbingController::SetUpPhysXFilters()
{
	PxFilterData pxFilterData;
	pxFilterData.word0 = CModulePhysics::FilterGroup::Scenario;
	groundDetectionFilter.data = pxFilterData;

	pxFilterData.word0 = ~(CModulePhysics::FilterGroup::Player | CModulePhysics::FilterGroup::Triggers);
	headDetectionFilter.data = pxFilterData;
}

void TCompClimbingController::renderDebug()
{
	VEC3 pos = myTrans->getPosition();
	VEC3 front = myTrans->getFront();
	VEC3 down = myTrans->getDown();
	VEC3 up = myTrans->getUp();

	VEC3 headOrigin1 = pos;
	VEC3 headDest1 = headOrigin1 + up * distHeadRay;

	VEC3 headOrigin2 = pos + 0.3f * front;
	VEC3 headDest2 = headOrigin2 + up * distHeadRay;

	VEC3 headOrigin3 = pos - 0.3f * front;
	VEC3 headDest3 = headOrigin3 + up * distHeadRay;

	VEC3 frontOrigin = pos + VEC3(0, offsetFrontRay, 0);
	VEC3 frontDest = frontOrigin + front * distFrontRay;

	VEC3 downRayOrigin = pos + up * offsetDownRayY + front * offsetDownRayX;
	VEC3 downRayDest = downRayOrigin + down * distDownRay;

	//climbing checkers
	VEC4 green = VEC4(0, 1, 0, 1);
	VEC4 red = VEC4(1, 0, 0, 1);

	if (headRaycast1) drawLine(PXVEC3_TO_VEC3(headOrigin1), PXVEC3_TO_VEC3(headDest1), green);
	else drawLine(PXVEC3_TO_VEC3(headOrigin1), PXVEC3_TO_VEC3(headDest1), red);

	if (headRaycast2) drawLine(PXVEC3_TO_VEC3(headOrigin2), PXVEC3_TO_VEC3(headDest2), green);
	else drawLine(PXVEC3_TO_VEC3(headOrigin2), PXVEC3_TO_VEC3(headDest2), red);

	if (headRaycast3) drawLine(PXVEC3_TO_VEC3(headOrigin3), PXVEC3_TO_VEC3(headDest3), green);
	else drawLine(PXVEC3_TO_VEC3(headOrigin3), PXVEC3_TO_VEC3(headDest3), red);

	if (downRaycast) drawLine(PXVEC3_TO_VEC3(downRayOrigin), PXVEC3_TO_VEC3(downRayDest), green);
	else drawLine(PXVEC3_TO_VEC3(downRayOrigin), PXVEC3_TO_VEC3(downRayDest), red);

	if (frontRaycast) drawLine(PXVEC3_TO_VEC3(frontOrigin), PXVEC3_TO_VEC3(frontDest), green);
	else drawLine(PXVEC3_TO_VEC3(frontOrigin), PXVEC3_TO_VEC3(frontDest), red);
}

void TCompClimbingController::debugInMenu()
{
	ImGui::Checkbox("IsClimbing", &IsClimbing);
	ImGui::DragFloat("distHeadRay", &distHeadRay, 0.1f, -5.0f, 5.0f);

	ImGui::DragFloat("distFrontRay", &distFrontRay, 0.1f, -5.0f, 5.0f);
	ImGui::DragFloat("frontRayOffset", &offsetFrontRay, 0.1f, -5.0f, 5.0f);

	ImGui::DragFloat("distDownRay", &distDownRay, 0.1f, -5.0f, 5.0f);
	ImGui::DragFloat("downRayOffset X", &offsetDownRayX, 0.1f, -5.0f, 5.0f);
	ImGui::DragFloat("downRayOffset Y", &offsetDownRayY, 0.1f, -5.0f, 5.0f);

	ImGui::DragFloat("maxDistPush", &maxDistPush, 0.1f, -5.0f, 5.0f);
}