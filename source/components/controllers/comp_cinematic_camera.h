#pragma once

#include "components/common/comp_base.h"
#include "geometry/curve.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "components/common/comp_transform.h"

class TCompCinematicCamera : public TCompBase
{
private:
	void setLookAt();
	const CCurve* curve = nullptr;
	CHandle target;
	std::string targetName;
	float ratio = 0.0f;
	float speed = 2.0f;
	VEC3 offset = VEC3(0, 0, 0);
	bool enabled = false;
	TCompTransform* cTransform = nullptr;
	TCompTransform* cTargetTransform = nullptr;
	MAT44 originalTransform;

public:
  DECL_SIBLING_ACCESS();
  void debugInMenu();
  void load(const json& j, TEntityParseContext& ctx);
  void update(float dt);
  void renderDebug();
  void enableTransition() { enabled = true; }
};

