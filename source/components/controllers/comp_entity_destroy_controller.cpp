#include "mcv_platform.h"
#include "comp_entity_destroy_controller.h"
#include "entity/entity.h"
#include "engine.h"

DECL_OBJ_MANAGER("comp_entities_destroy", TCompEntityDestController);

void TCompEntityDestController::registerMsgs()
{
}

bool TCompEntityDestController::allEntitiesOk()
{
  for (int i = 0; i < entities.size(); ++i)
  {
    CHandle e = getEntityByName(entities[i]);
    if (!e.isValid()) return false;
  }
  return true;
}

bool TCompEntityDestController::allEntitiesDestroyed()
{
  for (int i = 0; i < entities.size(); ++i)
  {
    CHandle e = getEntityByName(entities[i]);
    if (e.isValid()) return false;
  }
  return true;
}

bool TCompEntityDestController::anyEntityDestroyed()
{
	for (int i = 0; i < entities.size(); ++i)
	{
		CHandle e = getEntityByName(entities[i]);
		if (!e.isValid()) return true;
	}
	return false;
}

void TCompEntityDestController::load(const json& j, TEntityParseContext& ctx)
{
  //Load enemies
  json e = j["entities"];
  for (json::iterator it = e.begin(); it != e.end(); ++it) {
    std::string entity = it.value().get<std::string>();
    entities.push_back(entity);
  }
  event = j.value("event",0);
  all = j.value("all", true);
	eventVoice = j.value("eventVoice", eventVoice);
}

void TCompEntityDestController::update(float dt)
{
  if (enabled)
  {
	  if (allEntitiesOk())
		  entitiesCreated = true;
	  if (all) {
		  if (entitiesCreated and allEntitiesDestroyed())
		  {
			  if (event != 0) {
			  	if (!eventVoice) EngineLua.launchEvent(event);
			  	else EngineLua.playDialogue(event);
			  }
			  enabled = false;
		  }
	  }
	  else {
		  if (entitiesCreated and anyEntityDestroyed())
		  {
		  	if (event != 0) {
			  if (!eventVoice) EngineLua.launchEvent(event);
			  else EngineLua.playDialogue(event);
			}
			enabled = false;
		  }
	  }
  }
}

void TCompEntityDestController::debugInMenu()
{
  ImGui::Text("Event to load %d", event);
  ImGui::Checkbox("Entities Created", &entitiesCreated);
  ImGui::Checkbox("All", &all);
  if(allEntitiesDestroyed())
	  ImGui::Text("All Entities Destroyed");
  if (anyEntityDestroyed())
	  ImGui::Text("Some Entities Destroyed");
}

void TCompEntityDestController::renderDebug()
{
}
