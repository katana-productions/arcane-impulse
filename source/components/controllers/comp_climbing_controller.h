#pragma once
#include "components\player\comp_player_move.h"
#include "components\player\comp_player_magic.h"

using namespace physx;

class TCompClimbingController : public TCompBase
{
private:
	TCompCharController* characterController;
	TCompPlayerMove* playerMove;
	TCompPlayerMagic* playerMagic;
	TCompTransform* myTrans;

	//Config vars that can be modified in JSON config file
	bool enabled = false;
	bool IsClimbing = false;
	bool IsPullingUp = true;
	bool headRaycast1, headRaycast2, headRaycast3;
	bool frontRaycast;
	bool downRaycast;
	bool isPlatform = false;
	bool climbSound = false;
	PxRigidBody *platRb;

	//PhysX stuff
	PxRaycastHit headRaycastHit;
	PxRaycastHit frontRaycastHit;
	PxRaycastHit downRaycastHit;
	PxRaycastHit footRaycastHit;
	PxQueryFilterData groundDetectionFilter;
	PxQueryFilterData headDetectionFilter;

	//Settings
	float distPush = 0.0;
	float maxDistPush = 1.5f;
	float distUp = 0.0;
	float maxDistUp = 1.5f;
	CTransform originalOrientation;
	VEC3 prevPos;

	float stepByDelta = 6.0f;
	float stepPlatf = 12.0f;

	float offsetFrontRay = 3.0f;
	float distFrontRay = 1.6f;

	float offsetDownRayX = 0.7f;
	float offsetDownRayY = 3.0f;
	float distDownRay = 3.0f;

	float distHeadRay = 3.5f;

	float distCheck = 1.0f;

	void init(const TMsgEntityCreated& msg);
	void HandlePlayerClimb(float dt);
	void CheckClimbRay();
	void SetUpPhysXFilters();
	void SetFootPos(VEC3 new_pos);

public:
	DECL_SIBLING_ACCESS();
	void debugInMenu();
	void renderDebug();
	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);
	static void registerMsgs();

};