#pragma once

#include "components/common/comp_base.h"
#include "components/common/comp_transform.h"
#include "components/common/comp_collider.h"
#include "entity/entity.h"
#include "geometry/curve.h"

enum PlatformMode
{
  PingPong,
  Circuit
};

class TCompPlatfController : public TCompBase
{
private:
  bool enabled;
  int direction = 1;
  int prevDirection = 1;
  float currentDist = 0.0f;
  float currentRatio = 0.0f;
  float speed = 0.0f;
  float stopping_time;
  float current_stop_time = 0.0f;
  float time = 0.0f;
  float frequency = 2.0f;
  float amplitude = 0.5f;
  bool isStopped = false;
  bool stopOnce = false;
  bool dontStop = false;
  PlatformMode mode;
  CCurve *curve = nullptr;
  TCompTransform *myTrans;
  TCompCollider *myCol;

  int current_stop_ratio = 0;
  std::vector<float> stopping_ratios;

  void onEntityCreated(const TMsgEntityCreated& msg);
  void onSceneCreated(const TMsgSceneCreated& msg);
  void keyEntered(const TMsgPuzzleTriggerEntered& msg);
  void movePlatform();

public:
  DECL_SIBLING_ACCESS();
  static void registerMsgs();
  void update(float dt);
  void debugInMenu();
  void load(const json& j, TEntityParseContext& ctx);
  void renderDebug();
};
