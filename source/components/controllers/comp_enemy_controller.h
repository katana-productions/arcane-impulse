#pragma once

#include "components/common/comp_base.h"
#include "entity/common_msgs.h"

class TCompEnemyController : public TCompBase
{
	DECL_SIBLING_ACCESS();
	void onPowerMessage(const TMsgIntObjState& msg);
	void onAlarmRecieve(const TMsgAlarmClock& msg);
	void onEntityCreated(const TMsgEntityCreated& msg);

public:
	void update(float dt);
	void debugInMenu();
	void load(const json& j, TEntityParseContext& ctx);
	static void registerMsgs();
	
	void newMoveDirection(VEC3 newDirNormalized, const float& speedMultiplier, VEC3 lookAtPos);
	void lookAt(VEC3 lookAtPos);

	bool enableMovement();
	float getVelocity();

private:
	float speed = 1;
	float acceleration = 10;
	float rotationSpeed = 2.0f;
	float drag = 0.3f;

	int lastState;
	bool enabled = true;
	bool checkEnable = false;


	VEC3 lastLookAtPosition = VEC3(0, 0, 0);
	VEC3 lookAtPosition = VEC3(0, 0, 0);
	VEC3 direction = VEC3(0, 0, 0);

	float multiplier = 0;
};