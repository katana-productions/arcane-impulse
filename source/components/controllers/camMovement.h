#pragma once
#include "entity/common_msgs.h"
#include "components/common/comp_base.h"
#include "components/common/comp_transform.h"
#include "components/controllers/camShake.h"

class TCompCamMove : public TCompBase
{
private:
  bool enabled = true;
	CHandle camShakeH;
	TCompCamShake* camShake;
	VEC3 currentP = VEC3(0, 0, 0);
	VEC3 currentPShake = VEC3(0, 0, 0);
	VEC3 targetP = VEC3(0,0,0);
	VEC3 targetPShake = VEC3(0,0,0);
	CHandle myHandle;
	float prevPos = 0;

	void StartHeadBob(const float& value);

public:
	DECL_SIBLING_ACCESS();
	void onEntityCreated(const TMsgEntityCreated& msg);
	void onAlarmRecieve(const TMsgAlarmClock& msg);
	static void registerMsgs();

	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);
	void debugInMenu();

	float walkAmplitude = 0.1f;
	float walkSpeed = 6.0f;
	float multYWalk = 2.0f;
	float runAmplitude = 0.2f;
	float runSpeed = 10.0f;
	float multYRun = 2.0f;
	float idleAmplitude = 0.1f;
	float idleSpeed = 2.5f;
	float smoothVec = 10.0f;
	float jumpY = 0.25f;
	float landY = -0.5f;

	TCompCameraFP* cameraFP;

	enum camStates
	{
		idle = 0,
		walk,
		run,
		land,
		jump,
		headBob
	};
	camStates states = idle;
	void SetState(const camStates& state);
  bool getEnabled() { return enabled; };
  void setEnabled(bool b) { enabled = b; };
  void toggleEnabled() { enabled = !enabled; };
};