#include "mcv_platform.h"
#include "comp_camera_fp.h"
#include "engine.h"
#include "input/input.h"
#include "entity/entity.h"
#include "components/controllers/camShake.h"

DECL_OBJ_MANAGER("camera_fp_controller", TCompCameraFP);

void TCompCameraFP::debugInMenu()
{
  ImGui::DragFloat("Sensitivity", &_sensitivity, 0.001f, 0.0f, 20.0f);
  ImGui::DragFloat("ControllerSensMult", &_controllerSMult, 0.1f, 0.1f, 20.0f);
  ImGui::SliderFloat3("Offset", &_offset.x, -10.0f, 10.0f);
}

void TCompCameraFP::load(const json& j, TEntityParseContext& ctx)
{
  _offset = loadVEC3(j, "offset");
  //_sensitivity = j.value("sensitivity", _sensitivity);
  _controllerSMult = j.value("controllerSensMult", _controllerSMult);

  json cfg = loadJson("data/config.json");
  const json& cfg_settings = cfg["settings"];
  _sensitivity = cfg_settings.value("sensitivity", 0.01);

  trans_offset.setPosition(_offset);
}

void TCompCameraFP::update(float dt)
{
  if (_docked)
  {
    CEntity *camShakeEnt = getEntityByName("CameraShaker");
    TCompCamShake *camShake = camShakeEnt->get<TCompCamShake>();
    camShake->setEnabled(true);
    if (trans_player == nullptr) {
      //Cache player transform
      CEntity* player = (CEntity *)getEntityByName("Player");
      trans_player = player->get<TCompTransform>();
    }
    trans_offset.setPosition(_offset);

    if (EngineSettings.getCursorVisibilityEnabled())
    {
      c_transform = get<TCompTransform>();

      if (!c_transform || !trans_player)
        return;

      // Get Raw Mouse Motion
      auto& mouse = EngineInput.mouse();
      VEC2 motion = mouse.getDelta();
      if (!_enabled) motion = VEC2(0, 0);

      float yaw, pitch;
      c_transform->getAngles(&yaw, &pitch);

      yaw -= EngineInput["Cam_Right"].value * _sensitivity * dt * _controllerSMult;
      pitch -= EngineInput["Cam_Up"].value * _sensitivity * dt * _controllerSMult;

      yaw += -motion.x * _sensitivity;
      pitch += motion.y * _sensitivity;

      if (pitch < -_maxPitch)
        pitch = -_maxPitch;
      else if (pitch > _maxPitch)
        pitch = _maxPitch;

      c_transform->setAngles(yaw, pitch, 0.0f);
    }
    c_transform->setPosition(trans_player->getPosition() + _offset + _headMoveOffset);
  }
  else
  {
    CEntity *camShakeEnt = getEntityByName("CameraShaker");
    TCompCamShake *camShake = camShakeEnt->get<TCompCamShake>();
    camShake->setEnabled(false);
  }
}

VEC3 TCompCameraFP::limitPitchMovement(VEC3 front) {
  return VEC3(front.x, 0, front.z);
}
