#pragma once
#include "engine.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "physics/module_physics.h"
#include "components/common/comp_base.h"
#include "components/common/comp_collider.h"
#include "components/common/comp_transform.h"

using namespace physx;

class TCompCharController : public TCompBase
{
	DECL_SIBLING_ACCESS();
	void onEntityCreated(const TMsgEntityCreated& msg);
	void onAlarmRecieve(const TMsgAlarmClock& msg);
	void onTriggerEnter(const TMsgEntityTriggerEnter& msg);
	void onRecieveDamage(const TMsgDamage& msg);

public:
	//public in order to enable external lerping
	float gravity = 20.0f;
	float iniGravity;
	float forceDecayAmountGround = 5.0f;
	float iniForceDecayAmountGround;
	CHandle groundHandle;
	VEC3 currentVel;

	static void registerMsgs();
	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);
	void debugInMenu();
	void renderDebug();

	void teleport(VEC3 pos, VEC3 front);

	void ApplyForces();
	void newMoveDirection(const VEC3& newDirNormalized, const bool& rotation = false);
	void newMoveAndLook(const VEC3& newDirNormalized, VEC3& posLook);
	void jump(const float& forceMultiplier = 1.0f, const VEC3& jumpDirNorm = VEC3(0, 1, 0));
	void addExternForce(const VEC3& force);
	void addExternForcePower(const VEC3& force);
	void lookAt(const VEC3& lookAtPos);
	bool IsGrounded() { return isGrounded; };
	void SetFootPos(const VEC3& new_pos);
	VEC3 GetFootPos() { return PXVEC3_TO_VEC3(comp_collider->controller->getFootPosition()); };
	float GetOverfootDist() { return overFootDist; };
	float GetGroundRayDist() { return groundRayDist; };
	float GetGroundCheckDist() { return checkGndDist; };
	bool Check3GroundedRays();
	void ChangeGravityMult(const float& newValue);
	float GetVerticalSpeed() { return jumpForce.y + externForce.y; };
	float GetTerminalVelocity() { return verticalTerminalVel; };
	VEC3 GetExternForce() { return externForce; }
	float GetMaxSpeed() { return speed; }
	void setMaxSpeed(float maxSpeed) { speed = maxSpeed; }
	float getForceFalling() { return jumpForce.y; }
	void MovingPlatform();
	void StopFriction();

	void toggleEnabled() { enabled = !enabled; }
	void setEnabled(bool b) { enabled = b; }

private:
	void CheckCeilingRay();
	void CheckPlatform();
	void HandleGravity();
	void RotateAndInputCorrect(VEC3 *lookTo = nullptr, bool rotation = false);
	void RotateTowardsVelocity();
	void PushDynamicObjects();
	void SetUpPhysXFilters();

	//Config vars that can be modified in JSON config file
	bool enabled = true;
	float stickToGroundForce = 10.0f;
	float speed = 15.0f;
	float airSpeedMult = 0.75f;
	float rotationSpeed = 10.0f;
	float jumpF = 15.0f;
	float checkGndDist = 0.5f;
	float jumpFSecond = 30.0f;
	float forceDecayAmount = 2.0f;
	float groundRayDist = 1.5f;
	float pushRayDist = 1.0f;
	bool setOffset = false;
	float stepOffset = 4.0f;
	float verticalTerminalVel = 40.0f;
	bool canPush = false;
	float pushForceMult = 1.0f;
	float headBumpOffset = 2.0f;

	//PhysX stuff
	PxControllerCollisionFlags ccColFlags;
	PxRaycastHit groundRayHit, ceilingRayHit, pushRayHit;
	PxQueryFilterData groundDetectionFilter, ceilingDetectionFilter, pushDetectionFilter;

	//Internal stuff that shouldn't be modified
	bool isGrounded = false;
	bool canJump = true;
	bool canHeadBump = true;
	float timeBetweenJumps = 0.5f;
	float deltaTime = 0.0f;
	TCompCollider* comp_collider;
	TCompTransform* c_my_transform;
	VEC3 input = VEC3(0, 0, 0);
	VEC3 moveForce = VEC3(0, 0, 0);
	VEC3 jumpForce = VEC3(0, 0, 0);
	VEC3 externForce = VEC3(0, 0, 0);
	VEC3 front = VEC3(0, 0, 0);
	float overFootDist = 1.0f;
	float gravityMult = 1.0f;
	bool isPushing = false;
	VEC3 pushDir = VEC3(0, 0, 0);
	bool frictionActive = true;

	// Platform inertia
	bool platfInertia;
	TCompTransform *platTrans;
	VEC3 oldpos;
	VEC3 platForce;
	bool isOnPlatform = false;
	bool wasOnPlatform = false;
	bool jumpedFromPlat = false;
};