#include "mcv_platform.h"
#include "comp_char_controller.h"

DECL_OBJ_MANAGER("char_controller", TCompCharController);

void TCompCharController::registerMsgs() {
	DECL_MSG(TCompCharController, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompCharController, TMsgAlarmClock, onAlarmRecieve);
	DECL_MSG(TCompCharController, TMsgEntityTriggerEnter, onTriggerEnter);
	DECL_MSG(TCompCharController, TMsgDamage, onRecieveDamage);
}

void TCompCharController::onEntityCreated(const TMsgEntityCreated& msg) {
	if (setOffset)
	{
		comp_collider = get<TCompCollider>();
		assert(comp_collider);
		comp_collider->controller->setStepOffset(stepOffset);
	}
}

void TCompCharController::onAlarmRecieve(const TMsgAlarmClock& msg) {
	if (msg.id == 1) canJump = true;
	if (msg.id == 99) canHeadBump = true;
	if (msg.id == 413) frictionActive = true;
}

void TCompCharController::onTriggerEnter(const TMsgEntityTriggerEnter& msg) { }

void TCompCharController::onRecieveDamage(const TMsgDamage& msg)
{
	addExternForce(msg.force);
}

void TCompCharController::update(float scaled_dt) {
	//if (!enabled) return;
	deltaTime = scaled_dt;

	comp_collider = get<TCompCollider>();
	c_my_transform = get<TCompTransform>();
	if (!comp_collider || !comp_collider->controller || !c_my_transform) return;
	front = c_my_transform->getFront();

	CheckCeilingRay();

	HandleGravity();

	if (frictionActive)
	{
		if (!isGrounded) externForce = VEC3::Lerp(externForce, VEC3(0, 0, 0), forceDecayAmount * deltaTime);
		else externForce = VEC3::Lerp(externForce, VEC3(0, 0, 0), forceDecayAmountGround * deltaTime);
	}

	MovingPlatform();

	ApplyForces();

	if (!enabled) moveForce *= VEC3(0, 1, 0);
	ccColFlags = comp_collider->controller->move(VEC3_TO_PXVEC3(moveForce) * deltaTime,
		0.0f, deltaTime, PxControllerFilters());
	isGrounded = ccColFlags.isSet(physx::PxControllerCollisionFlag::eCOLLISION_DOWN);

	currentVel = PXVEC3_TO_VEC3(comp_collider->controller->getActor()->getLinearVelocity());

	if (canPush) PushDynamicObjects();

	input = VEC3(0, 0, 0);
	moveForce = VEC3(0, 0, 0);
}

void TCompCharController::ApplyForces()
{
	if (platfInertia)
	{
		if (isGrounded and jumpedFromPlat)
		{
			jumpedFromPlat = false;
			platForce = VEC3(0, 0, 0);
		}

		if (!isGrounded and wasOnPlatform)
		{
			wasOnPlatform = false;
			jumpedFromPlat = true;
		}

		if (!isGrounded and jumpedFromPlat) {
			platForce = VEC3::Lerp(platForce, VEC3(0, 0, 0), 0.1f * deltaTime);
			moveForce = moveForce + jumpForce + externForce + platForce;
		}
		else moveForce = moveForce + jumpForce + externForce;
	}
	else moveForce = moveForce + jumpForce + externForce;
}

void TCompCharController::CheckCeilingRay()
{
	if (!canHeadBump) return;
	if (EnginePhysics.Raycast(PXVEC3_TO_VEC3(comp_collider->controller->getFootPosition()) + VEC3(0.f, 0.1f, 0.f),
		VEC3(0.0f, 1.0f, 0.0f), headBumpOffset, ceilingRayHit,
		(physx::PxQueryFlag::eSTATIC | physx::PxQueryFlag::eDYNAMIC), physx::PxHitFlag::eDEFAULT, ceilingDetectionFilter)) {
		jumpForce.y = 0.0f;
		externForce.y = 0.0f;

		canHeadBump = false;
		CHandle myHandle = CHandle(this);
		EngineAlarms.AddAlarm(0.5f, 99, myHandle);
	}
}

void TCompCharController::HandleGravity()
{
	if (isGrounded && canJump) jumpForce = VEC3(0.0f, -stickToGroundForce, 0.0f);
	else
	{
		jumpForce.y -= gravity * deltaTime * gravityMult;
		if (-jumpForce.y > verticalTerminalVel) jumpForce.y = -verticalTerminalVel;
	}
}

void TCompCharController::RotateAndInputCorrect(VEC3* lookTo, bool rotation)
{
	if (lookTo == nullptr)
	{
		if (rotation) RotateTowardsVelocity();
	}
	else {
		lookAt(*lookTo);
	}

	//This if could be (isGrounded) if grounded values didn't jitter randomly
	if (Check3GroundedRays()) {
		isGrounded = true;
		VEC3 temp = PXVEC3_TO_VEC3(groundRayHit.normal.cross(VEC3_TO_PXVEC3(input)));
		input = temp.Cross(PXVEC3_TO_VEC3(groundRayHit.normal));

		groundHandle.fromVoidPtr(groundRayHit.actor->userData);
		groundHandle = groundHandle.getOwner();
	}

	moveForce = input * speed;
	if (!isGrounded) moveForce *= airSpeedMult;
}

void TCompCharController::newMoveDirection(const VEC3& newDirNormalized, const bool& rotation)
{
	if (!enabled) return;
	PROFILE_FUNCTION("char_controller");
	input = newDirNormalized;
	RotateAndInputCorrect(nullptr, rotation);
}

void TCompCharController::newMoveAndLook(const VEC3& newDirNormalized, VEC3& posLook)
{
	if (!enabled) return;
	input = newDirNormalized;
	RotateAndInputCorrect(&posLook);
}

void TCompCharController::lookAt(const VEC3& lookAtPos) {
	float yaw, pitch;
	c_my_transform->getAngles(&yaw, &pitch);
	c_my_transform->setAngles(c_my_transform->getDeltaYawToAimTo(lookAtPos) + yaw, 0.0f, 0.0f);
}

void TCompCharController::jump(const float& forceMultiplier, const VEC3& jumpDirNorm)
{
	externForce.y = 0.0f;
	jumpForce = jumpDirNorm * forceMultiplier * jumpF;
	//dbg("JumpF: %f, %f, %f  %f\n", jumpDirNorm.x, jumpDirNorm.y, jumpDirNorm.z, Time.current);
	canJump = false;
	CHandle myHandle = CHandle(this);
	EngineAlarms.AddAlarm(timeBetweenJumps, 1, myHandle);
}

void TCompCharController::addExternForce(const VEC3& force)
{
	jumpForce = VEC3(0.0f, 0.0f, 0.0f);
	externForce = externForce + force;
}

void TCompCharController::addExternForcePower(const VEC3& force)
{
	canJump = false;
	CHandle myHandle = CHandle(this);
	EngineAlarms.AddAlarm(0.1f, 1, myHandle);

	externForce = VEC3(0.0f, 0.0f, 0.0f);
	jumpForce = VEC3(0.0f, 0.0f, 0.0f);
	addExternForce(force);
}

void TCompCharController::RotateTowardsVelocity()
{
	float yaw, pitch;
	c_my_transform->getAngles(&yaw, &pitch);
	c_my_transform->setAngles((c_my_transform->getDeltaYawToAimTo(c_my_transform->getPosition()
		+ VEC3(input.x, 0.0f, input.z))
		* rotationSpeed * deltaTime) + yaw, 0.0f, 0.0f);
}

void TCompCharController::SetFootPos(const VEC3& new_pos) {
	comp_collider = get<TCompCollider>();
	comp_collider->controller->setFootPosition(VEC3_TO_PXEXTENDEDVEC3(new_pos));
}

bool TCompCharController::Check3GroundedRays()
{
	comp_collider = get<TCompCollider>();
	return EnginePhysics.Raycast(PXVEC3_TO_VEC3(comp_collider->controller->getFootPosition()) + VEC3(0.0f, overFootDist, 0.0f),
		VEC3(0.0f, -1.0f, 0.0f), groundRayDist, groundRayHit,
		(physx::PxQueryFlag::eSTATIC | physx::PxQueryFlag::eDYNAMIC), physx::PxHitFlag::eDEFAULT, groundDetectionFilter) ||
		EnginePhysics.Raycast(PXVEC3_TO_VEC3(comp_collider->controller->getFootPosition()) + VEC3(0.0f, overFootDist, 0.0f) + (front * checkGndDist),
			VEC3(0.0f, -1.0f, 0.0f), groundRayDist, groundRayHit,
			(physx::PxQueryFlag::eSTATIC | physx::PxQueryFlag::eDYNAMIC), physx::PxHitFlag::eDEFAULT, groundDetectionFilter) ||
		EnginePhysics.Raycast(PXVEC3_TO_VEC3(comp_collider->controller->getFootPosition()) + VEC3(0.0f, overFootDist, 0.0f) - (front * checkGndDist),
			VEC3(0.0f, -1.0f, 0.0f), groundRayDist, groundRayHit,
			(physx::PxQueryFlag::eSTATIC | physx::PxQueryFlag::eDYNAMIC), physx::PxHitFlag::eDEFAULT, groundDetectionFilter);
}

void TCompCharController::CheckPlatform()
{
	PxRaycastHit hit1, hit2, hit3;
	comp_collider = get<TCompCollider>();
	bool b1 = EnginePhysics.Raycast(PXVEC3_TO_VEC3(comp_collider->controller->getFootPosition()) + VEC3(0.0f, overFootDist, 0.0f) - (front * checkGndDist),
		VEC3(0.0f, -1.0f, 0.0f), groundRayDist, hit1,
		(physx::PxQueryFlag::eSTATIC | physx::PxQueryFlag::eDYNAMIC), physx::PxHitFlag::eDEFAULT, groundDetectionFilter);
	bool b2 = EnginePhysics.Raycast(PXVEC3_TO_VEC3(comp_collider->controller->getFootPosition()) + VEC3(0.0f, overFootDist, 0.0f) + (front * checkGndDist),
		VEC3(0.0f, -1.0f, 0.0f), groundRayDist, hit2,
		(physx::PxQueryFlag::eSTATIC | physx::PxQueryFlag::eDYNAMIC), physx::PxHitFlag::eDEFAULT, groundDetectionFilter);
	bool b3 = EnginePhysics.Raycast(PXVEC3_TO_VEC3(comp_collider->controller->getFootPosition()) + VEC3(0.0f, overFootDist, 0.0f),
		VEC3(0.0f, -1.0f, 0.0f), groundRayDist, hit3,
		(physx::PxQueryFlag::eSTATIC | physx::PxQueryFlag::eDYNAMIC), physx::PxHitFlag::eDEFAULT, groundDetectionFilter);
	if (b1 or b2 or b3)
	{
		PxRaycastHit hit;
		if (b1) hit = hit1;
		else if (b2) hit = hit2;
		else hit = hit3;
		CHandle platHdl;
		platHdl.fromVoidPtr(hit.actor->userData);
		if (platHdl.isValid())
		{
			CEntity *platEnt = platHdl.getOwner();
			if (platEnt != nullptr)
			{
				TCompCollider *platCol = platEnt->get<TCompCollider>();
				PxRigidBody *platRb = platCol->actor->is<PxRigidBody>();
				if (platRb != nullptr and platRb->getRigidBodyFlags().isSet(physx::PxRigidBodyFlag::eKINEMATIC))
				{
					platTrans = platEnt->get<TCompTransform>();
					platForce = PXVEC3_TO_VEC3(platRb->getLinearVelocity());
					wasOnPlatform = true;
					if (!isOnPlatform) oldpos = platTrans->getPosition();
					isOnPlatform = true;
				}
			}
		}
	}
}

void TCompCharController::ChangeGravityMult(const float & newValue)
{
	if (newValue == 1.0f)
	{
		gravityMult = 1.0f;
		return;
	}
	if (jumpForce.y > 0) gravityMult = newValue;
	else gravityMult = 1.0f;
}

void TCompCharController::MovingPlatform()
{
	isOnPlatform = false;
	CheckPlatform();

	// Follow platf height check
	if (isOnPlatform and platTrans != nullptr)
	{
		TCompTransform *myTrans = get<TCompTransform>();
		VEC3 plat_move = VEC3(0, platTrans->getPosition().y - oldpos.y, 0);
		comp_collider->controller->move(VEC3_TO_PXVEC3(plat_move) * deltaTime, 0.0f, deltaTime, PxControllerFilters());
		oldpos = platTrans->getPosition();
	}
}

void TCompCharController::StopFriction()
{
	frictionActive = false;
	CHandle myHandle = CHandle(this);
	EngineAlarms.AddAlarm(0.2f, 413, myHandle);
}

void TCompCharController::PushDynamicObjects()
{
	isPushing = ccColFlags.isSet(physx::PxControllerCollisionFlag::eCOLLISION_SIDES);
	if (isPushing)
	{
		pushDir = moveForce;
		pushDir.Normalize();
		float pushForce = magnitude(moveForce);
		if (magnitude(pushDir) > 0.1f && EnginePhysics.Raycast(PXVEC3_TO_VEC3(comp_collider->controller->getFootPosition()) + VEC3(0.0f, overFootDist, 0.0f),
			pushDir, pushRayDist, pushRayHit, physx::PxQueryFlag::eDYNAMIC, physx::PxHitFlag::eDEFAULT, pushDetectionFilter))
		{
			PxRigidDynamic *rd = pushRayHit.actor->is<PxRigidDynamic>();
			if (rd and !rd->getRigidBodyFlags().isSet(PxRigidBodyFlag::eKINEMATIC))
			{
				PxRigidBodyExt::addForceAtPos(*rd, VEC3_TO_PXVEC3(pushDir) * pushForce * pushForceMult * deltaTime,
					pushRayHit.position, physx::PxForceMode::eFORCE);
			}
		}
	}
}

void TCompCharController::SetUpPhysXFilters()
{
	PxFilterData pxFilterData;
	pxFilterData.word0 = CModulePhysics::FilterGroup::Scenario;
	groundDetectionFilter.data = pxFilterData;
	ceilingDetectionFilter.data = pxFilterData;
	pxFilterData.word0 = CModulePhysics::FilterGroup::Objects;
	pushDetectionFilter.data = pxFilterData;
}

//====================================================================================

void TCompCharController::load(const json& j, TEntityParseContext& ctx) {
	enabled = j.value("enabled", enabled);
	stickToGroundForce = j.value("stickToGroundForce", stickToGroundForce);
	gravity = j.value("gravity", gravity);
	iniGravity = gravity;
	speed = j.value("speed", speed);
	airSpeedMult = j.value("airSpeedMult", airSpeedMult);
	rotationSpeed = j.value("rotationSpeed", rotationSpeed);
	jumpF = j.value("jumpF", jumpF);
	checkGndDist = j.value("checkGndDist", checkGndDist);
	jumpFSecond = j.value("jumpFSecond", jumpFSecond);
	forceDecayAmount = j.value("forceDecayAmount", forceDecayAmount);
	forceDecayAmountGround = j.value("forceDecayAmountGround", forceDecayAmountGround);
	iniForceDecayAmountGround = forceDecayAmountGround;
	groundRayDist = j.value("groundRayDist", groundRayDist);
	setOffset = j.value("setOffset", setOffset);
	stepOffset = j.value("stepOffset", stepOffset);
	verticalTerminalVel = j.value("verticalTerminalVel", verticalTerminalVel);
	canPush = j.value("canPush", canPush);
	pushForceMult = j.value("pushForceMult", pushForceMult);
	headBumpOffset = j.value("headBumpOffset", headBumpOffset);
	platfInertia = j.value("platfInertia", false);

	SetUpPhysXFilters();
}

void TCompCharController::debugInMenu() {
	ImGui::LabelText("Velocity", "%f %f %f", moveForce.x, moveForce.y, moveForce.z);
	ImGui::Checkbox("Enabled", &enabled);
	ImGui::Checkbox("Grounded", &isGrounded);
	ImGui::DragFloat("stickToGroundForce", &stickToGroundForce, 0.5f, 0.0f, 100.0f);
	ImGui::DragFloat("Gravity", &gravity, 0.01f, -10.0f, 10.0f);
	ImGui::DragFloat("speed", &speed, 0.5f, 0.0f, 100.0f);
	ImGui::DragFloat("airSpeedMult", &airSpeedMult, 0.01f, 0.0f, 1.0f);
	ImGui::DragFloat("rotationSpeed", &rotationSpeed, 0.5f, 0.0f, 100.0f);
	ImGui::DragFloat("jumpF", &jumpF, 0.5f, 0.0f, 100.0f);
	ImGui::DragFloat("checkGndDist", &checkGndDist, 0.01f, 0.0f, 2.0f);
	ImGui::DragFloat("jumpFSecond", &jumpFSecond, 0.5f, 0.0f, 100.0f);
	ImGui::DragFloat("forceDecayAmount", &forceDecayAmount, 0.1f, 0.0f, 10.0f);
	ImGui::DragFloat("forceDecayAmountGround", &forceDecayAmountGround, 0.2f, 0.0f, 20.0f);
	ImGui::DragFloat("groundRayDist", &groundRayDist, 0.1f, 0.0f, 10.0f);
	ImGui::DragFloat("verticalTerminalVel", &verticalTerminalVel, 0.5f, 0.0f, 100.0f);
	ImGui::Checkbox("canPush", &canPush);
	ImGui::DragFloat("pushForceMult", &pushForceMult, 0.02f, 0.0f, 5.0f);
	ImGui::DragFloat("headBumpOffset", &headBumpOffset, 0.02f, 0.0f, 5.0f);
}

void TCompCharController::renderDebug()
{
	TCompTransform *c_my_transform = get<TCompTransform>();
	assert(c_my_transform);

	TCompCollider* comp_collider = get<TCompCollider>();
	if (!comp_collider || !comp_collider->controller)
		return;

	VEC4 color = VEC4(1, 0, 0, 1);
	if (isGrounded) color = VEC4(1, 1, 1, 1);

	drawCircle(PXVEC3_TO_VEC3(comp_collider->controller->getFootPosition()), 0.1f, VEC4(0, 1, 0, 1));

	drawCircle(PXVEC3_TO_VEC3(comp_collider->controller->getPosition()), comp_collider->controller->getRadius(), color);

	drawCircle(PXVEC3_TO_VEC3(comp_collider->controller->getPosition()) -
		VEC3(0.0f, comp_collider->controller->getHeight() / 2.0f, 0.0f), comp_collider->controller->getRadius(), color);

	drawLine(PXVEC3_TO_VEC3(comp_collider->controller->getFootPosition()) + VEC3(0.0f, overFootDist, 0.0f),
		PXVEC3_TO_VEC3(comp_collider->controller->getFootPosition()) + VEC3(0.0f, overFootDist, 0.0f) - VEC3(0.0f, groundRayDist, 0.0f),
		color);

	drawLine(PXVEC3_TO_VEC3(comp_collider->controller->getFootPosition()) + VEC3(0.0f, overFootDist, 0.0f) + (front * checkGndDist),
		PXVEC3_TO_VEC3(comp_collider->controller->getFootPosition()) + VEC3(0.0f, overFootDist, 0.0f) + (front * checkGndDist) - VEC3(0.0f, groundRayDist, 0.0f),
		color);

	drawLine(PXVEC3_TO_VEC3(comp_collider->controller->getFootPosition()) + VEC3(0.0f, overFootDist, 0.0f) - (front * checkGndDist),
		PXVEC3_TO_VEC3(comp_collider->controller->getFootPosition()) + VEC3(0.0f, overFootDist, 0.0f) - (front * checkGndDist) - VEC3(0.0f, groundRayDist, 0.0f),
		color);

	drawLine(PXVEC3_TO_VEC3(comp_collider->controller->getFootPosition()),
		PXVEC3_TO_VEC3(comp_collider->controller->getFootPosition()) + VEC3(0.0f, 2.1f, 0.0f),
		VEC4(1, 0, 1, 1));

	VEC3 mypos = PXVEC3_TO_VEC3(comp_collider->controller->getPosition()) + (c_my_transform->getFront() * 2.0f);
	drawWiredSphere(mypos, 0.2f, color);
}

void TCompCharController::teleport(VEC3 pos, VEC3 front)
{
	comp_collider->controller->setFootPosition(VEC3_TO_PXEXTENDEDVEC3(pos));
	lookAt(c_my_transform->getPosition() + front);
}