#pragma once

#include "components/common/comp_base.h"
#include "components/common/comp_transform.h"
#include "entity/common_msgs.h"
#include "entity/entity.h"

class TCompCanonController : public TCompBase
{
private:
  VEC3 offset; // Offset where the bombs will be created
  float height; // Maximum height of the parabola
  float rof; // Rate of fire in seconds
  float time = 0.0f;
  float start_delay = 0.0f; //Time delay before cannon starts firing
  int current_target = 0;
  std::vector<VEC3> targets;

  TCompTransform *myTransform;

  void onEntityCreated(const TMsgEntityCreated& msg);
  void onSceneCreated(const TMsgSceneCreated& msg);

public:
  DECL_SIBLING_ACCESS();

  void load(const json& j, TEntityParseContext& ctx);
  void update(float dt);
  void renderDebug();
  void debugInMenu();
  static void registerMsgs();
};

