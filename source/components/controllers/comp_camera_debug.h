#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"

class TCompCameraDebug: public TCompBase
{
  DECL_SIBLING_ACCESS();

public:
  void debugInMenu();
  void load(const json& j, TEntityParseContext& ctx);
  void update(float dt);

private:
  float _speed = 20.f;
  float _sensitivity = 1.0f;
  float _maxPitch = (float)M_PI_2 - 1e-4f;
  VEC3  _ispeed;
  bool  _enabled = true;
  std::string  _key_toggle_enabled = "";
};

