#include "mcv_platform.h"
#include "comp_enemy_controller.h"
#include "components/common/comp_collider.h"
#include "components/common/comp_transform.h"
#include "physics/module_physics.h"
#include "engine.h"
#include "entity/entity.h"
#include "entity/entity_parser.h"

using namespace physx;

DECL_OBJ_MANAGER("enemy_controller", TCompEnemyController);

void TCompEnemyController::debugInMenu() {
	ImGui::DragFloat("Speed", &speed, 0.1f, 0.f, 10.f);
	ImGui::DragFloat("Acceleration", &acceleration, 1.f, 0.f, 1000.f);
	ImGui::DragFloat("rotationSpeed", &rotationSpeed, 0.1f, 0.f, 10.f);
	ImGui::DragFloat("drag", &drag, 0.1f, 0.f, 10.f);
}

void TCompEnemyController::load(const json& j, TEntityParseContext& ctx) {
	speed = j.value("speed", speed);
	acceleration = j.value("acceleration", acceleration);
	rotationSpeed = j.value("rotationSpeed", rotationSpeed);
	drag = j.value("drag", drag);
}

void TCompEnemyController::registerMsgs() {
DECL_MSG(TCompEnemyController, TMsgIntObjState, onPowerMessage);
DECL_MSG(TCompEnemyController, TMsgAlarmClock, onAlarmRecieve);
DECL_MSG(TCompEnemyController, TMsgEntityCreated, onEntityCreated);
}

void TCompEnemyController::onEntityCreated(const TMsgEntityCreated& msg) {
	TCompCollider *c_collider = get<TCompCollider>();
	PxRigidDynamic *rb = c_collider->actor->is<PxRigidDynamic>();

	rb->setRigidDynamicLockFlags(PxRigidDynamicLockFlag::eLOCK_ANGULAR_X
		| PxRigidDynamicLockFlag::eLOCK_ANGULAR_Y | PxRigidDynamicLockFlag::eLOCK_ANGULAR_Z);
	rb->setLinearDamping(drag);
}

void TCompEnemyController::onPowerMessage(const TMsgIntObjState& msg) {
	if (msg.state == Pushed || msg.state == Pulled) {
		enabled = false;
		TCompCollider *c_collider = get<TCompCollider>();
		PxRigidDynamic *rb = c_collider->actor->is<PxRigidDynamic>();
		rb->setLinearDamping(1);
	}
	checkEnable = false;
	EngineAlarms.AddAlarm(1, 1, CHandle(this));
	lastState = msg.state;
}


void TCompEnemyController::onAlarmRecieve(const TMsgAlarmClock& msg) {
	if (msg.id == 1) {
		checkEnable = true;
	}
}

void TCompEnemyController::update(float dt) {
	PROFILE_FUNCTION("enemy_controller");
	if (!enabled) { 
		lookAt(lookAtPosition);
		return; 
	}
	CHandle myHandle = CHandle(this);
	if (!myHandle.getOwner().isValid()) return;


	lookAtPosition = VEC3::Lerp(lastLookAtPosition, lookAtPosition, dt * 18);
	lastLookAtPosition = lookAtPosition;
	TCompCollider *c_collider = get<TCompCollider>();
	PxRigidDynamic *rb = c_collider->actor->is<PxRigidDynamic>();

	float value = PXVEC3_TO_VEC3(rb->getLinearVelocity()).Length() / (speed * multiplier);
	float factor = 1 - OurClamp(value, 0.0f, 1.0f);
	VEC3 vel = direction * factor * acceleration * Time.scale_factor;

	rb->addForce(VEC3_TO_PXVEC3(vel), PxForceMode::eVELOCITY_CHANGE);
	lookAt(lookAtPosition);
}


void TCompEnemyController::newMoveDirection(VEC3 newDirNormalized, const float& speedMultiplier, VEC3 lookAtPos) {
	PROFILE_FUNCTION("enemy_controller");

	newDirNormalized.y = 0.f;
	newDirNormalized.Normalize();

	direction = newDirNormalized;
	multiplier = speedMultiplier;
	lookAtPosition = lookAtPos;
}

void TCompEnemyController::lookAt(VEC3 lookAtPos) {
	CHandle myHandle = CHandle(this);
	if (!myHandle.getOwner().isValid()) return;

	TCompTransform *c_my_transform = get<TCompTransform>();
	float yaw, pitch;
	c_my_transform->getAngles(&yaw, &pitch);
	c_my_transform->setAngles(c_my_transform->getDeltaYawToAimTo(lookAtPos) + yaw, 0.0f, 0.0f);
}

bool TCompEnemyController::enableMovement() {
	if ((lastState == Normal || lastState == Pushed) && checkEnable) {
		enabled = true;

		TCompCollider *c_collider = get<TCompCollider>();
		PxRigidDynamic *rb = c_collider->actor->is<PxRigidDynamic>();
		rb->setRigidDynamicLockFlags(PxRigidDynamicLockFlag::eLOCK_ANGULAR_X
			| PxRigidDynamicLockFlag::eLOCK_ANGULAR_Y | PxRigidDynamicLockFlag::eLOCK_ANGULAR_Z);
		rb->setLinearDamping(drag);
		return true;
	}
	return false;
}

float TCompEnemyController::getVelocity() {
	TCompCollider *c_collider = get<TCompCollider>();
	PxRigidDynamic *rb = c_collider->actor->is<PxRigidDynamic>();

	return PXVEC3_TO_VEC3(rb->getLinearVelocity()).Length();
}
