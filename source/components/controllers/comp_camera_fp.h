#pragma once
#include "components/common/comp_base.h"
#include "components/common/comp_transform.h"

class TCompCameraFP : public TCompBase
{
private:
  bool _enabled = true;
  bool _docked = true;
  float _sensitivity = 1.0f;
  float _controllerSMult = 4.0f;
  float _maxPitch = (float)M_PI_2 - 1e-4f;
  VEC3  _offset = VEC3(0, 0, 0);
  TCompTransform* trans_player = nullptr;
  TCompTransform* c_transform;
  CTransform trans_offset;

public:
  DECL_SIBLING_ACCESS();
  void debugInMenu();
  void load(const json& j, TEntityParseContext& ctx);
  void update(float dt);
  VEC3 limitPitchMovement(VEC3 front);
  void toggleEnabled() { _enabled = !_enabled; }
  void setEnabled(bool b) { _enabled = b; }
  void toggleDocked() { _docked = !_docked; }
  void setDocked(bool b) { _docked = !_docked; }

  VEC3  _headMoveOffset = VEC3(0, 0, 0);
};
