#include "mcv_platform.h"
#include "comp_cinematic_camera.h"
#include "engine.h"
#include "input/input.h"

DECL_OBJ_MANAGER("cinematic_camera", TCompCinematicCamera);

void TCompCinematicCamera::debugInMenu()
{

}

void TCompCinematicCamera::setLookAt()
{
	if (cTransform && cTargetTransform) {
		VEC3 pos = cTransform->getPosition();
		VEC3 targetPos = cTargetTransform->getPosition();
		VEC3 lookAtPos = targetPos + offset;
		if (pos != targetPos)
		{
			cTransform->lookAt(pos, lookAtPos);
		}
	}
}

void TCompCinematicCamera::load(const json& j, TEntityParseContext& ctx) {
  targetName = j.value("target", "");
  ratio = j.value("ratio", ratio);
  speed = j.value("speed", speed);
  offset = loadVEC3(j, "offset", VEC3(0, 0, 0));
  std::string curve_name = j.value("curve", "");
  if(curve_name != "") curve = Resources.get(curve_name)->as<CCurve>();
}

void TCompCinematicCamera::update(float dt)
{
  if (cTransform == nullptr && cTargetTransform == nullptr) {
		cTransform = get<TCompTransform>();
		CEntity* eTarget = getEntityByName(targetName);
		if (eTarget != nullptr) {
			cTargetTransform = eTarget->get<TCompTransform>();
			setLookAt();
			originalTransform = cTransform->asMatrix();
		}
		if (curve) {
			VEC3 pos = VEC3::Transform(curve->evaluate(ratio), originalTransform);
			cTransform->setPosition(pos);
			setLookAt();
		}

  }

  if (ratio > 1.f) {
	  enabled = false;
	  return;
  }

  if (curve && enabled) {
	  VEC3 pos = VEC3::Transform(curve->evaluate(ratio), originalTransform);
	  cTransform->setPosition(pos);
	  ratio += dt * speed;
  }

  setLookAt();  
}

void TCompCinematicCamera::renderDebug()
{
  if (curve)
  {
	CTransform tr;
	tr.fromMatrix(originalTransform);
	curve->renderDebug(tr);
  }
}