#include "mcv_platform.h"
#include "camMovement.h"
#include "engine.h"
#include "components/controllers/comp_char_controller.h"
#include "components/controllers/comp_camera_fp.h"
#include "components/common/comp_fsm.h"

DECL_OBJ_MANAGER("camera_move", TCompCamMove);

void TCompCamMove::registerMsgs() {
	DECL_MSG(TCompCamMove, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompCamMove, TMsgAlarmClock, onAlarmRecieve);
}

void TCompCamMove::onEntityCreated(const TMsgEntityCreated& msg) {
	camShakeH = TCompCamShake::shakerHandle;

	CEntity *camera = (CEntity *)getEntityByName("Camera");
	cameraFP = camera->get<TCompCameraFP>();
}

void TCompCamMove::onAlarmRecieve(const TMsgAlarmClock& msg) {
	if (msg.id == 112)
	{
		EngineBasics.LerpElement(&targetP.y, 0.0f, 0.1f);
		myHandle = CHandle(this);
		EngineAlarms.AddAlarm(0.1f, 212, myHandle);
	}
	else if (msg.id == 212) {
		states = idle;
		targetP = VEC3(0, 0, 0);
	}
}

void TCompCamMove::update(float dt)
{
  if (!enabled) return;
	CEntity* eCamShakeH = camShakeH;
	camShake = eCamShakeH->get<TCompCamShake>();
	if (!camShake) return;

	CEntity* e_left_hand = getEntityByName("LeftHand");
	TCompFSM* cFsm_left = e_left_hand->get<TCompFSM>();
	CEntity* e_right_hand = getEntityByName("RightHand");
	TCompFSM* cFsm_right = e_right_hand->get<TCompFSM>();

	switch (states)
	{
	case TCompCamMove::walk:
		cFsm_right->changeVariable("speed", 1, false);
		cFsm_left->changeVariable("speed", 1, false);
		cFsm_right->changeVariable("running", 0, true);
		cFsm_left->changeVariable("running", 0, true);
		cFsm_right->changeVariable("floating", 0, true);
		cFsm_left->changeVariable("floating", 0, true);

		//targetP.x = walkAmplitude * sin(Time.current * walkSpeed);
		targetP.y = walkAmplitude * sin(Time.current * walkSpeed * multYWalk);
		targetPShake.y = 0;

		if ((prevPos * sin(Time.current * walkSpeed) <= 0)) {
			if (!EnginePhysics.isPaused) EngineAudio.playEvent("event:/Movement/Steps", 1);
			prevPos = sin(Time.current * walkSpeed);
		}
		break;
	case TCompCamMove::run:
		cFsm_right->changeVariable("running", 1, true);
		cFsm_left->changeVariable("running", 1, true);
		cFsm_right->changeVariable("speed", 0, false);
		cFsm_left->changeVariable("speed", 0, false);
		cFsm_right->changeVariable("floating", 0, true);
		cFsm_left->changeVariable("floating", 0, true);

		//targetP.x = runAmplitude * sin(Time.current * runSpeed);
		targetP.y = runAmplitude * sin(Time.current * runSpeed * multYRun);
		targetPShake.y = 0;

		if ((prevPos * sin(Time.current * runSpeed) <= 0)) {
			if (!EnginePhysics.isPaused) EngineAudio.playEvent("event:/Movement/Steps", 1);
			prevPos = sin(Time.current * runSpeed);
		}
		break;
	case TCompCamMove::idle:
		targetPShake.y = idleAmplitude * sin(Time.current * idleSpeed);
		cFsm_right->changeVariable("running", 0, true);
		cFsm_left->changeVariable("running", 0, true);
		cFsm_right->changeVariable("speed", 0, false);
		cFsm_left->changeVariable("speed", 0, false);
		cFsm_right->changeVariable("floating", 0, true);
		cFsm_left->changeVariable("floating", 0, true);
		break;
	}

	currentP = VEC3::Lerp(currentP, targetP, smoothVec * dt);
	cameraFP->_headMoveOffset = currentP;

	currentPShake = VEC3::Lerp(currentPShake, targetPShake, smoothVec * dt);
	camShake->movementOffset = currentPShake;
}

void TCompCamMove::SetState(const camStates& state)
{
	if (states == headBob) return;
	targetP = VEC3(0, 0, 0);
	states = state;
	if (states == jump) StartHeadBob(jumpY);
	else if (states == land) StartHeadBob(landY);
}

void TCompCamMove::StartHeadBob(const float & value)
{
	if (value < 0.0f) {
		states = headBob;
		CEntity* playerE = (CEntity *)getEntityByName("Player");
		TCompCharController* playerController = playerE->get<TCompCharController>();
		float landYMult = OurClamp((-playerController->GetVerticalSpeed() / playerController->GetTerminalVelocity()), 0.0f, 1.0f);
		EngineBasics.LerpElement(&targetP.y, value * landYMult, 0.1f);
		myHandle = CHandle(this);
 		EngineAlarms.AddAlarm(0.1f, 112, myHandle);
	}
	//else
	//{
	//	EngineBasics.LerpElement(&targetPShake.y, value, 0.1f);
	//}
}

//===========================================================================

void TCompCamMove::load(const json & j, TEntityParseContext & ctx)
{
	walkAmplitude = j.value("walkAmplitude", walkAmplitude);
	walkSpeed = j.value("walkSpeed", walkSpeed);
	multYWalk = j.value("multYWalk", multYWalk);
	runAmplitude = j.value("runAmplitude", runAmplitude);
	runSpeed = j.value("runSpeed", runSpeed);
	multYRun = j.value("multYRun", multYRun);
	idleAmplitude = j.value("idleAmplitude", idleAmplitude);
	idleSpeed = j.value("idleSpeed", idleSpeed);
	smoothVec = j.value("smoothVec", smoothVec);
	jumpY = j.value("jumpY", jumpY);
	landY = j.value("landY", landY);
}

void TCompCamMove::debugInMenu()
{
	ImGui::DragFloat("walkAmplitude", &walkAmplitude, 0.01f, 0.0f, 1.0f);
	ImGui::DragFloat("walkSpeed", &walkSpeed, 0.1f, 0.0f, 20.0f);
	ImGui::DragFloat("multYWalk", &multYWalk, 0.1f, 0.0f, 5.0f);
	ImGui::DragFloat("runAmplitude", &runAmplitude, 0.01f, 0.0f, 1.0f);
	ImGui::DragFloat("runSpeed", &runSpeed, 0.1f, 0.0f, 30.0f);
	ImGui::DragFloat("multYRun", &multYRun, 0.1f, 0.0f, 5.0f);
	ImGui::DragFloat("idleAmplitude", &idleAmplitude, 0.01f, 0.0f, 1.0f);
	ImGui::DragFloat("idleSpeed", &idleSpeed, 0.1f, 0.0f, 20.0f);
	ImGui::DragFloat("smoothVec", &smoothVec, 0.25f, 0.0f, 50.0f);
	ImGui::DragFloat("jumpY", &jumpY, 0.01f, -2.0f, 2.0f);
	ImGui::DragFloat("landY", &landY, 0.01f, -2.0f, 2.0f);
}