#pragma once

#include "components/common/comp_collider.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

struct CamTransition {
  std::string		camName;
  float			blendTime;
  float     nextTime;
  Interpolator::IInterpolator *interpolator;
};

class TCompCameraTransition : public TCompBase
{
private:
  bool can_skip = false;
  bool skipped = false;
  float time = 0.0f;
  float max_time = 2.0f;
  bool loop = false;
	std::vector< CamTransition > transitions;
	int current_id = 0;
  bool noFinalTransition = false;
	float finalBlendTime = 0.f;
	bool canMove = false;

	void makeTransition();
  void makeFinalTransition();
	void activateUIAndInput();
	void deactivateUIAndInput();

	void objEntered(const TMsgEntityTriggerEnter& msg);
	void onAlarmRecieve(const TMsgAlarmClock& msg);

public:

	DECL_SIBLING_ACCESS();
	static void registerMsgs();
	void update(float dt);
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void renderDebug();
  void start();
  void startInMenu();
  void end();

  std::vector< CamTransition > getTransitions() { return transitions; };

	~TCompCameraTransition();
};