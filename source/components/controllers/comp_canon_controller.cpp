#include "mcv_platform.h"
#include "comp_canon_controller.h"
#include "components/common/comp_collider.h"
#include "engine.h"

DECL_OBJ_MANAGER("canon_controller", TCompCanonController);

void TCompCanonController::load(const json& j, TEntityParseContext& ctx)
{
  offset = loadVEC3(j,"offset");
  rof = j.value("rof", 0.0f);
  start_delay = j.value("start_delay", 0.0f);
  height = j.value("height", 0.0f);
  json v = j["targets"];
  for (auto t : v)
  {
    VEC3 pos = loadVEC3(t);
    targets.push_back(pos);
  }
}

void TCompCanonController::update(float dt)
{
  if (dt >= 0.1f) dt = 0.1f;
  
  if (start_delay > 0.0f) {
	  start_delay -= dt;
  }
  else {
	  time += dt;
  }
  if (time >= rof)
  {
    time = 0.0f;

    // Start ballistic calculus
    // from: https://www.youtube.com/watch?v=IvT8hjy6q4o&feature=youtu.be
    VEC3 origin = myTransform->getPosition() + offset;
    float g = EnginePhysics.gravity;
    float displacementY = targets[current_target].y - (origin.y + height);
    VEC3 displacementXZ = VEC3(targets[current_target].x - origin.x, 0, targets[current_target].z - origin.z);

    // Velocity on Y
    VEC3 vY = VEC3(0,1,0) * sqrt(-2 * g * height);
    VEC3 vXZ = displacementXZ / ((sqrt(-2 * height / g)) + (sqrt(2 * (displacementY) / g)));
    VEC3 finalVel = vXZ + vY;

    CEntity *t = EngineBasics.Instantiate("data/prefabs/bomb.json", origin, 0, 0, 0);
    TCompCollider *bombCol = t->get<TCompCollider>();
    physx::PxRigidDynamic *rb = bombCol->actor->is<physx::PxRigidDynamic>();
    rb->addForce(VEC3_TO_PXVEC3(finalVel), physx::PxForceMode::eVELOCITY_CHANGE);
    rb->addTorque(VEC3_TO_PXVEC3(finalVel), physx::PxForceMode::eVELOCITY_CHANGE);
    ++current_target;
    if (current_target >= targets.size())
      current_target = 0;
  }
}

void TCompCanonController::renderDebug()
{

}

void TCompCanonController::debugInMenu()
{
  ImGui::DragFloat("Rate of fire", &rof, 0.1f, 0.f, 10.0f);
}

void TCompCanonController::registerMsgs() {
  DECL_MSG(TCompCanonController, TMsgEntityCreated, onEntityCreated);
  DECL_MSG(TCompCanonController, TMsgSceneCreated, onSceneCreated);
}

void TCompCanonController::onEntityCreated(const TMsgEntityCreated& msg) 
{
	CEntity* ent = msg.h_entity;
	myTransform = ent->get<TCompTransform>();
}

void TCompCanonController::onSceneCreated(const TMsgSceneCreated& msg)
{
	if (myTransform == nullptr) {
		CEntity* ent = msg.h_entity;
		myTransform = ent->get<TCompTransform>();
	}
}
