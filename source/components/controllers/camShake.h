#pragma once
#include "comp_camera_fp.h"
#include "entity/common_msgs.h"
#include "components/common/comp_hierarchy.h"

class TCompCamShake : public TCompBase
{
private:
  bool enabled = true;
	float shakeIntensity = 0.0f;
	float xInfluence = 1.0f;
	float yInfluence = 1.0f;
	float zInfluence = 1.0f;
	float smoothing = 25.0f;
	float roughnessMult = 1.0f;
	VEC3 targetPos = VEC3(0, 0, 0);
	int nShakes = 0;

public:
	VEC3 shakeOffset = VEC3(0, 0, 0);
	VEC3 movementOffset = VEC3(0, 0, 0);

	DECL_SIBLING_ACCESS();
	void onAlarmRecieve(const TMsgAlarmClock& msg);
	static void registerMsgs();

	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);
	void debugInMenu();

	void startShake(float intensity, float duration, float new_roughnessMult = 1.0f);
  void toggleEnabled() { enabled = !enabled; };
  void setEnabled(bool b) { enabled = b; };

	static CHandle shakerHandle;
};