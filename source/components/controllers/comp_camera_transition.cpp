#include "mcv_platform.h"
#include "comp_camera_transition.h"
#include "comp_cinematic_camera.h"
#include "engine.h"
#include "input/module_input.h"
#include "modules/module_camera_mixer.h"
#include "assert.h"
#include "components/player/comp_ui.h"
#include "components/player/comp_player_magic.h"
#include "ui/module_ui.h"
#include "ui/widgets/ui_progress.h"
#include "ui/widgets/ui_image.h"

DECL_OBJ_MANAGER("camera_transition", TCompCameraTransition);

void TCompCameraTransition::update(float dt)
{
  if (can_skip and EngineInput["any_key"].isPressed())
  {
    time += dt;
    if (time >= max_time)
    {
      skipped = true;
      EngineLua.skipDialogue();
      makeFinalTransition();
      can_skip = false;
      time = 0.0f;
    }
  }
  else time = 0.0f;

  UI::CProgress* skipProgress = dynamic_cast<UI::CProgress*>(Engine.getUI().getWidgetByAlias("skip_progress"));
  skipProgress->setRatio(time / max_time);
}

void TCompCameraTransition::load(const json& j, TEntityParseContext& ctx)
{
  if (j.count("cameras")) {
    CamTransition ct;
    auto& jcams = j["cameras"];
    assert(jcams.is_array());
    for (int i = 0; i < jcams.size(); ++i) {
      ct.camName = jcams[i].value("cam_name", "CameraShaker");
      ct.blendTime = jcams[i].value("blend_time", 0.f);
      ct.nextTime = jcams[i].value("next_time", 2.f);
      std::string interp = jcams[i].value("interpolator", "quad");
      if (interp == "linear")
        ct.interpolator = new Interpolator::TLinearInterpolator();
      else {
        ct.interpolator = new Interpolator::TQuadInOutInterpolator();
      }
      transitions.push_back(ct);
    }
  }
  else {
    /*camera = j.value("camera", "CameraShaker");
    blendTime = j.value("blend_time", 0.f);
    returnTime = j.value("return_time", 2.f);*/
  }
  assert(transitions.size() > 0);

  finalBlendTime = j.value("final_blend_time", finalBlendTime);
  if (finalBlendTime == -1.0f)
    noFinalTransition = true;

  can_skip = j.value("can_skip", false);
}

void TCompCameraTransition::debugInMenu()
{
  for (auto& t : transitions) {
    ImGui::PushID(&t);
    ImGui::LabelText("Camera", "%s", t.camName.c_str());
    ImGui::DragFloat("Blend Time", &t.blendTime, 0.1f, 0.f, 20.f);
    ImGui::DragFloat("Next Time", &t.nextTime, 0.1f, 0.f, 20.f);
    ImGui::PopID();
  }

}

void TCompCameraTransition::renderDebug()
{
  TCompTransform * myTransform = get<TCompTransform>();
  drawWiredSphere(myTransform->getPosition(), 0.2f, VEC4(0, 0, 1, 1));
}

void TCompCameraTransition::registerMsgs()
{
  DECL_MSG(TCompCameraTransition, TMsgEntityTriggerEnter, objEntered);
  DECL_MSG(TCompCameraTransition, TMsgAlarmClock, onAlarmRecieve);
  //DECL_MSG(TCompCameraTransition, TMsgEntityTriggerExit, objExited);
}

void TCompCameraTransition::makeTransition()
{
  assert(current_id <= transitions.size());

  if (current_id < transitions.size()) {
    CamTransition c = transitions[current_id];

    EngineAlarms.AddAlarm(c.blendTime, 1, CHandle(this));
    EngineAlarms.AddAlarm(c.blendTime + c.nextTime, 2, CHandle(this));
    CHandle hCamera = getEntityByName(c.camName);
    if (hCamera.isValid()) Engine.getCameraMixer().blendCamera(hCamera, c.blendTime, c.interpolator);
  }
  else makeFinalTransition();
}

void TCompCameraTransition::makeFinalTransition()
{
  static Interpolator::TQuintOutInterpolator finalInt;

  if (!noFinalTransition) {
    if (!skipped)
    {
      EngineAlarms.AddAlarm(finalBlendTime, 3, CHandle(this));
      CHandle hCamera = getEntityByName("CameraShaker");
      Engine.getCameraMixer().blendCamera(hCamera, finalBlendTime, &finalInt);
    }
    else
    {
      EngineAlarms.AddAlarm(0.1f, 3, CHandle(this));
      CHandle hCamera = getEntityByName("CameraShaker");
      Engine.getCameraMixer().blendCamera(hCamera, 0.1f, &finalInt);

      for (auto c : transitions)
      {
        CHandle h = getEntityByName(c.camName);
        if (h.isValid())
        {
          Engine.getCameraMixer().eraseBlendCamera(h);
        }
      }
    }
    if (can_skip) Engine.getUI().deactivateWidget("skip_cinematic");
  }
}

void TCompCameraTransition::activateUIAndInput()
{
  EngineLua.setUIVisible(true);

  //EnginePhysics.isPaused = false;

  if (!canMove) {
    //Restart player movement/input
    //if (!EngineInput.getActive()) EngineInput.toggleActive();

    EngineLua.unlockPlayer();
    EngineLua.setInputEnabled(true);
  }
}

void TCompCameraTransition::deactivateUIAndInput()
{
  EngineLua.setUIVisible(false);
  EngineLua.stopPlayerAnimations();
  //EnginePhysics.isPaused = true; if desactiva Physics, AI cant move

  if (!canMove) {
    //Halt player movement/input
    //if (EngineInput.getActive()) EngineInput.toggleActive();

    EngineLua.blockPlayer();
    EngineLua.setInputEnabled(false);
  }

  if (can_skip) Engine.getUI().activateWidget("skip_cinematic");
}

void TCompCameraTransition::objEntered(const TMsgEntityTriggerEnter& msg)
{
  CEntity *ent = msg.h_entity;
  std::string name = ent->getName();

  if (name == "Player") {
    if (current_id == 0) {
      //Check if at least one of the camera entities exist, if not then deactivate transition
      CHandle hCamera = getEntityByName(transitions[current_id].camName);
      if (hCamera.isValid()) {
        start();
      }
    }

  }
}

void TCompCameraTransition::onAlarmRecieve(const TMsgAlarmClock& msg)
{
  if (msg.id == 1) {
    CamTransition c = transitions[current_id];
    CEntity *entityUI = (CEntity *)getEntityByName(c.camName);
    if (entityUI != nullptr) {
      TCompCinematicCamera* cam = entityUI->get<TCompCinematicCamera>();
      if (cam != nullptr) cam->enableTransition();
    }
    current_id++;
  }
  if (msg.id == 2) {
    makeTransition();
  }
  if (msg.id == 3) {
    activateUIAndInput();
    CHandle(this).getOwner().destroy();
  }
}

TCompCameraTransition::~TCompCameraTransition()
{
  //Destroy all camera entities after transition is done
  CHandle cam;
  for (auto it = transitions.begin(); it != transitions.end(); ++it) {
    cam = getEntityByName(it->camName);
    cam.destroy();
  }
}

void TCompCameraTransition::start()
{
  CEntity *player = getEntityByName("Player");
  if (player != nullptr)
  {
    TCompPlayerMagic *m = player->get < TCompPlayerMagic >();
    if (m->pullingObj) m->ReleasePullObj();
  }
  deactivateUIAndInput();
  makeTransition();
}

void TCompCameraTransition::startInMenu()
{
  loop = true;
  makeTransition();
}

void TCompCameraTransition::end()
{
  loop = false;
  static Interpolator::TQuintOutInterpolator finalInt;
  CHandle hCamera = getEntityByName("CameraShaker");
  Engine.getCameraMixer().blendCamera(hCamera, 0, &finalInt);
}
