#include "mcv_platform.h"
#include "comp_camera_debug.h"
#include "components/common/comp_transform.h"
#include "components/controllers/comp_char_controller.h"
#include "utils/utils.h"
#include "engine.h"
#include "input/input.h"
#include "input/module_input.h"

DECL_OBJ_MANAGER("camera_debug", TCompCameraDebug);

void TCompCameraDebug::debugInMenu() {
  ImGui::DragFloat("Max Speed", &_speed, 0.1f, 1.f, 100.f);
  ImGui::DragFloat("Sensitivity", &_sensitivity, 0.001f, 0.001f, 0.1f);
  ImGui::LabelText("Curr Speed", "%f", _ispeed);
}

void TCompCameraDebug::load(const json& j, TEntityParseContext& ctx) {
  _speed = j.value("speed", _speed);
  _sensitivity = j.value("sensitivity", _sensitivity);
  _enabled = j.value("enabled", _enabled);

  if (j.count("key")) {
	_key_toggle_enabled = j.value("key", _key_toggle_enabled);
  }
}

void TCompCameraDebug::update(float scaled_dt)
{
  if (EngineInput["escape"].justPressed()) {
      _enabled = !_enabled;
  }

  if (!_enabled)
    return;

  TCompTransform* c_transform = get<TCompTransform>();
  if (!c_transform)
    return;
  
  // if the game is paused, we still want full camera speed 
  float dt = Time.delta_unscaled;

  VEC3 pos = c_transform->getPosition();
  VEC3 front = c_transform->getFront();
  VEC3 left = c_transform->getLeft();
  VEC3 up = VEC3::UnitY;

  _ispeed = VEC3(0,0,0);

  // Movement
  if(isPressed('U')) _ispeed.z = 1.f; //Front
  if (isPressed('J')) _ispeed.z = -1.f; //Back
  if (isPressed('H')) _ispeed.x = 1.f; //Left
  if (isPressed('K')) _ispeed.x = -1.f; //Right
  if (isPressed('I')) _ispeed.y = 1.f; //Up
  if (isPressed('Y')) _ispeed.y = -1.f; //Down

  // Speed regulation
  float velAdjust = 5.0f;
  if (EngineInput["Push"].isPressed()) 
    _speed += velAdjust * dt;
  if (EngineInput["Pull"].isPressed()) 
    _speed -= velAdjust * dt;
  _speed = clampFloat(_speed, 0.1f, 100.0f);
  float deltaSpeed = _speed * dt;
  if (isPressed('L')) deltaSpeed *= 7.f;

  // Amount in each direction
  VEC3 off;
  off += front * _ispeed.z * deltaSpeed;
  off += left * _ispeed.x * deltaSpeed;
  off += up * _ispeed.y * deltaSpeed;

  // Get Raw Mouse Motion
  auto& mouse = EngineInput.mouse();
  VEC2 motion = mouse.getDelta();

  // rotation
  float yaw, pitch;
  c_transform->getAngles(&yaw, &pitch);
  yaw += -motion.x * _sensitivity * 0.2f * dt;
  pitch += motion.y * _sensitivity * 0.2f * dt;
  if (pitch < -_maxPitch)
	  pitch = -_maxPitch;
  else if (pitch > _maxPitch)
	  pitch = _maxPitch;

  c_transform->setAngles(yaw, pitch, 0.0f);

  // final values
  VEC3 newPos = pos + off;
  VEC3 newFront = yawPitchToVector(yaw, pitch);

  c_transform->lookAt(newPos, newPos + newFront);
}