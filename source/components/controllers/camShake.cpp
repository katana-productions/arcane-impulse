#include "mcv_platform.h"
#include "camShake.h"
#include "components/common/comp_camera.h"
#include "entity/entity_parser.h"
#include "engine.h"

DECL_OBJ_MANAGER("camera_shaker", TCompCamShake);

CHandle TCompCamShake::shakerHandle;

void TCompCamShake::registerMsgs() {
	DECL_MSG(TCompCamShake, TMsgAlarmClock, onAlarmRecieve);
}

void TCompCamShake::onAlarmRecieve(const TMsgAlarmClock& msg) {
	if (msg.id == -1)
	{
		nShakes--;
		if (nShakes == 0)
		{
			shakeIntensity = 0.0f;
			targetPos = VEC3(0, 0, 0);
		}
	}
}

void TCompCamShake::load(const json & j, TEntityParseContext & ctx)
{
	xInfluence = j.value("xInfluence", xInfluence);
	yInfluence = j.value("yInfluence", yInfluence);
	zInfluence = j.value("zInfluence", zInfluence);
	smoothing = j.value("smoothing", smoothing);

	shakerHandle = ctx.current_entity;
}

void TCompCamShake::update(float dt)
{
  if (!enabled) return;
	TCompHierarchy* hierarchy = get<TCompHierarchy>();

	if (nShakes > 0 && shakeIntensity > 0 && hierarchy->manhattanDistance(targetPos) < 0.05f)
	{
		if (hierarchy->manhattanDistance(VEC3(0,0,0)) < 0.05f)
		{
			targetPos = VEC3(randomRange(-1.0f, 1.0f) * shakeIntensity * xInfluence,
				(randomRange(-1.0f, 1.0f) * shakeIntensity * yInfluence) + (1.0f * shakeIntensity * yInfluence),
				randomRange(-1.0f, 1.0f) * shakeIntensity * zInfluence);
		}
		else targetPos = VEC3(0,0,0);
	}
	
	shakeOffset = VEC3::Lerp(shakeOffset, targetPos, OurClamp(smoothing * dt * roughnessMult, 0.0f, 1.0f));
	hierarchy->setPosition(shakeOffset + movementOffset);

	//if (isPressed('V')) //TODO: REMOVE THIS WHEN HAPPY WITH SHAKES
	//{
	//	EngineBasics.ShakeCamera(0.5f, 0.1f);
	//}
}

void TCompCamShake::startShake(float intensity, float duration, float new_roughnessMult)
{
	nShakes++;
	shakeIntensity = intensity;
	roughnessMult = new_roughnessMult;
 	CHandle myHandle = CHandle(this);
	EngineAlarms.AddAlarm(duration, -1, myHandle);
}

void TCompCamShake::debugInMenu()
{
	ImGui::DragFloat("xInfluence", &xInfluence, 0.01f, 0.0f, 1.0f);
	ImGui::DragFloat("yInfluence", &yInfluence, 0.01f, 0.0f, 1.0f);
	ImGui::DragFloat("zInfluence", &zInfluence, 0.01f, 0.0f, 1.0f);
	ImGui::DragFloat("smoothing", &smoothing, 0.1f, 0.0f, 50.0f);
}