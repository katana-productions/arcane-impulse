#include "mcv_platform.h"
#include "comp_platform_controller.h"
#include "components/controllers/comp_char_controller.h"
#include "physics/module_physics.h"

DECL_OBJ_MANAGER("platform_controller", TCompPlatfController);

void TCompPlatfController::registerMsgs() {
  DECL_MSG(TCompPlatfController, TMsgEntityCreated, onEntityCreated);
  DECL_MSG(TCompPlatfController, TMsgSceneCreated, onSceneCreated);
  DECL_MSG(TCompPlatfController, TMsgPuzzleTriggerEntered, keyEntered);
}

void TCompPlatfController::onEntityCreated(const TMsgEntityCreated& msg)
{
  myTrans = get<TCompTransform>();
  myCol = get<TCompCollider>();

  // Move to initial position;
  currentDist = curve->ratio2Dist(currentRatio);
  movePlatform();
}

void TCompPlatfController::onSceneCreated(const TMsgSceneCreated& msg)
{
	//if (myTrans == nullptr && myCol == nullptr) {
		CEntity* ent = msg.h_entity;
		myTrans = ent->get<TCompTransform>();
		myCol = ent->get<TCompCollider>();

		// Move to initial position;
		currentDist = curve->ratio2Dist(currentRatio);
		movePlatform();
	//}
	
}

void TCompPlatfController::keyEntered(const TMsgPuzzleTriggerEntered& msg)
{
  enabled = true;
}

void TCompPlatfController::update(float dt)
{
  if (!enabled) return;
  if (dt > 0.1f) dt = 0.1f;

  if (isStopped and !dontStop)
  {
    current_stop_time += dt;
    if (current_stop_time >= stopping_time)
    {
      isStopped = false;
      current_stop_time = 0.0f;
      if (stopOnce) dontStop = true;
    }
  }
  else
  {
    prevDirection = direction;
    if (currentDist >= (curve->getTotalDistance() - 0.1f) and mode == PlatformMode::Circuit) currentDist = 0.0f;
    else if (currentDist >= (curve->getTotalDistance() - 0.1f) and mode == PlatformMode::PingPong) direction = -1;
    else if (currentDist <= 0.1f and mode == PlatformMode::PingPong) direction = 1;

    if (mode == PlatformMode::Circuit and currentDist == 0.0f and dontStop == true)
    {
      stopOnce = false;
      dontStop = false;
    }
    if (mode == PlatformMode::PingPong and prevDirection != direction and dontStop == true)
    {
      stopOnce = false;
      dontStop = false;
    }

    currentDist += dt * speed * direction;
    currentRatio = curve->dist2Ratio(currentDist);

    if (!stopping_ratios.empty() and !dontStop)
    {
      if (mode == PlatformMode::Circuit)
      {
        if (currentRatio >= stopping_ratios[current_stop_ratio])
        {
          isStopped = true;
          ++current_stop_ratio;
          if (current_stop_ratio >= stopping_ratios.size())
          {
            current_stop_ratio = 0;
            stopOnce = true;
          }
        }
      }
      else
      {
        if (direction == 1)
        {
          if (currentRatio >= stopping_ratios[current_stop_ratio])
          {
            isStopped = true;
            ++current_stop_ratio;
            if (current_stop_ratio >= stopping_ratios.size())
            {
              --current_stop_ratio;
              stopOnce = true;
            }
          }
        }
        else
        {
          if (currentRatio <= stopping_ratios[current_stop_ratio])
          {
            isStopped = true;
            --current_stop_ratio;
            if (current_stop_ratio < 0)
            {
              ++current_stop_ratio;
              stopOnce = true;
            }
          }
        }
      }
    }
  }

  // Move
  movePlatform();

  time += dt;
  if (time >= 2 * M_PI) time = 0.0f;
}

void TCompPlatfController::movePlatform()
{
  VEC3 p = curve->evaluateAtDistance(currentDist);
  physx::PxTransform point(VEC3_TO_PXVEC3(p) + VEC3_TO_PXVEC3(VEC3(0, amplitude * sin(time * frequency), 0)));
  physx::PxRigidDynamic *rb = myCol->actor->is<physx::PxRigidDynamic>();
  rb->setKinematicTarget(point);
}

void TCompPlatfController::debugInMenu() {
  ImGui::DragFloat("Max Speed", &speed, 0.1f, 0.f, 100.f);
  ImGui::LabelText("Curr Speed", "%f", speed);
  ImGui::LabelText("Curr Dist", "%f", currentDist);
  ImGui::LabelText("Curr Ratio", "%f", currentRatio);
}

void TCompPlatfController::load(const json& j, TEntityParseContext& ctx)
{
  enabled = j.value("enabled", true);
  const CCurve *c = Resources.get(j.value("curve", ""))->as<CCurve>();
  curve = (CCurve *)c;
  std::string m = j.value("mode", "pingpong");
  if (m == "pingpong") mode = PlatformMode::PingPong;
  else mode = PlatformMode::Circuit;
  currentRatio = j.value("initial_ratio", currentRatio);
  speed = j.value("speed", speed);

  stopping_time = j.value("stopping_time", 0.0f);
  amplitude = j.value("amplitude", amplitude);
  frequency = j.value("frequency", frequency);

  json s = j["stopping_knots"];
  for (json::iterator it = s.begin(); it != s.end(); ++it)
  {
    int k = it.value().get<int>();
    assert(k >= 0 and k < curve->getRealKnots().size());
    
    bool stop = false;
    float step = 0.001f;
    float f;
    for (f = 0.0f; !stop and f <= 1.0f; f += step)
    {
      VEC3 p = curve->evaluate(f);
      VEC3 knot = curve->getRealKnots()[k];
      float d = VEC3::Distance(p, knot);
      if (d <= 1.0f) stop = true;
    }
    stopping_ratios.push_back(f);
  }

  s = j["stopping_ratios"];
  for (json::iterator it = s.begin(); it != s.end(); ++it)
  {
    float k = it.value().get<float>();
    assert(k >= 0.0 and k <= 1.0f);
    stopping_ratios.push_back(k);
  }
  std::sort(stopping_ratios.begin(), stopping_ratios.end());
}

void TCompPlatfController::renderDebug()
{
  curve->renderDebug();
  for (int i = 0; i < curve->getKnots().size(); ++i)
  {
    drawWiredSphere(curve->getKnots()[i], 2.0f, VEC4(0, 1, 1, 1));
  }
}
