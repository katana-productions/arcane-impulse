#pragma once

#include "components/common/comp_base.h"
#include "components/common/comp_transform.h"
#include "components/common/comp_collider.h"
#include "entity/entity.h"

class TCompTimedPlatfController : public TCompBase
{
private:
  bool enabled = false;
  bool oneShot = false;
  bool affectedByPlayer = false;
  bool playerIsOn = false;
  bool disappeared = false;
	bool isTrigger = false;
  float ttl;
  float time_to_regenerate = 3.0f;
  float time = 0.0f;
  std::string triggerSwitch;
  std::string myTriggerName;
  CHandle h_trigger;
  TCompTransform triggerOrigTrans;
  TCompTransform origTrans;
  TCompTransform *myTrans;
  TCompCollider *myCol;

  void onEntityCreated(const TMsgEntityCreated& msg);
  void onSceneCreated(const TMsgSceneCreated& msg);
  void keyEntered(const TMsgPuzzleTriggerEntered& msg);
  void onDamageMsg(const TMsgDamage& msg);

public:
  DECL_SIBLING_ACCESS();
  static void registerMsgs();
  void update(float dt);
  void debugInMenu();
  void load(const json& j, TEntityParseContext& ctx);
  void renderDebug();
  void destroy();
};
