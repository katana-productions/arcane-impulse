#pragma once

#include "comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "components/shaders/shader_controller.h"

class CMesh;
class CMaterial;

struct TCompRender : public TCompBase {

  struct MeshPart {
    const CMesh*      mesh = nullptr;
    const CMaterial*  material = nullptr;
    int               mesh_group = 0;
    int               mesh_instances_group = 0;
    bool              instanciable = false;
    int               key = 0;
    bool              is_visible = true;
    int               state = 0;          // user defined...
  };

  AABB                    aabb;
  bool                    is_visible = true;
  CHandle                 h_transform;
  VEC4                    color;
  float                   glowAmount;
  float                   rimFactor;
  float                   specularFactor;
  std::vector< MeshPart > parts;
  int                     curr_state = 0;
  ShaderController        shader_controller = ShaderController::Nothing;

  std::string			  new_particle_material_name;
  std::string			  new_particle_mesh_name;

  void readMesh(const json& j);
  void onDefineLocalAABB(const TMsgDefineLocalAABB& msg);
  void renderDebug();

public:
  DECL_SIBLING_ACCESS();
  void onEntityCreated(const TMsgEntityCreated& msg);

  ~TCompRender();
  void load(const json& j, TEntityParseContext& ctx);
  void debugInMenu();
  int getCurrentState() { return curr_state; }
  void showMeshesWithState(int new_state);

  void updateRenderManager();

  static void registerMsgs();
  void setShaderController(ShaderController s) { shader_controller = s; };
  ShaderController getShaderController() { return shader_controller; };

  void setVisible(bool isVisible) { is_visible = isVisible; }
};
