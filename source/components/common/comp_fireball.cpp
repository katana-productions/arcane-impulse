#include "mcv_platform.h"
#include "comp_fireball.h"
#include "components/common/comp_transform.h"
#include "components/common/comp_collider.h"
#include "components/common/comp_particle_move.h"
#include "engine.h"

DECL_OBJ_MANAGER("fireball_controller", TCompFireballController);

using namespace physx;

void TCompFireballController::debugInMenu() {
  ImGui::DragFloat("Speed", &speed, 0.1f, 0.f, 10.f);
  ImGui::DragFloat("Damage", &damage, 1.f, 0.f, 100.f);
}

void TCompFireballController::load(const json& j, TEntityParseContext& ctx) {
  speed = j.value("speed", speed);
  damage = j.value("damage", damage);
  minKnockBack = j.value("minknockback", minKnockBack);
  timeToExploit = j.value("timeToExploit", timeToExploit);
  correctionLimitDist = j.value("correctionLimitDist", correctionLimitDist);
  knockbackForce = j.value("knockbackForce", knockbackForce);
  directionLerpFactor = j.value("directionLerpFactor", directionLerpFactor);
  senAmplitude = j.value("senAmplitude", senAmplitude);
  senFrequency = j.value("senFrequency", senFrequency);  
  limitAngle = j.value("limitAngle", limitAngle);
  fireballEditor = j.value("fireballEditor", fireballEditor);
  fireball_explosion_particles = j.value("fireball_explosion_particles", fireball_explosion_particles);
}

void TCompFireballController::registerMsgs() {
  DECL_MSG(TCompFireballController, TMsgIntObjState, changeState);
  DECL_MSG(TCompFireballController, TMsgAssignBulletOwner, onBulletInfoMsg);
  DECL_MSG(TCompFireballController, TMsgAlarmClock, onAlarmRecieve);
  DECL_MSG(TCompFireballController, TMsgOnContact, onContactMsg);
  DECL_MSG(TCompFireballController, TMsgDestroy, onDestroy);
  DECL_MSG(TCompFireballController, TMsgEntityCreated, onEntityCreated);

}

void TCompFireballController::onEntityCreated(const TMsgEntityCreated& msg) {
	TCompCollider* c_collider = get<TCompCollider>();
	PxRigidDynamic* rb = c_collider->actor->is<PxRigidDynamic>();
	rb->setActorFlag(PxActorFlag::eDISABLE_GRAVITY, true);
	EngineAlarms.AddAlarm(timeSecureShoot, 102, CHandle(this));
}

void TCompFireballController::changeState(const TMsgIntObjState& msg)
{
  TCompCollider *col = get<TCompCollider>();
  PxRigidDynamic *rb = col->actor->is<PxRigidDynamic>();
  if (msg.state == ObjState::Pulled) {
	  rb->setActorFlag(PxActorFlag::eDISABLE_GRAVITY, false);
	  correctionDisabled = true;
  }
  if (msg.state == ObjState::Pushed) {
	  rb->setActorFlag(PxActorFlag::eDISABLE_GRAVITY, true);
	  correctionDisabled = true;
  }
}

void TCompFireballController::onBulletInfoMsg(const TMsgAssignBulletOwner& msg) {
  TCompTransform* c_trans = get<TCompTransform>();
  player = getEntityByName("Player");
  VEC3 player_pos = EngineBasics.GetPlayerPos(player);
  VEC3 desiredToPlayer = player_pos + player_offset - c_trans->getPosition();
  desiredToPlayer.Normalize();

  h_sender = msg.h_owner;
  origPos = msg.source;
  origDir = desiredToPlayer;
  currentDir = desiredToPlayer;

  TCompCollider* c_collider = get<TCompCollider>();
  if (!c_collider)
    return;

  TCompSystemParticleWorldMove* c_particle_move = get< TCompSystemParticleWorldMove>();
  if (!c_particle_move) return;
  c_particle_move->ActivateParticles();
  particleActivated = true;

  PxRigidDynamic* rb = c_collider->actor->is<PxRigidDynamic>();
  VEC3 push = currentDir * speed;
  rb->setLinearVelocity(VEC3_TO_PXVEC3(push));
	
  EngineAlarms.AddAlarm(timeToExploit, 101, CHandle(this));
  activate = true;
}

void TCompFireballController::onAlarmRecieve(const TMsgAlarmClock& msg) {
	if (msg.id == 101) {
		destroy = true;
		destroyMe();
	}
	else if (msg.id == 102) {
		ShootErrorCase();
	}
}

void TCompFireballController::onContactMsg(const TMsgOnContact& msg) {
  TMsgDamage damage_msg;
  damage_msg.h_sender = h_sender;
  damage_msg.h_bullet = CHandle(this).getOwner();
  damage_msg.damage = damage;

  TCompCollider* c_collider = msg.source;


  TCompTransform *myTrans = get<TCompTransform>();
  CEntity* objective = CHandle(c_collider).getOwner();
  if (objective)
  {
    TCompTransform *objTrans = objective->get<TCompTransform>();
    VEC3 dir = objTrans->getPosition() - myTrans->getPosition();
    dir.Normalize();
    damage_msg.force = dir * knockbackForce;
    objective->sendMsg(damage_msg);
  }

  destroy = true;
}

void TCompFireballController::onDestroy(const TMsgDestroy& msg) {
	destroyMe();
}


void TCompFireballController::update(float delta) {
	if (destroy)
		destroyMe();

	if (fireballEditor) {
		TCompTransform* c_trans = get<TCompTransform>();
		if (!c_trans) return;
		c_trans->setScale(1.0f);


		editorTimer += delta;
		if (editorTimer > 3.f) {
			editorDir *= -1;
			editorTimer = 0.0f;
		}

		TCompCollider* c_collider = get<TCompCollider>();
		if (!c_collider)
			return;

		PxRigidDynamic* rb = c_collider->actor->is<PxRigidDynamic>();
		VEC3 push = VEC3(editorDir,0, 0) * speed;
		rb->setLinearVelocity(VEC3_TO_PXVEC3(push));
		return;
	}

	if (!activate) {
		TCompTransform* c_trans = get<TCompTransform>();
		if (!c_trans) return;
		scaleFireball = clampFloat(lerpFloat(scaleFireball, 1.0f, delta * 2.5), 0.f, 1.f);
		c_trans->setScale(scaleFireball);
		return;
	}


	globalTime += delta;

	DirectionCorrection(delta);
}

void TCompFireballController::DirectionCorrection(float dt) {
	if (correctionDisabled) 
		return;
	
	TCompTransform* c_trans = get<TCompTransform>();
	TCompCollider* c_collider = get<TCompCollider>();
	if (!c_collider)
		return;
	PxRigidDynamic* rb = c_collider->actor->is<PxRigidDynamic>();

	VEC3 player_pos = EngineBasics.GetPlayerPos(player);
	VEC3 senModifier = VEC3(0, senAmplitude * sin(2 * PI * globalTime * senFrequency), 0);

	VEC3 desiredToPlayer = player_pos + player_offset - c_trans->getPosition();
	desiredToPlayer.Normalize();


	float angle = rad2deg(acos(origDir.Dot(desiredToPlayer) / (magnitude(origDir) * magnitude(desiredToPlayer))));
	//if less distance to player, correct direction
	if (c_trans->accurateDistance(origPos) < VEC3::Distance(origPos, player_pos) - correctionLimitDist && angle < limitAngle) {
		currentDir = VEC3::Lerp(currentDir, desiredToPlayer, dt * directionLerpFactor);
	}
	currentDir.Normalize();

	//apply sen modifier
	VEC3 finalDir = currentDir + senModifier;
	finalDir.Normalize();

	VEC3 push = finalDir * speed;
	rb->setLinearVelocity(VEC3_TO_PXVEC3(push));
}

void TCompFireballController::ShootErrorCase()
{
	if (activate) return;

	if (!particleActivated) {
		TCompSystemParticleWorldMove* c_particle_move = get< TCompSystemParticleWorldMove>();
		if (c_particle_move) {
			c_particle_move->ActivateParticles();
			particleActivated = true;
		}
	}

	TCompTransform* c_trans = get<TCompTransform>();
	VEC3 orig_pos = c_trans->getPosition();
	player = getEntityByName("Player");
	VEC3 player_pos = EngineBasics.GetPlayerPos(player);
	VEC3 desiredToPlayer = player_pos + player_offset - orig_pos;
	desiredToPlayer.Normalize();

	h_sender = CHandle(this).getOwner();
	origPos = orig_pos;
	origDir = desiredToPlayer;
	currentDir = desiredToPlayer;

	TCompCollider* c_collider = get<TCompCollider>();
	if (!c_collider)
		return;

	PxRigidDynamic* rb = c_collider->actor->is<PxRigidDynamic>();
	VEC3 push = currentDir * speed;
	rb->setLinearVelocity(VEC3_TO_PXVEC3(push));

	EngineAlarms.AddAlarm(timeToExploit, 101, CHandle(this));
	activate = true;
}

void TCompFireballController::destroyMe() {
	if (!destroy || fireballEditor) return;
	destroy = false;
	TCompTransform* c_trans = get<TCompTransform>();
	float yaw, pitch, roll;
	c_trans->getAngles(&yaw, &pitch, &roll);

	if (c_trans)
	{
		c_trans->setRotation(VEC4(0, 0, 0, 1));
		EngineParticles.CreateOneShootSystemParticles(fireball_explosion_particles.c_str(), c_trans);
		EnginePhysics.AddExplosion(c_trans->getPosition(), 7.5f, 50.0f, damage, 0.4f);
		EngineAudio.playEvent3D("event:/Props/Explosion", c_trans, 2);
		//CEntity* light = EngineBasics.InstantiateAtParent("data/prefabs/lights/light_point_explosion.json", c_trans->getPosition(), yaw, pitch, roll, CHandle(this));
		//TMsgDestroy msgDestroy;
		//msgDestroy.h_object = CHandle(light);
		//light->sendMsg(msgDestroy);
		CHandle(this).getOwner().destroy();
	}
}