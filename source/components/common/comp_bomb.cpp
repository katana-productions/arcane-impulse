#include "mcv_platform.h"
#include "comp_bomb.h"
#include "components/controllers/comp_char_controller.h"
#include "components/common/comp_transform.h"
#include "components/common/comp_collider.h"
#include "engine.h"

DECL_OBJ_MANAGER("bomb_controller", TCompBombController);

using namespace physx;

void TCompBombController::debugInMenu()
{
  ImGui::DragFloat("Damage", &damage, 1.f, 0.f, 100.f);
  ImGui::DragFloat("Knockback", &knockback, 1.f, 0.f, 100.f);
  ImGui::DragFloat("Min Knockback", &min_knockback, 1.f, 0.f, 100.f);
  ImGui::DragFloat("Explosion Radius", &explosion_radius, 1.f, 0.f, 100.f);
  ImGui::DragInt("Allowed bounces", &allowed_bounces, 1, 10);
  ImGui::DragFloat("Allowed time_to_explode", &time_to_explode, 0.1f, 0.0f, 10.0f);
}

void TCompBombController::load(const json& j, TEntityParseContext& ctx)
{
  damage = j.value("damage", damage);
  knockback = j.value("knockback", knockback);
  min_knockback = j.value("min_knockback", min_knockback);
  explosion_radius = j.value("explosion_radius", explosion_radius);
  allowed_bounces = j.value("allowed_bounces", allowed_bounces);
  time_to_explode = j.value("time_to_explode", time_to_explode);
}

void TCompBombController::registerMsgs()
{
  DECL_MSG(TCompBombController, TMsgOnContact, onContactMsg);
  DECL_MSG(TCompBombController, TMsgDamage, onDamageMsg);
}

void TCompBombController::onContactMsg(const TMsgOnContact& msg)
{
  CHandle h_other = msg.source;
  if (h_other.isValid())
  {
    CEntity *other = h_other.getOwner();
    if (other != nullptr)
    {
      TCompCharController *charController = other->get<TCompCharController>();
      if (charController != nullptr)
        explode();
    }
  }

  ++bounces;

  if (bounces >= allowed_bounces)
    explode();
}

void TCompBombController::onDamageMsg(const TMsgDamage& msg)
{
  explode();
}

void TCompBombController::update(float dt)
{
  time += dt;
  if (time_to_explode != 0.0f and time >= time_to_explode)
    explode();
}

void TCompBombController::explode()
{
  if (!exploding)
  {
    TCompTransform* myTrans = get<TCompTransform>();
    VEC3 pos = myTrans->getPosition();
    CHandle(this).getOwner().destroy();
    exploding = true;
    // TODO: substitute for better explosion
    EngineBasics.CreateExplosionMeshAt(explosion_radius, pos);
    EnginePhysics.AddExplosion(pos, explosion_radius, knockback, damage, min_knockback);
  }
}


