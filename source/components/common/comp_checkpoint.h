#pragma once
#include "components/common/comp_base.h"

class TCompCheckpoint : public TCompBase {
	DECL_SIBLING_ACCESS();

private:
	int id = 0;
	bool pullWallUnlocked = true;
	bool pullObjUnlocked = true;
	bool pushWallUnlocked = true;
	bool pushObjUnlocked = true;
	bool gloves = false;
	bool oneShoot = false;

	std::string scene = "Initial";

	void onTriggerEnter(const TMsgEntityTriggerEnter& msg);

public:
	void load(const json& j, TEntityParseContext& ctx);
	int getId() { return id; }

	static void registerMsgs();
};