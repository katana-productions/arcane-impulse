#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

class TCompFireballController : public TCompBase {
	bool correctionDisabled = false;
	bool fireballEditor = false;
	VEC3 currentDir = VEC3(0, 0, 0);
	VEC3 origDir = VEC3(0, 0, 0);
	float globalTime;
	float senAmplitude = 1;
	float senFrequency = 1;
	float limitAngle = 1;

	float speed = 10.0f;
	float damage = 1;
	float minKnockBack = 0.25f;

	float timeToExploit = 10.0f;
	float correctionLimitDist = 5.0f;
	float knockbackForce = 20.0f;
	float directionLerpFactor = 1;

	std::string fireball_explosion_particles;

	VEC3 origPos;
	VEC3  front;
	CHandle h_sender;

	CHandle player;
	VEC3 player_offset = VEC3(0, 1.5f, 0);
	bool destroy = false;
	bool activate = false;
	float scaleFireball = 0.1f;

	float timeSecureShoot = 2.0f;

	float editorTimer = 0.0;

	bool particleActivated = false;
	int editorDir = 1;

	DECL_SIBLING_ACCESS();
    void changeState(const TMsgIntObjState& msg);
	void onBulletInfoMsg(const TMsgAssignBulletOwner& msg);
	void onAlarmRecieve(const TMsgAlarmClock& msg);
	void onContactMsg(const TMsgOnContact& msg);
	void onDestroy(const TMsgDestroy& msg);
	void onEntityCreated(const TMsgEntityCreated& msg);
	void destroyMe();
	void ShootErrorCase();
	void DirectionCorrection(float dt);

public:
	void update(float dt);
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	static void registerMsgs();

	bool GetIsActivate() { return activate; }
	void ExplodeFireball() { destroy = true; }
};