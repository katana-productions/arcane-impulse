#include "mcv_platform.h"
#include "comp_transform.h"
#include "entity/entity_parser.h"
#include <fstream>
#include <iomanip>

DECL_OBJ_MANAGER("transform", TCompTransform);

void TCompTransform::registerMsgs() {
	DECL_MSG(TCompTransform, TMsgEntityCreated, onEntityCreated);
}

void TCompTransform::onEntityCreated(const TMsgEntityCreated& msg) {
	TCompName *n = get<TCompName>();
	entity_name = n->getName();
}

void TCompTransform::debugInMenu() {
  CTransform::renderInMenu();
  if (ImGui::Button("Save To JSON"))
	  saveToJSON();
}

void TCompTransform::load(const json& j, TEntityParseContext& ctx) {
	if (ctx.parsing_prefab) {
		json_filename = ctx.parent->filename;
	}
	else {
		if (json_filename == "") json_filename = ctx.filename;
	}
  CTransform::load(j);
  set(ctx.root_transform.combineWith(*this));
	if (j.count("offset")) renderOffset = loadVEC3(j, "offset");
}

void TCompTransform::set(const CTransform& new_tmx) {
  *(CTransform*)this = new_tmx;
}

void TCompTransform::renderDebug() {
  drawAxis( asMatrix() );
}

void TCompTransform::saveToJSON() {
	auto jData = loadJson(json_filename);
	auto& jEntry = jData;

	auto& EntireEntities = jEntry;

	for (int i = 0; i < EntireEntities.size(); i++) {
		auto& jEntityToModify = EntireEntities[i];
		auto& jEntity = jEntityToModify["entity"];
		auto& jName = jEntity["name"];
		if (jName.size() >= 2) {
			if (jName["entityName"] == entity_name) {
				saveEntityDataJson(jEntity);
				break;
			}
		}
		else if (jName == entity_name) {
			saveEntityDataJson(jEntity);
			break;
		}
	}
	std::ofstream outputFileStream(json_filename);
	outputFileStream << std::setw(4) << jData << std::endl;
}

void TCompTransform::saveEntityDataJson(json& jEntity) {
	VEC3 pos = getPosition();
	QUAT rot = getRotation();
	
	jEntity["transform"]["pos"] = std::to_string(pos.x) + " " + std::to_string(pos.y) + " " + std::to_string(pos.z);
	jEntity["transform"]["rotation"] = std::to_string(rot.x) + " " + std::to_string(rot.y) + " " + std::to_string(rot.z) + " " + std::to_string(rot.w);
}
