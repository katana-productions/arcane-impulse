#pragma once

#include "comp_base.h"
 
class TCompTransform : public CTransform, public TCompBase {
public:
	std::string		json_filename = "";
	std::string		entity_name = "";

	void saveToJSON();
	void saveEntityDataJson(json& jEntity);
	DECL_SIBLING_ACCESS();
	static void registerMsgs();
	void onEntityCreated(const TMsgEntityCreated& msg);

	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void renderDebug();
	void set(const CTransform& new_tmx);
};