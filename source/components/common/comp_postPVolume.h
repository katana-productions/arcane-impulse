#pragma once
#include "components/common/comp_base.h"

class TCompPostPVolume : public TCompBase {
	DECL_SIBLING_ACCESS();

private:
	float transitionTime;

	//Global
	float globalAmbient;
	float globalExposure;

	//Vignete
	float  vigExtend;
	float  vigIntensity;

	//Fog
	float fogDepthFade;
	float maxFog;
	float fogContrast;
	float4 fogColor;

	//Color Grading
	std::string lut_name;

	void onTriggerEnter(const TMsgEntityTriggerEnter& msg);

public:
	void load(const json& j, TEntityParseContext& ctx);
	static void registerMsgs();
};