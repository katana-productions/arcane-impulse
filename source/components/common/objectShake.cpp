#include "mcv_platform.h"
#include "objectShake.h"
#include "entity/entity_parser.h"
#include "engine.h"

using namespace physx;

DECL_OBJ_MANAGER("object_shaker", TCompObjectShake);

void TCompObjectShake::registerMsgs() {
	DECL_MSG(TCompObjectShake, TMsgEntityCreated, init);
	DECL_MSG(TCompObjectShake, TMsgAlarmClock, onAlarmRecieve);
}

void TCompObjectShake::init(const TMsgEntityCreated& msg) {
	myTransform = get<TCompTransform>();
	myCol = get<TCompCollider>();
}

void TCompObjectShake::onAlarmRecieve(const TMsgAlarmClock& msg) {
	if (msg.id == -99)
	{
		nShakes--;
		if (nShakes == 0)
		{
			shakeIntensity = 0.0f;
			targetOffset = VEC3(0,0,0);
			hasShaked = true;
			EngineLua.runCode("destroyParticleTutorialPull()");
		}
	}
}

void TCompObjectShake::load(const json & j, TEntityParseContext & ctx)
{
	smoothing = j.value("smoothing", smoothing);
}

void TCompObjectShake::update(float dt)
{
	if (hasShaked) return;
	if (!hasShaked && nShakes > 0 && shakeIntensity > 0)
	{
		if (myTransform->manhattanDistance(myTransform->getPosition() + myOffset) < 0.03f)
		{
			targetOffset = VEC3(negPosUnitRandom() * shakeIntensity,
				(negPosUnitRandom() * shakeIntensity),
				negPosUnitRandom() * shakeIntensity);
		}
		else targetOffset = VEC3(0,0,0);
	}
	
	myOffset = VEC3::Lerp(myOffset, targetOffset, OurClamp(smoothing * dt * roughnessMult, 0.0f, 1.0f));
	myTransform->setPosition(myTransform->getPosition() + myOffset);

	float yaw, pitch, roll;
	myTransform->getAngles(&yaw, &pitch, &roll);
	PxTransform pxTrans(VEC3_TO_PXVEC3(myTransform->getPosition()), QUAT_TO_PXQUAT(QUAT::CreateFromYawPitchRoll(yaw, pitch, roll)));
	myCol->actor->setGlobalPose(pxTrans);
}

void TCompObjectShake::startShake(float intensity, float duration, float new_roughnessMult)
{
	if (nShakes > 0) hasShaked = false;
	nShakes++;
	shakeIntensity = intensity;
	roughnessMult = new_roughnessMult;
 	CHandle myHandle = CHandle(this);
	EngineAlarms.AddAlarm(duration, -99, myHandle);
}

void TCompObjectShake::debugInMenu()
{
	ImGui::DragFloat("smoothing", &smoothing, 0.1f, 0.0f, 50.0f);
}