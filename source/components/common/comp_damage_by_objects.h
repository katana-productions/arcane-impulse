#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

class TCompDamageByObjects : public TCompBase
{
	DECL_SIBLING_ACCESS();
	void onContactMessage(const TMsgOnContact& msg);

public:
	void debugInMenu();
	void load(const json& j, TEntityParseContext& ctx);

	static void registerMsgs();

	float minimumVelocityToDamage = 20;
	float damageAttenuation = 3;
};

