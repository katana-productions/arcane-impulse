#include "mcv_platform.h"
#include "engine.h"

class TCompMsgLogger : public TCompBase {

  std::vector< std::string > msgs;

  void log(const char* format, ...) {
    va_list argptr;
    va_start(argptr, format);
    char dest[1024];
    _vsnprintf(dest, sizeof(dest), format, argptr);
    va_end(argptr);
    
    msgs.push_back(std::string(dest));
    dbg("%s \n", dest );
  }

  void onTriggerEnter(const TMsgEntityTriggerEnter& trigger_enter) {
    CEntity* e = trigger_enter.h_entity;
    if (trigger_enter.h_entity.isValid())
      log("onTriggerEnter %s", e->getName());
  }

  void onTriggerExit(const TMsgEntityTriggerExit& trigger_exit) {
    CEntity* e = trigger_exit.h_entity;
    if (trigger_exit.h_entity.isValid())
      log("onTriggerExit %s", e->getName());
  }

  void onContact(const TMsgOnContact& msg)
  {
    //log("Contact");
  }

  void onLostContact(const TMsgOnLostContact& msg)
  {
    log("Contact Lost");
  }

  void changeState(const TMsgIntObjState& msg)
  {
    std::string s;
    if (msg.state == ObjState::Normal) s = "normal";
    else if (msg.state == ObjState::Pulled) s = "pulled";
    else if (msg.state == ObjState::Pushed) s = "pushed";
    else if (msg.state == ObjState::Pieces) s = "pieces";
    else s = "static";
    log("State: %s", s.c_str());
  }

public:
  void debugInMenu() {
    ImGui::SameLine();
    if (ImGui::SmallButton("Clear"))
      msgs.clear();
    for( size_t i=0; i<msgs.size(); ++i)
      ImGui::Text(msgs[msgs.size()-1-i].c_str());
  }
  static void registerMsgs() {
    DECL_MSG(TCompMsgLogger, TMsgEntityTriggerEnter, onTriggerEnter);
    DECL_MSG(TCompMsgLogger, TMsgEntityTriggerExit, onTriggerExit);
    DECL_MSG(TCompMsgLogger, TMsgOnContact, onContact);
    DECL_MSG(TCompMsgLogger, TMsgOnLostContact, onLostContact);
    DECL_MSG(TCompMsgLogger, TMsgIntObjState, changeState);
  }
};

DECL_OBJ_MANAGER("msg_logger", TCompMsgLogger);

