#include "mcv_platform.h"
#include "comp_name.h"
#include "comp_aabb.h"
#include "engine.h"

DECL_OBJ_MANAGER("name", TCompName);

std::unordered_map< std::string, CHandle > TCompName::all_names;

void TCompName::registerMsgs() {
	DECL_MSG(TCompName, TMsgEntityCreated, onEntityCreated);
}


void TCompName::onEntityCreated(const TMsgEntityCreated& msg) {
	std::string navMeshName = name;
	if (navMeshName.find("nav") != std::string::npos)
		EngineNavMesh.addHandle(CHandle(this).getOwner());
}

void TCompName::debugInMenu() 
{
  ImGui::Text(name);
  std::string s = "";
  if (tag == Tag::Interactable) s = "Interactable";
  if (tag == Tag::NoInteractable) s = "No Interactable";
  ImGui::Text("Tag: %s", s.c_str());
}

void TCompName::setName(const char* new_name) 
{
  assert(strlen(new_name) < max_size);
  strcpy(name, new_name);

  // Store the handle of the CompName, not the Entity, 
  // because during load, I still don't have an owner
  all_names[name] = CHandle(this);
}

void TCompName::setTag(Tag newTag) 
{
  tag = newTag;
}
 
void TCompName::load(const json& j, TEntityParseContext& ctx) 
{
  std::string s = "";
  std::string t = "";

  if (j.size() == 1)
    s = j.get<std::string>();
  else
  {
    s = j.value("entityName", "");
    t = j.value("tag", "");
  }
  assert(s != "");
  setName(s.c_str());

  if (t == "" or t == "Interactable") setTag(Tag::Interactable);
  else setTag(Tag::NoInteractable);
}

CHandle getEntityByName(const std::string& name) 
{
  auto it = TCompName::all_names.find(name);
  if( it == TCompName::all_names.end() )
    return CHandle();

  CHandle h_name = it->second;
  return h_name.getOwner();
}

