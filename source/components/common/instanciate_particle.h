#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

class TCompInstanciateParticle : public TCompBase {
	std::vector<std::string> v_filename;
	std::vector<CHandle> h_group_systems;

	VEC3 offset = VEC3(0, 0, 0);

	DECL_SIBLING_ACCESS();
	void onEntityCreated(const TMsgEntityCreated& msg);

	void DoSetup();

public:
	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);
	void debugInMenu();
	static void registerMsgs();
	~TCompInstanciateParticle();

private:
	bool isSetup = false;
	int frameCount = 0;
};