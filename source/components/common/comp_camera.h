#pragma once

#include "comp_base.h"
#include "entity/entity.h"

class TCompCamera : public CCamera, public TCompBase {
public:
	DECL_SIBLING_ACCESS();
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void renderDebug();
	void update(float delta);
};