#include "mcv_platform.h"
#include "comp_fsm.h"
#include "entity/entity.h"
#include "entity/entity_parser.h"
#include "entity/common_msgs.h"
#include "skeleton/comp_skeleton.h"
#include "fsm/fsm.h"

DECL_OBJ_MANAGER("fsm", TCompFSM);

void TCompFSM::load(const json& j, TEntityParseContext& ctx) 
{
  _fsm = Resources.get(j.value("file", ""))->as<CFSM>();
}

void TCompFSM::registerMsgs()
{
  DECL_MSG(TCompFSM, TMsgEntityCreated, onEntityCreated);
}

void TCompFSM::debugInMenu()
{
  _context.debugInMenu();
} 

void TCompFSM::update(float dt)
{
  _context.update(dt);
}

void TCompFSM::onEntityCreated(const TMsgEntityCreated&)
{
  _skeleton = get<TCompSkeleton>();
  _context.setSkeleton(_skeleton);
  _context.init(_fsm);
}

void TCompFSM::changeVariable(const char* varName, float value, bool isBool) {
	TVariable var;
	var._name = varName;
	if (isBool)
	{
		if (value > 0)
			var._value = true;
		else
			var._value = false;
	}
	else
	{
		var._value = value;
	}
	setVariable(var);
}

void TCompFSM::setVariable(const TVariable& var)
{
  _context.setVariable(var);
}

void TCompFSM::setWeight(float weight)
{
	_context.setWeight(weight);
}

std::string TCompFSM::getCurrentAnim() {
	return _context.getContextAnim();
}
