#include "mcv_platform.h"
#include "comp_damage_by_objects.h"
#include "components/common/comp_collider.h"
#include "components/controllers/comp_char_controller.h"
#include "components/common/comp_fireball.h"

using namespace physx;

DECL_OBJ_MANAGER("damage_by_objects", TCompDamageByObjects);

void TCompDamageByObjects::debugInMenu() {
	ImGui::DragFloat("MinimumVelocityToDamage", &minimumVelocityToDamage, 0.1f, 0.f, 10.f);
	ImGui::DragFloat("DamageAttenuation", &damageAttenuation, 0.1f, 0.f, 10.f);
}

void TCompDamageByObjects::load(const json& j, TEntityParseContext& ctx) {
	minimumVelocityToDamage = j.value("minimumVelocityToDamage", minimumVelocityToDamage);
	damageAttenuation = j.value("damageAttenuation", damageAttenuation);
}

void TCompDamageByObjects::registerMsgs() {
	DECL_MSG(TCompDamageByObjects, TMsgOnContact, onContactMessage);
}

void TCompDamageByObjects::onContactMessage(const TMsgOnContact& msg) {
	TCompCollider* c_src = msg.source;
	if (!c_src)
		return;

	PxRigidDynamic* rb_src = c_src->actor->is<PxRigidDynamic>();
	if (!rb_src)
		return;

	CEntity* e_src = CHandle(c_src).getOwner();
	if (!e_src)
		return;
	
	TCompFireballController* fireball = e_src->get<TCompFireballController>();
	if (fireball)
		return;

	TCompCharController* controller = e_src->get<TCompCharController>();
	if (controller)
		return;

	float vel_length = PXVEC3_TO_VEC3(rb_src->getLinearVelocity()).Length();
	if (vel_length > minimumVelocityToDamage) {
		TMsgDamage damage_msg;
		damage_msg.h_sender = msg.source.getOwner();
		damage_msg.h_bullet = msg.source.getOwner();
		damage_msg.damage = (vel_length / damageAttenuation) * rb_src->getMass();
		damage_msg.vel = PXVEC3_TO_VEC3(rb_src->getLinearVelocity());

		CEntity* objective = CHandle(this).getOwner();
		objective->sendMsg(damage_msg);
	}
}