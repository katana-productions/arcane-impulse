#pragma once
#include "genericDoor.h"
#include "components/common/comp_transform.h"
#include "components/common/comp_render.h"

class TCompHorizontalDoor : public TCompGenericDoor {
	DECL_SIBLING_ACCESS();

private:
	float targetXoffset;
	float speedOpen;
	float speedClose;
	VEC3 offsetL;
	VEC3 offsetR;
	std::string prefabL;
	std::string prefabR;

	bool open = false;
	bool close = false;

	float iniXL;
	float iniXR;
	float iniZL;
	float iniZR;
	TCompCollider *colLeft;
	TCompCollider *colRight;
	TCompTransform *doorTransform;
	TCompTransform *transLeft;
	TCompTransform *transRight;
	physx::PxRigidDynamic *rbLeft;
	physx::PxRigidDynamic *rbRight;

	TCompRender *renderLeft;
	TCompRender *renderRight;

public:
	static void registerMsgs();
	void start(const TMsgEntityCreated &msg);
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void renderDebug();
	void update(float dt);
};