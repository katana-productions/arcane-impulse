#include "mcv_platform.h"
#include "engine.h"
#include "comp_postPVolume.h"
#include "components/postfx/comp_render_focus.h"
#include "components/postfx/comp_render_bloom.h"
#include "components/postfx/comp_color_grading.h"
#include "components/postfx/comp_render_outlines.h"
#include "components/postfx/comp_render_vignette.h"
#include "render/module_render.h"

using namespace physx;

DECL_OBJ_MANAGER("comp_postPVolume", TCompPostPVolume);

void TCompPostPVolume::load(const json & j, TEntityParseContext & ctx)
{
	transitionTime = j.value("transitionTime", 2.0f);

	//Global
	globalAmbient = j.value("globalAmbient", -10.0f);
	globalExposure = j.value("globalExposure", -10.0f);

	//Vignette
	vigIntensity = j.value("vigIntensity", -10.0f);
	vigExtend = j.value("vigExtend", -10.0f);

	//Fog
	fogDepthFade = j.value("fogDepthFade", -10.0f);
	maxFog = j.value("maxFog", -10.0f);
	fogContrast = j.value("fogContrast", -10.0f);
	fogColor = loadVEC4(j, "fogColor");

	//Color Grading
	lut_name = j.value("lut", "data/textures/lut/neutralLut.dds");
}

void TCompPostPVolume::registerMsgs() {
	DECL_MSG(TCompPostPVolume, TMsgEntityTriggerEnter, onTriggerEnter);
}

void TCompPostPVolume::onTriggerEnter(const TMsgEntityTriggerEnter& msg) {
	CEntity *ent = msg.h_entity;
	if (!ent) return;

	TCompName* c_name = ent->get<TCompName>();
	if (!c_name) return;

	std::string name = ent->getName();

	if (name == "Player") {

    // PLAYER CAMERA
		CHandle h_camera = getEntityByName("CameraShaker");
		CEntity* e_camera = h_camera;

		if (globalAmbient > -10.0f) EngineBasics.LerpElement(&EngineRender.globalAmbient, globalAmbient, transitionTime, 0.0f);
		if (globalExposure > -10.0f) EngineBasics.LerpElement(&EngineRender.globalExposure, globalExposure, transitionTime, 0.0f);

		TCompVignette* render_vignette = e_camera->get<TCompVignette>();
		if (render_vignette) {
			if(vigExtend > -10.0f) EngineBasics.LerpElement(&render_vignette->vigExtend, vigExtend, transitionTime, 0.0f);
			if(vigIntensity > -10.0f) EngineBasics.LerpElement(&render_vignette->vigIntensity, vigIntensity, transitionTime, 0.0f);
		}

		TCompOutlines* render_outlines = e_camera->get<TCompOutlines>();
		if (render_outlines) {
			if(fogDepthFade > -10.0f) EngineBasics.LerpElement(&render_outlines->fogDepthFade, fogDepthFade, transitionTime, 0.0f);
			if(maxFog > -10.0f) EngineBasics.LerpElement(&render_outlines->maxFog, maxFog, transitionTime, 0.0f);
			if(fogContrast > -10.0f) EngineBasics.LerpElement(&render_outlines->fogContrast, fogContrast, transitionTime, 0.0f);
			if(fogColor.x > -10.0f) EngineBasics.LerpElement(&render_outlines->fogColor.x, fogColor.x, transitionTime, 0.0f);
			if(fogColor.y > -10.0f) EngineBasics.LerpElement(&render_outlines->fogColor.y, fogColor.y, transitionTime, 0.0f);
			if(fogColor.z > -10.0f) EngineBasics.LerpElement(&render_outlines->fogColor.z, fogColor.z, transitionTime, 0.0f);
		}

		TCompColorGrading* render_colGrading = e_camera->get<TCompColorGrading>();
		render_colGrading->CreateLutTexture(lut_name);

    // DEBUG CAMERA
    h_camera = getEntityByName("Camera Debug");
    e_camera = h_camera;

    if (globalAmbient > -10.0f) EngineBasics.LerpElement(&EngineRender.globalAmbient, globalAmbient, transitionTime, 0.0f);
    if (globalExposure > -10.0f) EngineBasics.LerpElement(&EngineRender.globalExposure, globalExposure, transitionTime, 0.0f);

    render_vignette = e_camera->get<TCompVignette>();
    if (render_vignette) {
      if (vigExtend > -10.0f) EngineBasics.LerpElement(&render_vignette->vigExtend, vigExtend, transitionTime, 0.0f);
      if (vigIntensity > -10.0f) EngineBasics.LerpElement(&render_vignette->vigIntensity, vigIntensity, transitionTime, 0.0f);
    }

    render_outlines = e_camera->get<TCompOutlines>();
    if (render_outlines) {
      if (fogDepthFade > -10.0f) EngineBasics.LerpElement(&render_outlines->fogDepthFade, fogDepthFade, transitionTime, 0.0f);
      if (maxFog > -10.0f) EngineBasics.LerpElement(&render_outlines->maxFog, maxFog, transitionTime, 0.0f);
      if (fogContrast > -10.0f) EngineBasics.LerpElement(&render_outlines->fogContrast, fogContrast, transitionTime, 0.0f);
      if (fogColor.x > -10.0f) EngineBasics.LerpElement(&render_outlines->fogColor.x, fogColor.x, transitionTime, 0.0f);
      if (fogColor.y > -10.0f) EngineBasics.LerpElement(&render_outlines->fogColor.y, fogColor.y, transitionTime, 0.0f);
      if (fogColor.z > -10.0f) EngineBasics.LerpElement(&render_outlines->fogColor.z, fogColor.z, transitionTime, 0.0f);
    }

    render_colGrading = e_camera->get<TCompColorGrading>();
    render_colGrading->CreateLutTexture(lut_name);



		EnginePersistence.setPostProcess(globalAmbient, globalExposure, vigExtend, vigIntensity, fogDepthFade, maxFog, fogContrast, fogColor, lut_name);

		CHandle(this).getOwner().destroy();
	}
}