#pragma once
#include "genericDoor.h"
#include "components/common/comp_transform.h"
#include "components/common/comp_render.h"

class TCompVerticalDoor : public TCompGenericDoor {
	DECL_SIBLING_ACCESS();

private:
	float iniY;
	float targetYoffset;
	float speedOpen;
	float speedClose;

	bool open = false;
	bool close = false;

	TCompTransform *doorTransform;
	TCompRender *render;

public:
	static void registerMsgs();
	void start(const TMsgEntityCreated &msg);
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void renderDebug();
	void update(float dt);
};