#pragma once
#include "entity/common_msgs.h"
#include "components/common/comp_base.h"
#include "components/common/comp_transform.h"
#include "components/common/comp_collider.h"
#include "entity/entity.h"

class TCompObjectShake : public TCompBase
{
private:
	float shakeIntensity = 0.0f;
	float smoothing = 25.0f;
	float roughnessMult = 1.0f;
	int nShakes = 0;
	VEC3 targetOffset = VEC3(0, 0, 0);
	VEC3 myOffset = VEC3(0, 0, 0);
	bool hasShaked = false;
  TCompTransform* myTransform;
  TCompCollider* myCol;

public:
	DECL_SIBLING_ACCESS();
	void init(const TMsgEntityCreated & msg);
	void onAlarmRecieve(const TMsgAlarmClock& msg);
	static void registerMsgs();

	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);
	void debugInMenu();

	void startShake(float intensity, float duration, float new_roughnessMult = 1.0f);
};