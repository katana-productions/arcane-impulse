#include "mcv_platform.h"
#include "engine.h"
#include "comp_checkpoint.h"

using namespace physx;

DECL_OBJ_MANAGER("comp_checkpoint", TCompCheckpoint);

void TCompCheckpoint::load(const json & j, TEntityParseContext & ctx)
{
	id = j.value("id", id);
	pullWallUnlocked = j.value("pullWallUnlocked", pullWallUnlocked);
	pullObjUnlocked = j.value("pullObjUnlocked", pullObjUnlocked);
	pushWallUnlocked = j.value("pushWallUnlocked", pushWallUnlocked);
	pushObjUnlocked = j.value("pushObjUnlocked", pushObjUnlocked);
	gloves = j.value("gloves", gloves);
	scene = j["scene"].get<std::string>();
	oneShoot = j.value("oneShoot", oneShoot);
}

void TCompCheckpoint::registerMsgs() {
	DECL_MSG(TCompCheckpoint, TMsgEntityTriggerEnter, onTriggerEnter);
}

void TCompCheckpoint::onTriggerEnter(const TMsgEntityTriggerEnter& msg) {
	CEntity *ent = msg.h_entity;
	if (!ent)
		return;

	TCompName* c_name = ent->get<TCompName>();
	if (!c_name)
		return;

	std::string name = ent->getName();

	if (msg.h_entity != CHandle(this) && ent != nullptr && name == "Player") {
		EnginePersistence.setId(id);
		EnginePersistence.setMagic(pullObjUnlocked, pullWallUnlocked, pushObjUnlocked, pushWallUnlocked);
		EnginePersistence.setGloves(gloves);
    if (scene != EnginePersistence.getScene())
      EngineLua.loadStartEvent(scene);
			//EnginePersistence.setScene(scene);
		name = "";
	}
	if (oneShoot)
		CHandle(this).getOwner().destroy();
	EnginePersistence.setUIMagicPower(gloves);
}
