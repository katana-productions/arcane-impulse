#pragma once

#include "comp_base.h"
#include "entity/common_msgs.h"
#include <unordered_map>

enum Tag {
  Interactable,
  NoInteractable
};
 
class TCompName : public TCompBase {
  static const size_t max_size = 64;
  char name[max_size];
  Tag tag;

  void onEntityCreated(const TMsgEntityCreated& msg);

public:

  static std::unordered_map< std::string, CHandle > all_names;
  const char* getName() const { return name; }
  void setName(const char* new_name);

  Tag getTag() const { return tag; }
  void setTag(Tag newTag);

  void debugInMenu();
  void load(const json& j, TEntityParseContext& ctx);

  static void registerMsgs();

};