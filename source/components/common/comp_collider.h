#pragma once

#include "comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "PxPhysicsAPI.h"

class TCompCollider : public TCompBase {
public:
  DECL_SIBLING_ACCESS();

  json jconfig;

  ~TCompCollider();

  physx::PxRigidActor*        actor = nullptr;
  physx::PxCapsuleController* controller = nullptr;
  physx::PxShape* shape = nullptr;
  CMesh*                      debug_mesh = nullptr;

  ObjState state = ObjState::Normal;
  ObjState getState();

  bool sleeping = false;
  VEC3 offset = VEC3(0,0,0);
  bool isSleeping() { return sleeping; }
  void changeSleepState(bool state);
  bool awakePower = false;
  void awakeWithPower();
  bool getAwakePower() { return awakePower; }

  void load(const json& j, TEntityParseContext& ctx);
  void debugInMenu();
  void renderDebug();
  void init(const TMsgSceneCreated& msg);
  void onEntityCreated(const TMsgEntityCreated& msg);
  void onAlarmRecieve(const TMsgAlarmClock & msg);
  void changeState(const TMsgIntObjState &msg);
  void onContact(const TMsgOnContact &msg);
  void setActive(bool active);

  typedef void (TCompCollider::*TShapeFn)(physx::PxShape* shape, physx::PxGeometryType::Enum geometry_type, const void* geom, MAT44 world);
  void renderDebugShape(physx::PxShape* shape, physx::PxGeometryType::Enum geometry_type, const void* geom, MAT44 world);
  void debugInMenuShape(physx::PxShape* shape, physx::PxGeometryType::Enum geometry_type, const void* geom, MAT44 world);
  void onEachShape(TShapeFn fn);

  static void registerMsgs();
};