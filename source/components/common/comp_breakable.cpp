#include "mcv_platform.h"
#include "utils/utils.h"
#include "modules/Basics.h"
#include "physics/module_physics.h"
#include "engine.h"
#include "input\input.h"
#include "entity/entity_parser.h"
#include "comp_breakable.h"
#include "comp_damage_by_objects.h"
#include "components/player/comp_player_magic.h"

using namespace physx;

DECL_OBJ_MANAGER("comp_breakable", TCompBreakable);

void TCompBreakable::load(const json & j, TEntityParseContext & ctx)
{
	meshName = j["mesh"].get<std::vector<std::string>>();
	int count = meshName.size();
	offsetPos = loadVectorVEC3(j, "offset", count);
	vel = j.value("vel", vel);
	sound = j.value("sound", sound);
	particles = j.value("particles", particles);
}

void TCompBreakable::registerMsgs() {
	DECL_MSG(TCompBreakable, TMsgOnContact, onContactMessage);
	DECL_MSG(TCompBreakable, TMsgAlarmClock, onTimer);
	DECL_MSG(TCompBreakable, TMsgIntObjState, onState);
	DECL_MSG(TCompBreakable, TMsgDamage, onExplosion);
}

void TCompBreakable::onContactMessage(const TMsgOnContact& msg) {
	CEntity* player = getEntityByName("Player");
	TCompCollider* playerCollider = player->get<TCompCollider>();
	CEntity* messageEntity = msg.source.getOwner();
	if (messageEntity == nullptr) return;
	TCompCollider* collider = messageEntity->get<TCompCollider>();
	TCompCollider* colliderParent = get<TCompCollider>();

	if (CHandle(this) != msg.source && collider != nullptr && colliderParent != nullptr && protection == 0 && state != ObjState::Pulled && playerCollider != msg.source && state != ObjState::Pieces) {
		CEntity* player = getEntityByName("Player");
		TCompPlayerMagic* magic = player->get<TCompPlayerMagic>();

		auto status = collider->getState();
		if (status == ObjState::Pulled) return;

		TCompTransform* transform = get<TCompTransform>();
		assert(transform);
		PxRigidDynamic* rb = colliderParent->actor->is<PxRigidDynamic>();
		//assert(rb);
		PxRigidDynamic* rbReceptor = collider->actor->is<PxRigidDynamic>();
		//assert(rbReceptor);

		VEC3 pos = transform->getPosition();
		float yaw, pitch, roll;
		transform->getAngles(&yaw, &pitch, &roll);

		PxRigidDynamic* rbBroken;

		//Object Himself
		float linVel = 0;
		float dotResult = 0;

		if (rb != nullptr) {
			VEC3 velObject = PXVEC3_TO_VEC3(rb->getLinearVelocity());
			linVel = velObject.Length();
			velObject.Normalize();
			VEC3 normal = msg.normal;
			dotResult = velObject.Dot(-msg.normal);
			dotResult = clampFloat(dotResult, 0.18, 1);
		}
		
		//Other object
		float linVelRec = 0;
		float dotResultRec = 0;

		if (rbReceptor != nullptr) {
			VEC3 velObjectRec = PXVEC3_TO_VEC3(rbReceptor->getLinearVelocity());
			linVelRec = velObjectRec.Length();
			velObjectRec.Normalize();
			VEC3 normalRec = msg.normal;
			dotResultRec = velObjectRec.Dot(-msg.normal);
			dotResultRec = clampFloat(dotResultRec, 0.18, 1);
		}

		if ((linVel * dotResult > vel) || (linVelRec * dotResultRec) > vel)
		{
			for (int i = 0; i < meshName.size(); ++i) {
				CEntity* part = EngineBasics.InstantiateAtParent(meshName[i], pos + offsetPos[i], yaw, pitch, roll, CHandle(this));
				EnginePersistence.brokenPieces.emplace_back(CHandle(part));
				TCompCollider* colliderBroken = part->get<TCompCollider>();
				rbBroken = colliderBroken->actor->is<PxRigidDynamic>();
				/*
				auto realForce = ((rb->getMass()*rb->getLinearVelocity()) + (rbReceptor->getMass()*rbReceptor->getLinearVelocity())) / (rb->getMass() + rbReceptor->getMass())
				*/
				if (linVel == 0 || linVelRec == 0) {
					if (rbReceptor == nullptr) 
						rbBroken->addForce(rb->getLinearVelocity() / 2, PxForceMode::eIMPULSE);
					else if (rb == nullptr) 
						rbBroken->addForce(rbReceptor->getLinearVelocity() / 2, PxForceMode::eIMPULSE);
				}
				else {
					auto realForce = (rb->getLinearVelocity() + rbReceptor->getLinearVelocity()) / 2;
					rbBroken->addForce(realForce, PxForceMode::eIMPULSE);
				}

				TMsgIntObjState msg;
				msg.state = ObjState::Pieces;
				part->sendMsg(msg);
			}

			EngineParticles.CreateOneShootSystemParticles(particles.c_str(), transform);

			protection = 1;
			float vol = 0;

			if (rb != nullptr)
				vol = clampFloat(PXVEC3_TO_VEC3(rb->getLinearVelocity()).Length() / magic->getPushForce(), 0.2f, 1.0f);
			else 
				vol = clampFloat(PXVEC3_TO_VEC3(rbReceptor->getLinearVelocity()).Length() / magic->getPushForce(), 0.2f, 1.0f);

			Studio::EventInstance* event = EngineAudio.createEvent(sound);
			event->setVolume(vol);
			EngineAudio.playEvent3D(sound, get<TCompTransform>(), 2, event);

			EngineAlarms.AddAlarm(0, 50, CHandle(this));
		}
	}
}

void TCompBreakable::onTimer(const TMsgAlarmClock& msg) {
	if (msg.id == 50) {
		CHandle(this).getOwner().destroy();
	}
}

void TCompBreakable::onState(const TMsgIntObjState& msg) {
	state = msg.state;
}

void TCompBreakable::onExplosion(const TMsgDamage & msg) {
	TCompTransform* transform = get<TCompTransform>();
	VEC3 pos = transform->getPosition();
	float yaw, pitch, roll;
	transform->getAngles(&yaw, &pitch, &roll);

	PxRigidDynamic* rbBroken;
	VEC3 aux = msg.force;

	for (int i = 0; i < meshName.size(); ++i) {
		CEntity* part = EngineBasics.InstantiateAtParent(meshName[i], pos + offsetPos[i], yaw, pitch, roll, CHandle(this));
		EnginePersistence.brokenPieces.emplace_back(CHandle(part));
		TCompCollider* colliderBroken = part->get<TCompCollider>();
		rbBroken = colliderBroken->actor->is<PxRigidDynamic>();

		rbBroken->addForce(VEC3_TO_PXVEC3(aux)*2, PxForceMode::eIMPULSE);

		TMsgIntObjState msg;
		msg.state = ObjState::Pieces;
		part->sendMsg(msg);
	}

	EngineParticles.CreateOneShootSystemParticles(particles.c_str(), transform);

	protection = 1;
	float vol = 0;

	vol = clampFloat(aux.Length()*2, 0.2f, 1.0f);

	Studio::EventInstance* event = EngineAudio.createEvent(sound);
	event->setVolume(vol);
	EngineAudio.playEvent3D(sound, get<TCompTransform>(), 2, event);

	EngineAlarms.AddAlarm(0, 50, CHandle(this));
}


void TCompBreakable::debugInMenu()
{

}
