#include "mcv_platform.h"
#include "instanciate_particle.h"
#include "components/common/comp_transform.h"
#include "engine.h"

DECL_OBJ_MANAGER("instanciate_particle", TCompInstanciateParticle);

void TCompInstanciateParticle::debugInMenu() {
	ImGui::DragFloat3("Offset", &offset.x, 0.1f, -10.f, 10.f);
}

void TCompInstanciateParticle::load(const json& j, TEntityParseContext& ctx) {
	if (j.find("system_particles") != j.end()) {
		json jsystems = j["system_particles"];
		for (json::iterator it = jsystems.begin(); it != jsystems.end(); ++it) {
			std::string filename = it.value().get<std::string>();
			v_filename.push_back(filename);
		}
	}
	offset = loadVEC3(j, "offset", VEC3(0, 0, 0));
}

void TCompInstanciateParticle::update(float dt)
{
	if(frameCount > 6) return;
	if (!isSetup && frameCount == 5)
	{
		DoSetup();
		isSetup = true;
	}
	frameCount++;
}

void TCompInstanciateParticle::registerMsgs() {
	DECL_MSG(TCompInstanciateParticle, TMsgEntityCreated, onEntityCreated);
}

void TCompInstanciateParticle::onEntityCreated(const TMsgEntityCreated& msg) {
	
}

void TCompInstanciateParticle::DoSetup()
{
	std::vector<CHandle> new_group_systems;
	TCompTransform* c_trans = get< TCompTransform>();
	VEC3 pos = c_trans->getPosition() + offset;
	for (auto filename : v_filename) {
		new_group_systems = EngineParticles.CreateLogicSeparatedSystemParticles(filename.c_str(), c_trans, pos);
		for (auto new_system : new_group_systems) {
			h_group_systems.push_back(new_system);
		}
	}
}

TCompInstanciateParticle::~TCompInstanciateParticle() {
	for (auto h_loaded : h_group_systems) {
		if (!h_loaded.isValid()) continue;
		EngineParticles.DestroySystem(h_loaded);
	}
}