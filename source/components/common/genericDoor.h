#pragma once
#include "mcv_platform.h"
#include "entity/entity.h"
#include "components/common/comp_base.h"
#include "components/common/comp_transform.h"

class TCompGenericDoor {

protected:
	bool isOpening;
private:

public:
	void SetDoorState(bool isOpening) { this->isOpening = isOpening; }
};