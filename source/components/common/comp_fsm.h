#ifndef INC_COMP_FSM_H_
#define INC_COMP_FSM_H_

#include "geometry/transform.h"
#include "comp_base.h"
#include "entity/entity.h"
#include "fsm/context.h"

struct TMsgEntityCreated;

struct TCompFSM : public TCompBase
{
  DECL_SIBLING_ACCESS();

  void load(const json& j, TEntityParseContext& ctx);
  void update(float dt);
  void debugInMenu();
  static void registerMsgs();

  void onEntityCreated(const TMsgEntityCreated&);

  void changeVariable(const char* varName, float value, bool isBool);
  void setVariable(const TVariable& var);
  void setWeight(float weight);

	std::string getCurrentAnim();

	CFSMContext getContext() { return _context; }

private:
  const CFSM* _fsm = nullptr;
  CFSMContext _context;
  CHandle _skeleton;
};

#endif
