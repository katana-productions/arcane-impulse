#pragma once

#include "components/common/comp_base.h"

class TCompExplosionMesh : public TCompBase {
  DECL_SIBLING_ACCESS();
private:
	float timeToExplode = 0.5f;
	float totalTime = 0.0f;
	float radius = 1.0f;

public:
  void update(float dt);
  void load(const json& j, TEntityParseContext& ctx);
  void debugInMenu();
  void setRadius(float new_radius);
};