#include "mcv_platform.h"
#include "comp_collider.h"
#include "engine.h"
#include "physics/module_physics.h"
#include "components/common/comp_transform.h"
#include "components/common/objectShake.h"

DECL_OBJ_MANAGER("collider", TCompCollider);

using namespace physx;

void TCompCollider::load(const json& j, TEntityParseContext& ctx) {
	jconfig = j;
	sleeping = j.value("sleeping", false);
	awakePower = j.value("awakePower", false);
}

// --------------------------------------------------------------------
TCompCollider::~TCompCollider() {
	if (actor && EnginePhysics.isActive()) {
		if (controller) {
			controller->release();
			controller = nullptr;
			// No need to release the actor.
			actor = nullptr;
		}
		else actor->release(), actor = nullptr;
	}
}

// --------------------------------------------------------------------
// Iterates over all shapes of the actor, calling the given callback
// resolves the invalid cases, and gives the world matrix including the
// shape offset.
void TCompCollider::onEachShape(TShapeFn fn) {
	TCompTransform* trans = get<TCompTransform>();
	assert(trans);

	static const PxU32 max_shapes = 8;
	PxShape* shapes[max_shapes];

	PxU32 nshapes = actor->getNbShapes();
	assert(nshapes <= max_shapes);

	// Even when the buffer is small, it writes all the shape pointers
	PxU32 shapes_read = actor->getShapes(shapes, sizeof(shapes), 0);

	// An actor can have several shapes
	for (PxU32 i = 0; i < nshapes; ++i) {
		PxShape* shape = shapes[i];
		assert(shape);

		// Combine physics local offset with world transform of the entity
		MAT44 world = toTransform(shape->getLocalPose()).asMatrix() * trans->asMatrix();

		switch (shape->getGeometryType()) {
		case PxGeometryType::eSPHERE: {
			PxSphereGeometry sphere;
			if (shape->getSphereGeometry(sphere))
				(this->*fn)(shape, PxGeometryType::eSPHERE, &sphere, world);
			break;
		}
		case PxGeometryType::eBOX: {
			PxBoxGeometry box;
			if (shape->getBoxGeometry(box))
				(this->*fn)(shape, PxGeometryType::eBOX, &box, world);
			break;
		}

		case PxGeometryType::ePLANE: {
			PxPlaneGeometry plane;
			if (shape->getPlaneGeometry(plane))
				(this->*fn)(shape, PxGeometryType::ePLANE, &plane, world);
			break;
		}

		case PxGeometryType::eCAPSULE: {
			PxCapsuleGeometry capsule;
			if (shape->getCapsuleGeometry(capsule))
				(this->*fn)(shape, PxGeometryType::eCAPSULE, &capsule, world);
			break;
		}

		case PxGeometryType::eTRIANGLEMESH: {
			PxTriangleMeshGeometry trimesh;
			if (shape->getTriangleMeshGeometry(trimesh))
				(this->*fn)(shape, PxGeometryType::eTRIANGLEMESH, &trimesh, world);
			break;
		}

		case PxGeometryType::eCONVEXMESH: {
			PxConvexMeshGeometry convex;
			if (shape->getConvexMeshGeometry(convex))
				(this->*fn)(shape, PxGeometryType::eCONVEXMESH, &convex, world);
			break;
		}

		default:
			break;
		}
	}
}

// --------------------------------------------------------------------
void TCompCollider::renderDebugShape(physx::PxShape* shape, physx::PxGeometryType::Enum geometry_type, const void* geom, MAT44 world) {
	VEC4 color = VEC4(1, 0, 0, 1);

	PxShapeFlags flags = shape->getFlags();
	if (flags & PxShapeFlag::eTRIGGER_SHAPE)
		color = VEC4(1, 1, 0, 1);

	switch (geometry_type) {
	case PxGeometryType::eSPHERE: {
		PxSphereGeometry* sphere = (PxSphereGeometry*)geom;
		drawWiredSphere(world, sphere->radius, color);
		break;
	}
	case PxGeometryType::eBOX: {
		PxBoxGeometry* box = (PxBoxGeometry*)geom;
		drawWiredAABB(VEC3(0, 0, 0), PXVEC3_TO_VEC3(box->halfExtents), world, color);
		break;
	}
	case PxGeometryType::ePLANE: {
		PxBoxGeometry* box = (PxBoxGeometry*)geom;
		const CMesh* grid = Resources.get("grid.mesh")->as<CMesh>();
		// To generate a PxPlane from a PxTransform, transform PxPlane(1, 0, 0, 0).
		// Our plane is 0,1,0
		MAT44 Z2Y = MAT44::CreateRotationZ((float)-M_PI_2);
		drawMesh(grid, Z2Y * world, VEC4(1, 1, 1, 1));
		break;
	}
	case PxGeometryType::eCAPSULE: {
		PxCapsuleGeometry* capsule = (PxCapsuleGeometry*)geom;
		// world indicates the foot position
		world = world * MAT44::CreateTranslation(0.f, capsule->radius, 0.f);
		drawWiredSphere(world, capsule->radius, color);
		world = world * MAT44::CreateTranslation(0.f, capsule->halfHeight * 2, 0.f);
		drawWiredSphere(world, capsule->radius, color);
		break;
	}
	case PxGeometryType::eTRIANGLEMESH: {
		const CMesh* debug_mesh = (const CMesh*)shape->userData;
		assert(debug_mesh);
		drawMesh(debug_mesh, world, color);
		break;
	}

	case PxGeometryType::eCONVEXMESH: {
		const CMesh* debug_mesh = (const CMesh*)shape->userData;
		assert(debug_mesh);
		drawMesh(debug_mesh, world, color);
		break;
	}
	}
}

void TCompCollider::renderDebug() {
	onEachShape(&TCompCollider::renderDebugShape);
}

// --------------------------------------------------------------------
void TCompCollider::debugInMenuShape(physx::PxShape* shape, physx::PxGeometryType::Enum geometry_type, const void* geom, MAT44 world) {
	PxShapeFlags flags = shape->getFlags();
	if (flags & PxShapeFlag::eTRIGGER_SHAPE)
		ImGui::Text("Is trigger");

	switch (geometry_type) {
	case PxGeometryType::eSPHERE: {
		PxSphereGeometry* sphere = (PxSphereGeometry*)geom;
		ImGui::LabelText("Sphere Radius", "%f", sphere->radius);
		break;
	}
	case PxGeometryType::eBOX: {
		PxBoxGeometry* box = (PxBoxGeometry*)geom;
		ImGui::LabelText("Box", "Half:%f %f %f", box->halfExtents.x, box->halfExtents.y, box->halfExtents.z);
		break;
	}
	case PxGeometryType::ePLANE: {
		PxPlaneGeometry* plane = (PxPlaneGeometry*)geom;
		ImGui::Text("Plane");
		break;
	}
	case PxGeometryType::eCAPSULE: {
		PxCapsuleGeometry* capsule = (PxCapsuleGeometry*)geom;
		ImGui::LabelText("Capsule", "Rad:%f Height:%f", capsule->radius, capsule->halfHeight);
		break;
	}
	case PxGeometryType::eTRIANGLEMESH: {
		PxTriangleMeshGeometry* trimesh = (PxTriangleMeshGeometry*)geom;
		ImGui::LabelText("Tri mesh", "%d verts", trimesh->triangleMesh->getNbVertices());
		break;
	}

	case PxGeometryType::eCONVEXMESH: {
		PxConvexMeshGeometry* convex = (PxConvexMeshGeometry*)geom;
		ImGui::LabelText("Convex mesh", "%d verts", convex->convexMesh->getNbVertices());
		break;
	}
	}
}

void TCompCollider::debugInMenu() {

	std::string s = "";
	if (state == ObjState::Normal) s = "Normal";
	else if (state == ObjState::Pulled) s = "Pulled";
	else s = "Pushed";
	ImGui::Text("ObjState: %s", s.c_str());

	auto actor_type = actor->getType();
	if (actor_type == physx::PxActorTypeFlag::eRIGID_DYNAMIC)
		ImGui::Text("Rigid Dynamic");
	else if (actor_type == physx::PxActorTypeFlag::eRIGID_STATIC)
		ImGui::Text("Rigid Static");

	if (controller) {
		float rad = controller->getRadius();
		if (ImGui::DragFloat("Controller Radius", &rad, 0.02f, 0.1f, 5.0f))
			controller->setRadius(rad);
		float height = controller->getHeight();
		if (ImGui::DragFloat("Controller Height", &height, 0.02f, 0.1f, 5.0f))
			controller->setHeight(height);
	}

	onEachShape(&TCompCollider::debugInMenuShape);
}

// --------------------------------------------------------------------
void TCompCollider::registerMsgs() {
	DECL_MSG(TCompCollider, TMsgSceneCreated, init);
	DECL_MSG(TCompCollider, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompCollider, TMsgIntObjState, changeState);
	DECL_MSG(TCompCollider, TMsgOnContact, onContact);
	DECL_MSG(TCompCollider, TMsgAlarmClock, onAlarmRecieve);
}

void TCompCollider::init(const TMsgSceneCreated& msg)
{
	if (actor == nullptr) {
		EnginePhysics.createActor(*this);
		actor->setActorFlag(physx::PxActorFlag::eDISABLE_SIMULATION, true);
	}
	actor->setActorFlag(physx::PxActorFlag::eDISABLE_SIMULATION, false);
	physx::PxRigidDynamic *rb = actor->is< physx::PxRigidDynamic >();
	if (rb != nullptr and !rb->getRigidBodyFlags().isSet(physx::PxRigidBodyFlag::eKINEMATIC))
	{
		TCompName *n = get<TCompName>();
		std::string asdf = n->getName();
		changeSleepState(sleeping);
		if (awakePower)
		{
			rb->setRigidBodyFlag(physx::PxRigidBodyFlag::eKINEMATIC, true);
		}
	}
}

void TCompCollider::onEntityCreated(const TMsgEntityCreated& msg)
{
	EnginePhysics.createActor(*this);
	actor->setActorFlag(physx::PxActorFlag::eDISABLE_SIMULATION, true);
	changeSleepState(sleeping);
}

void TCompCollider::changeState(const TMsgIntObjState &msg)
{
	if (actor->is<PxRigidDynamic>()) state = msg.state;
}

void TCompCollider::onContact(const TMsgOnContact &msg)
{
	if (actor->is<PxRigidDynamic>() and state == ObjState::Pushed) state = ObjState::Normal;
}

void TCompCollider::setActive(bool active)
{
	EnginePhysics.getScene()->removeActor(*actor);
	if (active)
	{
		EnginePhysics.createActor(*this);
	}
}

void TCompCollider::changeSleepState(bool state) {
	sleeping = state;
	physx::PxRigidDynamic *rb = actor->is< physx::PxRigidDynamic >();
	if (rb != nullptr and !rb->getRigidBodyFlags().isSet(physx::PxRigidBodyFlag::eKINEMATIC))
	{
		if (sleeping) {
			rb->putToSleep();
		}
		else {
			rb->wakeUp();
		}
	}
}

void TCompCollider::onAlarmRecieve(const TMsgAlarmClock& msg) {
	if (msg.id == -149)
	{
		physx::PxRigidDynamic *rb = actor->is< physx::PxRigidDynamic >();
		rb->setRigidBodyFlag(physx::PxRigidBodyFlag::eKINEMATIC, false);
		actor->setActorFlag(PxActorFlag::eDISABLE_GRAVITY, false);
	}
}

void TCompCollider::awakeWithPower()
{
	if (awakePower)
	{
		awakePower = false;
		float animT = 0.4f;

		TCompObjectShake* shake = get<TCompObjectShake>();
		shake->startShake(0.1f, animT);

		CHandle myHandle = CHandle(this);
		EngineAlarms.AddAlarm(animT, -149, myHandle);
	}
}

ObjState TCompCollider::getState() { return state; }