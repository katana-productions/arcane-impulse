#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

class TCompBombController : public TCompBase {
private:
  bool exploding = false;
  int allowed_bounces = 1;
  int bounces;
  float time = 0.0f;
  float time_to_explode = 0.0f;
  float damage = 40.0f;
  float knockback = 40.0f;
  float min_knockback = 25.0f;
  float explosion_radius = 5.0f;

  void onContactMsg(const TMsgOnContact& msg);
  void onDamageMsg(const TMsgDamage& msg);

public:
  DECL_SIBLING_ACCESS();
  void update(float dt);
  void load(const json& j, TEntityParseContext& ctx);
  void debugInMenu();
  static void registerMsgs();
  void explode();
};