#include "mcv_platform.h"
#include "verticalDoor.h"
#include "engine.h"

DECL_OBJ_MANAGER("comp_verticalDoor", TCompVerticalDoor);

void TCompVerticalDoor::registerMsgs()
{
	DECL_MSG(TCompVerticalDoor, TMsgEntityCreated, start);
}

void TCompVerticalDoor::start(const TMsgEntityCreated &msg)
{
	//dbg("start %f\n", Time.current);
	doorTransform = get<TCompTransform>();
	iniY = doorTransform->getPosition().y;
	render = get<TCompRender>();
	isOpening = false;
}

void TCompVerticalDoor::load(const json & j, TEntityParseContext & ctx)
{
	targetYoffset = j.value("targetYoffset", 20.0f);
	speedOpen = j.value("speedOpen", 5.0f);
	speedClose = j.value("speedClose", speedOpen);
}

void TCompVerticalDoor::update(float dt)
{
	doorTransform = get<TCompTransform>();
	TCompCollider *col = get<TCompCollider>();
	if (col == nullptr) return;
	physx::PxRigidDynamic *rb = col->actor->is< physx::PxRigidDynamic >();

	VEC3 pos = doorTransform->getPosition();
	float dif = pos.y - iniY;
	if (isOpening && dif < targetYoffset)
	{
		pos.y += speedOpen * dt;
		rb->setKinematicTarget(physx::PxTransform(VEC3_TO_PXVEC3(pos), QUAT_TO_PXQUAT(doorTransform->getRotation())));
		if (!open) {
			EngineAudio.playEvent3D("event:/Environment/PortculisOpening", doorTransform, 0);
			open = true;
			close = false;
			if (render) {
				//render->glowAmount = 150.0f;
			}
		}
	}
	else if (!isOpening && dif > 0)
	{
		pos.y -= speedClose * dt;
		rb->setKinematicTarget(physx::PxTransform(VEC3_TO_PXVEC3(pos), QUAT_TO_PXQUAT(doorTransform->getRotation())));
		if (!close) {
			EngineAudio.playEvent3D("event:/Environment/PortculisClosing", doorTransform, 0);
			close = true;
			open = false;
			if (render) {
				//render->glowAmount = 0.0f;
			}
		}
	}
}

void TCompVerticalDoor::debugInMenu()
{
	ImGui::Checkbox("IsOpening", &isOpening);
}

void TCompVerticalDoor::renderDebug()
{

}