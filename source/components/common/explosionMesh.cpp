#include "mcv_platform.h"
#include "explosionMesh.h"
#include "utils/utils.h"
#include "engine.h"
#include "entity/entity.h"
#include "entity/entity_parser.h"
#include "components/common/comp_transform.h"

DECL_OBJ_MANAGER("explosion_mesh", TCompExplosionMesh);

void TCompExplosionMesh::load(const json & j, TEntityParseContext & ctx)
{
	radius = j.value("radius", radius);
	timeToExplode = j.value("timeToExplode", timeToExplode);
}

void TCompExplosionMesh::update(float dt)
{
	if (totalTime >= timeToExplode)
	{
		CHandle(this).getOwner().destroy();
	}

	TCompTransform* transform = get<TCompTransform>();
	if (transform) transform->setScale(radius);

	totalTime += dt;
}

void TCompExplosionMesh::setRadius(float new_radius) {
	TCompTransform* transform = get<TCompTransform>();
	radius = new_radius;
	EngineBasics.LerpElement(&radius, 0.0f, 1.0f);
	if (transform) transform->setScale(radius);
}

void TCompExplosionMesh::debugInMenu()
{
}
