#include "mcv_platform.h"
#include "horizontalDoor.h"
#include "engine.h"

DECL_OBJ_MANAGER("comp_horizontal_door", TCompHorizontalDoor);

void TCompHorizontalDoor::registerMsgs()
{
	DECL_MSG(TCompHorizontalDoor, TMsgEntityCreated, start);
}

void TCompHorizontalDoor::start(const TMsgEntityCreated &msg)
{
	CEntity *ent = msg.h_entity;
	doorTransform = ent->get<TCompTransform>();
	TCompName* myName = ent->get<TCompName>();

	float y, p, r;
	doorTransform->getAngles(&y, &p, &r);

	// Instantiate the two parts of the door
	/*VEC3 offsetL;
	offsetL.x = -2.9624;
	offsetL.y = 0.183838;
	offsetL.z = -3.94647;*/
	VEC3 posL = doorTransform->getPosition() + offsetL;
	CEntity *doorLeft = EngineBasics.Instantiate(prefabL, posL, y, p, r);
	TCompName *leftName = doorLeft->get<TCompName>();
	std::string n = (std::string)(myName->getName()) + "_door_left";
	leftName->setName(n.c_str());

	/*VEC3 offsetR;
	offsetR.x = 3.03286;
	offsetR.y = 0.183838;
	offsetR.z = -3.94647;*/
	VEC3 posR = doorTransform->getPosition() + offsetR;
	CEntity *doorRight = EngineBasics.Instantiate(prefabR, posR, y, p, r);
	TCompName *rightName = doorRight->get<TCompName>();
	n = (std::string)(myName->getName()) + "_door_right";
	rightName->setName(n.c_str());

	transLeft = doorLeft->get<TCompTransform>();
	transRight = doorRight->get<TCompTransform>();
	colLeft = doorLeft->get<TCompCollider>();
	colRight = doorRight->get<TCompCollider>();
	rbLeft = colLeft->actor->is<physx::PxRigidDynamic>();
	rbRight = colRight->actor->is<physx::PxRigidDynamic>();

	iniXL = transLeft->getPosition().x;
	iniXR = transRight->getPosition().x;
	iniZL = transLeft->getPosition().z;
	iniZR = transRight->getPosition().z;
	renderLeft = doorLeft->get<TCompRender>();
	renderRight = doorRight->get<TCompRender>();
}

void TCompHorizontalDoor::load(const json & j, TEntityParseContext & ctx)
{
	targetXoffset = j.value("targetXoffset", 10.0f);
	speedOpen = j.value("speedOpen", 5.0f);
	speedClose = j.value("speedClose", speedOpen);
	offsetL = loadVEC3(j, "offsetL");
	offsetR = loadVEC3(j, "offsetR");
	prefabL = j.value("prefabL", "");
	prefabR = j.value("prefabR", "");
}

void TCompHorizontalDoor::update(float dt)
{
	doorTransform = get<TCompTransform>();
	rbLeft = colLeft->actor->is<physx::PxRigidDynamic>();
	rbRight = colRight->actor->is<physx::PxRigidDynamic>();

	//Left part
	VEC3 pos = transLeft->getPosition();
	//float dif = pos.x - iniXL;
	float dif = pos.z - iniZL;
	if (isOpening && dif > -targetXoffset)
	{
		//pos.x -= speedOpen * dt;
		pos.z -= speedOpen * dt;
		rbLeft->setKinematicTarget(physx::PxTransform(VEC3_TO_PXVEC3(pos), QUAT_TO_PXQUAT(doorTransform->getRotation())));
		if (!open) {
			EngineAudio.playEvent3D("event:/Environment/PortculisOpening", transLeft, 0);
			open = true;
			close = false;
			if (renderLeft && renderRight)
			{
				//renderLeft->glowAmount = 150.0f;
				//renderRight->glowAmount = 150.0f;
			}
		}
	}
	else if (!isOpening && dif < 0)
	{
		//pos.x += speedClose * dt;
		pos.z += speedClose * dt;
		rbLeft->setKinematicTarget(physx::PxTransform(VEC3_TO_PXVEC3(pos), QUAT_TO_PXQUAT(doorTransform->getRotation())));
		if (!close) {
			EngineAudio.playEvent3D("event:/Environment/PortculisClosing", transLeft, 0);
			close = true;
			open = false;
			if (renderLeft && renderRight) {
				//renderLeft->glowAmount = 0.0f;
				//renderRight->glowAmount = 0.0f;
			}
		}
	}


	//RightPart
	pos = transRight->getPosition();
	//dif = pos.x - iniXR;
	dif = pos.z - iniZR;
	if (isOpening && dif < targetXoffset)
	{
		//pos.x += speedOpen * dt;
		pos.z += speedOpen * dt;
		rbRight->setKinematicTarget(physx::PxTransform(VEC3_TO_PXVEC3(pos), QUAT_TO_PXQUAT(doorTransform->getRotation())));
		if (!open) {
			EngineAudio.playEvent3D("event:/Environment/PortculisOpening", transRight, 0);
			open = true;
			close = false;
		}
	}
	else if (!isOpening && dif > 0)
	{
		//pos.x -= speedClose * dt;
		pos.z -= speedClose * dt;
		rbRight->setKinematicTarget(physx::PxTransform(VEC3_TO_PXVEC3(pos), QUAT_TO_PXQUAT(doorTransform->getRotation())));
		if (!close) {
			EngineAudio.playEvent3D("event:/Environment/PortculisClosing", transRight, 0);
			close = true;
			open = false;
		}
	}

}

void TCompHorizontalDoor::debugInMenu()
{
	ImGui::Checkbox("IsOpening", &isOpening);
}

void TCompHorizontalDoor::renderDebug()
{

}
