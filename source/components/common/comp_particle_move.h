#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

class TCompSystemParticleWorldMove : public TCompBase {
	std::vector<std::string> v_filename;
	std::vector<CHandle> h_group_systems;

	VEC3 offset = VEC3(0,0,0);
	VEC3 originalPos = VEC3(0, 0, 0);
	VEC3 lastPos = VEC3(0, 0, 0);
	bool enabled = true;
	bool trailDirCorrection = false;
	bool isActive = true;

	CHandle player;

	DECL_SIBLING_ACCESS();
	void onEntityCreated(const TMsgEntityCreated& msg);

public:
	void update(float dt);
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	static void registerMsgs();

	void SetEnabled(bool isEnabled) { enabled = isEnabled; }
	void ActivateParticles();

	~TCompSystemParticleWorldMove();
};