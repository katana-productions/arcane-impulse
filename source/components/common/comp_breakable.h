#pragma once
#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "components/common/comp_transform.h"

using namespace physx;

class TCompBreakable : public TCompBase {
  DECL_SIBLING_ACCESS();

private:
	int protection = 0;
	int vel = 2;
	int state = 0;
	std::string sound = "";
	std::string particles = "data/particles/prefabs_particles/defaultObjDust.json";

	std::vector <std::string> meshName;
	std::vector <VEC3> offsetPos;
	CHandle lastMsg;


	void onContactMessage(const TMsgOnContact& msg);
	void onTimer(const TMsgAlarmClock& msg);
	void onState(const TMsgIntObjState& msg);
	void onExplosion(const TMsgDamage & msg);

public:
  void load(const json& j, TEntityParseContext& ctx);
  void debugInMenu();

	static void registerMsgs();
};