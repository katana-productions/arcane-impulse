#include "mcv_platform.h"
#include "comp_particle_move.h"
#include "components/common/comp_transform.h"
#include "engine.h"

DECL_OBJ_MANAGER("particle_move", TCompSystemParticleWorldMove);

void TCompSystemParticleWorldMove::debugInMenu() {
	ImGui::DragFloat3("Offset", &offset.x, 0.1f, -10.f, 10.f);
	ImGui::Checkbox("enabled", &enabled);
}

void TCompSystemParticleWorldMove::load(const json& j, TEntityParseContext& ctx) {
	if (j.find("system_particles") != j.end()) {
		json jsystems = j["system_particles"];
		for (json::iterator it = jsystems.begin(); it != jsystems.end(); ++it) {
			std::string filename = it.value().get<std::string>();
			v_filename.push_back(filename);
		}
	}
	offset = loadVEC3(j, "offset", VEC3(0, 0, 0));
	isActive = j.value("isActive", true);

	trailDirCorrection = j.value("trailDirCorrection", trailDirCorrection);
}

void TCompSystemParticleWorldMove::registerMsgs() {
	DECL_MSG(TCompSystemParticleWorldMove, TMsgEntityCreated, onEntityCreated);
}

void TCompSystemParticleWorldMove::onEntityCreated(const TMsgEntityCreated& msg) {
	if(isActive) ActivateParticles();
}

void TCompSystemParticleWorldMove::ActivateParticles()
{
	TCompTransform* c_trans = get<TCompTransform>();
	c_trans->setRotation(VEC4(0, 0, 0, 1));
	originalPos = c_trans->getPosition();
	lastPos = originalPos;
	player = EngineBasics.GetPlayerEntity();
	VEC3 player_pos;
	if(player.isValid())
		player_pos = EngineBasics.GetPlayerPos(player);

	std::vector<CHandle> new_group_systems;
	for (auto filename : v_filename) {
		new_group_systems = EngineParticles.CreateLogicSeparatedSystemParticles(filename.c_str(), c_trans);
		for (auto new_system : new_group_systems) {
			h_group_systems.push_back(new_system);
		}
	}
	isActive = true;
	enabled = true;
}

void TCompSystemParticleWorldMove::update(float delta) {
	if (!isActive) return;

	TCompTransform* c_trans = get<TCompTransform>();
	if (!c_trans || !enabled) {
		for (auto h_loaded : h_group_systems) {
			if (!h_loaded.isValid()) return;

			CEntity* e_loaded = h_loaded;
			TCompBuffers* c_buffer = e_loaded->get<TCompBuffers>();
			auto cte_buffer = c_buffer->getCteByName("TCtesParticles");
			TCtesParticles* particles_buffer = dynamic_cast<TCtesParticles*>(cte_buffer);

			particles_buffer->emitter_looping = false;
			EngineParticles.UpdateConstantBuffer(cte_buffer);
		}
		EngineParticles.RegisterToDestroyParticles(h_group_systems);

		CHandle(this).destroy();
		return;
	}
	VEC3 pos = c_trans->getPosition();
	for (auto h_loaded : h_group_systems) {
		if (!h_loaded.isValid()) continue;
	
		CEntity* e_loaded = h_loaded;
	
		TCompBuffers* c_buffer = e_loaded->get<TCompBuffers>();
		auto cte_buffer = c_buffer->getCteByName("TCtesParticles");
		TCtesParticles* particles_buffer = dynamic_cast<TCtesParticles*>(cte_buffer);

		if(particles_buffer->emitter_billboard)
			particles_buffer->emitter_offset_center = c_trans->getPosition();
		else {
			VEC3 deltaPos = c_trans->getPosition() - originalPos;
			particles_buffer->emitter_offset_center = deltaPos;
		}

		if (trailDirCorrection) {
			VEC3 new_emitter_dir = lastPos - pos;
			new_emitter_dir.Normalize();
			particles_buffer->emitter_dir = new_emitter_dir;
		}

		particles_buffer->emitter_offset_center += offset;
		EngineParticles.UpdateConstantBuffer(cte_buffer);
	}
	lastPos = pos;
}

TCompSystemParticleWorldMove::~TCompSystemParticleWorldMove()
{
	for (auto h_loaded : h_group_systems) {
		if (!h_loaded.isValid()) continue;

		CEntity* e_loaded = h_loaded;
		TCompBuffers* c_buffer = e_loaded->get<TCompBuffers>();
		auto cte_buffer = c_buffer->getCteByName("TCtesParticles");
		TCtesParticles* particles_buffer = dynamic_cast<TCtesParticles*>(cte_buffer);

		particles_buffer->emitter_looping = false;
		EngineParticles.UpdateConstantBuffer(cte_buffer);
	}
	EngineParticles.RegisterToDestroyParticles(h_group_systems);
}