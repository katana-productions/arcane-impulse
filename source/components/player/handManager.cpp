#include "mcv_platform.h"
#include "handManager.h"
#include "engine.h"
#include "components/common/comp_render.h"
#include "components/common/comp_fsm.h"

DECL_OBJ_MANAGER("hand_manager", TCompHandManager);

CHandle TCompHandManager::handle;


void TCompHandManager::registerMsgs()
{
	DECL_MSG(TCompHandManager, TMsgAlarmClock, onAlarmRecieve);
	DECL_MSG(TCompHandManager, TMsgEntitiesGroupCreated, onEntitiesCreated);
}

void TCompHandManager::load(const json & j, TEntityParseContext & ctx)
{
	handle = ctx.current_entity;
}

void TCompHandManager::onEntitiesCreated(const TMsgEntitiesGroupCreated & msg) {}

void TCompHandManager::onAlarmRecieve(const TMsgAlarmClock & msg)
{
	CEntity* e_right_hand = getEntityByName("RightHand");
	TCompFSM* cFsm_right = e_right_hand->get<TCompFSM>();
	CEntity* e_left_hand = getEntityByName("LeftHand");
	TCompFSM* cFsm_left = e_left_hand->get<TCompFSM>();
	if (msg.id == endPull)
	{
		currPull--;
		if (currPull == 0) {
			if (cFsm_left) cFsm_left->changeVariable("pull", 0, true);
			if (cFsm_right) cFsm_right->changeVariable("pull", 0, true);
		}
	}
	else if (msg.id == endPush)
	{
		currPush--;
		if (currPush == 0) {
			if (cFsm_left) cFsm_left->changeVariable("push", 0, true);
			if (cFsm_right) cFsm_right->changeVariable("push", 0, true);
		}
	}
}

void TCompHandManager::setPullBurst()
{
	CHandle myHandle = CHandle(this);
	EngineAlarms.AddAlarm(burstTime, endPull, myHandle);
	currPull++;
}

void TCompHandManager::setPushBurst()
{
	CHandle myHandle = CHandle(this);
	EngineAlarms.AddAlarm(burstTime, endPush, myHandle);
	currPush++;
}