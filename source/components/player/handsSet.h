#pragma once
#include "components/common/comp_base.h"
#include "entity/entity.h"

class TCompHandSet : public TCompBase {
	DECL_SIBLING_ACCESS();
private:
	bool lockedToCamera = true;
	float frontOffset = 0.5f;
	float lateralOffset = 0.0f;
	float extraHeight = -0.4f;
	float extraPitch = 1.0f;
public:
	float new_frontOffset = 0.5f;
	float new_lateralOffset = 0.0f;
	float new_extraHeight = -0.4f;
	float new_extraPitch = 1.0f;
	float speed = 0.35f;

	void update(float dt);
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void changePositions(float dt);

	float getFrontOffset() {
		return frontOffset;
	}
	void setFrontOffset(float val) {
		new_frontOffset = val;
	}
	float getLateralOffset() {
		return lateralOffset;
	}
	void setLateralOffset(float val) {
		new_lateralOffset = val;
	}
	float getHeight() {
		return extraHeight;
	}
	void setHeight(float val) {
		new_extraHeight = val;
	}
	float getPitch() {
		return extraPitch;
	}
	void setPitch(float val) {
		new_extraPitch = val;
	}
	void setLockCamera(bool b) {
		lockedToCamera = b;
	}
};