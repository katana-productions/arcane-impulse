#pragma once
#include "components/common/comp_transform.h"
#include "components/controllers/comp_char_controller.h"
#include "components/controllers/camMovement.h"

class TCompPlayerMove : public TCompBase
{
	DECL_SIBLING_ACCESS();
	void onAlarmRecieve(const TMsgAlarmClock& msg);
	void onTriggerEnter(const TMsgEntityTriggerEnter& msg);
	void onEntityCreated(const TMsgEntityCreated& msg);

public:
	static void registerMsgs();
	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);
	void InputManipulation();
	void JumpHandle();
	void RotateToCamForward();
	void CaptureInput();
	void debugInMenu();
	void SetActive(const bool& isActive);
	void setHasJumped(bool jumped) { hasJumped = jumped; }
	bool getHasJumped() { return hasJumped; }
	void setPaused(bool paused) { isPaused = paused; }

	bool isDead = false;

private:
	//Config vars that can be modified in JSON config file
	float runMult = 1.75f;
	float smoothSpeedMult = 10.0f;
	float airSpeedMult = 0.8f;
	float airInputMult = 3.0;
	float lowJumpMult = 0.75f;
	float maxJumpDirInf = 0.2f;
	float groundInputSmooth = 10.0f;
	float lastInputSmooth = 0.75f;
	float coyoteTime = 0.1f;

	//Internal stuff that shouldn't be modified
	bool isPaused = false;
	bool isGrounded = false;
	bool prevIsGrounded = true;
	bool canJump = true;
	bool canCoyoteJump = false;
	bool canJumpHigher = false;
	float timeBetweenJumps = 0.5f;
	float deltaTime = 0.0f;
	float currSpeedMult = 1.0f;
	float targetSpeedMult = 1.0f;
	float yaw, pitch;
	TCompCharController* charController;
	TCompTransform* myTransform;
	VEC3 input = VEC3(0, 0, 0);
	VEC3 lastGroundedInput = VEC3(0, 0, 0);
	bool active = true;
	CHandle camMovementH;
	TCompCamMove* camMove;
	bool hasJumped = false;
};
