#include "mcv_platform.h"
#include "comp_player_magic.h"
#include "comp_player_magic_particles.h"
#include "components/common/comp_name.h"
#include "components/common/comp_collider.h"
#include "components/common/comp_transform.h"
#include "components/common/comp_aabb.h"
#include "components/player/comp_player_stats.h"
#include "components/player/handManager.h"
#include "physics/module_physics.h"
#include "engine.h"
#include "input/input.h"
#include "utils/utils.h"
#include "components/common/comp_fsm.h"
#include "components/postfx/comp_render_vignette.h"
#include "components/shaders/comp_magic_outline.h"
#include "components/common/comp_camera.h"

DECL_OBJ_MANAGER("player_magic", TCompPlayerMagic);

using namespace physx;

void TCompPlayerMagic::debugInMenu()
{
	std::string name = "";
	if (pullingObj)
	{
		CEntity *pulledObjEntity = pulledObjHandle;
		name = pulledObjEntity->getName();
	}
	ImGui::Text("PulledObj Name: %s", name.c_str());
	ImGui::Checkbox("Pulling Obj:", &pullingObj);
	ImGui::DragFloat("Max Magic Dist", &maxDistanceMagic, 0.1f, 0.0f, 30.0f);
	ImGui::DragFloat("Min Magic Dist", &minDistanceMagic, 0.1f, 0.0f, 30.0f);
	ImGui::DragFloat("Curr Magic Dist", &currentDistanceMagic, 0.1f, 0.0f, 30.0f);
	ImGui::DragFloat("Force Push Obj", &pushObjForce, 1.0f, 0.0f, 1000.0f);
	ImGui::DragFloat("Force Pull Obj", &pullObjForce, 1.0f, 0.0f, 1000.0f);
	ImGui::DragFloat("Force Push Wall Base", &pushWallForceBase, 1.0f, 0.0f, 100.0f);
	ImGui::DragFloat("Force Push Wall Extra", &pushWallForceExtra, 1.0f, 0.0f, 100.0f);
	ImGui::DragFloat("Force Push Wall Min Factor", &pushWallMinFactor, 0.01f, 0.0f, 1.0f);
	ImGui::DragFloat("Force Pull Wall Base", &pullWallForceBase, 1.0f, 0.0f, 1000.0f);
	ImGui::DragFloat("Force Pull Wall Extra", &pullWallForceExtra, 1.0f, 0.0f, 1000.0f);
	ImGui::DragFloat("Gravity Recover Time", &gravityRecoverT, 0.2f, 0.0f, 5.0f);
	ImGui::DragFloat("Min Dist Up Help", &minDistUpHelp, 0.5f, -20.0f, 10.0f);
	ImGui::DragFloat("Max Dist Up Help", &maxDistUpHelp, 0.5f, 0.0f, 50.0f);
	ImGui::DragFloat("Up Help Amount", &upHelpAmount, 0.5f, 0.0f, 25.0f);
	bool p1 = ImGui::DragFloat("Push Cooldown", &pushCooldown, 0.1f, 0.1f, 3.0f);
	bool p2 = ImGui::DragFloat("Pull Cooldown", &pullCooldown, 0.1f, 0.1f, 3.0f);
	ImGui::DragFloat3("RayOrigin", &origin.x, 1.0f, -10.0f, 10.0f);
	ImGui::DragFloat3("RayDir", &dir.x, 1.0f, -10.0f, 10.0f);
	ImGui::DragFloat3("Pull Move Target", &target.x, 0.1f, -10.0f, 10.0f);
	ImGui::DragFloat("ChromA InitialV", &chromAInitialV, 0.01f, 0.0f, 1.0f);
	ImGui::DragFloat("ChromA anim T", &chromATime, 0.01f, 0.0f, 1.5f);
	ImGui::DragFloat("pullFov", &pullFov, 0.5f, 0.0f, 180.0f);
	ImGui::DragFloat("pushFov", &pushFov, 0.5f, 0.0f, 180.0f);

	if (p1 or p2)
	{
		CHandle h_ui = getEntityByName("UI");
		CEntity* e_ui = h_ui;
		TCompPlayerStats *plStats = get<TCompPlayerStats>();
		TMsgInitUI msg;
		msg.max_health = plStats->getHealth();
		msg.pull_cooldown = pullCooldown;
		msg.push_cooldown = pushCooldown;
		e_ui->sendMsg(msg);
		plStats->setPullCdw(pullCooldown);
		plStats->setPushCdw(pushCooldown);
	}
}

void TCompPlayerMagic::renderDebug()
{
	drawWiredSphere(origin + dir * currentDistanceMagic, 0.3f, VEC4(1, 0, 0, 1));
	drawWiredSphere(target, 0.3f, VEC4(0, 1, 0, 1));
}

void TCompPlayerMagic::registerMsgs()
{
	DECL_MSG(TCompPlayerMagic, TMsgEntityCreated, init);
	DECL_MSG(TCompPlayerMagic, TMsgAlarmClock, onAlarmRecieve);
}

void TCompPlayerMagic::onAlarmRecieve(const TMsgAlarmClock& msg) {
	if (msg.id == 11) magic = true;
}

void TCompPlayerMagic::init(const TMsgEntityCreated &msg)
{
	CEntity *camera = (CEntity*)getEntityByName("Camera");
	myTransform = camera->get<TCompTransform>();
	plController = get<TCompCharController>();
	plStats = get<TCompPlayerStats>();
	CEntity *entityUI = (CEntity *)getEntityByName("UI");
	plUI = entityUI->get<TCompUI>();
	vignettePPHandle = TCompVignette::vignetteHandle;
	currentDistanceMagic = maxDistanceMagic;
	cameraShaker = (CEntity*)getEntityByName("CameraShaker");
}

void TCompPlayerMagic::load(const json& j, TEntityParseContext& ctx)
{
	maxDistanceMagic = j.value("maxDistanceMagic", 15.0f);
	minDistanceMagic = j.value("minDistanceMagic", 9.0f);
	pullObjForce = j.value("pullObjForce", 1.0f);
	pullWallForceBase = j.value("pullWallForceBase", 25.0f);
	pullWallForceExtra = j.value("pullWallForceExtra", 25.0f);
	pushObjForce = j.value("pushObjForce", 40.0f);
	pushWallForceBase = j.value("pushWallForceBase", 25.0f);
	pushWallForceExtra = j.value("pushWallForceExtra", 25.0f);
	pushWallMinFactor = j.value("pushWallMinFactor", 0.25f);
	pullCooldown = j.value("pullCooldown", 0.5f);
	pushCooldown = j.value("pushCooldown", 0.5f);
	gravityRecoverT = j.value("gravityRecoverT", 1.0f);
	minDistUpHelp = j.value("minDistUpHelp", minDistUpHelp);
	maxDistUpHelp = j.value("maxDistUpHelp", maxDistUpHelp);
	upHelpAmount = j.value("upHelpAmount", upHelpAmount);
	reticleColorNormal = loadColor(j, "reticleColorNormal");
	reticleColorYes = loadColor(j, "reticleColorYes");
	reticleColorNo = loadColor(j, "reticleColorNo");
	chromAInitialV = j.value("chromAInitialV", chromAInitialV);
	chromATime = j.value("chromATime", chromATime);
	pullFov = j.value("pullFov", pullFov);
	pushFov = j.value("pushFov", pushFov);

	particles_push_player = j.value("ParticlePushPlayer", particles_push_player);
	particles_pull_player = j.value("ParticlePullPlayer", particles_pull_player);

	particles_push_object = j.value("ParticlePushObject", particles_push_object);
	particles_pull_object = j.value("ParticlePullObject", particles_pull_object);

	particles_push_spot = j.value("ParticlePushSpot", particles_push_spot);
	particles_pull_spot = j.value("ParticlePullSpot", particles_pull_spot);

	myTransform = nullptr;
	plController = nullptr;
	plStats = nullptr;
	plUI = nullptr;

	//Set Filtering data for raycast

	//IN filter data
	filterDataPull = PxQueryFilterData();
	//Which groups are active when the query is made
	filterDataPull.data.word0 = ~(CModulePhysics::FilterGroup::Player | CModulePhysics::FilterGroup::Triggers); //All minus Triggers
	filterDataPush = PxQueryFilterData();
	//Which groups are active when the query is made
	filterDataPush.data.word0 = ~(CModulePhysics::FilterGroup::Player | CModulePhysics::FilterGroup::Triggers | CModulePhysics::FilterGroup::MagicOnly); //All minus Triggers
	//Hit flags
	hitFlags = PxHitFlag::eDEFAULT;
	//queryFlags
	queryFlags = PxQueryFlag::eDYNAMIC | PxQueryFlag::eSTATIC;

	checkHitGnome.resize(6);
	checkHitStatus.resize(6);
}

void TCompPlayerMagic::update(float delta)
{
	if (!enabled || isPaused) return;

	dt = delta;

	RaySetup(myTransform->getPosition(), myTransform->getFront());

	SetNewDistanceMagic();

	bool hitStatus = EnginePhysics.Raycast(origin, dir, currentDistanceMagic, hit, queryFlags, hitFlags, filterDataPull);
	distanceHit = hit.distance;

	HandleHit(hitStatus, &hit);

	if (timeStopFovUpdate >= Time.current)
	{
		TCompCamera* cam = cameraShaker->get<TCompCamera>();
		cam->setProjectionParams(deg2rad(currFovValue), 0, 0, false);
	}
}

void TCompPlayerMagic::SetNewDistanceMagic()
{
	float cameraDotVertical = 1 - abs(myTransform->getFront().Dot(VEC3(0, 1, 0)));
	currentDistanceMagic = minDistanceMagic + (cameraDotVertical * (maxDistanceMagic - minDistanceMagic));
}

void TCompPlayerMagic::HandleHit(bool hitStatus, physx::PxRaycastHit *hit)
{
	CHandle targetHdl;

	if (hitStatus)
	{
		targetHdl.fromVoidPtr(hit->actor->userData);
		targetHdl = targetHdl.getOwner();
	}
	else
	{
		CHandle h_correction = checkDoubleRayGoblin();
		if (h_correction.isValid()) {
			targetHdl = h_correction;
			hitStatus = true;
		}
	}

	SetReticleColor(hitStatus, targetHdl);
	ActivateOutlines(hitStatus, targetHdl);

	if (!pullingObj and hitStatus) PullPushPlayer(targetHdl, hit);
	PullPushObject(targetHdl, hit);
}

void TCompPlayerMagic::SetReticleColor(bool hitStatus, CHandle targetHdl)
{
	if (pullingObj && (pullObjUnlocked || pushObjUnlocked || pullWallUnlocked || pushWallUnlocked))
		plUI->SetCrosshair(reticleColorYes, true, CrosshairType::Objects);
	else if (!hitStatus)
		plUI->SetCrosshair(reticleColorNormal, false, CrosshairType::Neutral);
	else
	{
		if (targetHdl.isValid()) {
			//Surfaces
			if (pullWallUnlocked || pushWallUnlocked) {
				if (isSurface(targetHdl) and isInteractable(targetHdl))
					plUI->SetCrosshair(reticleColorYes, true, CrosshairType::Surfaces);
				else if (isSurface(targetHdl))
					plUI->SetCrosshair(reticleColorNo, true, CrosshairType::Surfaces);
			}

			if (pullObjUnlocked || pushObjUnlocked) {
				//Objects
				if (isObject(targetHdl) and isInteractable(targetHdl))
					plUI->SetCrosshair(reticleColorYes, true, CrosshairType::Objects);
				else if (isObject(targetHdl))
					plUI->SetCrosshair(reticleColorNo, true, CrosshairType::Objects);

				//Enemies
				else if (isEnemy(targetHdl) and isInteractable(targetHdl))
					plUI->SetCrosshair(reticleColorYes, true, CrosshairType::Enemies);
				else if (isEnemy(targetHdl))
					plUI->SetCrosshair(reticleColorNo, true, CrosshairType::Enemies);
			}
		}
	}
}

void TCompPlayerMagic::ActivateOutlines(bool hitStatus, CHandle targetHdl)
{
	if (!pullObjUnlocked and !pushObjUnlocked) return;
	if (pulledObjHandle.isValid())
	{
		CEntity *e = pulledObjHandle;
		TCompMagicOutline *me = e->get< TCompMagicOutline >();
		if (me != nullptr and plStats->getCanPull())
			me->setEnabled(true);
	}
	else if (hitStatus)
	{
		if (targetHdl.isValid())
		{
			CEntity *t = targetHdl;
			CEntity *c = currentTargetHandle;
			if (c == nullptr or t == c)
			{
				// Turn on outline for entity, store it's handle
				currentTargetHandle = targetHdl;
				CEntity *e = currentTargetHandle;
				TCompMagicOutline *m = e->get< TCompMagicOutline >();
				if (m != nullptr and plStats->getCanPull())
					m->setEnabled(true);
			}
			if (t != c)
			{
				// Turn off outline for current entity, update handle and turn on new outline
				CEntity *e = currentTargetHandle;
				TCompMagicOutline *mc = e->get< TCompMagicOutline >();
				if (mc != nullptr)
					mc->setEnabled(false);

				currentTargetHandle = targetHdl;
				e = targetHdl;
				TCompMagicOutline *mt = e->get< TCompMagicOutline >();
				if (mt != nullptr and plStats->getCanPull())
					mt->setEnabled(true);
			}
		}
	}
	else
	{
		// Turn off outline for current entity, reset handle
		if (currentTargetHandle.isValid())
		{
			CEntity *e = currentTargetHandle;
			TCompMagicOutline *m = e->get< TCompMagicOutline >();
			if (m != nullptr)
				m->setEnabled(false);
			currentTargetHandle = CHandle();
		}
	}
}

void TCompPlayerMagic::PullPushObject(CHandle targetHdl, physx::PxRaycastHit *hit)
{
	CEntity* e_left_hand = getEntityByName("LeftHand");
	TCompFSM* cFsm_left = e_left_hand->get<TCompFSM>();

	CEntity* e_dummy_left_hand = getEntityByName("DummyLeftHandParticle");
	TCompPlayerPowerParticles* magic_power_left = e_dummy_left_hand->get< TCompPlayerPowerParticles>();

	CEntity* e_right_hand = getEntityByName("RightHand");
	TCompFSM* cFsm_right = e_right_hand->get<TCompFSM>();

	CEntity* e_dummy_right_hand = getEntityByName("DummyRightHandParticle");
	TCompPlayerPowerParticles* magic_power_right = e_dummy_right_hand->get< TCompPlayerPowerParticles>();

	//Push
	if (pushObjUnlocked and EngineInput["Push"].justPressed())
	{
		//if (pullingObj and plStats->useMana(pushObjMana))
		if (pullingObj and plStats->usePush())
		{
			if (cFsm_right && cFsm_left) {
				cFsm_left->changeVariable("pull", 0, true);
				cFsm_right->changeVariable("pull", 0, true);
				cFsm_left->changeVariable("push", 1, true);
				cFsm_right->changeVariable("push", 1, true);
			}
			magic_power_right->desactivatePlayerPower();
			magic_power_left->activatePlayerPower(TCompPlayerPowerParticles::EnumPlayerObject::eObject);

			PushPulledObj();
			pullingObj = false;

			CreateSpotMagicPS(hit, 2);
		}
		//else if (targetHdl.isValid() and isInteractable(targetHdl) and (isObject(targetHdl) or isEnemy(targetHdl)) and plStats->useMana(pushObjMana))
		else if (targetHdl.isValid() and isInteractable(targetHdl) and (isObject(targetHdl) or isEnemy(targetHdl)) and plStats->usePush())
		{
			if (cFsm_left) cFsm_left->changeVariable("push", 1, true);
			if (cFsm_right) cFsm_right->changeVariable("push", 1, true);
			magic_power_left->activatePlayerPower(TCompPlayerPowerParticles::EnumPlayerObject::eObject);

			PushTargetObj(targetHdl);
			pullingObj = false;

			CreateSpotMagicPS(hit, 2);
		}
	}
	//Pull Start
	else if (pullObjUnlocked and EngineInput["Pull"].justPressed())
	{
		CEntity *obj = targetHdl;
		CEntity *grnd = plController->groundHandle;
		if (plController->groundHandle.isValid() && targetHdl.isValid() &&
			grnd == obj &&
			isInteractable(targetHdl) && (isObject(targetHdl) || isEnemy(targetHdl))) //When on top of the object you are pulling
		{
			if (cFsm_right) cFsm_right->changeVariable("pull", 1, true);
			if (cFsm_left) cFsm_left->changeVariable("pull", 1, true);
			PullTargetObj(targetHdl);
			pullingObj = false;

			CreateSpotMagicPS(hit, 1);
		}
		else if (!pullingObj and targetHdl.isValid() and isInteractable(targetHdl) and (isObject(targetHdl) or isEnemy(targetHdl)) and plStats->usePull())
		{
			if (cFsm_left) cFsm_left->changeVariable("pull", 1, true);
			if (cFsm_right) cFsm_right->changeVariable("pull", 1, true);
			magic_power_right->activatePlayerPower(TCompPlayerPowerParticles::EnumPlayerObject::eObject);
			StartPullingObj(targetHdl);
			pullingObj = true;

			CreateSpotMagicPS(hit, 1);
		}
	}
	//Pulling
	else if (pullObjUnlocked and EngineInput["Pull"].isPressed())
	{
		if (pullingObj)
		{
			CHandle groundHdl = groundPullCheck();
			CEntity *grnd = groundHdl;
			CEntity *pulledObjEntity = pulledObjHandle;
			if (groundHdl.isValid() && pulledObjHandle.isValid() and grnd == pulledObjEntity and
				isInteractable(pulledObjHandle) and (isObject(pulledObjHandle) or isEnemy(pulledObjHandle))) //When on top of the object you are pulling
			{
				ReleasePullObj();
				if (cFsm_right) cFsm_right->changeVariable("pull", 0, true);
				if (cFsm_left) cFsm_left->changeVariable("pull", 0, true);
				magic_power_right->desactivatePlayerPower();
			}
			else PullingObj();
		}
		else {
			if (cFsm_left) cFsm_left->changeVariable("pull", 0, true);
			if (cFsm_right) cFsm_right->changeVariable("pull", 0, true);
			magic_power_right->desactivatePlayerPower();
		}
	}
	//Pull Release
	if (pullObjUnlocked and EngineInput["Pull"].justReleased())
	{
		if (pullingObj) {
			ReleasePullObj();
			if (cFsm_left) cFsm_left->changeVariable("pull", 0, true);
			if (cFsm_right) cFsm_right->changeVariable("pull", 0, true);
			magic_power_right->desactivatePlayerPower();
		}
		else
		{
			if (cFsm_left) cFsm_left->changeVariable("pull", 0, true);
			if (cFsm_right) cFsm_right->changeVariable("pull", 0, true);
		}
	}
}

void TCompPlayerMagic::FreezePulledObj()
{
	CEntity *pulledObjEntity = pulledObjHandle;
	TCompTransform *pulledObjTrans = pulledObjEntity->get<TCompTransform>();
	TCompCollider *pulledObjCol = pulledObjEntity->get<TCompCollider>();
	pulledObjCol->changeSleepState(true);
}

void TCompPlayerMagic::PushPulledObj()
{
	CEntity *pulledObjEntity = pulledObjHandle;
	if (!pulledObjEntity) return;
	TCompTransform *pulledObjTrans = pulledObjEntity->get<TCompTransform>();
	TCompCollider *pulledObjCol = pulledObjEntity->get<TCompCollider>();
	PxRigidDynamic *pulledObjRb = pulledObjCol->actor->is<PxRigidDynamic>();
	TCompCharController *charEnemy = pulledObjEntity->get<TCompCharController>();

	//True aiming direction
	VEC3 aimingDir;
	PxRaycastHit aimingHit;
	CHandle targetHdl;

	bool hitStatus = EnginePhysics.Raycast(origin, dir, 100, aimingHit, queryFlags, hitFlags, filterDataPush);
	if (hitStatus) targetHdl.fromVoidPtr(aimingHit.actor->userData);

	if (targetHdl.isValid() && targetHdl != pulledObjHandle && targetHdl.getOwner() != pulledObjHandle)
		aimingDir = PXVEC3_TO_VEC3(aimingHit.position) - pulledObjTrans->getPosition();
	else
		aimingDir = (origin + (dir * 100)) - pulledObjTrans->getPosition();
	aimingDir.Normalize();

	pulledObjCol->actor->setActorFlag(PxActorFlag::eDISABLE_GRAVITY, false);
	if (charEnemy)
	{
		charEnemy->addExternForcePower(aimingDir * pushObjForce);
	}
	else {
		pulledObjRb->clearForce();
		//pulledObjRb->setLinearVelocity(PxZero);
		pulledObjRb->addForce(VEC3_TO_PXVEC3(aimingDir) * pushObjForce, PxForceMode::eIMPULSE);
	}

	CEntity* entity = (CEntity*)TCompHandManager::handle;
	TCompHandManager* handM = entity->get<TCompHandManager>();
	handM->setPushBurst();

	TMsgIntObjState msg; msg.state = ObjState::Pushed;
	pulledObjEntity->sendMsg(msg);

	TCompMagicOutline *me = pulledObjEntity->get< TCompMagicOutline >();
	if (me != nullptr)
		me->setEnabled(false);

	pulledObjHandle = CHandle();
}

void TCompPlayerMagic::PushTargetObj(CHandle targetHdl)
{
	if (!targetHdl.isValid()) return;

	CEntity *ent = targetHdl;
	TCompCollider *col = ent->get<TCompCollider>();
	PxRigidDynamic *rb = col->actor->is<PxRigidDynamic>();
	TCompCharController *charEnemy = ent->get<TCompCharController>();

	if (charEnemy)
	{
		charEnemy->addExternForcePower(dir * pushObjForce);
	}
	else {
		rb->clearForce();
		rb->setLinearVelocity(PxZero);
		rb->addForce(VEC3_TO_PXVEC3(dir) * pushObjForce, PxForceMode::eIMPULSE);
	}

	CEntity* entity = (CEntity*)TCompHandManager::handle;
	TCompHandManager* handM = entity->get<TCompHandManager>();
	handM->setPushBurst();

	TMsgIntObjState msg;
	msg.state = ObjState::Pushed;
	ent->sendMsg(msg);
}

void TCompPlayerMagic::PullTargetObj(CHandle targetHdl)
{
	if (!targetHdl.isValid()) return;

	CEntity *ent = targetHdl;
	TCompCollider *col = ent->get<TCompCollider>();
	PxRigidDynamic *rb = col->actor->is<PxRigidDynamic>();
	TCompCharController *charEnemy = ent->get<TCompCharController>();

	if (charEnemy) {
		charEnemy->addExternForcePower(dir * pullObjForce);
	}
	else if (rb) {
		rb->clearForce();
		rb->setLinearVelocity(PxZero);
		rb->addForce(VEC3_TO_PXVEC3(dir) * pullObjForce, PxForceMode::eIMPULSE);
	}

	CEntity* entity = (CEntity*)TCompHandManager::handle;
	TCompHandManager* handM = entity->get<TCompHandManager>();
	handM->setPullBurst();
}

void TCompPlayerMagic::StartPullingObj(CHandle targetHdl)
{
	if (!targetHdl.isValid()) return;

	pulledObjHandle = targetHdl;
	CEntity *ent = pulledObjHandle;

	TCompLocalAABB *aabb = ent->get<TCompLocalAABB>();
	bbhalfextents = aabb->half_extents;

	// This number should be bigger for bigger objects
	pullObjFloatDistance = bbhalfextents.Length() * 2.0f + 0.75f;

	TCompCollider *pulledObjCol = ent->get<TCompCollider>();
	pulledObjCol->awakeWithPower();
	pulledObjCol->actor->setActorFlag(PxActorFlag::eDISABLE_GRAVITY, true);

	//CEntity* entity = (CEntity*)TCompHandManager::handle;
	//TCompHandManager* handM = entity->get<TCompHandManager>();

	angularVel.x = randomRange(-1.0f, 1.0f);
	angularVel.y = randomRange(-1.0f, 1.0f);
	angularVel.z = randomRange(-1.0f, 1.0f);

	TMsgIntObjState msg; msg.state = ObjState::Pulled;
	ent->sendMsg(msg);
}

void TCompPlayerMagic::PullingObj()
{
	if (!pulledObjHandle.isValid())
	{
		CEntity* entity = (CEntity*)TCompHandManager::handle;
		TCompHandManager* handM = entity->get<TCompHandManager>();
		pullingObj = false;
		return;
	}
	CEntity *pulledObjEntity = pulledObjHandle;
	TCompTransform *pulledObjTrans = pulledObjEntity->get<TCompTransform>();
	TCompCollider *pulledObjCollider = pulledObjEntity->get<TCompCollider>();
	PxRigidDynamic *pulledObjRb = pulledObjCollider->actor->is<PxRigidDynamic>();
	TCompCharController *charEnemy = pulledObjEntity->get<TCompCharController>();

	//if (enoughMana and isInteractable(pulledObjHandle))
	if (isInteractable(pulledObjHandle))
	{
		//Get normalized direction
		VEC3 targetDir = target - pulledObjTrans->getPosition();
		targetDir.Normalize();

		//Get distance to target pos and new pos
		float dist2target = VEC3::Distance(target, pulledObjTrans->getPosition());

		// After many tests this works
		// This formula creates the perfect curve for the force
		float l = log10(dist2target) / log10(maxDistanceMagic * maxDistanceMagic) + 0.5f;
		l = clampFloat(l, 0.0f, 1.0f);

		//Calculate displacement with velocity
		VEC3 newPos = pulledObjTrans->getPosition() + (targetDir * (pullObjForce * l * dt));
		float dist2newpos = VEC3::Distance(newPos, pulledObjTrans->getPosition());

		VEC3 Vel = (targetDir * dist2target * pullObjForce * l) + plController->currentVel * 0.8f;

		if (charEnemy)
		{
			charEnemy->addExternForcePower(Vel);
		}
		else {
			pulledObjRb->setLinearVelocity(VEC3_TO_PXVEC3(Vel));
			pulledObjRb->setAngularVelocity(VEC3_TO_PXVEC3(angularVel));
		}
	}
	else
	{
		ReleasePullObj();
	}
}

void TCompPlayerMagic::ReleasePullObj()
{
	CEntity *pulledObjEntity = pulledObjHandle;
	if (!pulledObjEntity) return;
	TCompCollider *pulledObjCol = pulledObjEntity->get<TCompCollider>();
	PxRigidDynamic *pulledObjRb = pulledObjCol->actor->is<PxRigidDynamic>();

	pulledObjCol->actor->setActorFlag(PxActorFlag::eDISABLE_GRAVITY, false);
	pulledObjRb->clearForce();

	CEntity* entity = (CEntity*)TCompHandManager::handle;
	TCompHandManager* handM = entity->get<TCompHandManager>();
	pullingObj = false;

	TMsgIntObjState msg; msg.state = ObjState::Normal;
	pulledObjEntity->sendMsg(msg);

	TCompMagicOutline *me = pulledObjEntity->get< TCompMagicOutline >();
	if (me != nullptr)
		me->setEnabled(false);

	pulledObjHandle = CHandle();
}

void TCompPlayerMagic::PullPushPlayer(CHandle targetHdl, PxRaycastHit *hit)
{
	if (!targetHdl.isValid()) return;

	CEntity* e_left_hand = getEntityByName("LeftHand");
	TCompFSM* cFsm_left = e_left_hand->get<TCompFSM>();

	CEntity* e_dummy_left_hand = getEntityByName("DummyLeftHandParticle");
	if (!e_dummy_left_hand) return;
	TCompPlayerPowerParticles* magic_power_left = e_dummy_left_hand->get< TCompPlayerPowerParticles>();

	CEntity* e_right_hand = getEntityByName("RightHand");
	TCompFSM* cFsm_right = e_right_hand->get<TCompFSM>();

	CEntity* e_dummy_right_hand = getEntityByName("DummyRightHandParticle");
	if (!e_dummy_right_hand) return;
	TCompPlayerPowerParticles* magic_power_right = e_dummy_right_hand->get< TCompPlayerPowerParticles>();

	TCompCollider *targetCol = ((CEntity*)(targetHdl))->get<TCompCollider>();

	isPlayerPullPush = false;

	if (isSurface(targetHdl) and isInteractable(targetHdl))
	{
		//Push
		if (pushWallUnlocked and EngineInput["Push"].justPressed() and plStats->usePush())
		{
			CreateSpotMagicPS(hit, 0);
			MagicVFX(false);

			if (cFsm_left) cFsm_left->changeVariable("push", 1, true);
			if (cFsm_right) cFsm_right->changeVariable("push", 1, true);

			magic_power_left->activatePlayerPower(TCompPlayerPowerParticles::EnumPlayerObject::ePlayer);
			EngineAlarms.AddAlarm(0.1, 11, CHandle(this));

			isPlayerPullPush = true;

			VEC3 dir = myTransform->getPosition() - PXVEC3_TO_VEC3(hit->position);
			float factor = 1 - clamp(magnitude(dir) / currentDistanceMagic, 0.0f, 1.0f - pushWallMinFactor);
			//dbg("PUSH Dist:%f, FactorF:%f, Force:%f\n", magnitude(dir), factor,	pushWallForceBase + (pushWallForceExtra * factor));

			dir.Normalize();
			plController->addExternForcePower(dir * (pushWallForceBase + (pushWallForceExtra * factor)));

			CEntity* entity = (CEntity*)TCompHandManager::handle;
			TCompHandManager* handM = entity->get<TCompHandManager>();
			handM->setPushBurst();
		}
		//Pull
		else if (pullWallUnlocked and EngineInput["Pull"].justPressed() and targetCol != nullptr and !targetCol->getAwakePower() and plStats->usePull())
		{
			CreateSpotMagicPS(hit, 1);
			MagicVFX(true);

			if (cFsm_left) cFsm_left->changeVariable("pull", 1, true);
			if (cFsm_right) cFsm_right->changeVariable("pull", 1, true);
			magic_power_right->activatePlayerPower(TCompPlayerPowerParticles::EnumPlayerObject::ePlayer);
			EngineAlarms.AddAlarm(0.1, 11, CHandle(this));

			isPlayerPullPush = true;

			VEC3 dir = PXVEC3_TO_VEC3(hit->position) - myTransform->getPosition();
			float factor = clamp(magnitude(dir) / currentDistanceMagic, 0.0f, 1.0f);

			if (dir.y > minDistUpHelp) {
				dir.y += clamp(dir.y / (maxDistUpHelp - minDistUpHelp), 0.0f, 1.0f) * upHelpAmount;
			}
			/*dbg("PULL Dist:%f, FactorF:%f, FactorUp:%f, Force:%f\n", magnitude(dir), factor,
				clamp(dir.y / (maxDistUpHelp - minDistUpHelp), 0.0f, 1.0f),
				pullWallForceBase + (pullWallForceExtra * factor));*/

			TCompCharController* charController = get<TCompCharController>();
			if (charController)
			{
				charController->gravity = 0.0f;
				EngineBasics.LerpElement(&charController->gravity, charController->iniGravity, gravityRecoverT);

				charController->gravity = 0.0f;
				EngineBasics.LerpElement(&charController->forceDecayAmountGround, charController->iniForceDecayAmountGround, pullCooldown);
			}

			dir.Normalize();
			plController->addExternForcePower(dir * (pullWallForceBase + (pullWallForceExtra * factor)));

			CEntity* entity = (CEntity*)TCompHandManager::handle;
			TCompHandManager* handM = entity->get<TCompHandManager>();
			handM->setPullBurst();

			charController->StopFriction();
		}
	}
}

void TCompPlayerMagic::MagicVFX(bool isPull)
{
	TCompVignette* vignetteComp = cameraShaker->get<TCompVignette>();
	vignetteComp->ChromAberrAnim(-chromAInitialV, chromATime);

	TCompCamera* cam = cameraShaker->get<TCompCamera>();
	if (isPull) currFovValue = pullFov;
	else  currFovValue = pushFov;
	cam->setProjectionParams(deg2rad(currFovValue), 0, 0, false);
	EngineBasics.LerpElement(&currFovValue, 75.0f, chromATime);
	timeStopFovUpdate = Time.current + chromATime + 0.05f;
}


void TCompPlayerMagic::CreateSpotMagicPS(physx::PxRaycastHit* hit, const int& mode)
{
	VEC3 playerPos = myTransform->getPosition() + VEC3(0.0f, 0.1f, 0.0f);
	if (mode < 2)
	{
		VEC3 hitPos = PXVEC3_TO_VEC3(hit->position);
		VEC3 vecPlayerHit = playerPos - hitPos;
		vecPlayerHit.Normalize();
		hitPos += vecPlayerHit * 0.5f;
		if (mode == 0)
		{
			EngineParticles.CreateOneShootSystemParticles(particles_push_spot.c_str(), myTransform, hitPos);
		}
		else if (mode == 1)
		{
			EngineParticles.CreateOneShootSystemParticles(particles_pull_spot.c_str(), myTransform, hitPos);
		}
	}
	else if (mode == 2)
	{
		VEC3 front = myTransform->getFront();
		playerPos = playerPos + (front * 3.0f);
		EngineParticles.CreateOneShootSystemParticles(particles_push_spot.c_str(), myTransform, playerPos);
	}
}

void TCompPlayerMagic::RaySetup(VEC3 newOrigin, VEC3 newDir)
{
	origin = newOrigin;
	dir = newDir;

	// Update target
	VEC3 offsetDir;

	if (pulledObjHandle.isValid() and isEnemy(pulledObjHandle))
	{
		offsetDir = (-0.3f * myTransform->getLeft()) + (-0.3f * myTransform->getUp()) + newDir;
		offsetDir.Normalize();
	}
	else if (pulledObjHandle.isValid())
	{
		offsetDir = (-0.3f * myTransform->getLeft()) + (0.3f * myTransform->getUp()) + newDir;
		offsetDir.Normalize();
	}

	target = origin + offsetDir * pullObjFloatDistance;
}

CHandle TCompPlayerMagic::checkDoubleRayGoblin() {
	checkHitStatus[0] = EnginePhysics.Raycast(origin + VEC3(0, 1.5, 0), dir, currentDistanceMagic, checkHitGnome[0], queryFlags, hitFlags, filterDataPull);
	checkHitStatus[1] = EnginePhysics.Raycast(origin + myTransform->getLeft() * 0.5, dir, currentDistanceMagic, checkHitGnome[1], queryFlags, hitFlags, filterDataPull);
	checkHitStatus[2] = EnginePhysics.Raycast(origin - myTransform->getLeft() * 0.5, dir, currentDistanceMagic, checkHitGnome[2], queryFlags, hitFlags, filterDataPull);
	checkHitStatus[3] = EnginePhysics.Raycast(origin - VEC3(0, 0.5, 0), dir, currentDistanceMagic, checkHitGnome[3], queryFlags, hitFlags, filterDataPull);
	checkHitStatus[4] = EnginePhysics.Raycast(origin + VEC3(0, 1.0, 0) + myTransform->getLeft() * 0.5, dir, currentDistanceMagic, checkHitGnome[4], queryFlags, hitFlags, filterDataPull);
	checkHitStatus[5] = EnginePhysics.Raycast(origin + VEC3(0, 1.0, 0) - myTransform->getLeft() * 0.5, dir, currentDistanceMagic, checkHitGnome[5], queryFlags, hitFlags, filterDataPull);
	CHandle targetHdl;
	CHandle correctHandle;
	float minimumDistance = maxDistanceMagic;
	for (int i = 0; i < checkHitStatus.size(); i++) {
		if (checkHitStatus[i]) {
			targetHdl.fromVoidPtr(checkHitGnome[i].actor->userData);
			targetHdl = targetHdl.getOwner();
			if (targetHdl.isValid()) {
				CEntity* e = targetHdl;
				if (e && checkHitGnome[i].distance < minimumDistance) {
					correctHandle == targetHdl;
					minimumDistance = checkHitGnome[i].distance;
				}
			}
		}
	}
	return correctHandle;
}

CHandle TCompPlayerMagic::groundPullCheck()
{
	CHandle groundHdl = CHandle();
	PxRaycastHit groundRayHit;
	if (
		EnginePhysics.Raycast(PXVEC3_TO_VEC3(plController->GetFootPos()) + VEC3(0.0f, 0.2f, 0.0f),
			VEC3(0.0f, -1.0f, 0.0f), 0.3f, groundRayHit, queryFlags, hitFlags, filterDataPull) or

		EnginePhysics.Raycast(PXVEC3_TO_VEC3(plController->GetFootPos()) + VEC3(0.0f, 0.0f, 0.0f) + (myTransform->getFront() * 0.45f),
			VEC3(0.0f, -1.0f, 0.0f), 0.6f, groundRayHit, queryFlags, hitFlags, filterDataPull) or

		EnginePhysics.Raycast(PXVEC3_TO_VEC3(plController->GetFootPos()) + VEC3(0.0f, 0.0f, 0.0f) - (myTransform->getFront() * 0.45f),
			VEC3(0.0f, -1.0f, 0.0f), 0.6f, groundRayHit, queryFlags, hitFlags, filterDataPull) or

		EnginePhysics.Raycast(PXVEC3_TO_VEC3(plController->GetFootPos()) + VEC3(0.0f, 0.0f, 0.0f) + (myTransform->getLeft() * 0.45f),
			VEC3(0.0f, -1.0f, 0.0f), 0.6f, groundRayHit, queryFlags, hitFlags, filterDataPull) or

		EnginePhysics.Raycast(PXVEC3_TO_VEC3(plController->GetFootPos()) + VEC3(0.0f, 0.0f, 0.0f) - (myTransform->getLeft() * 0.45f),
			VEC3(0.0f, -1.0f, 0.0f), 0.6f, groundRayHit, queryFlags, hitFlags, filterDataPull) or

		EnginePhysics.Raycast(PXVEC3_TO_VEC3(plController->GetFootPos()) + VEC3(0.0f, 0.0f, 0.0f) + (myTransform->getFront() * 0.45f) + (myTransform->getLeft() * 0.45f),
			VEC3(0.0f, -1.0f, 0.0f), 0.6f, groundRayHit, queryFlags, hitFlags, filterDataPull) or

		EnginePhysics.Raycast(PXVEC3_TO_VEC3(plController->GetFootPos()) + VEC3(0.0f, 0.0f, 0.0f) + (myTransform->getFront() * 0.45f) - (myTransform->getLeft() * 0.45f),
			VEC3(0.0f, -1.0f, 0.0f), 0.6f, groundRayHit, queryFlags, hitFlags, filterDataPull) or

		EnginePhysics.Raycast(PXVEC3_TO_VEC3(plController->GetFootPos()) + VEC3(0.0f, 0.0f, 0.0f) - (myTransform->getFront() * 0.45f) + (myTransform->getLeft() * 0.45f),
			VEC3(0.0f, -1.0f, 0.0f), 0.6f, groundRayHit, queryFlags, hitFlags, filterDataPull) or

		EnginePhysics.Raycast(PXVEC3_TO_VEC3(plController->GetFootPos()) + VEC3(0.0f, 0.0f, 0.0f) - (myTransform->getFront() * 0.45f) - (myTransform->getLeft() * 0.45f),
			VEC3(0.0f, -1.0f, 0.0f), 0.6f, groundRayHit, queryFlags, hitFlags, filterDataPull)
		)
	{
		groundHdl.fromVoidPtr(groundRayHit.actor->userData);
	}
	return groundHdl.getOwner();
}

