#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

enum CrosshairType
{
	Neutral,
	Objects,
	Enemies,
	Surfaces
};

class TCompUI : public TCompBase
{
	//crosshair
	CrosshairType type;
	CrosshairType lastType;

	float deltaT = 0.0;
	float distanceObjective;
	float distanceCursor;

	VEC2 standarArrowPosLeft = VEC2(-27, 0);
	VEC2 standarArrowPosRight = VEC2(25, 0);

	VEC2 pointingArrowPosLeft = VEC2(-67, 0);
	VEC2 pointingArrowPosRight = VEC2(65, 0);

	VEC2 currentArrowPosLeft = VEC2(-27, 0);
	VEC2 currentArrowPosRight = VEC2(25, 0);

	float windowSize = 400.0f;
	float sizeExteriorCross = 20.0f;
	float multOnFocused = 1.5f;
	float sizeInteriorCross = 4.0f;
	float thick = 4.0f;
	ImVec4 col = ImVec4(1.0f, 1.0f, 0.4f, 1.0f);

	//max
	float max_health = 100;
	float health = 100;
	float per_health = 1;

	//position
	float x_pos = 20;
	float y_pos = 20;
	float xOff = 0;
	float yOff = 0;

  //cooldowns
  float push_cooldown;
  float pull_cooldown;
  float push_val;
  float pull_val;
  int per_pull;
  int per_push;
  bool recoverPush = false;
  bool recoverPull = false;

	bool lose = false;
	bool win = false;
	float timer = 6.f;
	float currExteriorMult = 1.0f;

	// TUTORIALS
	std::string tutorialMsg;
	bool showTutorial = false;

	//TEMPORAL
	bool visible = true;

	DECL_SIBLING_ACCESS();
	void InitUIValuesMsg(const TMsgInitUI& msg);
	void rcvChangeUIStats(const TMsgChangeUIStats& msg);
	void rcvLoseStateMsg(const TMsgLoseState& msg);
	void rcvWinStateMsg(const TMsgWinState& msg);
	void onAlarmRecieve(const TMsgAlarmClock& msg);
	void tutorialMessage(const TMsgMessageUI& msg);
	void displayTutorialMsg();

public:
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void update(float dt);

	static void registerMsgs();

	//TEMPORAL
	void toggleVisible() { visible = !visible; }
	bool getVisible() { return visible; }
	void setVisible(bool isVisible) { visible = isVisible; }

	void calculatePercentage();
	void updateUIBar();

	float ProgressBar(const char* optionalPrefixText, float value, const float minValue = 0.f, const float maxValue = 1.f, const char* format = "%1.0f%%", const ImVec2& sizeOfBarWithoutTextInPixels = ImVec2(-1, -1),
		const ImVec4& colorLeft = ImVec4(0, 1, 0, 0.8), const ImVec4& colorRight = ImVec4(0, 0.4, 0, 0.8), const ImVec4& colorBorder = ImVec4(0.25, 0.25, 1.0, 1));

	void SetCrosshair(float size, float thickness, VEC4 color, CrosshairType t);
	void SetCrosshair(float size, float thickness, CrosshairType t);
	void SetCrosshair(VEC4 color, bool hasTarget, CrosshairType t);
	void UpdateReticle();
	void UpdateCrystalPower();
	void TurnOffGlow();

  float getTimer() { return timer; };
  void setTimer(float t) { timer = t; };
};