#pragma once

#include "components/common/comp_base.h"
#include "components/controllers/comp_char_controller.h"
#include "comp_player_stats.h"
#include "comp_ui.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

class TCompPlayerMagic : public TCompBase
{
private:
	bool isPaused = false;
	bool isPlayerPullPush = false;
	float dt;
	float dtCounter = 0.0f;
	bool magic = false;
	VEC3 bbhalfextents;
	CHandle vignettePPHandle;
	float currentDistanceMagic;
	float distanceHit;
	CEntity* cameraShaker;
	float timeStopFovUpdate = -1.0f;
	float currFovValue = 75.0f;

	bool pullWallUnlocked = true;
	bool pullObjUnlocked = true;
	bool pushWallUnlocked = true;
	bool pushObjUnlocked = true;

	std::string particles_push_player = "data/particles/prefabs_particles/default_particle.json";
	std::string particles_pull_player = "data/particles/prefabs_particles/default_particle.json";
											 
	std::string particles_push_object = "data/particles/prefabs_particles/default_particle.json";
	std::string particles_pull_object = "data/particles/prefabs_particles/default_particle.json";

	std::string particles_push_spot = "data/particles/prefabs_particles/powerSpotLeft.json";
	std::string particles_pull_spot = "data/particles/prefabs_particles/powerSpotRight.json";

	VEC3 origin;                          // [in] Ray origin
	VEC3 dir;                             // [in] Normalized ray direction
	physx::PxQueryFilterData filterDataPull;  // [in] Filtering data
	physx::PxQueryFilterData filterDataPush;  // [in] Filtering data
	physx::PxQueryFlags queryFlags;       // [in] Query flags
	physx::PxRaycastHit hit;              // [out] Raycast results

	std::vector<physx::PxRaycastHit> checkHitGnome;
	std::vector<bool> checkHitStatus;
	physx::PxHitFlags hitFlags;           // [out] Raycast hit flags

	CHandle currentTargetHandle;
	TCompTransform *myTransform;
	TCompCharController *plController;
	TCompPlayerStats *plStats;
	TCompUI *plUI;

	void HandleHit(bool hitStatus, physx::PxRaycastHit *hit);
	void SetReticleColor(bool hitStatus, CHandle targetHdl);
	void ActivateOutlines(bool hitStatus, CHandle targetHdl);
	void PullPushObject(CHandle targetHdl, physx::PxRaycastHit *hit);
	void PullingObj();
	void StartPullingObj(CHandle targetHdl);
	void PushTargetObj(CHandle targetHdl);
	void PullTargetObj(CHandle targetHdl);
	void PushPulledObj();
	void FreezePulledObj();
	void PullPushPlayer(CHandle targetHdl, PxRaycastHit *hit);


	CHandle checkDoubleRayGoblin();
	void SetNewDistanceMagic();
	void MagicVFX(bool isPull);
	void CreateSpotMagicPS(physx::PxRaycastHit * hit, const int& mode);
	CHandle groundPullCheck();

public:
	bool enabled = true;
	bool pullingObj = false;
  CHandle pulledObjHandle;
	VEC3 target;
	VEC3 angularVel;
	VEC4 reticleColorNormal;
	VEC4 reticleColorYes;
	VEC4 reticleColorNo;
	float maxDistanceMagic;
	float minDistanceMagic;
	float pullObjFloatDistance;
	float pullObjForce;
	float pullWallForceBase;
	float pullWallForceExtra;
	float pushObjForce;
	float pushWallForceBase;
	float pushWallForceExtra;
	float pushWallMinFactor;
	float pushCooldown;
	float pullCooldown;
	float gravityRecoverT;
	float minDistUpHelp = -5.0f;
	float maxDistUpHelp = 25.0f;
	float upHelpAmount = 10.0f;
	float chromATime = 0.5f;
	float chromAInitialV = 0.6f;
	float pushFov = 100.0f;
	float pullFov = 65.0f;

	DECL_SIBLING_ACCESS();
	static void registerMsgs();
	void init(const TMsgEntityCreated &msg);
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void renderDebug();
	void update(float dt);

	void RaySetup(VEC3 newOrigin, VEC3 newDir);
	void setMagic(bool mMagic) { magic = mMagic; }
	bool getMagic() { return magic; }
	void onAlarmRecieve(const TMsgAlarmClock& msg);

	void setIsPlayerPullPush(bool b) { isPlayerPullPush = b; }
	bool getIsPlayerPullPush() { return isPlayerPullPush; }
  void ReleasePullObj();
	bool getPullWallUnlocked() { return pullWallUnlocked; }
	void setPullWallUnlocked(bool unlock) { pullWallUnlocked = unlock; }
	bool getPullObjUnlocked() { return pullObjUnlocked; }
	void setPullObjUnlocked(bool unlock) { pullObjUnlocked = unlock; }
	bool getPushWallUnlocked() { return pushWallUnlocked; }
	void setPushWallUnlocked(bool unlock) { pushWallUnlocked = unlock; }
	bool getPushObjUnlocked() { return pushObjUnlocked; }
	void setPushObjUnlocked(bool unlock) { pushObjUnlocked = unlock; }
	void activateMagic() { pullWallUnlocked = pullObjUnlocked = pushWallUnlocked = pushObjUnlocked = true; }
	void deactivateMagic() { pullWallUnlocked = pullObjUnlocked = pushWallUnlocked = pushObjUnlocked = false; }
	void toggleEnabled() { enabled = !enabled; }
  void setEnabled(bool b) { enabled = b; }
	void setPaused(bool paused) { isPaused = paused; }

	float getPushForce() { return pushObjForce; }
	float getDistance() { return distanceHit; }
	float getMaxDistance() { return maxDistanceMagic; }

	std::string getParticlePushPlayer() { return particles_push_player; }
	std::string getParticlePullPlayer() { return particles_pull_player; }

	std::string getParticlePushObject() { return particles_push_object; }
	std::string getParticlePullObject() { return particles_pull_object; }
};
