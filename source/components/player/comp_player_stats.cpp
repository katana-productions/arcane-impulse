#include "mcv_platform.h"
#include "comp_player_stats.h"
#include "engine.h"
#include "input/module_input.h"
#include "components/common/comp_fsm.h"
#include "components/player/comp_player_move.h"
#include "components/player/comp_player_magic.h"
#include "render/module_render.h"

DECL_OBJ_MANAGER("player_stats", TCompPlayerStats);

void TCompPlayerStats::debugInMenu() {
	ImGui::DragFloat("Health", &health, 0.1f, 0.f, max_health);
	ImGui::DragFloat("vignetteSpeed", &vignetteSpeed, 0.5f, 0.f, 1000.0f);
	ImGui::DragFloat("hitSpeed", &hitSpeed, 0.5f, 0.f, 1000.0f);
	ImGui::DragFloat("baseHeartRate", &baseHeartRate, 0.1f, 0.f, 50.0f);
	ImGui::DragFloat("heartIntensity", &heartIntensity, 0.1f, 0.f, 50.0f);
}

void TCompPlayerStats::load(const json& j, TEntityParseContext& ctx) {
	max_health = j.value("max_health", max_health);
	health = max_health;

	timeBeforeHeal = j.value("time_before_heal", 3.0f);
	healPerSecond = j.value("heal_per_second", 5.0f);
	vignetteSpeed = j.value("vignetteSpeed", vignetteSpeed);
	hitSpeed = j.value("hitSpeed", hitSpeed);
	baseHeartRate = j.value("baseHeartRate", baseHeartRate);
	heartIntensity = j.value("heartIntensity", heartIntensity);
}

void TCompPlayerStats::registerMsgs() {
	DECL_MSG(TCompPlayerStats, TMsgDamage, rcvDmgMsg);
	DECL_MSG(TCompPlayerStats, TMsgHeal, rcvHealMsg);
	DECL_MSG(TCompPlayerStats, TMsgAlarmClock, onAlarmRecieve);
	DECL_MSG(TCompPlayerStats, TMsgEntityCreated, onEntityCreated);
}

void TCompPlayerStats::onEntityCreated(const TMsgEntityCreated& msg)
{
	h_ui = getEntityByName("UI");

	CEntity* e_ui = h_ui;
	TMsgInitUI msgInit;
	msgInit.max_health = max_health;
	msgInit.pull_cooldown = pull_cooldown;
	msgInit.push_cooldown = push_cooldown;
	e_ui->sendMsg(msgInit);

	myTrans = get<TCompTransform>();
	prevPos = myTrans->getPosition();

	CEntity *camera = (CEntity *)getEntityByName("CameraShaker");
	render_overlay = camera->get<TCompOverlay>();

	maxVignette = render_overlay->vignette;
	currVignette = 0.0f;

	iniIntensity = render_overlay->intensity;

	render_overlay->black = 1;
	EngineAlarms.AddAlarm(1.0f, 921, CHandle(this));
}

void TCompPlayerStats::onAlarmRecieve(const TMsgAlarmClock& msg)
{
	if (msg.id == 50) canPush = true;
	if (msg.id == 51) canPull = true;
	if (msg.id == 921) EngineBasics.LerpElement(&render_overlay->black, 0.0f, 1.0f);
	if (msg.id == 1080) {
		EnginePersistence.blockPlayer(false);
	}
}

void TCompPlayerStats::rcvDmgMsg(const TMsgDamage& msg) {
	if (!immortal)
	{
		CEntity* e_ui = h_ui;
		health -= msg.damage;
		if (health < 0)	health = 0;
		if (health == 0) loseState();

		// Reset heal time
		time = 0.0f;

		TMsgChangeUIStats msgUI;
		msgUI.health = -msg.damage;
		msgUI.pull_used = false;
		msgUI.push_used = false;
		e_ui->sendMsg(msgUI);

    EngineLua.setRandomCamShake(0.2f, 0.2f, 0.2f);

		//Posar so de hit
		EngineAudio.playEvent("event:/PlayerVoice/Hurt", 4);


		currVignette = maxVignette;
		targetVignette = 0.0f;
	}
}

void TCompPlayerStats::rcvHealMsg(const TMsgHeal& msg)
{
	heal(msg.hp);
}

bool TCompPlayerStats::usePush()
{
	if (infMagic) return true;
	if (canPush)
	{
		canPush = false;
		time = 0;
		EngineAlarms.AddAlarm(push_cooldown, 50, CHandle(this));
		CEntity* e_ui = h_ui;
		TMsgChangeUIStats msgUI;
		msgUI.health = 0;
		msgUI.pull_used = false;
		msgUI.push_used = true;
		e_ui->sendMsg(msgUI);
		return true;
	}
	return false;
}

bool TCompPlayerStats::usePull()
{
	if (infMagic) return true;
	if (canPull)
	{
		canPull = false;
		time = 0;
		EngineAlarms.AddAlarm(pull_cooldown, 51, CHandle(this));
		CEntity* e_ui = h_ui;
		TMsgChangeUIStats msgUI;
		msgUI.health = 0;
		msgUI.pull_used = true;
		msgUI.push_used = false;
		e_ui->sendMsg(msgUI);
		return true;
	}
	return false;
}

void TCompPlayerStats::heal(float health_recovered) {
	CEntity* e_ui = h_ui;
	health += health_recovered;

	if (health > max_health)
		health = max_health;

	TMsgChangeUIStats msgUI;
	msgUI.health = health_recovered;
	msgUI.pull_used = false;
	msgUI.push_used = false;
	e_ui->sendMsg(msgUI);
}

void TCompPlayerStats::loseState() {
	/*CEntity* e_left_hand = getEntityByName("LeftHand");
	TCompFSM* cFsm_left = e_left_hand->get<TCompFSM>();

	CEntity* e_right_hand = getEntityByName("RightHand");
	TCompFSM* cFsm_right = e_right_hand->get<TCompFSM>();
	if (cFsm_right && cFsm_left) {
		cFsm_right->changeVariable("life", 0, false);
		cFsm_left->changeVariable("life", 0, false);
	}*/
	
	EngineAudio.playEvent("event:/PlayerVoice/Death", 4);

	TCompPlayerMove* playerMove = get<TCompPlayerMove>();
	playerMove->isDead = true;

	TCompPlayerMagic* playerMagic = get<TCompPlayerMagic>();
	playerMagic->enabled = false;

	EngineLua.launchPlayerDeath();

	CEntity* e_ui = h_ui;

	TMsgLoseState msg;
	e_ui->sendMsg(msg);
}

void TCompPlayerStats::update(float dt)
{
	time += dt;
	if (time >= timeBeforeHeal)
	{
		heal(healPerSecond * dt);
	}

	if (trap) {
		EngineAlarms.AddAlarm(2, 1080, CHandle(this));
		trap = false;
	}

	if (render_overlay)
	{
		float factor = clamp(health / 30.0f, 0.0f, 1.0f);
		currHit = 1 - factor;
		render_overlay->hitAmount = currHit;

		currVignette = lerpFloat(currVignette, targetVignette, vignetteSpeed * dt);
		if (health <= 30.0f)
		{
			currVignette = std::max(currVignette, maxVignette * currHit);
		}
		render_overlay->vignette = currVignette;

		float sinHeartBeat = heartIntensity * sin(Time.current * baseHeartRate);
		sinHeartBeat = (sinHeartBeat + 1) / 2; //0-1 range
		sinHeartBeat *= 0.5; //Hardcoded, it reduces intensity. The effect was too harsh
		render_overlay->intensity = iniIntensity + sinHeartBeat;

		EngineRender.globalExposure = 1.0f + (5.0f * currHit);
	}
}