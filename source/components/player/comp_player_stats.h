#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "components/common/comp_transform.h"
#include "components/postfx/comp_render_overlay.h"

class TCompPlayerStats : public TCompBase
{
	bool immortal = false;
	bool infMagic = false;
	float max_health = 100;
	float health = 100;
	float pull_cooldown = 1.0f;
	float push_cooldown = 1.0f;

	bool canPush = true;
	bool canPull = true;
	bool trap = false;

	float timeBeforeHeal;
	float healPerSecond;
	float time = 0.0f;

	CHandle h_ui;
	TCompTransform *myTrans;
	VEC3 prevPos;

	TCompOverlay* render_overlay;
	float currVignette = 0;
	float currHit = 0;
	float targetVignette = 0;
	float targetHit = 0;
	float vignetteSpeed = 2;
	float hitSpeed = 2;
	float maxVignette;

	float baseHeartRate = 5;
	float heartIntensity = 1;
	float iniIntensity;


	DECL_SIBLING_ACCESS();
	void rcvDmgMsg(const TMsgDamage& msg);
	void rcvHealMsg(const TMsgHeal& msg);
	void onAlarmRecieve(const TMsgAlarmClock& msg);
	void onEntityCreated(const TMsgEntityCreated& msg);


public:
	void debugInMenu();
	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);

	static void registerMsgs();
	bool usePush();
	bool usePull();
	void heal(float health_recovered);
	void loseState();

	bool getCanPush() { return canPush; };
	bool getCanPull() { return canPull; };
	float getMaxHealth() { return max_health; };
	bool getImmortal() { return immortal; };
	void setImmortal(bool i) { immortal = i; };
	void toggleImmortal() { immortal = !immortal; };
	bool getInfMagic() { return infMagic; };
	void setInfMagic(bool i) { infMagic = i; };
	void toggleInfMagic() { infMagic = !infMagic; };
	float getHealth() { return health; };
	float getPullCdw() { return pull_cooldown; };
	void setPullCdw(float cdw) { pull_cooldown = cdw; };
	float getPushCdw() { return push_cooldown; };
	void setPushCdw(float cdw) { push_cooldown = cdw; };
	void setTrap(bool trapped) { trap = trapped; };
	float getHp() { return health; }

};

