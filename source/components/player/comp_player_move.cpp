#include "mcv_platform.h"
#include "comp_player_move.h"
#include "input/module_input.h"
#include "components/common/comp_fsm.h"

DECL_OBJ_MANAGER("player_move", TCompPlayerMove);

void TCompPlayerMove::registerMsgs() {
	DECL_MSG(TCompPlayerMove, TMsgAlarmClock, onAlarmRecieve);
	DECL_MSG(TCompPlayerMove, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompPlayerMove, TMsgEntityTriggerEnter, onTriggerEnter);
}

void TCompPlayerMove::onAlarmRecieve(const TMsgAlarmClock& msg) {
	if (msg.id == 10) canJump = true;
	if (msg.id == 11) hasJumped = true;
	if (msg.id == 20) canCoyoteJump = false;
	if (msg.id == 46) {
		canJumpHigher = false;
		CEntity* e_left_hand = getEntityByName("LeftHand");
		TCompFSM* cFsm_left = e_left_hand->get<TCompFSM>();

		CEntity* e_right_hand = getEntityByName("RightHand");
		TCompFSM* cFsm_right = e_right_hand->get<TCompFSM>();

		cFsm_right->changeVariable("floating", 1, true);
		cFsm_left->changeVariable("floating", 1, true);
	}
}

void TCompPlayerMove::onEntityCreated(const TMsgEntityCreated& msg) {
	camMovementH = getEntityByName("CameraShaker");
}

void TCompPlayerMove::onTriggerEnter(const TMsgEntityTriggerEnter& msg) {
	if (!msg.h_entity.isValid()) return;
	CEntity* src = msg.h_entity;
	std::string name = src->getName();

	//TODO: Improve this placeholder mess
	std::string trigger_name = "Trigger";
	TCompEnemyStats* enemy = get<TCompEnemyStats>();
	if (name == trigger_name && enemy == nullptr) {
		TMsgWinState msg;
		CEntity *e_ui = getEntityByName("UI");
		e_ui->sendMsg(msg);
	}
}

void TCompPlayerMove::update(float scaled_dt) {
	if (isPaused || isDead) return;

	charController = get<TCompCharController>();
	myTransform = get<TCompTransform>();
	CEntity* eCamMovement = camMovementH;
	camMove = eCamMovement->get<TCompCamMove>();
	if (!active || !charController || !myTransform || !camMove) return;
	deltaTime = scaled_dt;

	isGrounded = charController->IsGrounded();

	CaptureInput();
	RotateToCamForward();
	JumpHandle();
	InputManipulation();

	charController->newMoveDirection(input, false);
}

void TCompPlayerMove::CaptureInput()
{
	input = VEC3(EngineInput["Move_Right"].value - EngineInput["Move_Left"].value
		, 0.0f, EngineInput["Move_Up"].value - EngineInput["Move_Down"].value);
	input = input.x * -myTransform->getLeft() + input.z * myTransform->getFront();
	input.Normalize();
	if (isGrounded) lastGroundedInput = VEC3::Lerp(lastGroundedInput, input, deltaTime * groundInputSmooth);
}

void TCompPlayerMove::RotateToCamForward()
{
	CHandle camHandle = getEntityByName("Camera");
	if (camHandle.isValid())
	{
		CEntity* camEntity = (CEntity*)camHandle;
		TCompTransform* camTransform = camEntity->get<TCompTransform>();
		camTransform->getAngles(&yaw, &pitch);
		myTransform->setAngles(yaw, 0.0f, 0.0f);
	}
}

void TCompPlayerMove::JumpHandle()
{
	CEntity* e_left_hand = getEntityByName("LeftHand");
	TCompFSM* cFsm_left = e_left_hand->get<TCompFSM>();

	CEntity* e_right_hand = getEntityByName("RightHand");
	TCompFSM* cFsm_right = e_right_hand->get<TCompFSM>();

	if (isGrounded && !prevIsGrounded)
	{
		 //Land
		camMove->SetState(camMove->land);
		if (cFsm_right && cFsm_left) {
			cFsm_right->changeVariable("jump", 0, true);
			cFsm_left->changeVariable("jump", 0, true);
			cFsm_right->changeVariable("floating", 0, true);
			cFsm_left->changeVariable("floating", 0, true);
		}
	}
	else if (!isGrounded && prevIsGrounded)
	{
		//TakeOff
		camMove->SetState(camMove->jump);
	}

	if (EngineInput["Jump"].justPressed() && ((isGrounded && canJump) || (canCoyoteJump && canJump)))
	{
		if (cFsm_right && cFsm_left) {
			cFsm_right->changeVariable("jump", 1, true);
			cFsm_left->changeVariable("jump", 1, true);
		}

		EngineAudio.playEvent("event:/PlayerVoice/Jump", 4);

		VEC3 dirJump = VEC3::Lerp(VEC3(0,1,0), input, clamp(magnitude(input), 0.0f, maxJumpDirInf));
		dirJump.Normalize();
		charController->jump(1.0f, dirJump);

		hasJumped = true;
		canJump = false;
		canCoyoteJump = false;
		CHandle myHandle = CHandle(this);
		EngineAlarms.AddAlarm(timeBetweenJumps, 10, myHandle);
		EngineAlarms.AddAlarm(0.1, 11, myHandle); 
		
		canJumpHigher = true;
		EngineAlarms.AddAlarm(0.3, 46, myHandle);
	}
	if (isGrounded)
	{
		if (!prevIsGrounded) {
			if (cFsm_right && cFsm_left) {
				cFsm_right->changeVariable("jump", 0, true);
				cFsm_left->changeVariable("jump", 0, true);
				cFsm_right->changeVariable("floating", 0, true);
				cFsm_left->changeVariable("floating", 0, true);
			}
			TCompCharController* charController = get<TCompCharController>();

			Studio::EventInstance* event = EngineAudio.createEvent("event:/Movement/Landing");
			float force = charController->getForceFalling();
			float vol = clampFloat(abs(force) / 20, 0.05f, 1.0f);
			event->setVolume(vol);
			EngineAudio.playEvent("event:/Movement/Landing", 1, event);
			if (abs(force) > 25 && !EngineAudio.voiceIsPlaying()) {
				int value = randomInt(1, 20);
				if (value == 1)
					EngineLua.playDialogue(randomInt(2, 3));
				else {
					EngineAudio.playEvent("event:/PlayerVoice/Hurt", 4);
				}
			}
		}
 		prevIsGrounded = true;
		hasJumped = false;
	}
	else
	{
		if (prevIsGrounded)
		{
			canCoyoteJump = true;
			CHandle myHandle = CHandle(this);
			EngineAlarms.AddAlarm(coyoteTime, 20, myHandle);
		}
		prevIsGrounded = false;
	}
	if (EngineInput["Jump"].isPressed() && canJumpHigher) charController->ChangeGravityMult(lowJumpMult);
	else charController->ChangeGravityMult(1.0f);
	if (EngineInput["Jump"].justReleased()) canJumpHigher = false;
}

void TCompPlayerMove::InputManipulation()
{
	if (!isGrounded) {
		lastGroundedInput = VEC3::Lerp(lastGroundedInput, VEC3(0, 0, 0), deltaTime * lastInputSmooth);
		lastGroundedInput += input * airInputMult * deltaTime;
		if (magnitude(lastGroundedInput) > 1.0f) lastGroundedInput.Normalize();
		input = lastGroundedInput;
	}
	if (isGrounded && EngineInput["Run"].isPressed() && EngineInput["Move_Up"].value > 0.1f) {
		targetSpeedMult = runMult;
		camMove->SetState(camMove->run);
	}
	else if (isGrounded) {
		targetSpeedMult = 1.0f;
		lastGroundedInput = VEC3::Lerp(lastGroundedInput, input, deltaTime * groundInputSmooth);
		if (magnitude(input) > 0.1f) camMove->SetState(camMove->walk);
		else camMove->SetState(camMove->idle);
	}
	currSpeedMult = currSpeedMult + ((smoothSpeedMult * deltaTime) * (targetSpeedMult - currSpeedMult));
	input *= currSpeedMult;
	if (!isGrounded) input *= airSpeedMult;
}

void TCompPlayerMove::SetActive(const bool & isActive)
{
	active = isActive;
}

//====================================================================================

void TCompPlayerMove::load(const json& j, TEntityParseContext& ctx) {
	runMult = j.value("runMult", runMult);
	smoothSpeedMult = j.value("smoothSpeedMult", smoothSpeedMult);
	airSpeedMult = j.value("airSpeedMult", airSpeedMult);
	airInputMult = j.value("airInputMult", airInputMult);
	lowJumpMult = j.value("lowJumpMult", lowJumpMult);
	maxJumpDirInf = j.value("maxJumpDirInf", maxJumpDirInf);
	groundInputSmooth = j.value("groundInputSmooth", groundInputSmooth);
	lastInputSmooth = j.value("lastInputSmooth", lastInputSmooth);
	coyoteTime = j.value("coyoteTime", coyoteTime);
}

void TCompPlayerMove::debugInMenu() {
	ImGui::DragFloat("runMult", &runMult, 0.05f, 0.0f, 5.0f);
	ImGui::DragFloat("smoothSpeedMult", &smoothSpeedMult, 0.5f, 0.0f, 100.0f);
	ImGui::DragFloat("airSpeedMult", &airSpeedMult, 0.01f, 0.0f, 1.0f);
	ImGui::DragFloat("airInputMult", &airInputMult, 0.01f, 0.0f, 10.0f);
	ImGui::DragFloat("lowJumpMult", &lowJumpMult, 0.01f, 0.0f, 1.0f);
	ImGui::DragFloat("maxJumpDirInf", &maxJumpDirInf, 0.01f, 0.0f, 1.0f);
	ImGui::DragFloat("groundInputSmooth", &groundInputSmooth, 0.1f, 0.0f, 100.0f);
	ImGui::DragFloat("lastInputSmooth", &lastInputSmooth, 0.01f, 0.0f, 10.0f);
	ImGui::DragFloat("coyoteTime", &coyoteTime, 0.001f, 0.0f, 1.0f);
}
