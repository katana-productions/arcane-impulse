#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "components/common/comp_transform.h"
#include "components/postfx/comp_render_overlay.h"

class TCompPlayerPowerParticles : public TCompBase
{
	enum Enum
	{
		ePull = 0,
		ePush = 1,

		eINVALID = -1		//!< internal use only!
	};

	int type;

	std::string particles_system_player = "data\particles\prefabs_particles\default_particle.json";

	std::string particles_system_object = "data\particles\prefabs_particles\default_particle.json";

	struct SParticleSystem{
		CHandle particleSystem;
		float lifeTime;
		bool toBeDestroyed;
	};

	std::vector<SParticleSystem> v_SParticleSystem;
	VEC3 originalPos = VEC3(0, 0, 0);
	int timeToDisappear = 1.0f;
	bool destroy = false;

	float deltaT = 0;

	DECL_SIBLING_ACCESS();
	void onEntityCreated(const TMsgEntitiesGroupCreated& msg);

public:
	enum EnumPlayerObject
	{
		ePlayer = 0,
		eObject = 1
	};

	static void registerMsgs();
	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);

	void activatePlayerPower(EnumPlayerObject obj);
	void desactivatePlayerPower();
	void MoveSystemParticle();
	void CheckDestroySystems();
	void debugInMenu();
};

