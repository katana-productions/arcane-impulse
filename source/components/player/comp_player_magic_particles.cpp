#include "mcv_platform.h"
#include "comp_player_magic_particles.h"
#include "comp_player_magic.h"

DECL_OBJ_MANAGER("player_power_particles", TCompPlayerPowerParticles);

void TCompPlayerPowerParticles::registerMsgs() {
	DECL_MSG(TCompPlayerPowerParticles, TMsgEntitiesGroupCreated, onEntityCreated);
}

void TCompPlayerPowerParticles::onEntityCreated(const TMsgEntitiesGroupCreated& msg)
{
	CEntity* player = getEntityByName("Player");
	TCompPlayerMagic* c_player_magic = player->get< TCompPlayerMagic>();

	if (type == Enum::ePull) {
		particles_system_player = c_player_magic->getParticlePullPlayer();
		particles_system_object = c_player_magic->getParticlePullObject();
	}
	else if (type == Enum::ePush)
	{
		particles_system_player = c_player_magic->getParticlePushPlayer();
		particles_system_object = c_player_magic->getParticlePushObject();
	}
}

void TCompPlayerPowerParticles::load(const json& j, TEntityParseContext& ctx) 
{
	timeToDisappear = j.value("timeToDisappear", timeToDisappear);
	if (!j.count("type"))
		type = Enum::ePush;
	else {
		std::string powerType = j["type"];

		if (powerType == "push") {
			type = Enum::ePush;
		}
		else if (powerType == "pull") {
			type = Enum::ePull;
		}
	}
}

void TCompPlayerPowerParticles::update(float dt)
{
	deltaT = dt;
	MoveSystemParticle();
	CheckDestroySystems();
}

void TCompPlayerPowerParticles::activatePlayerPower(EnumPlayerObject obj) {
	TCompTransform* c_trans = get<TCompTransform>();
	c_trans->setRotation(VEC4(0,0,0,1));

	std::vector<CHandle> h_group_systems;
	bool destroy;
	if (obj == EnumPlayerObject::ePlayer) {
		h_group_systems = EngineParticles.CreateLogicSeparatedSystemParticles(particles_system_player.c_str(), c_trans);
		destroy = true;
	}
	else if (obj == EnumPlayerObject::eObject) {
		h_group_systems = EngineParticles.CreateLogicSeparatedSystemParticles(particles_system_object.c_str(), c_trans);
		destroy = type;
	}

	for (auto h_loaded : h_group_systems) {
		CEntity* e_loaded = h_loaded;
		TCompBuffers* c_buffer = e_loaded->get<TCompBuffers>();
		auto cte_buffer = c_buffer->getCteByName("TCtesParticles");
		TCtesParticles* particles_buffer = dynamic_cast<TCtesParticles*>(cte_buffer);

		SParticleSystem spsystem;
		spsystem.toBeDestroyed = destroy;
		spsystem.particleSystem = h_loaded;
		spsystem.lifeTime = 2.0f;

		v_SParticleSystem.push_back(spsystem);
	}

	originalPos = c_trans->getPosition();
}

void TCompPlayerPowerParticles::MoveSystemParticle() {
	TCompTransform* c_trans = get<TCompTransform>();
	if (!c_trans)
		return;

	for (auto spsystem : v_SParticleSystem) {
		CHandle h_loaded = spsystem.particleSystem;
		if (!h_loaded.isValid())
			continue;

		CEntity* e_loaded = h_loaded;
		TCompBuffers* c_buffer = e_loaded->get<TCompBuffers>();
		auto cte_buffer = c_buffer->getCteByName("TCtesParticles");
		TCtesParticles* particles_buffer = dynamic_cast<TCtesParticles*>(cte_buffer);

		if (particles_buffer->emitter_billboard)
			particles_buffer->emitter_offset_center = c_trans->getPosition();
		else {
			VEC3 deltaPos = c_trans->getPosition() - originalPos;
			particles_buffer->emitter_offset_center = deltaPos;
		}
		EngineParticles.UpdateConstantBuffer(cte_buffer);
	}
}

void TCompPlayerPowerParticles::desactivatePlayerPower() {
	for (auto& spsystem : v_SParticleSystem) {
		if (spsystem.toBeDestroyed) continue;

		CHandle h_loaded = spsystem.particleSystem;
		if (!h_loaded.isValid())
			continue;

		CEntity* e_loaded = h_loaded;
		TCompBuffers* c_buffer = e_loaded->get<TCompBuffers>();
		auto cte_buffer = c_buffer->getCteByName("TCtesParticles");
		TCtesParticles* particles_buffer = dynamic_cast<TCtesParticles*>(cte_buffer);

		particles_buffer->emitter_looping = false;
		EngineParticles.UpdateConstantBuffer(cte_buffer);

		spsystem.toBeDestroyed = true;
	}
}

void TCompPlayerPowerParticles::CheckDestroySystems() {
	int numSystem = 0;
	for (auto& spsystem : v_SParticleSystem) {

		CHandle h_loaded = spsystem.particleSystem;
		if (!h_loaded.isValid())
			continue;

		if (!spsystem.toBeDestroyed) 
			continue;

		spsystem.lifeTime -= deltaT;


		if (spsystem.lifeTime < 0) {
			EngineParticles.DestroySystem(h_loaded);
			v_SParticleSystem.erase(v_SParticleSystem.begin() + numSystem);
			break;
		}
		numSystem++;
	}
}

void TCompPlayerPowerParticles::debugInMenu() {
}