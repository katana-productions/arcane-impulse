#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

class TCompHandManager : public TCompBase
{
private:
	float burstTime = 0.5f;
	int currPull = 0;
	int currPush = 0;
	enum handStates
	{
		endPull = 0,
		endPush = 1
	};
	handStates states = endPull;
	void onAlarmRecieve(const TMsgAlarmClock& msg);
	void onEntitiesCreated(const TMsgEntitiesGroupCreated & msg);

	int idle_state = 0;
	int open_state = 1;
	int closed_state = 2;

	VEC4 baseColor = VEC4(0.5f, 0.0f, 0.5f, 1.0);
	VEC4 pullColor = VEC4(0.1f, 0.4f, 1.0f, 1.0);
	VEC4 pushColor = VEC4(1.0f, 0.75f, 0.3f, 1.0);

public:
	DECL_SIBLING_ACCESS();
	static void registerMsgs();
	void load(const json& j, TEntityParseContext& ctx);

	void setPullBurst();
	void setPushBurst();

	static CHandle handle;
};