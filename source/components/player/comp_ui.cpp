#include "mcv_platform.h"
#include "comp_ui.h"
#include "engine.h"
#include "ui/module_ui.h"
#include "ui/widgets/ui_progress.h"
#include "ui/widgets/ui_image.h"
#include "components/common/comp_fsm.h"
#include "components/other/comp_glove_glow.h"
#include "components\player\comp_player_magic.h"

DECL_OBJ_MANAGER("comp_ui", TCompUI);

void TCompUI::debugInMenu() {
  ImGui::DragFloat("ManaHealth X_Pos", &x_pos, 1.0f, 0.f, Render.width);
  ImGui::DragFloat("ManaHealth Y_Pos", &y_pos, 1.0f, 0.f, Render.height);
  ImGui::DragFloat("Exterior reticle", &sizeExteriorCross, 0.1f, 0.0f, 100.0f);
  ImGui::DragFloat("Interior reticle", &sizeInteriorCross, 0.1f, 0.0f, 100.0f);
  ImGui::DragFloat("Window Size", &windowSize, 10.0f, 0.0f, 1000.0f);
  ImGui::DragFloat("Reticle Thickness", &thick, 0.1f, 0.0f, 100.0f);
  ImGui::DragFloat4("Color", &col.x, 0.01f, 0.0f, 1.0f);
  ImGui::DragFloat("xOff", &xOff, 0.01f, 0.0f, 1.0f);
  ImGui::DragFloat("yOff", &yOff, 0.01f, 0.0f, 1.0f);
}

void TCompUI::load(const json& j, TEntityParseContext& ctx) {
  xOff = j.value("xOff", 0.0f);
  yOff = j.value("yOff", 0.0f);
  x_pos = Render.width * xOff;
  y_pos = Render.height * yOff;
}

void TCompUI::registerMsgs() {
  DECL_MSG(TCompUI, TMsgInitUI, InitUIValuesMsg);
  DECL_MSG(TCompUI, TMsgChangeUIStats, rcvChangeUIStats);
  DECL_MSG(TCompUI, TMsgLoseState, rcvLoseStateMsg);
  DECL_MSG(TCompUI, TMsgWinState, rcvWinStateMsg);
  DECL_MSG(TCompUI, TMsgAlarmClock, onAlarmRecieve);
  DECL_MSG(TCompUI, TMsgMessageUI, tutorialMessage);
}

void TCompUI::InitUIValuesMsg(const TMsgInitUI& msg) {
  max_health = msg.max_health;
  health = max_health;
  pull_cooldown = msg.pull_cooldown;
  push_cooldown = msg.push_cooldown;
  pull_val = pull_cooldown;
  push_val = push_cooldown;
  calculatePercentage();
}

void TCompUI::onAlarmRecieve(const TMsgAlarmClock& msg) {
  if (msg.id == 2)
  {
    CEngine::get().getModules().changeToGamestate("gs_reset");

    //EnginePersistence.resetPlayerPos();

    //CEntity* e_left_hand = getEntityByName("LeftHand");
    //TCompFSM* cFsm_left = e_left_hand->get<TCompFSM>();
    //CEntity* e_right_hand = getEntityByName("RightHand");
    //TCompFSM* cFsm_right = e_right_hand->get<TCompFSM>();
    ///*cFsm_right->changeVariable("beginPlay", 1, true);
    //cFsm_left->changeVariable("beginPlay", 1, true);*/
    //cFsm_right->changeVariable("life", 100, false);
    //cFsm_left->changeVariable("life", 100, false);

    lose = false;

  }
}

void TCompUI::rcvLoseStateMsg(const TMsgLoseState& msg) {
  lose = true;
  CHandle myHandle = CHandle(this);
  EngineAlarms.AddAlarm(timer, 2, myHandle);
}

void TCompUI::rcvWinStateMsg(const TMsgWinState& msg) {
  win = true;
  CHandle myHandle = CHandle(this);
  EngineAlarms.AddAlarm(timer, 2, myHandle);
}

void TCompUI::rcvChangeUIStats(const TMsgChangeUIStats& msg) {
  health += msg.health;

  if (health < 0)
    health = 0;
  if (health > max_health)
    health = max_health;

  calculatePercentage();

  if (msg.pull_used)
  {
    pull_val = 0.0f;
    recoverPull = true;
    TurnOffGlow();
  }
  if (msg.push_used)
  {
    push_val = 0.0f;
    recoverPush = true;
    TurnOffGlow();
  }
}

void TCompUI::tutorialMessage(const TMsgMessageUI& msg)
{
  tutorialMsg = msg.message.c_str();
  showTutorial = msg.show;
}

void TCompUI::displayTutorialMsg()
{
  bool p_open = NULL;
  //ImGuiWindowFlags_NoBackground
  ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize
    | ImGuiWindowFlags_NoNav | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove
    | ImGuiWindowFlags_NoScrollWithMouse | ImGuiWindowFlags_NoInputs;

  ImGui::SetNextWindowPos(ImVec2(Render.width * 1.0f / 4.0f, Render.height * 3.0f / 4.0f));
  ImGui::SetNextWindowSize(ImVec2(Render.width * 2.0f / 4.0f, Render.height * 1.0f / 8.0f));

  // Main body starts here.
  if (!ImGui::Begin("tutorial", &p_open, window_flags))
  {
    // Early out if the window is collapsed, as an optimization.
    ImGui::End();
    return;
  }
  ImGui::SetWindowFontScale(2);
  ImGui::TextWrapped(tutorialMsg.c_str());
  ImGui::End();
}

void TCompUI::calculatePercentage() {
  per_health = health / max_health;
  per_pull = (int)(pull_val / pull_cooldown * 100);
  per_push = (int)(push_val / push_cooldown * 100);
  updateUIBar();
}

void TCompUI::updateUIBar() {
  UI::CProgress* progressBar = dynamic_cast<UI::CProgress*>(Engine.getUI().getWidgetByAlias("life_bar_inGame"));
  progressBar->setRatio(per_health);
}

void TCompUI::update(float delta) {
  deltaT = delta;

  x_pos = Render.width * xOff;
  y_pos = Render.height * yOff;

  //Temporal, until we can change components that are rendered by type
  if (!visible)  return;

  if (pull_val >= pull_cooldown)
  {
    recoverPull = false;
    pull_val = pull_cooldown;
  }
  if (push_val >= push_cooldown)
  {
    recoverPush = false;
    push_val = push_cooldown;
  }
  if (recoverPull)
    pull_val += delta;
  if (recoverPush)
    push_val += delta;

  calculatePercentage();
  UpdateReticle();
  UpdateCrystalPower();

  bool p_open = NULL;
  ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoBackground | ImGuiWindowFlags_NoResize
    | ImGuiWindowFlags_NoNav | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove
    | ImGuiWindowFlags_NoScrollWithMouse | ImGuiWindowFlags_NoInputs;


  //SET UI PUSH PULL


  if (lose || win) {
    /*window_flags = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize;

    float window_size_width = 190;
    float window_size_height = 55;

    ImGui::SetNextWindowPos(ImVec2(Render.width / 2 - window_size_width / 2, Render.height / 2 - window_size_height / 2));
    ImGui::SetNextWindowSize(ImVec2(window_size_width, window_size_height));*/

    // Main body starts here.
    /*if (!ImGui::Begin("Lose", &p_open, window_flags))
    {
      // Early out if the window is collapsed, as an optimization.
      ImGui::End();
      return;
    }*/

    //ImGui::SetWindowFontScale(3);
    if (lose) {
      //ImGui::TextColored(ImVec4(1, 0, 0, 1), "YOU DIED");
      //EngineLua.setVisibleDeathUI(true);
			if (EnginePersistence.gameState != "gs_credits") 
				EnginePersistence.gameState = "gs_gameplay";
    }
    else {
      //ImGui::TextColored(ImVec4(0, 1, 0, 1), "YOU WIN");
    }
    //End code
    //ImGui::End();
    timer -= delta;
    if (timer <= 0.f)
    {
      //EngineLua.setVisibleDeathUI(false);
      //CEngine::get().getModules().changeToGamestate("gs_reset");
      //EnginePersistence.resetPlayerPos();
      timer = 3.f;
    }
  }


  //ImGui::SetNextWindowPos(ImVec2(x_pos, y_pos));
  //ImGui::SetNextWindowSize(ImVec2(Render.width / 4, Render.height / 8));
  //Reticle Start
  /*ImVec2 _reticleWindowSize = ImVec2(windowSize, windowSize);

  ImGui::GetStyle().WindowPadding = ImVec2(0, 0);
  ImGui::GetStyle().WindowRounding = 0;
  ImGui::GetStyle().WindowBorderSize = 1;

  float posX = (Render.width / 2 - _reticleWindowSize.x);
  float posY = (Render.width / 2 - _reticleWindowSize.y);

  ImGui::SetNextWindowPosCenter(ImGuiCond_Always);
  ImGui::SetNextWindowSize(_reticleWindowSize, ImGuiCond_Always);

  ImGui::Begin("reticle", nullptr, window_flags);

  ImDrawList* drawList = ImGui::GetWindowDrawList();
  const ImVec2 p = ImGui::GetCursorScreenPos();
  const ImU32 col32 = ImColor(col);
  float x = p.x, y = p.y;

  if (type == CrosshairType::Neutral or type == CrosshairType::Surfaces)
  {
    drawList->AddCircle(ImVec2(x + windowSize * 0.5f, y + windowSize * 0.5f), sizeExteriorCross * currExteriorMult, col32, 32, thick);
    drawList->AddCircleFilled(ImVec2(x + windowSize * 0.5f, y + windowSize * 0.5f), sizeInteriorCross, col32, 32);
  }
  else if (type == CrosshairType::Objects)
  {
    drawList->AddRect(
      ImVec2(x + windowSize * 0.5f - (sizeExteriorCross * currExteriorMult), y + windowSize * 0.5f - (sizeExteriorCross * currExteriorMult)),
      ImVec2(x + windowSize * 0.5f + (sizeExteriorCross * currExteriorMult), y + windowSize * 0.5f + (sizeExteriorCross * currExteriorMult)), col32, 1.0f, 15, thick);
    drawList->AddCircleFilled(ImVec2(x + windowSize * 0.5f, y + windowSize * 0.5f), sizeInteriorCross, col32, 32);
  }
  else if (type == CrosshairType::Enemies)
  {
    drawList->AddTriangle(
      ImVec2(x + windowSize * 0.5f, y + windowSize * 0.5f - (sizeExteriorCross * currExteriorMult)),
      ImVec2(x + windowSize * 0.5f - (sizeExteriorCross * currExteriorMult), y + windowSize * 0.5f + (sizeExteriorCross)),
      ImVec2(x + windowSize * 0.5f + (sizeExteriorCross * currExteriorMult), y + windowSize * 0.5f + (sizeExteriorCross)), col32, 2 * thick);
    drawList->AddCircleFilled(ImVec2(x + windowSize * 0.5f, y + windowSize * 0.5f), sizeInteriorCross, col32, 32);
  }
  float posx = x + windowSize * 0.5f;
  float posy = y + windowSize * 0.5f;

  ImGui::End();*/
  //Reticle Stop

  // TUTORIAL
  //if (showTutorial)
    //displayTutorialMsg();
}

//function to draw a progress bar
float TCompUI::ProgressBar(const char *optionalPrefixText, float value, const float minValue, const float maxValue, const char *format, const ImVec2 &sizeOfBarWithoutTextInPixels, const ImVec4 &colorLeft, const ImVec4 &colorRight, const ImVec4 &colorBorder) {
  if (value < minValue) value = minValue;
  else if (value > maxValue) value = maxValue;
  const float valueFraction = (maxValue == minValue) ? 1.0f : ((value - minValue) / (maxValue - minValue));
  const bool needsPercConversion = strstr(format, "%%") != NULL;

  ImVec2 size = sizeOfBarWithoutTextInPixels;
  if (size.x <= 0) size.x = ImGui::GetWindowWidth()*0.25f;
  if (size.y <= 0) size.y = ImGui::GetTextLineHeightWithSpacing(); // or without

  const ImFontAtlas* fontAtlas = ImGui::GetIO().Fonts;

  if (optionalPrefixText && strlen(optionalPrefixText) > 0) {
    ImGui::AlignFirstTextHeightToWidgets();
    ImGui::Text(optionalPrefixText);
    ImGui::SameLine();
  }

  if (valueFraction > 0) {
    ImGui::Image(fontAtlas->TexID, ImVec2(size.x*valueFraction, size.y), fontAtlas->TexUvWhitePixel, fontAtlas->TexUvWhitePixel, colorLeft, colorBorder);
  }
  if (valueFraction < 1) {
    if (valueFraction > 0) ImGui::SameLine(0, 0);
    ImGui::Image(fontAtlas->TexID, ImVec2(size.x*(1.f - valueFraction), size.y), fontAtlas->TexUvWhitePixel, fontAtlas->TexUvWhitePixel, colorRight, colorBorder);
  }
  ImGui::SameLine();

  ImGui::Text(format, needsPercConversion ? (valueFraction*100.f + 0.0001f) : value);
  return valueFraction;
}

void TCompUI::SetCrosshair(float size, float thickness, VEC4 color, CrosshairType t)
{
  sizeExteriorCross = size;
  thick = thickness;
  col = ImVec4(color.x, color.y, color.z, color.w);
  type = t;
}

void TCompUI::SetCrosshair(float size, float thickness, CrosshairType t)
{
  sizeExteriorCross = size;
  thick = thickness;
  type = t;
}

void TCompUI::SetCrosshair(VEC4 color, bool hasTarget, CrosshairType t)
{
  if (hasTarget) currExteriorMult = multOnFocused;
  else currExteriorMult = 1.0f;
  col = ImVec4(color.x, color.y, color.z, color.w);
  type = t;
}

void TCompUI::UpdateReticle()
{
	CEntity* player = getEntityByName("Player");
	TCompPlayerMagic* magic = player->get<TCompPlayerMagic>();
	distanceObjective = magic->getDistance();
	distanceObjective = magic->getMaxDistance() / distanceObjective;
	distanceCursor = clampFloat(distanceObjective, 1, 6);

  if (type == CrosshairType::Neutral && (currentArrowPosLeft != pointingArrowPosLeft || currentArrowPosRight != pointingArrowPosRight)) {
    UI::CWidget* ReticleLeftArrowWidget = dynamic_cast<UI::CWidget*>(Engine.getUI().getWidgetByAlias("ReticleLeftArrow"));
    UI::CWidget* ReticleRightArrowWidget = dynamic_cast<UI::CWidget*>(Engine.getUI().getWidgetByAlias("ReticleRightArrow"));

    ReticleLeftArrowWidget->getParams()->position = VEC2::Lerp(ReticleLeftArrowWidget->getParams()->position, standarArrowPosLeft, 15 * deltaT);
    ReticleRightArrowWidget->getParams()->position = VEC2::Lerp(ReticleRightArrowWidget->getParams()->position, standarArrowPosRight, 15 * deltaT);

    currentArrowPosLeft == ReticleLeftArrowWidget->getParams()->position;
    currentArrowPosRight == ReticleRightArrowWidget->getParams()->position;

    ReticleLeftArrowWidget->updateTransform();
    ReticleRightArrowWidget->updateTransform();
  }
  else if ((type == CrosshairType::Surfaces || type == CrosshairType::Objects || type == CrosshairType::Enemies) && (currentArrowPosLeft != pointingArrowPosLeft || currentArrowPosRight != pointingArrowPosRight))
  {
    UI::CWidget* ReticleLeftArrowWidget = dynamic_cast<UI::CWidget*>(Engine.getUI().getWidgetByAlias("ReticleLeftArrow"));
    UI::CWidget* ReticleRightArrowWidget = dynamic_cast<UI::CWidget*>(Engine.getUI().getWidgetByAlias("ReticleRightArrow"));

    ReticleLeftArrowWidget->getParams()->position = VEC2::Lerp(ReticleLeftArrowWidget->getParams()->position, pointingArrowPosLeft * distanceCursor, 10 * deltaT);
    ReticleRightArrowWidget->getParams()->position = VEC2::Lerp(ReticleRightArrowWidget->getParams()->position, pointingArrowPosRight * distanceCursor, 10 * deltaT);

    currentArrowPosLeft == ReticleLeftArrowWidget->getParams()->position;
    currentArrowPosRight == ReticleRightArrowWidget->getParams()->position;

    ReticleLeftArrowWidget->updateTransform();
    ReticleRightArrowWidget->updateTransform();
  }

  lastType = type;
}


void TCompUI::UpdateCrystalPower() {
  UI::CImage* pushImage = dynamic_cast<UI::CImage*>(Engine.getUI().getWidgetByAlias("push_cristal"));
  if (pushImage)
    pushImage->setImageAlpha(1 * push_val);

  UI::CImage* pullImage = dynamic_cast<UI::CImage*>(Engine.getUI().getWidgetByAlias("pull_cristal"));
  if (pullImage)
    pullImage->setImageAlpha(1 * pull_val);

  float startRecoverGlowPush = 0.75;
  float startRecoverGlowPull = 0.75;

  UI::CImage* glowPushImage = dynamic_cast<UI::CImage*>(Engine.getUI().getWidgetByAlias("glow_push_cristal"));
  if (glowPushImage && push_val > startRecoverGlowPush)
    glowPushImage->setImageAlpha(1 * (push_val - startRecoverGlowPush) * (1 / (1 - startRecoverGlowPush)));

  UI::CImage* glowPullImage = dynamic_cast<UI::CImage*>(Engine.getUI().getWidgetByAlias("glow_pull_cristal"));
  if (glowPullImage && pull_val > startRecoverGlowPull)
    glowPullImage->setImageAlpha(1 * (pull_val - startRecoverGlowPull) * (1 / (1 - startRecoverGlowPull)));

  CEntity *player = getEntityByName("Player");
  TCompGloveGlow *glGlow = player->get< TCompGloveGlow >();
  float f1 = pull_val / pull_cooldown;
  float f2 = push_val / push_cooldown;
  glGlow->setCurrentGlowPull(f1);
  glGlow->setCurrentGlowPush(f2);
}


void TCompUI::TurnOffGlow()
{
  if (recoverPush) {
    UI::CImage* glowPushImage = dynamic_cast<UI::CImage*>(Engine.getUI().getWidgetByAlias("glow_push_cristal"));
    if (glowPushImage)
      glowPushImage->setImageAlpha(0);
  }

  if (recoverPull) {
    UI::CImage* glowPullImage = dynamic_cast<UI::CImage*>(Engine.getUI().getWidgetByAlias("glow_pull_cristal"));
    if (glowPullImage)
      glowPullImage->setImageAlpha(0);
  }
}
