#include "mcv_platform.h"
#include "handsSet.h"
#include "components/common/comp_transform.h"
#include "components/common/comp_camera.h"
#include "engine.h"

DECL_OBJ_MANAGER("hands_set", TCompHandSet);

void TCompHandSet::update(float dt)
{
	changePositions(dt);
	TCompTransform* myTransform = get<TCompTransform>();

	if (lockedToCamera)
	{
		CHandle cam = getEntityByName("Camera");
		CEntity* camEntity = (CEntity*)cam;
		TCompTransform* camTransform = camEntity->get<TCompTransform>();
		if (camTransform && myTransform)
		{
			float yaw, pitch;
			camTransform->getAngles(&yaw, &pitch);
			myTransform->setAngles(yaw, pitch - extraPitch, 0.0f);
			myTransform->setPosition(camTransform->getPosition()
				+ (camTransform->getFront() * frontOffset) + (camTransform->getUp() * extraHeight) +
				(camTransform->getLeft() * lateralOffset));
		}
	}
}

void TCompHandSet::load(const json & j, TEntityParseContext & ctx)
{
	frontOffset = j.value("frontOffset", frontOffset);
	lateralOffset = j.value("lateralOffset", lateralOffset);
	extraHeight = j.value("extraHeight", extraHeight);
	extraPitch = j.value("extraPitch", extraPitch);
	new_frontOffset = j.value("frontOffset", frontOffset);
	new_lateralOffset = j.value("lateralOffset", lateralOffset);
	new_extraHeight = j.value("extraHeight", extraHeight);
	new_extraPitch = j.value("extraPitch", extraPitch);
}

void TCompHandSet::debugInMenu()
{
	ImGui::DragFloat("frontOffset", &new_frontOffset, 0.05f, -5.0f, -5.0f);
	ImGui::DragFloat("lateralOffset", &new_lateralOffset, 0.05f, -5.0f, -5.0f);
	ImGui::DragFloat("height", &new_extraHeight, 0.05f, -5.0f, -5.0f);
	ImGui::DragFloat("pitch", &new_extraPitch, 0.05f, -5.0f, -5.0f);
	ImGui::DragFloat("speed", &speed, 0.01f, -5.0f, -5.0f);
}

void TCompHandSet::changePositions(float dt) {
	frontOffset = lerpFloat(frontOffset, new_frontOffset, speed * dt);
	lateralOffset = lerpFloat(lateralOffset, new_lateralOffset, speed * dt);
	extraHeight = lerpFloat(extraHeight, new_extraHeight, speed * dt);
	extraPitch = lerpFloat(extraPitch, new_extraPitch, speed * dt);
	//Progressively update positions relative to dt
	/*if (new_frontOffset != frontOffset) {
		if (new_frontOffset < frontOffset) {
			frontOffset -= dt * speed;
			if (frontOffset < new_frontOffset) frontOffset = new_frontOffset;
		}
		else {
			frontOffset += dt * speed;
			if (frontOffset > new_frontOffset) frontOffset = new_frontOffset;
		}
	}
	if (new_lateralOffset != lateralOffset) {
		if (new_lateralOffset < lateralOffset) {
			lateralOffset -= dt * speed;
			if (lateralOffset < new_lateralOffset) lateralOffset = new_lateralOffset;
		}
		else {
			lateralOffset += dt * speed;
			if (lateralOffset > new_lateralOffset) lateralOffset = new_lateralOffset;
		}
	}
	if (new_extraHeight != extraHeight) {
		if (new_extraHeight < extraHeight) {
			extraHeight -= dt * speed;
			if (extraHeight < new_extraHeight) extraHeight = new_extraHeight;
		}
		else {
			extraHeight += dt * speed;
			if (extraHeight > new_extraHeight) extraHeight = new_extraHeight;
		}
	}
	if (new_extraPitch != extraPitch) {
		if (new_extraPitch < extraPitch) {
			extraPitch -= dt * speed;
			if (extraPitch < new_extraPitch) extraPitch = new_extraPitch;
		}
		else {
			extraPitch += dt * speed;
			if (extraPitch > new_extraPitch) extraPitch = new_extraPitch;
		}
	}*/
}