#include "mcv_platform.h"
#include "comp_light_point.h"
#include "components/common/comp_transform.h"
#include "render/render_manager.h" 
#include "entity/entity_parser.h"
#include "engine.h"
#include <fstream>
#include <iomanip>

DECL_OBJ_MANAGER("light_point", TCompLightPoint);

// Use nvidia photoshop plugin (dds) to convert the RGBTable16x1.dds recolored to a dds volume texture 
// Volume Texture
// No Mipmaps
// 8.8.8.8 ARGB 32 bpp

DXGI_FORMAT readFormat(const json& j, const std::string& label);

void TCompLightPoint::registerMsgs() {
	DECL_MSG(TCompLightPoint, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompLightPoint, TMsgDestroy, onDestroy);
	DECL_MSG(TCompLightPoint, TMsgAlarmClock, onAlarm);
}


void TCompLightPoint::onEntityCreated(const TMsgEntityCreated& msg) {
	TCompName *n = get<TCompName>();
	entity_name = n->getName();
}

void TCompLightPoint::onDestroy(const TMsgDestroy& msg) {
	EngineAlarms.AddAlarm(0.3, 3333, CHandle(this));
}

void TCompLightPoint::onAlarm(const TMsgAlarmClock& msg) {
	CHandle(this).getOwner().destroy();
}

// -------------------------------------------------
void TCompLightPoint::debugInMenu() {
  ImGui::ColorEdit3("Color", &color.x);
  ImGui::Checkbox("Flicker", &flicker);
  if (flicker) {
    ImGui::DragInt("Smoothness", &smoothness, 0.1f, 0, 75);
    ImGui::DragFloat("Min. Intensity", &min_intensity, 0.1f, 0.f, 75.f);
    ImGui::DragFloat("Max. Intensity", &max_intensity, 0.1f, 0.f, 75.f);
    ImGui::DragFloat("Min. Radius", &min_radius, 0.1f, 0.f, 20.f);
    ImGui::DragFloat("Max. Max Radius", &max_radius, 0.1f, 0.f, 20.f);
  }
  else
  {
    ImGui::DragFloat("Radius", &radius, 0.1f, 0.f, 300.f);
    ImGui::DragFloat("Intensity", &intensity, 0.1f, 0.f, 75.f);
  }
	ImGui::DragFloat3("Offset", &offset.x, 0.1f, -1000.0f, 1000.0f);
	if (ImGui::Button("Save To JSON"))
		saveToJSON();
}

MAT44 TCompLightPoint::getWorld() {
	TCompTransform* c = get<TCompTransform>();
	if (!c)
		return MAT44::Identity;
	return MAT44::CreateScale(radius) * MAT44::CreateWorld(c->getPosition() + offset, c->getFront(), c->getUp());
}

// -------------------------------------------------
void TCompLightPoint::renderDebug() {
	TCompTransform* t = get<TCompTransform>();
	drawWiredSphere(t->getPosition() + offset, radius, color);
}

// -------------------------------------------------
void TCompLightPoint::load(const json& j, TEntityParseContext& ctx) {
	if (ctx.parsing_prefab) {
		json_filename = ctx.parent->filename;
	}
	else {
		if (json_filename == "") json_filename = ctx.filename;
	}
	color = loadColor(j, "color");
	flicker = j.value("flicker", false);
	radius = j.value("radius", radius);
	intensity = j.value("intensity", intensity);
	min_intensity = j.value("min_intensity", 0.0f);
	max_intensity = j.value("max_intensity", 0.0f);
	min_radius = j.value("min_radius", 5.0f);
	max_radius = j.value("max_radius", 6.0f);
	smoothness = j.value("smoothness", 15);
	offset = loadVEC3(j, "offset");

	if (flicker)
	{
		for (int i = 0; i < smoothness; ++i)
		{
			float n = randomFloat(min_intensity, max_intensity);
      float r = randomFloat(min_radius, max_radius);
			totalIntensitySum += n;
      totalRadiusSum += r;
      intensityQueue.push(n);
      radiusQueue.push(r);
		}
	}
}

// -------------------------------------------------
// Updates the Shader Cte Light with MY information
void TCompLightPoint::activate() {
	TCompTransform* c = get<TCompTransform>();
	if (!c) return;

	if (flicker)
	{
		float i = intensityQueue.front();
    intensityQueue.pop();
    totalIntensitySum -= i;

		i = randomFloat(min_intensity, max_intensity);
    totalIntensitySum += i;
    intensityQueue.push(i);

		intensity = totalIntensitySum / (float)intensityQueue.size();

    float r = radiusQueue.front();
    radiusQueue.pop();
    totalRadiusSum -= r;

    r = randomFloat(min_radius, max_radius);
    totalRadiusSum += r;
    radiusQueue.push(r);

    radius = totalRadiusSum / (float)radiusQueue.size();
	}

	ctes_light.LightColor = color;
	ctes_light.LightIntensity = intensity;
	ctes_light.LightPosition = c->getPosition() + offset;
	ctes_light.LightRadius = radius * c->getScale();
	ctes_light.LightViewProjOffset = MAT44::Identity;
	ctes_light.updateGPU();
}


void TCompLightPoint::saveToJSON(){
	auto jData = loadJson(json_filename);
	auto& jEntry = jData;

	auto& EntireEntities = jEntry;

	for (int i = 0; i < EntireEntities.size(); i++) {
		auto& jEntityToModify = EntireEntities[i];
		auto& jEntity = jEntityToModify["entity"];
		auto& jName = jEntity["name"];
		if (jName.size() >= 2) {
			if (jName["entityName"] == entity_name) {
				saveEntityDataJson(jEntity);
				break;
			}
		}else if (jName == entity_name) {
			saveEntityDataJson(jEntity);
			break;
		}
	}
	std::ofstream outputFileStream(json_filename);
	outputFileStream << std::setw(4) << jData << std::endl;
}

void TCompLightPoint::saveEntityDataJson(json& jEntity) {
	jEntity["light_point"]["intensity"] = intensity;
	jEntity["light_point"]["radius"] = radius;
	jEntity["light_point"]["color"] = std::to_string(color.x) + " " + std::to_string(color.y) + " " + std::to_string(color.z);
	jEntity["light_point"]["offset"] = std::to_string(offset.x) + " " + std::to_string(offset.y) + " " + std::to_string(offset.z);
	if (flicker) {
		jEntity["light_point"]["flicker"] = true;
		jEntity["light_point"]["smoothness"] = smoothness;
		jEntity["light_point"]["min_intensity"] = min_intensity;
		jEntity["light_point"]["max_intensity"] = max_intensity;
	}
	else {
		jEntity["light_point"].erase("flicker");
		jEntity["light_point"].erase("smoothness");
		jEntity["light_point"].erase("min_intensity");
		jEntity["light_point"].erase("max_intensity");
	}
}


