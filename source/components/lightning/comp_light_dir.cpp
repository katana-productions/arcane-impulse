#include "mcv_platform.h"
#include "comp_light_dir.h"
#include "components/common/comp_transform.h"
#include "render/textures/texture.h" 
#include "render/textures/render_to_texture.h" 
#include "render/render_manager.h" 

DECL_OBJ_MANAGER("light_dir", TCompLightDir);

DXGI_FORMAT readFormat(const json& j, const std::string& label);

// -------------------------------------------------
void TCompLightDir::debugInMenu() {
  TCompCamera::debugInMenu();
  ImGui::DragFloat("Intensity", &intensity, 0.01f, 0.f, 10.f);
  ImGui::ColorEdit3("Color", &color.x);
  ImGui::DragFloat("Step Size", &shadows_step, 0.01f, 0.f, 5.f);
}

// -------------------------------------------------
void TCompLightDir::renderDebug() {
  TCompCamera::renderDebug();
}

// -------------------------------------------------
void TCompLightDir::load(const json& j, TEntityParseContext& ctx) {
  TCompCamera::load( j, ctx );
  if(j.count("color"))
    color = loadColor(j, "color");
  intensity = j.value("intensity", intensity);

  if( j.count("projector")) {
    std::string projector_name = j.value("projector", "");
    projector = Resources.get(projector_name)->as<CTexture>();
  }

  // Check if we need to allocate a shadow map
  casts_shadows = j.value("casts_shadows", false);
  if (casts_shadows) {
    shadows_step = j.value("shadows_step", shadows_step);
    shadows_resolution = j.value("shadows_resolution", shadows_resolution);
    auto shadowmap_fmt = readFormat(j, "shadows_fmt");
    assert(shadows_resolution > 0);
    shadows_rt = new CRenderToTexture;
    // Make a unique name to have the Resource Manager happy with the unique names for each resource
    char my_name[64];
    sprintf(my_name, "shadow_map_%08x", CHandle(this).asUnsigned());
    
    // Added a placeholder Color Render Target to be able to do a alpha test when rendering
    // the grass
    bool is_ok = shadows_rt->createRT(my_name, shadows_resolution, shadows_resolution, DXGI_FORMAT_R8G8B8A8_UNORM, shadowmap_fmt);
    assert(is_ok);
  }

  shadows_enabled = casts_shadows;
}

void TCompLightDir::update(float dt) {
  // Can't use the TCompCamera::update because inside it calls 
  // get<TCompTransform> which tries to convert 'this' to an instance
  // of TCompCamera, but will fail because we are a CompLightDir
  TCompTransform* c = get<TCompTransform>();
  if (!c)
    return;
  this->lookAt(c->getPosition(), c->getPosition() + c->getFront(), c->getUp());
}

// -------------------------------------------------
// Updates the Shader Cte Light with MY information
void TCompLightDir::activate() {
  TCompTransform* c = get<TCompTransform>();
  if (!c)
    return;

  if (projector)
    projector->activate(TS_PROJECTOR);

  // To avoid converting the range -1..1 to 0..1 in the shader
  // we concatenate the view_proj with a matrix to apply this offset
  MAT44 mtx_offset = MAT44::CreateScale(VEC3(0.5f, -0.5f, 1.0f))
                   * MAT44::CreateTranslation(VEC3(0.5f, 0.5f, 0.0f));
                  
  ctes_light.LightColor = color;
  ctes_light.LightIntensity = intensity;
  ctes_light.LightPosition = c->getPosition();
  ctes_light.LightRadius = getFar();
  ctes_light.LightViewProjOffset = getViewProjection() * mtx_offset;
  ctes_light.LightFront = c->getFront();

  // If we have a ZTexture, it's the time to activate it
  if (shadows_rt) {

    ctes_light.LightShadowInverseResolution = 1.0f / (float)shadows_rt->getWidth();
    ctes_light.LightShadowStep = shadows_step;
    ctes_light.LightShadowStepDivResolution = shadows_step / (float)shadows_rt->getWidth();
    ctes_light.LightDummy2 = 1.0f;

    assert(shadows_rt->getZTexture());
    shadows_rt->getZTexture()->activate(TS_LIGHT_SHADOW_MAP);
  }
  
  ctes_light.updateGPU();
}


// ------------------------------------------------------
void TCompLightDir::generateShadowMap() {
  PROFILE_FUNCTION("ShadowMap");
  if (!shadows_rt || !shadows_enabled )
    return;

  // In this slot is where we activate the render targets that we are going
  // to update now. You can't be active as texture and render target at the
  // same time
  CTexture::setNullTexture(TS_LIGHT_SHADOW_MAP);

  CGpuScope gpu_scope(shadows_rt->getName().c_str());
  shadows_rt->activateRT();

  {
    PROFILE_FUNCTION("Clear&SetCommonCtes");
    shadows_rt->clearZ();
    // We are going to render the scene from the light position & orientation
    activateCamera(*this, shadows_rt->getWidth(), shadows_rt->getHeight());
  }

  CRenderManager::get().render(CATEGORY_SHADOWS);
}


