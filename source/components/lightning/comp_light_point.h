#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"

class TCompLightPoint : public TCompBase {

	// Light params
	VEC4            color;
	float           intensity = 1.0f;
	float           radius = 1.0f;
	VEC3            offset;

	bool            flicker;
	float           min_intensity;
	float           max_intensity;
  float           min_radius;
  float           max_radius;
	int             smoothness;

  std::queue<float> intensityQueue;
  std::queue<float> radiusQueue;
	float           totalIntensitySum = 0.0f;
  float           totalRadiusSum = 0.0f;
	std::string		  json_filename = "";
	std::string		  entity_name = "";

	

public:
	void debugInMenu();
	void renderDebug();
	void load(const json& j, TEntityParseContext& ctx);
	void saveToJSON();
	void saveEntityDataJson(json& jEntity);
	DECL_SIBLING_ACCESS();
	static void registerMsgs();
	void onEntityCreated(const TMsgEntityCreated& msg);
	void onDestroy(const TMsgDestroy& msg);
	void onAlarm(const TMsgAlarmClock& msg);

	void activate();
	MAT44 getWorld();
};