#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "render/textures/render_to_texture.h"

class TCompLightPointShadows : public TCompBase {

private:
  // Light params
  VEC4            color = VEC4(1, 1, 1, 1);
  VEC3            offset;
  float           intensity = 1.0f;
  float           radius = 1.0f;
  const CTexture* projector = nullptr;

  bool            flicker;
  float           min_intensity;
  float           max_intensity;
  float           min_radius;
  float           max_radius;
  int             smoothness;

  std::queue<float> intensityQueue;
  std::queue<float> radiusQueue;
  float           totalIntensitySum = 0.0f;
  float           totalRadiusSum = 0.0f;

  // Shadows params
  bool              shadows_enabled = false;    // Dynamic
  bool              casts_shadows = false;      // Static
  int               shadows_resolution = 256;
  float             shadows_step = 1.f;
  static const int  nsides = 6;
  CRenderToTexture  *shadows_render_cube[nsides];
  CCamera           *shadow_views[nsides];

public:
  DECL_SIBLING_ACCESS();
  ~TCompLightPointShadows();
  void debugInMenu();
  void renderDebug();
  void load(const json& j, TEntityParseContext& ctx);

  void update(float dt);

  void generateShadowMapSide(int i);
  void activateSide(int i);
  MAT44 getWorld();
  int getNSides() const { return nsides; }
  CCamera* getShadowView(int i) { return shadow_views[i]; }
};