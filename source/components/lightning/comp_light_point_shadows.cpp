#include "mcv_platform.h"
#include "comp_light_point_shadows.h"
#include "components/common/comp_transform.h"
#include "render/render_manager.h" 

DECL_OBJ_MANAGER("light_point_shadows", TCompLightPointShadows);

DXGI_FORMAT readFormat(const json& j, const std::string& label);

TCompLightPointShadows::~TCompLightPointShadows()
{
  //HERE WE SAFE RELEASE EVERYTHING NEEDED
  /*for (int i = 0; i < nsides; ++i)
  {
    delete shadows_render_cube[i];
    delete shadow_views[i];
  }
  delete shadows_render_cube;
  delete shadow_views;
  delete projector;*/
}

// -------------------------------------------------
void TCompLightPointShadows::debugInMenu() {
  ImGui::ColorEdit3("Color", &color.x);
  ImGui::Checkbox("Flicker", &flicker);
  if (flicker) {
    ImGui::DragInt("Smoothness", &smoothness, 0.1f, 0, 75);
    ImGui::DragFloat("Min. Intensity", &min_intensity, 0.1f, 0.f, 75.f);
    ImGui::DragFloat("Max. Intensity", &max_intensity, 0.1f, 0.f, 75.f);
    ImGui::DragFloat("Min. Radius", &min_radius, 0.1f, 0.f, 20.f);
    ImGui::DragFloat("Max. Max Radius", &max_radius, 0.1f, 0.f, 20.f);
  }
  else
  {
    ImGui::DragFloat("Radius", &radius, 0.1f, 0.f, 300.f);
    ImGui::DragFloat("Intensity", &intensity, 0.1f, 0.f, 75.f);
  }
  ImGui::DragFloat("Step Size", &shadows_step, 0.01f, 0.f, 5.f);
  ImGui::DragFloat3("Offset", &offset.x, 0.1f, -1000.0f, 1000.0f);

  for (int i = 0; i < nsides; ++i)
  {
    float fov_deg = rad2deg(shadow_views[i]->getFov());
    float new_znear = shadow_views[i]->getNear();
    float new_zfar = shadow_views[i]->getFar();

    bool changed = ImGui::DragFloat("Fov", &fov_deg, 0.1f, 30.f, 175.f);

    changed |= ImGui::DragFloat("Z Near", &new_znear, 0.001f, 0.01f, 5.0f);
    changed |= ImGui::DragFloat("Z Far", &new_zfar, 1.0f, 2.0f, 3000.0f);

    if (changed && new_znear > 0.f && new_znear < new_zfar) {
      shadow_views[i]->setProjectionParams(deg2rad(fov_deg), new_znear, new_zfar);
    }

    VEC3 f = shadow_views[i]->getFront();
    ImGui::LabelText("Front", "%f %f %f", f.x, f.y, f.z);
    VEC3 up = shadow_views[i]->getUp();
    ImGui::LabelText("Up", "%f %f %f", up.x, up.y, up.z);
    VEC3 left = shadow_views[i]->getLeft();
    ImGui::LabelText("Left", "%f %f %f", left.x, left.y, left.z);
  }  
}
/*
MAT44 TCompLightPointShadows::getWorld() {
  TCompTransform* c = get<TCompTransform>();
  if (!c)
    return MAT44::Identity;
  return MAT44::CreateScale(radius * 1.05f) * MAT44::CreateTranslation(offset) * c->asMatrix();
}
*/

// -------------------------------------------------
void TCompLightPointShadows::renderDebug() {
  // Render a wire sphere
  TCompTransform* c = get<TCompTransform>();
  if (!c) return;

  drawWiredSphere(c->getPosition() + offset, radius, VEC4(1, 1, 1, 1));

  for (int i = 0; i < nsides; ++i)
  {
    MAT44 inv_view_proj = shadow_views[i]->getViewProjection().Invert();

    // xy between -1..1 and z betwee 0 and 1
    auto mesh = Resources.get("unit_frustum.mesh")->as<CMesh>();

    //// Sample several times to 'view' the z distribution along the 3d space
    const int nsamples = 10;
    for (int i = 1; i < nsamples; ++i) {
      float f = (float)i / (float)(nsamples - 1);
      MAT44 world = MAT44::CreateScale(1.f, 1.f, f) * inv_view_proj;
      drawMesh(mesh, world, VEC4(1, 1, 1, 1));
    }
  }
}

// -------------------------------------------------
void TCompLightPointShadows::load(const json& j, TEntityParseContext& ctx) {
  
  color = loadColor(j, "color");
  flicker = j.value("flicker", false);
  radius = j.value("radius", radius);
  intensity = j.value("intensity", intensity);
  min_intensity = j.value("min_intensity", 0.0f);
  max_intensity = j.value("max_intensity", 0.0f);
  min_radius = j.value("min_radius", 0.0f);
  max_radius = j.value("max_radius", 0.0f);
  smoothness = j.value("smoothness", 15);
  offset = loadVEC3(j, "offset");

  if (flicker)
  {
    for (int i = 0; i < smoothness; ++i)
    {
      float n = randomFloat(min_intensity, max_intensity);
      float r = randomFloat(min_radius, max_radius);
      totalIntensitySum += n;
      totalRadiusSum += r;
      intensityQueue.push(n);
      radiusQueue.push(r);
    }
  }

  if (j.count("projector")) {
    std::string projector_name = j.value("projector", "");
    projector = Resources.get(projector_name)->as<CTexture>();
  }

  // Check if we need to allocate a shadow map for each side of the cube
  casts_shadows = j.value("casts_shadows", false);
  if (casts_shadows) {
    shadows_step = j.value("shadows_step", shadows_step);
    shadows_resolution = j.value("shadows_resolution", shadows_resolution);
    auto shadowmap_fmt = readFormat(j, "shadows_fmt");
    assert(shadows_resolution > 0);

    //Load camera values for all sides
    CCamera cam;
    float znear = j.value("near", 1.0f);
    float zfar = j.value("far", 10.0f);
    //does it make sense to make all views ortho?
    /*if (j.value("ortho", false)) {
      float new_width = j.value("ortho_width", 4.0f);
      float new_height = j.value("ortho_height", 4.0f);
      float new_left = j.value("ortho_left", 4.0f);
      float new_top = j.value("ortho_top", 4.0f);
      bool centered = j.value("ortho_centered", true);
      cam.setOrthoParams(centered, new_left, new_width, new_top, new_height, znear, zfar);
    }
    else {*/
    float fov = deg2rad(j.value("fov", rad2deg(90)));
    cam.setProjectionParams(fov, znear, zfar);
    //}

    // Create a render texture for each side
    for (int i = 0; i < nsides; ++i)
    {
      shadows_render_cube[i] = new CRenderToTexture;
      shadow_views[i] = new CCamera(cam);
      // Make a unique name to have the Resource Manager happy with the unique names for each resource
      char my_name[64];
      sprintf(my_name, "shadow_map_%08x", CHandle(this).asUnsigned());
      std::string n = my_name;
      std::string shadow_name = n + std::to_string(i);

      // Added a placeholder Color Render Target to be able to do a alpha test when rendering
      // the grass
      bool is_ok = shadows_render_cube[i]->createRT(shadow_name.c_str(), shadows_resolution, shadows_resolution, DXGI_FORMAT_R8G8B8A8_UNORM, shadowmap_fmt);
      assert(is_ok);
    }
  }
  shadows_enabled = casts_shadows;
}

void TCompLightPointShadows::update(float dt)
{
  // Can't use the TCompCamera::update because inside it calls 
// get<TCompTransform> which tries to convert 'this' to an instance
// of TCompCamera, but will fail because we are a CompLightDir
  TCompTransform* c = get<TCompTransform>();
  if (!c)
    return;
  // Assign different fronts to all the cameras
  shadow_views[0]->lookAt(c->getPosition() + offset, c->getPosition() + offset + c->getFront(), c->getUp());
  shadow_views[1]->lookAt(c->getPosition() + offset, c->getPosition() + offset + c->getLeft(), c->getUp());
  shadow_views[2]->lookAt(c->getPosition() + offset, c->getPosition() + offset - c->getFront(), c->getUp());
  shadow_views[3]->lookAt(c->getPosition() + offset, c->getPosition() + offset - c->getLeft(), c->getUp());
  shadow_views[4]->lookAt(c->getPosition() + offset, c->getPosition() + offset + c->getUp(), c->getFront());
  shadow_views[5]->lookAt(c->getPosition() + offset, c->getPosition() + offset - c->getUp(), -c->getFront());
}

// -------------------------------------------------
// Updates the Shader Cte Light with MY information
void TCompLightPointShadows::activateSide(int i) {
  TCompTransform* c = get<TCompTransform>();
  if (!c)
    return;

  assert(i < nsides and i >= 0);

  if (projector)
    projector->activate(TS_PROJECTOR);

  if (flicker)
  {
    float i = intensityQueue.front();
    intensityQueue.pop();
    totalIntensitySum -= i;

    i = randomFloat(min_intensity, max_intensity);
    totalIntensitySum += i;
    intensityQueue.push(i);

    intensity = totalIntensitySum / (float)intensityQueue.size();

    float r = radiusQueue.front();
    radiusQueue.pop();
    totalRadiusSum -= r;

    r = randomFloat(min_radius, max_radius);
    totalRadiusSum += r;
    radiusQueue.push(r);

    radius = totalRadiusSum / (float)radiusQueue.size();
  }

  // To avoid converting the range -1..1 to 0..1 in the shader
  // we concatenate the view_proj with a matrix to apply this offset
  MAT44 mtx_offset = MAT44::CreateScale(VEC3(0.5f, -0.5f, 1.0f))
    * MAT44::CreateTranslation(VEC3(0.5f, 0.5f, 0.0f));

  ctes_light.LightColor = color;
  ctes_light.LightIntensity = intensity;
  ctes_light.LightPosition = c->getPosition() + offset;
  ctes_light.LightRadius = radius;

  ctes_light.LightViewProjOffset = shadow_views[i]->getViewProjection() * mtx_offset;
  ctes_light.LightFront = shadow_views[i]->getFront();

  // If we have a ZTexture, it's the time to activate it
  if (shadows_render_cube[i]) {

    ctes_light.LightShadowInverseResolution = 1.0f / (float)shadows_render_cube[i]->getWidth();
    ctes_light.LightShadowStep = shadows_step;
    ctes_light.LightShadowStepDivResolution = shadows_step / (float)shadows_render_cube[i]->getWidth();
    ctes_light.LightDummy2 = 1.0f;

    assert(shadows_render_cube[i]->getZTexture());
    shadows_render_cube[i]->getZTexture()->activate(TS_LIGHT_SHADOW_MAP);
  }
  ctes_light.updateGPU();
}

// ------------------------------------------------------
void TCompLightPointShadows::generateShadowMapSide(int i) {
  if (!shadows_enabled)
    return;

  assert(i < nsides and i >= 0);

  if (!shadows_render_cube[i] or !shadow_views[i])
    return;

  // In this slot is where we activate the render targets that we are going
  // to update now. You can't be active as texture and render target at the
  // same time
  CTexture::setNullTexture(TS_LIGHT_SHADOW_MAP);

  CGpuScope gpu_scope(shadows_render_cube[i]->getName().c_str());
  shadows_render_cube[i]->activateRT();

  {
    PROFILE_FUNCTION("Clear&SetCommonCtes");
    shadows_render_cube[i]->clearZ();
    // We are going to render the scene from the light position & orientation
    activateCamera(*shadow_views[i], shadows_render_cube[i]->getWidth(), shadows_render_cube[i]->getHeight());

    CRenderManager::get().render(CATEGORY_SHADOWS);
  }
}


