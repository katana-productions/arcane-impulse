#pragma once
#include "door_controller.h"
#include "entity/common_msgs.h"

class TCompDoorPlayerGoto : public TCompDoorController
{
private:
  std::string triggerOpen;

  CHandle doorUp;
  CHandle doorLeft;
  CHandle doorRight;

  void init(const TMsgEntityCreated &msg);
  void keyEntered(const TMsgPuzzleTriggerEntered& msg);

public:
  DECL_SIBLING_ACCESS();
  static void registerMsgs();
  void load(const json& j, TEntityParseContext& ctx);
  void renderDebug();
  void debugInMenu();
};

