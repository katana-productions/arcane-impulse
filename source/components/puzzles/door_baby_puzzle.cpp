#include "mcv_platform.h"
#include "door_baby_puzzle.h"
#include "components/common/comp_render.h"
#include "comp_puzzle_trigger.h"
#include "entity/entity.h"
#include "engine.h"

DECL_OBJ_MANAGER("door_baby_puzzle", TCompDoorBabyPuzzle);

void TCompDoorBabyPuzzle::init(const TMsgEntityCreated &msg)
{
	TCompDoorController::initBase(msg);

	/*TCompTransform *myTransform = get<TCompTransform>();
	TCompName *myName = get<TCompName>();

	// Open Trigger Instantiate
	VEC3 pos = myTransform->getPosition() + (myTransform->getFront() * 3);
	float y, p, r;
	myTransform->getAngles(&y, &p, &r);
	CEntity *tOpen = EngineBasics.Instantiate("data/prefabs/puzzles/triggers/door_trigger_instance.json", pos, y, p, r);
	TCompName *tName = tOpen->get<TCompName>();
	tName->setName(triggerOpen.c_str());
	TCompPuzzleTrigger *pTrigger = tOpen->get<TCompPuzzleTrigger>();
	pTrigger->puzzle_controller = myName->getName();*/
}

void TCompDoorBabyPuzzle::keyEntered(const TMsgPuzzleTriggerEntered& msg)
{
	TCompDoorController::keyEnteredBase(msg);

	CEntity *trigger = msg.h_trigger;
	CEntity *obj = msg.h_entity;
	std::string tName = trigger->getName();
	std::string kName = obj->getName();
	TCompRender *tRender = trigger->get<TCompRender>();
	std::map<std::string, std::string>::iterator itTrigObj = allTriggersObj.find(tName);

	if (playerCrossedDoor)
	{
		if (tName == triggerOpen and kName == "Player")
			setOpenDoor(true);
	}
  else if (itTrigObj != allTriggersObj.end())
  {
    std::map<std::string, int>::iterator itNumObj = numObjInTriggers.find(tName);
    std::map<std::string, bool>::iterator itOkObj = okObjExists.find(tName);
    std::string keyName = itTrigObj->second;
    if (kName == keyName)
    {

      int nObj = itNumObj->second;
      bool existsOk = itOkObj->second;

	  nObj = 1;
      itNumObj->second = nObj;

      //ZERO OBJ
      if (nObj == 0)
      {
        solvedTriggers[tName] = false;
        itOkObj->second = false;
        //tRender->color = VEC4(1, 1, 1, 1); //neutral
        tRender->glowAmount = 0.0f;
      }
      //ONLY ONE OBJ
      else if (nObj == 1)
      {
        //Correct obj
        if (keyName == kName)
        {
          solvedTriggers[tName] = true;
          itOkObj->second = true;
          //tRender->color = VEC4(0, 1, 0, 1); //green
          tRender->glowAmount = 2000.0f;
        }
        //Incorrect obj
        else
        {
          solvedTriggers[tName] = false;
          itOkObj->second = false;
          //tRender->color = VEC4(1, 0, 0, 1); //red
          tRender->glowAmount = 0.0f;
        }
      }
      //MORE OBJ
      else
      {
        //Correct obj
        if (keyName == kName)
        {
          solvedTriggers[tName] = false;
          itOkObj->second = true;
          //tRender->color = VEC4(1, 0, 0, 1); //red
          tRender->glowAmount = 0.0f;
        }
        //Incorrect obj
        else
        {
          solvedTriggers[tName] = false;
          //tRender->color = VEC4(1, 0, 0, 1); //red
          tRender->glowAmount = 0.0f;
        }
      }
    }
    checkSolved();
  }
}

void TCompDoorBabyPuzzle::keyExited(const TMsgPuzzleTriggerExited& msg)
{
	CEntity *trigger = msg.h_trigger;
	CEntity *obj = msg.h_entity;
	std::string tName = trigger->getName();
	std::string kName = obj->getName();
	TCompRender *tRender = trigger->get<TCompRender>();
	std::map<std::string, std::string>::iterator itTrigObj = allTriggersObj.find(tName);
  if (itTrigObj != allTriggersObj.end())
  {
    std::map<std::string, int>::iterator itNumObj = numObjInTriggers.find(tName);
    std::map<std::string, bool>::iterator itOkObj = okObjExists.find(tName);
    std::string keyName = itTrigObj->second;
    int nObj = itNumObj->second;
    bool existsOk = itOkObj->second;

    if (kName == keyName)
    {

      --nObj;
      itNumObj->second = nObj;

      //LAST OBJ
      if (nObj == 0)
      {
        solvedTriggers[tName] = false;
        itOkObj->second = false;
        // tRender->color = VEC4(1, 1, 1, 1); //neutral
        tRender->glowAmount = 0.0f;
      }
      //MORE OBJ
      else
      {
        //Correct obj
        if (keyName == kName)
        {
          solvedTriggers[tName] = false;
          itOkObj->second = true;
          //tRender->color = VEC4(1, 0, 0, 1); //red
          tRender->glowAmount = 0.0f;
        }
        //Incorrect obj
        else
        {
          if (itOkObj->second)
          {
            solvedTriggers[tName] = true;
            //tRender->color = VEC4(0, 1, 0, 1); //green
            tRender->glowAmount = 2000.0f;
          }
          else
          {
            solvedTriggers[tName] = false;
            //tRender->color = VEC4(1, 0, 0, 1); //green
            tRender->glowAmount = 2000.0f;
          }
        }
      }
    }
    checkSolved();
  }
}

void TCompDoorBabyPuzzle::checkSolved()
{
	if (!stayOpen)
	{
		if (!playerCrossedDoor)
		{
			TCompName* name = get<TCompName>();
			std::string nameDoor = name->getName();

			int nSolved = 0;
			for (auto it = solvedTriggers.begin(); it != solvedTriggers.end(); ++it)
				if (it->second) ++nSolved;

			if (nSolved == nTriggers / 2) {
				if (firstTime2 && nameDoor == "DoublePuzzleDoor") {
					EngineLua.playDialogue(18);
					firstTime2 = false;
				}
			}

			if (nSolved == nTriggers or stayOpen) {
				if (eventOpen != 0)
				{
					EngineLua.launchEvent(eventOpen);
				}
				else
				{
					setOpenDoor(true);
					if (solveOnce) stayOpen = true;

					if (firstTime && nameDoor == "Cst_Gate_Puzzle") {
						EngineLua.playDialogue(11);
            EngineLua.setRandomCamShake(0.2f, 1.0f, 0.2f);
						firstTime = false;
					}

					if (firstTime3 && nameDoor == "DoublePuzzleDoor") {
						EngineLua.playDialogue(19);
            EngineLua.setRandomCamShake(0.1f, 1.0f, 0.1f);
						firstTime3 = false;
					}
				}
			}
			else setOpenDoor(false);
		}
	}
}

void TCompDoorBabyPuzzle::registerMsgs()
{
	DECL_MSG(TCompDoorBabyPuzzle, TMsgEntityCreated, init);
	DECL_MSG(TCompDoorBabyPuzzle, TMsgPuzzleTriggerEntered, keyEntered);
	DECL_MSG(TCompDoorBabyPuzzle, TMsgPuzzleTriggerExited, keyExited);
}

void TCompDoorBabyPuzzle::load(const json& j, TEntityParseContext& ctx)
{
	TCompDoorController::load(j, ctx);

	triggerOpen = j.value("trigger_open", "");

	//Load triggers
	json triggers = j["all_triggers"];
	for (json::iterator it = triggers.begin(); it != triggers.end(); ++it) {
		std::string trig = it.key().c_str();
		std::string obj = it.value().get<std::string>();

		allTriggersObj[trig] = obj;
		numObjInTriggers[trig] = 0;
		okObjExists[trig] = false;
		solvedTriggers[trig] = false;
	}
	nTriggers = allTriggersObj.size();
	solveOnce = j.value("solve_once", true);
	stayOpen = false;
}

void TCompDoorBabyPuzzle::debugInMenu()
{
	//ImGui::Checkbox("ECreated", &enemiesCreated);
	TCompDoorController::debugInMenu();
}

void TCompDoorBabyPuzzle::renderDebug()
{
	TCompDoorController::renderDebug();
}

void TCompDoorBabyPuzzle::setOpenDoor(bool o)
{
	TCompDoorController::setOpenDoor(o);
	if (solveOnce and o) stayOpen = true;
}
