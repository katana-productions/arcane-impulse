#include "mcv_platform.h"
#include "door_enemies.h"
#include "entity/entity.h"
#include "engine.h"

DECL_OBJ_MANAGER("door_enemies", TCompDoorEnemies);

void TCompDoorEnemies::init(const TMsgEntityCreated &msg)
{
  TCompDoorController::initBase(msg);
}

void TCompDoorEnemies::keyEntered(const TMsgPuzzleTriggerEntered& msg)
{
  TCompDoorController::keyEnteredBase(msg);
}

void TCompDoorEnemies::registerMsgs()
{
  DECL_MSG(TCompDoorEnemies, TMsgEntityCreated, init);
  DECL_MSG(TCompDoorEnemies, TMsgPuzzleTriggerEntered, keyEntered);
}

// TODO: improve this to message and events instead of polling
bool TCompDoorEnemies::allEnemiesOk()
{
  for (int i = 0; i < enemies.size(); ++i)
  {
    CHandle e = getEntityByName(enemies[i]);
    if (!e.isValid()) return false;
  }
  return true;
}

bool TCompDoorEnemies::allEnemiesKilled()
{
  for (int i = 0; i < enemies.size(); ++i)
  {
    CHandle e = getEntityByName(enemies[i]);
    if (e.isValid()) return false;
  }
  return true;
}

void TCompDoorEnemies::load(const json& j, TEntityParseContext& ctx)
{
  TCompDoorController::load(j, ctx);
  //Load enemies
  json e = j["enemies"];
  for (json::iterator it = e.begin(); it != e.end(); ++it) {
    std::string enemy = it.value().get<std::string>();
    enemies.push_back(enemy);
  }
}

void TCompDoorEnemies::update(float dt)
{
  if (enabled)
  {
    if (allEnemiesOk())
      enemiesCreated = true;
    if (enemiesCreated and allEnemiesKilled() and !playerCrossedDoor)
    {
      if (eventOpen != 0)
        EngineLua.launchEvent(eventOpen);
      else
        setOpenDoor(true);
      enabled = false;
    }
    /*else
      setOpenDoor(false);*/
  }
  TCompDoorController::update(dt);
}

void TCompDoorEnemies::debugInMenu()
{
  ImGui::Checkbox("ECreated", &enemiesCreated);
  TCompDoorController::debugInMenu();
}

void TCompDoorEnemies::renderDebug()
{
  TCompDoorController::renderDebug();
}
