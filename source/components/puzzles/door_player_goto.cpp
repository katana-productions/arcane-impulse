#include "mcv_platform.h"
#include "door_player_goto.h"
#include "comp_puzzle_trigger.h"
#include "entity/entity.h"
#include "engine.h"

DECL_OBJ_MANAGER("door_player_goto", TCompDoorPlayerGoto);


void TCompDoorPlayerGoto::init(const TMsgEntityCreated &msg)
{
  TCompDoorController::initBase(msg);
}

void TCompDoorPlayerGoto::keyEntered(const TMsgPuzzleTriggerEntered& msg)
{
  TCompDoorController::keyEnteredBase(msg);
  CHandle trig = msg.h_trigger;
  CHandle key = msg.h_entity;
  if (trig.isValid() and key.isValid())
  {
    CEntity *trigEnt = trig;
    CEntity *keyEnt = key;
    TCompName *trigName = trigEnt->get<TCompName>();
    TCompName *keyName = keyEnt->get<TCompName>();
    std::string tName = trigName->getName();
    std::string kName = keyName->getName();

    if (tName == triggerOpen and kName == "Player")
    {
      if (eventOpen != 0)
        EngineLua.launchEvent(eventOpen);
      else
        setOpenDoor(true);
      if (triggerOpen == "trigger_HugeDoor")
        EngineLua.setRandomCamShake(0.1f, 2.25f, 0.1f);
    }
  }
}

void TCompDoorPlayerGoto::registerMsgs()
{
  DECL_MSG(TCompDoorPlayerGoto, TMsgEntityCreated, init);
  DECL_MSG(TCompDoorPlayerGoto, TMsgPuzzleTriggerEntered, keyEntered);
}

void TCompDoorPlayerGoto::load(const json& j, TEntityParseContext& ctx)
{
  TCompDoorController::load(j, ctx);
  triggerOpen = j.value("trigger_open", "trigger");
  assert(triggerOpen != "");
}

void TCompDoorPlayerGoto::debugInMenu()
{
  TCompDoorController::debugInMenu();
}

void TCompDoorPlayerGoto::renderDebug() 
{
  TCompDoorController::renderDebug();
}
