#pragma once
#include "door_controller.h"
#include "entity/common_msgs.h"

class TCompDoorEnemies : public TCompDoorController
{
private:
  bool enabled = true;
  bool enemiesCreated = false;
  std::vector<std::string> enemies;

  void init(const TMsgEntityCreated &msg);
  void keyEntered(const TMsgPuzzleTriggerEntered& msg);

  bool allEnemiesOk();
  bool allEnemiesKilled();

public:
  DECL_SIBLING_ACCESS();
  static void registerMsgs();
  void load(const json& j, TEntityParseContext& ctx);
  void update(float dt);
  void renderDebug();
  void debugInMenu();
};

