#pragma once

#include "components/common/comp_base.h"
#include "components/common/comp_transform.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

class TCompPuzzleTrigger : public TCompBase
{
private:

  int nObj = 0;
  TCompTransform *myTransform;
  VEC3 snapTarget;
  VEC3 angularVel;
  VEC3 offset = VEC3(0.0f, 2.0f, 2.0f);

  CHandle entityHdl;

  void init(const TMsgEntityCreated& msg);
  void objEntered(const TMsgEntityTriggerEnter& msg);
  void objExited(const TMsgEntityTriggerExit& msg);

public:
  DECL_SIBLING_ACCESS();
  bool snap;
  std::string puzzle_controller;
  static void registerMsgs();
  void update(float dt);
  void load(const json& j, TEntityParseContext& ctx);
  void debugInMenu();
  void renderDebug();
};