#include "mcv_platform.h"
#include "comp_core_trigger.h"
#include "physics/module_physics.h"
#include "components/common/comp_collider.h"
#include "engine.h"

DECL_OBJ_MANAGER("core_trigger", TCompCoreTrigger);

void TCompCoreTrigger::update(float dt)
{
  if (snap)
  {
    myTransform = get<TCompTransform>();
    if (myTransform != nullptr)
    {
      snapTarget = myTransform->getPosition() + VEC3(0.0f, 2.0f, 0.0f);
    }
    if (entityHdl.isValid())
    {
      TCompCollider *objCol = ((CEntity *)entityHdl)->get<TCompCollider>();
      physx::PxRigidDynamic *rb = objCol->actor->is<physx::PxRigidDynamic>();
      if (nObj == 1 and !isFireball(entityHdl) and isObject(entityHdl) and objCol->getState() != ObjState::Pulled and objCol->getState() != ObjState::Pushed)
      {
        CTransform t = toTransform(rb->getGlobalPose());
        float dist = VEC3::Distance(snapTarget, t.getPosition());
        if (dist > 0.01f)
        {
          VEC3 dir = snapTarget - t.getPosition();
          dir.Normalize();
          float v = 50.0f;
          if (v > dist) v = dist;
          VEC3 vel = v * dir;
          rb->setActorFlag(physx::PxActorFlag::eDISABLE_GRAVITY, true);
          rb->setLinearVelocity(VEC3_TO_PXVEC3(vel));
        }
        else
        {
          rb->setActorFlag(physx::PxActorFlag::eDISABLE_GRAVITY, true);
          rb->setLinearVelocity(physx::PxZero);
        }
        rb->setAngularVelocity(VEC3_TO_PXVEC3(angularVel));
      }
      else if (!isFireball(entityHdl) and isObject(entityHdl) and objCol->getState() != ObjState::Pulled and objCol->getState() != ObjState::Pushed)
      {
        rb->setActorFlag(physx::PxActorFlag::eDISABLE_GRAVITY, true);
        rb->setLinearVelocity(physx::PxZero);
      }
    }
  }
}

void TCompCoreTrigger::load(const json& j, TEntityParseContext& ctx)
{
}

void TCompCoreTrigger::debugInMenu()
{
}

void TCompCoreTrigger::renderDebug()
{
  drawWiredSphere(snapTarget, 0.2f, VEC4(0, 0, 1, 1));
}

void TCompCoreTrigger::registerMsgs()
{
  DECL_MSG(TCompCoreTrigger, TMsgEntityTriggerEnter, objEntered);
  DECL_MSG(TCompCoreTrigger, TMsgEntityTriggerExit, objExited);
}

void TCompCoreTrigger::objEntered(const TMsgEntityTriggerEnter& msg)
{
	if (activated) {
		//TMsgPuzzleTriggerEntered message;
		//Which trigger sends the message
		//message.h_trigger = CHandle(this).getOwner();
		//Which object has entered the trigger
		//message.h_entity = msg.h_entity;

		CEntity *ent = msg.h_entity;
		if (!ent) return;
		TCompName *entName = ent->get<TCompName>();
		if (!entName) return;
		std::string s = entName->getName();

		//TODO: filter out particles at a later date
		if ((s == "Player" or isObject(msg.h_entity)) and !isFireball(msg.h_entity))
		{
			/*CEntity *puzzleEntity = (CEntity *)getEntityByName(puzzle_controller);
			puzzleEntity->sendMsg(message);*/

			++nObj;

			if (s != "Player") entityHdl = msg.h_entity;

			angularVel.x = randomRange(-1.0f, 1.0f);
			angularVel.y = randomRange(-1.0f, 1.0f);
			angularVel.z = randomRange(-1.0f, 1.0f);
		}
	}
}

void TCompCoreTrigger::objExited(const TMsgEntityTriggerExit& msg)
{
  //TMsgPuzzleTriggerExited message;
  //Which trigger sends the message
  //message.h_trigger = CHandle(this).getOwner();
  //Which object has left the trigger
  //message.h_entity = msg.h_entity;

  CEntity *ent = msg.h_entity;
  if (!ent) return;
  TCompName *entName = ent->get<TCompName>();
  if (!entName) return;
  std::string s = entName->getName();

  if ((s == "Player" or isObject(msg.h_entity)) and !isFireball(msg.h_entity))
  {
    /*CEntity *puzzleEntity = (CEntity *)getEntityByName(puzzle_controller);
    puzzleEntity->sendMsg(message);*/

    --nObj;
    if (s != "Player")
    {
      if (nObj == 0 && entityHdl.isValid())
      {
        TCompCollider *objCol = ((CEntity *)entityHdl)->get<TCompCollider>();
        physx::PxRigidDynamic *rb = objCol->actor->is<physx::PxRigidDynamic>();
        rb->setActorFlag(physx::PxActorFlag::eDISABLE_GRAVITY, false);
        entityHdl = CHandle();
		if (activated) {
			activated = false;
			EnginePersistence.deactivateCore();
		}
      }
      else
      {
        CEntity *objLeft = msg.h_entity;
        TCompCollider *col = objLeft->get<TCompCollider>();
        physx::PxRigidDynamic *objLeftRb = col->actor->is<physx::PxRigidDynamic>();
        objLeftRb->setActorFlag(physx::PxActorFlag::eDISABLE_GRAVITY, false);
        if (msg.h_entity == entityHdl) entityHdl = CHandle();
      }
    }
  }
}
