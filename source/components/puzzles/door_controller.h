#pragma once
#include "components/common/comp_base.h"
#include "entity/common_msgs.h"
#include "components/common/comp_transform.h"
#include "modules/module_lua.h"
#include "components/common/verticalDoor.h"
#include "components/common/horizontalDoor.h"

class TCompDoorController : public TCompBase
{
protected:
	bool enabled = false;
	bool open;
	bool closeAfterCrossing;
	bool playerCrossedDoor = false;
	float speed = 2.0f;
	int eventOpen = 0;
	std::string triggerClose;
	VEC3 targetUp;
	VEC3 targetLeft;
	VEC3 targetRight;
	VEC3 originUp;
	VEC3 originLeft;
	VEC3 originRight;

	TCompTransform *myTransform;
	TCompName *myName;
	TCompTransform *transUp;
	TCompTransform *transLeft;
	TCompTransform *transRight;
	physx::PxRigidDynamic *rbUp;
	physx::PxRigidDynamic *rbLeft;
	physx::PxRigidDynamic *rbRight;

	TCompVerticalDoor *v;
	TCompHorizontalDoor *h;

	void initBase(const TMsgEntityCreated &msg);
	void keyEnteredBase(const TMsgPuzzleTriggerEntered& msg);

public:
	DECL_SIBLING_ACCESS();
	static void registerMsgs();
	void load(const json& j, TEntityParseContext& ctx);
	void renderDebug();
	void debugInMenu();
	void update(float dt);
	void setOpenDoor(bool o);
};

