#pragma once

#include "components/common/comp_base.h"
#include "components/common/comp_transform.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"

class TCompCoreTrigger : public TCompBase
{
private:

  int nObj = 0;
  TCompTransform *myTransform;
  VEC3 snapTarget;
  VEC3 angularVel;

  CHandle entityHdl;

  void objEntered(const TMsgEntityTriggerEnter& msg);
  void objExited(const TMsgEntityTriggerExit& msg);
  bool activated = true;

public:
  DECL_SIBLING_ACCESS();
  bool snap = true;
  //std::string puzzle_controller;
  static void registerMsgs();
  void update(float dt);
  void load(const json& j, TEntityParseContext& ctx);
  void debugInMenu();
  void renderDebug();
};