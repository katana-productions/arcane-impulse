#include "mcv_platform.h"
#include "comp_puzzle_trigger.h"
#include "engine.h"
#include "physics/module_physics.h"
#include "components/common/comp_collider.h"

DECL_OBJ_MANAGER("puzzle_trigger", TCompPuzzleTrigger);

void TCompPuzzleTrigger::update(float dt)
{
  if (snap)
  {
    myTransform = get<TCompTransform>();
    if (myTransform != nullptr)
    {
      snapTarget = myTransform->getPosition() + myTransform->getLeft() * offset.x + myTransform->getUp() * offset.y + myTransform->getFront() * offset.z;
    }
    if (entityHdl.isValid())
    {
      CEntity *ent = entityHdl;
      if (!ent) return;
      TCompName *entName = ent->get<TCompName>();
      if (!entName) return;
      std::string s = entName->getName();
      TCompCollider *objCol = ((CEntity *)entityHdl)->get<TCompCollider>();
      physx::PxRigidDynamic *rb = objCol->actor->is<physx::PxRigidDynamic>();
      if (nObj == 1 and !isFireball(entityHdl) and isObject(entityHdl) and objCol->getState() != ObjState::Pulled and objCol->getState() != ObjState::Pushed and (s == "BlueOrb" or s == "RedOrbe" or s == "Cst_PuzzleOrb" or s == "Cst_PuzzleOrb001"))
      {
        CTransform t = toTransform(rb->getGlobalPose());
        float dist = VEC3::Distance(snapTarget, t.getPosition());
        if (dist > 0.01f)
        {
          VEC3 dir = snapTarget - t.getPosition();
          dir.Normalize();
          float v = 50.0f;
          if (v > dist) v = dist;
          VEC3 vel = v * dir;
          rb->setActorFlag(physx::PxActorFlag::eDISABLE_GRAVITY, true);
          rb->setLinearVelocity(VEC3_TO_PXVEC3(vel));
        }
        else
        {
          rb->setActorFlag(physx::PxActorFlag::eDISABLE_GRAVITY, true);
          rb->setLinearVelocity(physx::PxZero);
        }
        rb->setAngularVelocity(VEC3_TO_PXVEC3(angularVel));
      }
      else if (!isFireball(entityHdl) and isObject(entityHdl) and objCol->getState() != ObjState::Pulled and objCol->getState() != ObjState::Pushed)
      {
        rb->setActorFlag(physx::PxActorFlag::eDISABLE_GRAVITY, true);
        rb->setLinearVelocity(physx::PxZero);
      }
    }
  }
}

void TCompPuzzleTrigger::load(const json& j, TEntityParseContext& ctx)
{
  puzzle_controller = j.value("puzzle_controller", "");
  snap = j.value("snap", false);
}

void TCompPuzzleTrigger::debugInMenu()
{
  ImGui::Text("Puzzle controller to call: ");
  ImGui::Text(puzzle_controller.c_str());
}

void TCompPuzzleTrigger::renderDebug()
{
  drawWiredSphere(snapTarget, 0.2f, VEC4(0, 0, 1, 1));
}

void TCompPuzzleTrigger::registerMsgs()
{
  DECL_MSG(TCompPuzzleTrigger, TMsgEntityCreated, init);
  DECL_MSG(TCompPuzzleTrigger, TMsgEntityTriggerEnter, objEntered);
  DECL_MSG(TCompPuzzleTrigger, TMsgEntityTriggerExit, objExited);
}

void TCompPuzzleTrigger::init(const TMsgEntityCreated& msg)
{
  TCompRender *tRender = get<TCompRender>();
  if (tRender != nullptr)
    tRender->glowAmount = 0.0f;
}

void TCompPuzzleTrigger::objEntered(const TMsgEntityTriggerEnter& msg)
{
  TMsgPuzzleTriggerEntered message;
  //Which trigger sends the message
  message.h_trigger = CHandle(this).getOwner();
  //Which object has entered the trigger
  message.h_entity = msg.h_entity;

  CEntity *ent = msg.h_entity;
  if (!ent) return;
  TCompName *entName = ent->get<TCompName>();
  if (!entName) return;
  std::string s = entName->getName();

  //TODO: filter out particles at a later date
  if (((s == "BlueOrb" or s == "RedOrbe" or s == "Cst_PuzzleOrb" or s == "Cst_PuzzleOrb001") and isObject(msg.h_entity)) and !isFireball(msg.h_entity) or s == "Player")
  {
    CEntity *puzzleEntity = (CEntity *)getEntityByName(puzzle_controller);
    if (puzzle_controller != "" and puzzleEntity != nullptr) 
      puzzleEntity->sendMsg(message);

    ++nObj;

		if (puzzle_controller == "HugeGate" && s == "Player") 
			EnginePersistence.finalMusic = true;

    if(s != "Player") entityHdl = msg.h_entity;

    angularVel.x = randomRange(-1.0f, 1.0f);
    angularVel.y = randomRange(-1.0f, 1.0f);
    angularVel.z = randomRange(-1.0f, 1.0f);
  }
}

void TCompPuzzleTrigger::objExited(const TMsgEntityTriggerExit& msg)
{
  TMsgPuzzleTriggerExited message;
  //Which trigger sends the message
  message.h_trigger = CHandle(this).getOwner();
  //Which object has left the trigger
  message.h_entity = msg.h_entity;

  CEntity *ent = msg.h_entity;
  if (!ent) return;
  TCompName *entName = ent->get<TCompName>();
  if (!entName) return;
  std::string s = entName->getName();

  if (((s == "BlueOrb" or s == "RedOrbe" or s == "Cst_PuzzleOrb" or s == "Cst_PuzzleOrb001") and isObject(msg.h_entity)) and !isFireball(msg.h_entity) or s == "Player")
  {
    CEntity *puzzleEntity = (CEntity *)getEntityByName(puzzle_controller);
    if (puzzle_controller != "" and puzzleEntity != nullptr)
      puzzleEntity->sendMsg(message);

    --nObj;
    if (s != "Player")
    {
      if (nObj == 0 && entityHdl.isValid())
      {
        TCompCollider *objCol = ((CEntity *)entityHdl)->get<TCompCollider>();
        physx::PxRigidDynamic *rb = objCol->actor->is<physx::PxRigidDynamic>();
        rb->setActorFlag(physx::PxActorFlag::eDISABLE_GRAVITY, false);
        entityHdl = CHandle();
      }
      else
      {
        CEntity *objLeft = msg.h_entity;
        TCompCollider *col = objLeft->get<TCompCollider>();
        physx::PxRigidDynamic *objLeftRb = col->actor->is<physx::PxRigidDynamic>();
        objLeftRb->setActorFlag(physx::PxActorFlag::eDISABLE_GRAVITY, false);
        if (msg.h_entity == entityHdl) entityHdl = CHandle();
      }
    }
  }
}