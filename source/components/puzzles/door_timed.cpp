#include "mcv_platform.h"
#include "door_timed.h"
#include "components/common/comp_render.h"
#include "entity/entity.h"

DECL_OBJ_MANAGER("door_timed", TCompDoorTimed);

void TCompDoorTimed::init(const TMsgEntityCreated &msg)
{
  TCompDoorController::initBase(msg);
}

void TCompDoorTimed::keyEntered(const TMsgPuzzleTriggerEntered& msg)
{
  TCompDoorController::keyEnteredBase(msg);
  CHandle trig = msg.h_trigger;
  CHandle key = msg.h_entity;
  if (trig.isValid() and key.isValid())
  {
    CEntity *trigEnt = trig;
    CEntity *keyEnt = key;
    TCompName *trigName = trigEnt->get<TCompName>();
    TCompName *keyName = keyEnt->get<TCompName>();
    std::string tName = trigName->getName();
    std::string kName = keyName->getName();

    if (tName == triggerOpen and kName == "Player")
      start = true;
  }
}

void TCompDoorTimed::registerMsgs()
{
  DECL_MSG(TCompDoorTimed, TMsgEntityCreated, init);
  DECL_MSG(TCompDoorTimed, TMsgPuzzleTriggerEntered, keyEntered);
}

void TCompDoorTimed::load(const json& j, TEntityParseContext& ctx)
{
  TCompDoorController::load(j, ctx);

  triggerOpen = j.value("trigger_open", "");
  time = j.value("time", 5.0f);
}

void TCompDoorTimed::update(float dt)
{
  if (start)
  {
    counter += dt;
    //open = true;
    setOpenDoor(true);
    if (counter >= time)
    {
      start = false;
      setOpenDoor(false);
      counter = 0;
    }
  }
  TCompDoorController::update(dt);
}

void TCompDoorTimed::debugInMenu()
{
  TCompDoorController::debugInMenu();
}

void TCompDoorTimed::renderDebug()
{
  TCompDoorController::renderDebug();
}
