#pragma once
#include "components/ai/bt.h"
#include "door_controller.h"
#include "entity/common_msgs.h"

class TCompDoorTimed : public TCompDoorController
{
private:
  std::string triggerOpen;
  float time;
  float counter = 0;
  bool start = false;

  void init(const TMsgEntityCreated &msg);
  void keyEntered(const TMsgPuzzleTriggerEntered& msg);

public:
  DECL_SIBLING_ACCESS();
  static void registerMsgs();
  void load(const json& j, TEntityParseContext& ctx);
  void update(float dt);
  void renderDebug();
  void debugInMenu();
};

