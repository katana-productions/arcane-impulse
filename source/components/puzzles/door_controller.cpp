#include "mcv_platform.h"
#include "door_controller.h"
#include "comp_puzzle_trigger.h"
#include "entity/entity.h"
#include "engine.h"

DECL_OBJ_MANAGER("door_controller", TCompDoorController);

void TCompDoorController::initBase(const TMsgEntityCreated &msg)
{
  CEntity *ent = msg.h_entity;
  myTransform = ent->get<TCompTransform>();
  myName = ent->get<TCompName>();

  float y, p, r;
  myTransform->getAngles(&y, &p, &r);

  // Instantiate Trigger Close
  if (closeAfterCrossing)
  {
    VEC3 pos = myTransform->getPosition() - (myTransform->getFront() * 3);
    CEntity *tClose = EngineBasics.Instantiate("data/prefabs/puzzles/triggers/door_trigger_instance.json", pos, y, p, r);
    TCompName *tName = tClose->get<TCompName>();
    triggerClose = (std::string)(myName->getName()) + "_trigger_close";
    tName->setName(triggerClose.c_str());
    TCompPuzzleTrigger *pTrigger = tClose->get<TCompPuzzleTrigger>();
    pTrigger->puzzle_controller = myName->getName();
  }

  // Instantiate the three parts of the door
  /*VEC3 posUp = myTransform->getPosition() - myTransform->getUp() * 0.8f;
  CEntity *doorUp = EngineBasics.Instantiate("data/prefabs/puzzles/doors/door_up_instance.json", posUp, y, p, r);
  TCompName *upName = doorUp->get<TCompName>();
  std::string n = (std::string)(myName->getName()) + "_door_up";
  upName->setName(n.c_str());
  

  VEC3 posLeft = myTransform->getPosition() - myTransform->getLeft() * 0.64f - myTransform->getUp() * 1.9f;
  CEntity *doorLeft = EngineBasics.Instantiate("data/prefabs/puzzles/doors/door_left_instance.json", posLeft, y, p, r);
  TCompName *leftName = doorLeft->get<TCompName>();
  n = (std::string)(myName->getName()) + "_door_left";
  leftName->setName(n.c_str());

  VEC3 posRight = myTransform->getPosition() + myTransform->getLeft() * 0.64f - myTransform->getUp() * 1.9f;
  CEntity *doorRight = EngineBasics.Instantiate("data/prefabs/puzzles/doors/door_right_instance.json", posRight, y, p, r);
  TCompName *rightName = doorRight->get<TCompName>();
  n = (std::string)(myName->getName()) + "_door_right";
  rightName->setName(n.c_str());

  transUp = doorUp->get<TCompTransform>();
  transLeft = doorLeft->get<TCompTransform>();
  transRight = doorRight->get<TCompTransform>();
  TCompCollider *colUp = doorUp->get<TCompCollider>();
  TCompCollider *colLeft = doorLeft->get<TCompCollider>();
  TCompCollider *colRight = doorRight->get<TCompCollider>();
  rbUp = colUp->actor->is<physx::PxRigidDynamic>();
  rbLeft = colLeft->actor->is<physx::PxRigidDynamic>();
  rbRight = colRight->actor->is<physx::PxRigidDynamic>();

  targetUp = transUp->getPosition() + transUp->getUp() * 2;
  targetLeft = transLeft->getPosition() - transLeft->getLeft() * 2;
  targetRight = transRight->getPosition() + transRight->getLeft() * 2;
  originUp = transUp->getPosition();
  originLeft = transLeft->getPosition();
  originRight = transRight->getPosition();*/

  v = ent->get< TCompVerticalDoor >();
  h = ent->get< TCompHorizontalDoor >();

  enabled = true;
}

void TCompDoorController::keyEnteredBase(const TMsgPuzzleTriggerEntered& msg)
{
  CHandle trig = msg.h_trigger;
  CHandle key = msg.h_entity;
  if (trig.isValid() and key.isValid() and closeAfterCrossing)
  {
    CEntity *trigEnt = trig;
    CEntity *keyEnt = key;
    TCompName *trigName = trigEnt->get<TCompName>();
    TCompName *keyName = keyEnt->get<TCompName>();
    std::string tName = trigName->getName();
    std::string kName = keyName->getName();

    if (tName == triggerClose and kName == "Player")
    {
      playerCrossedDoor = true;
      open = false;
    }
  }
}

void TCompDoorController::registerMsgs()
{
  DECL_MSG(TCompDoorController, TMsgEntityCreated, initBase);
  DECL_MSG(TCompDoorController, TMsgPuzzleTriggerEntered, keyEnteredBase);
}

void TCompDoorController::load(const json& j, TEntityParseContext& ctx)
{
  open = j.value("init_state", false);
  closeAfterCrossing = j.value("close_after_crossing", false);
  eventOpen = j.value("eventOpen", 0);
}

void TCompDoorController::debugInMenu()
{
  ImGui::Checkbox("Open", &this->open);
  setOpenDoor(this->open);
}

void TCompDoorController::renderDebug() {}

void TCompDoorController::update(float dt)
{
  /*if (!enabled) return;
  if (dt >= 0.1f) dt = 0.1f;
  // Define here how to open the door, for all types of doors
  if (open)
  {  
    float d1 = VEC3::Distance(targetUp, transUp->getPosition());
    VEC3 pUp = transUp->getPosition() + (transUp->getUp() * d1 * speed* dt);
    if (d1 > 0.01f)
      rbUp->setKinematicTarget(physx::PxTransform(pUp.x, pUp.y, pUp.z, QUAT_TO_PXQUAT(transUp->getRotation())));

    float d2 = VEC3::Distance(targetLeft, transLeft->getPosition());
    VEC3 pLeft = transLeft->getPosition() - (transLeft->getLeft() * d2 * speed * dt);
    if (d2 > 0.01f)
      rbLeft->setKinematicTarget(physx::PxTransform(pLeft.x, pLeft.y, pLeft.z, QUAT_TO_PXQUAT(transLeft->getRotation())));

    float d3 = VEC3::Distance(targetRight, transRight->getPosition());
    VEC3 pRight = transRight->getPosition() + (transRight->getLeft() * d3 * speed * dt);
    if (d3 > 0.01f)
      rbRight->setKinematicTarget(physx::PxTransform(pRight.x, pRight.y, pRight.z, QUAT_TO_PXQUAT(transRight->getRotation())));



  }
  else
  {  
    float d1 = VEC3::Distance(originUp, transUp->getPosition());
    VEC3 pUp = transUp->getPosition() - (transUp->getUp() * d1 * speed* dt);
    if (d1 > 0.01f)
      rbUp->setKinematicTarget(physx::PxTransform(pUp.x, pUp.y, pUp.z, QUAT_TO_PXQUAT(transUp->getRotation())));

    float d2 = VEC3::Distance(originLeft, transLeft->getPosition());
    VEC3 pLeft = transLeft->getPosition() + (transLeft->getLeft() * d2 * speed * dt);
    if (d2 > 0.01f)
      rbLeft->setKinematicTarget(physx::PxTransform(pLeft.x, pLeft.y, pLeft.z, QUAT_TO_PXQUAT(transLeft->getRotation())));

    float d3 = VEC3::Distance(originRight, transRight->getPosition());
    VEC3 pRight = transRight->getPosition() - (transRight->getLeft() * d3 * speed * dt);
    if (d3 > 0.01f)
      rbRight->setKinematicTarget(physx::PxTransform(pRight.x, pRight.y, pRight.z, QUAT_TO_PXQUAT(transRight->getRotation())));
  }*/
}

void TCompDoorController::setOpenDoor(bool o)
{
  this->open = o;
	if (v != nullptr) {
		v->SetDoorState(this->open);
	}
	else {
		h->SetDoorState(this->open);
	}
}
