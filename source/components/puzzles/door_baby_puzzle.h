#pragma once
#include "components/ai/bt.h"
#include "door_controller.h"
#include "entity/common_msgs.h"

class TCompDoorBabyPuzzle : public TCompDoorController
{
private:
  std::string triggerOpen;
  int nTriggers, nCallEntities, nSolved;
  std::map<std::string, std::string> allTriggersObj;
  std::map<std::string, int> numObjInTriggers;
  std::map<std::string, bool> okObjExists;
  std::map<std::string, bool> solvedTriggers;

  //Variables to keep state
  bool oneShot, solveOnce, stayOpen;

  void init(const TMsgEntityCreated &msg);
  void keyEntered(const TMsgPuzzleTriggerEntered& msg);
  void keyExited(const TMsgPuzzleTriggerExited& msg);

  void checkSolved();
	bool firstTime = true;
	bool firstTime2 = true;
	bool firstTime3 = true;

public:
  DECL_SIBLING_ACCESS();
  static void registerMsgs();
  void load(const json& j, TEntityParseContext& ctx);
  void renderDebug();
  void debugInMenu();
  void setOpenDoor(bool o);
};

