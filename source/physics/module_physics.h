#pragma once

#include "modules/module.h"
#include "entity/entity.h"
#include "components/common/comp_collider.h"
#include "physics/comp_ragdoll.h"

#define VEC3_TO_PXVEC3(vec3) physx::PxVec3(vec3.x, vec3.y, vec3.z)
#define VEC3_TO_PXEXTENDEDVEC3(vec3) physx::PxExtendedVec3(vec3.x, vec3.y, vec3.z)
#define PXVEC3_TO_VEC3(pxvec3) VEC3(pxvec3.x, pxvec3.y, pxvec3.z)

#define QUAT_TO_PXQUAT(quat) physx::PxQuat(quat.x, quat.y, quat.z, quat.w)
#define PXQUAT_TO_QUAT(pxquat) QUAT(pxquat.x, pxquat.y, pxquat.z,pxquat.w)

extern CTransform toTransform(physx::PxTransform pxtrans);
extern physx::PxTransform toPxTransform(CTransform mcvtrans);

class CModulePhysics : public IModule
{
	physx::PxRigidActor* createController(TCompCollider& comp_collider);
	physx::PxMaterial* readMaterial(const json& jcfg);
	bool readShape(physx::PxRigidActor* actor, const json& jcfg);

public:
	physx::PxScene*				gScene = NULL;
	static physx::PxQueryFilterData defaultFilter;

	enum FilterGroup {
		Wall = 1 << 0,
		Floor = 1 << 1,
		Player = 1 << 2,
		Enemy = 1 << 3,
		Triggers = 1 << 4,
		Objects = 1 << 5,
		Bones = 1 << 6,
		MagicOnly = 1 << 7,
		Scenario = Wall | Floor,
		Characters = Player | Enemy,
		All = -1
	};
	float totalTime = 0.0f;
	bool isPaused = false;
	float gravity = -15.0f;

	CModulePhysics(const std::string& aname) : IModule(aname) { }
	bool start() override;
	void stop() override;
	void update(float delta) override;
	void renderDebug() override;
	void renderInMenu() override;

	FilterGroup getFilterByName(const std::string& name);
	void setupFiltering(physx::PxShape* shape, physx::PxU32 filterGroup, physx::PxU32 filterMask);
	void setupFilteringOnAllShapesOfActor(physx::PxRigidActor* actor, physx::PxU32 filterGroup, physx::PxU32 filterMask);
	void createActor(TCompCollider& compCollider);
	void createRagdoll(TCompRagdoll& compRagdoll);
	void createRagdollJoints(TCompRagdoll& comp_ragdoll, int bone_id);

	bool isActive() const;

	physx::PxScene* getPhysxScene() { return gScene; }
	physx::PxMaterial* CreateMaterial(const VEC3 & settings);

	bool Raycast(const VEC3 & origin, const VEC3 & dir, float distance, physx::PxRaycastHit & hit,
		physx::PxQueryFlags queryFlags = physx::PxQueryFlag::eSTATIC, physx::PxHitFlags hitFlags = physx::PxHitFlag::eDEFAULT, physx::PxQueryFilterData filterdata = defaultFilter);
	bool Overlap(physx::PxGeometry& geometry, VEC3 pos, std::vector<physx::PxOverlapHit> & hits, physx::PxQueryFilterData filterdata);
	void AddExplosion(const VEC3& pos, const float& radius, const float& knockback, const float& damage, const float& minKnockBack);
	void DamageMessage(CEntity* enemyEntity, float damage, VEC3 force);

	physx::PxScene* getScene() const;
	physx::PxPhysics* getPhysics() const;

	class CustomBehaviourCallback : public physx::PxControllerBehaviorCallback
	{
	public:

		physx::PxControllerBehaviorFlags getBehaviorFlags(const physx::PxShape &shape, const physx::PxActor &actor)
		{
			return physx::PxControllerBehaviorFlag::eCCT_CAN_RIDE_ON_OBJECT;
		}
		physx::PxControllerBehaviorFlags getBehaviorFlags(const physx::PxController &controller)
		{
			return physx::PxControllerBehaviorFlags(0);
		}
		physx::PxControllerBehaviorFlags getBehaviorFlags(const physx::PxObstacle &obstacle)
		{
			return physx::PxControllerBehaviorFlag::eCCT_CAN_RIDE_ON_OBJECT;
		}
	};
	CustomBehaviourCallback customBehaviourCallback;


	class CustomSimulationEventCallback : public physx::PxSimulationEventCallback
	{
		// Implements PxSimulationEventCallback
		virtual void							onContact(const physx::PxContactPairHeader& pairHeader, const physx::PxContactPair* pairs, physx::PxU32 nbPairs);
		virtual void							onTrigger(physx::PxTriggerPair* pairs, physx::PxU32 count);
		virtual void							onConstraintBreak(physx::PxConstraintInfo*, physx::PxU32) {}
		virtual void							onWake(physx::PxActor**, physx::PxU32) {}
		virtual void							onSleep(physx::PxActor**, physx::PxU32) {}
		virtual void							onAdvance(const physx::PxRigidBody*const*, const physx::PxTransform*, const physx::PxU32) {}

	};
	CustomSimulationEventCallback customSimulationEventCallback;

	class CustomUserControllerHitReport : public physx::PxUserControllerHitReport
	{
	public:

		virtual void onShapeHit(const physx::PxControllerShapeHit& hit);
		virtual void onControllerHit(const physx::PxControllersHit& hit);
		virtual void onObstacleHit(const physx::PxControllerObstacleHit& hit);
	};
	CustomUserControllerHitReport customUserControllerHitReport;

	struct TJoint {
		struct TObj {
			std::string          name;
			CHandle              h_collider;
			CTransform           transform;
			physx::PxRigidActor* actor = nullptr;
		};
		std::string joint_type;
		TObj obj0;
		TObj obj1;
		void create();
		physx::PxJoint*         px_joint = nullptr;
	};

	typedef std::vector< TJoint > VJoints;
	VJoints joints;
	void debugActor(const char* title, physx::PxRigidActor* actor);
	void debugInMenuJoints();
	void debugInMenuJoint(TJoint& c);
	void debugInMenuJointObj(TJoint& j, physx::PxJointActorIndex::Enum idx);

private:
	CEntity* player;
};
