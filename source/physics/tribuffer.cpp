#include "mcv_platform.h"
#include "tribuffer.h"

class CTriBufferResourceType : public CResourceType {
public:
  const char* getExtension(int idx) const override { return "tribuffer"; }
  const char* getName() const override {
    return "Tribuffer";
  }
  IResource* create(const std::string& filename) const override {
    CTriBuffer* new_res = new CTriBuffer();
    bool is_ok = new_res->create(filename);
    if (!is_ok)
      return nullptr;
    new_res->setNameAndType(filename, this);
    return new_res;
  }
};

template<>
const CResourceType* getResourceTypeFor<CTriBuffer>() {
  static CTriBufferResourceType resource_type;
  return &resource_type;
}

bool CTriBuffer::create(const std::string& filename)
{
  CMemoryDataProvider mdp(filename.c_str());
  buf.resize(mdp.size());
  memcpy(buf.data(), mdp.data(), mdp.size());
  return true;
}

void CTriBuffer::write(const void* addr, uint32_t num_bytes)
{
  CFileDataSaver fds(getName().c_str());
  fds.writeBytes( addr, num_bytes );
  buf.resize(num_bytes);
  memcpy(buf.data(), addr, num_bytes);
}


