#pragma once

#include "resources/resource.h"
#include "utils/data_saver.h"
#include "utils/data_provider.h"

class CTriBuffer : public IResource {

private:
  std::vector< uint8_t > buf;

public:
  bool isValid() const { return !buf.empty(); }

  uint32_t getSize() const { return buf.size(); }
  const void* getData() const { return buf.data(); }

  bool create(const std::string& filename);
  void write(const void* addr, uint32_t size);
};
