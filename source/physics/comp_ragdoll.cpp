#include "mcv_platform.h"
#include "comp_ragdoll.h"
#include "components/common/comp_transform.h"
#include "skeleton/comp_skeleton.h"
#include "modules/module_manager.h"
#include "entity/entity.h"
#include "physics/module_physics.h"
#include "render/meshes/mesh.h"
#include "physics/ragdoll.h"
#include "physics/ragdoll_core.h"
#include "cal3d/cal3d.h"
#include "skeleton/cal3d2engine.h"
#include "engine.h"

#include "components/common/comp_collider.h"
#include "components/controllers/comp_char_controller.h"
#include "render/module_render.h"

DECL_OBJ_MANAGER("ragdoll", TCompRagdoll);

#include "resources/resource.h"
#include "render/render.h"

using namespace physx;

TCompRagdoll::~TCompRagdoll() {
	if (EnginePhysics.isActive()) {
		for (size_t i = 0; i < ragdoll.num_bones; i++)
		{
			ragdoll.bones[i].actor->release();
			ragdoll.bones[i].actor = nullptr;
		}
	}
}

void TCompRagdoll::renderInMenu() {
}

void TCompRagdoll::registerMsgs() {
	DECL_MSG(TCompRagdoll, TMsgEntityCreated, onCreated);
	DECL_MSG(TCompRagdoll, TMsgAlarmClock, onAlarmRecieve);
}


bool isActive = false;
void TCompRagdoll::load(const json& j, TEntityParseContext& ctx) {
	isActive = false;
}

void TCompRagdoll::onCreated(const TMsgEntityCreated&) {
	EnginePhysics.createRagdoll(*this);

	CHandle debugCamH = getEntityByName("Camera Debug");
	CHandle playerH = getEntityByName("Player");
	if (debugCamH.isValid() && playerH.isValid())
	{
		CEntity* debugCamE = (CEntity*)debugCamH;
		CEntity* playerE = (CEntity*)playerH;
		TCompTransform* debugCamTrans = debugCamE->get<TCompTransform>();
		TCompTransform* playerTrans = playerE->get<TCompTransform>();
		debugCamTrans->setPosition(playerTrans->getPosition() + VEC3(0, 5, 0));
	}
}

void TCompRagdoll::onAlarmRecieve(const TMsgAlarmClock& msg) {
	if (msg.id == 100 && !isActive) { //DEBUG:: Alarm that activates ragdoll
		//TODO: Deactivate logic too 
		CEntity* e = CHandle(this).getOwner();
		TMsgDamage damage_msg;
		damage_msg.h_sender = CHandle(this).getOwner();
		damage_msg.h_bullet = CHandle(this).getOwner();
		damage_msg.damage = 100;
		e->sendMsg(damage_msg);
	}
}

//Update when inactive: It sets the position of all our colliders to its corresponding Cal3D bone
void TCompRagdoll::updateRagdollFromSkeleton() {
	TCompTransform* comp_transform = get<TCompTransform>();
	if (!comp_transform) return;
	TCompSkeleton* comp_skel = get<TCompSkeleton>();
	if (!comp_skel) return;

	if (comp_skel) {
		auto model = comp_skel->model;
		if (model) {
			auto core_model = model->getCoreModel();
			if (core_model) {
				auto skel = model->getSkeleton();
				if (skel) {
					auto core_skel = skel->getCoreSkeleton();
					if (core_skel) {

						int root_core_bone_id = core_skel->getVectorRootCoreBoneId()[0];

						for (int i = 0; i < ragdoll.num_bones; ++i) {
							auto& ragdoll_bone = ragdoll.bones[i];

							CalBone *cal_bone = skel->getBone(ragdoll_bone.idx);

							CTransform transform;
							transform.setPosition(Cal2DX(cal_bone->getTranslationAbsolute()) + loadVEC3(ragdoll_bone.core->jconfig, "offsetPos"));
							//transform.setPosition(Cal2DX(cal_bone->getTranslationAbsolute()));
							/*float yaw, pitch, roll;
							toEulerAngle(Cal2DX(cal_bone->getRotationAbsolute()), roll, pitch, yaw);*/
							//transform.setRotation(QUAT::CreateFromYawPitchRoll(rotationRPY.z + yaw, rotationRPY.y + pitch, rotationRPY.x + roll));
							transform.setRotation(Cal2DX(cal_bone->getRotationAbsolute()));
							//transform.setPosition(transform.getPosition() + (transform.getLeft()*ragdoll_bone.core->height*0.5f));

							physx::PxTransform px_transform;
							px_transform.p = VEC3_TO_PXVEC3(transform.getPosition());
							px_transform.q = QUAT_TO_PXQUAT(transform.getRotation());

							ragdoll_bone.actor->setGlobalPose(px_transform);
						}
					}
				}
			}
		}
	}
}

void TCompRagdoll::activateRagdoll() {
	if (active) return;
	active = true;

	updateRagdollFromSkeleton();

	CHandle h_comp_ragdoll(this);
	TCompCollider* col = get<TCompCollider>();
	PxRigidActor* actor = col->actor;

	TCompCharController* c_controller = get<TCompCharController>();
	VEC3 externForce = c_controller->GetExternForce();
	externForce *= 0.5;

	DetachControllerShape();

	for (int i = 0; i < ragdoll.num_bones; ++i) {
		auto& ragdoll_bone = ragdoll.bones[i];
		EnginePhysics.getScene()->addActor(*ragdoll_bone.actor);
		PxRigidDynamic* rb_new = ragdoll_bone.actor->is<PxRigidDynamic>();
		rb_new->addForce(VEC3_TO_PXVEC3(externForce) * 1.5, PxForceMode::eIMPULSE);
	}
}

void TCompRagdoll::deactivateRagdoll() {
	if (!active)
		return;
	active = false;
	CHandle h_comp_ragdoll(this);
	for (int i = 0; i < ragdoll.num_bones; ++i) {
		auto& ragdoll_bone = ragdoll.bones[i];
		EnginePhysics.getScene()->removeActor(*ragdoll_bone.actor);
	}
	updateRagdollFromSkeleton();

	TCompCollider* col = get<TCompCollider>();
	if (col)
	{ 
		col->setActive(true);
	}
}

void TCompRagdoll::update(float elapsed) {
	PROFILE_FUNCTION("Ragdoll");
	deltaT = elapsed;
	//------------------------------
	//R AND Q FOR DEBUG PURPOSE
	//------------------------------
	//if (isPressed('R')) //TODO: Remove this, activates ragdoll for debug purposes
	//{
	//	CHandle myHandle = CHandle(this);
	//	EngineAlarms.AddAlarm(0.1f, 100, myHandle);
	//}
	//if (isPressed('Q')) //TODO: Remove this, deactivates ragdoll for debug purposes
	//{
	//	//TODO: After activating the actor we should place the player/enemy at the ragdoll position
	//	deactivateRagdoll();
	//}
	//------------------------------

	/* What is all this crap Roger?
	//TODO: DEACTIVATE SKEL UPDATE  ????????????????????
	//TODO: initially not simualted ??????????????????
	static float accum = 0.f;
	accum += elapsed;
	if (accum > 10.f) {
		static bool first_time = true;
		if (first_time) {
		first_time = false;
		activateRagdoll();

		for (int i = 0; i < ragdoll.num_bones; ++i) {
			auto& ragdoll_bone = ragdoll.bones[i];

			if (ragdoll_bone.core->bone== "Bip001 Head") {
			ragdoll_bone.actor->addForce(VEC3_TO_PXVEC3(VEC3(0.f, 0.016f, 0.f)), physx::PxForceMode::eIMPULSE);
			}
		}
		}
	}
	else {
		return;
	}
	*/

	//If not active we set the position of all our colliders to its corresponding Cal3D bone
	if (!active)
	{
		updateRagdollFromSkeleton();
		return;
	}


	//If active we set the position of the corresponding Cal3D bones of each ragdoll collider
	TCompSkeleton* comp_skel = get<TCompSkeleton>();
	VEC3 new_pos;
	if (comp_skel) {
		auto model = comp_skel->model;
		if (model) {
			auto core_model = model->getCoreModel();
			if (core_model) {
				auto skel = model->getSkeleton();
				if (skel) {
					auto& cal_bones = skel->getVectorBone();
					assert(cal_bones.size() < MAX_SUPPORTED_BONES);

					for (int i = 0; i < cal_bones.size(); ++i) {
						bool found = false;
						for (int j = 0; j < ragdoll.num_bones; ++j) {
							const auto& ragdoll_bone = ragdoll.bones[j];
							if (ragdoll_bone.idx == i) {
								CalBone* bone = cal_bones[ragdoll_bone.idx];

								const json& jcfg = ragdoll_bone.core->jconfig;
								VEC3 offsetPos = loadVEC3(jcfg, "offsetPos");
								VEC3 rotationRPY = loadVEC3(jcfg, "rotationRPY");

								CTransform trans;
								auto t = ragdoll_bone.actor->getGlobalPose();

								//TODO: Offset or remove
								trans.setPosition(PXVEC3_TO_VEC3(t.p) - offsetPos);

								if (j == 0) {
									PxExtendedVec3 pos = PxExtendedVec3(t.p.x, t.p.y, t.p.z);
									TCompCollider* c_collider = get< TCompCollider>();
									c_collider->controller->setPosition(pos);
									//TCompTransform* c_trans = get< TCompTransform>();
									//c_trans->setPosition(PXVEC3_TO_VEC3(t.p));
								}

								//trans.setPosition(PXVEC3_TO_VEC3(t.p));

								//float yaw, pitch, roll;
								//toEulerAngle(PXQUAT_TO_QUAT(t.q), roll, pitch, yaw);
								//trans.setRotation(QUAT::CreateFromYawPitchRoll(rotationRPY.z + yaw, rotationRPY.y + pitch,
								//	rotationRPY.x + roll));
								trans.setRotation(PXQUAT_TO_QUAT(t.q));

								bone->clearState();
								bone->blendState(1.f, DX2Cal(trans.getPosition()), DX2Cal(trans.getRotation()));
								bone->calculateBoneSpace();
								found = true;
								break;
							}
						}
						if (!found) {
							CalBone* bone = cal_bones[i];
							bone->setCoreState();
							bone->calculateBoneSpace();
						}
					}
				}
			}
		}
	}
}

void TCompRagdoll::DetachControllerShape() {
	TCompCollider* col = get<TCompCollider>();
	PxRigidActor* actor = col->actor;

	static const PxU32 max_shapes = 8;
	PxShape* shapes[max_shapes];
	PxU32 nshapes = actor->getNbShapes();
	PxU32 shapes_read = actor->getShapes(shapes, sizeof(shapes), 0);
	PxShape* shape = shapes[0];
	shape->setFlag(PxShapeFlag::eSIMULATION_SHAPE, false);
	
	EnginePhysics.setupFiltering(shape, CModulePhysics::FilterGroup::Enemy, CModulePhysics::FilterGroup::Scenario);

	//actor->release();
}

void TCompRagdoll::CreateTriggerShape() {
	TCompCollider* col = get<TCompCollider>();
	PxRigidActor* actor = col->actor;

	VEC3 jhalfExtent = VEC3(0.5, 0.5, 0.5);
	PxVec3 halfExtent = VEC3_TO_PXVEC3(jhalfExtent);
	PxMaterial *pxMaterial;
	pxMaterial = EnginePhysics.getPhysics()->createMaterial(0, 0, 0);
	shapeTrigger = actor->createShape(PxBoxGeometry(halfExtent), *pxMaterial);

	shapeTrigger->setFlag(PxShapeFlag::eSIMULATION_SHAPE, false);
	shapeTrigger->setFlag(PxShapeFlag::eTRIGGER_SHAPE, true);

	PxFilterData filterData;

	filterData.word0 = CModulePhysics::FilterGroup::Triggers;	// word0 = own ID
	filterData.word1 = CModulePhysics::FilterGroup::All;		// word1 = ID mask to filter pairs that trigger a contact callback;

	shapeTrigger->setSimulationFilterData(filterData);
	shapeTrigger->setQueryFilterData(filterData);
}

void TCompRagdoll::debugInMenu() {
	static bool activated = false;
	ImGui::Checkbox("activated", &activated);
	if (activated) activateRagdoll();
	else deactivateRagdoll();
}

void TCompRagdoll::renderDebug()
{
	for (int i = 0; i < ragdoll.num_bones; ++i) {
		VEC4 debugColor = VEC4(0, 1, 1, 1);

		TRagdoll::TRagdollBone& ragdoll_bone = ragdoll.bones[i];
		physx::PxTransform px_transform = ragdoll_bone.actor->getGlobalPose();
		CTransform transform = toTransform(px_transform);

		using namespace physx;

		PxShape* shape = ragdoll_bone.core->shape;
		MAT44 world = toTransform(shape->getLocalPose()).asMatrix() * transform.asMatrix();
		PxShapeFlags flags = shape->getFlags();

		switch (shape->getGeometryType()) {
		case PxGeometryType::eSPHERE: {
			PxSphereGeometry geom;
			shape->getSphereGeometry(geom);
			PxSphereGeometry* sphere = (PxSphereGeometry*)&geom;
			drawWiredSphere(world, sphere->radius, debugColor);
			break;
		}
		case PxGeometryType::eBOX: {
			PxBoxGeometry geom;
			shape->getBoxGeometry(geom);
			PxBoxGeometry* box = (PxBoxGeometry*)&geom;
			drawWiredAABB(VEC3(0, 0, 0), PXVEC3_TO_VEC3(box->halfExtents), world, debugColor);
			break;
		}
		case PxGeometryType::eCAPSULE: {
			PxCapsuleGeometry geom;
			shape->getCapsuleGeometry(geom);
			PxCapsuleGeometry* capsule = (PxCapsuleGeometry*)&geom;
			drawWiredCapsule(world, capsule->halfHeight*2.f, capsule->radius, debugColor);
			break;
		}
		}
	}
}