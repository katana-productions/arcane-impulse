#include "mcv_platform.h"
#include "module_physics.h"
#include "entity/entity.h"
#include "handle/handle.h"
#include "components/common/comp_transform.h"
#include "components/common/comp_name.h"
#include "components/ai/bt_gnome.h"
#include "components/controllers/comp_char_controller.h"
#include "components/common/comp_fireball.h"
#include "render/meshes/collision_mesh.h"
#include "skeleton/comp_skeleton.h"
#include "skeleton/game_core_skeleton.h"
#include "cal3d/cal3d.h"
#include "skeleton/cal3d2engine.h"
#include "geometry/geometry.h"
#include "utils/data_saver.h"
#include "utils/data_provider.h"
#include "tribuffer.h"

#include "PxPhysicsAPI.h"

#pragma comment(lib, "PhysX3_x64.lib")
#pragma comment(lib, "PhysX3Common_x64.lib")
#pragma comment(lib, "PhysX3Extensions.lib")
#pragma comment(lib, "PxFoundation_x64.lib")
#pragma comment(lib, "PxPvdSDK_x64.lib")
#pragma comment(lib, "PhysX3CharacterKinematic_x64.lib")
#pragma comment(lib, "PhysX3Cooking_x64.lib")

using namespace physx;

PxQueryFilterData CModulePhysics::defaultFilter;

// -----------------------------------------------------
CTransform toTransform(PxTransform pxtrans) {
	CTransform trans;
	trans.setPosition(PXVEC3_TO_VEC3(pxtrans.p));
	trans.setRotation(PXQUAT_TO_QUAT(pxtrans.q));
	return trans;
}

// -----------------------------------------------------
PxTransform toPxTransform(CTransform mcvtrans) {
	PxTransform trans;
	trans.p = VEC3_TO_PXVEC3(mcvtrans.getPosition());
	trans.q = QUAT_TO_PXQUAT(mcvtrans.getRotation());
	return trans;
}

// -----------------------------------------------------
PxDefaultAllocator      gAllocator;
PxDefaultErrorCallback  gErrorCallback;
PxFoundation*           gFoundation = nullptr;
PxPhysics*              gPhysics = nullptr;
PxControllerManager*    gControllerManager = nullptr;
PxDefaultCpuDispatcher* gDispatcher = nullptr;
PxScene*                gScene = nullptr;
PxMaterial*             gMaterial = nullptr;
PxPvd*                  gPvd = nullptr;

// -----------------------------------------------------
static PxGeometryType::Enum readGeometryType(const json& j) {
	PxGeometryType::Enum geometryType = PxGeometryType::eINVALID;
	if (!j.count("shape"))
		return geometryType;

	std::string jgeometryType = j["shape"];
	if (jgeometryType == "sphere") {
		geometryType = PxGeometryType::eSPHERE;
	}
	else if (jgeometryType == "box") {
		geometryType = PxGeometryType::eBOX;
	}
	else if (jgeometryType == "plane") {
		geometryType = PxGeometryType::ePLANE;
	}
	else if (jgeometryType == "convex") {
		geometryType = PxGeometryType::eCONVEXMESH;
	}
	else if (jgeometryType == "capsule") {
		geometryType = PxGeometryType::eCAPSULE;
	}
	else if (jgeometryType == "trimesh") {
		geometryType = PxGeometryType::eTRIANGLEMESH;
	}
	else {
		dbg("Unsupported shape type in collider component %s", jgeometryType.c_str());
	}
	return geometryType;
}

bool CModulePhysics::isActive() const {
	return gScene != nullptr;
}

PxScene* CModulePhysics::getScene() const {
	return gScene;
}

PxPhysics* CModulePhysics::getPhysics() const {
	return gPhysics;
}

// -----------------------------------------------------------------------------
PxRigidActor* CModulePhysics::createController(TCompCollider& comp_collider) {
	const json& jconfig = comp_collider.jconfig;

	PX_ASSERT(desc.mType == PxControllerShapeType::eCAPSULE);

	PxCapsuleControllerDesc capsuleDesc;
	capsuleDesc.height = jconfig.value("height", 1.f);
	capsuleDesc.radius = jconfig.value("radius", 1.f);
	capsuleDesc.climbingMode = PxCapsuleClimbingMode::eCONSTRAINED;
	capsuleDesc.material = readMaterial(jconfig);
	capsuleDesc.reportCallback = &customUserControllerHitReport;
	capsuleDesc.behaviorCallback = &customBehaviourCallback;

	PxCapsuleController* ctrl = static_cast<PxCapsuleController*>(gControllerManager->createController(capsuleDesc));
	PX_ASSERT(ctrl);
	TCompTransform* c = comp_collider.get<TCompTransform>();
	VEC3 pos = c->getPosition();
	ctrl->setFootPosition(PxExtendedVec3(pos.x, pos.y, pos.z));
	PxRigidActor* actor = ctrl->getActor();
	comp_collider.controller = ctrl;

	//CCD ENABLE
	PxRigidDynamic* rb = actor->is<PxRigidDynamic>();
	rb->setRigidBodyFlag(PxRigidBodyFlag::eENABLE_CCD, jconfig.value("continuous_collision", false));

	setupFilteringOnAllShapesOfActor(actor,
		getFilterByName(jconfig.value("group", "all")),
		getFilterByName(jconfig.value("mask", "all"))
	);

	return actor;
}

PxMaterial* CModulePhysics::readMaterial(const json& jcfg)
{
	/************************* MATS ***************************/
	//Default material
	PxMaterial *pxMaterial = CreateMaterial(VEC3(0.3f, 0.3f, 0.0f));  //Static friction, Dynamic friction, Bounciness (Restitution)
	std::string matName = jcfg.value("material", "default");

	if (matName == "0all")
		pxMaterial = CreateMaterial(VEC3(0.0f, 0.0f, 0.0f));
	else if (matName == "sticky")
		pxMaterial = CreateMaterial(VEC3(5.0f, 5.0f, 0.f));
	else if (matName == "superSticky")
		pxMaterial = CreateMaterial(VEC3(10.0f, 10.0f, 0.1f));
	else if (matName == "normal")
		pxMaterial = CreateMaterial(VEC3(0.6f, 0.6f, 0.0f));
	else if (matName == "object")
		pxMaterial = CreateMaterial(VEC3(0.6f, 0.6f, 0.2f));
	else if (matName == "ice")
		pxMaterial = CreateMaterial(VEC3(0.01f, 0.01f, 0.1f));
	else if (matName == "rubber")
		pxMaterial = CreateMaterial(VEC3(0.1f, 0.1f, 1.0f));
	else if (matName == "wood")
		pxMaterial = CreateMaterial(VEC3(0.5f, 0.3f, 0.2f));
	else if (matName == "rock")
		pxMaterial = CreateMaterial(VEC3(0.7f, 0.6f, 0.1f));
	else if (matName == "pelotino")
		pxMaterial = CreateMaterial(VEC3(0.3f, 0.3f, 0.4f));

	return pxMaterial;
}

// ---------------------------------------------------------------------
// Reads a single shape for jcfg and adds it to actor
bool CModulePhysics::readShape(PxRigidActor* actor, const json& jcfg) {
	PROFILE_FUNCTION("CModulePhysics::readShape");

	// Shapes....
	PxGeometryType::Enum geometryType = readGeometryType(jcfg);
	if (geometryType == PxGeometryType::eINVALID)
		return false;

	PxMaterial *pxMaterial = readMaterial(jcfg);

	PxShape* shape = nullptr;
	if (geometryType == PxGeometryType::eBOX)
	{
		VEC3 jhalfExtent = loadVEC3(jcfg, "half_size");
		PxVec3 halfExtent = VEC3_TO_PXVEC3(jhalfExtent);
		shape = gPhysics->createShape(PxBoxGeometry(halfExtent), *pxMaterial);
	}
	else if (geometryType == PxGeometryType::ePLANE)
	{
		VEC3 jplaneNormal = loadVEC3(jcfg, "normal");
		float jplaneDistance = jcfg.value("distance", 0.f);
		PxVec3 planeNormal = VEC3_TO_PXVEC3(jplaneNormal);
		PxReal planeDistance = jplaneDistance;
		PxPlane plane(planeNormal, planeDistance);
		PxTransform offset = PxTransformFromPlaneEquation(plane);
		shape = gPhysics->createShape(PxPlaneGeometry(), *pxMaterial);
		shape->setLocalPose(offset);
	}
	else if (geometryType == PxGeometryType::eSPHERE)
	{
		PxReal radius = jcfg.value("radius", 1.f);
		shape = gPhysics->createShape(PxSphereGeometry(radius), *pxMaterial);
	}
	else if (geometryType == PxGeometryType::eCAPSULE)
	{
		if (jcfg.count("controller") == 0) {
			PxReal radius = jcfg.value("radius", 1.f);
			PxReal height = jcfg.value("height", 1.f);
			shape = gPhysics->createShape(PxCapsuleGeometry(radius, height / 2), *pxMaterial);
		}
	}
	else if (geometryType == PxGeometryType::eCONVEXMESH)
	{
		PROFILE_FUNCTION("eCONVEXMESH");
		//fatal("Convex mesh not implemented yet");
		std::string col_mesh_name = jcfg.value("collision_mesh", "");
		const CCollisionMesh* mesh = Resources.get(col_mesh_name)->as<CCollisionMesh>();
		dbg("Collision mesh has %d vtxs\n", mesh->header.num_vertex);

		assert(strcmp(mesh->header.vertex_type_name, "Pos") == 0);

		PxConvexMeshDesc meshDesc;
		meshDesc.points.count = mesh->header.num_vertex;
		meshDesc.points.stride = sizeof(PxVec3);
		meshDesc.points.data = mesh->vertices.data();

		// Swap faces for physics
		meshDesc.flags |= PxConvexFlag::eCOMPUTE_CONVEX;

		// Indices
		meshDesc.indices.count = mesh->header.num_indices / 3;
		meshDesc.indices.stride = 3 * mesh->header.bytes_per_index;
		meshDesc.indices.data = mesh->indices.data();
		if (mesh->header.bytes_per_index == 2)
			meshDesc.flags |= PxConvexFlag::e16_BIT_INDICES;

		// We could save this cooking process
		PxTolerancesScale scale;
		PxCooking *cooking = PxCreateCooking(PX_PHYSICS_VERSION, gPhysics->getFoundation(), PxCookingParams(scale));

		PxDefaultMemoryOutputStream writeBuffer;
		PxConvexMeshCookingResult::Enum result;
		bool status = cooking->cookConvexMesh(meshDesc, writeBuffer, &result);
		assert(status);

		// writeBuffer could be saved to avoid the cooking in the next execution.
		PxDefaultMemoryInputData readBuffer(writeBuffer.getData(), writeBuffer.getSize());
		PxConvexMesh* convex_mesh = gPhysics->createConvexMesh(readBuffer);
		shape = gPhysics->createShape(PxConvexMeshGeometry(convex_mesh), *pxMaterial);
		assert(shape);

		// We could create CMesh and save it in shape->userData
		CMesh* render_mesh = mesh->createRenderMesh();
		char res_name[64];
		sprintf(res_name, "Physics_%p", render_mesh);
		render_mesh->setNameAndType(res_name, getResourceTypeFor<CMesh>());
		Resources.registerResource(render_mesh);

		// Bind the render mesh as userData of the SHAPE, not the ACTOR.
		shape->userData = render_mesh;
	}
	else if (geometryType == PxGeometryType::eTRIANGLEMESH)
	{
		PROFILE_FUNCTION("eTRIANGLEMESH");
		std::string col_mesh_name = jcfg.value("collision_mesh", "");
		const CCollisionMesh* mesh = Resources.get(col_mesh_name)->as<CCollisionMesh>();
		dbg("Collision mesh has %d vtxs\n", mesh->header.num_vertex);

		assert(strcmp(mesh->header.vertex_type_name, "Pos") == 0);

		PxTriangleMeshDesc meshDesc;
		meshDesc.points.count = mesh->header.num_vertex;
		meshDesc.points.stride = sizeof(PxVec3);
		meshDesc.points.data = mesh->vertices.data();

		// Swap faces for physics
		meshDesc.flags |= PxMeshFlag::eFLIPNORMALS;

		// Indices
		meshDesc.triangles.count = mesh->header.num_indices / 3;
		meshDesc.triangles.stride = 3 * mesh->header.bytes_per_index;
		meshDesc.triangles.data = mesh->indices.data();
		if (mesh->header.bytes_per_index == 2)
			meshDesc.flags |= PxMeshFlag::e16_BIT_INDICES;

		// We could save this cooking process
		PxTolerancesScale scale;
		PxCooking *cooking = PxCreateCooking(PX_PHYSICS_VERSION, gPhysics->getFoundation(), PxCookingParams(scale));

		// Search for the serialized cooked mesh if it exists first to save time
		int start = col_mesh_name.rfind('/');
		int end = col_mesh_name.rfind('.');
		std::string col_name = col_mesh_name.substr(start + 1, (end - start - 1));
		std::string tribuffer_name = "data/tribuffers/" + col_name + ".tribuffer";
		const CTriBuffer* tribuffer = Resources.get(tribuffer_name)->as<CTriBuffer>();

		// If it doesn't exist, we need to cook and serialize it as a resource
		if (!tribuffer->isValid()) {
			// Cook the trimesh
			// The cooking process writes the trimesh in this buffer
			PxDefaultMemoryOutputStream writeBuffer;
			PxTriangleMeshCookingResult::Enum result;
			bool status = cooking->cookTriangleMesh(meshDesc, writeBuffer, &result);
			assert(status);
			// update resource
			CTriBuffer* nc_tribuffer = (CTriBuffer*)tribuffer;
			nc_tribuffer->write(writeBuffer.getData(), writeBuffer.getSize());
		}

		// Either way the buffer is ready here
		PxDefaultMemoryInputData readBuffer((PxU8*)tribuffer->getData(), (PxU32)tribuffer->getSize());
		PxTriangleMesh* tri_mesh = gPhysics->createTriangleMesh(readBuffer);
		shape = gPhysics->createShape(PxTriangleMeshGeometry(tri_mesh), *pxMaterial);
		assert(shape);

		// We could create CMesh and save it in shape->userData
		CMesh* render_mesh = mesh->createRenderMesh();
		char res_name[64];
		sprintf(res_name, "Physics_%p", render_mesh);
		render_mesh->setNameAndType(res_name, getResourceTypeFor<CMesh>());
		Resources.registerResource(render_mesh);

		// Bind the render mesh as userData of the SHAPE, not the ACTOR.
		shape->userData = render_mesh;
	}

	if (jcfg.value("trigger", false))
	{
		shape->setFlag(PxShapeFlag::eSIMULATION_SHAPE, false);
		shape->setFlag(PxShapeFlag::eTRIGGER_SHAPE, true);
	}

	// Shape offset
	if (jcfg.count("offset")) {
		CTransform trans;
		trans.load(jcfg["offset"]);
		shape->setLocalPose(toPxTransform(trans));
	}

	assert(shape);

	setupFiltering(shape,
		getFilterByName(jcfg.value("group", "all")), //ID
		getFilterByName(jcfg.value("mask", "all"))   //Mask to filter trigger callbacks
	);

	actor->attachShape(*shape);
	shape->release();
	return true;
}

// --------------------------------------------------------------
void CModulePhysics::createActor(TCompCollider& comp_collider)
{
	const json& jconfig = comp_collider.jconfig;

	// Entity start transform
	TCompTransform* c = comp_collider.get<TCompTransform>();
	PxTransform transform = toPxTransform(*c);

	PxRigidActor* actor = nullptr;

	bool is_controller = jconfig.count("controller") > 0;

	// Controller or physics based?
	if (is_controller) {
		actor = createController(comp_collider);

	}
	else {

		// Dynamic or static actor?
		bool isDynamic = jconfig.value("dynamic", false);
		if (isDynamic)
		{
			PxRigidDynamic* dynamicActor = gPhysics->createRigidDynamic(transform);
			PxReal density = jconfig.value("density", 1.0f);
			PxRigidBodyExt::updateMassAndInertia(*dynamicActor, density);
			//actor = dynamicActor;

			if (jconfig.value("kinematic", false))
				dynamicActor->setRigidBodyFlag(PxRigidBodyFlag::eKINEMATIC, true);
			else
			{
				//Set Mass
				float m = jconfig.value("mass", 1.0f);
				dynamicActor->setMass(m);

				//Set Drags
				float lDrag = jconfig.value("linearDrag", 0.5f);
				dynamicActor->setLinearDamping(lDrag);
				float aDrag = jconfig.value("angularDrag", 0.25f);
				dynamicActor->setAngularDamping(aDrag);

				//kinematic actors are incompatible with CCD
				bool ccd_on = jconfig.value("continuous_collision", true);
				dynamicActor->setRigidBodyFlag(PxRigidBodyFlag::eENABLE_CCD, ccd_on);
			}

			actor = dynamicActor;
		}
		else
		{
			PxRigidStatic* static_actor = gPhysics->createRigidStatic(transform);
			actor = static_actor;
		}
	}

	// The capsule was already loaded as part of the controller
	if (!is_controller) {
		// if shapes exists, try to read as an array of shapes
		if (jconfig.count("shapes")) {
			const json& jshapes = jconfig["shapes"];
			for (const json& jshape : jshapes)
				readShape(actor, jshape);
		}
		// Try to read shape directly (general case...)
		else {
			readShape(actor, jconfig);
		}
	}

	// PhysX is complaining 'adding the actor twice...'
	if (!is_controller)
		gScene->addActor(*actor);

	comp_collider.actor = actor;
	CHandle h_collider(&comp_collider);
	comp_collider.actor->userData = h_collider.asVoidPtr();

	// Joints
	if (jconfig.count("joints")) {
		for (const json& j : jconfig["joints"]) {
			TJoint joint;
			joint.joint_type = j.value("type", "spherical");
			joint.obj0.name = j["obj0"].value("name", "");
			joint.obj0.transform.load(j["obj0"]["transform"]);
			joint.obj1.name = j["obj1"].value("name", "");
			joint.obj1.transform.load(j["obj1"]["transform"]);
			joint.create();
			joints.push_back(joint);
		}
	}
}

void CModulePhysics::createRagdoll(TCompRagdoll& comp_ragdoll) {
	if (comp_ragdoll.ragdoll.created) return;
	CHandle h_comp_ragdoll(&comp_ragdoll);
	CEntity* e = h_comp_ragdoll.getOwner();

	TCompSkeleton* skel = e->get<TCompSkeleton>();
	TCompTransform* comp_transform = e->get<TCompTransform>();
	CTransform* entity_trans = (CTransform*)comp_transform;

	auto core_skel = (CGameCoreSkeleton*)skel->model->getCoreModel();
	core_skel->ragdoll_core;

	//For every bone that has a collider attatched we will create a physX actor
	for (auto& ragdoll_bone_core : core_skel->ragdoll_core.ragdoll_bone_cores) {
		auto cal_core_bone = core_skel->getCoreSkeleton()->getCoreBone(ragdoll_bone_core.bone);
		assert(cal_core_bone);

		//Read the pos and rotation offset we want to apply
		const json& jcfg = ragdoll_bone_core.jconfig;
		VEC3 offsetPos = loadVEC3(jcfg, "offsetPos");
		VEC3 rotationRPY = loadVEC3(jcfg, "rotationRPY");

		//Set position and rotation of the collider
		CTransform trans;
		trans.setPosition(Cal2DX(cal_core_bone->getTranslationAbsolute()) + offsetPos);
		trans.setRotation(Cal2DX(cal_core_bone->getRotationAbsolute()));
		//trans.setRotation(QUAT::CreateFromYawPitchRoll(rotationRPY.z, rotationRPY.y, rotationRPY.x)); //Doesn't work, same than before

		//Set the wanted shape with JSON data
		PxShape* shape = nullptr;
		PxGeometryType::Enum geometryType = readGeometryType(jcfg);
		if (geometryType == PxGeometryType::eBOX) //"box"
		{
			VEC3 jhalfExtent = loadVEC3(jcfg, "half_size");
			jhalfExtent /= 2.0f;
			PxVec3 halfExtent = VEC3_TO_PXVEC3(jhalfExtent);
			shape = gPhysics->createShape(PxBoxGeometry(halfExtent), *gMaterial);
		}
		else if (geometryType == PxGeometryType::eSPHERE) //"sphere"
		{
			PxReal radius = jcfg.value("radius", 1.f);
			shape = gPhysics->createShape(PxSphereGeometry(radius), *gMaterial);
		}
		else if (geometryType == PxGeometryType::eCAPSULE) //"capsule"
		{
			float radius = jcfg.value("radius", 0.5f);
			float height = jcfg.value("height", 0.1f);
			shape = gPhysics->createShape(PxCapsuleGeometry(radius, height), *gMaterial);
		}

		PxTransform px_entity_transform = toPxTransform(*entity_trans);
		PxTransform px_transform = toPxTransform(trans);
		px_transform = px_entity_transform * px_transform;

		//Create actual PhysX actor with position and shape we decided
		//But it will change in next comp_ragdoll update, so it doesn't rly matter

		PxRigidActor* actor = nullptr;
		PxRigidDynamic* dynamicActor = gPhysics->createRigidDynamic(px_transform);
		if (dynamicActor == nullptr) return;
		PxRigidBodyExt::updateMassAndInertia(*dynamicActor, 1.f);
		dynamicActor->setSolverIterationCounts(16, 16);
		dynamicActor->setRigidBodyFlag(PxRigidBodyFlag::eENABLE_CCD, true);
		actor = dynamicActor;
		ragdoll_bone_core.shape = shape;
		setupFiltering(shape, CModulePhysics::FilterGroup::Bones, CModulePhysics::FilterGroup::All);

		//It completely messes up boxes, but doesn't affect casules. Why?
		//CTransform offset;
		//offset.setPosition(VEC3(0.0f, ragdoll_bone_core.height, 0.0f));
		//shape->setLocalPose(toPxTransform(offset));

		actor->attachShape(*shape);
		ragdoll_bone_core.actor = actor;
		//shape->release();
		PxRigidDynamic* body = (PxRigidDynamic*)actor;
		assert(body);

		TRagdoll::TRagdollBone& ragdoll_bone = comp_ragdoll.ragdoll.bones[comp_ragdoll.ragdoll.num_bones];
		ragdoll_bone.actor = body;
		ragdoll_bone.core = &ragdoll_bone_core;

		ragdoll_bone.idx = core_skel->getCoreSkeleton()->getCoreBoneId(ragdoll_bone_core.bone);
		ragdoll_bone.core->instance_idx = comp_ragdoll.ragdoll.num_bones;
		++comp_ragdoll.ragdoll.num_bones;
	}

	for (int i = 0; i < comp_ragdoll.ragdoll.num_bones; ++i) {
		auto& ragdoll_bone = comp_ragdoll.ragdoll.bones[i];
		if (ragdoll_bone.core->parent_core) {
			ragdoll_bone.parent_idx = ragdoll_bone.core->parent_core->instance_idx;
			auto& parent_ragdoll_bone = comp_ragdoll.ragdoll.bones[ragdoll_bone.parent_idx];

			parent_ragdoll_bone.children_idxs[parent_ragdoll_bone.num_children] = i;
			++parent_ragdoll_bone.num_children;
		}
	}

	createRagdollJoints(comp_ragdoll, 0);
	comp_ragdoll.ragdoll.created = true;
}

void CModulePhysics::createRagdollJoints(TCompRagdoll& comp_ragdoll, int bone_id) {
	TRagdoll::TRagdollBone& ragdoll_bone = comp_ragdoll.ragdoll.bones[bone_id];

	for (int i = 0; i < ragdoll_bone.num_children; ++i) {
		auto child_id = ragdoll_bone.children_idxs[i];
		TRagdoll::TRagdollBone& child_ragdoll_bone = comp_ragdoll.ragdoll.bones[child_id];

		PxVec3 offset(0.1f, 0.f, 0.f);

		assert(child_ragdoll_bone.actor);
		assert(ragdoll_bone.actor);
		PxVec3 d1 = child_ragdoll_bone.actor->getGlobalPose().q.rotate(PxVec3(1, 0, 0));
		PxVec3 d2 = ragdoll_bone.actor->getGlobalPose().q.rotate(PxVec3(1, 0, 0));
		PxVec3 axis = d1.cross(d2).getNormalized();
		PxVec3 pos = ragdoll_bone.actor->getGlobalPose().p;
		PxVec3 diff = (pos - child_ragdoll_bone.actor->getGlobalPose().p).getNormalized();
		if (diff.dot(d2) < 0) d2 = -d2;

		PxShape* shape;
		if (ragdoll_bone.actor->getShapes(&shape, 1) == 1)
		{
			PxCapsuleGeometry capsule;
			if (shape->getCapsuleGeometry(capsule))
			{
				pos -= (capsule.halfHeight + capsule.radius) * d2;
			}
		}

		PxTransform tr0 = PxTransform(PxVec3(0.f, 0.f, 0.f));
		PxMat44 mat(d1, axis, d1.cross(axis).getNormalized(), pos);
		PxTransform pxTrans(mat);
		if (pxTrans.isSane())
		{
			PxTransform ragdoll_bone_actor_trans = ragdoll_bone.actor->getGlobalPose();
			if (ragdoll_bone_actor_trans.isSane())
			{
				PxTransform ragdoll_bone_actor_trans_inv = ragdoll_bone_actor_trans.getInverse();
				if (ragdoll_bone_actor_trans_inv.isSane())
				{
					tr0 = ragdoll_bone_actor_trans_inv * pxTrans;
				}
			}
		}

		PxTransform tr1 = child_ragdoll_bone.actor->getGlobalPose().getInverse() * ragdoll_bone.actor->getGlobalPose() * tr0;
		if (!tr1.isValid()) {
			tr1 = PxTransform(PxVec3(0.f, 0.f, 0.f));
		}

		PxJoint* joint = nullptr;
		/*joint = PxSphericalJointCreate(gScene->getPhysics(), ragdoll_bone.actor, tr0, child_ragdoll_bone.actor, tr1);
		assert(joint);
		if (joint)
		{
			auto* spherical = static_cast<PxSphericalJoint*>(joint);
			spherical->setProjectionLinearTolerance(0.1f);
			spherical->setLimitCone(PxJointLimitCone(0.2f, 0.2f, 0.01f)); //elliptical angular cone: PxReal yLimitAngle, PxReal zLimitAngle
	  spherical->setSphericalJointFlag(physx::PxSphericalJointFlag::eLIMIT_ENABLED, true);
		}*/

		joint = PxD6JointCreate(gScene->getPhysics(), ragdoll_bone.actor, tr0, child_ragdoll_bone.actor, tr1);
		if (joint)
		{
			auto* d6j = static_cast<PxD6Joint*>(joint);
			//Origin translation axis
			d6j->setMotion(PxD6Axis::eX, PxD6Motion::eLOCKED);
			d6j->setMotion(PxD6Axis::eY, PxD6Motion::eLOCKED);
			d6j->setMotion(PxD6Axis::eZ, PxD6Motion::eLOCKED);

			d6j->setMotion(PxD6Axis::eTWIST, PxD6Motion::eLOCKED); //X rotation
			d6j->setMotion(PxD6Axis::eSWING1, PxD6Motion::eLIMITED); //Y rotation
			d6j->setMotion(PxD6Axis::eSWING2, PxD6Motion::eLIMITED); //Y rotation
			d6j->setSwingLimit(PxJointLimitCone(PxPi / 6, PxPi / 6, 0.01f));

			joint->setConstraintFlag(PxConstraintFlag::eVISUALIZATION, true);
			joint->setConstraintFlag(PxConstraintFlag::eCOLLISION_ENABLED, false);
			joint->setConstraintFlag(PxConstraintFlag::ePROJECTION, true);
		}
		child_ragdoll_bone.parent_joint = joint;

		createRagdollJoints(comp_ragdoll, child_id);
	}

}

// ------------------------------------------------------------------
PxFilterFlags CustomFilterShader(
	PxFilterObjectAttributes attributes0, PxFilterData filterData0,
	PxFilterObjectAttributes attributes1, PxFilterData filterData1,
	PxPairFlags& pairFlags, const void* constantBlock, PxU32 constantBlockSize
)
{
	if ((filterData0.word0 & filterData1.word1) && (filterData1.word0 & filterData0.word1))
	{
		if (PxFilterObjectIsTrigger(attributes0) || PxFilterObjectIsTrigger(attributes1))
		{
			pairFlags = PxPairFlag::eTRIGGER_DEFAULT;
		}
		else {
			pairFlags = PxPairFlag::eCONTACT_DEFAULT | PxPairFlag::eNOTIFY_TOUCH_FOUND | PxPairFlag::eNOTIFY_TOUCH_LOST | PxPairFlag::eDETECT_CCD_CONTACT | PxPairFlag::eNOTIFY_TOUCH_CCD | PxPairFlag::eNOTIFY_CONTACT_POINTS;
		}
		return PxFilterFlag::eDEFAULT;
	}
	return PxFilterFlag::eSUPPRESS;
}

// ------------------------------------------------------------------
bool CModulePhysics::start()
{
	gFoundation = PxCreateFoundation(PX_FOUNDATION_VERSION, gAllocator, gErrorCallback);

	gPvd = PxCreatePvd(*gFoundation);
	PxPvdTransport* transport = PxDefaultPvdSocketTransportCreate("127.0.0.1", 5425, 10);
	gPvd->connect(*transport, PxPvdInstrumentationFlag::eALL);

	gPhysics = PxCreatePhysics(PX_PHYSICS_VERSION, *gFoundation, PxTolerancesScale(), true, gPvd);
	PxInitExtensions(*gPhysics, gPvd);

	PxSceneDesc sceneDesc(gPhysics->getTolerancesScale());
	sceneDesc.gravity = PxVec3(0.0f, gravity, 0.0f);
	gDispatcher = PxDefaultCpuDispatcherCreate(2);
	sceneDesc.cpuDispatcher = gDispatcher;
	sceneDesc.filterShader = CustomFilterShader;
	sceneDesc.flags |= PxSceneFlag::eENABLE_CCD;
	//sceneDesc.ccdMaxSeparation = 0;
	gScene = gPhysics->createScene(sceneDesc);
	gScene->setFlag(PxSceneFlag::eENABLE_ACTIVE_ACTORS, true);
	PxPvdSceneClient* pvdClient = gScene->getScenePvdClient();
	if (pvdClient)
	{
		pvdClient->setScenePvdFlag(PxPvdSceneFlag::eTRANSMIT_CONSTRAINTS, true);
		pvdClient->setScenePvdFlag(PxPvdSceneFlag::eTRANSMIT_CONTACTS, true);
		pvdClient->setScenePvdFlag(PxPvdSceneFlag::eTRANSMIT_SCENEQUERIES, true);
	}
	//Default material
	gMaterial = gPhysics->createMaterial(0.5f, 0.5f, 0.1f);

	gControllerManager = PxCreateControllerManager(*gScene);

	gScene->setSimulationEventCallback(&customSimulationEventCallback);

	CEntity* player = getEntityByName("Player");
	return true;
}

// ------------------------------------------------------------------
void CModulePhysics::update(float scaled_dt) {

	//=================================================================
	//DIRTY SOLUTION TO INITIAL FLYING OBJECTS
	totalTime += scaled_dt;
	//if (totalTime < 1.f) return;
	//if (isPaused) return;

	//TODO: FIGURE THIS WHOLE DELTA TIME THING OUT
	//Right now it protects against slooow frames
	//dbg("DELTA PHX: %f \n", delta);
	if (scaled_dt > 0.1f) scaled_dt = 0.1f;
	//if (delta < 0.01f) delta = 0.01f;
	//=================================================================

	{
		PROFILE_FUNCTION("Simulate");
		gScene->simulate(scaled_dt);
	}
	{
		PROFILE_FUNCTION("fetch");
		gScene->fetchResults(true);
	}

	PxU32 nbActorsOut = 0;
	PxActor** activeActors = gScene->getActiveActors(nbActorsOut);
	for (unsigned int i = 0; i < nbActorsOut; ++i)
	{
		PROFILE_FUNCTION("Actor");
		PxRigidActor* rigidActor = ((PxRigidActor*)activeActors[i]);
		assert(rigidActor);
		CHandle h_collider;
		h_collider.fromVoidPtr(rigidActor->userData);
		if (!h_collider.getOwner().isValid()) continue;
		TCompCollider* c_collider = h_collider;
		CEntity* entity = h_collider.getOwner();
		if (c_collider != nullptr)
		{
			TCompTransform* c = c_collider->get<TCompTransform>();
			assert(c);
			if (c_collider->controller) {
				PxExtendedVec3 pxpos_ext = c_collider->controller->getFootPosition();
				c->setPosition(VEC3((float)pxpos_ext.x, (float)pxpos_ext.y, (float)pxpos_ext.z));
			}
			else {
				PxTransform PxTrans = rigidActor->getGlobalPose();
				c->set(toTransform(PxTrans));
			}
		}
	}
}

void CModulePhysics::stop() {
	gScene->release(); gScene = nullptr;
	gDispatcher->release();
	gPhysics->release();
	PxPvdTransport* transport = gPvd->getTransport();
	gPvd->release();
	transport->release();
	gFoundation->release();
}

void CModulePhysics::renderInMenu()
{
	debugInMenuJoints();
}

void CModulePhysics::renderDebug() {

	// Show all the joints
	for (auto& c : joints) {
		PxRigidActor* actors[2];
		c.px_joint->getActors(actors[0], actors[1]);
		for (int i = 0; i < 2; ++i) {
			if (!actors[i])
				continue;
			CTransform local_t = toTransform(c.px_joint->getLocalPose(i == 0 ? PxJointActorIndex::eACTOR0 : PxJointActorIndex::eACTOR1));
			CTransform actor_t = toTransform(actors[i]->getGlobalPose());
			MAT44 world = local_t.asMatrix() * actor_t.asMatrix();
			drawAxis(world);
		}
	}
}

CModulePhysics::FilterGroup CModulePhysics::getFilterByName(const std::string& name)
{
	if (strcmp("player", name.c_str()) == 0) {
		return CModulePhysics::FilterGroup::Player;
	}
	else if (strcmp("enemy", name.c_str()) == 0) {
		return CModulePhysics::FilterGroup::Enemy;
	}
	else if (strcmp("characters", name.c_str()) == 0) {
		return CModulePhysics::FilterGroup::Characters;
	}
	else if (strcmp("triggers", name.c_str()) == 0) {
		return CModulePhysics::FilterGroup::Triggers;
	}
	else if (strcmp("objects", name.c_str()) == 0) {
		return CModulePhysics::FilterGroup::Objects;
	}
	else if (strcmp("wall", name.c_str()) == 0) {
		return CModulePhysics::FilterGroup::Wall;
	}
	else if (strcmp("floor", name.c_str()) == 0) {
		return CModulePhysics::FilterGroup::Floor;
	}
	else if (strcmp("scenario", name.c_str()) == 0) {
		return CModulePhysics::FilterGroup::Scenario;
	}
	else if (strcmp("magicOnly", name.c_str()) == 0) {
		return CModulePhysics::FilterGroup::MagicOnly;
	}
	return CModulePhysics::FilterGroup::All;
}


void CModulePhysics::setupFiltering(PxShape* shape, PxU32 filterGroup, PxU32 filterMask)
{
	PxFilterData filterData;
	filterData.word0 = filterGroup; // word0 = own ID
	filterData.word1 = filterMask;	// word1 = ID mask to filter pairs that trigger a contact callback;
	shape->setSimulationFilterData(filterData);
	shape->setQueryFilterData(filterData);
}

void CModulePhysics::setupFilteringOnAllShapesOfActor(PxRigidActor* actor, PxU32 filterGroup, PxU32 filterMask)
{
	const PxU32 numShapes = actor->getNbShapes();
	std::vector<PxShape*> shapes;
	shapes.resize(numShapes);
	actor->getShapes(&shapes[0], numShapes);
	for (PxU32 i = 0; i < numShapes; i++)
		setupFiltering(shapes[i], filterGroup, filterMask);
}

void CModulePhysics::CustomSimulationEventCallback::onTrigger(PxTriggerPair* pairs, PxU32 count)
{
	PROFILE_FUNCTION("onTrigger");
	for (PxU32 i = 0; i < count; i++)
	{
		// ignore pairs when shapes have been deleted
		if (pairs[i].flags & (PxTriggerPairFlag::eREMOVED_SHAPE_TRIGGER | PxTriggerPairFlag::eREMOVED_SHAPE_OTHER))
			continue;

		CHandle h_comp_trigger;
		h_comp_trigger.fromVoidPtr(pairs[i].triggerActor->userData);

		CHandle h_comp_other;
		h_comp_other.fromVoidPtr(pairs[i].otherActor->userData);

		CEntity* e_trigger = h_comp_trigger.getOwner();
		CEntity* e_other = h_comp_other.getOwner();

		if (pairs[i].status == PxPairFlag::eNOTIFY_TOUCH_FOUND)
		{
			TMsgEntityTriggerEnter msg;

			// Notify the trigger someone entered
			msg.h_entity = h_comp_other.getOwner();
			if (e_trigger)
				e_trigger->sendMsg(msg);

			// Notify that someone he entered in a trigger
			msg.h_entity = h_comp_trigger.getOwner();
			if (e_other)
				e_other->sendMsg(msg);
		}
		else if (pairs[i].status == PxPairFlag::eNOTIFY_TOUCH_LOST)
		{
			TMsgEntityTriggerExit msg;

			// Notify the trigger someone exit
			msg.h_entity = h_comp_other.getOwner();
			if (e_trigger)
				e_trigger->sendMsg(msg);

			// Notify that someone he exit a trigger
			msg.h_entity = h_comp_trigger.getOwner();
			if (e_other)
				e_other->sendMsg(msg);
		}
	}
}

void CModulePhysics::CustomSimulationEventCallback::onContact(const physx::PxContactPairHeader& pairHeader, const physx::PxContactPair* pairs, physx::PxU32 nbPairs) {
	const PxU32 bufferSize = 64;
	PxContactPairPoint contacts[bufferSize] = {};

	for (PxU32 i = 0; i < nbPairs; i++)
	{
		const PxContactPair& cp = pairs[i];
		PxU32 nbContacts = pairs[i].extractContacts(contacts, bufferSize);

		if (cp.events & PxPairFlag::eNOTIFY_TOUCH_FOUND)
		{
			CHandle h_comp_physics;
			h_comp_physics.fromVoidPtr(pairHeader.actors[0]->userData);
			CEntity* entityContact1 = h_comp_physics.getOwner();
			CHandle h_comp_physics2;
			h_comp_physics2.fromVoidPtr(pairHeader.actors[1]->userData);
			CEntity* entityContact2 = h_comp_physics2.getOwner();

			TMsgOnContact msg;

			if (entityContact1 != nullptr) {
				msg.source = h_comp_physics2;
				msg.normal = PXVEC3_TO_VEC3(contacts[0].normal);
				entityContact1->sendMsg(msg);
			}
			if (entityContact2 != nullptr) {
				msg.source = h_comp_physics;
				msg.normal = PXVEC3_TO_VEC3(contacts[0].normal);
				entityContact2->sendMsg(msg);
			}
		}
		if (cp.events & PxPairFlag::eNOTIFY_TOUCH_LOST)
		{
			CHandle h_comp_physics;
			h_comp_physics.fromVoidPtr(pairHeader.actors[0]->userData);
			CEntity* entityContact1 = h_comp_physics.getOwner();
			CHandle h_comp_physics2;
			h_comp_physics2.fromVoidPtr(pairHeader.actors[1]->userData);
			CEntity* entityContact2 = h_comp_physics2.getOwner();

			TMsgOnLostContact msg;

			if (entityContact1 != nullptr) {
				msg.source = h_comp_physics2;
				entityContact1->sendMsg(msg);
			}
			if (entityContact2 != nullptr) {
				msg.source = h_comp_physics;
				entityContact2->sendMsg(msg);
			}
		}
	}
}

void CModulePhysics::CustomUserControllerHitReport::onShapeHit(const physx::PxControllerShapeHit& hit) {
	CHandle h_comp_physics;
	h_comp_physics.fromVoidPtr(hit.actor->userData);
	CEntity* entityContact = h_comp_physics.getOwner();

	PxRigidDynamic* rb_src = hit.actor->is<PxRigidDynamic>();
	VEC3 vel = VEC3(0, 0, 0);
	if (rb_src)
		vel = PXVEC3_TO_VEC3(rb_src->getLinearVelocity());

	CHandle h_comp_character;
	h_comp_character.fromVoidPtr(hit.controller->getActor()->userData);
	CEntity* entityCharacter = h_comp_character.getOwner();

	TMsgOnContact msg;

	VEC3 a = PXVEC3_TO_VEC3(hit.worldPos);
	msg.pos = a;
	msg.vel = vel;

	if (entityContact != nullptr) {
		msg.source = h_comp_character;
		entityContact->sendMsg(msg);
	}

	if (entityCharacter != nullptr) {
		msg.source = h_comp_physics;
		entityCharacter->sendMsg(msg);
	}
}

void CModulePhysics::CustomUserControllerHitReport::onControllerHit(const physx::PxControllersHit& hit) {
	CHandle h_comp_character;
	CHandle h_comp_enemy;

	h_comp_character.fromVoidPtr(hit.controller->getActor()->userData);
	h_comp_enemy.fromVoidPtr(hit.other->getActor()->userData);

	CEntity* entityCharacter = h_comp_character.getOwner();
	CEntity* entityEnemy = h_comp_enemy.getOwner();

	TMsgOnControllerHit msg;

	msg.h_controller = h_comp_character.getOwner();
	msg.h_other = h_comp_enemy.getOwner();
	if (entityCharacter)
		entityCharacter->sendMsg(msg);
	if (entityEnemy)
		entityEnemy->sendMsg(msg);
}

void CModulePhysics::CustomUserControllerHitReport::onObstacleHit(const physx::PxControllerObstacleHit& hit) {
	//dbg("obstacle hit");
}

physx::PxMaterial* CModulePhysics::CreateMaterial(const VEC3 & settings)
{
	return gPhysics->createMaterial(settings.x, settings.y, settings.z);
}

/* Returns true if there was some hit with the sweep cast. Hit will contain all hits */
bool CModulePhysics::Raycast(const VEC3 & origin, const VEC3 & dir, float distance, physx::PxRaycastHit & hit, physx::PxQueryFlags queryFlags, physx::PxHitFlags hitFlags, physx::PxQueryFilterData filterdata)
{
	PxVec3 px_origin = VEC3_TO_PXVEC3(origin);
	PxVec3 px_dir = VEC3_TO_PXVEC3(dir);
	PxReal px_distance = (PxReal)(distance);

	PxRaycastBuffer px_hit;
	filterdata.flags = queryFlags;

	bool status = gScene->raycast(px_origin, px_dir, px_distance, px_hit, hitFlags, filterdata); // Closest hit
	hit = px_hit.block;

	return status;
}

/* Returns true if there was some hit with the sphere cast. Hit will contain all hits */
bool CModulePhysics::Overlap(physx::PxGeometry& geometry, VEC3 pos, std::vector<physx::PxOverlapHit> & hits, physx::PxQueryFilterData filterdata)
{
	PxOverlapHit overlapHit[256];
	PxOverlapBuffer px_hit(overlapHit, 256);

	physx::PxTransform transform(PxVec3(pos.x, pos.y, pos.z));

	bool status = gScene->overlap(geometry, transform, px_hit, filterdata);

	if (status) {
		for (PxU32 i = 0; i < px_hit.nbTouches; i++) {
			hits.push_back(px_hit.touches[i]);
		}
	}

	return status;
}

void CModulePhysics::AddExplosion(const VEC3& pos, const float& radius, const float& knockback, const float& damage, const float& minKnockBack) {
	PxSphereGeometry geometry;
	geometry.radius = radius;
	std::vector<physx::PxOverlapHit> hits;
	PxFilterData pxFilterData;
	pxFilterData.word0 = CModulePhysics::FilterGroup::Enemy | CModulePhysics::FilterGroup::Objects | CModulePhysics::FilterGroup::Player | CModulePhysics::FilterGroup::Scenario | CModulePhysics::FilterGroup::Characters;
	PxQueryFilterData explosionDetectionFilter;
	explosionDetectionFilter.data = pxFilterData;

	//Screenshake
	CEntity* player = getEntityByName("Player"); player = getEntityByName("Player");
	TCompTransform* playerT = player->get<TCompTransform>();
	if (playerT)
	{
		float dist = euclideanDistance(playerT->getPosition(), pos);
		float screenShakeFactor = 1 - clampFloat01(dist / 40.0f);
		EngineBasics.ShakeCamera(1.25f * screenShakeFactor, 0.2f);
	}
	

	if (Overlap(geometry, pos, hits, explosionDetectionFilter)) {
		for (int i = 0; i < hits.size(); i++) {
			CHandle hitElement;
			hitElement.fromVoidPtr(hits[i].actor->userData);
			if (hitElement.getOwner().isValid()) {
				hitElement = hitElement.getOwner();
				CEntity* hitEntity = (CEntity*)hitElement;
				VEC3 vecForce;
				float distance = 0;
				float realKnockBack = 0;

				//Character Controller
				TCompCharController* playerCC = hitEntity->get<TCompCharController>();
				if (playerCC) {
					TCompTransform* hitTransform = hitEntity->get<TCompTransform>();
					distance = hitTransform->manhattanDistance(pos);
					realKnockBack = knockback * (1 - clampFloat(distance / radius, minKnockBack, 1.0f));
					vecForce = hitTransform->getPosition() - pos;
					vecForce.Normalize();
					playerCC->addExternForce(vecForce * realKnockBack);

					DamageMessage(hitEntity, damage, VEC3(0, 0, 0));
					continue;
				}

				//Fireball
				TCompFireballController* fireball = hitEntity->get<TCompFireballController>();
				if (fireball) {
					TMsgDestroy desMsg;
					desMsg.h_object = hitEntity;
					hitEntity->sendMsg(desMsg);
					continue;
				}

				//Gnome
				bt_gnome* hitGnome = hitEntity->get<bt_gnome>();
				if (hitGnome) {
					DamageMessage(hitEntity, damage, VEC3(0, 0, 0));
					continue;
				}

				//Objects
				TCompCollider* hitCollider = hitEntity->get<TCompCollider>();
				if (hitCollider)
				{
					PxRigidDynamic* rb = hitCollider->actor->is<PxRigidDynamic>();
					PxRigidStatic* rs = hitCollider->actor->is<PxRigidStatic>();

					if (rb and !rb->getRigidBodyFlags().isSet(physx::PxRigidBodyFlag::eKINEMATIC)) {
						TCompTransform* hitTransform = hitEntity->get<TCompTransform>();
						distance = hitTransform->manhattanDistance(pos);
						realKnockBack = knockback * (1 - clampFloat(distance / radius, minKnockBack, 1.0f));
						vecForce = hitTransform->getPosition() - pos;
						vecForce.Normalize();
						vecForce *= realKnockBack;
						rb->addForce(VEC3_TO_PXVEC3(vecForce), PxForceMode::eIMPULSE);

						DamageMessage(hitEntity, damage, vecForce);
						continue;
					}
					else if (rb and rb->getRigidBodyFlags().isSet(physx::PxRigidBodyFlag::eKINEMATIC))
					{
						DamageMessage(hitEntity, damage, VEC3(0, 0, 0));
						continue;
					}
					else if (rs) {
						TCompTransform* hitTransform = hitEntity->get<TCompTransform>();
						distance = hitTransform->manhattanDistance(pos);
						realKnockBack = knockback * (1 - clampFloat(distance / radius, minKnockBack, 1.0f));
						vecForce = hitTransform->getPosition() - pos;
						vecForce.Normalize();
						vecForce *= realKnockBack;

						DamageMessage(hitEntity, damage, vecForce);
						continue;
					}
				}
			}
		}
	}
}


void CModulePhysics::DamageMessage(CEntity* hitEntity, float damage, VEC3 force) {
	TMsgDamage dmgMsg;

	if (hitEntity != NULL) {
		dmgMsg.h_bullet = hitEntity;
		dmgMsg.h_sender = hitEntity;
		dmgMsg.damage = damage;
		if (force != VEC3(0, 0, 0)) {
			dmgMsg.force = force;
		}

		hitEntity->sendMsg(dmgMsg);
	}
}


void CModulePhysics::debugInMenuJoints() {
	if (ImGui::TreeNode("Joints")) {
		for (auto& c : joints)
			debugInMenuJoint(c);
		ImGui::TreePop();
	}
}

void CModulePhysics::debugActor(const char* title, PxRigidActor* actor) {
	if (actor == nullptr) {
		ImGui::Text("%s NULL", title);
		return;
	}
	ImGui::Text("%s", title); ImGui::SameLine();
	CHandle h_collider;
	h_collider.fromVoidPtr(actor->userData);
	CEntity* e = h_collider.getOwner();
	if (e)
		e->debugInMenu();
	else
		ImGui::Text("NULL");
}

void CModulePhysics::TJoint::create() {
	PxRigidActor* actor0 = nullptr;
	PxRigidActor* actor1 = nullptr;

	TCompCollider* c0 = obj0.h_collider;
	if (!c0) {
		CEntity* e = getEntityByName(obj0.name);
		if (e)
			obj0.h_collider = e->get<TCompCollider>();
		c0 = obj0.h_collider;
	}
	if (c0)
		actor0 = c0->actor;

	TCompCollider* c1 = obj1.h_collider;
	if (!c1) {
		CEntity* e = getEntityByName(obj1.name);
		if (e)
			obj1.h_collider = e->get<TCompCollider>();
		c1 = obj1.h_collider;
	}
	if (c1) {
		actor1 = c1->actor;
	}

	PxTransform frame0 = toPxTransform(obj0.transform);
	PxTransform frame1 = toPxTransform(obj1.transform);
	if (joint_type == "spherical")
		px_joint = PxSphericalJointCreate(*gPhysics, actor0, frame0, actor1, frame1);
	else if (joint_type == "fixed")
		px_joint = PxFixedJointCreate(*gPhysics, actor0, frame0, actor1, frame1);
	else if (joint_type == "distance")
		px_joint = PxDistanceJointCreate(*gPhysics, actor0, frame0, actor1, frame1);
	else if (joint_type == "revolute")
		px_joint = PxRevoluteJointCreate(*gPhysics, actor0, frame0, actor1, frame1);
	else
		fatal("Invalid joint type %s\n", joint_type.c_str());
}

void CModulePhysics::debugInMenuJointObj(TJoint& j, physx::PxJointActorIndex::Enum idx) {
	std::string title = (idx == PxJointActorIndex::eACTOR0) ? "Obj0 " : "Obj1 ";
	TJoint::TObj& obj = (idx == PxJointActorIndex::eACTOR0) ? j.obj0 : j.obj1;
	title += obj.name;
	if (ImGui::TreeNode(title.c_str())) {
		// Ask physx
		PxTransform pxtrans = j.px_joint->getLocalPose(idx);
		// Save it locally
		obj.transform = toTransform(pxtrans);

		// Update phyxs trans if changed from imgui
		if (obj.transform.renderInMenu()) {
			pxtrans = toPxTransform(obj.transform);
			j.px_joint->setLocalPose(idx, pxtrans);
		}
		ImGui::TreePop();
	}
}

void CModulePhysics::debugInMenuJoint(TJoint& j) {
	if (!j.px_joint) {
		ImGui::LabelText("Joint Type", "Invalid Joint");
		return;
	}

	auto flags = j.px_joint->getConstraintFlags();
	if (flags.isSet(PxConstraintFlag::eBROKEN))
		ImGui::Text("Is Broken!");

	ImGui::LabelText("Joint Type", "%s", j.px_joint->getConcreteTypeName());
	debugInMenuJointObj(j, PxJointActorIndex::eACTOR0);
	debugInMenuJointObj(j, PxJointActorIndex::eACTOR1);
	if (j.joint_type == "spherical") {
		PxSphericalJoint* custom = (PxSphericalJoint*)j.px_joint;
		//spherical->setLimitCone();
	}
	else if (j.joint_type == "fixed") {
		PxFixedJoint* custom = (PxFixedJoint*)j.px_joint;
	}
	else if (j.joint_type == "distance")
	{
		PxDistanceJoint* custom = (PxDistanceJoint*)j.px_joint;
		float distance = custom->getDistance();
		ImGui::LabelText("Distance", "%f", distance);
		// ..
	}
	if (ImGui::SmallButton("Break"))
		j.px_joint->setBreakForce(0.0f, 0.0f);
	ImGui::Separator();
}
