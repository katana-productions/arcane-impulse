#pragma once

#include "mcv_platform.h"
#include "input/button.h"
#include "input/enums.h"

namespace Input
{
  struct TGamepadData
  {
    float buttons[BT_GAMEPAD_COUNT];
    bool connected = false;
  };

  class CGamepad
  {
  public:
    void update(const TGamepadData& data, float delta)
    {
      PROFILE_FUNCTION("CGamepad");
      _connected = data.connected;
      for (int i = 0; i < BT_GAMEPAD_COUNT; ++i)
      {
        _buttons[i].update(data.buttons[i], delta);
      }
    }

    TButton _buttons[BT_GAMEPAD_COUNT];
    bool _connected = false;
  };
}
