#pragma once

#include "mcv_platform.h"
#include "input/button.h"
#include "input/enums.h"
#include "windows/app.h"

namespace Input
{
  struct TMouseData
  {
    bool buttons[BT_MOUSE_COUNT];
    VEC2 position;
    VEC2 delta;
    float wheelDelta = 0.f;
  };

  class CMouse
  {
  public:
    void update(const TMouseData& data, float delta)
    {
      PROFILE_FUNCTION("CMouse");
      _prevPosition = _currPosition;
      _currPosition = data.position;
      _deltaPosition = data.delta;
      for (int i = 0; i < BT_MOUSE_COUNT; ++i)
      {
        float newValue = data.buttons[i] ? 1.f : 0.f;
        _buttons[i].update(newValue, delta);
      }
      _wheelDelta = data.wheelDelta;

	  //If application is focused && cursor toggle is on
	  if (CApplication::get().getFocus() && !CApplication::get().getCursorVisible()){
		  VEC2 windowCenter = CApplication::get().getWindowSize() * 0.5f;
		  POINT p{ LONG(windowCenter.x), LONG(windowCenter.y)};
		  ClientToScreen(CApplication::get().getHandle(),&p);

		  SetCursorPos(p.x,p.y);
		  _currPosition = windowCenter;
	  }

    }

    VEC2 getDelta() const
    {
      return _deltaPosition;
    }

    TButton _buttons[BT_MOUSE_COUNT];
    VEC2 _currPosition;
    VEC2 _prevPosition;
    VEC2 _deltaPosition;
    float _wheelDelta = 0.f;
  };
}
