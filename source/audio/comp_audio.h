#pragma once
#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "components/common/comp_transform.h"
#include "audio\SoundEvent.h"

class TCompAudio : public TCompBase {
  DECL_SIBLING_ACCESS();

private:
	std::string name = "";
	std::string sound = "";
	std::string particles = "data/particles/prefabs_particles/defaultObjDust.json";
	bool isContact = false;
	bool newLevel = true;
	bool isBreakable = true;
	float timelastParticle = 0.0f;
	int state = 0;

	CHandle lastMsg;

	void onContactMessage(const TMsgOnContact& msg);
	void onEntityCreated(const TMsgEntityCreated& msg);
	void onDestroy(const TMsgDestroy& msg);
	void onTimer(const TMsgAlarmClock& msg);
	void onState(const TMsgIntObjState& msg);

	CSoundEvent event;

public:
	~TCompAudio();

	void load(const json& j, TEntityParseContext& ctx);
	void update(float dt);
	void debugInMenu();

	static void registerMsgs();
};
