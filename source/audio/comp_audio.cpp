#include "mcv_platform.h"
#include "utils/utils.h"
#include "physics/module_physics.h"
#include "engine.h"
#include "entity/entity_parser.h"
#include "comp_audio.h"
#include "components\common\comp_transform.h"
#include "audio/SoundEvent.h"
#include "components/player/comp_player_magic.h"

using namespace physx;

DECL_OBJ_MANAGER("comp_audio", TCompAudio);

TCompAudio::~TCompAudio() {
	event.Stop();
}

void TCompAudio::load(const json & j, TEntityParseContext & ctx)
{
	name = j.value("name", name);
	sound = j.value("sound", sound);
	isContact = j.value("isContact", isContact);
	isBreakable = j.value("isBreakable", isBreakable);
	particles = j.value("particles", particles);
}

void TCompAudio::registerMsgs() {
	DECL_MSG(TCompAudio, TMsgOnContact, onContactMessage);
	DECL_MSG(TCompAudio, TMsgEntityCreated, onEntityCreated);
	DECL_MSG(TCompAudio, TMsgDestroy, onDestroy);
  DECL_MSG(TCompAudio, TMsgIntObjState, onState);
}

void TCompAudio::onContactMessage(const TMsgOnContact& msg) {
	TCompCollider* collider = msg.source;
	TCompCollider* colliderParent = get<TCompCollider>();
	if (CHandle(this) != msg.source && collider != nullptr && colliderParent != nullptr && isContact) {
		CEntity* player = getEntityByName("Player");
		TCompPlayerMagic* magic = player->get<TCompPlayerMagic>();

		TCompTransform* transform = get<TCompTransform>();
		assert(transform);
		TCompCollider* collider = get<TCompCollider>();
		assert(collider);
		PxRigidDynamic* rb = collider->actor->is<PxRigidDynamic>();
		assert(rb);

		if (PXVEC3_TO_VEC3(rb->getLinearVelocity()).Length() > 3) {

			float vol = clampFloat(PXVEC3_TO_VEC3(rb->getLinearVelocity()).Length() / magic->getPushForce(), 0.3f, 1.0f);

			Studio::EventInstance* event = EngineAudio.createEvent(sound);			
			event->setVolume(vol);
			EngineAudio.playEvent3D(sound, get<TCompTransform>(), 2, event);

			if (!isBreakable && PXVEC3_TO_VEC3(rb->getLinearVelocity()).Length() > 6 && Time.current - timelastParticle > 0.15f && state != ObjState::Pulled) {
				timelastParticle = Time.current;
				EngineParticles.CreateOneShootSystemParticles(particles.c_str(), transform);
			}
		}

	}
}

void TCompAudio::onEntityCreated(const TMsgEntityCreated& msg) {
	TCompTransform* transform = get<TCompTransform>();
	assert(transform);

	if (name == "fire") {
		event = EngineAudio.playEvent3D(sound, transform, 0);
	}

	if (name == "green_fire" || name == "fire_small") {
		Studio::EventInstance* event = EngineAudio.createEvent(sound);
		event->setVolume(0.3);
		EngineAudio.playEvent3D(sound, transform, 0, event);
	}
	
	if (name == "fire_torch") {
		Studio::EventInstance* event = EngineAudio.createEvent(sound);
		event->setVolume(0.5);
		EngineAudio.playEvent3D(sound, transform, 0, event);
	}

	if (name == "fireball") {
		event = EngineAudio.playEvent3D(sound, transform, 2);
	}

	/*if (name == "blue_orb" || name == "red_orb") {
		event = EngineAudio.playEvent3D("event:/Props/MagicBall", transform, 2);
	}*/
}

void TCompAudio::onDestroy(const TMsgDestroy& msg) {
	if (name == "fireball") event.Stop(true);
}

void TCompAudio::update(float dt) {
	
}

void TCompAudio::onState(const TMsgIntObjState& msg) {
	state = msg.state;
}

void TCompAudio::onTimer(const TMsgAlarmClock& msg) {
}

void TCompAudio::debugInMenu()
{

}
