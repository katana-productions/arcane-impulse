#pragma once

class CSoundEvent {

private:
	class CModuleAudio* audioModule;
	int id;

	bool* paused = false;
	//float* volume = nullptr;
	//float* pitch = nullptr;

protected:
	friend class CModuleAudio;
	friend class TCompAudio;
	CSoundEvent(class CModuleAudio*, int);


public:
	CSoundEvent();

	bool isValid();
	void Restart();
	void Stop(bool fadeOut = true);

	//3D
	bool is3D();
	void set3DAttributes(VEC3, VEC3, VEC3);

	//Setters
	void setPaused(bool);
	void setVolume(float);
	void setPitch(float);
	void setParameter(const char* name, float value);

	//Getters
	bool getPaused() const;
	float getVolume() const;
	float getPitch() const;
	float getParameter(const char* name);
	int getID() { return id; }
	

};