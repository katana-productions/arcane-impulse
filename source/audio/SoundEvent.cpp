#include "mcv_platform.h"
#include "engine.h"
#include "audio/SoundEvent.h"


CSoundEvent::CSoundEvent() {
}

CSoundEvent::CSoundEvent(class CModuleAudio* mAudio, int ID) {
	audioModule = mAudio;
	id = ID;
}

bool CSoundEvent::isValid() {
	auto event = audioModule ? audioModule->getEventInstance(id) : nullptr;

	if (event) return true;
	else return false;
}

void CSoundEvent::Stop(bool fadeout) {
	auto event = audioModule ? audioModule->getEventInstance(id) : nullptr;

	if (event) {
		if (fadeout) event->stop(FMOD_STUDIO_STOP_ALLOWFADEOUT);
		else event->stop(FMOD_STUDIO_STOP_IMMEDIATE);
	}
	event->release();
}

void CSoundEvent::Restart() {
	auto event = audioModule ? audioModule->getEventInstance(id) : nullptr;

	if (event) event->start();
}

void CSoundEvent::set3DAttributes(VEC3 pos, VEC3 forward, VEC3 up) {
	auto event = audioModule ? audioModule->getEventInstance(id) : nullptr;

	FMOD_3D_ATTRIBUTES attributes;
	attributes.position = EngineAudio.Vec3toFmodVect(pos);
	attributes.forward = EngineAudio.Vec3toFmodVect(forward);
	attributes.up = EngineAudio.Vec3toFmodVect(up);
	attributes.velocity = { 0, 0, 0 };

	event->set3DAttributes(&attributes);
}

bool CSoundEvent::is3D() {
	auto event = audioModule ? audioModule->getEventInstance(id) : nullptr;

	bool isAudio3D = false;

	if (event) {
		Studio::EventDescription* eventDescription = nullptr;
		event->getDescription(&eventDescription);
		if (eventDescription) eventDescription->is3D(&isAudio3D);
	}

	return isAudio3D;
}

//Setters
void CSoundEvent::setPaused(bool pause) {
	auto event = audioModule ? audioModule->getEventInstance(id) : nullptr;

	if (event) event->setPaused(pause);
}

void CSoundEvent::setVolume(float volume) {
	auto event = audioModule ? audioModule->getEventInstance(id) : nullptr;

	if (event) event->setVolume(volume);
}

void CSoundEvent::setPitch(float pitch) {
	auto event = audioModule ? audioModule->getEventInstance(id) : nullptr;

	if (event) event->setPitch(pitch);
}

void CSoundEvent::setParameter(const char* name, float value) {
	auto event = audioModule ? audioModule->getEventInstance(id) : nullptr;

	if (event) {
		event->setParameterByName(name, value);
	}

}



//Getters
bool CSoundEvent::getPaused() const {
	auto event = audioModule ? audioModule->getEventInstance(id) : nullptr;

	if (event) event->getPaused(paused);
	return paused;
}

float CSoundEvent::getVolume() const {
	auto event = audioModule ? audioModule->getEventInstance(id) : nullptr;
	float vol;

	if (event) event->getVolume(&vol);
	return vol;
}

float CSoundEvent::getPitch() const {
	auto event = audioModule ? audioModule->getEventInstance(id) : nullptr;
	float p;

	if (event) event->getPitch(&p);
	return p;
}

float CSoundEvent::getParameter(const char* name) {
	auto event = audioModule ? audioModule->getEventInstance(id) : nullptr;
	float aux;

	if (event) aux = event->getParameterByName(name, &aux);
	return aux;
}



