#include "mcv_platform.h"
#include "editor_particles.h"
#include "geometry/transform.h"
#include "components/common/comp_buffers.h"
#include "render/compute/gpu_buffer.h"
#include "engine.h"
#include <fstream>
#include <iomanip>
#include <experimental/filesystem>
#include <filesystem>



DECL_OBJ_MANAGER("editor_particles", TCompEditorParticles);

void TCompEditorParticles::debugInMenu() {

}

void TCompEditorParticles::load(const json& j, TEntityParseContext& ctx) {
	curveInterpolator.push_back(std::pair("Linear", &interLinear));

	curveInterpolator.push_back(std::pair("QuadIn", &interQuadIn));
	curveInterpolator.push_back(std::pair("QuadOut", &interQuadOut));
	curveInterpolator.push_back(std::pair("QuadInOut", &interQuadInOut));

	curveInterpolator.push_back(std::pair("CubicIn", &interCubicIn));
	curveInterpolator.push_back(std::pair("CubicOut", &interCubicOut));
	curveInterpolator.push_back(std::pair("CubicInOut", &interCubicInOut));

	curveInterpolator.push_back(std::pair("QuartIn", &interQuartIn));
	curveInterpolator.push_back(std::pair("QuartOut", &interQuartOut));
	curveInterpolator.push_back(std::pair("QuartInOut", &interQuartInOut));

	curveInterpolator.push_back(std::pair("QuintIn", &interQuintIn));
	curveInterpolator.push_back(std::pair("QuintOut", &interQuintOut));
	curveInterpolator.push_back(std::pair("QuintInOut", &interQuintInOut));

	curveInterpolator.push_back(std::pair("BackIn", &interBackIn));
	curveInterpolator.push_back(std::pair("BackOut", &interBackOut));
	curveInterpolator.push_back(std::pair("BackInOut", &interBackInOut));


	curveInterpolator.push_back(std::pair("ElasticIn", &interElasticIn));
	curveInterpolator.push_back(std::pair("ElasticOut", &interElasticOut));
	curveInterpolator.push_back(std::pair("ElasticInOut", &interElasticInOut));

	curveInterpolator.push_back(std::pair("BounceIn", &interBounceIn));
	curveInterpolator.push_back(std::pair("BounceOut", &interBounceOut));
	curveInterpolator.push_back(std::pair("BounceInOut", &interBounceInOut));


	curveInterpolator.push_back(std::pair("CircularIn", &interCircularIn));
	curveInterpolator.push_back(std::pair("CircularOut", &interCircularOut));
	curveInterpolator.push_back(std::pair("CircularInOut", &interCircularInOut));

	curveInterpolator.push_back(std::pair("ExpoIn", &interExpoIn));
	curveInterpolator.push_back(std::pair("ExpoOut", &interExpoOut));
	curveInterpolator.push_back(std::pair("ExpoInOut", &interExpoInOut));

	curveInterpolator.push_back(std::pair("SineIn", &interSineIn));
	curveInterpolator.push_back(std::pair("SineOut", &interSineOut));
	curveInterpolator.push_back(std::pair("SineInOut", &interSineInOut));


}

void TCompEditorParticles::registerMsgs() {
	DECL_MSG(TCompEditorParticles, TMsgParticleEditorInfo, onSystemCreated);
	DECL_MSG(TCompEditorParticles, TMsgAlarmClock, onAlarmRecieve);
}

void TCompEditorParticles::onAlarmRecieve(const TMsgAlarmClock& msg) {

	if (msg.id == 1) {
		LoadSystem("data/particles/prefabs_particles/temp/temp.json");
	}
}

void TCompEditorParticles::onSystemCreated(const TMsgParticleEditorInfo& msg) {
	jLastSystem.push_back(msg.jsonTCtesParticles);
}

void TCompEditorParticles::update(float delta) {

	ImGuiStyle * style = &ImGui::GetStyle();

	// Main body starts here.
	if (!ImGui::Begin("Editor Particles"))
	{
		// Early out if the window is collapsed, as an optimization.
		ImGui::End();
		return;
	}

	ImGui::Separator();
	ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.9f, 0.6f, 0.9f, 0.8f));
	if (ImGui::SmallButton("New System Particles")) {
		InstanciateSystemParticles();
	}	
	ImGui::PopStyleColor();

	ImGui::Separator();

	if(ImGui::TreeNode("Load System")) {

		std::string file = ShowFiles("data/particles/prefabs_particles/");
		if (file.size()>0)
			LoadSystem(file.c_str());
		ImGui::TreePop();
	}

	if (vSystems_particles.size() > 1) {
		static char strNameFile[128] = "Name Save File";
		ImGui::InputText("NameSaveFile", strNameFile, IM_ARRAYSIZE(strNameFile));
		ImGui::SameLine();
		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.1f, 0.90f, 0.3f, 0.8f));
		if (ImGui::Button("SaveAllSystems"))
			SaveAllSystems(strNameFile);
		ImGui::PopStyleColor();
		ImGui::Separator();
	}

	ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.4f, 0.80f, 0.1f, 0.8f));
	if (ImGui::Button("Reset Timers"))
		ResetTimers();
	ImGui::PopStyleColor();

	int numSystem = 0;
	bool changed;
	for (auto& struct_system : vSystems_particles) {

		CHandle h_system = struct_system.system_particles;
		ImGui::Separator();
		CEntity* e_particles = h_system;
		if (!e_particles) continue;
		TCompBuffers* c_buffer = e_particles->get<TCompBuffers>();

		auto cte_buffer = c_buffer->getCteByName("TCtesParticles");

		TCtesParticles* particles_buffer = dynamic_cast<TCtesParticles*>(cte_buffer);


		if (ImGui::TreeNode(e_particles->getName())) {

			ImGui::SameLine();
			ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(1.0f, 0.00f, 0.0f, 1.00f));
			if (ImGui::SmallButton("Del")) {
				changed |= true;
				particles_buffer->emitter_looping = false;
				vSystems_particles.erase(vSystems_particles.begin() + numSystem);
				ImGui::PopStyleColor();
				ImGui::TreePop();

				cte_buffer->updateGPU();
				break;
			}
			ImGui::PopStyleColor();

			ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.1f, 0.90f, 0.3f, 0.8f));
			if (ImGui::SmallButton("Save System")) {
				SaveSingleSystem(e_particles->getName(), h_system, &struct_system);
			}
			ImGui::PopStyleColor();


			changed |= MenuEditSingleSystem(h_system, &struct_system, c_buffer);

			ImGui::TreePop();
		}

		numSystem++;
		if (changed) 
			cte_buffer->updateGPU();	
	}
	//End code
	ImGui::End();	
}

void TCompEditorParticles::InstanciateSystemParticles() {
	TCompTransform* c_trans = get<TCompTransform>();
	jLastSystem.clear();
	CHandle h_loaded = EngineBasics.Create("data/particles/prefabs_particles/default_particle.json", c_trans);
	CEntity* e_loaded = h_loaded;

	ChangeName(h_loaded, e_loaded->getName());

	system p;
	p.system_particles = h_loaded;
	p.colorCurve.resize(31);
	p.sizeCurve.resize(31);
	p.speedCurve.resize(31);

	p.yawCurve.resize(31);
	p.pitchCurve.resize(31);
	p.rollCurve.resize(31);

	p.yawCurve2.resize(31);
	p.pitchCurve2.resize(31);
	p.rollCurve2.resize(31);

	LoadRotationValues(&p, jLastSystem[0]);
	LoadCurveValues(&p, jLastSystem[0]);
	LoadMaxNumElems(&p, jLastSystem[0]);

	vSystems_particles.push_back(p);
}

std::string TCompEditorParticles::ShowFiles(const char* path) {
	std::string fileName = "";
	if (std::experimental::filesystem::exists(path) && std::experimental::filesystem::is_directory(path)) {

		std::experimental::filesystem::recursive_directory_iterator iter(path);
		std::experimental::filesystem::recursive_directory_iterator end;

		for (iter; iter != end; iter++) {
			fileName = iter->path().string();
			const char *c = fileName.c_str();
			if (ImGui::SmallButton(c)) {
				return fileName;
			}
		}
		fileName = "";
	}
	return fileName;
}

void TCompEditorParticles::LoadSystem(const char* fileName) {
	TCompTransform* c_trans = get<TCompTransform>();

	if (!CheckIfExists(fileName))
		return;		

	jLastSystem.clear();
	std::vector<CHandle> h_group_loaded = EngineBasics.CreateGroup(fileName, c_trans);

	int numSys = 0;
	for (auto h_loaded : h_group_loaded) {
		if (!h_loaded.isValid())
			return;

		CEntity* e_loaded = h_loaded;
		ChangeName(h_loaded, e_loaded->getName());
		system p;
		p.system_particles = h_loaded;

		p.colorCurve.resize(31);
		p.sizeCurve.resize(31);
		p.speedCurve.resize(31);

		p.yawCurve.resize(31);
		p.pitchCurve.resize(31);
		p.rollCurve.resize(31);

		p.yawCurve2.resize(31);
		p.pitchCurve2.resize(31);
		p.rollCurve2.resize(31);


		LoadRotationValues(&p, jLastSystem[numSys]);
		LoadCurveValues(&p, jLastSystem[numSys]);
		LoadMaxNumElems(&p, jLastSystem[numSys]);

		vSystems_particles.push_back(p);

		//We don't want Offset in the editor
		TCompBuffers* c_buffer = e_loaded->get<TCompBuffers>();
		auto cte_buffer = c_buffer->getCteByName("TCtesParticles");
		TCtesParticles* particles_buffer = dynamic_cast<TCtesParticles*>(cte_buffer);

		particles_buffer->emitter_offset_center = VEC3(0,0,0);
		cte_buffer->updateGPU();

		numSys++;
	}
}

void TCompEditorParticles::LoadRotationValues(system* p, json jSystem) {
	p->random_over_time_type = jSystem.value("random_over_time_type", 0);;

	p->yaw_over_time.resize(MAX_QUATS);
	p->pitch_over_time.resize(MAX_QUATS);
	p->roll_over_time.resize(MAX_QUATS);

	p->yaw_over_time2.resize(MAX_QUATS);
	p->pitch_over_time2.resize(MAX_QUATS);
	p->roll_over_time2.resize(MAX_QUATS);

	p->yaw.x = jSystem.value("yaw1", 0);
	p->pitch.x = jSystem.value("pitch1", 0);
	p->roll.x = jSystem.value("roll1", 0);
	
	p->yaw.y = jSystem.value("yaw2", 0);
	p->pitch.y = jSystem.value("pitch2", 0);
	p->roll.y = jSystem.value("roll2", 0);

	if (jSystem.find("yawOverTimeValues") != jSystem.end() && jSystem.find("rollOverTimeValues") != jSystem.end() && jSystem.find("pitchOverTimeValues") != jSystem.end()) {

		auto& jValuesYaw = jSystem["yawOverTimeValues"];
		auto& jValuesPitch = jSystem["pitchOverTimeValues"];
		auto& jValuesRoll = jSystem["rollOverTimeValues"];

		for (int i = 0; i < MAX_QUATS; i++)
		{
			p->yaw_over_time[i] = jValuesYaw[i][1];
			p->pitch_over_time[i] = jValuesPitch[i][1];
			p->roll_over_time[i] = jValuesRoll[i][1];
		}
	}
	else {
		for (int i = 0; i < MAX_QUATS; i++)
		{
			p->yaw_over_time[i] = 0;
			p->pitch_over_time[i] = 0;
			p->roll_over_time[i] = 0;
		}
	}

	if (jSystem.find("yawOverTimeValues2") != jSystem.end() && jSystem.find("rollOverTimeValues2") != jSystem.end() && jSystem.find("pitchOverTimeValues2") != jSystem.end()) {

		auto& jValuesYaw = jSystem["yawOverTimeValues2"];
		auto& jValuesPitch = jSystem["pitchOverTimeValues2"];
		auto& jValuesRoll = jSystem["rollOverTimeValues2"];

		for (int i = 0; i < MAX_QUATS; i++)
		{
			p->yaw_over_time2[i] = jValuesYaw[i][1];
			p->pitch_over_time2[i] = jValuesPitch[i][1];
			p->roll_over_time2[i] = jValuesRoll[i][1];
		}
	}
	else {
		for (int i = 0; i < MAX_QUATS; i++)
		{
			p->yaw_over_time2[i] = 0;
			p->pitch_over_time2[i] = 0;
			p->roll_over_time2[i] = 0;
		}
	}
}

void TCompEditorParticles::LoadCurveValues(system* p, json jSystem) {
	num_curve_color = jSystem.value("color_curve", -1);
	num_curve_size = jSystem.value("size_curve", -1);
	num_curve_velocity = jSystem.value("speed_curve", -1);

	num_curve_yaw = jSystem.value("yaw_curve", -1);
	num_curve_pitch = jSystem.value("pitch_curve", -1);
	num_curve_roll = jSystem.value("roll_curve", -1);

	num_curve_yaw2 = jSystem.value("yaw_curve2", -1);
	num_curve_pitch2 = jSystem.value("pitch_curve2", -1);
	num_curve_roll2 = jSystem.value("roll_curve2", -1);

	if (num_curve_color > -1) p->colorCurve[num_curve_color] = true;
	else num_curve_color = 0;
	if (num_curve_size > -1) p->sizeCurve[num_curve_size] = true;
	else num_curve_size = 0;
	if (num_curve_velocity > -1) p->speedCurve[num_curve_velocity] = true;
	else num_curve_velocity = 0;

	if (num_curve_yaw > -1) p->yawCurve[num_curve_yaw] = true;
	else num_curve_yaw = 0;
	if (num_curve_pitch > -1) p->pitchCurve[num_curve_pitch] = true;
	else num_curve_pitch = 0;
	if (num_curve_roll > -1) p->rollCurve[num_curve_roll] = true;
	else num_curve_roll = 0;

	if (num_curve_yaw2 > -1) p->yawCurve2[num_curve_yaw2] = true;
	else num_curve_yaw2 = 0;
	if (num_curve_pitch2 > -1) p->pitchCurve2[num_curve_pitch2] = true;
	else num_curve_pitch2 = 0;
	if (num_curve_roll2 > -1) p->rollCurve2[num_curve_roll2] = true;
	else num_curve_roll2 = 0;

	for (auto& jNodeSize : jSystem["node_size"])
	{
		p->nodeSize.push_back(std::pair(jNodeSize[1], jNodeSize[0]));
	}

	for (auto& jNodeColor : jSystem["node_color"])
	{
		p->nodeColor.push_back(std::pair(StringToVec4(jNodeColor[1]), jNodeColor[0]));
	}

	for (auto& jNodeSpeed : jSystem["node_speed"])
	{
		p->nodeSpeed.push_back(std::pair(jNodeSpeed[1], jNodeSpeed[0]));
	}

	for (auto& jNodeYaw : jSystem["node_yaw"])
	{
		p->nodeYaw.push_back(std::pair(jNodeYaw[1], jNodeYaw[0]));
	}

	for (auto& jNodePitch : jSystem["node_pitch"])
	{
		p->nodePitch.push_back(std::pair(jNodePitch[1], jNodePitch[0]));
	}

	for (auto& jNodeRoll : jSystem["node_roll"])
	{
		p->nodeRoll.push_back(std::pair(jNodeRoll[1], jNodeRoll[0]));
	}

	for (auto& jNodeYaw : jSystem["node_yaw2"])
	{
		p->nodeYaw2.push_back(std::pair(jNodeYaw[1], jNodeYaw[0]));
	}

	for (auto& jNodePitch : jSystem["node_pitch2"])
	{
		p->nodePitch2.push_back(std::pair(jNodePitch[1], jNodePitch[0]));
	}

	for (auto& jNodeRoll : jSystem["node_roll2"])
	{
		p->nodeRoll2.push_back(std::pair(jNodeRoll[1], jNodeRoll[0]));
	}
}

void TCompEditorParticles::LoadMaxNumElems(system* struct_system, json jSystem) {
	if(jSystem.find("num_elems") != jSystem.end())
		struct_system->max_num_elems = jSystem["num_elems"];
	else
		struct_system->max_num_elems = 1024;
}


bool TCompEditorParticles::CheckIfExists(const char* name) {
	return std::experimental::filesystem::exists(name);
}

void TCompEditorParticles::ResetTimers() {

	std::experimental::filesystem::create_directory("data/particles/prefabs_particles/temp");
	std::experimental::filesystem::permissions("data/particles/prefabs_particles/temp", std::experimental::filesystem::perms::owner_all);

	char filename[128];
	const char* path = "data/particles/prefabs_particles/temp/";
	const char* name = "temp";
	const char* ext = ".json";

	strcpy(filename, path);
	strcat(filename, name);
	strcat(filename, ext);

	auto jData = loadJson("data/particles/prefabs_particles/default_particle.json");
	auto& jEntry = jData;

	auto& EntireEntities = jEntry;

	for (int i = 0; i < vSystems_particles.size(); i++) {
		EntireEntities[i] = EntireEntities[0];
		auto& jEntityToModify = EntireEntities[i];
		auto& jEntity = jEntityToModify["entity"];

		CHandle h_current = vSystems_particles[i].system_particles;
		CEntity* e_current = h_current;
		jEntity["name"] = e_current->getName();

		SaveNewMaterial(jEntity, h_current);
		SaveNewMesh(jEntity, h_current);
		SaveNewBuffer(jEntity, h_current);

		TCompBuffers* c_buffer = e_current->get<TCompBuffers>();
		auto cte_buffer = c_buffer->getCteByName("TCtesParticles");
		TCtesParticles* particles_buffer = dynamic_cast<TCtesParticles*>(cte_buffer);

		ParseSingleEntity(jEntity, particles_buffer, &vSystems_particles[i]);
	}

	std::ofstream outputFileStream(filename);
	outputFileStream << std::setw(4) << jData << std::endl;

	for (auto& struct_system : vSystems_particles) {

		CHandle h_system = struct_system.system_particles;
		h_system.destroy();
	}
	vSystems_particles.clear();


	EngineAlarms.AddAlarm(1, 1, CHandle(this));
}


bool TCompEditorParticles::MenuEditSingleSystem(CHandle h_system, system* struct_system, TCompBuffers* c_buffer) {
	bool changed = false;

	CEntity* e_particles = h_system;
	auto cte_buffer = c_buffer->getCteByName("TCtesParticles");
	TCtesParticles* particles_buffer = dynamic_cast<TCtesParticles*>(cte_buffer);

	static char str0[128] = "Nombre del Sistema";
	ImGui::InputText("ChangeName", str0, IM_ARRAYSIZE(str0));
	ImGui::SameLine();
	ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(1.0f, 0.70f, 0.2f, 0.8f));
	if (ImGui::SmallButton("Apply"))
		ChangeName(h_system, str0);
	ImGui::PopStyleColor();

	ImGui::Separator();

	if (ImGui::TreeNode("Particle System")) {
		changed |= ImGui::Checkbox("World Space", &particles_buffer->emitter_world_space);
		changed |= ImGui::DragFloat("System LifeTime", &particles_buffer->psystem_total_life_time, 0.01f, 0.1f, 50.f);
		changed |= ImGui::Checkbox("Looping", &particles_buffer->emitter_looping);
		changed |= ImGui::DragFloat("System Delay", &particles_buffer->psystem_delay_time, 0.01f, 0.0f, 50.f);
		changed |= ImGui::DragFloat2("Particle LifeTime", &particles_buffer->emitter_duration.x, 0.01f, 0.1f, 15.f);
		changed |= ImGui::DragFloat2("Start Speed", &particles_buffer->emitter_speed.x, 0.01f, -100.f, 100.f);
		changed |= ImGui::DragFloat2("Start Size", &particles_buffer->emitter_modifier_size_factor.x, 0.01f, 0.1f, 15.f);
		changed |= ImGui::DragFloat2("Gravity Modifier", &particles_buffer->emitter_gravity_factor.x, 0.01f, -10.0f, 10.0f);
		StartRotation(particles_buffer, struct_system);
		changed |= ImGui::DragInt("Max Particles", &struct_system->max_num_elems, 1, 0, 2048);
		ImGui::SameLine();
		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(1.0f, 0.70f, 0.2f, 0.8f));
		if (ImGui::SmallButton("Apply Max. Particles"))
			ApplyMaxParticles();
		ImGui::PopStyleColor();
		ImGui::TreePop();
	}
	ImGui::Separator();
	if (ImGui::TreeNode("Emission")) {
		changed |= ImGui::DragFloat2("Time Between Spawn", &particles_buffer->emitter_time_between_spawns.x, 0.01f, 0.01f, 100.0f);
		changed |= ImGui::DragFloat2("Num Particles Per Spawn", &particles_buffer->emitter_num_particles_per_spawn.x, 0.1f, 1, 100);
		changed |= ImGui::DragFloat3("Center", &particles_buffer->emitter_center.x, 0.01f, -15.f, 15.f);

		ImGui::TreePop();
	}
	ImGui::Separator();

	if (ImGui::TreeNode("Shape")) {

		static const char* render_output_str =
			"Cone\0"
			"Sphere\0"
			"Cube\0"
			"\0";

		changed |= ImGui::Combo("Shape Type", (int*)&particles_buffer->emitter_type, render_output_str);
		if (particles_buffer->emitter_type == eCone) {
			changed |= ImGui::DragFloat("Angle", &particles_buffer->emitter_dir_aperture, 0.01f, -10.0f, 100.0f);
			changed |= ImGui::DragFloat("Radius", &particles_buffer->emitter_center_radius, 0.01f, 0.0f, 100.0f);
			changed |= ImGui::DragFloat("Shell Thickness", &particles_buffer->emitter_shell_thickness, 0.01f, 0.0f, 1.0f);
			changed |= ImGui::DragFloat3("Dir", &particles_buffer->emitter_dir.x, 0.01f, -1.f, 1.f);
		}
		else if (particles_buffer->emitter_type == eSphere) {
			changed |= ImGui::DragFloat("Radius", &particles_buffer->emitter_center_radius, 0.01f, 0.0f, 100.0f);
			changed |= ImGui::DragFloat("Shell Thickness", &particles_buffer->emitter_shell_thickness, 0.01f, 0.0f, 1.0f);
		}
		else if (particles_buffer->emitter_type == eCube) {
			changed |= ImGui::DragFloat3("Cube Half Size", &particles_buffer->emitter_cube_half_size.x, 0.01f, 0.f, 100.f);
			changed |= ImGui::DragFloat("Shell Thickness", &particles_buffer->emitter_shell_thickness, 0.01f, 0.0f, 1.0f);
			changed |= ImGui::DragFloat3("Dir", &particles_buffer->emitter_dir.x, 0.01f, -1.f, 1.f);
			changed |= ImGui::DragFloat("Random Dir Factor", &particles_buffer->emitter_random_dir_factor, 0.01f, 0.0f, 1.0f);
		}
		ImGui::TreePop();
	}
	ImGui::Separator();

	if (ImGui::TreeNode("Noise")) {
		changed |= ImGui::DragFloat2("Noise Strenght", &particles_buffer->emitter_noise_factor.x, 0.01f, 0.0f, 1.0f);
		changed |= ImGui::DragFloat2("Noise Frequency", &particles_buffer->emitter_noise_frequency.x, 0.01f, 0.0f, 60.0f);

		float auxNoiseF1 = particles_buffer->emitter_noise_frequency.x == 0 ? 0 : 1 / particles_buffer->emitter_noise_frequency.x;
		float auxNoiseF2 = particles_buffer->emitter_noise_frequency.y == 0 ? 0 : 1 / particles_buffer->emitter_noise_frequency.y;
		ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(.2f, 0.90f, 0.0f, 1.0f));
		std::string auxInfo = "1:Noise each " + std::to_string(auxNoiseF1) + " seconds.   2:Noise each " + std::to_string(auxNoiseF2) + " seconds.";
		ImGui::Text(auxInfo.c_str());
		ImGui::PopStyleColor();
		ImGui::TreePop();
	}
	ImGui::Separator();

	if (ImGui::TreeNode("Velocity over Lifetime")) {
		for (int i = 0; i < MAX_SPEEDS; ++i) {
			char title[64];
			sprintf(title, "%d", i);
			changed |= ImGui::DragFloat(title, &particles_buffer->psystem_speed_over_time[i].x, 0.01f, 0.0f, 10.0f);
		}

		changed |= SpeedCurves(particles_buffer, struct_system);

		ImGui::TreePop();
	}
	ImGui::Separator();

	if (ImGui::TreeNode("Color over Lifetime")) {
		for (int i = 0; i < MAX_COLORS; ++i) {
			char title[64];
			sprintf(title, "%d", i);
			changed |= ImGui::ColorEdit4(title, &particles_buffer->psystem_colors_over_time[i].x);
		}

		changed |= ColorCurves(particles_buffer, struct_system);

		ImGui::TreePop();
	}
	ImGui::Separator();

	if (ImGui::TreeNode("Size over Lifetime")) {
		for (int i = 0; i < MAX_SIZES; ++i) {
			char title[64];
			sprintf(title, "%d", i);
			changed |= ImGui::DragFloat(title, &particles_buffer->psystem_sizes_over_time[i].x, 0.01f, 0.0f, 10.0f);
		}

		changed |= SizeCurves(particles_buffer, struct_system);

		ImGui::TreePop();
	}
	ImGui::Separator();

	if(ImGui::TreeNode("Rotation over Lifetime")) {

		changed |= ImGui::DragFloat2("Rotation Speed", &particles_buffer->emitter_rotation_speed.x, 0.01f, -10.f, 10.f);

		static const char* random_life_type =
			"Simple Rotation\0"
			"Randow Between Two Rotations\0"
			"Simple Curve\0"
			"Random Between Two Curves\0"
			"\0";

		if (particles_buffer->emitter_billboard) {
			ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(1.0f, 0.80f, 0.0f, 1.0f));
			ImGui::Text("Render/Billboard activated");
			ImGui::PopStyleColor();
		}
		changed |= ImGui::Combo("Random Over Life Type", (int*)&struct_system->random_over_time_type, random_life_type);

		if (struct_system->random_over_time_type == RandomType::eSimpleValue)
			changed |= SimpleRotation(particles_buffer, struct_system);
		else if (struct_system->random_over_time_type == RandomType::eRandom2Values)
			changed |= RandomBetweenTwoSimpleRotations(particles_buffer, struct_system);
		else if (struct_system->random_over_time_type == RandomType::eSimpleCurve)
			changed |= SimpleCurveRotation(particles_buffer, struct_system);
		else if(struct_system->random_over_time_type == RandomType::eRandom2Curves)
			changed |= RandomBetweenTwoCurves(particles_buffer, struct_system);
		
		ImGui::TreePop();
	}

	ImGui::Separator();

	if (ImGui::TreeNode("Rotation Over Direction")) {
		if (particles_buffer->emitter_billboard) {
			ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(1.0f, 0.80f, 0.0f, 1.0f));
			ImGui::Text("Render/Billboard activated");
			ImGui::PopStyleColor();
		}
		ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(1.0f, 0.80f, 0.0f, 1.0f));
		ImGui::Text("Speed must be >0");
		ImGui::PopStyleColor();

		changed |= ImGui::Checkbox("Rotation Over Direction", &particles_buffer->emitter_rotation_over_direction);
		changed |= ImGui::DragFloat3("Forward", &particles_buffer->emitter_forward.x, 0.01f, -1.0f, 1.0f);
		if (changed)
			particles_buffer->emitter_forward.Normalize();

		ImGui::TreePop();
	}
	ImGui::Separator();

	if (ImGui::TreeNode("Render")) {

		changed |= ImGui::Checkbox("Billboard", &particles_buffer->emitter_billboard);

		if (ImGui::TreeNode("Load Mesh")) {
			std::string file = ShowFiles("data/meshes/particles/");
			if (file.size() > 0)
				changed |= ChangeMesh(h_system, file.c_str() , c_buffer);

			ImGui::TreePop();
		}


		if (ImGui::TreeNode("Load Material")) {
			std::string file = ShowFiles("data/materials/particles/");
			if (file.size() > 0)
				changed |= ChangeMaterial(h_system, file.c_str(), c_buffer);

			ImGui::TreePop();
		}

		changed |= ImGui::DragFloat("Particle Glow", &particles_buffer->emitter_glow, 1.0f, 0.0f, 1000.0f);

		ImGui::TreePop();
	}

	ImGui::Separator();

	if (changed) {
		particles_buffer->emitter_dir.Normalize();
	}

	return changed;
}

bool TCompEditorParticles::ChangeMesh(CHandle h_system, const char* newMeshName, TCompBuffers* c_buffer) {
	if (!CheckIfExists(newMeshName))
		return false;

	CEntity* e_system = h_system;
	TCompRender* c_render = e_system->get<TCompRender>();
	c_render->parts.clear();

	auto jData = loadJson("data/particles/prefabs_particles/default_particle.json");

	auto& jEntry = jData;
	auto& EntireEntities = jEntry;
	auto& jEntityToModify = EntireEntities[0];
	auto& jEntity = jEntityToModify["entity"];
	auto& jRender = jEntity["render"];

	if (strcmp(newMeshName, "data\\meshes\\particles\\unit_quad_xy_centered.mesh") == 0) jRender["mesh"] = "unit_quad_xy_centered.mesh";
	else jRender["mesh"] = newMeshName;

	SaveNewMaterial(jEntity, h_system);

	c_render->readMesh(jRender);
	c_render->showMeshesWithState(c_render->curr_state);


	ApplyNewBuffer(jEntity, h_system, c_buffer);
	return true;
}

bool TCompEditorParticles::ChangeMaterial(CHandle h_system, const char* newMatName, TCompBuffers* c_buffer) {

	if (!CheckIfExists(newMatName))
		return false;

	CEntity* e_system = h_system;
	TCompRender* c_render = e_system->get<TCompRender>();
	c_render->parts.clear();

	auto jData = loadJson("data/particles/prefabs_particles/default_particle.json");

	auto& jEntry = jData;
	auto& EntireEntities = jEntry;
	auto& jEntityToModify = EntireEntities[0];
	auto& jEntity = jEntityToModify["entity"];
	auto& jRender = jEntity["render"];

	SaveNewMesh(jEntity, h_system);
	auto& jMaterial = jRender["materials"];
	jMaterial.clear();

	std::string mesh_name = c_render->new_particle_mesh_name;
	const CMesh* mesh = Resources.get(mesh_name)->as<CMesh>();
	auto& g = mesh->getGroups();
	assert(g.size() > 0);

	jMaterial.clear();
	for (int i = 0; i < g.size(); i++) {
		jMaterial.push_back(newMatName);
	}

	c_render->readMesh(jRender);
	c_render->showMeshesWithState(c_render->curr_state);

	ApplyNewBuffer(jEntity, h_system, c_buffer);

	return true;
}

void TCompEditorParticles::ChangeName(CHandle h, const char* newName) {
	CEntity* e_loaded = h;
	TCompName *e_name = e_loaded->get<TCompName>();

	std::string name = (std::string) newName;

	int numeration = 0;
	for (auto system : vSystems_particles) {

		CHandle h_system = system.system_particles;
		CEntity* e_systems = h_system;
		std::string system_name = (std::string)(e_systems->getName());
		if (name == system_name) { 
			numeration++;
			name = (std::string) (name + "_" + std::to_string(numeration));
		}
	}
	e_name->setName(name.c_str());
}

void TCompEditorParticles::SaveAllSystems(const char* name) {

	char filename[128];
	const char* path = "data/particles/prefabs_particles/";
	const char* ext = ".json";

	strcpy(filename, path);
	strcat(filename, name);
	strcat(filename, ext);

	auto jData = loadJson("data/particles/prefabs_particles/default_particle.json");
	auto& jEntry = jData;

	auto& EntireEntities = jEntry;

	for (int i = 0; i < vSystems_particles.size(); i++) {
		EntireEntities[i] = EntireEntities[0];
		auto& jEntityToModify = EntireEntities[i];
		auto& jEntity = jEntityToModify["entity"];

		CHandle h_current = vSystems_particles[i].system_particles;
		CEntity* e_current = h_current;
		jEntity["name"] = e_current->getName();

		SaveNewMaterial(jEntity, h_current);
		SaveNewMesh(jEntity, h_current);
		SaveNewBuffer(jEntity, h_current);

		TCompBuffers* c_buffer = e_current->get<TCompBuffers>();
		auto cte_buffer = c_buffer->getCteByName("TCtesParticles");
		TCtesParticles* particles_buffer = dynamic_cast<TCtesParticles*>(cte_buffer);

		ParseSingleEntity(jEntity, particles_buffer, &vSystems_particles[i]);
	}

	//Check if Filename Exists
	bool nameExist = true;
	int numeration = 0;
	const char* sep = "_";
	while (nameExist) {
		if (CheckIfExists(filename)) {
				numeration++;
				std::string aux = std::to_string(numeration);
				char const* num = aux.c_str();

				strcpy(filename, path);
				strcat(filename, name);
				strcat(filename, sep);
				strcat(filename, num);
				strcat(filename, ext);
		}
		else {
			nameExist = false;
		}
	}

	std::ofstream outputFileStream(filename);
	outputFileStream << std::setw(4) << jData << std::endl;
}

void TCompEditorParticles::SaveSingleSystem(const char* name, CHandle h_system, system* struct_system){

	CEntity* e_particles = h_system;
	TCompBuffers* c_buffer = e_particles->get<TCompBuffers>();
	auto cte_buffer = c_buffer->getCteByName("TCtesParticles");
	TCtesParticles* particles_buffer = dynamic_cast<TCtesParticles*>(cte_buffer);


	auto jData = loadJson("data/particles/prefabs_particles/default_particle.json");
	for (auto& jEntry : jData){
		auto& jEntity = jEntry["entity"];

		jEntity["name"] = name;		
		SaveNewMesh(jEntity, h_system);
		SaveNewMaterial(jEntity, h_system);
		SaveNewBuffer(jEntity, h_system);

		ParseSingleEntity(jEntity, particles_buffer, struct_system);
	}

	char filename[128];
	const char* path = "data/particles/prefabs_particles/";
	const char* ext = ".json";

	strcpy(filename, path);
	strcat(filename, name);
	strcat(filename, ext);

	//Check if Filename Exists
	bool nameExist = true;
	int numeration = 0;
	const char* sep = "_";
	while (nameExist) {
		if (CheckIfExists(filename)) {
			numeration++;
			std::string aux = std::to_string(numeration);
			char const* num = aux.c_str();

			strcpy(filename, path);
			strcat(filename, name);
			strcat(filename, sep);
			strcat(filename, num);
			strcat(filename, ext);
		}
		else {
			nameExist = false;
		}
	}

	std::ofstream outputFileStream(filename);
	outputFileStream << std::setw(4) << jData << std::endl;
}

void TCompEditorParticles::SaveNewMaterial(json& jEntity, CHandle h_system) {
	CEntity* e_particles = h_system;
	TCompRender* e_render = e_particles->get<TCompRender>();

	auto& jRender = jEntity["render"];
	auto& jMaterial = jRender["materials"];

	std::string mesh_name = e_render->new_particle_mesh_name;
	const CMesh* mesh = Resources.get(mesh_name)->as<CMesh>();
	auto& g = mesh->getGroups();
	assert(g.size() > 0);

	jMaterial.clear();
	for (int i = 0; i < g.size(); i++) {
		jMaterial.push_back(e_render->new_particle_material_name);
	}
}

void TCompEditorParticles::SaveNewMesh(json& jEntity, CHandle h_system) {
	CEntity* e_particles = h_system;
	TCompRender* e_render = e_particles->get<TCompRender>();

	auto& jRender = jEntity["render"];
	jRender["mesh"] = e_render->new_particle_mesh_name;
}

void TCompEditorParticles::ApplyNewBuffer(json& jBuffer, CHandle h_system, TCompBuffers* c_buffer) {
	CEntity* e_particles = h_system;
	TCompRender* e_render = e_particles->get<TCompRender>();

	for (auto gpu_buffer : c_buffer->gpu_buffers) {
		if (gpu_buffer->name == "indirect_draw") {
			std::string mesh_name = e_render->new_particle_mesh_name;
			const CMesh* mesh = Resources.get(mesh_name)->as<CMesh>();
			auto& g = mesh->getGroups();
			assert(g.size() > 0);
			uint32_t args[5] = { g[0].num_indices, 0, g[0].first_idx, 0, 0 };
			gpu_buffer->copyCPUtoGPUFrom(args);
		}
	}
}

void TCompEditorParticles::ApplyMaxParticles() {
	ResetTimers();
}


void TCompEditorParticles::SaveNewBuffer(json& jEntity, CHandle h_system) {
	CEntity* e_particles = h_system;
	TCompRender* e_render = e_particles->get<TCompRender>();

	auto& jBuffer = jEntity["buffers"];
	auto& jIDraw = jBuffer["indirect_draw"];
	jIDraw["init_indirect_from_mesh"] = e_render->new_particle_mesh_name;	
}

void TCompEditorParticles::ParseSingleEntity(json& jEntity, TCtesParticles* particles_buffer, system* struct_system) {

	auto& jBuffer = jEntity["buffers"];

	auto& jInstances = jBuffer["instances"];
	auto& jInstancesActive = jBuffer["instances_active"];

	jInstances["num_elems"] = struct_system->max_num_elems;
	jInstancesActive["num_elems"] = struct_system->max_num_elems;

	auto& jParticles = jBuffer["TCtesParticles"];
	jParticles["looping"] = particles_buffer->emitter_looping;
	if (particles_buffer->emitter_type == eCone)
		jParticles["type"] = "cone";
	else if (particles_buffer->emitter_type == eSphere)
		jParticles["type"] = "sphere";
	else if (particles_buffer->emitter_type == eCube)
		jParticles["type"] = "cube";

	jParticles["num_particles_per_spawn"] = std::to_string(particles_buffer->emitter_num_particles_per_spawn.x) + " " + std::to_string(particles_buffer->emitter_num_particles_per_spawn.y);
	jParticles["center"] = std::to_string(particles_buffer->emitter_center.x) + " " + std::to_string(particles_buffer->emitter_center.y) + " " + std::to_string(particles_buffer->emitter_center.z);
	jParticles["dir"] = std::to_string(particles_buffer->emitter_dir.x) + " " + std::to_string(particles_buffer->emitter_dir.y) + " " + std::to_string(particles_buffer->emitter_dir.z);
	jParticles["dir_aperture"] = particles_buffer->emitter_dir_aperture;
	jParticles["center_radius"] = particles_buffer->emitter_center_radius;
	jParticles["shell_thickness"] = particles_buffer->emitter_shell_thickness;
	jParticles["speed"] = std::to_string(particles_buffer->emitter_speed.x) + " " + std::to_string(particles_buffer->emitter_speed.y);
	jParticles["duration"] = std::to_string(particles_buffer->emitter_duration.x) + " " + std::to_string(particles_buffer->emitter_duration.y);
	jParticles["system_total_life_time"] = particles_buffer->psystem_total_life_time;
	jParticles["system_delay_time"] = particles_buffer->psystem_delay_time;
	jParticles["time_between_spawns"] = std::to_string(particles_buffer->emitter_time_between_spawns.x) + " " + std::to_string(particles_buffer->emitter_time_between_spawns.y);
	jParticles["gravity_factor"] = std::to_string(particles_buffer->emitter_gravity_factor.x) + " " + std::to_string(particles_buffer->emitter_gravity_factor.y);
	jParticles["modifier_size_factor"] = std::to_string(particles_buffer->emitter_modifier_size_factor.x) + " " + std::to_string(particles_buffer->emitter_modifier_size_factor.y);
	jParticles["cube_half_size"] = std::to_string(particles_buffer->emitter_cube_half_size.x) + " " + std::to_string(particles_buffer->emitter_cube_half_size.y) + " " + std::to_string(particles_buffer->emitter_cube_half_size.z);
	jParticles["random_dir_factor"] = particles_buffer->emitter_random_dir_factor;
	jParticles["noise_factor"] = std::to_string(particles_buffer->emitter_noise_factor.x) + " " + std::to_string(particles_buffer->emitter_noise_factor.y);
	jParticles["noise_frequency"] = std::to_string(particles_buffer->emitter_noise_frequency.x) + " " + std::to_string(particles_buffer->emitter_noise_frequency.y);
	jParticles["billboard"] = particles_buffer->emitter_billboard;
	jParticles["rotation_over_direction"] = particles_buffer->emitter_rotation_over_direction;
	jParticles["forward"] = std::to_string(particles_buffer->emitter_forward.x) + " " + std::to_string(particles_buffer->emitter_forward.y) + " " + std::to_string(particles_buffer->emitter_forward.z);
	jParticles["rotation_speed"] = std::to_string(particles_buffer->emitter_rotation_speed.x) + " " + std::to_string(particles_buffer->emitter_rotation_speed.y);

	jParticles["glow"] = particles_buffer->emitter_glow;
	jParticles["world_space"] = particles_buffer->emitter_world_space;
	jParticles["num_elems"] = struct_system->max_num_elems;

	jParticles["yaw1"] = struct_system->yaw.x;
	jParticles["pitch1"] = struct_system->pitch.x;
	jParticles["roll1"] = struct_system->roll.x;

	jParticles["yaw2"] = struct_system->yaw.y;
	jParticles["pitch2"] = struct_system->pitch.y;
	jParticles["roll2"] = struct_system->roll.y;
	
	auto& jColor = jParticles["colors"];
	jColor.clear();
	for (int timeStep = 0; timeStep < MAX_COLORS; timeStep++) {
		json color;

		color[0] = (1. / 7.) * timeStep;
		color[1] = std::to_string(particles_buffer->psystem_colors_over_time[timeStep].x) + " " + std::to_string(particles_buffer->psystem_colors_over_time[timeStep].y) + " " + std::to_string(particles_buffer->psystem_colors_over_time[timeStep].z) + " " + std::to_string(particles_buffer->psystem_colors_over_time[timeStep].w);

		jColor.push_back(color);
	}

	auto& jNodeColor = jParticles["node_color"];
	jNodeColor.clear();
	for (int j = 0; j < struct_system->nodeColor.size(); j++) {
		json node;

		node[0] = struct_system->nodeColor[j].second; //ratio
		node[1] = std::to_string(struct_system->nodeColor[j].first.x) + " " + std::to_string(struct_system->nodeColor[j].first.y) + " " + std::to_string(struct_system->nodeColor[j].first.z) + " " + std::to_string(struct_system->nodeColor[j].first.w);

		jNodeColor.push_back(node);
	}

	auto& jSize = jParticles["sizes"];
	jSize.clear();
	for (int timeStep = 0; timeStep < MAX_SIZES; timeStep ++ ) {
		json size;

		size[0] = (1. / 7.) * timeStep;
		size[1] = particles_buffer->psystem_sizes_over_time[timeStep].x;

		jSize.push_back(size);
	}

	auto& jNodeSize = jParticles["node_size"];
	jNodeSize.clear();
	for (int j = 0; j < struct_system->nodeSize.size(); j++) {
		json node;

		node[0] = struct_system->nodeSize[j].second; //ratio
		node[1] = struct_system->nodeSize[j].first;

		jNodeSize.push_back(node);
	}

	auto& jSpeed = jParticles["speedOverTimeValues"];
	jSpeed.clear();
	for (int timeStep = 0; timeStep < MAX_SPEEDS; timeStep++) {
		json speed;

		speed[0] = (1. / 7.) * timeStep;
		speed[1] = particles_buffer->psystem_speed_over_time[timeStep].x;

		jSpeed.push_back(speed);
	}

	auto& jNodeSpeed = jParticles["node_speed"];
	jNodeSpeed.clear();
	for (int j = 0; j < struct_system->nodeSpeed.size(); j++) {
		json node;

		node[0] = struct_system->nodeSpeed[j].second; //ratio
		node[1] = struct_system->nodeSpeed[j].first;

		jNodeSpeed.push_back(node);
	}

	auto& jYaw = jParticles["yawOverTimeValues"];
	jYaw.clear();
	for (int timeStep = 0; timeStep < MAX_QUATS; timeStep++) {
		json yaw;

		yaw[0] = (1. / 7.) * timeStep;
		yaw[1] = struct_system->yaw_over_time[timeStep];

		jYaw.push_back(yaw);
	}

	auto& jPitch = jParticles["pitchOverTimeValues"];
	jPitch.clear();
	for (int timeStep = 0; timeStep < MAX_QUATS; timeStep++) {
		json pitch;

		pitch[0] = (1. / 7.) * timeStep;
		pitch[1] = struct_system->pitch_over_time[timeStep];

		jPitch.push_back(pitch);
	}

	auto& jRoll = jParticles["rollOverTimeValues"];
	jRoll.clear();
	for (int timeStep = 0; timeStep < MAX_QUATS; timeStep++) {
		json roll;

		roll[0] = (1. / 7.) * timeStep;
		roll[1] = struct_system->roll_over_time[timeStep];

		jRoll.push_back(roll);
	}

	auto& jYaw2 = jParticles["yawOverTimeValues2"];
	jYaw2.clear();
	for (int timeStep = 0; timeStep < MAX_QUATS; timeStep++) {
		json yaw;

		yaw[0] = (1. / 7.) * timeStep;
		yaw[1] = struct_system->yaw_over_time2[timeStep];

		jYaw2.push_back(yaw);
	}

	auto& jPitch2 = jParticles["pitchOverTimeValues2"];
	jPitch2.clear();
	for (int timeStep = 0; timeStep < MAX_QUATS; timeStep++) {
		json pitch;

		pitch[0] = (1. / 7.) * timeStep;
		pitch[1] = struct_system->pitch_over_time2[timeStep];

		jPitch2.push_back(pitch);
	}

	auto& jRoll2 = jParticles["rollOverTimeValues2"];
	jRoll2.clear();
	for (int timeStep = 0; timeStep < MAX_QUATS; timeStep++) {
		json roll;

		roll[0] = (1. / 7.) * timeStep;
		roll[1] = struct_system->roll_over_time2[timeStep];

		jRoll2.push_back(roll);
	}

	auto& jNodeYaw = jParticles["node_yaw"];
	jNodeYaw.clear();
	for (int j = 0; j < struct_system->nodeYaw.size(); j++) {
		json node;

		node[0] = struct_system->nodeYaw[j].second; //ratio
		node[1] = struct_system->nodeYaw[j].first;

		jNodeYaw.push_back(node);
	}

	auto& jNodePitch = jParticles["node_pitch"];
	jNodePitch.clear();
	for (int j = 0; j < struct_system->nodePitch.size(); j++) {
		json node;

		node[0] = struct_system->nodePitch[j].second; //ratio
		node[1] = struct_system->nodePitch[j].first;

		jNodePitch.push_back(node);
	}

	auto& jNodeRoll = jParticles["node_roll"];
	jNodeRoll.clear();
	for (int j = 0; j < struct_system->nodeRoll.size(); j++) {
		json node;

		node[0] = struct_system->nodeRoll[j].second; //ratio
		node[1] = struct_system->nodeRoll[j].first;

		jNodeRoll.push_back(node);
	}

	auto& jNodeYaw2 = jParticles["node_yaw2"];
	jNodeYaw2.clear();
	for (int j = 0; j < struct_system->nodeYaw2.size(); j++) {
		json node;

		node[0] = struct_system->nodeYaw2[j].second; //ratio
		node[1] = struct_system->nodeYaw2[j].first;

		jNodeYaw2.push_back(node);
	}

	auto& jNodePitch2 = jParticles["node_pitch2"];
	jNodePitch2.clear();
	for (int j = 0; j < struct_system->nodePitch2.size(); j++) {
		json node;

		node[0] = struct_system->nodePitch2[j].second; //ratio
		node[1] = struct_system->nodePitch2[j].first;

		jNodePitch2.push_back(node);
	}

	auto& jNodeRoll2 = jParticles["node_roll2"];
	jNodeRoll2.clear();
	for (int j = 0; j < struct_system->nodeRoll2.size(); j++) {
		json node;

		node[0] = struct_system->nodeRoll2[j].second; //ratio
		node[1] = struct_system->nodeRoll2[j].first;

		jNodeRoll2.push_back(node);
	}

	int auxColorCurve = -1;
	int auxSizeCurve = -1;
	int auxSpeedCurve = -1;

	int auxYawCurve = -1;
	int auxPitchCurve = -1;
	int auxRollCurve = -1;

	int auxYawCurve2 = -1;
	int auxPitchCurve2 = -1;
	int auxRollCurve2 = -1;

	for (int j = 0; j < struct_system->colorCurve.size(); j++)
	{
		if (struct_system->colorCurve[j]) auxColorCurve = j;
		if (struct_system->sizeCurve[j]) auxSizeCurve = j;
		if (struct_system->speedCurve[j]) auxSpeedCurve = j;

		if (struct_system->yawCurve[j]) auxYawCurve = j;
		if (struct_system->pitchCurve[j]) auxPitchCurve = j;
		if (struct_system->rollCurve[j]) auxRollCurve = j;

		if (struct_system->yawCurve2[j]) auxYawCurve2 = j;
		if (struct_system->pitchCurve2[j]) auxPitchCurve2 = j;
		if (struct_system->rollCurve2[j]) auxRollCurve2 = j;

		if (auxColorCurve != -1 && auxSizeCurve != -1 && auxSpeedCurve != -1 && auxYawCurve != -1 && auxPitchCurve != -1 && auxRollCurve != -1 && auxYawCurve2 != -1 && auxPitchCurve2 != -1 && auxRollCurve2 != -1)	break;
	}

	jParticles["color_curve"] = auxColorCurve;
	jParticles["size_curve"] = auxSizeCurve;
	jParticles["speed_curve"] = auxSpeedCurve;

	jParticles["yaw_curve"] = auxYawCurve;
	jParticles["pitch_curve"] = auxPitchCurve;
	jParticles["roll_curve"] = auxRollCurve;

	jParticles["yaw_curve2"] = auxYawCurve2;
	jParticles["pitch_curve2"] = auxPitchCurve2;
	jParticles["roll_curve2"] = auxRollCurve2;

	jParticles["random_over_time_type"] = struct_system->random_over_time_type;
}

bool TCompEditorParticles::StartRotation(TCtesParticles* particles_buffer, system* struct_system) {
	bool changed = false;
	if (ImGui::TreeNode("3D Start Rotation")) {

		if (particles_buffer->emitter_billboard) {
			ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(1.0f, 0.80f, 0.0f, 1.0f));
			ImGui::Text("Render/Billboard activated");
			ImGui::PopStyleColor();
		}

		float yaw_r1 = deg2rad(struct_system->yaw.x);
		float yaw_r2 = deg2rad(struct_system->yaw.y);
		if (ImGui::DragFloat2("Initial Yaw", &struct_system->yaw.x, 0.1f, -180.0f, 180.0f)) {
			changed = true;
			yaw_r1 = deg2rad(struct_system->yaw.x);
			yaw_r2 = deg2rad(struct_system->yaw.y);
		}

		float pitch_r1 = deg2rad(struct_system->pitch.x);
		float pitch_r2 = deg2rad(struct_system->pitch.y);
		float max_pitch = 90.0f - 1e-3f;
		if (ImGui::DragFloat2("Initial Pitch", &struct_system->pitch.x, 0.1f, -max_pitch, max_pitch)) {
			changed = true;
			pitch_r1 = deg2rad(struct_system->pitch.x);
			pitch_r2 = deg2rad(struct_system->pitch.y);
		}

		float roll_r1 = deg2rad(struct_system->roll.x);
		float roll_r2 = deg2rad(struct_system->roll.y);
		if (ImGui::DragFloat2("Initial Roll", &struct_system->roll.x, 0.1f, -180.0f, 180.0f)) {
			changed = true;
			roll_r1 = deg2rad(struct_system->roll.x);
			roll_r2 = deg2rad(struct_system->roll.y);
		}
		if (changed) {
			particles_buffer->emitter_initial_yaw.x = yaw_r1;
			particles_buffer->emitter_initial_yaw.y = yaw_r1;
			particles_buffer->emitter_initial_roll.x = roll_r1;
			particles_buffer->emitter_initial_roll.y = roll_r2;
			particles_buffer->emitter_initial_pitch.x = pitch_r1;
			particles_buffer->emitter_initial_pitch.y = pitch_r2;
		}
		ImGui::TreePop();
	}
	return changed;
}

bool TCompEditorParticles::SimpleRotation(TCtesParticles* particles_buffer, system* struct_system) {
	ImGui::PushItemWidth(100);
	bool quatChanged = false;

	char title[64];
	strcpy(title, "Yaw");

	float yaw_r = deg2rad(struct_system->yaw_over_time[0]);
	if (ImGui::DragFloat(title, &struct_system->yaw_over_time[0], 0.1f, -180.0f, 180.0f)) {
		quatChanged = true;
		yaw_r = deg2rad(struct_system->yaw_over_time[0]);
	}

	ImGui::SameLine();
	strcpy(title, "Pitch");
	float pitch_r = deg2rad(struct_system->pitch_over_time[0]);
	float max_pitch = 90.0f - 1e-3f;
	if (ImGui::DragFloat(title, &struct_system->pitch_over_time[0], 0.1f, -max_pitch, max_pitch)) {
		quatChanged = true;
		pitch_r = deg2rad(struct_system->pitch_over_time[0]);
	}
	ImGui::SameLine();

	strcpy(title, "Roll");

	float roll_r = deg2rad(struct_system->roll_over_time[0]);
	if (ImGui::DragFloat(title, &struct_system->roll_over_time[0], 0.1f, -180.0f, 180.0f)) {
		quatChanged = true;
		roll_r = deg2rad(struct_system->roll_over_time[0]);
	}

	if (quatChanged) {
		for (int i = 0; i < MAX_QUATS; i++) {
			struct_system->yaw_over_time[i] = struct_system->yaw_over_time[0];
			struct_system->roll_over_time[i] = struct_system->roll_over_time[0];
			struct_system->pitch_over_time[i] = struct_system->pitch_over_time[0];

			struct_system->yaw_over_time2[i] = struct_system->yaw_over_time[0];
			struct_system->roll_over_time2[i] = struct_system->roll_over_time[0];
			struct_system->pitch_over_time2[i] = struct_system->pitch_over_time[0];

			particles_buffer->emitter_yaw_over_time[i].x = yaw_r;
			particles_buffer->emitter_pitch_over_time[i].x = pitch_r;
			particles_buffer->emitter_roll_over_time[i].x = roll_r;

			particles_buffer->emitter_yaw_over_time[i].y = yaw_r;
			particles_buffer->emitter_pitch_over_time[i].y = pitch_r;
			particles_buffer->emitter_roll_over_time[i].y = roll_r;
		}
	}

	return quatChanged;
	ImGui::PopItemWidth();
}

bool TCompEditorParticles::RandomBetweenTwoSimpleRotations(TCtesParticles* particles_buffer, system* struct_system) {
	ImGui::PushItemWidth(100);
	bool quatChanged = false;

	ImGui::Text("Rotation 1:");
	char title[64];
	strcpy(title, "Yaw1");
	float yaw_r = deg2rad(struct_system->yaw_over_time[0]);
	if (ImGui::DragFloat(title, &struct_system->yaw_over_time[0], 0.1f, -180.0f, 180.0f)) {
		quatChanged = true;
		yaw_r = deg2rad(struct_system->yaw_over_time[0]);
	}

	ImGui::SameLine();
	strcpy(title, "Pitch1");
	float pitch_r = deg2rad(struct_system->pitch_over_time[0]);
	float max_pitch = 90.0f - 1e-3f;
	if (ImGui::DragFloat(title, &struct_system->pitch_over_time[0], 0.1f, -max_pitch, max_pitch)) {
		quatChanged = true;
		pitch_r = deg2rad(struct_system->pitch_over_time[0]);
	}
	ImGui::SameLine();

	strcpy(title, "Roll1");

	float roll_r = deg2rad(struct_system->roll_over_time[0]);
	if (ImGui::DragFloat(title, &struct_system->roll_over_time[0], 0.1f, -180.0f, 180.0f)) {
		quatChanged = true;
		roll_r = deg2rad(struct_system->roll_over_time[0]);
	}

	ImGui::Text("Rotation 2:");
	strcpy(title, "Yaw2");
	float yaw_r2 = deg2rad(struct_system->yaw_over_time2[0]);
	if (ImGui::DragFloat(title, &struct_system->yaw_over_time2[0], 0.1f, -180.0f, 180.0f)) {
		quatChanged = true;
		yaw_r2 = deg2rad(struct_system->yaw_over_time2[0]);
	}

	ImGui::SameLine();
	strcpy(title, "Pitch2");
	float pitch_r2 = deg2rad(struct_system->pitch_over_time2[0]);
	if (ImGui::DragFloat(title, &struct_system->pitch_over_time2[0], 0.1f, -max_pitch, max_pitch)) {
		quatChanged = true;
		pitch_r2 = deg2rad(struct_system->pitch_over_time2[0]);
	}
	ImGui::SameLine();

	strcpy(title, "Roll2");

	float roll_r2 = deg2rad(struct_system->roll_over_time2[0]);
	if (ImGui::DragFloat(title, &struct_system->roll_over_time2[0], 0.1f, -180.0f, 180.0f)) {
		quatChanged = true;
		roll_r2 = deg2rad(struct_system->roll_over_time2[0]);
	}


	if (quatChanged) {
		for (int i = 0; i < MAX_QUATS; i++) {
			struct_system->yaw_over_time[i] = struct_system->yaw_over_time[0];
			struct_system->roll_over_time[i] = struct_system->roll_over_time[0];
			struct_system->pitch_over_time[i] = struct_system->pitch_over_time[0];

			struct_system->yaw_over_time2[i] = struct_system->yaw_over_time2[0];
			struct_system->roll_over_time2[i] = struct_system->roll_over_time2[0];
			struct_system->pitch_over_time2[i] = struct_system->pitch_over_time2[0];

			particles_buffer->emitter_yaw_over_time[i].x = yaw_r;
			particles_buffer->emitter_pitch_over_time[i].x = pitch_r;
			particles_buffer->emitter_roll_over_time[i].x = roll_r;

			particles_buffer->emitter_yaw_over_time[i].y = yaw_r2;
			particles_buffer->emitter_pitch_over_time[i].y = pitch_r2;
			particles_buffer->emitter_roll_over_time[i].y = roll_r2;
		}
	}

	return quatChanged;
	ImGui::PopItemWidth();
}

bool TCompEditorParticles::SimpleCurveRotation(TCtesParticles* particles_buffer, system* struct_system) {
	bool changed = false;

	if (ImGui::TreeNode("Rotation 1")) {

		ImGui::Text("  Yaw       -       ");
		ImGui::SameLine();
		ImGui::Text("Pitch       -       ");
		ImGui::SameLine();
		ImGui::Text("Roll");

		ImGui::PushItemWidth(100);
		for (int i = 0; i < MAX_QUATS; ++i) {
			bool quatChanged = false;

			char title[64];
			sprintf(title, "y%d", i);

			float yaw_r = deg2rad(struct_system->yaw_over_time[i]);
			if (ImGui::DragFloat(title, &struct_system->yaw_over_time[i], 0.1f, -180.0f, 180.0f)) {
				quatChanged = true;
				yaw_r = deg2rad(struct_system->yaw_over_time[i]);
			}
			ImGui::SameLine();

			sprintf(title, "p%d", i);
			float pitch_r = deg2rad(struct_system->pitch_over_time[i]);
			float max_pitch = 90.0f - 1e-3f;
			if (ImGui::DragFloat(title, &struct_system->pitch_over_time[i], 0.1f, -max_pitch, max_pitch)) {
				quatChanged = true;
				pitch_r = deg2rad(struct_system->pitch_over_time[i]);
			}
			ImGui::SameLine();

			sprintf(title, "r%d", i);
			float roll_r = deg2rad(struct_system->roll_over_time[i]);
			if (ImGui::DragFloat(title, &struct_system->roll_over_time[i], 0.1f, -180.0f, 180.0f)) {
				quatChanged = true;
				roll_r = deg2rad(struct_system->roll_over_time[i]);
			}

			if (quatChanged) {
				particles_buffer->emitter_yaw_over_time[i].x = yaw_r;
				particles_buffer->emitter_pitch_over_time[i].x = pitch_r;
				particles_buffer->emitter_roll_over_time[i].x = roll_r;
				changed |= quatChanged;
			}
		}
		ImGui::PopItemWidth();

		bool curveChanged = false;
		curveChanged |= YawCurves(particles_buffer, struct_system);
		curveChanged |= PitchCurves(particles_buffer, struct_system);
		curveChanged |= RollCurves(particles_buffer, struct_system);


		if (curveChanged) {
			changed |= curveChanged;
			for (int i = 0; i < MAX_QUATS; ++i) {
				struct_system->yaw_over_time2[i] = struct_system->yaw_over_time[i];
				struct_system->pitch_over_time2[i] = struct_system->pitch_over_time[i];
				struct_system->roll_over_time2[i] = struct_system->roll_over_time[i];

				particles_buffer->emitter_yaw_over_time[i].x = deg2rad(struct_system->yaw_over_time[i]);
				particles_buffer->emitter_pitch_over_time[i].x = deg2rad(struct_system->pitch_over_time[i]);
				particles_buffer->emitter_roll_over_time[i].x = deg2rad(struct_system->roll_over_time[i]);

				particles_buffer->emitter_yaw_over_time[i].y = particles_buffer->emitter_yaw_over_time[i].x;
				particles_buffer->emitter_pitch_over_time[i].y = particles_buffer->emitter_pitch_over_time[i].x;
				particles_buffer->emitter_roll_over_time[i].y = particles_buffer->emitter_roll_over_time[i].x;
			}
		}
		ImGui::TreePop();
	}
	return changed;
}

bool TCompEditorParticles::RandomBetweenTwoCurves(TCtesParticles* particles_buffer, system* struct_system) {
	bool changed = false;

	if (ImGui::TreeNode("Rotation 1")) {

		ImGui::Text("  Yaw       -       ");
		ImGui::SameLine();
		ImGui::Text("Pitch       -       ");
		ImGui::SameLine();
		ImGui::Text("Roll");

		ImGui::PushItemWidth(100);
		for (int i = 0; i < MAX_QUATS; ++i) {
			bool quatChanged = false;

			char title[64];
			sprintf(title, "y%d", i);

			float yaw_r = deg2rad(struct_system->yaw_over_time[i]);
			if (ImGui::DragFloat(title, &struct_system->yaw_over_time[i], 0.1f, -180.0f, 180.0f)) {
				quatChanged = true;
				yaw_r = deg2rad(struct_system->yaw_over_time[i]);
			}
			ImGui::SameLine();

			sprintf(title, "p%d", i);
			float pitch_r = deg2rad(struct_system->pitch_over_time[i]);
			float max_pitch = 90.0f - 1e-3f;
			if (ImGui::DragFloat(title, &struct_system->pitch_over_time[i], 0.1f, -max_pitch, max_pitch)) {
				quatChanged = true;
				pitch_r = deg2rad(struct_system->pitch_over_time[i]);
			}
			ImGui::SameLine();

			sprintf(title, "r%d", i);
			float roll_r = deg2rad(struct_system->roll_over_time[i]);
			if (ImGui::DragFloat(title, &struct_system->roll_over_time[i], 0.1f, -180.0f, 180.0f)) {
				quatChanged = true;
				roll_r = deg2rad(struct_system->roll_over_time[i]);
			}

			if (quatChanged) {
				particles_buffer->emitter_yaw_over_time[i].x = yaw_r;
				particles_buffer->emitter_pitch_over_time[i].x = pitch_r;
				particles_buffer->emitter_roll_over_time[i].x = roll_r;

				changed |= quatChanged;
			}
		}
		ImGui::PopItemWidth();

		bool curveChanged = false;
		curveChanged |= YawCurves(particles_buffer, struct_system);
		curveChanged |= PitchCurves(particles_buffer, struct_system);
		curveChanged |= RollCurves(particles_buffer, struct_system);


		if (curveChanged) {
			changed |= curveChanged;
			for (int i = 0; i < MAX_QUATS; ++i) {
				particles_buffer->emitter_yaw_over_time[i].x = deg2rad(struct_system->yaw_over_time[i]);
				particles_buffer->emitter_pitch_over_time[i].x = deg2rad(struct_system->pitch_over_time[i]);
				particles_buffer->emitter_roll_over_time[i].x = deg2rad(struct_system->roll_over_time[i]);
			}
		}
		ImGui::TreePop();

	}

	if (ImGui::TreeNode("Rotation 2")) {

		ImGui::Text("  Yaw       -       ");
		ImGui::SameLine();
		ImGui::Text("Pitch       -       ");
		ImGui::SameLine();
		ImGui::Text("Roll");

		ImGui::PushItemWidth(100);
		for (int i = 0; i < MAX_QUATS; ++i) {
			bool quatChanged = false;

			char title[64];
			sprintf(title, "y%d", i);

			float yaw_r2 = deg2rad(struct_system->yaw_over_time2[i]);
			if (ImGui::DragFloat(title, &struct_system->yaw_over_time2[i], 0.1f, -180.0f, 180.0f)) {
				quatChanged = true;
				yaw_r2 = deg2rad(struct_system->yaw_over_time2[i]);
			}
			ImGui::SameLine();

			sprintf(title, "p%d", i);
			float pitch_r2 = deg2rad(struct_system->pitch_over_time2[i]);
			float max_pitch = 90.0f - 1e-3f;
			if (ImGui::DragFloat(title, &struct_system->pitch_over_time2[i], 0.1f, -max_pitch, max_pitch)) {
				quatChanged = true;
				pitch_r2 = deg2rad(struct_system->pitch_over_time2[i]);
			}
			ImGui::SameLine();

			sprintf(title, "r%d", i);
			float roll_r2 = deg2rad(struct_system->roll_over_time2[i]);
			if (ImGui::DragFloat(title, &struct_system->roll_over_time2[i], 0.1f, -180.0f, 180.0f)) {
				quatChanged = true;
				roll_r2 = deg2rad(struct_system->roll_over_time2[i]);
			}

			if (quatChanged) {
				particles_buffer->emitter_yaw_over_time[i].y = yaw_r2;
				particles_buffer->emitter_pitch_over_time[i].y = pitch_r2;
				particles_buffer->emitter_roll_over_time[i].y = roll_r2;
				changed |= quatChanged;
			}
		}
		ImGui::PopItemWidth();

		bool curveChanged = false;
		curveChanged |= YawCurves2(particles_buffer, struct_system);
		curveChanged |= PitchCurves2(particles_buffer, struct_system);
		curveChanged |= RollCurves2(particles_buffer, struct_system);


		if (curveChanged) {
			changed |= curveChanged;
			for (int i = 0; i < MAX_QUATS; ++i) {
				particles_buffer->emitter_yaw_over_time[i].y = deg2rad(struct_system->yaw_over_time2[i]);
				particles_buffer->emitter_pitch_over_time[i].y = deg2rad(struct_system->pitch_over_time2[i]);
				particles_buffer->emitter_roll_over_time[i].y = deg2rad(struct_system->roll_over_time2[i]);
			}
		}
		ImGui::TreePop();
	}

	return changed;
}

bool TCompEditorParticles::ColorCurves(TCtesParticles* particles_buffer, system* struct_system) {
	bool changed = false;

	if (ImGui::TreeNode("Color Curves"))
	{
		ImGui::Separator();
		changed |= ImGui::ColorEdit4("color in", &particles_buffer->psystem_colors_over_time[0].x);

		ImGui::Separator();
		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.9f, 0.6f, 0.9f, 0.8f));
		if (ImGui::Button("Add Node"))
			struct_system->nodeColor.push_back(std::pair(VEC4(1,1,1,1), 0.5));
		ImGui::PopStyleColor();
		ImGui::Separator();
		for (int j = 0; j < struct_system->nodeColor.size(); j++)
		{
			char title[64];
			sprintf(title, "Node: %d", j);
			ImGui::Text(title);
			ImGui::SameLine();
			ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(1.0f, 0.00f, 0.0f, 1.00f));

			char titleDel[64];
			sprintf(titleDel, "Del Node: %d", j);
			if (ImGui::Button(titleDel)) {
				struct_system->nodeColor.erase(struct_system->nodeColor.begin() + j);
				ImGui::PopStyleColor();
				break;
			}
			ImGui::PopStyleColor();


			char titleSize[64];
			sprintf(titleSize, "Color Node: %d", j);
			changed |= ImGui::ColorEdit4(titleSize, &struct_system->nodeColor[j].first.x);


			char titleRatio[64];
			sprintf(titleRatio, "Ratio Node: %d", j);
			if (struct_system->nodeColor.size() == 0 || j == 0)
				changed |= ImGui::DragFloat(titleRatio, &struct_system->nodeColor[j].second, 0.01f, 0.0f, 1.0f);
			else
				changed |= ImGui::DragFloat(titleRatio, &struct_system->nodeColor[j].second, 0.01f, struct_system->nodeColor[struct_system->nodeColor.size() - 2].second, 1.0f);

			if (j > 0 && struct_system->nodeColor[j].second < struct_system->nodeColor[j - 1].second)
				struct_system->nodeColor[j].second = struct_system->nodeColor[j - 1].second;


			ImGui::Separator();
		}

		changed |= ImGui::ColorEdit4("color out", &particles_buffer->psystem_colors_over_time[7].x);

		VEC4 colorIn = particles_buffer->psystem_colors_over_time[0];
		VEC4 colorOut = particles_buffer->psystem_colors_over_time[7];

		ImGui::Combo("Curve Type", (int*)&num_curve_color, curve_type_str);
		changed |= SingleColorCuve(curveInterpolator[num_curve_color].first, curveInterpolator[num_curve_color].second, colorIn, colorOut, particles_buffer, struct_system, num_curve_color);

		ImGui::TreePop();
	}

	return changed;
}

bool TCompEditorParticles::SingleColorCuve(const char* name, Interpolator::IInterpolator* interpolator, VEC4 colorIn, VEC4 colorOut, TCtesParticles* particles_buffer, system* struct_system, int num) {
	char applyTxt[128];
	strcpy(applyTxt, "Apply ");
	strcat(applyTxt, name);

	bool changed = false;

	renderInterpolator(name, interpolator);
	bool auxReference = struct_system->colorCurve[num];
	ImGui::SameLine();
	ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.92f, 0.30f, 0.87f, 0.8f));
	if (ImGui::Checkbox(applyTxt, &auxReference) || struct_system->colorCurve[num]) {
		std::fill(begin(struct_system->colorCurve), end(struct_system->colorCurve), false);
		struct_system->colorCurve[num] = auxReference;
		changed |= ApplyColorCurve(colorIn, colorOut, interpolator, particles_buffer, struct_system);
	}
	ImGui::PopStyleColor();

	return changed;
}

bool TCompEditorParticles::ApplyColorCurve(VEC4 colorIn, VEC4 colorOut, Interpolator::IInterpolator* interpolator, TCtesParticles* particles_buffer, system* struct_system){

	bool blended = false;

	//X
	for (int timeStep = 0; timeStep < MAX_COLORS; timeStep++) {
		blended = false;

		float ratio = (1. / 7.) * timeStep;
		for (int i = 0; i < struct_system->nodeColor.size(); i++) {
			if (ratio < struct_system->nodeColor[i].second) {
				if (i == 0)
					particles_buffer->psystem_colors_over_time[timeStep].x = interpolator->blend(colorIn.x, struct_system->nodeColor[i].first.x, ratio * (1 / struct_system->nodeColor[i].second));
				else
					particles_buffer->psystem_colors_over_time[timeStep].x = interpolator->blend(struct_system->nodeColor[i - 1].first.x, struct_system->nodeColor[i].first.x, (ratio - struct_system->nodeColor[i - 1].second) * (1 / (struct_system->nodeColor[i].second - struct_system->nodeColor[i - 1].second)));
				blended = true;
				break;
			}
		}

		if (blended)
			continue;

		if (struct_system->nodeColor.size() == 0)
			particles_buffer->psystem_colors_over_time[timeStep].x = interpolator->blend(colorIn.x, colorOut.x, ratio);
		else
			particles_buffer->psystem_colors_over_time[timeStep].x = interpolator->blend(struct_system->nodeColor.back().first.x, colorOut.x, (ratio - struct_system->nodeColor.back().second) * (1 / (1 - struct_system->nodeColor.back().second)));
	}

	//Y
	for (int timeStep = 0; timeStep < MAX_COLORS; timeStep++) {
		blended = false;

		float ratio = (1. / 7.) * timeStep;
		for (int i = 0; i < struct_system->nodeColor.size(); i++) {
			if (ratio < struct_system->nodeColor[i].second) {
				if (i == 0)
					particles_buffer->psystem_colors_over_time[timeStep].y = interpolator->blend(colorIn.y, struct_system->nodeColor[i].first.y, ratio * (1 / struct_system->nodeColor[i].second));
				else
					particles_buffer->psystem_colors_over_time[timeStep].y = interpolator->blend(struct_system->nodeColor[i - 1].first.y, struct_system->nodeColor[i].first.y, (ratio - struct_system->nodeColor[i - 1].second) * (1 / (struct_system->nodeColor[i].second - struct_system->nodeColor[i - 1].second)));
				blended = true;
				break;
			}
		}

		if (blended)
			continue;

		if (struct_system->nodeColor.size() == 0)
			particles_buffer->psystem_colors_over_time[timeStep].y = interpolator->blend(colorIn.y, colorOut.y, ratio);
		else
			particles_buffer->psystem_colors_over_time[timeStep].y = interpolator->blend(struct_system->nodeColor.back().first.y, colorOut.y, (ratio - struct_system->nodeColor.back().second) * (1 / (1 - struct_system->nodeColor.back().second)));
	}

	//Z
	for (int timeStep = 0; timeStep < MAX_COLORS; timeStep++) {
		blended = false;

		float ratio = (1. / 7.) * timeStep;
		for (int i = 0; i < struct_system->nodeColor.size(); i++) {
			if (ratio < struct_system->nodeColor[i].second) {
				if (i == 0)
					particles_buffer->psystem_colors_over_time[timeStep].z = interpolator->blend(colorIn.z, struct_system->nodeColor[i].first.z, ratio * (1 / struct_system->nodeColor[i].second));
				else
					particles_buffer->psystem_colors_over_time[timeStep].z = interpolator->blend(struct_system->nodeColor[i - 1].first.z, struct_system->nodeColor[i].first.z, (ratio - struct_system->nodeColor[i - 1].second) * (1 / (struct_system->nodeColor[i].second - struct_system->nodeColor[i - 1].second)));
				blended = true;
				break;
			}
		}

		if (blended)
			continue;

		if (struct_system->nodeColor.size() == 0)
			particles_buffer->psystem_colors_over_time[timeStep].z = interpolator->blend(colorIn.z, colorOut.z, ratio);
		else
			particles_buffer->psystem_colors_over_time[timeStep].z = interpolator->blend(struct_system->nodeColor.back().first.z, colorOut.z, (ratio - struct_system->nodeColor.back().second) * (1 / (1 - struct_system->nodeColor.back().second)));
	}

	//W
	for (int timeStep = 0; timeStep < MAX_COLORS; timeStep++) {
		blended = false;

		float ratio = (1. / 7.) * timeStep;
		for (int i = 0; i < struct_system->nodeColor.size(); i++) {
			if (ratio < struct_system->nodeColor[i].second) {
				if (i == 0)
					particles_buffer->psystem_colors_over_time[timeStep].w = interpolator->blend(colorIn.w, struct_system->nodeColor[i].first.w, ratio * (1 / struct_system->nodeColor[i].second));
				else
					particles_buffer->psystem_colors_over_time[timeStep].w = interpolator->blend(struct_system->nodeColor[i - 1].first.w, struct_system->nodeColor[i].first.w, (ratio - struct_system->nodeColor[i - 1].second) * (1 / (struct_system->nodeColor[i].second - struct_system->nodeColor[i - 1].second)));
				blended = true;
				break;
			}
		}

		if (blended)
			continue;

		if (struct_system->nodeColor.size() == 0)
			particles_buffer->psystem_colors_over_time[timeStep].w = interpolator->blend(colorIn.w, colorOut.w, ratio);
		else
			particles_buffer->psystem_colors_over_time[timeStep].w = interpolator->blend(struct_system->nodeColor.back().first.w, colorOut.w, (ratio - struct_system->nodeColor.back().second) * (1 / (1 - struct_system->nodeColor.back().second)));
	}

	return true;
}

bool TCompEditorParticles::SizeCurves(TCtesParticles* particles_buffer, system* struct_system) {
	bool changed = false;

	if (ImGui::TreeNode("SizeCurves"))
	{
		ImGui::Separator();
		changed |= ImGui::DragFloat("Size In", &particles_buffer->psystem_sizes_over_time[0].x, 0.01f, 0.0f, 10.0f);

		ImGui::Separator();
		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.9f, 0.6f, 0.9f, 0.8f));
		if (ImGui::Button("Add Node"))
			struct_system->nodeSize.push_back(std::pair(1.0, 0.5));
		ImGui::PopStyleColor();
		ImGui::Separator();
		for (int j = 0; j < struct_system->nodeSize.size(); j++)
		{
			char title[64];
			sprintf(title, "Node: %d", j);
			ImGui::Text(title);
			ImGui::SameLine();
			ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(1.0f, 0.00f, 0.0f, 1.00f));

			char titleDel[64];
			sprintf(titleDel, "Del Node: %d", j);
			if (ImGui::Button(titleDel)) {
				struct_system->nodeSize.erase(struct_system->nodeSize.begin() + j);
				ImGui::PopStyleColor();
				break;
			}
			ImGui::PopStyleColor();

			ImGui::PushItemWidth(100);

			char titleSize[64];
			sprintf(titleSize, "Size Node: %d", j);
			changed |= ImGui::DragFloat(titleSize, &struct_system->nodeSize[j].first, 0.01f, 0.0f, 10.0f);

			ImGui::SameLine();

			char titleRatio[64];
			sprintf(titleRatio, "Ratio Node: %d", j);
			if(struct_system->nodeSize.size() == 0 || j == 0)
				changed |= ImGui::DragFloat(titleRatio, &struct_system->nodeSize[j].second, 0.01f, 0.0f, 1.0f);
			else
				changed |= ImGui::DragFloat(titleRatio, &struct_system->nodeSize[j].second, 0.01f, struct_system->nodeSize[struct_system->nodeSize.size()-2].second, 1.0f);

			if (j > 0 && struct_system->nodeSize[j].second < struct_system->nodeSize[j - 1].second)
				struct_system->nodeSize[j].second = struct_system->nodeSize[j - 1].second;
			ImGui::PopItemWidth();
			ImGui::Separator();
		}
		changed |= ImGui::DragFloat("Size Out", &particles_buffer->psystem_sizes_over_time[7].x, 0.01f, 0.0f, 10.0f);
		ImGui::Separator();

		float sizeIn = particles_buffer->psystem_sizes_over_time[0].x;
		float sizeOut  = particles_buffer->psystem_sizes_over_time[7].x;


		ImGui::Combo("Curve Type", (int*)&num_curve_size, curve_type_str);
		changed |= SingleSizeCuve(curveInterpolator[num_curve_size].first, curveInterpolator[num_curve_size].second, sizeIn, sizeOut, particles_buffer, struct_system, num_curve_size);

		ImGui::TreePop();
	}

	return changed;
}


bool TCompEditorParticles::SingleSizeCuve(const char* name, Interpolator::IInterpolator* interpolator, float sizeIn, float sizeOut, TCtesParticles* particles_buffer, system* struct_system, int num) {
	char applyTxt[128];
	strcpy(applyTxt, "Apply ");
	strcat(applyTxt, name);

	bool changed = false;

	renderInterpolator(name, interpolator);
	bool auxReference = struct_system->sizeCurve[num];
	ImGui::SameLine();
	ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.92f, 0.30f, 0.87f, 0.8f));
	if (ImGui::Checkbox(applyTxt, &auxReference) || struct_system->sizeCurve[num]) {
		std::fill(begin(struct_system->sizeCurve), end(struct_system->sizeCurve), false);
		struct_system->sizeCurve[num] = auxReference;
		changed |= ApplySizeCurve(sizeIn, sizeOut, interpolator, particles_buffer, struct_system);
	}
	ImGui::PopStyleColor();

	return changed;
}


bool TCompEditorParticles::ApplySizeCurve(float sizeIn, float sizeOut, Interpolator::IInterpolator* interpolator, TCtesParticles* particles_buffer, system* struct_system) {

	bool blended = false;
	for (int timeStep = 0; timeStep < MAX_SIZES; timeStep++) {
		blended = false;

		float ratio = (1. / 7.) * timeStep;
		for (int i = 0; i < struct_system->nodeSize.size(); i++) {
			if (ratio < struct_system->nodeSize[i].second) {
				if(i == 0)
					particles_buffer->psystem_sizes_over_time[timeStep].x = interpolator->blend(sizeIn, struct_system->nodeSize[i].first, ratio * (1/ struct_system->nodeSize[i].second));
				else
					particles_buffer->psystem_sizes_over_time[timeStep].x = interpolator->blend(struct_system->nodeSize[i -1].first, struct_system->nodeSize[i].first, (ratio - struct_system->nodeSize[i-1].second) * (1 / (struct_system->nodeSize[i].second - struct_system->nodeSize[i - 1].second)));
				blended = true;
				break;
			}
		}

		if (blended)
			continue;

		if(struct_system->nodeSize.size() == 0)
			particles_buffer->psystem_sizes_over_time[timeStep].x = interpolator->blend(sizeIn, sizeOut, ratio);
		else
			particles_buffer->psystem_sizes_over_time[timeStep].x = interpolator->blend(struct_system->nodeSize.back().first, sizeOut, (ratio - struct_system->nodeSize.back().second) * (1/(1- struct_system->nodeSize.back().second)));
	}

	return true;
}
 

bool TCompEditorParticles::SpeedCurves(TCtesParticles* particles_buffer, system* struct_system) {
	bool changed = false;

	if (ImGui::TreeNode("SpeedModifierCurves"))
	{
		ImGui::Separator();
		changed |= ImGui::DragFloat("Speed Modifier In", &particles_buffer->psystem_speed_over_time[0].x, 0.01f, -10.0f, 10.0f);

		ImGui::Separator();
		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.9f, 0.6f, 0.9f, 0.8f));
		if (ImGui::Button("Add Node"))
			struct_system->nodeSpeed.push_back(std::pair(1.0, 0.5));
		ImGui::PopStyleColor();
		ImGui::Separator();
		for (int j = 0; j < struct_system->nodeSpeed.size(); j++)
		{
			char title[64];
			sprintf(title, "Node: %d", j);
			ImGui::Text(title);
			ImGui::SameLine();
			ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(1.0f, 0.00f, 0.0f, 1.00f));

			char titleDel[64];
			sprintf(titleDel, "Del Node: %d", j);
			if (ImGui::Button(titleDel)) {
				struct_system->nodeSpeed.erase(struct_system->nodeSpeed.begin() + j);
				ImGui::PopStyleColor();
				break;
			}
			ImGui::PopStyleColor();
			ImGui::PushItemWidth(100);
			char titleSize[64];
			sprintf(titleSize, "Speed Modifier Node: %d", j);
			changed |= ImGui::DragFloat(titleSize, &struct_system->nodeSpeed[j].first, 0.01f, -10.0f, 10.0f);

			ImGui::SameLine();

			char titleRatio[64];
			sprintf(titleRatio, "Ratio Node: %d", j);
			if (struct_system->nodeSize.size() == 0 || j == 0)
				changed |= ImGui::DragFloat(titleRatio, &struct_system->nodeSpeed[j].second, 0.01f, 0.0f, 1.0f);
			else
				changed |= ImGui::DragFloat(titleRatio, &struct_system->nodeSpeed[j].second, 0.01f, struct_system->nodeSpeed[struct_system->nodeSpeed.size() - 2].second, 1.0f);

			if (j > 0 && struct_system->nodeSpeed[j].second < struct_system->nodeSpeed[j - 1].second)
				struct_system->nodeSpeed[j].second = struct_system->nodeSpeed[j - 1].second;

			ImGui::PopItemWidth();
			ImGui::Separator();
		}
		changed |= ImGui::DragFloat("Speed Modifier Out", &particles_buffer->psystem_speed_over_time[7].x, 0.01f, -10.0f, 10.0f);
		ImGui::Separator();

		float speedIn = particles_buffer->psystem_speed_over_time[0].x;
		float speedOut = particles_buffer->psystem_speed_over_time[7].x;

		ImGui::Combo("Curve Type", (int*)&num_curve_velocity, curve_type_str);
		changed |= SingleSpeedCuve(curveInterpolator[num_curve_velocity].first, curveInterpolator[num_curve_velocity].second, speedIn, speedOut, particles_buffer, struct_system, num_curve_velocity);

		ImGui::TreePop();
	}

	return changed;
}

bool TCompEditorParticles::SingleSpeedCuve(const char* name, Interpolator::IInterpolator* interpolator, float speedIn, float speedOut, TCtesParticles* particles_buffer, system* struct_system, int num) {
	char applyTxt[128];
	strcpy(applyTxt, "Apply ");
	strcat(applyTxt, name);

	bool changed = false;

	renderInterpolator(name, interpolator);
	bool auxReference = struct_system->speedCurve[num];
	ImGui::SameLine();
	ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.92f, 0.30f, 0.87f, 0.8f));
	if (ImGui::Checkbox(applyTxt, &auxReference) || struct_system->speedCurve[num]) {
		std::fill(begin(struct_system->speedCurve), end(struct_system->speedCurve), false);
		struct_system->speedCurve[num] = auxReference;
		changed |= ApplySpeedCurve(speedIn, speedOut, interpolator, particles_buffer, struct_system);
	}
	ImGui::PopStyleColor();

	return changed;
}

bool TCompEditorParticles::ApplySpeedCurve(float speedIn, float speedOut, Interpolator::IInterpolator* interpolator, TCtesParticles* particles_buffer, system* struct_system) {

	bool blended = false;
	for (int timeStep = 0; timeStep < MAX_SPEEDS; timeStep++) {
		blended = false;

		float ratio = (1. / 7.) * timeStep;
		for (int i = 0; i < struct_system->nodeSpeed.size(); i++) {
			if (ratio < struct_system->nodeSpeed[i].second) {
				if (i == 0)
					particles_buffer->psystem_speed_over_time[timeStep].x = interpolator->blend(speedIn, struct_system->nodeSpeed[i].first, ratio * (1 / struct_system->nodeSpeed[i].second));
				else
					particles_buffer->psystem_speed_over_time[timeStep].x = interpolator->blend(struct_system->nodeSpeed[i - 1].first, struct_system->nodeSpeed[i].first, (ratio - struct_system->nodeSpeed[i - 1].second) * (1 / (struct_system->nodeSpeed[i].second - struct_system->nodeSpeed[i - 1].second)));
				blended = true;
				break;
			}
		}

		if (blended)
			continue;

		if (struct_system->nodeSpeed.size() == 0)
			particles_buffer->psystem_speed_over_time[timeStep].x = interpolator->blend(speedIn, speedOut, ratio);
		else
			particles_buffer->psystem_speed_over_time[timeStep].x = interpolator->blend(struct_system->nodeSpeed.back().first, speedOut, (ratio - struct_system->nodeSpeed.back().second) * (1 / (1 - struct_system->nodeSpeed.back().second)));
	}

	return true;
}


bool TCompEditorParticles::YawCurves(TCtesParticles* particles_buffer, system* struct_system) {
	bool changed = false;

	if (ImGui::TreeNode("YawModifierCurves"))
	{
		ImGui::Separator();
		changed |= ImGui::DragFloat("Yaw Modifier In", &struct_system->yaw_over_time[0], 0.1f, -180.0f, 180.0f);

		ImGui::Separator();
		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.9f, 0.6f, 0.9f, 0.8f));
		if (ImGui::Button("Add Node"))
			struct_system->nodeYaw.push_back(std::pair(1.0, 0.5));
		ImGui::PopStyleColor();
		ImGui::Separator();
		for (int j = 0; j < struct_system->nodeYaw.size(); j++)
		{
			char title[64];
			sprintf(title, "Node: %d", j);
			ImGui::Text(title);
			ImGui::SameLine();
			ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(1.0f, 0.00f, 0.0f, 1.00f));

			char titleDel[64];
			sprintf(titleDel, "Del Node: %d", j);
			if (ImGui::Button(titleDel)) {
				struct_system->nodeYaw.erase(struct_system->nodeYaw.begin() + j);
				ImGui::PopStyleColor();
				break;
			}
			ImGui::PopStyleColor();

			ImGui::PushItemWidth(100);
			char titleSize[64];
			sprintf(titleSize, "Yaw Modifier Node: %d", j);
			changed |= ImGui::DragFloat(titleSize, &struct_system->nodeYaw[j].first, 0.1f, -180.0f, 180.0f);

			ImGui::SameLine();

			char titleRatio[64];
			sprintf(titleRatio, "Ratio Node: %d", j);
			if (struct_system->nodeSize.size() == 0 || j == 0)
				changed |= ImGui::DragFloat(titleRatio, &struct_system->nodeYaw[j].second, 0.01f, 0.0f, 1.0f);
			else
				changed |= ImGui::DragFloat(titleRatio, &struct_system->nodeYaw[j].second, 0.01f, struct_system->nodeYaw[struct_system->nodeYaw.size() - 2].second, 1.0f);

			if (j > 0 && struct_system->nodeYaw[j].second < struct_system->nodeYaw[j - 1].second)
				struct_system->nodeYaw[j].second = struct_system->nodeYaw[j - 1].second;

			ImGui::PopItemWidth();
			ImGui::Separator();
		}
		changed |= ImGui::DragFloat("Yaw Modifier Out", &struct_system->yaw_over_time[7], 0.1f, -180.0f, 180.0f);
		ImGui::Separator();

		float yawIn = struct_system->yaw_over_time[0];
		float yawOut = struct_system->yaw_over_time[7];


		ImGui::Combo("Curve Type", (int*)&num_curve_yaw, curve_type_str);
		changed |= SingleYawCuve(curveInterpolator[num_curve_yaw].first, curveInterpolator[num_curve_yaw].second, yawIn, yawOut, particles_buffer, struct_system, num_curve_yaw);
		
		ImGui::TreePop();
	}

	return changed;
}

bool TCompEditorParticles::SingleYawCuve(const char* name, Interpolator::IInterpolator* interpolator, float yawIn, float yawOut, TCtesParticles* particles_buffer, system* struct_system, int num) {
	char applyTxt[128];
	strcpy(applyTxt, "Apply ");
	strcat(applyTxt, name);

	bool changed = false;

	renderInterpolator(name, interpolator);
	bool auxReference = struct_system->yawCurve[num];
	ImGui::SameLine();
	ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.92f, 0.30f, 0.87f, 0.8f));
	if (ImGui::Checkbox(applyTxt, &auxReference) || struct_system->yawCurve[num]) {
		std::fill(begin(struct_system->yawCurve), end(struct_system->yawCurve), false);
		struct_system->yawCurve[num] = auxReference;
		changed |= ApplyYawCurve(yawIn, yawOut, interpolator, particles_buffer, struct_system);
	}
	ImGui::PopStyleColor();

	return changed;
}

bool TCompEditorParticles::ApplyYawCurve(float yawIn, float yawOut, Interpolator::IInterpolator* interpolator, TCtesParticles* particles_buffer, system* struct_system) {

	bool blended = false;
	for (int timeStep = 0; timeStep < MAX_QUATS; timeStep++) {
		blended = false;

		float ratio = (1. / 7.) * timeStep;
		for (int i = 0; i < struct_system->nodeYaw.size(); i++) {
			if (ratio < struct_system->nodeYaw[i].second) {
				if (i == 0)
					struct_system->yaw_over_time[timeStep] = interpolator->blend(yawIn, struct_system->nodeYaw[i].first, ratio * (1 / struct_system->nodeYaw[i].second));
				else
					struct_system->yaw_over_time[timeStep] = interpolator->blend(struct_system->nodeYaw[i - 1].first, struct_system->nodeYaw[i].first, (ratio - struct_system->nodeYaw[i - 1].second) * (1 / (struct_system->nodeYaw[i].second - struct_system->nodeYaw[i - 1].second)));
				blended = true;
				break;
			}
		}

		if (blended)
			continue;

		if (struct_system->nodeYaw.size() == 0)
			struct_system->yaw_over_time[timeStep] = interpolator->blend(yawIn, yawOut, ratio);
		else
			struct_system->yaw_over_time[timeStep] = interpolator->blend(struct_system->nodeYaw.back().first, yawOut, (ratio - struct_system->nodeYaw.back().second) * (1 / (1 - struct_system->nodeYaw.back().second)));
	}

	return true;
}

bool TCompEditorParticles::PitchCurves(TCtesParticles* particles_buffer, system* struct_system) {
	bool changed = false;

	if (ImGui::TreeNode("PitchModifierCurve"))
	{
		ImGui::Separator();
		float max_pitch = 90.0f - 1e-3f;
		changed |= ImGui::DragFloat("Pitch Modifier In", &struct_system->pitch_over_time[0], 0.1f, -max_pitch, -max_pitch);

		ImGui::Separator();
		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.9f, 0.6f, 0.9f, 0.8f));
		if (ImGui::Button("Add Node"))
			struct_system->nodePitch.push_back(std::pair(1.0, 0.5));
		ImGui::PopStyleColor();
		ImGui::Separator();
		for (int j = 0; j < struct_system->nodePitch.size(); j++)
		{
			char title[64];
			sprintf(title, "Node: %d", j);
			ImGui::Text(title);
			ImGui::SameLine();
			ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(1.0f, 0.00f, 0.0f, 1.00f));

			char titleDel[64];
			sprintf(titleDel, "Del Node: %d", j);
			if (ImGui::Button(titleDel)) {
				struct_system->nodePitch.erase(struct_system->nodePitch.begin() + j);
				ImGui::PopStyleColor();
				break;
			}
			ImGui::PopStyleColor();

			ImGui::PushItemWidth(100);
			char titleSize[64];
			sprintf(titleSize, "Pitch Modifier Node: %d", j);
			changed |= ImGui::DragFloat(titleSize, &struct_system->nodePitch[j].first, 0.1f, -max_pitch, -max_pitch);

			ImGui::SameLine();
			char titleRatio[64];
			sprintf(titleRatio, "Ratio Node: %d", j);
			if (struct_system->nodeSize.size() == 0 || j == 0)
				changed |= ImGui::DragFloat(titleRatio, &struct_system->nodePitch[j].second, 0.01f, 0.0f, 1.0f);
			else
				changed |= ImGui::DragFloat(titleRatio, &struct_system->nodePitch[j].second, 0.01f, struct_system->nodePitch[struct_system->nodePitch.size() - 2].second, 1.0f);

			if (j > 0 && struct_system->nodePitch[j].second < struct_system->nodePitch[j - 1].second)
				struct_system->nodePitch[j].second = struct_system->nodePitch[j - 1].second;

			ImGui::PopItemWidth();
			ImGui::Separator();
		}
		changed |= ImGui::DragFloat("Pitch Modifier Out", &struct_system->pitch_over_time[7], 0.1f, -max_pitch, -max_pitch);
		ImGui::Separator();

		float pitchIn = struct_system->pitch_over_time[0];
		float pitchOut = struct_system->pitch_over_time[7];


		ImGui::Combo("Curve Type", (int*)&num_curve_pitch, curve_type_str);
		changed |= SinglePitchCuve(curveInterpolator[num_curve_pitch].first, curveInterpolator[num_curve_pitch].second, pitchIn, pitchOut, particles_buffer, struct_system, num_curve_pitch);

		ImGui::TreePop();
	}

	return changed;
}

bool TCompEditorParticles::SinglePitchCuve(const char* name, Interpolator::IInterpolator* interpolator, float pitchIn, float pitchOut, TCtesParticles* particles_buffer, system* struct_system, int num) {
	char applyTxt[128];
	strcpy(applyTxt, "Apply ");
	strcat(applyTxt, name);

	bool changed = false;

	renderInterpolator(name, interpolator);
	bool auxReference = struct_system->pitchCurve[num];
	ImGui::SameLine();
	ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.92f, 0.30f, 0.87f, 0.8f));
	if (ImGui::Checkbox(applyTxt, &auxReference) || struct_system->pitchCurve[num]) {
		std::fill(begin(struct_system->pitchCurve), end(struct_system->pitchCurve), false);
		struct_system->pitchCurve[num] = auxReference;
		changed |= ApplyPitchCurve(pitchIn, pitchOut, interpolator, particles_buffer, struct_system);
	}
	ImGui::PopStyleColor();

	return changed;
}

bool TCompEditorParticles::ApplyPitchCurve(float pitchIn, float pitchOut, Interpolator::IInterpolator* interpolator, TCtesParticles* particles_buffer, system* struct_system) {

	bool blended = false;
	for (int timeStep = 0; timeStep < MAX_QUATS; timeStep++) {
		blended = false;

		float ratio = (1. / 7.) * timeStep;
		for (int i = 0; i < struct_system->nodePitch.size(); i++) {
			if (ratio < struct_system->nodePitch[i].second) {
				if (i == 0)
					struct_system->pitch_over_time[timeStep] = interpolator->blend(pitchIn, struct_system->nodePitch[i].first, ratio * (1 / struct_system->nodePitch[i].second));
				else
					struct_system->pitch_over_time[timeStep] = interpolator->blend(struct_system->nodePitch[i - 1].first, struct_system->nodePitch[i].first, (ratio - struct_system->nodePitch[i - 1].second) * (1 / (struct_system->nodePitch[i].second - struct_system->nodePitch[i - 1].second)));
				blended = true;
				break;
			}
		}

		if (blended)
			continue;

		if (struct_system->nodePitch.size() == 0)
			struct_system->pitch_over_time[timeStep] = interpolator->blend(pitchIn, pitchOut, ratio);
		else
			struct_system->pitch_over_time[timeStep] = interpolator->blend(struct_system->nodePitch.back().first, pitchOut, (ratio - struct_system->nodePitch.back().second) * (1 / (1 - struct_system->nodePitch.back().second)));
	}

	return true;
}

bool TCompEditorParticles::RollCurves(TCtesParticles* particles_buffer, system* struct_system) {
	bool changed = false;

	if (ImGui::TreeNode("RollModifierCurve"))
	{
		ImGui::Separator();
		changed |= ImGui::DragFloat("Roll Modifier In", &struct_system->roll_over_time[0], 0.1f, -180.0f, 180.0f);

		ImGui::Separator();
		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.9f, 0.6f, 0.9f, 0.8f));
		if (ImGui::Button("Add Node"))
			struct_system->nodeRoll.push_back(std::pair(1.0, 0.5));
		ImGui::PopStyleColor();
		ImGui::Separator();
		for (int j = 0; j < struct_system->nodeRoll.size(); j++)
		{
			char title[64];
			sprintf(title, "Node: %d", j);
			ImGui::Text(title);
			ImGui::SameLine();
			ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(1.0f, 0.00f, 0.0f, 1.00f));

			char titleDel[64];
			sprintf(titleDel, "Del Node: %d", j);
			if (ImGui::Button(titleDel)) {
				struct_system->nodeRoll.erase(struct_system->nodeRoll.begin() + j);
				ImGui::PopStyleColor();
				break;
			}
			ImGui::PopStyleColor();
			ImGui::PushItemWidth(100);

			char titleSize[64];
			sprintf(titleSize, "Roll Modifier Node: %d", j);
			changed |= ImGui::DragFloat(titleSize, &struct_system->nodeRoll[j].first, 0.1f, -180.0f, 180.0f);

			ImGui::SameLine();
			char titleRatio[64];
			sprintf(titleRatio, "Ratio Node: %d", j);
			if (struct_system->nodeSize.size() == 0 || j == 0)
				changed |= ImGui::DragFloat(titleRatio, &struct_system->nodeRoll[j].second, 0.01f, 0.0f, 1.0f);
			else
				changed |= ImGui::DragFloat(titleRatio, &struct_system->nodeRoll[j].second, 0.01f, struct_system->nodeRoll[struct_system->nodeRoll.size() - 2].second, 1.0f);

			if (j > 0 && struct_system->nodeRoll[j].second < struct_system->nodeRoll[j - 1].second)
				struct_system->nodeRoll[j].second = struct_system->nodeRoll[j - 1].second;

			ImGui::PopItemWidth();
			ImGui::Separator();
		}
		changed |= ImGui::DragFloat("Roll Modifier Out", &struct_system->roll_over_time[7], 0.1f, -180.0f, 180.0f);
		ImGui::Separator();

		float rollIn = struct_system->roll_over_time[0];
		float rollOut = struct_system->roll_over_time[7];


		ImGui::Combo("Curve Type", (int*)&num_curve_roll, curve_type_str);
		changed |= SingleRollCuve(curveInterpolator[num_curve_roll].first, curveInterpolator[num_curve_roll].second, rollIn, rollOut, particles_buffer, struct_system, num_curve_roll);

		ImGui::TreePop();
	}

	return changed;
}

bool TCompEditorParticles::SingleRollCuve(const char* name, Interpolator::IInterpolator* interpolator, float rollIn, float rollOut, TCtesParticles* particles_buffer, system* struct_system, int num) {
	char applyTxt[128];
	strcpy(applyTxt, "Apply ");
	strcat(applyTxt, name);

	bool changed = false;

	renderInterpolator(name, interpolator);
	bool auxReference = struct_system->rollCurve[num];
	ImGui::SameLine();
	ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.92f, 0.30f, 0.87f, 0.8f));
	if (ImGui::Checkbox(applyTxt, &auxReference) || struct_system->rollCurve[num]) {
		std::fill(begin(struct_system->rollCurve), end(struct_system->rollCurve), false);
		struct_system->rollCurve[num] = auxReference;
		changed |= ApplyRollCurve(rollIn, rollOut, interpolator, particles_buffer, struct_system);
	}
	ImGui::PopStyleColor();

	return changed;
}

bool TCompEditorParticles::ApplyRollCurve(float rollIn, float rollOut, Interpolator::IInterpolator* interpolator, TCtesParticles* particles_buffer, system* struct_system) {

	bool blended = false;
	for (int timeStep = 0; timeStep < MAX_QUATS; timeStep++) {
		blended = false;

		float ratio = (1. / 7.) * timeStep;
		for (int i = 0; i < struct_system->nodeRoll.size(); i++) {
			if (ratio < struct_system->nodeRoll[i].second) {
				if (i == 0)
					struct_system->roll_over_time[timeStep] = interpolator->blend(rollIn, struct_system->nodeRoll[i].first, ratio * (1 / struct_system->nodeRoll[i].second));
				else
					struct_system->roll_over_time[timeStep] = interpolator->blend(struct_system->nodeRoll[i - 1].first, struct_system->nodeRoll[i].first, (ratio - struct_system->nodeRoll[i - 1].second) * (1 / (struct_system->nodeRoll[i].second - struct_system->nodeRoll[i - 1].second)));
				blended = true;
				break;
			}
		}

		if (blended)
			continue;

		if (struct_system->nodeRoll.size() == 0)
			struct_system->roll_over_time[timeStep] = interpolator->blend(rollIn, rollOut, ratio);
		else
			struct_system->roll_over_time[timeStep] = interpolator->blend(struct_system->nodeRoll.back().first, rollOut, (ratio - struct_system->nodeRoll.back().second) * (1 / (1 - struct_system->nodeRoll.back().second)));
	}

	return true;
}


bool TCompEditorParticles::YawCurves2(TCtesParticles* particles_buffer, system* struct_system) {
	bool changed = false;

	if (ImGui::TreeNode("YawModifierCurves"))
	{
		ImGui::Separator();
		changed |= ImGui::DragFloat("Yaw Modifier In", &struct_system->yaw_over_time2[0], 0.1f, -180.0f, 180.0f);

		ImGui::Separator();
		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.9f, 0.6f, 0.9f, 0.8f));
		if (ImGui::Button("Add Node"))
			struct_system->nodeYaw2.push_back(std::pair(1.0, 0.5));
		ImGui::PopStyleColor();
		ImGui::Separator();
		for (int j = 0; j < struct_system->nodeYaw2.size(); j++)
		{
			char title[64];
			sprintf(title, "Node: %d", j);
			ImGui::Text(title);
			ImGui::SameLine();
			ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(1.0f, 0.00f, 0.0f, 1.00f));

			char titleDel[64];
			sprintf(titleDel, "Del Node: %d", j);
			if (ImGui::Button(titleDel)) {
				struct_system->nodeYaw2.erase(struct_system->nodeYaw2.begin() + j);
				ImGui::PopStyleColor();
				break;
			}
			ImGui::PopStyleColor();

			ImGui::PushItemWidth(100);
			char titleSize[64];
			sprintf(titleSize, "Yaw Modifier Node: %d", j);
			changed |= ImGui::DragFloat(titleSize, &struct_system->nodeYaw2[j].first, 0.1f, -180.0f, 180.0f);

			ImGui::SameLine();

			char titleRatio[64];
			sprintf(titleRatio, "Ratio Node: %d", j);
			if (struct_system->nodeSize.size() == 0 || j == 0)
				changed |= ImGui::DragFloat(titleRatio, &struct_system->nodeYaw2[j].second, 0.01f, 0.0f, 1.0f);
			else
				changed |= ImGui::DragFloat(titleRatio, &struct_system->nodeYaw2[j].second, 0.01f, struct_system->nodeYaw2[struct_system->nodeYaw2.size() - 2].second, 1.0f);

			if (j > 0 && struct_system->nodeYaw2[j].second < struct_system->nodeYaw2[j - 1].second)
				struct_system->nodeYaw2[j].second = struct_system->nodeYaw2[j - 1].second;

			ImGui::PopItemWidth();
			ImGui::Separator();
		}
		changed |= ImGui::DragFloat("Yaw Modifier Out", &struct_system->yaw_over_time2[7], 0.1f, -180.0f, 180.0f);
		ImGui::Separator();

		float yawIn = struct_system->yaw_over_time2[0];
		float yawOut = struct_system->yaw_over_time2[7];


		ImGui::Combo("Curve Type", (int*)&num_curve_yaw2, curve_type_str);
		changed |= SingleYawCuve2(curveInterpolator[num_curve_yaw2].first, curveInterpolator[num_curve_yaw2].second, yawIn, yawOut, particles_buffer, struct_system, num_curve_yaw2);

		ImGui::TreePop();
	}

	return changed;
}

bool TCompEditorParticles::SingleYawCuve2(const char* name, Interpolator::IInterpolator* interpolator, float yawIn, float yawOut, TCtesParticles* particles_buffer, system* struct_system, int num) {
	char applyTxt[128];
	strcpy(applyTxt, "Apply ");
	strcat(applyTxt, name);

	bool changed = false;

	renderInterpolator(name, interpolator);
	bool auxReference = struct_system->yawCurve2[num];
	ImGui::SameLine();
	ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.92f, 0.30f, 0.87f, 0.8f));
	if (ImGui::Checkbox(applyTxt, &auxReference) || struct_system->yawCurve2[num]) {
		std::fill(begin(struct_system->yawCurve2), end(struct_system->yawCurve2), false);
		struct_system->yawCurve2[num] = auxReference;
		changed |= ApplyYawCurve2(yawIn, yawOut, interpolator, particles_buffer, struct_system);
	}
	ImGui::PopStyleColor();

	return changed;
}

bool TCompEditorParticles::ApplyYawCurve2(float yawIn, float yawOut, Interpolator::IInterpolator* interpolator, TCtesParticles* particles_buffer, system* struct_system) {

	bool blended = false;
	for (int timeStep = 0; timeStep < MAX_QUATS; timeStep++) {
		blended = false;

		float ratio = (1. / 7.) * timeStep;
		for (int i = 0; i < struct_system->nodeYaw2.size(); i++) {
			if (ratio < struct_system->nodeYaw2[i].second) {
				if (i == 0)
					struct_system->yaw_over_time2[timeStep] = interpolator->blend(yawIn, struct_system->nodeYaw2[i].first, ratio * (1 / struct_system->nodeYaw2[i].second));
				else
					struct_system->yaw_over_time2[timeStep] = interpolator->blend(struct_system->nodeYaw2[i - 1].first, struct_system->nodeYaw2[i].first, (ratio - struct_system->nodeYaw2[i - 1].second) * (1 / (struct_system->nodeYaw2[i].second - struct_system->nodeYaw2[i - 1].second)));
				blended = true;
				break;
			}
		}

		if (blended)
			continue;

		if (struct_system->nodeYaw2.size() == 0)
			struct_system->yaw_over_time2[timeStep] = interpolator->blend(yawIn, yawOut, ratio);
		else
			struct_system->yaw_over_time2[timeStep] = interpolator->blend(struct_system->nodeYaw2.back().first, yawOut, (ratio - struct_system->nodeYaw2.back().second) * (1 / (1 - struct_system->nodeYaw2.back().second)));
	}

	return true;
}

bool TCompEditorParticles::PitchCurves2(TCtesParticles* particles_buffer, system* struct_system) {
	bool changed = false;

	if (ImGui::TreeNode("PitchModifierCurve"))
	{
		ImGui::Separator();
		float max_pitch = 90.0f - 1e-3f;
		changed |= ImGui::DragFloat("Pitch Modifier In", &struct_system->pitch_over_time2[0], 0.1f, -max_pitch, -max_pitch);

		ImGui::Separator();
		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.9f, 0.6f, 0.9f, 0.8f));
		if (ImGui::Button("Add Node"))
			struct_system->nodePitch2.push_back(std::pair(1.0, 0.5));
		ImGui::PopStyleColor();
		ImGui::Separator();
		for (int j = 0; j < struct_system->nodePitch2.size(); j++)
		{
			char title[64];
			sprintf(title, "Node: %d", j);
			ImGui::Text(title);
			ImGui::SameLine();
			ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(1.0f, 0.00f, 0.0f, 1.00f));

			char titleDel[64];
			sprintf(titleDel, "Del Node: %d", j);
			if (ImGui::Button(titleDel)) {
				struct_system->nodePitch2.erase(struct_system->nodePitch2.begin() + j);
				ImGui::PopStyleColor();
				break;
			}
			ImGui::PopStyleColor();

			ImGui::PushItemWidth(100);

			char titleSize[64];
			sprintf(titleSize, "Pitch Modifier Node: %d", j);
			changed |= ImGui::DragFloat(titleSize, &struct_system->nodePitch2[j].first, 0.1f, -max_pitch, -max_pitch);

			ImGui::SameLine();

			char titleRatio[64];
			sprintf(titleRatio, "Ratio Node: %d", j);
			if (struct_system->nodeSize.size() == 0 || j == 0)
				changed |= ImGui::DragFloat(titleRatio, &struct_system->nodePitch2[j].second, 0.01f, 0.0f, 1.0f);
			else
				changed |= ImGui::DragFloat(titleRatio, &struct_system->nodePitch2[j].second, 0.01f, struct_system->nodePitch2[struct_system->nodePitch2.size() - 2].second, 1.0f);

			if (j > 0 && struct_system->nodePitch2[j].second < struct_system->nodePitch2[j - 1].second)
				struct_system->nodePitch2[j].second = struct_system->nodePitch2[j - 1].second;

			ImGui::PopItemWidth();
			ImGui::Separator();
		}
		changed |= ImGui::DragFloat("Pitch Modifier Out", &struct_system->pitch_over_time2[7], 0.1f, -max_pitch, -max_pitch);
		ImGui::Separator();

		float pitchIn = struct_system->pitch_over_time2[0];
		float pitchOut = struct_system->pitch_over_time2[7];

		ImGui::Combo("Curve Type", (int*)&num_curve_pitch2, curve_type_str);
		changed |= SinglePitchCuve2(curveInterpolator[num_curve_pitch2].first, curveInterpolator[num_curve_pitch2].second, pitchIn, pitchOut, particles_buffer, struct_system, num_curve_pitch2);

		ImGui::TreePop();
	}

	return changed;
}

bool TCompEditorParticles::SinglePitchCuve2(const char* name, Interpolator::IInterpolator* interpolator, float pitchIn, float pitchOut, TCtesParticles* particles_buffer, system* struct_system, int num) {
	char applyTxt[128];
	strcpy(applyTxt, "Apply ");
	strcat(applyTxt, name);

	bool changed = false;

	renderInterpolator(name, interpolator);
	bool auxReference = struct_system->pitchCurve2[num];
	ImGui::SameLine();
	ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.92f, 0.30f, 0.87f, 0.8f));
	if (ImGui::Checkbox(applyTxt, &auxReference) || struct_system->pitchCurve2[num]) {
		std::fill(begin(struct_system->pitchCurve2), end(struct_system->pitchCurve2), false);
		struct_system->pitchCurve2[num] = auxReference;
		changed |= ApplyPitchCurve2(pitchIn, pitchOut, interpolator, particles_buffer, struct_system);
	}
	ImGui::PopStyleColor();

	return changed;
}

bool TCompEditorParticles::ApplyPitchCurve2(float pitchIn, float pitchOut, Interpolator::IInterpolator* interpolator, TCtesParticles* particles_buffer, system* struct_system) {

	bool blended = false;
	for (int timeStep = 0; timeStep < MAX_QUATS; timeStep++) {
		blended = false;

		float ratio = (1. / 7.) * timeStep;
		for (int i = 0; i < struct_system->nodePitch2.size(); i++) {
			if (ratio < struct_system->nodePitch2[i].second) {
				if (i == 0)
					struct_system->pitch_over_time2[timeStep] = interpolator->blend(pitchIn, struct_system->nodePitch2[i].first, ratio * (1 / struct_system->nodePitch2[i].second));
				else
					struct_system->pitch_over_time2[timeStep] = interpolator->blend(struct_system->nodePitch2[i - 1].first, struct_system->nodePitch2[i].first, (ratio - struct_system->nodePitch2[i - 1].second) * (1 / (struct_system->nodePitch2[i].second - struct_system->nodePitch2[i - 1].second)));
				blended = true;
				break;
			}
		}

		if (blended)
			continue;

		if (struct_system->nodePitch2.size() == 0)
			struct_system->pitch_over_time2[timeStep] = interpolator->blend(pitchIn, pitchOut, ratio);
		else
			struct_system->pitch_over_time2[timeStep] = interpolator->blend(struct_system->nodePitch2.back().first, pitchOut, (ratio - struct_system->nodePitch2.back().second) * (1 / (1 - struct_system->nodePitch2.back().second)));
	}

	return true;
}

bool TCompEditorParticles::RollCurves2(TCtesParticles* particles_buffer, system* struct_system) {
	bool changed = false;

	if (ImGui::TreeNode("RollModifierCurve"))
	{
		ImGui::Separator();
		changed |= ImGui::DragFloat("Roll Modifier In", &struct_system->roll_over_time2[0], 0.1f, -180.0f, 180.0f);

		ImGui::Separator();
		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.9f, 0.6f, 0.9f, 0.8f));
		if (ImGui::Button("Add Node"))
			struct_system->nodeRoll2.push_back(std::pair(1.0, 0.5));
		ImGui::PopStyleColor();
		ImGui::Separator();
		for (int j = 0; j < struct_system->nodeRoll2.size(); j++)
		{
			char title[64];
			sprintf(title, "Node: %d", j);
			ImGui::Text(title);
			ImGui::SameLine();
			ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(1.0f, 0.00f, 0.0f, 1.00f));

			char titleDel[64];
			sprintf(titleDel, "Del Node: %d", j);
			if (ImGui::Button(titleDel)) {
				struct_system->nodeRoll2.erase(struct_system->nodeRoll2.begin() + j);
				ImGui::PopStyleColor();
				break;
			}
			ImGui::PopStyleColor();
			ImGui::PushItemWidth(100);

			char titleSize[64];
			sprintf(titleSize, "Roll Modifier Node: %d", j);
			changed |= ImGui::DragFloat(titleSize, &struct_system->nodeRoll2[j].first, 0.1f, -180.0f, 180.0f);

			ImGui::SameLine();

			char titleRatio[64];
			sprintf(titleRatio, "Ratio Node: %d", j);
			if (struct_system->nodeSize.size() == 0 || j == 0)
				changed |= ImGui::DragFloat(titleRatio, &struct_system->nodeRoll2[j].second, 0.01f, 0.0f, 1.0f);
			else
				changed |= ImGui::DragFloat(titleRatio, &struct_system->nodeRoll2[j].second, 0.01f, struct_system->nodeRoll2[struct_system->nodeRoll2.size() - 2].second, 1.0f);

			if (j > 0 && struct_system->nodeRoll2[j].second < struct_system->nodeRoll2[j - 1].second)
				struct_system->nodeRoll2[j].second = struct_system->nodeRoll2[j - 1].second;

			ImGui::PopItemWidth();
			ImGui::Separator();
		}
		changed |= ImGui::DragFloat("Roll Modifier Out", &struct_system->roll_over_time2[7], 0.1f, -180.0f, 180.0f);
		ImGui::Separator();

		float rollIn = struct_system->roll_over_time2[0];
		float rollOut = struct_system->roll_over_time2[7];

		ImGui::Combo("Curve Type", (int*)&num_curve_roll2, curve_type_str);
		changed |= SingleRollCuve2(curveInterpolator[num_curve_roll2].first, curveInterpolator[num_curve_roll2].second, rollIn, rollOut, particles_buffer, struct_system, num_curve_roll2);

		ImGui::TreePop();
	}

	return changed;
}

bool TCompEditorParticles::SingleRollCuve2(const char* name, Interpolator::IInterpolator* interpolator, float rollIn, float rollOut, TCtesParticles* particles_buffer, system* struct_system, int num) {
	char applyTxt[128];
	strcpy(applyTxt, "Apply ");
	strcat(applyTxt, name);

	bool changed = false;

	renderInterpolator(name, interpolator);
	bool auxReference = struct_system->rollCurve2[num];
	ImGui::SameLine();
	ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.92f, 0.30f, 0.87f, 0.8f));
	if (ImGui::Checkbox(applyTxt, &auxReference) || struct_system->rollCurve2[num]) {
		std::fill(begin(struct_system->rollCurve2), end(struct_system->rollCurve2), false);
		struct_system->rollCurve2[num] = auxReference;
		changed |= ApplyRollCurve2(rollIn, rollOut, interpolator, particles_buffer, struct_system);
	}
	ImGui::PopStyleColor();

	return changed;
}

bool TCompEditorParticles::ApplyRollCurve2(float rollIn, float rollOut, Interpolator::IInterpolator* interpolator, TCtesParticles* particles_buffer, system* struct_system) {

	bool blended = false;
	for (int timeStep = 0; timeStep < MAX_QUATS; timeStep++) {
		blended = false;

		float ratio = (1. / 7.) * timeStep;
		for (int i = 0; i < struct_system->nodeRoll2.size(); i++) {
			if (ratio < struct_system->nodeRoll2[i].second) {
				if (i == 0)
					struct_system->roll_over_time2[timeStep] = interpolator->blend(rollIn, struct_system->nodeRoll2[i].first, ratio * (1 / struct_system->nodeRoll2[i].second));
				else
					struct_system->roll_over_time2[timeStep] = interpolator->blend(struct_system->nodeRoll2[i - 1].first, struct_system->nodeRoll2[i].first, (ratio - struct_system->nodeRoll2[i - 1].second) * (1 / (struct_system->nodeRoll2[i].second - struct_system->nodeRoll2[i - 1].second)));
				blended = true;
				break;
			}
		}

		if (blended)
			continue;

		if (struct_system->nodeRoll2.size() == 0)
			struct_system->roll_over_time2[timeStep] = interpolator->blend(rollIn, rollOut, ratio);
		else
			struct_system->roll_over_time2[timeStep] = interpolator->blend(struct_system->nodeRoll2.back().first, rollOut, (ratio - struct_system->nodeRoll2.back().second) * (1 / (1 - struct_system->nodeRoll2.back().second)));
	}

	return true;
}


void TCompEditorParticles::renderInterpolator(const char* name, Interpolator::IInterpolator* interpolator)
{
	const int nsamples = 8;
	float values[nsamples];
	for (int i = 0; i < nsamples; ++i)
	{
		values[i] = interpolator->blend(0.f, 1.f, (float)i / (float)nsamples);
	}
	ImGui::PlotLines(name, values, nsamples, 0, 0,
		std::numeric_limits<float>::min(), std::numeric_limits<float>::max(),
		ImVec2(150, 80));
}

