#pragma once

#include "modules/module.h"
#include "particles/particle.h"
#include "components/common/comp_transform.h"
#include "render/shaders/cte_buffer.h"

class CModuleParticles : public IModule {
	struct SParticleSystem {
		CHandle particleSystem;
		float lifeTime;
	};

	std::vector<SParticleSystem> v_SParticleSystem;

public:
	CModuleParticles(const std::string& name);

	bool start() override;
	void stop() override;
	void update(float delta) override;
	void renderDebug() override;
	void renderInMenu() override;

	//Create a Non-logic system particles that will be destroyed when its timelife ends
	std::vector<CHandle> CreateOneShootSystemParticles(const char* particle_system, TCompTransform* c_trans, VEC3 pos = VEC3(0, 0, 0));
  //Create a Non-logic system particles that will be destroyed when its timelife ends
  std::vector<CHandle> CreateOneShootSystemParticles(const char* particle_system, CTransform &c_trans, VEC3 pos = VEC3(0, 0, 0));
	//Create a system particles with logic managed in another component
	std::vector<CHandle> CreateLogicSeparatedSystemParticles(const char* particle_system, TCompTransform* c_trans, VEC3 pos = VEC3(0, 0, 0));
	//Create a system particles with logic managed in another component
	std::vector<CHandle> CreateLogicSeparatedSystemParticles(const char* particle_system, CTransform &c_trans, VEC3 pos = VEC3(0, 0, 0));
	//Update Constant Buffer
	void UpdateConstantBuffer(CCteBufferBase* cte_buffer);
	//Put the system particles in a buffer to be destroyed as soon as posible
	void DestroySystem(CHandle handle);
	void RegisterToDestroyParticles(std::vector<CHandle> v_hParticles);


private:
	//Register a system that will be destroyed when its timelife ends.
	//Check if a timelife systems ends
	void CheckDestroySystems(float deltaT);
};