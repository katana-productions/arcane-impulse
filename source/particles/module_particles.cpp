#include "mcv_platform.h"
#include "particles/module_particles.h"
#include "particles/particle_system.h"
#include "particles/particle_emitter.h"
#include "render/meshes/mesh_instanced.h"
#include "components/common/comp_render.h"
#include "components/common/comp_name.h"
#include "render/textures/material.h"
#include "engine.h"


CModuleParticles::CModuleParticles(const std::string& name)
	: IModule(name)
{}


bool CModuleParticles::start() {
	return true;
}

void CModuleParticles::stop() {
}

void CModuleParticles::update(float delta) {
	CheckDestroySystems(delta);
}

//Create a Non-logic system particles that will be destroyed when its timelife ends
std::vector<CHandle> CModuleParticles::CreateOneShootSystemParticles(const char* particle_system, TCompTransform* c_trans, VEC3 pos) {
	std::vector<CHandle> particle_systems;
	particle_systems = EngineBasics.CreateGroup(particle_system, c_trans, pos);
	RegisterToDestroyParticles(particle_systems);
	return particle_systems;
}

//Create a Non-logic system particles that will be destroyed when its timelife ends
std::vector<CHandle> CModuleParticles::CreateOneShootSystemParticles(const char* particle_system, CTransform &c_trans, VEC3 pos) {
  std::vector<CHandle> particle_systems;
  particle_systems = EngineBasics.CreateGroup(particle_system, c_trans, pos);
  RegisterToDestroyParticles(particle_systems);
  return particle_systems;
}

//Create a system particles with logic managed in another component
std::vector<CHandle> CModuleParticles::CreateLogicSeparatedSystemParticles(const char* particle_system, TCompTransform* c_trans, VEC3 pos) {
	return EngineBasics.CreateGroup(particle_system, c_trans, pos);
}

//Create a system particles with logic managed in another component
std::vector<CHandle> CModuleParticles::CreateLogicSeparatedSystemParticles(const char* particle_system, CTransform &c_trans, VEC3 pos) {
	return EngineBasics.CreateGroup(particle_system, c_trans, pos);
}

//Update a priority buffer 
void CModuleParticles::UpdateConstantBuffer(CCteBufferBase* cte_buffer) {
	cte_buffer->updateGPU();
}

//Put the system particles in a buffer to be destroyed as soon as posible
void CModuleParticles::DestroySystem(CHandle handle) {
	handle.destroy();
}

//Register a system that will be destroyed when its timelife ends.
void CModuleParticles::RegisterToDestroyParticles(std::vector<CHandle> v_hParticles) {
	for (auto h_loaded : v_hParticles) {
		CEntity* e_loaded = h_loaded;
		if (!e_loaded) return;
		TCompBuffers* c_buffer = e_loaded->get<TCompBuffers>();
		if (!c_buffer) continue;
		auto cte_buffer = c_buffer->getCteByName("TCtesParticles");
		TCtesParticles* particles_buffer = dynamic_cast<TCtesParticles*>(cte_buffer);

		SParticleSystem spsystem;
		spsystem.particleSystem = h_loaded;
		spsystem.lifeTime = 2.0f;

		v_SParticleSystem.push_back(spsystem);
	}
}

void CModuleParticles::CheckDestroySystems(float deltaT) {
	int numSystem = 0;
	for (auto& spsystem : v_SParticleSystem) {

		CHandle h_loaded = spsystem.particleSystem;
		if (!h_loaded.isValid())
			continue;

		spsystem.lifeTime -= deltaT;


		if (spsystem.lifeTime < 0) {
			DestroySystem(h_loaded);
			v_SParticleSystem.erase(v_SParticleSystem.begin() + numSystem);
			break;
		}
		numSystem++;
	}
}


void CModuleParticles::renderDebug() {}

void CModuleParticles::renderInMenu() {

}
