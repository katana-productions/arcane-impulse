#pragma once

#include "components/common/comp_base.h"
#include "entity/entity.h"
#include "entity/common_msgs.h"
#include "components/common/comp_buffers.h"

class TCompEditorParticles : public TCompBase
{
	enum Enum
	{
		eCone = 0,
		eSphere = 1,
		eCube = 2,

		eGEOMETRY_COUNT,	//!< internal use only!
		eINVALID = -1		//!< internal use only!
	};

	enum RandomType
	{
		eSimpleValue = 0,
		eRandom2Values = 1,
		eSimpleCurve = 2,
		eRandom2Curves = 3,

		eRandomINVALID = -1		//!< internal use only!
	};

	struct system {
		CHandle system_particles;

		std::vector<bool> colorCurve;
		std::vector<bool> sizeCurve;
		std::vector<bool> speedCurve;

		std::vector<bool> yawCurve;
		std::vector<bool> pitchCurve;
		std::vector<bool> rollCurve;

		std::vector<bool> yawCurve2;
		std::vector<bool> pitchCurve2;
		std::vector<bool> rollCurve2;

		std::vector<std::pair<float, float>> nodeYaw;
		std::vector<std::pair<float, float>> nodePitch;
		std::vector<std::pair<float, float>> nodeRoll;

		std::vector<std::pair<float, float>> nodeYaw2;
		std::vector<std::pair<float, float>> nodePitch2;
		std::vector<std::pair<float, float>> nodeRoll2;

		std::vector<std::pair<VEC4, float>> nodeColor;
		std::vector<std::pair<float, float>> nodeSize;
		std::vector<std::pair<float, float>> nodeSpeed;

		VEC2 yaw;
		VEC2 pitch;
		VEC2 roll;

		std::vector<float> yaw_over_time;
		std::vector<float> pitch_over_time;
		std::vector<float> roll_over_time;

		std::vector<float> yaw_over_time2;
		std::vector<float> pitch_over_time2;
		std::vector<float> roll_over_time2;

		int random_over_time_type;
		int max_num_elems;
	};



	std::vector<std::pair<const char*, Interpolator::IInterpolator*>> curveInterpolator;


	std::vector<system> vSystems_particles;
	int MAX_COLORS = 8;
	int MAX_SIZES = 8;
	int MAX_SPEEDS = 8;
	int MAX_QUATS = 8;

	std::vector<json> jLastSystem;
	DECL_SIBLING_ACCESS();
	void onSystemCreated(const TMsgParticleEditorInfo& msg);
	void onAlarmRecieve(const TMsgAlarmClock& msg);

public:
	void load(const json& j, TEntityParseContext& ctx);
	void debugInMenu();
	void update(float dt);
	static void registerMsgs();

private:
	void InstanciateSystemParticles();
	void ResetTimers();
	bool MenuEditSingleSystem(CHandle h_system, system* struct_system, TCompBuffers* c_buffer);

	bool ChangeMesh(CHandle h_system, const char* newMeshName, TCompBuffers* c_buffer);
	bool ChangeMaterial(CHandle h_system, const char* newMatName, TCompBuffers* c_buffer);

	void ChangeName(CHandle h, const char* name);

	std::string ShowFiles(const char* path);
	void LoadSystem(const char* fileName);
	void LoadRotationValues(system* struct_system, json jSystem);
	void LoadCurveValues(system* struct_system, json jSystem);
	void LoadMaxNumElems(system* struct_system, json jSystem);

	bool CheckIfExists(const char* name);
	void SaveAllSystems(const char* name);
	void SaveSingleSystem(const char* name, CHandle h_system, system* struct_system);

	void SaveNewMaterial(json& jEntity, CHandle h_system);
	void SaveNewMesh(json& jEntity, CHandle h_system);
	void ApplyNewBuffer(json& jBuffer, CHandle h_system, TCompBuffers* c_buffer);
	void ApplyMaxParticles();

	void SaveNewBuffer(json& jBuffer, CHandle h_system);

	void ParseSingleEntity(json& jEntity, TCtesParticles* particles_buffer, system* struct_system);

	bool ColorCurves(TCtesParticles* particles_buffer, system* struct_system);
	bool SingleColorCuve(const char* name, Interpolator::IInterpolator* interpolator, VEC4 colorIn, VEC4 colorOut, TCtesParticles* particles_buffer, system* struct_system, int num);
	bool ApplyColorCurve(VEC4 colorIn, VEC4 colorOut, Interpolator::IInterpolator* interpolator, TCtesParticles* particles_buffer, system* struct_system);

	bool SizeCurves(TCtesParticles* particles_buffer, system* struct_system);
	bool SingleSizeCuve(const char* name, Interpolator::IInterpolator* interpolator, float sizeOut, float valueOut, TCtesParticles* particles_buffer, system* struct_system, int num);
	bool ApplySizeCurve(float sizeIn, float sizeOut, Interpolator::IInterpolator* interpolator, TCtesParticles* particles_buffer, system* struct_system);


	bool SpeedCurves(TCtesParticles* particles_buffer, system* struct_system);
	bool SingleSpeedCuve(const char* name, Interpolator::IInterpolator* interpolator, float speedIn, float speedOut, TCtesParticles* particles_buffer, system* struct_system, int num);
	bool ApplySpeedCurve(float speedIn, float speedOut, Interpolator::IInterpolator* interpolator, TCtesParticles* particles_buffer, system* struct_system);


	bool StartRotation(TCtesParticles* particles_buffer, system* struct_system);
	bool SimpleRotation(TCtesParticles* particles_buffer, system* struct_system);
	bool RandomBetweenTwoSimpleRotations(TCtesParticles* particles_buffer, system* struct_system);
	bool SimpleCurveRotation(TCtesParticles* particles_buffer, system* struct_system);
	bool RandomBetweenTwoCurves(TCtesParticles* particles_buffer, system* struct_system);

	bool YawCurves(TCtesParticles* particles_buffer, system* struct_system);
	bool SingleYawCuve(const char* name, Interpolator::IInterpolator* interpolator, float yawIn, float yawOut, TCtesParticles* particles_buffer, system* struct_system, int num);
	bool ApplyYawCurve(float yawIn, float yawOut, Interpolator::IInterpolator* interpolator, TCtesParticles* particles_buffer, system* struct_system);

	bool PitchCurves(TCtesParticles* particles_buffer, system* struct_system);
	bool SinglePitchCuve(const char* name, Interpolator::IInterpolator* interpolator, float pitchIn, float pitchOut, TCtesParticles* particles_buffer, system* struct_system, int num);
	bool ApplyPitchCurve(float pitchIn, float pitchOut, Interpolator::IInterpolator* interpolator, TCtesParticles* particles_buffer, system* struct_system);

	bool RollCurves(TCtesParticles* particles_buffer, system* struct_system);
	bool SingleRollCuve(const char* name, Interpolator::IInterpolator* interpolator, float rollIn, float rollOut, TCtesParticles* particles_buffer, system* struct_system, int num);
	bool ApplyRollCurve(float rollIn, float rollOut, Interpolator::IInterpolator* interpolator, TCtesParticles* particles_buffer, system* struct_system);

	bool YawCurves2(TCtesParticles* particles_buffer, system* struct_system);
	bool SingleYawCuve2(const char* name, Interpolator::IInterpolator* interpolator, float yawIn, float yawOut, TCtesParticles* particles_buffer, system* struct_system, int num);
	bool ApplyYawCurve2(float yawIn, float yawOut, Interpolator::IInterpolator* interpolator, TCtesParticles* particles_buffer, system* struct_system);

	bool PitchCurves2(TCtesParticles* particles_buffer, system* struct_system);
	bool SinglePitchCuve2(const char* name, Interpolator::IInterpolator* interpolator, float pitchIn, float pitchOut, TCtesParticles* particles_buffer, system* struct_system, int num);
	bool ApplyPitchCurve2(float pitchIn, float pitchOut, Interpolator::IInterpolator* interpolator, TCtesParticles* particles_buffer, system* struct_system);

	bool RollCurves2(TCtesParticles* particles_buffer, system* struct_system);
	bool SingleRollCuve2(const char* name, Interpolator::IInterpolator* interpolator, float rollIn, float rollOut, TCtesParticles* particles_buffer, system* struct_system, int num);
	bool ApplyRollCurve2(float rollIn, float rollOut, Interpolator::IInterpolator* interpolator, TCtesParticles* particles_buffer, system* struct_system);

	void renderInterpolator(const char* name, Interpolator::IInterpolator* interpolator);

	VEC4 StringToVec4(std::string value) {
		VEC4 v;
		sscanf(value.c_str(), "%f %f %f %f", &v.x, &v.y, &v.z, &v.w);
		return v;
	}

	const char* curve_type_str = "Linear\0"
							 	"QuadIn\0"
							 	"QuadOut\0"
							 	"QuadInOut\0"
							 
							 	"CubicIn\0"
							 	"CubicOut\0"
							 	"CubicInOut\0"
							 
							 	"QuartIn\0"
							 	"QuartOut\0"
							 	"QuartInOut\0"
							 
							 	"QuintIn\0"
							 	"QuintOut\0"
							 	"QuintInOut\0"
							 
							 	"BackIn\0"
							 	"BackOut\0"
							 	"BackInOut\0"
							 
							 
							 	"ElasticIn\0"
							 	"ElasticOut\0"
							 	"ElasticInOut\0"
							 
							 	"BounceIn\0"
							 	"BounceOut\0"
							 	"BounceInOut\0"
							 
							 
							 	"CircularIn\0"
							 	"CircularOut\0"
							 	"CircularInOut\0"
							 
							 	"ExpoIn\0"
							 	"ExpoOut\0"
							 	"ExpoInOut\0"
							 
							 	"SineIn\0"
							 	"SineOut\0"
							 	"SineInOut\0"
								"\0";

	int num_curve_yaw = 0;
	int num_curve_pitch = 0;
	int num_curve_roll = 0;
	int num_curve_yaw2 = 0;
	int num_curve_pitch2 = 0;
	int num_curve_roll2 = 0;

	int num_curve_color = 0;
	int num_curve_size = 0;
	int num_curve_velocity = 0;

	Interpolator::TLinearInterpolator		interLinear;

	Interpolator::TQuadInInterpolator		interQuadIn;
	Interpolator::TQuadOutInterpolator		interQuadOut;
	Interpolator::TQuadInOutInterpolator	interQuadInOut;

	Interpolator::TCubicInInterpolator		interCubicIn;
	Interpolator::TCubicOutInterpolator		interCubicOut;
	Interpolator::TCubicInOutInterpolator	interCubicInOut;

	Interpolator::TQuartInInterpolator		interQuartIn;
	Interpolator::TQuartOutInterpolator		interQuartOut;
	Interpolator::TQuartInOutInterpolator	interQuartInOut;

	Interpolator::TQuintInInterpolator		interQuintIn;
	Interpolator::TQuintOutInterpolator		interQuintOut;
	Interpolator::TQuintInOutInterpolator	interQuintInOut;

	Interpolator::TBackInInterpolator		interBackIn;
	Interpolator::TBackOutInterpolator		interBackOut;
	Interpolator::TBackInOutInterpolator	interBackInOut;

	Interpolator::TElasticInInterpolator		interElasticIn;
	Interpolator::TElasticOutInterpolator		interElasticOut;
	Interpolator::TElasticInOutInterpolator		interElasticInOut;

	Interpolator::TBounceInInterpolator			interBounceIn;
	Interpolator::TBounceOutInterpolator		interBounceOut;
	Interpolator::TBounceInOutInterpolator		interBounceInOut;

	Interpolator::TCircularInInterpolator		interCircularIn;
	Interpolator::TCircularOutInterpolator		interCircularOut;
	Interpolator::TCircularInOutInterpolator	interCircularInOut;

	Interpolator::TExpoInInterpolator			interExpoIn;
	Interpolator::TExpoOutInterpolator			interExpoOut;
	Interpolator::TExpoInOutInterpolator		interExpoInOut;

	Interpolator::TSineInInterpolator			interSineIn;
	Interpolator::TSineOutInterpolator			interSineOut;
	Interpolator::TSineInOutInterpolator		interSineInOut;

};