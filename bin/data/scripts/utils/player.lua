SLB.using(SLB)

------- PLAYER -------

function immortal()
	Logic.immortal()
end

function setImmortal(b)
	Logic.setImmortal(b)
end

function infMagic()
	Logic.infMagic()
end

function setInputEnabled(b)
	Logic.setInputEnabled(b)
end

function activateMagic()
	Logic.activateMagic()
end

function deactivateMagic()
	Logic.deactivateMagic()
end

function teleportPlayer(x, y, z, fx, fy, fz)
	v = VEC3(x, y, z)
	r = VEC3(fx, fy, fz)
	Logic.teleportPlayer(v, r)
end

function damagePlayer(dmg)
	Logic.damagePlayer(dmg)
end

function healPlayer(hp)
	Logic.healPlayer(hp)
end

function setCameraDocked(b)
	Logic.setCameraDocked(b)
end

function toggleCameraDocked()
	Logic.toggleCameraDocked()
end

function getCameraPos()
	return Logic.getCameraPos()
end

function setRandomCamShake(intensity,duration,roughness)
	Logic.setRandomCamShake(intensity,duration,roughness)
end

function setHandsLockCamera(b)
	Logic.setLeftHandLockCamera(b)
	Logic.setRightHandLockCamera(b)
end

function getLeftHandPos()
	return Logic.getLeftHandPos()
end

function setLeftHandPos(x,y,z)
	p = VEC3(x,y,z)
	Logic.setLeftHandPos(p)
end

function getLeftHandFront()
	return Logic.getLeftHandFront()
end

function getRightHandPos()
	return Logic.getRightHandPos()
end

function setRightHandPos(x,y,z)
	p = VEC3(x,y,z)
	Logic.setLeftHandPos(p)
end

function getRightHandFront()
	return Logic.getRightHandFront()
end

function fadeInToBlack(duration)
	Logic.fadeInToBlack(duration)
end

function fadeOutToBlack(duration)
	Logic.fadeOutToBlack(duration)
end

function fadeInToWhite(duration)
	Logic.fadeInToWhite(duration)
end

function fadeOutToWhite(duration)
	Logic.fadeOutToWhite(duration)
end

function setGlovesRandGlow(b)
	Logic.setGlovesRandGlow(b)
end

function handsInitDeath(duration)
	local lateralOffsetL = 0.145
	local frontOffsetL = 0.05
	local upOffsetL = -0.4
	local extraPitchL = -0.6

	local lateralOffsetR = 0.14
	local frontOffsetR = 0.05
	local upOffsetR = -0.42
	local extraPitchR = -0.6

	local camFront = getEntityFront("Player")
	local camLeft = getEntityLeft("Player")
	local camUp = getEntityUp("Player")
	local camPos = getEntityPos("Camera")

	moveToPosInTime("LeftHand", camPos.x + (camFront.x * frontOffsetL) + (camUp.x * upOffsetL) + (camLeft.x * lateralOffsetL), 
								camPos.y + (camFront.y * frontOffsetL) + (camUp.y * upOffsetL) + (camLeft.y * lateralOffsetL),
								camPos.z + (camFront.z * frontOffsetL) + (camUp.z * upOffsetL) + (camLeft.z * lateralOffsetL),
								duration)
	moveToPosInTime("RightHand", camPos.x + (camFront.x * frontOffsetR) + (camUp.x * upOffsetR) + (camLeft.x * lateralOffsetR), 
								camPos.y + (camFront.y * frontOffsetR) + (camUp.y * upOffsetR) + (camLeft.y * lateralOffsetR),
								camPos.z + (camFront.z * frontOffsetR) + (camUp.z * upOffsetR) + (camLeft.z * lateralOffsetR),
								duration)

	local plAngles = getEntityAngles("Player")

	--this works too
	local newAnglesL = VEC3(plAngles.x, extraPitchL, 0)
	local newAnglesR = VEC3(plAngles.x, extraPitchR, 0)
	setEntityAngles("LeftHand", newAnglesL)
	setEntityAngles("RightHand", newAnglesR)
end

function blockPlayer()
	Logic.blockPlayer()
end

function unlockPlayer()
	Logic.unlockPlayer()
end

function setPlayerVisible(b)
	Logic.setPlayerVisible(b)
end

function stopPlayerAnimations()
	Logic.stopPlayerAnimations()
end

----------------------
--left
 --       "frontOffset": 0.1,
  --      "lateralOffset": 0.145,
   --     "extraHeight": -0.41,
    --    "extraPitch": 0.6

--RightHand
 --       "frontOffset": 0.1,
  --      "lateralOffset": 0.14,
   --     "extraHeight": -0.42,
    --    "extraPitch": 0.6