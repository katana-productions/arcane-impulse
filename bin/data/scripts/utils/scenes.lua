SLB.using(SLB)

------ SCENES --------

function loadScenesPersistance(scene)
	Logic.loadScenesPersistance(scene)
end

function loadStartEventCoroutine(scene)
	fadeInToBlack(0.2)
	lerpImageAlpha("ui_in_game_black_foreground", 1.0, 0.2)
	blockPlayer()
	waitTime(0.4)
	setInputEnabled(false)
	pausePhysx(true)
	loadScenesPersistance(scene)
end

function loadFinishEventCoroutine()

end

function loadStartEvent(scene)
	startSysCoroutine("loadStartEvent", loadStartEventCoroutine, scene)
end

function loadFinishEvent()
	--startSysCoroutine("loadFinishEvent", loadFinishEventCoroutine)
	pausePhysx(false)
	setInputEnabled(true)
	unlockPlayer()
	lerpImageAlpha("ui_in_game_black_foreground", 0.0, 0.2)
	fadeOutToBlack(0.5)
end

function pausePhysx(b)
	Logic.pausePhysx(b)
end

function loadSceneCoroutine(filename)
	local entitiesPerFrame = 100
	local skipFrames = 7
	local size = Logic.getJsonSize(filename)
	local frame = 0
	local i = 0
	local skip = false
	Logic.setCommonCtx()

	while i < size do
		if (skip == false) then
			Logic.partialLoadEntities(filename,i,entitiesPerFrame)
			i = i + entitiesPerFrame
			skip = true
		else
			if (frame == (skipFrames - 1)) then
				skip = false
				frame = 0
			else
				coroutine.yield()
				frame = frame + 1
			end
		end
	end
end

function unloadSceneCoroutine(filename)
	local entitiesPerFrame = 100
	local skipFrames = 2
	local size = Logic.getJsonSize(filename)
	local frame = 0
	local i = 0
	local skip = false
	Logic.setCommonCtx()

	while i < size do
		if (skip == false) then
			Logic.partialUnloadEntities(filename,i,entitiesPerFrame)
			i = i + entitiesPerFrame
			skip = true
		else
			if (frame == (skipFrames - 1)) then
				skip = false
				frame = 0
			else
				coroutine.yield()
				frame = frame + 1
			end
		end
	end
end

function loadSceneNoBlock(filename)
	startSysCoroutine(filename, loadSceneCoroutine, filename)
end

function loadSceneBlock(filename)
	return Logic.loadSceneBlock(filename)
end

function unloadSceneNoBlock(filename)
	startSysCoroutine(filename, unloadSceneCoroutine, filename)
end

function unloadScene(filename)
	return Logic.unloadScene(filename)
end

----------------------
