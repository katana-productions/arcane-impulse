SLB.using(SLB)

------ ENEMIES ------

function setCinematicControl(n,c)
	Logic.setCinematicControl(n,c)
end

function setEnemieViewPlayer(n,c)
	Logic.setEnemieViewPlayer(n,c)
end

function moveToPositionCinematic(n, x, y, z)
	p = VEC3(x, y, z)
	Logic.moveToPositionCinematic(n,p)
end

function setBerserkMode(n,p)
	Logic.setBerserkMode(n,p)
end


----------------------