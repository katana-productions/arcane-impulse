SLB.using(SLB)

----- CINEMATICS ------
function launchCinematic(s)
	Logic.launchCinematic(s)
end

function launchCinematicDeath()
	Logic.launchCinematicDeath()
end

function setMainMenuCamera(b)
	Logic.setMainMenuCamera(b)
end

----------------------