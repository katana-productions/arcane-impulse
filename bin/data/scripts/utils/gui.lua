SLB.using(SLB)

-------- GUI ---------
function showUIMessage(s)
	Logic.showUIMessage(s)
end

function hideUIMessage(s)
	Logic.hideUIMessage(s)
end

function hideUIinGame()
end

function showUIinGame()
end

function setUIScrollSpeed(s, x, y)
	Logic.setUIScrollSpeed(s, x, y)
end

function setInitImageUV(s)
	Logic.setInitImageUV(s)
end

function setImageAlpha(s, x)
	Logic.setImageAlpha(s, x)
end

function lerpImageAlpha(s, x, y)
	Logic.lerpImageAlpha(s, x, y)
end

function lerpImageBurnAmount(s, x, y)
	Logic.lerpImageBurnAmount(s, x, y)
end

function lerpUIScrollSpeed(s, x, y, z)
	Logic.lerpUIScrollSpeed(s, x, y, z)
end

function enabledTriggerTutorial(s, x)
	Logic.enabledTriggerTutorial(s, x)
end

function stateWidget(s, x)
	Logic.stateWidget(s, x)
end

----------------------