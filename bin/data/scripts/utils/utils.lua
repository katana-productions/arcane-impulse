SLB.using(SLB)

-------- UTILS -------

function write(s)
	Logic.write(s)
end

function VEC3tostring(vec)
	return Logic.VEC3tostring(vec)
end

function execScriptDelayed(script, time)
	Logic.execScriptDelayed(script, time);
end

function execSysScriptDelayed(script, time)
	Logic.execSysScriptDelayed(script, time)
end

function execScript(script)
	Logic.execScript(script)
end

function distance3D(origin, dest)
	return math.sqrt(math.pow(dest.x - origin.x, 2) + math.pow(dest.y - origin.y, 2) + math.pow(dest.z - origin.z, 2))
end

function diirection3D(origin, dest)
	local dist = distance3D(origin, dest)
	local dir = VEC3((dest.x - origin.x) / dist, (dest.y - origin.y) / dist, (dest.z - origin.z) / dist)
	return dir
end

function deg2rad(deg)
	return deg * math.pi / 180
end

function rad2deg(rad)
	return rad * 180 / math.pi
end

function getEntityPos(ent)
	return Logic.getEntityPos(ent)
end

function setEntityPos(ent, pos)
	Logic.setEntityPos(ent, pos)
end

function getEntityAngles(ent)
	return Logic.getEntityAngles(ent)
end

function setEntityAngles(ent, angles)
	Logic.setEntityAngles(ent, angles)
end

function getEntityFront(ent)
	return Logic.getEntityFront(ent)
end

function getEntityLeft(ent)
	return Logic.getEntityLeft(ent)
end

function getEntityUp(ent)
	return Logic.getEntityUp(ent)
end

function moveToPosInTime(ent, x,y,z, duration)
	local s = "moveToPosInTime" .. ent
	startCoroutine(s, moveToPosInTimeCoroutine, ent, x,y,z, duration)
end

function moveToPosInTimeOrigin(ent, x,y,z, duration)
	local s = "moveToPosInTimeOrigin" .. ent
	local origin = getEntityPos(ent)
	startCoroutine(s, moveToPosInTimeCoroutine, ent, origin.x + x, origin.y + y, origin.z + z, duration)
end


function moveToPosInTimeCoroutine(ent, x,y,z, duration)
	local pos = VEC3(x,y,z)
	local origin = getEntityPos(ent)
	local timer = 0
	while timer < duration do
		local f = timer / duration
		local newPos = VEC3(origin.x + (pos.x - origin.x) * f,  origin.y + (pos.y - origin.y) * f, origin.z + (pos.z - origin.z) * f)
		setEntityPos(ent, newPos)
		local delta = coroutine.yield()
		timer = timer + delta 
	end
end

function moveToPosInTimeRelative(ent, x,y,z, front,left,up, duration)
	local s = "moveToPosInTimeOrigin" .. ent
	startCoroutine(s, moveToPosInTimeRelativeCoroutine, ent, x,y,z, front,left,up, duration)
end

function moveToPosInTimeRelativeCoroutine(ent, x,y,z, front,left,up, duration)
	local origin = getEntityPos(ent)
	local dest = VEC3(origin.x + (x * left.x) + (y * up.x) + (z * front.x), 
					origin.y + (x * left.y) + (y * up.y) + (z * front.y), 
					origin.z + (x * left.z) + (y * up.z) + (z * front.z))
	local displ = VEC3(dest.x - origin.x, dest.y - origin.y, dest.z - origin.z)
	local timer = 0
	while timer < duration do
		local f = timer / duration
		local newPos = VEC3(origin.x + displ.x * f,  origin.y + displ.y * f, origin.z + displ.z * f)
		setEntityPos(ent, newPos)
		local delta = coroutine.yield()
		timer = timer + delta 
	end
end

function rotateInTime(ent, y,p,r, duration)
	local s = "rotateInTime" .. ent
	startCoroutine(s, rotateInTimeCoroutine, ent, y, p, r, duration)
end

function rotateInTimeOrigin(ent, y,p,r, duration)
	local s = "rotateInTimeOrigin" .. ent
	local angles = getEntityAngles(ent)
	startCoroutine(s, rotateInTimeCoroutine, ent, angles.x + y, angles.y + p, angles.z + r, duration)
end

function rotateInTimeCoroutine(ent, y,p,r, duration)
	local final = VEC3(y,p,r)
	local angles = getEntityAngles(ent)
	local timer = 0
	while timer < duration do
		local f = timer / duration
		local newAngles = VEC3(angles.x + (final.x - angles.x) * f, angles.y + (final.y - angles.y) * f, angles.z + (final.z - angles.z) * f)
		setEntityAngles(ent, newAngles)
		local delta = coroutine.yield()
		timer = timer + delta 
	end
end

function instantiateTrigger(ent)
	Logic.instantiateTrigger(ent)
end

function destroyEntity(n)
	Logic.destroyEntity(n)
end
----------------------
