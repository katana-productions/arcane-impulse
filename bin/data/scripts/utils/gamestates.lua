SLB.using(SLB)

----- GAMESTATES -----

function pause()
	Logic.pause()
end

function changeGameState(s)
	Logic.changeGameState(s)
end

function resetGame()
	Logic.resetGame()
end
----------------------