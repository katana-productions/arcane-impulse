function enterTitleGame()
	setMainMenuCamera(true)
	waitTime(2)
	lerpImageAlpha("title_game_black_foreground", 0, 3)
	waitTime(3)
end

function titleGameToMainMenu()
	changeGameState("gs_main_menu")
	waitTime(2)
	setImageAlpha("title_game_black_foreground", 1)
end

function enterMainMenu()
	lerpImageAlpha("bt_start", 1, 3)
	lerpImageAlpha("bt_exit", 1, 3)
	waitTime(3)
end

function mainMenuToIntro()
	lerpImageAlpha("main_menu_black_foreground", 1, 3)
	waitTime(3)
	changeGameState("gs_intro")
	setMainMenuCamera(false)
	waitTime(1)
	setImageAlpha("main_menu_black_foreground" , 0)
	setImageAlpha("bt_start", 0)
	setImageAlpha("bt_exit", 0)
end