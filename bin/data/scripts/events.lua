SLB.using(SLB)

-------- EVENTS -------

function setOpenDoor(d,o)
	Logic.setOpenDoor(d, o)
end

--function playDialogue(a,s,w)
--	showUIMessage(s)
--	playSoundEvent(a)
--	waitTime(w+0.5)
--	hideUIMessage(s)
--	waitTime(0.5)
--end

function launchEvent(num)
	local s = "launchEvent" .. num
	local f

	-- Add new events here
	if num == 0 then
		f = introScene
	elseif num == 1 then
		f = launchFirstCinematic
	elseif num == 2 then
		f = activatePowers
	elseif num == 3 then
		f = launchArena1Event1
	elseif num == 4 then
		f = launchArena1Event2_1
	elseif num == 5 then
		f = launchArena1Event3
	elseif num == 6 then
		f = loadFinalCrystal
	elseif num == 7 then
		f = playerCinematicDeath
	elseif num == 8 then
		f = playerDeathEnd
	elseif num == 9 then
		f = finalGame
	elseif num == 10 then
		f = creditsScene
	elseif num == 11 then
		f = mainMenuToIntro
	elseif num == 12 then
		f = preIntroScene
	elseif num == 13 then
		f = enterMainMenuScene
	elseif num == 14 then
		f = playerTrap
	elseif num == 15 then
		f = enterTitleGameScene
	elseif num == 16 then
		f = titleGameToMainMenu
	elseif num == 17 then
		f = launchPuzzleEvent
	elseif num == 18 then
		f = launchBridgeCinematic
	elseif num == 19 then
		f = launchInstantiateTrigger
	elseif num == 20 then
		f = firstMagesEvent
	elseif num == 21 then
		f = finalRoomAggro
	elseif num == 22 then
		f = launchArena1EventLastEnemies
	elseif num == 23 then
		f = destroyParticleTutorialPull
	elseif num == 24 then
		f = doubleRoomDisableEnemies
	elseif num == 25 then
		f = enableAndamiosGoblins
	elseif num == 26 then
		f = openFinalDoor
	end

	startCoroutine(s, f) 
end


function introScene()
	introDialogue()
end

function enterMainMenuScene()
	enterMainMenu()
end

function enterTitleGameScene()
	enterTitleGame()
end

function titleGameToMainMenu()
	titleGameToMainMenu()
end

function mainMenuToIntro()
	mainMenuToIntro()
end

function preIntroScene()
	preIntro()
end

function finalGame()
	changeGameState("gs_credits")
end

function creditsScene()
	creditsLogic()
end

function launchFirstCinematic()
	launchCinematic("firstCinematic")
	PlayDialogue(5)
end

function activatePowers()
	activateMagic()
	local platPos = VEC3(-18.5, 27.25, -0.7)
	local platAngles = VEC3(0, 0, 0)
	launchParticleAtTransform("data/particles/prefabs_particles/tutorialPull.json", platPos, platAngles)
end

function destroyParticleTutorialPull()
	destroyEntity("TutorialPullCenter")
	destroyEntity("TutorialPullEmitter")
	destroyEntity("TutorialPullPower_Emission_Right")
end

function launchArena1Event1()
	setCinematicControl("Cst_Goblin_000", true)
	launchCinematic("door_arena1")
	waitTime(3)
	setOpenDoor("Cst_Gate_Enemies_0", true)
	moveToPositionCinematic("Cst_Goblin_000",66,38,63)
	waitTime(2)
    setEnemieViewPlayer("Cst_Goblin_000", true)
	setOpenDoor("Cst_Gate_Enemies_0", false)
    setCinematicControl("Cst_Goblin_000", false)
	playFirstGoblin()
end

function launchArena1Event2_1()
	setOpenDoor("Cst_Gate_Enemies_1", true)	
	setOpenDoor("Cst_Gate_Enemies_2", true)	
	waitTime(1)
	setBerserkMode("Cst_Goblin_001", true)
    setBerserkMode("Cst_Goblin_002", true)
	setBerserkMode("Cst_Goblin_003", true)
    setBerserkMode("Cst_Goblin_004", true)
end

function launchArena1EventLastEnemies()
	setOpenDoor("Cst_Gate_Enemies_3", true)	
	setOpenDoor("Cst_Gate_Enemies_4", true)	
	waitTime(1)
	setBerserkMode("Cst_Goblin_005", true)
    setBerserkMode("Cst_Goblin_006", true)
    setBerserkMode("Cst_Goblin_007", true)
    setBerserkMode("Cst_Goblin_008", true)
	setBerserkMode("Cst_Goblin_009", true)
    setBerserkMode("Cst_Goblin_010", true)
end

function openFinalDoor()
	setOpenDoor("FinalDoor", true)	
end

function launchArena1Event3()
	fadeInToBlack(0.5)
	waitTime(0.5)
	setPlayerVisible(false);
	fadeOutToBlack(0.5)

	launchCinematic("firstArenaFinalCinematic")
	waitTime(0.5)
	setOpenDoor("Cst_Gate_Enemies_0", true)
	waitTime(2)

	fadeInToBlack(0.5)
	waitTime(0.5)
	setPlayerVisible(false);
	fadeOutToBlack(0.5)
	end

function launchPuzzleEvent()
	launchCinematic("door_puzzle")
	waitTime(4)
	playFirstPuzzle()
end

function launchArena2Event_()
	setCinematicControl("Gob1", true)
	launchCinematic("door_puzzle")
	teleportPlayer(-31,37,29,0,0,1)
	waitTime(3)
	setOpenDoor("door1", true)
	moveToPositionCinematic("Gob1",-21,37,65)
	waitTime(5)
    setEnemieViewPlayer("Gob1", true)
	waitTime(1)
    setCinematicControl("Gob1", false)
end

function launchArenaEvent1()
	setCinematicControl("Mage1", true)
	setCinematicControl("Mage2", true)
	launchCinematic("door_arena")
end

function launchArenaEvent2()
	setOpenDoor("door2", true)
	setOpenDoor("door3", true)
	moveToPositionCinematic("Mage1",26,173,175)
	moveToPositionCinematic("Mage2",26,173,175)
	waitTime(3)
	setCinematicControl("Mage1", false)
	setCinematicControl("Mage2", false)
end

function finalRoomCinematic()
    setCinematicControl("Gob5", true)
    setCinematicControl("Gob6", true)
    setCinematicControl("Gob7", true)
	launchCinematic("finalRoomCinematic")
	waitTime(10)
	setEnemieViewPlayer("Gob5", true)
    setEnemieViewPlayer("Gob6", true)
    setEnemieViewPlayer("Gob7", true)
	waitTime(2)
    setCinematicControl("Gob5", false)
    setCinematicControl("Gob6", false)
    setCinematicControl("Gob7", false)
end

function doubleRoomDisableEnemies()
	setCinematicControl("Mage", true)
    setCinematicControl("Mage001", true)

	setCinematicControl("Goblin1", true)
    setCinematicControl("Goblin002", true)
    setCinematicControl("Goblin003", true)
    setCinematicControl("Goblin004", true)
    setCinematicControl("Goblin005", true)
end


function enableAndamiosGoblins()
	setCinematicControl("Goblin1", false)
    setCinematicControl("Goblin002", false)
    setCinematicControl("Goblin003", false)
    setCinematicControl("Goblin004", false)
    setCinematicControl("Goblin005", false)
end


function firstMagesEvent()
	setImmortal(true)
    setCinematicControl("Mage", true)
    setCinematicControl("Mage001", true)
	launchCinematic("firstMagesCinematic")
	waitTime(10)
	setCinematicControl("Mage", false)
    setCinematicControl("Mage001", false)
	setBerserkMode("Mage", true)
	setBerserkMode("Mage001", true)
	setImmortal(false)
end

function finalRoomAggro()
	setBerserkMode("Mage006", true)
	setBerserkMode("Mage004", true)
	setBerserkMode("Mage002", true)
	setBerserkMode("Mage003", true)

	setBerserkMode("Gob", true)
	setBerserkMode("Gob001", true)
	setBerserkMode("Gob002", true)
	setBerserkMode("Gob003", true)
	setBerserkMode("Gob004", true)
	setBerserkMode("Gob005", true)
end

function loadFinalCrystal()
	Logic.loadFinalMessage()
	waitTime(5)
	Logic.sendToMenu()
end

function playerCinematicDeath()

	--ignore hp logic
	setImmortal(true)
	--unlock hands from following camera
	setHandsLockCamera(false)
	--launch cinematic
	launchCinematicDeath()
	--start the fade to black, finish in x seconds
	fadeInToBlack(5)

	--put the hands in a neutral position (like the first camera of the cinematic). 
	--this is done because we have to not see the non-existant body of the character
	handsInitDeath(0.1)
	waitTime(0.2)

	--set the hand movements
	plFront = getEntityFront("Player")
	plLeft = getEntityLeft("Player")
	plUp = getEntityUp("Player")

	moveToPosInTimeRelative("LeftHand", 0, -0.1, -0.2, plFront, plLeft, plUp, 1)
	moveToPosInTimeRelative("RightHand", 0, -0.1, -0.2, plFront, plLeft, plUp, 1)
	rotateInTimeOrigin("LeftHand", deg2rad(30), deg2rad(30), deg2rad(-25), 1.5)
	rotateInTimeOrigin("RightHand", deg2rad(-20), deg2rad(20), deg2rad(10), 1.5)
	waitTime(1)

	moveToPosInTimeRelative("LeftHand", 0, -0.3, -0.2, plFront, plLeft, plUp, 0.6)
	moveToPosInTimeRelative("RightHand", 0, -0.3, -0.2, plFront, plLeft, plUp, 0.6)
	waitTime(0.6)

	moveToPosInTimeRelative("LeftHand", 0, -2, 0, plFront, plLeft, plUp, 0.9)
	moveToPosInTimeRelative("RightHand", 0, -2, 0, plFront, plLeft, plUp, 0.9)
	waitTime(0.9)

	waitTime(4)

	fadeOutToBlack(0)
	setHandsLockCamera(true)
	setImmortal(false)
end

function playerDeathEnd()
	
	for i=1,20,1 do
		waitTime(1)
		if (i == 20) then
			damagePlayer(4)
		else 
			damagePlayer(5)
		end
	end

	stateWidget("ui_in_game", false)

	--unlock hands from following camera
	setHandsLockCamera(false)
	--launch cinematic
	launchCinematicDeath()
	--start the fade to black, finish in x seconds
	fadeInToBlack(5)

	--put the hands in a neutral position (like the first camera of the cinematic). 
	--this is done because we have to not see the non-existant body of the character
	handsInitDeath(0.1)
	waitTime(0.2)

	--set the hand movements
	plFront = getEntityFront("Player")
	plLeft = getEntityLeft("Player")
	plUp = getEntityUp("Player")

	moveToPosInTimeRelative("LeftHand", 0, -0.1, -0.2, plFront, plLeft, plUp, 1)
	moveToPosInTimeRelative("RightHand", 0, -0.1, -0.2, plFront, plLeft, plUp, 1)
	rotateInTimeOrigin("LeftHand", deg2rad(30), deg2rad(30), deg2rad(-25), 1.5)
	rotateInTimeOrigin("RightHand", deg2rad(-20), deg2rad(20), deg2rad(10), 1.5)
	waitTime(1)

	moveToPosInTimeRelative("LeftHand", 0, -0.3, -0.2, plFront, plLeft, plUp, 0.6)
	moveToPosInTimeRelative("RightHand", 0, -0.3, -0.2, plFront, plLeft, plUp, 0.6)
	waitTime(0.6)

	moveToPosInTimeRelative("LeftHand", 0, -2, 0, plFront, plLeft, plUp, 0.9)
	moveToPosInTimeRelative("RightHand", 0, -2, 0, plFront, plLeft, plUp, 0.9)
	waitTime(0.9)

	waitTime(4)
	
	setInputEnabled(true)
	
	finalGame()
end

function playerTrap()
	stopPlayerAnimations()
	setInputEnabled(false)
	setRandomCamShake(1, 1.5, 1)
	Logic.playSFX("event:/Environment/BreakFloor", "TimedPlatform1")

	local platPos = getEntityPos("TimedPlatform1")
	local platAngles = getEntityAngles("TimedPlatform1")
	launchParticleAtTransform("data/particles/prefabs_particles/trap_smoke.json", platPos, platAngles)

	waitTime(1.5)
	setInputEnabled(true)
end

function launchBridgeCinematic()
	launchCinematic("bridgeCinematic")
	playBridge()
end

function launchInstantiateTrigger()
	setOpenDoor("FinalDoor",true)
	launchCinematic("finalDoorCinematic")
	setOpenDoor("FinalDoor",true)
	waitTime(0.5)
	instantiateTrigger("data/triggers/triggerEndDialogue.json")
end
----------------------
