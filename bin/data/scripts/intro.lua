function preIntro()
	waitTime(2)
	lerpImageAlpha("preintro1", 1, 2.5)
	waitTime(2.5)

	waitTime(3) --time Image 1

	lerpImageAlpha("preintro1", 0, 2.5)
	waitTime(2.5)

	lerpImageAlpha("preintro2", 1, 2.5)
	waitTime(2.5)

	waitTime(0.5)
	changeGameState("gs_pre_intro_charge")
	waitTime(0.1)
	blockPlayer()
	waitTime(5) --time Image 2, load game
	lerpImageAlpha("preintro2", 0, 2.5)
	waitTime(2.5)
	
	lerpImageAlpha("fmod", 1, 2.5)
	lerpImageAlpha("lua", 1, 2.5)
	lerpImageAlpha("physx", 1, 2.5)
	waitTime(2.5)

	waitTime(1) --time Splash

	lerpImageAlpha("fmod", 0, 2.5)
	lerpImageAlpha("lua", 0, 2.5)
	lerpImageAlpha("physx", 0, 2.5)	
	waitTime(2.5)

	changeGameState("gs_title_game")	
end

function introDialogue()
	enabledTriggerTutorial("TriggerTutorial1", false)
	setImageAlpha("background_life",0.0)
	setImageAlpha("life_bar_inGame",0.0)
	setImageAlpha("progress_back",0.0)
	setImageAlpha("tutorialMessage1",0.0)

	waitTime(2)
	PlayDialogue(0)
	-- TODO: Retrasar voz 6 segundos
	lerpImageAlpha("intro_background", 1, 3)
	waitTime(3) --wait to alpha 1
	waitTime(3) --wait before start

	waitTime(5.5) --time image 1

	lerpUIScrollSpeed("intro_background",  0.08, 0.0, 2.7)
	waitTime(2.7)
	lerpUIScrollSpeed("intro_background",  0.0, 0.0, 2.7)
	waitTime(2.7) 
	waitTime(8.0) --time image 2

	lerpUIScrollSpeed("intro_background",  0.08, 0.0, 2.85)
	waitTime(2.85)
	lerpUIScrollSpeed("intro_background",  0.0, 0.0, 2.85)
	waitTime(2.85) 
	waitTime(5.5) --time image 3

	lerpUIScrollSpeed("intro_background",  0.08, 0.0, 3.0)
	waitTime(3.0)
	lerpUIScrollSpeed("intro_background",  0.0, 0.0, 3.0)
	waitTime(3.0) 
	waitTime(8.5) --time image 4
	-- TODO: change Audio Burn
	lerpImageBurnAmount("intro_background", 1, 5.0)
	waitTime(5.0) --wait to burn amount

	changeGameState("gs_gameplay")
	fadeOutToBlack(10)
	waitTime(1.5)

	lerpImageAlpha("background_life", 1, 3)
	lerpImageAlpha("life_bar_inGame", 1, 3)
	lerpImageAlpha("progress_back", 1, 3)
	lerpImageAlpha("tutorialMessage1", 0.8, 3)
	waitTime(3)
	enabledTriggerTutorial("TriggerTutorial1", true)
	unlockPlayer()

	setInitImageUV("intro_background")
	setImageAlpha("intro_background", 0.0)
	lerpImageBurnAmount("intro_background", 0, 1)
end

function skipIntroDialogue()
	stopCoroutine("launchEvent0")
	skipDialogue()

	fadeInToBlack(0.1)	
	changeGameState("gs_gameplay")
	fadeOutToBlack(3)
	unlockPlayer()
	
	lerpImageAlpha("background_life", 1, 3)
	lerpImageAlpha("life_bar_inGame", 1, 3)
	lerpImageAlpha("progress_back", 1, 3)
	enabledTriggerTutorial("TriggerTutorial1", true)

	setInitImageUV("intro_background")
	setImageAlpha("intro_background", 0.0)
	lerpImageBurnAmount("intro_background", 0, 1)
end
