function creditsLogic()
	waitTime(3)
	lerpImageAlpha("credits_background", 1, 3)
	waitTime(3)
	waitTime(2) --time image 1 (ARKANUM)


	lerpUIScrollSpeed("credits_background", 0.0, 0.02, 1)
	waitTime(1)
	waitTime(5.5) --time scrolling down
	lerpUIScrollSpeed("credits_background", 0.0, 0.00, 1)
	waitTime(1)
	waitTime(1) --time image 2 (KATANA PRODUCTIONS)

	lerpUIScrollSpeed("credits_background", 0.0, 0.02, 1)
	waitTime(1)
	waitTime(3.85) --time scrolling down
	lerpUIScrollSpeed("credits_background", 0.0, 0.00, 1)
	waitTime(1)
	waitTime(3) --time image 3 (GROUP)


	lerpUIScrollSpeed("credits_background", 0.0, 0.015, 1)
	waitTime(1)
	waitTime(38.5) --time scrolling down all credits
	lerpUIScrollSpeed("credits_background", 0.0, 0.00, 1)
	waitTime(1)
	waitTime(2)

	lerpImageAlpha("credits_background", 0, 3)
	waitTime(3)

	resetGame()
	waitTime(2)
	setInitImageUV("credits_background")
end