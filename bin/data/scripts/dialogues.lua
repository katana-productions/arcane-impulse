SLB.using(SLB)

function activateSubs()
	Logic.activateSubs()
end

function deactivateSubs()
	Logic.deactivateSubs()
end

function stopVoices()
	Logic.stopVoices()
end

function skipDialogue()
	local s = "playDialogue"
	stopCoroutine(s)
	stopVoices()
	deactivateSubs()
	setGlovesRandGlow(false)
end

function setSubsText(text)
	Logic.setSubsText(text)
end

function setSubsLine(line)
	Logic.setSubsLine(line - 1)
end

function setSubsLanguage(lang)
	Logic.setSubsLanguage(lang)
end

function PlayDialogue(num)
	local s = "playDialogue"
	local f

	--stop previous dialogue if any
	skipDialogue()

	-- Add new events here
	if num == 0 then
		f = playLegendDialogue
	elseif num == 1 then
		f = playSewerBegin
	elseif num == 2 then
		f = playFall1
	elseif num == 3 then
		f = playFall2
	elseif num == 4 then
		f = playSewerEnd
	elseif num == 5 then
		f = playGloves1
	elseif num == 6 then
		f = playGloves2
	elseif num == 7 then
		f = playAfterPlanks
	elseif num == 8 then
		f = playVases
	elseif num == 9 then
		f = playAfterVases
	elseif num == 10 then
		f = playFirstPuzzle
	elseif num == 11 then
		f = playAfterFirstPuzzle
	elseif num == 12 then
		f = playFirstGoblin
	elseif num == 13 then
		f = playAfterFirstGoblin
	elseif num == 14 then
		f = playAfterArmory
	elseif num == 15 then
		f = playFallingDown
	elseif num == 16 then
		f = playAfterFallingDown
	elseif num == 17 then
		f = playSecondPuzzle
	elseif num == 18 then
		f = playHalfSecondPuzzle
	elseif num == 19 then
		f = playAfterSecondPuzzle
	elseif num == 20 then
		f = playBridge
	elseif num == 21 then
		f = playBridgeConvo
	elseif num == 22 then
		f = playAfterBridge
	elseif num == 23 then
		f = playFirstMage
	elseif num == 24 then
		f = playAfterFireball
	elseif num == 25 then
		f = playEnd
	elseif num == 26 then
		f = playSewer2
	elseif num == 27 then
		f = playSewer3
	elseif num == 28 then
		f = playSewer4
	elseif num == 29 then
		f = playBridgeConvo2
	elseif num == 30 then
		f = playBridgeConvo3
	end

	startCoroutine(s, f) 
end

-- legend storytelling
function playLegendDialogue()
	playSoundEvent("event:/Voices/Introduction")
	waitTime(6)
	activateSubs()
	setSubsLine(1)
	waitTime(7.2)
	setSubsLine(2)
	waitTime(9.2)
	setSubsLine(3)
	waitTime(5.6)
	setSubsLine(4)
	waitTime(10.1)
	setSubsLine(5)
	waitTime(4.1)
	setSubsLine(6)
	waitTime(11.9) -- legend ends here

	deactivateSubs()
	waitTime(1)
	activateSubs()

	setSubsLine(7) -- and that's why I'm here
	waitTime(5.9)
	deactivateSubs()
end

-- random tidbids inside the sewer 
function playSewerBegin()
	playSoundEvent("event:/Voices/Sewer")
	
	activateSubs()
	setSubsLine(8)
	waitTime(9.5)
	deactivateSubs()
end

function playSewer2()
	playSoundEvent("event:/Voices/Sewer2")
	
	activateSubs()
	setSubsLine(9)
	waitTime(6.3)
	deactivateSubs()
end

function playSewer3()
	playSoundEvent("event:/Voices/Sewer3")
	
	activateSubs()
	setSubsLine(10)
	waitTime(3.5)
	deactivateSubs()
end

function playSewer4()
	playSoundEvent("event:/Voices/Sewer4")
	
	activateSubs()
	setSubsLine(11)
	waitTime(3.4)
	deactivateSubs()
end

function playFall1()
	playSoundEvent("event:/Voices/FallSewerEvents2")
	activateSubs()
	setSubsLine(12)
	waitTime(2.6)
	deactivateSubs()
end

function playFall2()
	playSoundEvent("event:/Voices/FallSewerEvents")
	activateSubs()
	setSubsLine(13)
	waitTime(5)
	deactivateSubs()
end

--play at the end of the sewer/begining of castle
function playSewerEnd()
	playSoundEvent("event:/Voices/SewerEnd")
	activateSubs()
	setSubsLine(14)
	waitTime(1.3)
	deactivateSubs()
end

-- first part when the thief finds the gloves
-- dunno about random glow here
function playGloves1()
	playSoundEvent("event:/Voices/ConversationPreGloves")
	activateSubs()
	setSubsLine(15)
	waitTime(9.8)
	--setSubsLine(16)
	--waitTime(11)
	--setSubsLine(17)
	--waitTime(4.9)
	setGlovesRandGlow(true)
	setSubsLine(18)
	waitTime(12.2)
	setGlovesRandGlow(false)

	setSubsLine(19)
	waitTime(2.7)
	teleportPlayer(-87,21,-12.5,0,0,1)
	setGlovesRandGlow(true)
	setSubsLine(20)
	waitTime(7.7)
	setGlovesRandGlow(false)
	--setSubsLine(21)
	--waitTime(4.3)
	--setSubsLine(22)
	--waitTime(6.9)
	--setSubsLine(23)
	--waitTime(5.4)
	--setSubsLine(24)
	--waitTime(5.4)
	--setSubsLine(25)
	--waitTime(1.4)
	--setSubsLine(26)
	--waitTime(6.3)
	setSubsLine(27)
	waitTime(5.9)

	setGlovesRandGlow(true)
	setSubsLine(28)
	waitTime(4.2)
	setGlovesRandGlow(false)

    setSubsLine(29)
	waitTime(2.5)
	deactivateSubs()
	--instantiateTrigger("data/triggers/triggerGloves.json")
	--instantiate trigger here
end

-- second part, maybe just add these together
function playGloves2()
	--deactivate UI here
	fadeInToWhite(0.1)
	local partPos = VEC3(-43, 21.4, -14.2)
	local partAngles = VEC3(0, 0, 0)
	launchParticleAtTransform("data/particles/prefabs_particles/tutorialPull.json", partPos, partAngles)
	waitTime(1)

	fadeOutToWhite(1)
	destroyEntity("MagicCircle")
	destroyEntity("MagicOrbs")
	destroyEntity("ExpandingCircles")
	waitTime(1)

	playSoundEvent("event:/Voices/ConversationGloves")
	activateSubs()

	setSubsLine(30)
	waitTime(1.8)

	setGlovesRandGlow(true)
	setSubsLine(31)
	waitTime(6.5)
	setSubsLine(32)
	waitTime(10.8)
	setGlovesRandGlow(false)

	deactivateSubs()
	instantiateTrigger("data/triggers/triggerTutorialPull.json")
	--instantiateTrigger("data/triggers/triggerPlanksDialogue.json")
end

-- after the player pulls the planks
function playAfterPlanks()
	playSoundEvent("event:/Voices/ConversationPlank")
	activateSubs()

	setGlovesRandGlow(true)
	setSubsLine(33)
	waitTime(2)
	setGlovesRandGlow(false)

	setSubsLine(34)
	waitTime(8.2)
	deactivateSubs()
end

-- when the player enters the room with vases
function playVases()
	playSoundEvent("event:/Voices/ConversationVases")
	activateSubs()

	setGlovesRandGlow(true)
	setSubsLine(35)
	waitTime(5.8)
	setSubsLine(36)
	waitTime(6)
	setGlovesRandGlow(false)

	deactivateSubs()
end

-- after the player breaks the vases
function playAfterVases()
	playSoundEvent("event:/Voices/ConversationVasesBroken")
	activateSubs()

	--setGlovesRandGlow(true)
	--setSubsLine(37)
	--waitTime(5.1)
	--setGlovesRandGlow(false)

	--setSubsLine(38)
	--waitTime(2)

	--setGlovesRandGlow(true)
	--setSubsLine(39)
	--waitTime(2.5)
	--setGlovesRandGlow(false)

	--setSubsLine(40)
	--waitTime(4.8)

	setGlovesRandGlow(true)
	setSubsLine(41)
	waitTime(7)
	setGlovesRandGlow(false)

	deactivateSubs()
end

-- first puzzle
function playFirstPuzzle()
	playSoundEvent("event:/Voices/ConversationFirstPuzzle")
	activateSubs()
	setSubsLine(42)
	waitTime(3.2)

	setGlovesRandGlow(true)
	setSubsLine(43)
	waitTime(5)
	setGlovesRandGlow(false)

	setSubsLine(44)
	waitTime(2.2)
	deactivateSubs()
end

-- after first puzzle
function playAfterFirstPuzzle()
	playSoundEvent("event:/Voices/ConversationFirstPuzzleSolved")
	activateSubs()

	setGlovesRandGlow(true)
	setSubsLine(45)
	waitTime(2.1)
	setGlovesRandGlow(false)

	setSubsLine(46)
	waitTime(4.1)
	deactivateSubs()
end

-- first goblin
function playFirstGoblin()
	playSoundEvent("event:/Voices/ConversationArenaCinematic")
	activateSubs()

	setGlovesRandGlow(true)
	setSubsLine(47)
	waitTime(9)
	--setSubsLine(48)
	--waitTime(6.3)
	--setGlovesRandGlow(false)

	--setSubsLine(49)
	--waitTime(2)

	--setGlovesRandGlow(true)
	setSubsLine(50)
	waitTime(5.4)
	setGlovesRandGlow(false)

	deactivateSubs()
end

-- after first goblin
function playAfterFirstGoblin()
	playSoundEvent("event:/Voices/ConversationArenaGoblin")
	activateSubs()

	setGlovesRandGlow(true)
	setSubsLine(51)
	waitTime(3.8)
	--setSubsLine(52)
	--waitTime(9.8)
	setGlovesRandGlow(false)

	deactivateSubs()
end

-- after armory
function playAfterArmory()
	playSoundEvent("event:/Voices/ConversationEndArena")
	activateSubs()
	setSubsLine(53)
	waitTime(2.3)

	setGlovesRandGlow(true)
	setSubsLine(54)
	waitTime(1.6)
	setGlovesRandGlow(false)

	setSubsLine(55)
	waitTime(2)
	deactivateSubs()
end

-- falling down
function playFallingDown()
	playSoundEvent("event:/Voices/ConversationFalling")
	activateSubs()

	setGlovesRandGlow(true)
	setSubsLine(56) -- we need to update this audio, both screams separate
	waitTime(5)
	setGlovesRandGlow(false)

	deactivateSubs()
end

-- after falling down
function playAfterFallingDown()
	playSoundEvent("event:/Voices/ConversationFall")
	activateSubs()
	setSubsLine(57)
	waitTime(5.1)

	setGlovesRandGlow(true)
	setSubsLine(58)
	waitTime(10)
	--setGlovesRandGlow(false)

	--deactivateSubs()
	playSoundEvent("event:/Voices/ConversationPostFall")
	setSubsLine(59)
	waitTime(7.8)
	setGlovesRandGlow(false)

	setSubsLine(60)
	waitTime(5.6)
	deactivateSubs()
end

-- play second puzzle
function playSecondPuzzle()
	--playSoundEvent("event:/Voices/ConversationPostFall")
	--activateSubs()

	--setGlovesRandGlow(true)
	--setSubsLine(59)
	--waitTime(13)
	--setGlovesRandGlow(false)

	--setSubsLine(60)
	--waitTime(6)
	--deactivateSubs()
end

-- play second puzzle
function playHalfSecondPuzzle()
	playSoundEvent("event:/Voices/ConversationHalfPuzzle2Solved")
	activateSubs()

	setGlovesRandGlow(true)
	setSubsLine(61)
	waitTime(5.4)
	setGlovesRandGlow(false)

	setSubsLine(62)
	waitTime(3.7)
	
	setGlovesRandGlow(true)
	setSubsLine(63)
	waitTime(9.8)
	setGlovesRandGlow(false)

	setSubsLine(64)
	waitTime(3.5)

	setGlovesRandGlow(true)
	setSubsLine(65)
	waitTime(2)
	setGlovesRandGlow(false)

	deactivateSubs()
end

-- play after second puzzle
function playAfterSecondPuzzle()
	playSoundEvent("event:/Voices/ConversationPuzzle2Solved")
	activateSubs()

	setGlovesRandGlow(true)
	setSubsLine(67)
	waitTime(2.5)
	setGlovesRandGlow(false)

	setSubsLine(68)
	waitTime(1.9)

	setGlovesRandGlow(true)
	setSubsLine(69)
	waitTime(2.2)
	setGlovesRandGlow(false)
	
	deactivateSubs()
end

-- play bridge
function playBridge()
	playSoundEvent("event:/Voices/ConversationStartBridge")
	activateSubs()
	setSubsLine(70)
	waitTime(1.3)

	setGlovesRandGlow(true)
	setSubsLine(71)
	waitTime(3)
	setGlovesRandGlow(false)

	setSubsLine(72)
	waitTime(1.9)
	
	setGlovesRandGlow(true)
	setSubsLine(73)
	waitTime(3.1)
	setGlovesRandGlow(false)

	setSubsLine(74)
	waitTime(3)
	deactivateSubs()

	--waitTime(7.0)

	--activateSubs()
	--setSubsLine(75)
	--waitTime(2.5)

	--setGlovesRandGlow(true)
	--setSubsLine(76)
	--waitTime(5.3)
	--setGlovesRandGlow(false)

	--deactivateSubs()
	--waitTime(12.5)
	--activateSubs()

	--setGlovesRandGlow(true)
	--setSubsLine(77)
	--waitTime(4.7)
	--setGlovesRandGlow(false)

	--setSubsLine(78)
	--waitTime(2)

	--deactivateSubs()
	--waitTime(10.5)
	--activateSubs()

	--setGlovesRandGlow(true)
	--setSubsLine(79)
	--waitTime(3.6)
	--setGlovesRandGlow(false)

	--setSubsLine(80)
	--waitTime(2.1)
	--deactivateSubs()
end

-- play bridge convo
function playBridgeConvo()
	playSoundEvent("event:/Voices/ConversationHalfBridgeCrossed")
	activateSubs()
	setSubsLine(75)
	waitTime(2.5)

	setGlovesRandGlow(true)
	setSubsLine(76)
	waitTime(5.1)
	setGlovesRandGlow(false)

	deactivateSubs()
end

function playBridgeConvo2()
	playSoundEvent("event:/Voices/ConversationHalfBridgeCrossed2")
	
	activateSubs()
	setGlovesRandGlow(true)
	setSubsLine(77)
	waitTime(4.7)
	setGlovesRandGlow(false)

	setSubsLine(78)
	waitTime(2)
	deactivateSubs()
end

function playBridgeConvo3()
	playSoundEvent("event:/Voices/ConversationHalfBridgeCrossed3")
	
	activateSubs()
	setGlovesRandGlow(true)
	setSubsLine(79)
	waitTime(3.6)
	setGlovesRandGlow(false)

	setSubsLine(80)
	waitTime(2)
	deactivateSubs()
end

-- play after bridge
function playAfterBridge()
	playSoundEvent("event:/Voices/ConversationEndBridge")
	activateSubs()
	setSubsLine(81)
	waitTime(1.6)

	setGlovesRandGlow(true)
	setSubsLine(82)
	waitTime(4.4)
	setGlovesRandGlow(false)
	deactivateSubs()
end

-- play first mage
function playFirstMage()
	playSoundEvent("event:/Voices/ConversationIntroMage")
	activateSubs()
	setGlovesRandGlow(true)
	setSubsLine(83)
	waitTime(13.5)
	setGlovesRandGlow(false)
	deactivateSubs()
end

-- play after fireball
function playAfterFireball()
	playSoundEvent("event:/Voices/ConversationCombatMage")
	activateSubs()
	setSubsLine(84)
	waitTime(4.4)

	setGlovesRandGlow(true)
	setSubsLine(85)
	waitTime(9.4)
	setGlovesRandGlow(false)
	deactivateSubs()
end

-- play after fireball
function playEnd()
	playSoundEvent("event:/Voices/ConversationEndGame")
	activateSubs()
	blockPlayer()
	stopPlayerAnimations()

	setGlovesRandGlow(true)
	setSubsLine(86)
	waitTime(9.7)
	setGlovesRandGlow(false)

	--setSubsLine(87)
	--waitTime(7)

	setGlovesRandGlow(true)
	setSubsLine(88)
	waitTime(6.4)
	setGlovesRandGlow(false)

	setSubsLine(89)
	waitTime(1.7)

	setGlovesRandGlow(true)
	--setSubsLine(90)
	--waitTime(10.2)

	setSubsLine(91)
	waitTime(7.2)

	launchEvent(8)

	setSubsLine(92)
	waitTime(13)

	setSubsLine(93)
	waitTime(7.8)
	setGlovesRandGlow(false)
	deactivateSubs()
end
