all_co = {}
all_sys_co = {}

function clearCoroutines()
	all_co = {}
	all_sys_co = {}
end

---- SYS COROUTINES ----
function startSysCoroutine(name, func, ...)
	local co = coroutine.create(func)
	coroutine.resume(co, ...)

	all_sys_co[name] = co
end

function stopSysCoroutine(name)
	all_sys_co[name] = nil
end

function updateSysCoroutines(...)
	local name, co = next(all_sys_co)
	while name ~= nil do
		coroutine.resume(co, ...)

		local status = coroutine.status(co)
		if status == "dead" then
			print("Removing sys coroutine " .. name)
			all_sys_co[name] = nil
		end

		name, co = next(all_sys_co, name)
	end
end

function stopSysCoroutine(name)
	all_sys_co[name] = nil
end

---- COROUTINES ----
function startCoroutine(name, func, ...)
	local co = coroutine.create(func)
	coroutine.resume(co, ...)

	all_co[name] = co
end

function stopCoroutine(name)
	all_co[name] = nil
end

function updateCoroutines(...)
	local name, co = next(all_co)
	while name ~= nil do
		coroutine.resume(co, ...)
		local status = coroutine.status(co)
		if status == "dead" then
			print("Removing coroutine " .. name)
			all_co[name] = nil
		end
		name, co = next(all_co, name)
	end
end

function waitTime(duration)
	local timer = 0
	while timer < duration do
		local delta = coroutine.yield()
		timer = timer + delta
	end
end

function waitCondition(conditionFunc, ...)
	local timer = 0
	while conditionFunc(...) == false do
		local delta = coroutine.yield()
		timer = timer + delta
	end
	return timer
end

--------------------------------
print("coroutines ready")
