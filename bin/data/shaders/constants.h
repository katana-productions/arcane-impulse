#include "platform.h"

#define CTE_BUFFER_SLOT_CAMERAS     0
#define CTE_BUFFER_SLOT_OBJECT      1
#define CTE_BUFFER_SLOT_SHARED      2
#define CTE_BUFFER_SLOT_DEBUG_LINE  3
#define CTE_BUFFER_SLOT_SKIN_BONES  4
#define CTE_BUFFER_SLOT_LIGHT       5
#define CTE_BUFFER_SLOT_BLUR        6
#define CTE_BUFFER_SLOT_FOCUS       7
#define CTE_BUFFER_SLOT_BLOOM       8
#define CTE_BUFFER_SLOT_MATERIAL    9
#define CTE_BUFFER_SLOT_COMP_BUFFERS 10
#define CTE_BUFFER_SLOT_POSTFX       11
#define CTE_BUFFER_SLOT_PARTICLES   12
#define CTE_BUFFER_SLOT_SHADERFX    13


// Texture Slots
// Materials
#define TS_ALBEDO            0
#define TS_NORMAL            1
#define TS_SPECULAR          2
#define TS_EMISSIVE          3

#define TS_PROJECTOR         4
#define TS_LIGHT_SHADOW_MAP  5
#define TS_ENVIRONMENT_MAP   6
#define TS_IRRADIANCE_MAP    7

//--------------------------------------
#define TS_DEFERRED_ALBEDOS        8
#define TS_DEFERRED_NORMALS        9
#define TS_DEFERRED_LINEAR_DEPTH  10
#define TS_DEFERRED_EMISSIVE      11
#define TS_DEFERRED_ACC_LIGHTS    12
#define TS_DEFERRED_AO            13
#define TS_DEFERRED_OUTLINES      14
#define TS_DEFERRED_DISTORSIONS   15
#define TS_DEFERRED_OUTLINEMASK   16

#define TS_NOISE_MAP          17

#define TS_ALBEDO1            18
#define TS_ALBEDO2            19
#define TS_ALBEDO3            20
#define TS_LUT_COLOR_GRADING  21

// ---------------------------------------------
// Mixing material extra texture slots
#define TS_MIX_BLEND_WEIGHTS          22

#define TS_FIRST_SLOT_MATERIAL_0      TS_ALBEDO
#define TS_FIRST_SLOT_MATERIAL_1      TS_ALBEDO1
#define TS_FIRST_SLOT_MATERIAL_2      TS_ALBEDO2

//#define TS_ALBEDO1 20
#define TS_NORMAL1 23
// #define TS_METALLIC1 22
// #define TS_ROUGHNESS1 23
//#define TS_ALBEDO2 24
#define TS_NORMAL2 24
// #define TS_METALLIC2 25
// #define TS_ROUGHNESS2 26

// -------------------------------------------------
// Render Outputs. Must be in sync with module_render.cpp
#define RO_COMPLETE           0
#define RO_ALBEDO             1
#define RO_NORMAL             2
#define RO_NORMAL_VIEW_SPACE  3
#define RO_SPECULAR           4
#define RO_WORLD_POS          5
#define RO_LINEAR_DEPTH       6
#define RO_AO                 7
#define RO_EMISSIVE	          8
#define RO_OUTLINES	          9
#define RO_DISTORSIONS	      10
#define RO_OUTLINEMASK	      11

// -------------------------------------------------
#define MAX_SUPPORTED_BONES        128

#define PI 3.14159265359f

// ALIGN FLOAT4 TO 4 WORDS! BOOL COUNTS AS HALF-WORD
SHADER_CTE_BUFFER(TCtesCamera, CTE_BUFFER_SLOT_CAMERAS)
{
	matrix Projection;
	matrix View;
	matrix ViewProjection;
	matrix InverseViewProjection;
	matrix CameraScreenToWorld;
	matrix CameraProjWithOffset;
	float3 CameraFront;
	float  CameraZFar;
	float3 CameraPosition;
	float  CameraZNear;
	float  CameraTanHalfFov;
	float  CameraAspectRatio;
	float2 CameraInvResolution;
	float3 CameraLeft;
	float  CameraDummy1;
	float3 CameraUp;
	float  CameraDummy2;
};

SHADER_CTE_BUFFER(TCtesObject, CTE_BUFFER_SLOT_OBJECT)
{
	matrix World;
	float4 ObjColor;
	float glowAmount;
	float rimFactor;
	float specularFactor;
	float dummy2;
};

SHADER_CTE_BUFFER(TCtesShaderFX, CTE_BUFFER_SLOT_SHADERFX)
{
	// Dissolve
	bool dissolve_enabled;
	float dissolve_ratio;
	float edge_thickness;
	float edge_glow;
	float4 edge_color;
	//8

	// Fire
	bool fire_enabled;
	float fire_distorsion_ammount;
	float fire_glow;
	float random_time;
	float4 inner_fire_color;
	float4 outer_fire_color;
	//20

	// Water
	// Watch the order, a bool is just a halfword, so two consecutive bools count as one word
	bool water_enabled;
	float contrast;
	bool posterize_enabled;
	int posterize_bands;
	//24

	float4 black_color;
	float4 white_color;
	//32

	float water_stretch;
	float water_glow;
	float water_speed;
	float dist_speed;
	//36

	float dist_ammount;
	float dist_stretch;
	float foam_intensity;
	float foamMaxDist;
	//40

	float foamMinDist;
	float foam_scale;
	float borderVisibility;
	float dummy3;
	//44

	float4 foam_color;
	//48

	// Magic Outlines
	float4 outline_color;
	//52
	float outline_glow;

	// Fake Vol Lights
	float volEmissive;
	float volDepth;
	float volRim;
	float4 lightCol;
	//60

	//Alpha Erosion
	float alphaErosion;
	float baseAlpha;
	float dummyAlpha1;
	float dummyAlpha2;
};

SHADER_CTE_BUFFER(TCtesShared, CTE_BUFFER_SLOT_SHARED)
{
	float  GlobalWorldTime;
	float  GlobalDeltaTime;
	float  GlobalDeltaUnscaledTime;
	float  GlobalDummy1;

	float  GlobalDummy2;
	int    GlobalRenderOutput;
	float  GlobalAmbientBoost;
	float  GlobalExposureAdjustment;

	float GlobalFXAmount;
	float GlobalFXVal1;
	float GlobalFXVal2;
	float GlobalFXVal3;

	float  GlobalLUTAmount;
	float  xScreenRes;
	float  yScreenRes;

	float  uiBurn;

	float2 UIminUV;
	float2 UImaxUV;
	float4 UItint;
};

SHADER_CTE_BUFFER(TCtesDebugLine, CTE_BUFFER_SLOT_DEBUG_LINE)
{
	// The float4 for the positions is to enforce alignment
	float4 DebugSrc;
	float4 DebugDst;
	float4 DebugColor;
};

SHADER_CTE_BUFFER(TCteSkinBones, CTE_BUFFER_SLOT_SKIN_BONES)
{
	matrix Bones[MAX_SUPPORTED_BONES];
};

SHADER_CTE_BUFFER(TCtesLight, CTE_BUFFER_SLOT_LIGHT)
{
	float4 LightColor;
	float3 LightPosition;
	float  LightIntensity;
	matrix LightViewProjOffset;
	float  LightShadowStep;
	float  LightShadowInverseResolution;
	float  LightShadowStepDivResolution;
	float  LightRadius;
	float3 LightFront;      // For the sun
	float  LightDummy2;
};

SHADER_CTE_BUFFER(TCtesBlur, CTE_BUFFER_SLOT_BLUR)
{
	float4 blur_w;        // weights
	float4 blur_d;        // distances for the 1st, 2nd and 3rd tap
	float2 blur_step;     // Extra modifier
	float2 blur_center;   // To keep aligned x4
};

SHADER_CTE_BUFFER(TCteFocus, CTE_BUFFER_SLOT_FOCUS)
{
	float focus_z_center_in_focus;
	float focus_z_margin_in_focus;
	float focus_transition_distance;
	float focus_modifier;
};

SHADER_CTE_BUFFER(TCteBloom, CTE_BUFFER_SLOT_BLOOM)
{
	float4 bloom_weights;
	float  bloom_threshold_min;
	float  bloom_threshold_max;
	float  bloom_pad1;
	float  bloom_pad2;
};

SHADER_CTE_BUFFER(TCtesMaterial, CTE_BUFFER_SLOT_MATERIAL)
{
	// float  scalar_roughness;
	// float  scalar_metallic;
	// float  scalar_irradiance_vs_mipmaps;
	// float  material_dummy;

	float  mix_boost_r;
	float  mix_boost_g;
	float  mix_boost_b;
	float  material_dummy2;
};

SHADER_CTE_BUFFER(TCtePostFx, CTE_BUFFER_SLOT_POSTFX)
{
	//Outline
	float  depthFade;
	float  outlineThreshold;
	float  maxOutline;
	//3

	//Vignete
	float  vigExtend;
	float  vigIntensity;
	//5

	//Chrom Aberration
	float  chromAberrationAmount;
	//6

	//Motion Blur
	float mblur_blur_samples;
	float mblur_blur_speed;
	matrix PrevViewProjection;
	//12

	//Fog
	float fogDepthFade;
	float maxFog;
	float fogContrast;
	float fogDummy;
	float4 fogColor;
	//20

	//Overlay
	float iluminance;
	float intensity;
	float colorShift;
	float vignette;
	float hitAmount;
	float black;
	float white;
	float overlayDummy1;
	//28
};

SHADER_CTE_BUFFER(TCtesParticles, CTE_BUFFER_SLOT_PARTICLES)
{
	float2 psystem_frame_size;
	float2 psystem_nframes;

	float4 psystem_colors_over_time[8];
	// Stored as float4 because hlsl will access it as array
	// Only the .x is currently being used in the shader particles.inc
	float4 psystem_sizes_over_time[8];

	float4 psystem_speed_over_time[8];

	float2  emitter_time_between_spawns;
	float2 emitter_duration;        // min/max

	float3 emitter_center;
	bool collision_simulation;

	float2 emitter_num_particles_per_spawn;
	bool emitter_billboard;
	float psystem_total_life_time;

	float3 emitter_dir;
	float psystem_delay_time;

	float  emitter_center_radius;
	float  emitter_dir_aperture;
	float2 emitter_speed;           // min/max

	bool  emitter_looping;
	float emitter_iterations;
	float2 emitter_gravity_factor;

	uint emitter_type;
	float emitter_shell_thickness;
	float2 emitter_modifier_size_factor;

	float3 emitter_cube_half_size;
	float emitter_random_dir_factor;

	float2 emitter_initial_yaw;
	float2 emitter_initial_pitch;

	float2 emitter_initial_roll;
	float2 particleDummy2;

	float4 emitter_yaw_over_time[8];
	float4 emitter_pitch_over_time[8];
	float4 emitter_roll_over_time[8];

	float2 emitter_noise_frequency;
	float2 emitter_noise_factor;

	float3 emitter_offset_center;
	float emitter_glow;

	bool emitter_rotation_over_direction;
	float3 emitter_forward;

	bool emitter_world_space;
	float2 emitter_rotation_speed;
	float particleDummy;
};
