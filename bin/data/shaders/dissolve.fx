//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
#include "common.fx"
#include "gbuffer.inc"
#include "pbr.inc"


//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VS(
  VS_INPUT input
)
{
  // Use world from the constants uniform
  return runObjVS(input, World);
}

VS_OUTPUT VS_skin(
  VS_INPUT input,
  VS_SKINNING skinning
)
{
  float4x4 SkinMatrix = getSkinMtx(skinning);
  return runObjVS(input, SkinMatrix);
}

//--------------------------------------------------------------------------------------
// Pixel Shader that does the dissolve
//--------------------------------------------------------------------------------------
void PS(
  VS_OUTPUT input
  , out float4 o_albedo   : SV_Target0
  , out float4 o_normal : SV_Target1
  , out float1 o_depth : SV_Target2
  , out float4 o_emissive : SV_Target3
)
{
  float4 noise = txNoise.Sample(samLinear, (input.Uv * 0.2) + (sin(2.0 * 3.14 * 0.05) + 1.0) / 2.0);
  float r = noise.x;
  float offset_r = r + edge_thickness;
  float alpha_threshold = dissolve_ratio;

  // Albedo under normal circumstances
  float4 albedo_color = txAlbedo.Sample(samLinear, input.Uv);
  o_albedo.xyz = albedo_color.xyz * ObjColor.xyz;
  o_albedo.a = 1.0;

  // Emisive under normal circumstances
  o_emissive.xyz = txEmissive.Sample(samLinear, input.Uv).xyz * glowAmount;
  o_emissive.a = rimFactor;

  float4 mainColor = o_albedo;
  float4 mainEmissive = o_emissive;
  float4 edgeColor = float4(edge_color.xyz * max(edge_glow, 1.0), 1.0);

  // if r>=a color = mainColor, else color = edgeColor
  o_albedo = lerp(edgeColor, mainColor, (r >= alpha_threshold));
  // write in the emisive if color = edgeColor
  o_emissive = lerp(edgeColor, mainEmissive, (r >= alpha_threshold));

  float4 transparent = float4(0.0, 0.0, 0.0, 0.0);
  // if offset_r>=a color = color, else color = transparent
  o_albedo = lerp(transparent, o_albedo, (offset_r >= alpha_threshold));

  // if not enabled return mainColor and mainEmissive
  o_albedo = lerp(mainColor, o_albedo, (dissolve_enabled));
  o_emissive = lerp(mainEmissive, o_emissive, (dissolve_enabled));

  // if transparent just clip
  //clip(o_albedo.a - (1.01 - o_albedo.a));

  // Normal mapping
  float4 N_tangent_space = txNormal.Sample(samLinear, input.Uv);  // Between 0..1
  N_tangent_space.x = 1 - N_tangent_space.x;
  N_tangent_space.xyz = N_tangent_space.xyz * 2 - 1.;  // Between -1..1

  float3 T = input.T.xyz;
  float3 B = cross(T, input.N) * input.T.w;
  float3x3 TBN = float3x3(T, B, input.N);
  float3 N = mul(N_tangent_space.xyz, TBN);   // Normal from NormalMap

  //Pass in specularFactor in normal alpha channel
  o_normal = encodeNormal(N, specularFactor);

  float3 cam2obj = input.WorldPos.xyz - CameraPosition.xyz;
  o_depth = dot(CameraFront, cam2obj) / CameraZFar;
  
  o_albedo.a = txSpecular.Sample(samLinear, input.Uv).r;
}
