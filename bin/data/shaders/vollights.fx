//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
#include "common.fx"
#include "gbuffer.inc"
#include "pbr.inc"

//--------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VS(
  float4 Pos : POSITION,
  float3 N : NORMAL,
  float2 Uv: TEXCOORD0,
  float4 T : NORMAL1
)
{
  VS_OUTPUT output = (VS_OUTPUT)0;
  output.Pos = mul(Pos, World);
  output.WorldPos = output.Pos.xyz;
  output.Pos = mul(output.Pos, ViewProjection);
  output.N = mul(N, (float3x3)World);
  output.T = float4(mul(T.xyz, (float3x3)World), T.w);
  output.Uv = Uv;
  return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
void PS(
  VS_OUTPUT input
  , in float4 iPosition : SV_Position
  , out float4 o_albedo   : SV_Target0
)
{
  /////Depth intersect
  GBuffer g;
  decodeGBuffer(iPosition.xy, g);
  float3 cam2obj = input.WorldPos.xyz - CameraPosition.xyz;
  float myDepth = dot(CameraFront, cam2obj) / CameraZFar;

  float depthScale = 20;	
  float depthMax = 0.075 * depthScale;
  float depthMin = 0.0075 * depthScale;
  float rim = saturate(dot(g.view_dir, input.N));
  float dotP = pow(rim, 0.2);
  float depthDist = lerp(depthMax, depthMin, dotP);
  
  float dif = pow(saturate(1 - (abs(g.specular_color.x - myDepth) / depthDist)),8);
  for(int i = 0; i < volDepth; ++i)dif = pow(dif, 2);
  //o_albedo = float4(dif, dif, dif, 1);
  ////
  
  ////Rim alpha
  rim = pow(rim, volRim);
  //o_albedo = float4(rim, rim, rim, 1);
  ////
  
  ////Depth alpha
  float proximity = smoothstep(0, 0.0015, myDepth);
  //o_albedo = float4(proximity, proximity, proximity, 1);
  ////
  
  o_albedo = lightCol;
  o_albedo *= 1 - dif;
  o_albedo *= rim;
  o_albedo *= proximity;
  
  ////Emissive
  o_albedo.rgb *= volEmissive;
  
  ////Apply noise from texture
  float noiseTiling = 3;
  float noiseXSpeed = 0.02;
  float noiseYSpeed = -0.04;
  float noise2Mult = 2.77;
  float noise3Mult = 5.33;
  float stretchFactor = 2;
  float noiseReduction = 0.4;
 
  input.Uv.x *= stretchFactor;
  float2 uvN1 = input.Uv * noiseTiling;
  uvN1.x += GlobalWorldTime * noiseXSpeed;
  uvN1.y += GlobalWorldTime * noiseYSpeed;
  float r1 = txAlbedo.Sample(samLinear, uvN1).r;
  
  float2 uvN2 = input.Uv * noiseTiling;
  uvN2.x += GlobalWorldTime * noiseXSpeed * noise2Mult;
  uvN2.y += GlobalWorldTime * noiseYSpeed * noise2Mult;
  float r2 = txAlbedo.Sample(samLinear, uvN2).g;
  
  float2 uvN3 = input.Uv * noiseTiling;
  uvN3.x += GlobalWorldTime * noiseXSpeed * noise3Mult;
  uvN3.y += GlobalWorldTime * noiseYSpeed * noise3Mult;
  float r3 = txAlbedo.Sample(samLinear, uvN3).b;
  
  o_albedo *= saturate((r1 * r2 * r3) + noiseReduction);
}
//------------------------------------------------------------------------
void PS_write(
  VS_OUTPUT input
  , in float4 iPosition : SV_Position
  , out float4 o_albedo : SV_Target0
  , out float4 o_outlineDepth : SV_Target6
)
{
  //o_albedo = float4(1,0,1,0.2);
  o_outlineDepth = lightCol.a;
}