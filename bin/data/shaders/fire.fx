//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
#include "common.fx"
#include "gbuffer.inc"
#include "pbr.inc"


//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VS(
  VS_INPUT input
)
{
  // Billboard VS
  VS_OUTPUT output = (VS_OUTPUT)0;
  float3 localPos = input.Pos.x * -CameraLeft + input.Pos.y * float3(0,1,0);
  output.Pos = mul(float4(localPos, 1.0), World);
  output.Pos = mul(output.Pos, ViewProjection);
  output.WorldPos = output.Pos.xyz;

  //output.Pos = mul(input.Pos, World);
  //output.WorldPos = output.Pos.xyz;
  //output.Pos = mul(output.Pos, ViewProjection);

  output.N = mul(input.N, (float3x3)World);
  output.T = float4( mul(input.T.xyz, (float3x3)World), input.T.w);
  output.Uv = input.Uv;
  return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader that does the fire effect
//--------------------------------------------------------------------------------------
void PS(
  VS_OUTPUT input
  , out float4 o_albedo   : SV_Target0
  , out float4 o_normal : SV_Target1
  , out float1 o_depth : SV_Target2
  , out float4 o_emissive : SV_Target3
)
{
  // TEXTURES ARE
  // albedo = fireRGB
  //	r = outter fire
  //	g = inner fire
  //	b = clip me
		
  // normal = fire_noise
  //    r = voronoi
  //    g = distorsion
  //    b = fire
  
  // txSpecular = gradFireMaskNoise
  //	r = gradient
  //	g = fire shape
  //	b = seamless noise

  // If not enabled just clip
  clip(fire_enabled - 1);

  float2 uv = input.Uv;
  
  //We are going to sample the noise
  //Explain:
  //Pow: We make the contrast of the texture more pronounced (by making it darker)
  //Uvs: tilingFactor * float2(uv.x, uv.y + (GlobalWorldTime * timeMultiplicator))
  //Numbers are arbitrary and placed by feel and experimentation
  
  float fire = pow(float4(txNormal.Sample(samLinear, 0.15 * float2(uv.x + ((GlobalWorldTime + random_time) * 0.25), uv.y + ((GlobalWorldTime + random_time) * 0.8)))).b, 2);
  float gradient = float4(txSpecular.Sample(samClampLinear, float2(uv.x, uv.y - 0.1))).r;
  float fireShape = float4(txSpecular.Sample(samLinear, uv)).g;
  float noise = pow(float4(txSpecular.Sample(samLinear, 0.1 * float2(uv.x + ((GlobalWorldTime + random_time) * 0.25), uv.y + ((GlobalWorldTime + random_time) * 1)))).b, 2);
 
  
  //We calculate the mask just like the Rime talk says
  float mask = (fire + noise) * gradient * fireShape;
  
  //We sample the fire image while distorsioning it with the mask
  //Uvs format: float2(uv.x, uv.y + (mask * distortMult) - moveAllDownwards)
  o_albedo = txAlbedo.Sample(samClampLinear, float2(uv.x, uv.y + (mask * fire_distorsion_ammount) - 0.3));
  
  //We discard blue fragments/pixels
  clip(0.1 - o_albedo.b);
  
  //We Color Swap rgb fire colors
  //float3 innerFireColor = float3(0.2, 0.75, 1); //change or make it a param
  //float3 outterFireColor = float3(0.0, 0.75, 1); //change or make it a param
  o_albedo = float4((o_albedo.g * inner_fire_color.rgb) + (o_albedo.r * outer_fire_color.rgb), 1);
  
  //Uncomment to visualize mask (used to decide and visualize parameters)
  //Comment clip to see the full mask
  //o_albedo = float4(mask, mask, mask, 1);
  
  //Apply emission
  o_emissive.xyz = o_albedo.xyz * fire_glow;//fire_glow;
  o_emissive.a = 1;
  
  //We save the depth to avoid outline painting over the fire
  //float3 cam2obj = input.WorldPos.xyz - CameraPosition.xyz;
  //o_depth = dot(CameraFront, cam2obj) / CameraZFar;
  o_depth = 0; //Bibloard messed with fire depth
}

//https://www.patreon.com/posts/quick-game-art-17021975
//https://www.youtube.com/watch?v=fwKQyDZ4ark