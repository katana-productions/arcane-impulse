//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
#include "common.fx"

//--------------------------------------------------------------------------------------
struct VS_OUTPUT
{
  float4 Pos : SV_POSITION;
  float2 Uv : TEXCOORD0;
  float4 Color : COLOR;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VS(
  float4 Pos : POSITION,
  float2 Uv: TEXCOORD0,
  float4 Color : COLOR
)
{
  VS_OUTPUT output = (VS_OUTPUT)0;
  output.Pos = mul(Pos, World);

  output.Pos = mul(output.Pos, ViewProjection);
  output.Color = Color * ObjColor;
  output.Uv = Uv;
  return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(VS_OUTPUT input) : SV_Target
{
  float2 final_uv = lerp(UIminUV, UImaxUV, input.Uv);
  float4 texture_color = txAlbedo.Sample(samClampLinear, final_uv.xy);
  return texture_color * input.Color * UItint;
}

float4 PS_Burn(VS_OUTPUT input) : SV_Target
{  
  float _FadeAmount = uiBurn;
  //float _FadeAmount = (sin(GlobalWorldTime * 0.5) + 1) / 2; //uiBurn
  //_FadeAmount = 0.4;
  float _FadeBurnWidth = 0.5;
  float _FadeBurnTransition = 0.05;
  float4 _FadeBurnColor = float4(1,0.1,0,1);
  float _FadeBurnGlow = 35.0;
  
  float2 uv = lerp(UIminUV, UImaxUV, input.Uv);
  float2 noiseUv = input.Uv * 0.35;
  noiseUv.x *= 2;
  float2 fireUv = uv * 4;
  fireUv.x *= 2;
  
  //_FadeTex = txEmissive (perlin)
  float _FadeTex = txEmissive.Sample(samLinear, noiseUv).r;
  
  //_FadeBurnTex = txNormal (fire tex)
  float4 _FadeBurnTex = txNormal.Sample(samLinear, fireUv) * _FadeBurnColor;
  _FadeBurnTex.rgb *= _FadeBurnGlow;

  float fade = smoothstep(_FadeAmount + 0.01, _FadeAmount + _FadeBurnTransition, _FadeTex);
  float fadeBurn = smoothstep(_FadeAmount - _FadeBurnWidth, _FadeAmount - _FadeBurnWidth + 0.1, _FadeTex);
  
  float4 texture_color = txAlbedo.Sample(samLinear, uv.xy) * input.Color * UItint;
  float alpha = texture_color.a;
  texture_color += fadeBurn * (1 - fade) * _FadeBurnColor * _FadeBurnGlow;
  texture_color.a = alpha * fade;
  return texture_color;
}

/////////////////////////////

//////////All In 1 Sprite Shader fade burn effect

//_FadeTex("Fade Texture", 2D) = "white" {}
//_FadeAmount("Fade Amount",  Range(-0.1,1)) = -0.1
//_FadeBurnWidth("Fade Burn Width",  Range(0,1)) = 0.025
//_FadeBurnTransition("Fade Burn Smooth Transition",  Range(0.01,0.5)) = 0.075
//_FadeBurnColor("Fade Burn Color", Color) = (1,1,0,1)
//_FadeBurnTex("Fade Burn Texture", 2D) = "white" {}
//_FadeBurnGlow("Fade Burn Glow",  Range(1,50)) = 2
//
//#if FADE_ON
//fixed2 tiledUvFade1= i.uvFadeTex1;
//fixed2 tiledUvFade2 = i.uvFadeTex2;
//fixed fadeTemp = tex2D(_FadeTex, tiledUvFade1).r;
//fixed fade = smoothstep(_FadeAmount + 0.01, _FadeAmount + _FadeBurnTransition, fadeTemp);
//fixed fadeBurn = smoothstep(_FadeAmount - _FadeBurnWidth, _FadeAmount - _FadeBurnWidth + 0.1, fadeTemp);
//col.a *= fade;
//_FadeBurnColor.rgb *= _FadeBurnGlow;
//col += fadeBurn * tex2D(_FadeBurnTex, tiledUvFade2) * _FadeBurnColor * (1 - col.a);
//#endif
//		
///////////////////////////////OLD Shader
//
//fixed4 frag (v2f i) : SV_Target
//{
//	_Burn = abs(sin(_Time.y)); //REMOVE ME
//
//	fixed4 c = tex2D(_MainTex, i.uv) * _Color;
//	float4 outline = float4(1,1,1,1) * _ColorB * _Glow2;
//	outline.a = step(max(0, _Burn - _BurnLineWidth), tex2D(_Noise, i.uv).r) * c.a;
//
//	half4 e = tex2D(_Emissive, i.uv) * _ColorG * _Glow;
//	c.rgb += e.rgb;
//	c.a *= _Alpha;
//	c.a *= step(_Burn, tex2D(_Noise, i.uv).r);
//
//	outline.a -= c.a;
//
//	return lerp(c, outline, outline.a);
//}		
