//--------------------------------------------------------------------------------------
#include "common.fx"

//-------------------------`-------------------------------------------------------------
struct VS_OUTPUT {   // Vertex to pixel
	float4 Pos   : SV_POSITION;
	float2 Uv    : TEXCOORD0;
	float3 WorldPos : TEXCOORD1;
	float4 Color : COLOR;
	float2 Glow : TEXCOORD2;
};

//--------------------------------------------------------------------------------------
void PS_common(
	VS_OUTPUT input
	, out float4 o_albedo
	, out float4 o_outlineDepth : SV_Target6
	, float baseAlpha
)
{
	float4 color = txAlbedo.Sample(samLinear, input.Uv);
	o_albedo = float4(color.rgb * input.Color.rgb * input.Glow.x, color.a * input.Color.a);
	float tempAlpha = 1 - o_albedo.a;
	
	float textureAlpha = txNormal.Sample(samLinear, input.Uv).r;
	clip(textureAlpha - tempAlpha - 0.001);
	o_albedo.a = textureAlpha * baseAlpha;
	o_outlineDepth = o_albedo.a;
}

void PS_01(
	VS_OUTPUT input
	, out float4 o_albedo   : SV_Target0
	, out float4 o_outlineDepth : SV_Target6
)
{
	PS_common(input, o_albedo, o_outlineDepth, 0.01);
}

void PS_05(
	VS_OUTPUT input
	, out float4 o_albedo   : SV_Target0
	, out float4 o_outlineDepth : SV_Target6
)
{
	PS_common(input, o_albedo, o_outlineDepth, 0.05);
}

void PS_1(
	VS_OUTPUT input
	, out float4 o_albedo   : SV_Target0
	, out float4 o_outlineDepth : SV_Target6
)
{
	PS_common(input, o_albedo, o_outlineDepth, 0.1);
}

void PS_2(
	VS_OUTPUT input
	, out float4 o_albedo   : SV_Target0
	, out float4 o_outlineDepth : SV_Target6
)
{
	PS_common(input, o_albedo, o_outlineDepth, 0.2);
}

void PS_3(
	VS_OUTPUT input
	, out float4 o_albedo   : SV_Target0
	, out float4 o_outlineDepth : SV_Target6
)
{
	PS_common(input, o_albedo, o_outlineDepth, 0.3);
}

void PS_4(
	VS_OUTPUT input
	, out float4 o_albedo   : SV_Target0
	, out float4 o_outlineDepth : SV_Target6
)
{
	PS_common(input, o_albedo, o_outlineDepth, 0.4);
}

void PS_5(
	VS_OUTPUT input
	, out float4 o_albedo   : SV_Target0
	, out float4 o_outlineDepth : SV_Target6
)
{
	PS_common(input, o_albedo, o_outlineDepth, 0.5);
}

void PS_6(
	VS_OUTPUT input
	, out float4 o_albedo   : SV_Target0
	, out float4 o_outlineDepth : SV_Target6
)
{
	PS_common(input, o_albedo, o_outlineDepth, 0.6);
}

void PS_7(
	VS_OUTPUT input
	, out float4 o_albedo   : SV_Target0
	, out float4 o_outlineDepth : SV_Target6
)
{
	PS_common(input, o_albedo, o_outlineDepth, 0.7);
}

void PS_8(
	VS_OUTPUT input
	, out float4 o_albedo   : SV_Target0
	, out float4 o_outlineDepth : SV_Target6
)
{
	PS_common(input, o_albedo, o_outlineDepth, 0.8);
}

void PS_9(
	VS_OUTPUT input
	, out float4 o_albedo   : SV_Target0
	, out float4 o_outlineDepth : SV_Target6
)
{
	PS_common(input, o_albedo, o_outlineDepth, 0.9);
}

void PS_10(
	VS_OUTPUT input
	, out float4 o_albedo   : SV_Target0
	, out float4 o_outlineDepth : SV_Target6
)
{
	PS_common(input, o_albedo, o_outlineDepth, 1);
}