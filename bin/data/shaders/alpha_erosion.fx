//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
#include "common.fx"

//--------------------------------------------------------------------------------------
struct VS_OUTPUT
{
  float4 Pos      : SV_POSITION;
  float3 N        : NORMAL;
  float2 Uv       : TEXCOORD0;
  float3 WorldPos : TEXCOORD1;
  float4 T        : NORMAL1;
};
//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VS(
	float4 Pos : POSITION,
	float3 N : NORMAL,
	float2 Uv : TEXCOORD0
)
{
	VS_OUTPUT output = (VS_OUTPUT)0;
	output.Pos = mul(Pos, World);
	output.WorldPos = output.Pos.xyz;
	output.Pos = mul(output.Pos, ViewProjection);
	output.N = normalize(mul(N, World));
	output.Uv = Uv;
	return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
void PS(
	VS_OUTPUT input
	, out float4 o_albedo   : SV_Target0
	, out float4 o_normal   : SV_Target1
	, out float1 o_depth    : SV_Target2
	, out float4 o_emissive : SV_Target3
	, out float4 o_outlineMask : SV_Target6
)
{
	//Sin to test without component
	//float alphaErosion = (sin(GlobalWorldTime * 1.25) + 1) / 2;
	
	o_albedo = txAlbedo.Sample(samLinear, input.Uv) * ObjColor;
	
	float textureAlpha = txNormal.Sample(samLinear, input.Uv).r;
	clip(textureAlpha - alphaErosion - 0.001);
	o_albedo.a = textureAlpha * baseAlpha;
	
	float3 cam2obj = input.WorldPos.xyz - CameraPosition.xyz;
	o_depth = dot(CameraFront, cam2obj) / CameraZFar;
	
	o_emissive.xyz = txEmissive.Sample(samLinear, input.Uv).xyz * o_albedo.xyz * glowAmount;
	o_emissive.a = 1;
	
	float mask = textureAlpha * baseAlpha * 4;
	o_outlineMask = float4(mask, mask, mask, mask);
}
