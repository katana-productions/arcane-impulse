//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
#include "common.fx"
#include "gbuffer.inc"
#include "pbr.inc"

float4 alphaBlend(float4 top, float4 bottom)
{
	float3 color = (top.rgb * top.a) + (bottom.rgb * (1 - top.a));
	float alpha = top.a + bottom.a * (1 - top.a);

	return float4(color, alpha);
}

//--------------------------------------------------------------------------------------
// Vertex Shader wave effect
//--------------------------------------------------------------------------------------

float4 GrestnerWave(float4 wave, float4 inputPos, float speed)
{
	float2 dir = normalize(wave.xy);
	float steepness = wave.z;
	float wavelength = wave.w;

	float k = 2 * PI / wavelength;
	float c = sqrt(9.8 / k);
	float f = k * (dot(dir, inputPos.xz) - c * GlobalWorldTime * speed);
	float a = steepness / k;

    float4 offset = float4(
    	dir.x * (a * cos(f)), 
    	a * sin(f), 
    	dir.y * (a * cos(f)),
    	0
    );

	return offset;
}

VS_OUTPUT VS_Waves(
  VS_INPUT input
)
{
  VS_OUTPUT output = (VS_OUTPUT)0;

  float speed = 2;
  // xy: direction, z: steepness, w: wavelength
  // wavelengths hace to be even powers of 2
  // the sum of all steepness can't exceed 1
  // this doesn't work parametric somehow
  float4 wave1 = float4(-1, -0.6, 0.2, 32);
  float4 wave2 = float4(-0.2, -0.4, 0.2, 16);
  float4 wave3 = float4(-0.3, -0.5, 0.2, 8);

  float4 vPos = mul(input.Pos, World);
  vPos += GrestnerWave(wave1, input.Pos, speed);
  vPos += GrestnerWave(wave2, input.Pos, speed);
  vPos += GrestnerWave(wave3, input.Pos, speed);
	
	output.Pos = vPos;
	output.WorldPos = output.Pos.xyz;
	output.Pos = mul(output.Pos, ViewProjection);

	output.N = mul(input.N, World);

	output.Uv = input.Uv;
	return output;
}
// Waves extracted from here: https://catlikecoding.com/unity/tutorials/flow/waves/

VS_OUTPUT VS(
	float4 Pos : POSITION,
	float3 N : NORMAL,
	float2 Uv : TEXCOORD0
)
{
	VS_OUTPUT output = (VS_OUTPUT)0;
	output.Pos = mul(Pos, World);
	output.WorldPos = output.Pos.xyz;
	output.Pos = mul(output.Pos, ViewProjection);
	output.N = normalize(mul(N, World));
	output.Uv = Uv;
	return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader that does the water effect
//--------------------------------------------------------------------------------------

void PS(
  VS_OUTPUT input
  ,in float4 iPosition : SV_Position
  , out float4 o_albedo   : SV_Target0
  , out float4 o_normal : SV_Target1
  , out float4 o_emissive : SV_Target3
)
{
  // Clip if not enabled
  clip(water_enabled - 0.1);

  // UV and uv distorsion
  float2 uv = input.Uv;
  uv.x += GlobalWorldTime * water_speed;
  uv.x *= water_stretch; //Stretch y coords;

  float2 uv_noise = input.Uv * 0.5;
  uv_noise.x += GlobalWorldTime * dist_speed;
  uv_noise.x *= dist_stretch; //Stretch y coords;
  
  float2 uv_foam = input.Uv * 2;
  uv_foam.x += GlobalWorldTime * max(water_speed, dist_speed);
  
  /////Foam
  GBuffer g;
  decodeGBuffer(iPosition.xy, g);
  float3 cam2obj = input.WorldPos.xyz - CameraPosition.xyz;
  float myDepth = dot(CameraFront, cam2obj) / CameraZFar;
	
  float foamMax = foamMaxDist * foam_scale;
  float foamMin = foamMinDist * foam_scale;
  float dotP = pow(saturate(dot(g.view_dir, input.N)), 0.2);
  float foamDist = lerp(foamMax, foamMin, dotP);
  
  //float dif = step(dist.x , saturate(1 - (abs(g.specular_color.x - myDepth) / foamDist))); //Noise not granular
  float dif = pow(saturate(1 - (abs(g.specular_color.x - myDepth) / foamDist)), 10); //Fade. 0.75 avoids intense white. 10 more gradient
  ////////
  
  //Sample Distortions texture (txNormal) to distort uv
  float2 dist = txNormal.Sample(samLinear, uv_noise).xy;
  uv += ((dist * 2) - 1) * dist_ammount * (1-dif);
  
  // Banding for Posterize
  //float nBands = 5;
  float noise = txAlbedo.Sample(samLinear, uv).x;
  float posterize = round(noise * posterize_bands) / posterize_bands; //Banding for toon look
  noise = lerp(noise, posterize, posterize_enabled);
  //o_albedo = float4(noise, noise, noise, 1);
  
  o_albedo = float4(float3(noise, noise, noise) * lerp(black_color.xyz, white_color.xyz, noise), 1);
    
  float borderVisibility = 3;
  for(int i = 0; i < (int)borderVisibility; i++)   dif = sqrt(dif);

  noise = pow(noise, contrast);
  o_albedo = float4(o_albedo.xyz * (noise * water_glow + 1), 1);
  o_albedo = lerp(o_albedo, foam_color * foam_intensity, saturate(dif + 0.5) * dif);
}
//------------------------------------------------------------------------

//http://halisavakis.com/my-take-on-shaders-unlit-waterfall-part-1/
//http://halisavakis.com/my-take-on-shaders-unlit-waterfall-part-2/

//------------------------------------------------------------------------
void PS_Water2(
  VS_OUTPUT input
  ,in float4 iPosition : SV_Position
  , out float4 o_albedo   : SV_Target0
  , out float4 o_normal : SV_Target1
  , out float4 o_emissive : SV_Target3
)
{
  // Clip if not enabled
  clip(water_enabled - 0.1);
  
  float2 uv = input.Uv;
  float4 waterColorShallow = float4(black_color.rgb,1);
  float4 waterColorDeep = float4(white_color.rgb,1);
  
  float2 uv_noise = input.Uv * 0.5;
  uv_noise.x *= 4;
  uv_noise.y *= 8;
  uv_noise.x += GlobalWorldTime * dist_speed;
  float2 dist = txNormal.Sample(samLinear, uv_noise).xy;
  
  float2 uv_noise2 = input.Uv;
  uv_noise2.x *= 8;
  uv_noise2.y *= 16;
  uv_noise2.x += GlobalWorldTime * dist_speed * 2.12;
  uv_noise2.y += GlobalWorldTime * dist_speed * 1.2;
  float2 dist2 = txNormal.Sample(samLinear, uv_noise2).xy;
  
  uv += ((((dist + dist2)/2) * 2) - 1) * dist_ammount;
  
  float fakeDepth = 1 - (0.5 - abs(uv.x - 0.5));
  fakeDepth = pow(fakeDepth, 7.5);
  
  float4 waterColor = float4(0,0,0,1);
  waterColor.rgb = lerp(waterColorDeep.rgb, waterColorShallow.rgb, fakeDepth);
  
  uv.x += GlobalWorldTime * water_speed * 0.1;
  uv.x *= 2;
  uv.y *= 4;
  
  /////Foam
  GBuffer g;
  decodeGBuffer(iPosition.xy, g);
  float3 cam2obj = input.WorldPos.xyz - CameraPosition.xyz;
  float myDepth = dot(CameraFront, cam2obj) / CameraZFar;
	
  float foamMax = foamMaxDist * foam_scale;
  float foamMin = foamMinDist * foam_scale;
  float dotP = pow(saturate(dot(g.view_dir, input.N)), 0.2);
  float foamDist = lerp(foamMax, foamMin, dotP);
  
  float dif = pow(saturate(1 - (abs(g.specular_color.x - myDepth) / foamDist)), 15); //Fade. 0.75 avoids intense white. 10 more gradient
  
  dif = pow(dif, 3 + (0.75 * sin(GlobalWorldTime * 4 + uv.x + uv.y + uv.x + uv.y)));
  //float borderVisibility = 0;
  //for(int i = 0; i < (int)borderVisibility; i++)   dif = sqrt(dif);
  ////////
  
  o_albedo = txNormal.Sample(samLinear, uv_noise);
  float surfaceNoise = step(saturate(0.75 - dif), txAlbedo.Sample(samLinear, uv));
  o_albedo = waterColor + min(surfaceNoise, 0.7);
  //o_albedo = txAlbedo.Sample(samLinear, uv);
  //o_albedo = float4(fakeDepth, fakeDepth, fakeDepth, 1);
  //o_albedo = float4(dif, dif, dif, 1);
}
//https://roystan.net/articles/toon-water.html