//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
#include "common.fx"
#include "gbuffer.inc"
#include "pbr.inc"

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VS(
	VS_INPUT input
)
{
	// Use world from the constants uniform
	return runObjVS(input, World);
}

//--------------------------------------------------------------------------------------
// Vertex Shader for Skin, using standard vertex + skin info
//--------------------------------------------------------------------------------------
VS_OUTPUT VS_skin(
	VS_INPUT input,
	VS_SKINNING skinning
)
{
	float4x4 SkinMatrix = getSkinMtx(skinning);
	return runObjVS(input, SkinMatrix);
}

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VS_instanced(
	VS_INPUT input,
	VS_INSTANCE_WORLD instance_data     // Stream 1
)
{
	// Use world from the instance
	float4x4 instanceWorld = getWorldOfInstance(instance_data);
	return runObjVS(input, instanceWorld);
}

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
SHADER_CTE_BUFFER(TCtesMorph, CTE_BUFFER_SLOT_COMP_BUFFERS)
{
	float  morph_weight_target;
	float3 morph_pad3;
};

VS_OUTPUT VS_morph(
	VS_INPUT_MORPH input_morph
)
{
	float amount_of_next = morph_weight_target; //saturate( ( cos(GlobalWorldTime) + 1 ) * 0.5 );
	VS_INPUT input;
	input.Pos = input_morph.Pos * (1.0 - amount_of_next) + input_morph.Pos1 * amount_of_next;
	input.N = input_morph.N   * (1.0 - amount_of_next) + input_morph.N1   * amount_of_next;
	input.Uv = input_morph.Uv;
	input.T = input_morph.T;
	return runObjVS(input, World);
}

//--------------------------------------------------------------------------------------
// Pixel Shader to fill the gBuffer
//--------------------------------------------------------------------------------------
void PS_common(
	VS_OUTPUT input
	, out float4 o_albedo : SV_Target0
	, out float4 o_normal : SV_Target1
	, out float1 o_depth  : SV_Target2
	, out float4 o_emissive : SV_Target3
	, bool use_alpha_test
)
{
	float4 albedo_color = txAlbedo.Sample(samLinear, input.Uv);
	o_albedo.xyz = albedo_color.xyz;
	//if (use_alpha_test && albedo_color.a <= 0.2)
	clip(1.79 - use_alpha_test - (1 - albedo_color.a)); //equivalent to conditional

	//Save specular in albedo alpha
	o_albedo.a = txSpecular.Sample(samLinear, input.Uv).r;

	// Normal mapping
	float4 N_tangent_space = txNormal.Sample(samLinear, input.Uv);  // Between 0..1
	N_tangent_space.y = 1 - N_tangent_space.y; //Invert normals correctly (green channel)
	N_tangent_space.xyz = N_tangent_space.xyz * 2 - 1.;  // Between -1..1

	float3 T = input.T.xyz;
	float3 B = cross(T, input.N) * input.T.w;
	float3x3 TBN = float3x3(T, B, input.N);
	float3 N = mul(N_tangent_space.xyz, TBN);   // Normal from NormalMap

	//Pass in specularFactor in normal alpha channel
	o_normal = encodeNormal(N, specularFactor);

	float3 cam2obj = input.WorldPos.xyz - CameraPosition.xyz;
	o_depth = dot(CameraFront, cam2obj) / CameraZFar;

	o_emissive.xyz = txEmissive.Sample(samLinear, input.Uv).x * o_albedo.xyz * ObjColor.xyz * glowAmount;
	o_emissive.a = rimFactor;
}

void PS(
	VS_OUTPUT input
	, out float4 o_albedo : SV_Target0
	, out float4 o_normal : SV_Target1
	, out float1 o_depth  : SV_Target2
	, out float4 o_emissive : SV_Target3
)
{
	PS_common( input, o_albedo, o_normal, o_depth, o_emissive, false);
}

void PS_alpha(VS_OUTPUT input
	, out float4 o_albedo : SV_Target0
	, out float4 o_normal : SV_Target1
	, out float1 o_depth  : SV_Target2
	, out float4 o_emissive : SV_Target3
) {
	PS_common(input, o_albedo, o_normal, o_depth, o_emissive, true);
}

void PS_NoOutline(VS_OUTPUT input
	, out float4 o_albedo : SV_Target0
	, out float4 o_normal : SV_Target1
	, out float1 o_depth  : SV_Target2
	, out float4 o_emissive : SV_Target3
	, out float4 o_outlines : SV_Target4
) {
	PS_common( input, o_albedo, o_normal, o_depth, o_emissive, false);
	o_outlines = float4(0,0,0,rimFactor);
}

//--------------------------------------------------------------------------------------
void PS_gbuffer_mix(
	VS_OUTPUT input
	, out float4 o_albedo : SV_Target0
	, out float4 o_normal : SV_Target1
	, out float1 o_depth : SV_Target2
) {
	// This is different -----------------------------------------
	float2 iTex0 = input.Uv * 2;
	float2 iTex1 = input.Uv;

	float4 weight_texture_boost = txMixBlendWeights.Sample(samLinear, iTex1);

	float4 albedoR = txAlbedo.Sample(samLinear, iTex0);
	float4 albedoG = txAlbedo1.Sample(samLinear, iTex0);
	float4 albedoB = txAlbedo2.Sample(samLinear, iTex0);

	// Use the alpha of the albedo as heights + texture blending extra weights + material ctes extra weights (imgui)
	float w1, w2, w3;
	computeBlendWeights(albedoR.a + mix_boost_r + weight_texture_boost.r
		, albedoG.a + mix_boost_g + weight_texture_boost.g
		, albedoB.a + mix_boost_b + weight_texture_boost.b
		, w1, w2, w3);

	// Use the weight to 'blend' the albedo colors
	float4 albedo = albedoR * w1 + albedoG * w2 + albedoB * w3;
	o_albedo.xyz = albedo.xyz;

	// Mix the normal
	float3 normalR = txNormal.Sample(samLinear, iTex0).xyz * 2.0 - 1.0;
	float3 normalG = txNormal1.Sample(samLinear, iTex0).xyz * 2.0 - 1.0;
	float3 normalB = txNormal2.Sample(samLinear, iTex0).xyz * 2.0 - 1.0;
	float3 normal_color = normalR * w1 + normalG * w2 + normalB * w3;
	float3x3 TBN = computeTBN(input.N, input.T);

	// Normal map comes in the range 0..1. Recover it in the range -1..1
	float3 wN = mul(normal_color, TBN);
	float3 N = normalize(wN);

	// Missing: Do the same with the metallic & roughness channels
	// ...

	// Possible plain blending without heights
	//o_albedo.xyz = lerp( albedoB.xyz, albedoG.xyz, weight_texture_boost.y );
	//o_albedo.xyz = lerp( o_albedo.xyz, albedoR.xyz, weight_texture_boost.x );

	//o_albedo.xyz = float3( iTex1.xy, 0 );   // Show the texture coords1

	//o_albedo.xyz = weight_texture_boost.xyz;  // Show the extra weight textures

	o_albedo.a = txSpecular.Sample(samLinear, iTex0).r;

	// This is the same -----------------------------------------
	// Save roughness in the alpha coord of the N render target
	float roughness = txSpecular.Sample(samLinear, iTex0).r;
	o_normal = encodeNormal(N, roughness);

	// Compute the Z in linear space, and normalize it in the range 0...1
	// In the range z=0 to z=zFar of the camera (not zNear)
	float3 cam2obj = input.WorldPos.xyz - CameraPosition.xyz;
	float  linear_depth = dot(cam2obj, CameraFront) / CameraZFar;
	o_depth = linear_depth;
}

// -------------------------------------------------
// The output of the deferred before the postFX
float4 PS_GBuffer_Resolve(
	float4 iPosition   : SV_POSITION,
	float2 iUV : TEXCOORD0
) : SV_Target
{
  int3 ss_load_coords = uint3(iPosition.xy, 0);

  float4 acc_light = txAccLights.Load(ss_load_coords);
  //float4 albedo_color = txGAlbedo.Load(ss_load_coords);
  //float  linear_depth = txGLinearDepth.Sample(samLinear, iUV).x;

  //return float4(0.5, 1, 0, 1);
  return acc_light;
}

// -------------------------------------------------
// Gloss = 1 - rough*rough
//float3 Specular_F_Roughness(float3 specularColor, float gloss, float3 h, float3 v) {
//	return (specularColor * pow((1 - saturate(dot(v, h))), 5));
//	//Sclick using roughness to attenuate fresnel.
//	//return (specularColor + (max(gloss, specularColor) - specularColor) * pow((1 - saturate(dot(v, h))), 5));
//}

//float NormalDistribution_GGX(float a, float NdH)
//{
//	// Isotropic ggx.
//	float a2 = a * a;
//	float NdH2 = NdH * NdH;
//
//	float denominator = NdH2 * (a2 - 1.0f) + 1.0f;
//	denominator *= denominator;
//	denominator *= PI;
//
//	return a2 / denominator;
//}
//
//float Geometric_Smith_Schlick_GGX(float a, float NdV, float NdL)
//{
//	// Smith schlick-GGX.
//	float k = a * 0.5f;
//	float GV = NdV / (NdV * (1 - k) + k);
//	float GL = NdL / (NdL * (1 - k) + k);
//	return GV * GL;
//}

//float Specular_D(float a, float NdH)
//{
//	return NormalDistribution_GGX(a, NdH);
//}
//
//float Specular_G(float a, float NdV, float NdL, float NdH, float VdH, float LdV)
//{
//	return Geometric_Smith_Schlick_GGX(a, NdV, NdL);
//}

//float3 Fresnel_Schlick(float3 specularColor, float3 h, float3 v)
//{
//	return (specularColor + (1.0f - specularColor) * pow((1.0f - saturate(dot(v, h))), 5));
//}
//
//float3 Specular_F(float3 specularColor, float3 h, float3 v)
//{
//	return Fresnel_Schlick(specularColor, h, v);
//}
//
//float3 Specular(float3 specularColor, float3 h, float3 v, float3 l, float a, float NdL, float NdV, float NdH, float VdH, float LdV, float r)
//{
//	return ((Specular_D(a, NdH) * Specular_G(a, NdV, NdL, NdH, VdH, LdV)) * Specular_F(specularColor, v, h)) / (4.0f * NdL * NdV + 0.0001f);
//	//return (Specular_D(a, NdH));
//}

// -------------------------------------------------
//We bright up all albedo colors in our scene
float4 PS_Ambient(
	in float4 iPosition : SV_Position
	, in float2 iUV : TEXCOORD0
) : SV_Target
{
	// Declare some float3 to store the values from the GBuffer
	GBuffer g;
	decodeGBuffer(iPosition.xy, g);

	int3 ss_load_coords = uint3(iPosition.xy, 0);
	float ao = txAO.Load(ss_load_coords).x;
	
	
	//Specular
	float3 light_dir_full = LightPosition.xyz - g.wPos;
	float  distance_to_light = length(light_dir_full);
	float3 light_dir = light_dir_full / distance_to_light;
	float3 h = normalize(light_dir + g.view_dir); // half vector between Viewer-Light
	float  NdH = saturate(dot(g.N, h)); //Normal-Half
	float glossy = 20;
	float specularIntensity = smoothstep(0, 0.01,pow(NdH, glossy * glossy));
	specularIntensity = smoothstep(0.001, 0.01, specularIntensity) * 0.5;
	float specularFactor = g.metallic * g.specular_color.y;
	float3 specular = float3(1,1,1) * specularIntensity * specularFactor;
	//return float4(specular, 1);
	//return float4(g.specular_color.y, g.specular_color.y, g.specular_color.y, 1);
	
	////Rim light
	//float  NdL = saturate(dot(g.N, light_dir)); //Normal-Light
	//float  NdV = saturate(dot(g.N, g.view_dir)); //Normal-Viewer
	//float rimAmount = 0.65; //Lower => thicker rim
	//float rimThresh = 0.25; //Greater => Needs more light incidence to be lit (HIGH number to avoid rim)
	//float rimIntensity = 1 - NdV;
	//rimIntensity = rimIntensity * pow(NdL, rimThresh);
	//rimIntensity = smoothstep(rimAmount - 0.01, rimAmount + 0.01, rimIntensity);
	//float3 rim = float3(1,1,1) * rimIntensity * g.emissive.a * 0.4;
	////rim = float3(g.emissive.a, g.emissive.a, g.emissive.a); //See rim affected parts
	////return float4(rim, 1); //See rim result

	float3 final_color = g.albedo.xyz + g.emissive;
	return float4(final_color + specular, 1) * ao * GlobalAmbientBoost;
	
	//float3 final_color = g.albedo * (light + specular + rim);
}

// ----------------------------------------
// Passthrough for the lights geometry
void VS_pass(
	in float4 iPos : POSITION
	, out float4 oPos : SV_POSITION
) {
	float4 world_pos = mul(iPos, World);
	oPos = mul(world_pos, ViewProjection);
}

// ----------------------------------------
void VS_skybox(
	in float4 iPosition  : POSITION
	, in float4 iColor : COLOR0
	, out float4 oPosition : SV_Position
) {
	// Convert the range 0..1 from iPosition to -1..1 to match the homo space
	oPosition = float4(iPosition.x * 2 - 1., 1 - iPosition.y * 2, 1, 1);
}

// --------------------------------------------------------
float4 PS_skybox(in float4 iPosition : SV_Position) : SV_Target
{
  float3 view_dir = mul(float4(iPosition.xy, 1, 1), CameraScreenToWorld).xyz;
  float4 skybox_color = txEnvironmentMap.Sample(samLinear, view_dir);
  //return float4(skybox_color.xyz,1) * GlobalAmbientBoost;
  return float4(skybox_color.xyz,1); //Not affected by ambient light
}

// --------------------------------------------------------
float3 Diffuse(float3 pAlbedo) {
	return pAlbedo / PI;
}

// --------------------------------------------------------
float4 shade(float4 iPosition, bool use_shadows) {
	GBuffer g;
	decodeGBuffer(iPosition.xy, g);

	// Shadow factor entre 0 (totalmente en sombra) y 1 (no ocluido)
	float shadow_factor = use_shadows ? getShadowFactor(g.wPos) : 1.;
	float step_shadow_factor = smoothstep(0.005, 0.35, shadow_factor);

	// From wPos to Light
	float3 light_dir_full = LightPosition.xyz - g.wPos;
	float  distance_to_light = length(light_dir_full);
	float3 light_dir = light_dir_full / distance_to_light;

	float  NdL = saturate(dot(g.N, light_dir)); //Normal-Light
	float  NdV = saturate(dot(g.N, g.view_dir)); //Normal-Viewer
	float3 h = normalize(light_dir + g.view_dir); // half vector between Viewer-Light

	float  NdH = saturate(dot(g.N, h)); //Normal-Half
	
	float att = saturate(distance_to_light / LightRadius);
	att = 1.0 - att;
		
	//Rim light
	float rimAmount = 0.65; //Lower => thicker rim
	float rimThresh = 0.25; //Greater => Needs more light incidence to be lit (HIGH number to avoid rim)
	float rimIntensity = 1 - NdV;
	rimIntensity = rimIntensity * pow(NdL, rimThresh);
	rimIntensity = smoothstep(rimAmount - 0.01, rimAmount + 0.01, rimIntensity);
	float3 rim = float3(1,1,1) * rimIntensity * g.emissive.a * att;
	//rim = float3(0, 0, 0); //Remove rim
	//rim = float3(g.emissive.a, g.emissive.a, g.emissive.a); //See rim affected parts
	//return float4(rim, 1); //See rim result
	
	//Base light (Not toon, shadows are)
	NdL = max(0.1, smoothstep(0.2, 0.35, NdL)); //Uncomment to make toon
	//Toon light on rim light affected pixels, regular light otherwise
	NdL = ((1 - g.emissive.a) * NdL) + (g.emissive.a * max(0.1, step(0.35, NdL))); 
	float3 light = (LightColor.xyz * NdL * att) * LightIntensity * shadow_factor;
	//return float4(light * 0.2, 1);
	
	//Specular
	float glossy = 15;
	float specularIntensity = smoothstep(0, 0.01,pow(NdH, glossy * glossy));
	specularIntensity = smoothstep(0.001, 0.01, specularIntensity) * LightIntensity * att * 15;
	float specularFactor = g.metallic * g.specular_color.y;
	float3 specular = LightColor.xyz * specularIntensity * step_shadow_factor * specularFactor;
	//return float4(specularIntensity, specularIntensity, specularIntensity, 1);
	//return float4(specular, 1);
	//return float4(specularFactor, specularFactor, specularFactor, 1);
	
	//emissiveAmt is a hardcoded value, should be passed in in the same way that ObjColor is and written into the deferred emissive texture
	float3 final_color = g.albedo * (light + specular + rim);
	return float4(final_color, 1);
}


//--------------------------------------------------------------------------------------
// Projection
//--------------------------------------------------------------------------------------
float4 projectWavedShadow(float3 wPos) {

  // Convert pixel position in world space to light space
  float4 pos_in_light_proj_space = mul(float4(wPos, 1), LightViewProjOffset);
  float3 pos_in_light_homo_space = pos_in_light_proj_space.xyz / pos_in_light_proj_space.w; // -1..1

  // Use these coords to access the projector texture of the light dir
  float2 t_uv = pos_in_light_homo_space.xy;
  
  // We can do distortion later
  //float distortionOffset = -GlobalWorldTime * 0.25;
  //float2 distort_uv = float2(t_uv.x + sin((t_uv.x - distortionOffset) * 20) * 0.025, t_uv.y + 6 * sin((3 * t_uv.x - 46 * distortionOffset)) * 0.0035 * 6 * gobal_player_speed);  
  //float2 distort_uv = float2(t_uv.x + sin((t_uv.y + distortionOffset) * 20) * 0.025, t_uv.y + sin((t_uv.x - distortionOffset * 2) * 20) * 0.025);
  //float4 noise0 = txNoiseMap.Sample(samLinear, distort_uv).r; 
  //float4 noise1 = txNoiseMap2.Sample(samLinear, distort_uv).r * 1.5; 
  //float4 value = lerp(noise0, noise1, global_player_speed);
  //float4 light_projector_color = txProjector.Sample(samLinear, distort_uv);

  float4 light_projector_color = txProjector.Sample(samLinear, t_uv);
  //light_projector_color *=  noise1; 

  // Fade to zero in the last 1% of the zbuffer of the light
  //light_projector_color *= smoothstep(1.f, 0.09f, pos_in_light_homo_space.z);
  return light_projector_color;
}

//--------------------------------------------------------------------------------------
// Pixel Shaders
//--------------------------------------------------------------------------------------

float4 PS_point_lights(
	in float4 iPosition : SV_Position
) : SV_Target
{
  return shade(iPosition, false);
}

float4 PS_dir_lights(
	in float4 iPosition : SV_Position
) : SV_Target
{
  GBuffer g;
  decodeGBuffer(iPosition.xy, g);

  return shade(iPosition, true) * projectWavedShadow(g.wPos);
}

//-------------------------------------------------------------------------------------------------------
//CLEAR Z hands
//-------------------------------------------------------------------------------------------------------
VS_OUTPUT VS_skin_ClearZ(
	VS_INPUT input,
	VS_SKINNING skinning
)
{
	float4x4 SkinMatrix = getSkinMtx(skinning);
	return runObjVSClearZ(input, SkinMatrix);
}

float4 PS_ClearZ(VS_OUTPUT input) : SV_Target
{
	return float4(0,1,0,1);
}

//-------------------------------------------------------------------------------------------------------
//HARDCODED Dirty PS
//-------------------------------------------------------------------------------------------------------

void PS_relic_mineral(
	VS_OUTPUT input
	, out float4 o_albedo : SV_Target0
	, out float4 o_normal : SV_Target1
	, out float1 o_depth  : SV_Target2
	, out float4 o_emissive : SV_Target3
)
{
	float3 hardCodedColor = float3(0.4, 1.0, 0.0);
	
	float4 albedo_color = txAlbedo.Sample(samLinear, input.Uv);
	o_albedo.xyz = albedo_color.xyz * hardCodedColor;

	//Save specular in albedo alpha
	o_albedo.a = txSpecular.Sample(samLinear, input.Uv).r;

	// Normal mapping
	float4 N_tangent_space = txNormal.Sample(samLinear, input.Uv);  // Between 0..1
	N_tangent_space.y = 1 - N_tangent_space.y; //Invert normals correctly (green channel)
	N_tangent_space.xyz = N_tangent_space.xyz * 2 - 1.;  // Between -1..1

	float3 T = input.T.xyz;
	float3 B = cross(T, input.N) * input.T.w;
	float3x3 TBN = float3x3(T, B, input.N);
	float3 N = mul(N_tangent_space.xyz, TBN);   // Normal from NormalMap

	//Pass in specularFactor in normal alpha channel
	o_normal = encodeNormal(N, specularFactor);

	float3 cam2obj = input.WorldPos.xyz - CameraPosition.xyz;
	o_depth = dot(CameraFront, cam2obj) / CameraZFar;

	o_emissive.xyz = txEmissive.Sample(samLinear, input.Uv).xyz * o_albedo.xyz * glowAmount;
	o_emissive.a = rimFactor;
}