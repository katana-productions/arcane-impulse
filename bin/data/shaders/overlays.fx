#include "common.fx"
#include "gbuffer.inc"

//--------------------------------------------------------------------------------------
// This shader is expected to be used only with the mesh unitQuadXY.mesh
// Where the iPos goes from 0,0..1,1
void VS(
    in float4 iPos : POSITION
  , out float4 oPos : SV_POSITION
  , out float2 oTex0 : TEXCOORD0
)
{
  oPos = float4(iPos.x * 2 - 1., 1 - iPos.y * 2, 0, 1);
  oTex0 = iPos.xy;
}

//--------------------------------------------------------------------------------------
float4 PS(
    in float4 iPosition : SV_Position
  , in float2 iTex0 : TEXCOORD0
) : SV_Target
{
  
  float2 uvVig = iTex0;
  uvVig *=  1.0 - uvVig.yx;
  float vig = uvVig.x*uvVig.y * 25;
  vig = pow(vig, vignette);
  
  float greyScale = dot(saturate(txAlbedo.Sample(samLinear, iTex0).r + iluminance), float3(0.3, 0.59, 0.11));
  greyScale = pow(greyScale, colorShift);

  float4 greyColor = float4(greyScale, greyScale, greyScale, 1);
  greyColor.rgb *= intensity;
  
  float4 finalColor =  lerp(greyColor, txAlbedo.Sample(samLinear, iTex0), 1 - hitAmount);
  finalColor = lerp(float4(1,0,0,1), finalColor, saturate(vig));
  //return finalColor;

  finalColor.rgb *= 1 - black;
  finalColor.rgb = lerp(finalColor.rgb, float3(3,3,3), white);
  return finalColor;
}