//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
#include "common.fx"
#include "gbuffer.inc"
#include "pbr.inc"

//--------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VS(
  float4 Pos : POSITION,
  float3 N : NORMAL,
  float2 Uv: TEXCOORD0,
  float4 T : NORMAL1
)
{
  VS_OUTPUT output = (VS_OUTPUT)0;
  output.Pos = mul(Pos, World);
  output.WorldPos = output.Pos.xyz;
  output.Pos = mul(output.Pos, ViewProjection);
  output.N = mul(N, (float3x3)World);
  output.T = float4(mul(T.xyz, (float3x3)World), T.w);
  output.Uv = Uv;
  return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
void PS(
  VS_OUTPUT input
  , in float4 iPosition : SV_Position
  , out float4 o_albedo : SV_Target0
  , out float4 o_outlineDepth : SV_Target6
)
{
  //o_albedo = float4(1,0,1,0.2);
  o_outlineDepth = 1;
}

/*
  , out float4 o_albedo : SV_Target0
  , out float4 o_normal : SV_Target1
  , out float1 o_depth  : SV_Target2
  , out float4 o_emissive : SV_Target3
  , out float4 o_magicOutlines : SV_Target4
  , out float4 o_distortions : SV_Target5
  , out float4 o_outlineDepth : SV_Target6
  */