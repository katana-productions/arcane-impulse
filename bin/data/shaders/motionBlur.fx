#include "common.fx"
#include "gbuffer.inc"

//--------------------------------------------------------------------------------------
// This shader is expected to be used only with the mesh unitQuadXY.mesh
// Where the iPos goes from 0,0..1,1
void VS(
	in float4 iPos : POSITION
	, out float4 oPos : SV_POSITION
	, out float2 oTex0 : TEXCOORD0
)
{
	// Passthrough of coords and UV's
	oPos = float4(iPos.x * 2 - 1., 1 - iPos.y * 2, 0, 1);
	oTex0 = iPos.xy;
}

//--------------------------------------------------------------------------------------
float4 PS(
	in float4 iPosition : SV_Position
	, in float2 iTex0 : TEXCOORD0
) : SV_Target
{
	GBuffer g;
	decodeGBuffer(iPosition.xy, g);
	
	float4 color = txAlbedo.Sample(samLinear, iTex0);
	float depth = txGLinearDepth.Load(uint3(iPosition.xy, 0)).x;
	float3 wPos = getWorldCoords(iPosition.xy, depth);
	//return float4(depth, depth, depth, 1);
	//return float4(wPos, 1);
	float depthMask = 1 - g.emissive.a;
	depthMask *= pow(1 - iTex0.y, 2);
	//return float4(depthMask, depthMask, depthMask, 1);

	float4 currentPos = float4(iTex0.x * 2 - 1, (1 - iTex0.y) * 2 - 1, depth, 1);
	float4 previousPos = mul(float4(wPos, 1), PrevViewProjection);
	previousPos /= previousPos.w;
	//return currentPos;
	//return previousPos;

	float2 velocity = mblur_blur_speed * depthMask * (currentPos.xy - previousPos.xy) / (mblur_blur_samples * 2);
	iTex0 -= velocity * mblur_blur_samples * 0.5;
	//return float4((currentPos.xy - previousPos.xy).x, (currentPos.xy - previousPos.xy).y, 0, 1);
	//return float4(velocity, 0, 1);


	for (float i = 0; i < mblur_blur_samples; ++i, iTex0 += velocity)
	{
		float4 currentColor = txAlbedo.Sample(samClampLinear, iTex0);
		color += currentColor * depthMask;
	}

	//return float4(1, 1, 0, 1);
	float4 result =  (color / (1 + (mblur_blur_samples * depthMask)));
	return result;
}