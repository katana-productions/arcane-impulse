#include "common.fx"
#include "gbuffer.inc"

//--------------------------------------------------------------------------------------
// This shader is expected to be used only with the mesh unitQuadXY.mesh
// Where the iPos goes from 0,0..1,1
void VS(
    in float4 iPos : POSITION
  , out float4 oPos : SV_POSITION
  , out float2 oTex0 : TEXCOORD0
)
{
  // Passthrough of coords and UV's
  oPos = float4(iPos.x * 2 - 1., 1 - iPos.y * 2, 0, 1);
  oTex0 = iPos.xy;
}

//--------------------------------------------------------------------------------------
float4 PS(
    in float4 iPosition : SV_Position
  , in float2 iTex0 : TEXCOORD0
) : SV_Target
{
  float2 uv = float2(iPosition.x / xScreenRes, iPosition.y / yScreenRes);
  //return float4(uv, 0, 1);
  //return float4(iTex0, 0, 1);
  float2 noise = txAlbedo2.Sample(samLinear, iTex0).xy;
  noise.x *= 0.15;
  noise.y *= 0.15;
  return txAlbedo.Sample(samLinear, iTex0 + noise);
}