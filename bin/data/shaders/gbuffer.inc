
//--------------------------------------------------------------------------------------
struct GBuffer
{
  float3 wPos;
  float3 N;
  float3 albedo;
  float3 specular_color; //x is linear depth (Y & Z are free)
  float  metallic; //it's the specular map value
  float4 emissive; //rim factor is placed in alpha
  float3 view_dir;
};

//--------------------------------------------------------------------------------------
// Macro function to return information from gBuffer
void decodeGBuffer( 
     in float2 iPosition          // Screen coords
   , out GBuffer g
   ) {

  int3 ss_load_coords = uint3(iPosition.xy, 0);

  // Recover world position coords
  float  zlinear = txGLinearDepth.Load(ss_load_coords).x;
  g.wPos = getWorldCoords(iPosition.xy, zlinear);

  // Recuperar la normal en ese pixel. Sabiendo que se
  // guardó en el rango 0..1 pero las normales se mueven
  // en el rango -1..1
  float4 N_rt = txGNormal.Load(ss_load_coords);
  g.N = decodeNormal( N_rt.xyz );
  g.N = normalize( g.N );

  // Get other inputs from the GBuffer
  float4 albedo = txGAlbedo.Load(ss_load_coords);
  
  //Metallic actually contains the roughness * metallic. Both need to be white to have reflections
  //Used for specular spots
  g.metallic = albedo.a;

  // Apply gamma correction to albedo to bring it back to linear.
  albedo.rgb = pow(abs(albedo.rgb), 2.2f);

  g.albedo = albedo.rgb;

  g.emissive = txGEmissive.Load(ss_load_coords);

  g.specular_color.x = zlinear;
  g.specular_color.y = N_rt.a; //Specular Factor
  g.specular_color.z = 0; //FREE

  // Eye to object
  float3 incident_dir = normalize(g.wPos - CameraPosition.xyz);
  g.view_dir = -incident_dir;
}

/* WHERE ARE THE GREYSCALE METALLIC AND ROUGHNESS VALUES?
  // Get other inputs from the GBuffer
  float4 albedo = txGAlbedo.Load(ss_load_coords);
  // In the alpha of the albedo, we stored the metallic value
  // and in the alpha of the normal, we stored the roughness
  float  metallic = albedo.a;
         g.roughness = N_rt.a;
*/