//--------------------------------------------------------------------------------------
#include "common.fx"

//-------------------------`-------------------------------------------------------------
struct VS_OUTPUT {   // Vertex to pixel
	float4 Pos   : SV_POSITION;
	float2 Uv    : TEXCOORD0;
	float3 WorldPos : TEXCOORD1;
	float4 Color : COLOR;
	float2 Glow : TEXCOORD2;
};

//--------------------------------------------------------------------------------------
void PS(
	VS_OUTPUT input
	, out float4 o_albedo   : SV_Target0
	, out float4 o_distorsions : SV_Target5
)
{
	o_albedo = float4(0,0,0,0);
	
	o_distorsions = txNormal.Sample(samLinear, input.Uv) * input.Color.a * 0.55;
	o_distorsions.b = 0;
}