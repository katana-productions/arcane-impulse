#include "common.fx"
#include "gbuffer.inc"

//--------------------------------------------------------------------------------------
// This shader is expected to be used only with the mesh unitQuadXY.mesh
// Where the iPos goes from 0,0..1,1
void VS(
    in float4 iPos : POSITION
  , out float4 oPos : SV_POSITION
  , out float2 oTex0 : TEXCOORD0
)
{
  // Passthrough of coords and UV's
  oPos = float4(iPos.x * 2 - 1., 1 - iPos.y * 2, 0, 1);
  oTex0 = iPos.xy;
}

//--------------------------------------------------------------------------------------
float4 PS(
    in float4 iPosition : SV_Position
  , in float2 iTex0 : TEXCOORD0
) : SV_Target
{
  float4 albedo = txAlbedo.Sample(samLinear, iTex0);
  
  GBuffer g;
  decodeGBuffer(iPosition.xy, g);
  float4 originalValue = float4(g.N, log(g.specular_color.x));

  float depthFx = 1 - saturate(g.specular_color.x / depthFade); //depthFade
  //depthFx = 1;  //COMMENT TO AFFECT OUTLINE BASED ON DEPTH
  //return float4(depthFx, depthFx, depthFx, 1);

  float2 offsets[24] = {
	float2(-1, -1), //8 positions at ditance 1
	float2(-1, 0),
	float2(-1, 1),
	float2(0, -1),
	float2(0, 1),
	float2(1, -1),
	float2(1, 0),
	float2(1, 1),
	float2(-2, 2), //From here on all pixels at distance 2 from current
	float2(-2, 1),
	float2(-2, 0),
	float2(-2, -1),
	float2(-2, -2),
	float2(2, 2),
	float2(2, 1),
	float2(2, 0),
	float2(2, -1),
	float2(2, -2),
	float2(-1, 2),
	float2(0, 2),
	float2(1, 2),
	float2(-1, -2),
	float2(0, -2),
	float2(1, -2)
  };
  float4 sampledValue = float4(0,0,0,0);
  for(int j = 0; j < 24; j++) {
    GBuffer tempG;
    decodeGBuffer(iPosition.xy + offsets[j], tempG);
	
	sampledValue += float4(tempG.N, log(tempG.specular_color.x));
  }
  sampledValue /= 24;
  
  float edge = step(outlineThreshold, length(originalValue - sampledValue));

  
  //POSTERIZE
	//float _PosterizeGamma = posterizeGamma; //posterizeGamma
	//float _PosterizeNumColors = posterizeNumColors; //posterizeNumColors
	//albedo.xyz = pow(albedo.xyz, _PosterizeGamma) * _PosterizeNumColors;
	//albedo.xyz = floor(albedo.xyz) / _PosterizeNumColors;
	//albedo.xyz = pow(albedo.xyz, 1.0 / _PosterizeGamma);
  ////

  float outlineMask = 1 - txAlbedo2.Sample(samLinear, iTex0).r;
  float resultLine = edge * depthFx * outlineMask;
  //return float4(resultLine, resultLine, resultLine, 1);
  float4 result = lerp(albedo, float4(0,0,0,1), min(resultLine, maxOutline));
  
  //FOG
  float fog = min(saturate(g.specular_color.x / fogDepthFade), maxFog);
  fog = saturate(fog + (fog * fogContrast));
  //return float4(fog, fog, fog, 1);

  result.rgb = lerp(result.rgb, fogColor.rgb, saturate(fog));
  return result;
}
