//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
#include "common.fx"
#include "gbuffer.inc"
#include "pbr.inc"

float4 alphaBlend(float4 top, float4 bottom)
{
	float3 color = (top.rgb * top.a) + (bottom.rgb * (1 - top.a));
	float alpha = top.a + bottom.a * (1 - top.a);

	return float4(color, alpha);
}

//--------------------------------------------------------------------------------------
// Vertex Shader wave effect
//--------------------------------------------------------------------------------------

VS_OUTPUT VS(
	float4 Pos : POSITION,
	float3 N : NORMAL,
	float2 Uv : TEXCOORD0
)
{
	VS_OUTPUT output = (VS_OUTPUT)0;
	output.Pos = mul(Pos, World);
	output.WorldPos = output.Pos.xyz;
	output.Pos = mul(output.Pos, ViewProjection);
	output.N = normalize(mul(N, World));
	output.Uv = Uv;
	return output;
}

VS_OUTPUT VS_skin(
  VS_INPUT input,
  VS_SKINNING skinning
)
{
  float4x4 SkinMatrix = getSkinMtx(skinning);
  return runObjVS(input, SkinMatrix);
}

//--------------------------------------------------------------------------------------
// Pixel Shader that does the water effect
//--------------------------------------------------------------------------------------

void PS(
  VS_OUTPUT input
  ,in float4 iPosition : SV_Position
  , out float4 o_albedo   : SV_Target0
  , out float4 o_normal : SV_Target1
  , out float1 o_depth    : SV_Target2
  , out float4 o_emissive : SV_Target3
)
{
  // Clip if not enabled
  clip(water_enabled - 0.1);

  // UV and uv distorsion
  float2 uv = input.Uv;
  uv.x += GlobalWorldTime * water_speed;
  uv.x *= water_stretch; //Stretch y coords;

  float2 uv_noise = input.Uv * 0.5;
  uv_noise.x -= GlobalWorldTime * dist_speed;
  uv_noise.y += GlobalWorldTime * dist_speed * 0.5;
  uv_noise.x *= dist_stretch; //Stretch y coords;
  
  float2 uv_foam = input.Uv * 2;
  uv_foam.x += GlobalWorldTime * max(water_speed, dist_speed);
  
  //Sample Distortions texture (txEmissive) to distort uv
  float2 dist = txEmissive.Sample(samLinear, uv_noise).xy;
  uv += ((dist * 2) - 1) * dist_ammount;
  
  // Banding for Posterize
  //float nBands = 5;
  float noise = txAlbedo.Sample(samLinear, uv).x;
  float posterize = round(noise * posterize_bands) / posterize_bands; //Banding for toon look
  noise = lerp(noise, posterize, posterize_enabled);
  //o_albedo = float4(noise, noise, noise, 1);
  
  o_albedo = float4(float3(noise, noise, noise) * lerp(black_color.xyz, white_color.xyz, noise), 1);

  noise = pow(noise, contrast);
  noise = saturate(noise + dist_ammount);
  o_albedo = float4(o_albedo.xyz * noise * (water_glow + 1), 1);
  
  // Normal mapping
  float4 N_tangent_space = txNormal.Sample(samLinear, input.Uv);  // Between 0..1
  N_tangent_space.y = 1 - N_tangent_space.y; //Invert normals correctly (green channel)
  N_tangent_space.xyz = N_tangent_space.xyz * 2 - 1.;  // Between -1..1
  
  float3 T = input.T.xyz;
  float3 B = cross(T, input.N) * input.T.w;
  float3x3 TBN = float3x3(T, B, input.N);
  float3 N = mul(N_tangent_space.xyz, TBN);   // Normal from NormalMap
  
  //Pass in specularFactor in normal alpha channel
  o_normal = encodeNormal(N, 0);
  
  float3 cam2obj = input.WorldPos.xyz - CameraPosition.xyz;
  o_depth = dot(CameraFront, cam2obj) / CameraZFar;
  
  float3 normal = decodeNormal(o_normal.xyz);
  //normal = mul( normal, (float3x3)View).xyz; //To view space
  float3 incident_dir = normalize(input.WorldPos.xyz - CameraPosition.xyz);
  float  NdV = saturate(dot(normal, -incident_dir));
  NdV = pow(NdV * NdV, 5);
  //float rim = smoothstep(0.35, 0.5, 1 - pow(NdV * NdV, 0.02));
  //o_albedo.xyz = float3(NdV, NdV, NdV);
  //o_albedo.xyz = float3(rim, rim, rim);
  
  o_emissive = float4(o_albedo.xyz * (water_glow + 1) * saturate(NdV + 0.1), 1);
  
  
  //float3 N = txGNormal.Load(ss_load_coords).xyz;
  //float3 q = float4( decodeNormal(N), 1);
  //return float4( mul( q, (float3x3)View).xyz, 1);
}
//------------------------------------------------------------------------

void PS_Orb1(
  VS_OUTPUT input
  ,in float4 iPosition : SV_Position
  , out float4 o_albedo   : SV_Target0
  , out float4 o_normal : SV_Target1
  , out float1 o_depth    : SV_Target2
  , out float4 o_emissive : SV_Target3
)
{
  // Clip if not enabled
  clip(water_enabled - 0.1);

  // UV and uv distorsion
  float2 uv = input.Uv;
  uv.x += GlobalWorldTime * water_speed;
  uv.x *= water_stretch; //Stretch y coords;

  float2 uv_noise = input.Uv * 0.5;
  uv_noise.x -= GlobalWorldTime * dist_speed;
  uv_noise.y += GlobalWorldTime * dist_speed * 0.5;
  uv_noise.x *= dist_stretch; //Stretch y coords;
  
  float2 uv_foam = input.Uv * 2;
  uv_foam.x += GlobalWorldTime * max(water_speed, dist_speed);
  
  //Sample Distortions texture (txEmissive) to distort uv
  float2 dist = txEmissive.Sample(samLinear, uv_noise).xy;
  uv += ((dist * 2) - 1) * dist_ammount;
  
  // Banding for Posterize
  //float nBands = 5;
  float noise = txAlbedo.Sample(samLinear, uv).x;
  float posterize = round(noise * posterize_bands) / posterize_bands; //Banding for toon look
  noise = lerp(noise, posterize, posterize_enabled);
  //o_albedo = float4(noise, noise, noise, 1);
  
  o_albedo = float4(float3(noise, noise, noise) * lerp(black_color.xyz, white_color.xyz, noise), 1);

  noise = pow(noise, contrast);
  noise = saturate(noise + dist_ammount);
  o_albedo = float4(o_albedo.xyz * noise * (water_glow + 1), 1);
  
  // Normal mapping
  float4 N_tangent_space = txNormal.Sample(samLinear, input.Uv);  // Between 0..1
  N_tangent_space.y = 1 - N_tangent_space.y; //Invert normals correctly (green channel)
  N_tangent_space.xyz = N_tangent_space.xyz * 2 - 1.;  // Between -1..1
  
  float3 T = input.T.xyz;
  float3 B = cross(T, input.N) * input.T.w;
  float3x3 TBN = float3x3(T, B, input.N);
  float3 N = mul(N_tangent_space.xyz, TBN);   // Normal from NormalMap
  
  //Pass in specularFactor in normal alpha channel
  o_normal = encodeNormal(N, 0);
  
  float3 cam2obj = input.WorldPos.xyz - CameraPosition.xyz;
  o_depth = dot(CameraFront, cam2obj) / CameraZFar;
  
  float3 normal = decodeNormal(o_normal.xyz);
  //normal = mul( normal, (float3x3)View).xyz; //To view space
  float3 incident_dir = normalize(input.WorldPos.xyz - CameraPosition.xyz);
  float  NdV = saturate(dot(normal, -incident_dir));
  NdV = pow(NdV * NdV, 5);
  float rim = smoothstep(0.2, 0.6, 1 - pow(NdV * NdV, 0.02));
  //o_albedo.xyz = float3(NdV, NdV, NdV);
  //o_albedo.xyz = float3(rim, rim, rim);
  o_albedo.xyz += o_albedo.xyz * rim * 30;
  
  o_emissive = float4(o_albedo.xyz * (water_glow + 1) * saturate(NdV + 0.1), 1);
  
  
  //float3 N = txGNormal.Load(ss_load_coords).xyz;
  //float3 q = float4( decodeNormal(N), 1);
  //return float4( mul( q, (float3x3)View).xyz, 1);
}

void PS_Orb2(
  VS_OUTPUT input
  ,in float4 iPosition : SV_Position
  , out float4 o_albedo   : SV_Target0
  , out float4 o_normal : SV_Target1
  , out float1 o_depth    : SV_Target2
  , out float4 o_emissive : SV_Target3
)
{
  // Clip if not enabled
  clip(water_enabled - 0.1);

  // UV and uv distorsion
  float2 uv = input.Uv;
  uv.x += GlobalWorldTime * water_speed;
  uv.x *= water_stretch; //Stretch y coords;

  float2 uv_noise = input.Uv * 0.5;
  uv_noise.x -= GlobalWorldTime * dist_speed;
  uv_noise.y += GlobalWorldTime * dist_speed * 0.5;
  uv_noise.x *= dist_stretch; //Stretch y coords;
  
  float2 uv_foam = input.Uv * 2;
  uv_foam.x += GlobalWorldTime * max(water_speed, dist_speed);
  
  //Sample Distortions texture (txEmissive) to distort uv
  float2 dist = txEmissive.Sample(samLinear, uv_noise).xy;
  uv += ((dist * 2) - 1) * dist_ammount;
  
  // Banding for Posterize
  //float nBands = 5;
  float noise = txAlbedo.Sample(samLinear, uv).x;
  float posterize = round(noise * posterize_bands) / posterize_bands; //Banding for toon look
  noise = lerp(noise, posterize, posterize_enabled);
  //o_albedo = float4(noise, noise, noise, 1);
  
  o_albedo = float4(float3(noise, noise, noise) * lerp(black_color.xyz, white_color.xyz, noise), 1);

  noise = pow(noise, contrast);
  noise = saturate(noise + dist_ammount);
  o_albedo = float4(o_albedo.xyz * noise * (water_glow + 1), 1);
  
  // Normal mapping
  float4 N_tangent_space = txNormal.Sample(samLinear, input.Uv);  // Between 0..1
  N_tangent_space.y = 1 - N_tangent_space.y; //Invert normals correctly (green channel)
  N_tangent_space.xyz = N_tangent_space.xyz * 2 - 1.;  // Between -1..1
  
  float3 T = input.T.xyz;
  float3 B = cross(T, input.N) * input.T.w;
  float3x3 TBN = float3x3(T, B, input.N);
  float3 N = mul(N_tangent_space.xyz, TBN);   // Normal from NormalMap
  
  //Pass in specularFactor in normal alpha channel
  o_normal = encodeNormal(N, 0);
  
  float3 cam2obj = input.WorldPos.xyz - CameraPosition.xyz;
  o_depth = dot(CameraFront, cam2obj) / CameraZFar;
  
  float3 normal = decodeNormal(o_normal.xyz);
  //normal = mul( normal, (float3x3)View).xyz; //To view space
  float3 incident_dir = normalize(input.WorldPos.xyz - CameraPosition.xyz);
  float  NdV = saturate(dot(normal, -incident_dir));
  NdV = pow(NdV * NdV, 5);
  float rim = smoothstep(0.2, 0.6, 1 - pow(NdV * NdV, 0.02));
  //o_albedo.xyz = float3(NdV, NdV, NdV);
  //o_albedo.xyz = float3(rim, rim, rim);
  o_albedo.xyz += float3(0.05,1,1) * o_albedo.z * rim * 20;
  
  o_emissive = float4(o_albedo.xyz * (water_glow + 1) * saturate(NdV + 0.1), 1);
  
  
  //float3 N = txGNormal.Load(ss_load_coords).xyz;
  //float3 q = float4( decodeNormal(N), 1);
  //return float4( mul( q, (float3x3)View).xyz, 1);
}