#include "common.fx"
#include "gbuffer.inc"

//--------------------------------------------------------------------------------------
// This shader is expected to be used only with the mesh unitQuadXY.mesh
// Where the iPos goes from 0,0..1,1
void VS(
    in float4 iPos : POSITION
  , out float4 oPos : SV_POSITION
  , out float2 oTex0 : TEXCOORD0
)
{
  // Passthrough of coords and UV's
  oPos = float4(iPos.x * 2 - 1., 1 - iPos.y * 2, 0, 1);
  oTex0 = iPos.xy;
}

//--------------------------------------------------------------------------------------
float4 PS(
    in float4 iPosition : SV_Position
  , in float2 iTex0 : TEXCOORD0
) : SV_Target
{ 
  // txAlbedo3 is the outline written texture in the deferred
  // txAlbedo2 is txAlbedo3 but BLURRED
  float4 outlineColor = saturate(txAlbedo2.Sample(samClampLinear, iTex0) - txAlbedo3.Sample(samClampLinear, iTex0));
  outlineColor.a = 1;
  
  float outlineAmount = (outlineColor.r + outlineColor.g + outlineColor.b)/ 3;
  outlineColor.rgb *= 10;
	
  return lerp(txAlbedo.Sample(samLinear, iTex0), outlineColor, outlineAmount);  
  
  //=====================================================================================================
  //////8 dir check outline approach
  //float outlineDistAmmount = 0.0002;
  //float isWhite = txAlbedo2.Sample(samLinear, iTex0).r;
  //float result = 0;
  //
  //// Direction test
  //float left = txAlbedo2.Sample(samClampLinear, iTex0 + float2(outlineDistAmmount, 0)).r;
  //float right = txAlbedo2.Sample(samClampLinear, iTex0 - float2(outlineDistAmmount, 0)).r;
  //float up = txAlbedo2.Sample(samClampLinear, iTex0 + float2(0, outlineDistAmmount)).r;
  //float down = txAlbedo2.Sample(samClampLinear, iTex0 - float2(0, outlineDistAmmount)).r;
  //
  //float upLeft = txAlbedo2.Sample(samClampLinear, iTex0 + float2(outlineDistAmmount, outlineDistAmmount)).r;
  //float upRight = txAlbedo2.Sample(samClampLinear, iTex0 + float2(-outlineDistAmmount, outlineDistAmmount)).r;
  //float downLeft = txAlbedo2.Sample(samClampLinear, iTex0 + float2(outlineDistAmmount, -outlineDistAmmount)).r;
  //float downRight = txAlbedo2.Sample(samClampLinear, iTex0 + float2(-outlineDistAmmount, -outlineDistAmmount)).r;
  //
  //result = left + right + up + down + upLeft + upRight + downLeft + downRight;
  //result = saturate(result);
  //
  //// Erase inner white, we only want outline
  //result *= 1 - isWhite;
  //
  //// Fade in of the color
  ////outlineColor = smoothstep(float4(0,0,0,1), outlineColor, isWhite);
  //
  //outlineColor.rgb *= 20; //Overexposure won't affect bloom :(
  //return lerp(txAlbedo.Sample(samLinear, iTex0), outlineColor, result);
  ////return lerp(lerp(txAlbedo.Sample(samLinear, iTex0), outlineColor, result), txAlbedo.Sample(samLinear, iTex0), isWhite);
}
