//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
#include "common.fx"
#include "gbuffer.inc"
#include "pbr.inc"

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------

VS_OUTPUT VS(
  VS_INPUT input
)
{
  return runObjVS(input, World);
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------

void PS(
  VS_OUTPUT input
  ,in float4 iPosition : SV_Position
  , out float4 o_albedo   : SV_Target0
  , out float4 o_emissive : SV_Target3
  , out float4 o_outlines : SV_Target4
)
{
  // just to test
  //o_outlines = float4(0,0,1,1);

  //clip if black (disabled) to get rid of weird occlusions
  clip(outline_color.r + outline_color.g + outline_color.b - 0.01);

  o_outlines = outline_color;
  o_outlines.rgb *= outline_glow;
}
//------------------------------------------------------------------------