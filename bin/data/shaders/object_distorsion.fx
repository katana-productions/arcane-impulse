//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
#include "common.fx"
#include "gbuffer.inc"

//--------------------------------------------------------------------------------------
struct VS_OUTPUT
{
  float4 Pos      : SV_POSITION;
  float3 N        : NORMAL;
  float2 Uv       : TEXCOORD0;
  float3 WorldPos : TEXCOORD1;
  float4 T        : NORMAL1;
};
//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VS(
	float4 Pos : POSITION,
	float3 N : NORMAL,
	float2 Uv : TEXCOORD0
)
{
	VS_OUTPUT output = (VS_OUTPUT)0;
	output.Pos = mul(Pos, World);
	output.WorldPos = output.Pos.xyz;
	output.Pos = mul(output.Pos, ViewProjection);
	output.N = normalize(mul(N, World));
	output.Uv = Uv;
	return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
void PS(
	VS_OUTPUT input
	, out float4 o_albedo : SV_Target0
	, out float4 o_normal : SV_Target1
	, out float1 o_depth  : SV_Target2
	, out float4 o_emissive : SV_Target3
	, out float4 o_distorsions : SV_Target5
)
{	
	float2 scrollUv = (input.Uv * 2) + (0.2 * GlobalWorldTime * float2(0,0));
	//-1 to 1 range
	//  float4 albedo_color = txAccLights.Sample(samClampLinear, uv);
	float4 noise = txNormal.Sample(samLinear, scrollUv);
	
	// Compute coords in screen space to sample the color under me
	noise.x = (noise.x * 2) - 1;
	noise.y = (noise.y * 2) - 1;
	float3 wPos = input.WorldPos.xyz + (noise.xyz * 0.075);
	float4 viewSpace = mul( float4(wPos,1.0), ViewProjection );
	float3 homoSpace = viewSpace.xyz / viewSpace.w;
	float2 uv = float2( ( homoSpace.x + 1.0 ) * 0.5, ( 1.0 - homoSpace.y ) * 0.5 );
	o_albedo = txAccLights.Sample(samClampLinear, uv);
	o_albedo.rgb *= max(1, glowAmount);
	o_albedo.a = 1;
	noise *=  (2 - 1.0);
	o_distorsions = float4(noise.xy, 0, 1);
	
	float3 cam2obj = input.WorldPos.xyz - CameraPosition.xyz;
	o_depth = dot(CameraFront, cam2obj) / CameraZFar;
	
	o_emissive.rgb = o_albedo.rgb * glowAmount;
	o_emissive.a = 1;
	
	// Normal mapping
	float4 N_tangent_space = txNormal.Sample(samLinear, scrollUv);  // Between 0..1
	N_tangent_space.y = 1 - N_tangent_space.y; //Invert normals correctly (green channel)
	N_tangent_space.xyz = N_tangent_space.xyz * 2 - 1.;  // Between -1..1

	float3 T = input.T.xyz;
	float3 B = cross(T, input.N) * input.T.w;
	float3x3 TBN = float3x3(T, B, input.N);
	float3 N = mul(N_tangent_space.xyz, TBN);   // Normal from NormalMap

	//Pass in specularFactor in normal alpha channel
	o_normal = encodeNormal(N, specularFactor);
}

void PS_Albedo(
	VS_OUTPUT input
	, in float4 iPosition : SV_Position
	, out float4 o_albedo : SV_Target0
	, out float4 o_normal : SV_Target1
	, out float1 o_depth  : SV_Target2
	, out float4 o_emissive : SV_Target3
	, out float4 o_distorsions : SV_Target5
	, out float4 o_outlineMask : SV_Target6
)
{	
    float2 inUv = input.Uv * 3;
	
	// Normal mapping
	float4 N_tangent_space = txNormal.Sample(samLinear, inUv);  // Between 0..1
	N_tangent_space.x = 1 - N_tangent_space.x;
	N_tangent_space.xyz = N_tangent_space.xyz * 2 - 1.;  // Between -1..1

	float3 T = input.T.xyz;
	float3 B = cross(T, input.N) * input.T.w;
	float3x3 TBN = float3x3(T, B, input.N);
	float3 N = mul(N_tangent_space.xyz, TBN);   // Normal from NormalMap
	float dotP = saturate(dot(N, CameraFront));
	float dotPMod = max(dotP, 0.45); 
	
	//float3 wPos = input.WorldPos.xyz;
	//float4 viewSpace = mul( float4(wPos,1.0), ViewProjection );
	//float3 homoSpace = viewSpace.xyz / viewSpace.w;
	//float2 uv = float2((homoSpace.x + 1.0) * 0.5, (1.0 - homoSpace.y) * 0.5);
	//float2 center = float2(0.5, 0.5);
	//float dist = length(center - uv);
	//dist = 1 - dist;
	//dist = pow(dist * dist, 10);
	
	float3 colorGlass = float3(0.0,0.3,0.3);
	o_albedo = txAlbedo.Sample(samLinear, inUv);
	o_albedo.rgb *= ObjColor * colorGlass * dotPMod;
	//o_albedo += dist;
	o_albedo.a = 0.4;
	//if(o_albedo.r < 0.005) o_albedo.a = 1;

    GBuffer g;
    decodeGBuffer(iPosition.xy, g);
    float3 cam2obj = input.WorldPos.xyz - CameraPosition.xyz;
    float myDepth = saturate(dot(CameraFront, cam2obj) / CameraZFar);
	myDepth = 1 - myDepth;
	float minDepth = 0.95;
	myDepth = saturate((myDepth - minDepth) / (1 - minDepth));
	
	float4 fakeReflect = txEmissive.Sample(samLinear, input.Uv);
	float factorSpec = fakeReflect.a * pow(dotP, 1);
	o_albedo.rgb =  lerp(o_albedo.rgb ,fakeReflect.r, saturate(factorSpec - 0.5) * myDepth);
	
	//o_albedo.rgb = float3(myDepth, myDepth, myDepth);

	// Save roughness in the alpha coord of the N render target
	float roughness = txSpecular.Sample(samLinear, inUv).r;
	o_normal = encodeNormal(N, roughness);
	
	o_depth = dot(CameraFront, cam2obj) / CameraZFar;
	
	o_emissive.xyz = txEmissive.Sample(samLinear, inUv).xyz * o_albedo.xyz * 1000;
	o_emissive.a = 1;
	
	o_outlineMask = float4(1,1,1,1);
}
