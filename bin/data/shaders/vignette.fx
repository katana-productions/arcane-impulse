#include "common.fx"
#include "gbuffer.inc"

//--------------------------------------------------------------------------------------
// This shader is expected to be used only with the mesh unitQuadXY.mesh
// Where the iPos goes from 0,0..1,1
void VS(
    in float4 iPos : POSITION
  , out float4 oPos : SV_POSITION
  , out float2 oTex0 : TEXCOORD0
)
{
  // Passthrough of coords and UV's
  oPos = float4(iPos.x * 2 - 1., 1 - iPos.y * 2, 0, 1);
  oTex0 = iPos.xy;
}

//--------------------------------------------------------------------------------------
float4 PS(
    in float4 iPosition : SV_Position
  , in float2 iTex0 : TEXCOORD0
) : SV_Target
{
  //Vignette----------------------------------------------------------------------------------------------------------
  float2 uv = float2(iPosition.x / xScreenRes, iPosition.y / yScreenRes);
  float2 uvVig = uv;
  uvVig *=  1.0 - uvVig.yx;
  float vig = uvVig.x*uvVig.y * vigIntensity;
  vig = pow(vig, vigExtend);
  //return float4(vig, vig, vig,1);


  //Chrom Aberration--------------------------------------
  //https://www.shadertoy.com/view/XlfBzs

  float4 channelFactor = float4(0.9, 1.0, 1.3, 1.0);
  float4 strength = chromAberrationAmount * 0.5 * channelFactor;

  uv = float2(iPosition.x / xScreenRes, iPosition.y / yScreenRes);
  float2 p = (uv - 0.5) * 2.0;
  float theta = atan2(p.y, p.x);
  float rd = length(p);
  float blurMask = saturate(pow(rd, 5)) * -chromAberrationAmount;
  //return lerp(txAlbedo.Sample(samLinear, iTex0), txAlbedo2.Sample(samLinear, iTex0), blurMask);
  //return float4(blurMask, blurMask, blurMask, 1);

  float4 ru = rd * (1.0 + strength * rd * rd);
  float2 dir = float2(cos(theta), sin(theta)); 
  float2 qr = (dir * ru.x / 2.0) + 0.5;
  float2 qg = (dir * ru.y / 2.0) + 0.5;
  float2 qb = (dir * ru.z / 2.0) + 0.5;
  float2 qa = (dir * ru.w / 2.0) + 0.5;

  float4 regular = float4(txAlbedo.Sample(samClampLinear, qr).r, txAlbedo.Sample(samClampLinear, qg).g, txAlbedo.Sample(samClampLinear, qb).b, 1);
  float4 blurred = float4(txAlbedo2.Sample(samClampLinear, qr).r, txAlbedo2.Sample(samClampLinear, qg).g, txAlbedo2.Sample(samClampLinear, qb).b, 1);
  //return regular * vig;
  return lerp(regular, blurred, blurMask) * vig; //Blurred Output
  
  return txAlbedo2.Sample(samClampLinear, iTex0) * vig;
  return txAlbedo.Sample(samClampLinear, iTex0) * vig;
  }