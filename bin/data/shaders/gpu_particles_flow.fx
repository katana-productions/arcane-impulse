//--------------------------------------------------------------------------------------
#include "common.fx"
#include "particles.inc"
#include "Quaternion.hlsl"

#define NUM_PARTICLES_PER_THREAD_GROUP  16
#define OFFSET_NUM_PARTICLES_TO_DRAW    4

// --------------------------------------------------------------
struct TInstance {
  float3 pos;
  uint   unique_id;
  
  float3 dir;
  float  global_speed;

  float3 acc;
  float  noise_frequency;

  float3 global_acc;
  float  noise_factor;

  float3 prev_pos;
  float time_next_noise;

  float4 color;

  float  scale;
  float  time_normalized;     // 0..1 When time reaches zero the particles dies.
  float  time_factor;         // real time adds to time_normalized by this factor
  float  modifier_scale;

  bool billboard;
  bool rotation_over_direction;
  float dummy;
  float glow;

  float3 forward;
  float dummy2;

  float4 rotation_quat[8];
  float4 current_quat;

  float3 center_pos;
  float rotation_speed;

  float3 displacement_pos;
  bool world_space;
};

struct TSystem {
  float  time_to_next_spawn;
  uint   num_particles_to_update;
  uint   next_unique_id;
  float  life_time;

  float3 dummy;
  float delay_time;
};


// ----------------------------------------------------------
// The spawn fn to customize
TInstance spawnParticleCone( uint unique_id ) {

  float2 rnd2 = hash2( unique_id ) * 2.0 - 1.0;    // -1, 1
  float2 rnd3 = hash2(unique_id + 57);
  float2 rnd4 = hash2(unique_id + 63);
  float2 rnd5 = hash2(unique_id + 73) * 2.0 - 1.0;    // -1, 1
  float2 rnd6 = hash2(unique_id + 7) * 2.0 - 1.0;    // -1, 1
  float2 rnd7 = hash2(unique_id + 83);
  float2 rnd8 = hash2(unique_id + 87);

  float  duration = emitter_duration.x + ( emitter_duration.y - emitter_duration.x ) * rnd3.x;
  float  speed  = emitter_speed.x + ( emitter_speed.y - emitter_speed.x ) * rnd3.y;
  float gravity_factor = emitter_gravity_factor.x + (emitter_gravity_factor.y - emitter_gravity_factor.x) * rnd4.x;
  float modifier_size_factor = emitter_modifier_size_factor.x + (emitter_modifier_size_factor.y - emitter_modifier_size_factor.x) * rnd4.y;
  float noise_frequency = emitter_noise_frequency.x + (emitter_noise_frequency.y - emitter_noise_frequency.x) * rnd7.y;
  float noise_factor = emitter_noise_factor.x + (emitter_noise_factor.y - emitter_noise_factor.x) * rnd8.x;
  float rotation_speed = emitter_rotation_speed.x + (emitter_rotation_speed.y - emitter_rotation_speed.x) * rnd8.y;

  float2 rnd9 = hash2(unique_id + 17);
  float2 rnd10 = hash2(unique_id + 33);

  float initial_yaw = emitter_initial_yaw.x + (emitter_initial_yaw.y - emitter_initial_yaw.x) * rnd9.x;
  float initial_pitch = emitter_initial_pitch.x + (emitter_initial_pitch.y - emitter_initial_pitch.x) * rnd9.y;
  float initial_roll = emitter_initial_roll.x + (emitter_initial_roll.y - emitter_initial_roll.x) * rnd10.x;

  float4 init_quaternion = frowYawPitchRoll(initial_yaw, initial_pitch, initial_roll);


  TInstance p;
  p.center_pos = emitter_center;
  p.displacement_pos = float3(0, 0, 0);

  p.acc = float3(0,-9.8,0) * gravity_factor;
  p.dir = emitter_dir;
  p.unique_id = unique_id;
  p.time_normalized = 0.0;
  p.time_factor = 1.0 / duration;
  p.scale = 1.0;
  p.color = float4(1,1,0,1);
  p.global_speed = speed;
  p.noise_frequency = noise_frequency;
  p.noise_factor = noise_factor;
  p.modifier_scale = modifier_size_factor;
  p.global_acc = float3(0,0,0);
  p.time_next_noise = 1 / noise_frequency;
  p.billboard = emitter_billboard;
  p.rotation_over_direction = emitter_rotation_over_direction;
  p.current_quat = init_quaternion;
  p.glow = emitter_glow;
  p.forward = emitter_forward;
  p.dummy = 0;
  p.dummy2 = 0;
  p.rotation_speed = rotation_speed;
  p.world_space = emitter_world_space;


  float rotation_yaw = 0;
  float rotation_pitch = 0;
  float rotation_roll = 0;

  for (int i = 0; i < 8; i++) {
	  rotation_yaw = emitter_yaw_over_time[i].x + (emitter_yaw_over_time[i].y - emitter_yaw_over_time[i].x) * rnd10.y;
	  rotation_pitch = emitter_pitch_over_time[i].x + (emitter_pitch_over_time[i].y - emitter_pitch_over_time[i].x) * rnd10.y;
	  rotation_roll = emitter_roll_over_time[i].x + (emitter_roll_over_time[i].y - emitter_roll_over_time[i].x) * rnd10.y;

	  p.rotation_quat[i] = frowYawPitchRoll(rotation_yaw, rotation_pitch, rotation_roll);
  }

  p.center_pos += normalize((float3(1,1,1) - abs(p.dir)) * float3(rnd2.x, rnd6.x, rnd2.y)) * (1 - (rnd3.x * emitter_shell_thickness)) * emitter_center_radius;

  //random relativo direccion
  p.dir = normalize(p.dir + ((float3(1, 1, 1) - abs(p.dir)) * normalize(float3(rnd2.x, rnd6.x, rnd2.y)) * emitter_dir_aperture));

  if (p.rotation_over_direction) {
	  float4 quat_between_vectors = normalize(from_to_rotation(p.forward, p.dir));
	  p.current_quat = quat_between_vectors;
  }

  if (p.world_space) {
	  p.center_pos += emitter_offset_center;
  }
  p.pos = p.center_pos;
  p.prev_pos = p.pos;

  return p;
}

TInstance spawnParticleSphere(uint unique_id) {
	float2 rnd1 = hash2(unique_id + 2) * 2 - 1.0;
	float2 rnd2 = hash2(unique_id) * 2.0 - 1.0;
	float2 rnd3 = hash2(unique_id + 57);
	float2 rnd4 = hash2(unique_id + 63);
	float2 rnd5 = hash2(unique_id + 83);
	float2 rnd6 = hash2(unique_id + 97);


	float  duration = emitter_duration.x + (emitter_duration.y - emitter_duration.x) * rnd3.x;
	float  speed = emitter_speed.x + (emitter_speed.y - emitter_speed.x) * rnd3.y;
	float gravity_factor = emitter_gravity_factor.x + (emitter_gravity_factor.y - emitter_gravity_factor.x) * rnd4.x;
	float modifier_size_factor = emitter_modifier_size_factor.x + (emitter_modifier_size_factor.y - emitter_modifier_size_factor.x) * rnd4.y;
	float noise_frequency = emitter_noise_frequency.x + (emitter_noise_frequency.y - emitter_noise_frequency.x) * rnd5.y;
	float noise_factor = emitter_noise_factor.x + (emitter_noise_factor.y - emitter_noise_factor.x) * rnd6.x;
	float rotation_speed = emitter_rotation_speed.x + (emitter_rotation_speed.y - emitter_rotation_speed.x) * rnd6.y;

	float2 rnd9 = hash2(unique_id + 17);
	float2 rnd10 = hash2(unique_id + 33);

	float initial_yaw = emitter_initial_yaw.x + (emitter_initial_yaw.y - emitter_initial_yaw.x) * rnd9.x;
	float initial_pitch = emitter_initial_pitch.x + (emitter_initial_pitch.y - emitter_initial_pitch.x) * rnd9.y;
	float initial_roll = emitter_initial_roll.x + (emitter_initial_roll.y - emitter_initial_roll.x) * rnd10.x;

	float4 init_quaternion = frowYawPitchRoll(initial_yaw, initial_pitch, initial_roll);

	TInstance p;
	p.center_pos = emitter_center;
	p.displacement_pos = float3(0, 0, 0);	
	
	p.acc = float3(0, -9.8, 0) * gravity_factor;
	p.unique_id = unique_id;
	p.time_normalized = 0.0;
	p.time_factor = 1.0 / duration;
	p.scale = 1.0;
	p.color = float4(1, 0, 0, 1);
	p.global_speed = speed;
	p.noise_frequency = noise_frequency;
	p.noise_factor = noise_factor;
	p.modifier_scale = modifier_size_factor;
	p.global_acc = float3(0, 0, 0);
	p.time_next_noise = 1 / noise_frequency;
	p.billboard = emitter_billboard;
	p.rotation_over_direction = emitter_rotation_over_direction;
	p.current_quat = init_quaternion;
	p.glow = emitter_glow;
	p.forward = emitter_forward;
	p.dummy = 0;
	p.dummy2 = 0;
	p.rotation_speed = rotation_speed;
	p.world_space = emitter_world_space;

	float rotation_yaw = 0;
	float rotation_pitch = 0;
	float rotation_roll = 0;

	for (int i = 0; i < 8; i++) {
		rotation_yaw = emitter_yaw_over_time[i].x + (emitter_yaw_over_time[i].y - emitter_yaw_over_time[i].x) * rnd10.y;
		rotation_pitch = emitter_pitch_over_time[i].x + (emitter_pitch_over_time[i].y - emitter_pitch_over_time[i].x) * rnd10.y;
		rotation_roll = emitter_roll_over_time[i].x + (emitter_roll_over_time[i].y - emitter_roll_over_time[i].x) * rnd10.y;

		p.rotation_quat[i] = frowYawPitchRoll(rotation_yaw, rotation_pitch, rotation_roll);
	}

	p.dir = normalize(float3(rnd2.x, rnd1.x, rnd2.y)) ;
	p.center_pos += p.dir * (1 - (rnd3.x * emitter_shell_thickness)) * emitter_center_radius;

	if (p.rotation_over_direction) {
		float4 quat_between_vectors = normalize(from_to_rotation(p.forward, p.dir));
		p.current_quat = quat_between_vectors;
	}

	if (p.world_space) {
		p.center_pos += emitter_offset_center;
	}
	p.pos = p.center_pos;
	p.prev_pos = p.pos;

	return p;
}

TInstance spawnParticleCube(uint unique_id) {
	float2 rnd1 = hash2(unique_id + 2) * 3; // [0 , 3 ] 

	rnd1.x = trunc(rnd1.x) % 3; //random 0, 1 ,2
	rnd1.y = round(rnd1.y % 1) * 2 - 1; //random -1 || 1

	float2 rnd2 = hash2(unique_id) * 2 -1; // [-1 , 1]

	float2 rnd3 = hash2(unique_id + 57);
	float2 rnd4 = hash2(unique_id + 63);
	float2 rnd5 = hash2(unique_id + 77) * 2 - 1;
	float2 rnd6 = hash2(unique_id + 79) * 2 - 1;
	float2 rnd7 = hash2(unique_id + 83);
	float2 rnd8 = hash2(unique_id + 93);

	float  duration = emitter_duration.x + (emitter_duration.y - emitter_duration.x) * rnd3.x;
	float  speed = emitter_speed.x + (emitter_speed.y - emitter_speed.x) * rnd3.y;
	float gravity_factor = emitter_gravity_factor.x + (emitter_gravity_factor.y - emitter_gravity_factor.x) * rnd4.x;
	float modifier_size_factor = emitter_modifier_size_factor.x + (emitter_modifier_size_factor.y - emitter_modifier_size_factor.x) * rnd4.y;
	float noise_frequency = emitter_noise_frequency.x + (emitter_noise_frequency.y - emitter_noise_frequency.x) * rnd7.y;
	float noise_factor = emitter_noise_factor.x + (emitter_noise_factor.y - emitter_noise_factor.x) * rnd8.x;
	float rotation_speed = emitter_rotation_speed.x + (emitter_rotation_speed.y - emitter_rotation_speed.x) * rnd8.y;

	float2 rnd9 = hash2(unique_id + 17);
	float2 rnd10 = hash2(unique_id + 33);

	float initial_yaw = emitter_initial_yaw.x + (emitter_initial_yaw.y - emitter_initial_yaw.x) * rnd9.x;
	float initial_pitch = emitter_initial_pitch.x + (emitter_initial_pitch.y - emitter_initial_pitch.x) * rnd9.y;
	float initial_roll = emitter_initial_roll.x + (emitter_initial_roll.y - emitter_initial_roll.x) * rnd10.x;

	float4 init_quaternion = frowYawPitchRoll(initial_yaw, initial_pitch, initial_roll);

	TInstance p;
	p.center_pos = emitter_center;
	p.displacement_pos = float3(0, 0, 0);	

	p.acc = float3(0, -9.8, 0) * gravity_factor;
	p.dir = emitter_dir;
	p.unique_id = unique_id;
	p.time_normalized = 0.0;
	p.time_factor = 1.0 / duration;
	p.scale = 1.0;
	p.color = float4(1, 0, 0, 1);
	p.global_speed = speed;
	p.noise_frequency = noise_frequency;
	p.noise_factor = emitter_noise_factor;
	p.modifier_scale = modifier_size_factor;
	p.global_acc = float3(0, 0, 0);
	p.time_next_noise = 1 / noise_frequency;
	p.billboard = emitter_billboard;
	p.rotation_over_direction = emitter_rotation_over_direction;
	p.current_quat = init_quaternion;
	p.glow = emitter_glow;
	p.forward = emitter_forward;
	p.dummy = 0;
	p.dummy2 = 0;
	p.rotation_speed = rotation_speed;
	p.world_space = emitter_world_space;

	float rotation_yaw = 0;
	float rotation_pitch = 0;
	float rotation_roll = 0;

	for (int i = 0; i < 8; i++) {
		rotation_yaw = emitter_yaw_over_time[i].x + (emitter_yaw_over_time[i].y - emitter_yaw_over_time[i].x) * rnd10.y;
		rotation_pitch = emitter_pitch_over_time[i].x + (emitter_pitch_over_time[i].y - emitter_pitch_over_time[i].x) * rnd10.y;
		rotation_roll = emitter_roll_over_time[i].x + (emitter_roll_over_time[i].y - emitter_roll_over_time[i].x) * rnd10.y;

		p.rotation_quat[i] = frowYawPitchRoll(rotation_yaw, rotation_pitch, rotation_roll);
	}

	float3 init_pos;
	init_pos.x = abs(rnd1.x - 1) * (2 - rnd1.x) / 2 * rnd1.y + rnd1.x * (2 - rnd1.x) * rnd2.y + rnd1.x * abs(rnd1.x - 1) / 2 * rnd2.x;
	init_pos.y = abs(rnd1.x - 1) * (2 - rnd1.x) / 2 * rnd2.y + rnd1.x * (2 - rnd1.x) * rnd1.y + rnd1.x * abs(rnd1.x - 1) / 2 * rnd2.y;
	init_pos.z = abs(rnd1.x - 1) * (2 - rnd1.x) / 2 * rnd2.x + rnd1.x * (2 - rnd1.x) * rnd2.x + rnd1.x * abs(rnd1.x - 1) / 2 * rnd1.y;
	
	p.center_pos += init_pos * (1 - (rnd3.x * emitter_shell_thickness)) * emitter_cube_half_size;
	p.dir = normalize(p.dir * (1 - emitter_random_dir_factor) + normalize(float3(rnd5.x, rnd5.y, rnd6.x)) * emitter_random_dir_factor);

	if (p.rotation_over_direction) {
		float4 quat_between_vectors = normalize(from_to_rotation(p.forward, p.dir));
		p.current_quat = quat_between_vectors;
	}

	if (p.world_space) {
		p.center_pos += emitter_offset_center;
	}
	p.pos = p.center_pos;
	p.prev_pos = p.pos;

	return p;
}

// ----------------------------------------------------------
// The update fn to customize
void updateParticle( inout TInstance p ) {
  float2 rnd1 = hash2(p.unique_id) * 2 - 1; // [-1 , 1] 
  float2 rnd2 = hash2(p.unique_id + 3) * 2 -1; // [-1 , 1] 
  p.unique_id++;

  p.color = sampleColor( p.time_normalized );
  p.scale = sampleScale( p.time_normalized );
  p.scale *= p.modifier_scale;
  float4 deltaRotation = sampleQuat(p.rotation_quat, p.time_normalized);

  p.time_next_noise -= GlobalDeltaTime;
  if (p.time_next_noise < 0) {
	  p.time_next_noise = 1 / p.noise_frequency;
	  p.dir = normalize((1- p.noise_factor) * p.dir + p.noise_factor * normalize(float3(rnd1.x, rnd1.y, rnd2.x)));
  }

  p.global_acc += p.acc * GlobalDeltaTime;
  p.displacement_pos += (p.dir * (p.global_speed * sampleSpeed(p.time_normalized)) + p.global_acc) * GlobalDeltaTime;
  p.pos = p.center_pos + p.displacement_pos;
  if (!p.world_space) {
	  p.pos += emitter_offset_center;
  }


  if (p.rotation_over_direction) {
	  float3 MoveVec = normalize(p.pos - p.prev_pos);
	  float4 quat_between_vectors = normalize(from_to_rotation(p.forward, MoveVec));
	  p.current_quat = normalize(lerp(p.current_quat, quat_between_vectors, GlobalDeltaTime));
  }
  else {
	  float4 delta_quat = normalize(lerp(float4(0, 0, 0, 1), deltaRotation, GlobalDeltaTime * p.rotation_speed));
	  delta_quat = normalize(delta_quat);
	  p.current_quat = normalize(qmul(p.current_quat, delta_quat));
  }
  p.prev_pos = p.pos;
}
// --------------------------------------------------------------
// using a single thread to spawn new particles
[numthreads(1, 1, 1)]
void cs_particles_flow_spawn( 
  uint thread_id : SV_DispatchThreadID,
  RWStructuredBuffer<TInstance> instances : register(u0),
  RWStructuredBuffer<TSystem> system  : register(u1),
  RWByteAddressBuffer indirect_draw   : register(u2),
  RWByteAddressBuffer indirect_update : register(u3)
) {
  // We start from the num particles left in the prev frame
  uint   nparticles_active = indirect_draw.Load( OFFSET_NUM_PARTICLES_TO_DRAW );

  // Clear num instances for indirect draw call. At offset 4, set zero
  indirect_draw.Store( OFFSET_NUM_PARTICLES_TO_DRAW, 0 );

  // Get access to the max capacity of the buffer
  uint max_elements, bytes_per_instance;
  instances.GetDimensions( max_elements, bytes_per_instance );

  float2 rnd = hash2(system[0].next_unique_id + 27);
  uint  num_particles_per_spawn = trunc(emitter_num_particles_per_spawn.x + (emitter_num_particles_per_spawn.y - emitter_num_particles_per_spawn.x) * rnd.x);

  // Can we spawn particles?
  system[0].delay_time += GlobalDeltaTime;

  if( nparticles_active + num_particles_per_spawn < max_elements && system[0].delay_time > psystem_delay_time) {
	system[0].time_to_next_spawn -= GlobalDeltaTime;
	system[0].life_time += GlobalDeltaTime;

    if((system[0].time_to_next_spawn < 0 && emitter_looping) || (system[0].time_to_next_spawn < 0 && system[0].life_time < psystem_total_life_time)) {
	
		float  time_between_spawns = emitter_time_between_spawns.x + (emitter_time_between_spawns.y - emitter_time_between_spawns.x) * rnd.y;
		system[0].time_to_next_spawn += time_between_spawns;

		if (emitter_looping)
			system[0].life_time = 0;  //usefull for editor

      // Spawn N
      uint unique_id = system[0].next_unique_id;

      for( uint i=0; i< num_particles_per_spawn; ++i ) {
		if(emitter_type == 0) instances[nparticles_active] = spawnParticleCone(unique_id);
		else if (emitter_type == 1) instances[nparticles_active] = spawnParticleSphere(unique_id);
		else if (emitter_type == 2) instances[nparticles_active] = spawnParticleCube(unique_id);
        ++nparticles_active;
        ++unique_id;
      }

      system[0].next_unique_id = unique_id;
    }
  }
  
  // Update DispatchIndirect 1st argument.
  uint nthread_groups = ( nparticles_active + NUM_PARTICLES_PER_THREAD_GROUP - 1 ) / NUM_PARTICLES_PER_THREAD_GROUP;
  indirect_update.Store(0, nthread_groups);
  indirect_update.Store(4, 1);
  indirect_update.Store(8, 1);
  system[0].num_particles_to_update = nparticles_active;

}

// --------------------------------------------------------------
[numthreads(NUM_PARTICLES_PER_THREAD_GROUP, 1, 1)]
void cs_particles_flow_update( 
  uint thread_id : SV_DispatchThreadID,
  StructuredBuffer<TInstance> instances : register(t0),
  StructuredBuffer<TSystem>   system    : register(t1),
  RWStructuredBuffer<TInstance> instances_active : register(u0),
  RWByteAddressBuffer indirect_draw : register(u2)
) {

  if( thread_id >= system[0].num_particles_to_update )
    return;

  //Grabs the particle that corresponds to the thread
  TInstance p = instances[ thread_id ];

  // Has died?
  p.time_normalized += GlobalDeltaTime * p.time_factor;
  if( p.time_normalized >= 1 ) 
    return;

  // Call the specific method to update each particle
  updateParticle( p );

  // Update indirect draw call args
  uint index;
  indirect_draw.InterlockedAdd( OFFSET_NUM_PARTICLES_TO_DRAW, 1, index );

  // Save in the nexts buffer
  instances_active[index] = p;
}

//-------------------------`-------------------------------------------------------------
struct VS_OUTPUT {   // Vertex to pixel
	float4 Pos   : SV_POSITION;
	float2 Uv    : TEXCOORD0;
	float3 WorldPos : TEXCOORD1;
	float4 Color : COLOR;
	float2 Glow : TEXCOORD2;
};

struct VS_INPUT {   // Input from billboard mesh
	float4 Pos   : POSITION;
	float2 Uv    : TEXCOORD0;
	float4 Color : COLOR;         // Not used anymore
};

//--------------------------------------------------------------------------------------
VS_OUTPUT VS(
	in VS_INPUT input
	, in uint InstanceID : SV_InstanceID
	, StructuredBuffer<TInstance> instances_active : register(t0)
)
{
	TInstance instance = instances_active[InstanceID];

	VS_OUTPUT output;

	//// orient billboard to camera
	if (instance.billboard) {
		float3 localPos = input.Pos.x * CameraLeft + input.Pos.y * CameraUp;
		float3 p = instance.pos - localPos * instance.scale;
		output.WorldPos = output.Pos.xyz;
		output.Pos = mul(float4(p, 1.0), ViewProjection);
	}
	else
	{
		float4x4 rotation_matrix = quaternion_to_matrix(instance.current_quat);

		float3 p = instance.pos;
		float scale = instance.scale;
		float4x4 mtx = { scale, 0, 0, 0
					   , 0, scale, 0, 0
					   , 0, 0, scale, 0
					   , p.x, p.y, p.z, 1 };

		float4x4 rotated_mtx = mul(rotation_matrix, mtx);
		float4x4 newWorld = mul(rotated_mtx, World);
		output.Pos = mul(input.Pos, newWorld);
		output.WorldPos = output.Pos.xyz;
		output.Pos = mul(output.Pos, ViewProjection);
	}

	output.Uv = input.Uv;
	output.Color = instance.color;
	output.Glow = float2(instance.glow, 1);
	return output;
}

//--------------------------------------------------------------------------------------
void PS(
	VS_OUTPUT input
	, out float4 o_albedo : SV_Target0
	, out float4 o_normal : SV_Target1
	, out float1 o_depth  : SV_Target2
	, out float4 o_emissive : SV_Target3
	, out float4 o_outlineDepth : SV_Target6
)
{
	float4 color = txAlbedo.Sample(samLinear, input.Uv);
	o_albedo.rgb = color.rgb * input.Color.rgb * input.Glow.x;
	o_albedo.a = color.a * input.Color.a;
	
	float3 cam2obj = input.WorldPos.xyz - CameraPosition.xyz;
	o_depth = 1;
	
	o_emissive.xyz = o_albedo.xyz * glowAmount;
	o_emissive.a = 0;
	
	o_outlineDepth = pow(o_albedo.a, 0.35);
}


