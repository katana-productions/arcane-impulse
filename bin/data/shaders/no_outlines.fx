//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
#include "common.fx"
#include "gbuffer.inc"
#include "pbr.inc"

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------

VS_OUTPUT VS(
  VS_INPUT input
)
{
  return runObjVS(input, World);
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------

void PS(
	VS_OUTPUT input
	, out float4 o_albedo   : SV_Target0
	, out float1 o_depth    : SV_Target2
	, out float4 o_outlineMask : SV_Target6
)
{
	o_albedo = float4(0,0,0,0); 
	
	o_outlineMask = float4(1,1,1,1); 
	
	float3 cam2obj = input.WorldPos.xyz - CameraPosition.xyz;
	o_depth = dot(CameraFront, cam2obj) / CameraZFar;
}
//------------------------------------------------------------------------