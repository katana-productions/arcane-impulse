//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------
#include "common.fx"

//--------------------------------------------------------------------------------------
struct VS_OUTPUT
{
  float4 Pos      : SV_POSITION;
  float3 N        : NORMAL;
  float2 Uv       : TEXCOORD0;
  float3 WorldPos : TEXCOORD1;
  float4 T        : NORMAL1;
};
//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VS(
	float4 Pos : POSITION,
	float3 N : NORMAL,
	float2 Uv : TEXCOORD0
)
{
	VS_OUTPUT output = (VS_OUTPUT)0;
	output.Pos = mul(Pos, World);
	output.WorldPos = output.Pos.xyz;
	output.Pos = mul(output.Pos, ViewProjection);
	output.N = normalize(mul(N, World));
	output.Uv = Uv;
	return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
void PS(
	VS_OUTPUT input
	, out float4 o_albedo   : SV_Target0
	, out float4 o_normal   : SV_Target1
	, out float1 o_depth    : SV_Target2
	, out float4 o_emissive : SV_Target3
)
{
	o_albedo = (txAlbedo.Sample(samLinear, input.Uv) * ObjColor); 
	
	// Normal mapping
	float4 N_tangent_space = txNormal.Sample(samLinear, input.Uv);  // Between 0..1
	N_tangent_space.x = 1 - N_tangent_space.x;
	N_tangent_space.xyz = N_tangent_space.xyz * 2 - 1.;  // Between -1..1

	float3 T = input.T.xyz;
	float3 B = cross(T, input.N) * input.T.w;
	float3x3 TBN = float3x3(T, B, input.N);
	float3 N = mul(N_tangent_space.xyz, TBN);   // Normal from NormalMap

	// Save roughness in the alpha coord of the N render target
	float roughness = txSpecular.Sample(samLinear, input.Uv).r;
	o_normal = encodeNormal(N, roughness);
	
	float3 cam2obj = input.WorldPos.xyz - CameraPosition.xyz;
	o_depth = dot(CameraFront, cam2obj) / CameraZFar;
	
	o_emissive.xyz = txEmissive.Sample(samLinear, input.Uv).xyz * o_albedo.xyz * glowAmount;
	o_emissive.a = 1;
}

void PS_Scroll(
	VS_OUTPUT input
	, out float4 o_albedo   : SV_Target0
	, out float4 o_normal   : SV_Target1
	, out float1 o_depth    : SV_Target2
	, out float4 o_emissive : SV_Target3
)
{
    float2 uv = input.Uv;
	
	float speed = 0.3;
	uv.x += GlobalWorldTime * speed;
	uv.y += GlobalWorldTime * speed;
	
	o_albedo = (txAlbedo.Sample(samLinear, uv) * ObjColor);
	float _PosterizeGamma = 0.2; //posterizeGamma
	float _PosterizeNumColors = 5; //posterizeNumColors
	o_albedo.xyz = pow(o_albedo.xyz, _PosterizeGamma) * _PosterizeNumColors;
	o_albedo.xyz = floor(o_albedo.xyz) / _PosterizeNumColors;
	o_albedo.xyz = pow(o_albedo.xyz, 1.0 / _PosterizeGamma);
	
	// Normal mapping
	float4 N_tangent_space = txNormal.Sample(samLinear, uv);  // Between 0..1
	N_tangent_space.x = 1 - N_tangent_space.x;
	N_tangent_space.xyz = N_tangent_space.xyz * 2 - 1.;  // Between -1..1

	float3 T = input.T.xyz;
	float3 B = cross(T, input.N) * input.T.w;
	float3x3 TBN = float3x3(T, B, input.N);
	float3 N = mul(N_tangent_space.xyz, TBN);   // Normal from NormalMap

	// Save roughness in the alpha coord of the N render target
	float roughness = txSpecular.Sample(samLinear, uv).r;
	o_normal = encodeNormal(N, roughness);
	
	float3 cam2obj = input.WorldPos.xyz - CameraPosition.xyz;
	o_depth = dot(CameraFront, cam2obj) / CameraZFar;
	
	o_emissive.xyz = txEmissive.Sample(samLinear, uv).xyz * o_albedo.xyz * glowAmount;
	o_emissive.a = 1;
}
