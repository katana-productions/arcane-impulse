# ARKANUM

[![Arkanum](repo_img.jpg)](https://katana-productions.itch.io/arkanum "Redirect to itch.io")

## Info

***Arkanum is a 3D first person action-adventure game in which you control a thief with magical powers of attraction and repulsion. It's set in a fantasy setting including magicians, goblins, fireballs and a mysterious magic castle. It has a cartoonish graphic style, with very vivid and saturated colors.***

The game runs on a custom game engine coded from scratch in C++ using DirectX11 graphics API, and was created by 9 students for our final project of the Master in Creation of Video games, at University Pompeu Fabra, Barcelona, Spain (2018-2019).

If you are related to the Master in Video Game Creation (MCV) of UPF, you are free to use any code in future games or projects, just try to understand it first. I'm sure your teacher Juan wwill be happy to help with anything, so don't hesitate to ask him.

If you are not related to the MCV, you are also free to use any code (if you can!), just give credit to us (Katana Productions). Thank you for your understanding.

Formerly known as ARCANE IMPULSE.

**Watch the launch trailer:**

[![Arkanum Trailer](https://img.youtube.com/vi/XUMOyF-ULx0/0.jpg)](https://www.youtube.com/watch?v=XUMOyF-ULx0)

## Social Networks

- [Itch.io](https://katana-productions.itch.io/arkanum)
- [Twitter](https://twitter.com/ArkanumGame)
- [YouTube](https://www.youtube.com/channel/UCoCd68yOlXzldEaHCPw0aLQ)
- Contact: katanaproductionsupf (at) gmail.com


## Credits

Programmers:

- Josep Llistosella
- Gerard Belenguer
- Victor Portabella
- Jordi Arús
- Dani Garcia

Artists:

- Pedro Borrelli
- Sergio Jimenez
- Clàudia Raventós
- Antón Miranda

Music

- Inés Mirás
- Edu Collin 

Voice Acting:

- Sean Kurz as Narrator
- Ariadna Rodríguez as Thief
- Daniel Lanzas as Spirit