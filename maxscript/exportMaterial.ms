
-- ----------------------------------------------------------
-- ----------------------------------------------------------
-- ----------------------------------------------------------
struct TMaterialExporter (
	
	fs = TJsonFormatter(), 
	project_path,                 -- Asigned in the ctor 
	base_path,                 -- Asigned in the ctor 	
	
	fn exportMap map alias default_value = (
		
		if map == undefined then (
			local json_filename = base_path + "textures/" + default_value + ".dds"
			fs.writeKeyValue alias json_filename
			return false
		)
		
		-- "C:\mcv\code\bin\data\textures\bricks.DDS"
		
		-- "C:\users\pep\desktop\download\bricks.dds"
		local map_filename = map.filename
		
		-- bricks
		local base_name = getFilenameFile map_filename
		
		-- data\textures\bricks.dds"
		local json_filename = base_path + "textures/" + base_name + ".dds"
		
		-- 
		local ofull_path = project_path + json_filename
		
		-- Check if ofull_path exists
		if not doesFileExist ofull_path then (
			format "We should copy from % to %\n" map_filename ofull_path
			copyFile map_filename ofull_path
		)
		
		fs.writeKeyValue alias json_filename
	),
	
	fn isValidName aname = (
		-- Add no �, accents, etc.
		return findString aname " " == undefined 
	),
	
	-- Exports a single std material to a json format
	fn exportStdMaterial mat mat_name = (
		
		format "Exporting material % % %\n" mat_name mat (classof mat as string)
		
		if not (isValidName mat.name) then (
			throw ("Obj " + obj.name + " has a material with an invalid name " + mat.name )
		)	
		
		fs.begin (project_path + mat_name )
		fs.beginObj()
		  --Will use the defaults set in the parser. Needs to be a full data/techniques/textured.tech"
			--fs.writeKeyValue "technique" "textured.tech"
			--fs.writeComma()
			--fs.writeKeyValue "shadows" true
			--fs.writeComma()
			fs.writeKey "textures" 
			fs.beginObj()
				
			if classof mat == Standardmaterial then (
				exportMap mat.diffusemap "albedo" "null_albedo"
			    fs.writeComma()
				exportMap mat.bumpMap "normal" "null_normal"
				-- ... other maps..
			)
		
			fs.endObj()
		fs.endObj()
		fs.end()
	),
	
	-- Will return an array of all the materials names used by obj and exported by us
	fn exportMaterial mat base_name obj = (
		
		local exported_materials = #()
		
		if classof mat == StandardMaterial then (
			local mat_name = base_name + mat.name + ".material"
			append exported_materials mat_name
			exportStdMaterial mat mat_name
			
		) else if classof mat == MultiMaterial then (
			local multi_mat = mat
			
			local materials_of_mesh = getMaterialsUsedByMesh obj
			for mat_idx = 1 to materials_of_mesh.count do (
				if materials_of_mesh[ mat_idx ] == undefined then continue
				local mat_of_mesh = multi_mat[ mat_idx ]
				
				if mat_of_mesh == undefined then throw ("Mesh " + obj.name + " is using a multimaterial in slot " + (mat_idx as string)+ " but the multimat does not have this submat")
				if classof mat_of_mesh != StandardMaterial then throw ("Mesh " + obj.name + " is using a multimaterial in slot " + (mat_idx as string) + " but the multimat in this slot is not a stdMaterial")
				
				local mat_name = base_name + mat_of_mesh.name + ".material"
				append exported_materials mat_name
				exportStdMaterial mat_of_mesh mat_name
			)
		)
		
		return exported_materials
	)

	
)

--gc()
--me = TMaterialExporter project_path:"c:/code/engine/bin/" base_path:"data/"
--me.exportMaterial $.mat "data/materials/" $
