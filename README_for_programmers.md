# ARKANUM

Formerly known as ARCANE IMPULSE.

This was the git setup we used while in development, if you are related to the Master in Video Game Creation (MCV) of UPF, you are free to use any code in future games or projects, just try to understand it first, even if it has meatballs and bolognesse sauce over all the spaghetti! I'm sure your teacher Juan wwill be happy to help with anything, so don't hesitate to ask him.

If you are not related to the MCV, you are also free to use any code (if you can!), just give credit to us (Katana Productions). Thank you for your understanding.

## SETUP

1. To clone, use git bash from the command line and type:
 
        git clone https://gitlab.com/katana-productions/arcane-impulse.git 

2. Next, you need to add the .gitconfig to your git config, to do so type:

        cd arcane-impulse

        git config --local include.path ../.gitconfig

        git config core.hooksPath hooks/

        git config core.excludesFile exclude-info/exclude

3. Now remote juan-senpai is added to your remotes, from there you can pull any branch, to do so do:

        ssh-agent.exe bash -c 'ssh-add alumnes2019; git fetch juan-senpai'


## WORKFLOW

Everytime you work in this repository, you should follow these steps:

1.  Checkout juan-master, make a pull from remote juan-senpai and push it into our remote origin:

        git checkout juan-master

        ssh-agent.exe bash -c 'ssh-add alumnes2019; git pull juan-senpai master'

        git add -A

        git commit -am "Updated with dayX name_of_class"

        git push origin

2.  Before merging juan-master into our master, ***MAKE SURE juan-master COMPILES AND WORKS PERFECTLY***.

3.  Merge juan-master into our master (origin/master), resolve any conflicts if there are any:

        git checkout master

        git merge juan-master

        git commit -am "Merged master with updated contents of juan-master".

4.  From there, merge our master branch into other branches, resolve any conflicts if there are any:

        git checkout your_branch

        git merge master

5.  Commit the changes in other branches and push them as you wish.
6.  If you wish to push commits into our origin master ***DO SO WITH A PULL REQUEST***.
