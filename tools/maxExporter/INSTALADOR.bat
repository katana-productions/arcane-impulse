@echo off
echo "Copying scripts"
copy /Y exports\scripts\*.*  C:\exports\scripts >nul
echo "Copying max files"
del /F/Q/S "C:\exports\max_files" > nul 
copy /Y exports\max_files\*.*  C:\exports\max_files >nul
call update_particles.bat
echo "Done!"
pause