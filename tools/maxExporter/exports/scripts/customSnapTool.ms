fn roundPositions unit = (
	for obj in $ do(
		local pos = obj.transform.position
		local half = unit/2
		--X
		local mod_x = mod pos.x unit
		--format "% %\n" pos.x mod_x
		if abs mod_x < half then (
			move obj [-mod_x,0,0]
		)else(
			if mod_x > 0 then move obj [(unit - mod_x),0,0] else move obj [-(unit + mod_x),0,0]
		)
		--Y
		local mod_y = mod pos.y unit
		if abs mod_y < half then (
			move obj [0,-mod_y,0]
		)else(
			if mod_y > 0 then move obj [0,(unit - mod_y),0] else move obj [0,-(unit + mod_y),0]
		)
		--Z
		local mod_z = mod pos.z unit
		if abs mod_z < half then (
			move obj [0,0,-mod_z]
		)else(
			if mod_z > 0 then move obj [0,0,(unit - mod_z)] else move obj [0,0,-(unit + mod_z)]
		)
	)
	MessageBox "Position Snap Finished" beep:false
)

fn roundRotation unit = (
	for obj in $ do(
		local rot = (obj.transform.rotation as eulerangles)
		local half = unit/2
		local x = 0
		local y = 0
		local z = 0
		--X
		local mod_x = mod rot.x unit
		format "% %\n" rot.x mod_x
		if abs mod_x < half then (
			x= -mod_x
		)else(
			if mod_x > 0 then x = (unit - mod_x) else x = -(unit + mod_x)
		)
		--Y
		local mod_y = mod rot.y unit
		if abs mod_y < half then (
			y = -mod_y
		)else(
			if mod_y > 0 then y = (unit - mod_y) else y = -(unit + mod_y)
		)
		--Z
		local mod_z = mod rot.z unit
		if abs mod_z < half then (
			z = -mod_z
		)else(
			if mod_z > 0 then z = (unit - mod_z) else z = -(unit + mod_z)
		)
		
		rot_obj = eulerangles x y z
		rotate obj rot_obj
	)
	MessageBox "Rotation Snap Finished" beep:false
)

rollout snap_tool "Snap Tool" 
(
	group "Snap Position"(
		spinner unit_pos "Unit: " range:[0,100,2] fieldWidth:65 align:#center scale:0.5
		button snap_pos "Snap Objects" width:100 height:25 align:#center
	)
	group "Snap Rotation"(
		spinner unit_rot "Unit: " range:[0,100,2] fieldWidth:65 align:#center scale:0.5
		button snap_rot "Snap Objects" width:100 height:25 align:#center
	)
	
	on snap_pos pressed do
	(
		if $ == undefined then(
			MessageBox "Warning: You MUST select an object!"
			return undefined
		)
		roundPositions unit_pos.value
	)
	
	on snap_rot pressed do
	(
		if $ == undefined then(
			MessageBox "Warning: You MUST select an object!"
			return undefined
		)
		roundRotation unit_rot.value
	)
)