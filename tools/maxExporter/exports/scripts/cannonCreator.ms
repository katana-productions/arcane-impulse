rollout cannon_creator "Cannon Creator" 
(
	group "Creator" (
		edittext name "Name:" labelontop:true
		label pos_label "Position:" align:#left
		spinner x "X:" align:#center width:60 height:15 range:[-1000000,1000000,0]
		spinner y "Y:" align:#center width:60 height:15 range:[-1000000,1000000,0]
		spinner z "Z:" align:#center width:60 height:15 range:[-1000000,1000000,0]
		
		label offset_label "Offset:" align:#left
		spinner x_offset "X:" align:#center width:60 height:15 range:[-1000000,1000000,0]
		spinner y_offset "Y:" align:#center width:60 height:15 range:[-1000000,1000000,0]
		spinner z_offset "Z:" align:#center width:60 height:15 range:[-1000000,1000000,0]
		label height_label "Height:" align:#left
		spinner height_spinner type:#float align:#left range:[0,100,10]
		label rof_label "Rate of Fire:" align:#left
		spinner rof_spinner type:#float align:#left range:[0,100,3.0]
		label delay_label "Start Delay:" align:#left
		spinner delay_spinner type:#float align:#left range:[0,100,0.0]
		combobox targets "Targets" labelOnTop:true height:5
		
		button create "Create Cannon" align:#center width:100 height:20
	)
	
	on targets entered txt do (
		if txt != "" then(
			obj = execute ("$" + txt)
			if obj == undefined or obj.name != txt then (
				MessageBox "Warning: Object doesn't exist in the scene. Check the name!"
				return undefined
			)
			items = targets.items
			append items txt
			targets.items = items
		)
	)

	on create pressed do
	(
		if name.text == "" then (
			MessageBox "Warning: Dummy MUST have a name!"
			return undefined
		)else(
			obj = execute ("$" + name.text)
			if obj != undefined then (
				MessageBox "Warning: Duplicate name!"
			return undefined
			)
			Dummy pos:[x.value,y.value,z.value] isSelected:on boxsize:[2,2,2]
			$.name = name.text
			--TODO insert mesh when it is done
			/*objXRefMgr.mergeTransforms = true
			local x_ref_obj = xrefs.addNewXRefObject "c:/exports/max_files/door_ark.max" "door_ark" modifiers:#drop manipulators:#merge reparentAction:#alwaysReparent dupMtlNameAction:#useXRefed 
			x_ref_obj.pos = $.pos
			append $.children x_ref_obj
			x_ref_obj.transform.controller = prs()
			freeze x_ref_obj*/
		)	
		custAttributes.add $ classCannon
		
		$.x_offset = x_offset.value
		$.y_offset = y_offset.value
		$.z_offset = z_offset.value
		$.height_value = height_spinner.value
		$.rof_value = rof_spinner.value
		$.delay_value = delay_spinner.value
		
		$.target_names = targets.items
	)
)