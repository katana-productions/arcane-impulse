rollout trigger_creator "Trigger Creator" 
(
	group "Creator" (
		edittext name "Name:" labelontop:true
		label pos "Position:" align:#left
		spinner x "X:" align:#center width:60 height:15 range:[-1000000,1000000,0]
		spinner y "Y:" align:#center width:60 height:15 range:[-1000000,1000000,0]
		spinner z "Z:" align:#center width:60 height:15 range:[-1000000,1000000,0]
		label prefab "Type:" align:#left
		radiobuttons trigger_type align:#left labels:#("Normal","Chasm Trigger","Tutorial Trigger","Camera Transition","Call Trigger") columns:1
		button create "Create Trigger" align:#center width:100 height:20
	)
	group "Options" (
		spinner l "Length:" align:#center width:80 height:15 range:[-1000000,1000000,10]
		spinner w "Width:" align:#center width:80 height:15 range:[-1000000,1000000,10]
		spinner h "Height:" align:#center width:80 height:15 range:[-1000000,1000000,10]
		button selection_size "Get Selection Size"
		button reset_size "Reset Sizes"
	)

	on create pressed do
	(
		if name.text == "" then (
			MessageBox "Warning: Trigger MUST have a name!"
			return undefined
		)else(
			obj = execute ("$" + name.text)
			if obj != undefined then (
				MessageBox "Warning: Duplicate name!"
			return undefined
			)
			Box pos:[x.value,y.value,z.value] isSelected:on length:l.value width:w.value height:h.value
			$.wireColor = color 255 0 0
			$.xray = true
			tLayer = layerManager.getLayerFromName "triggers"
			if tLayer == undefined then (
				tLayer = layerManager.newLayerFromName  "triggers"
			)
			tLayer.addnode $
		)			
		if name.text != "" then (
			$.name = name.text
		)
		x.value = 0
		y.value = 0
		z.value = 0
		if trigger_type.state == 2 then(
			--Chasm Trigger
			custAttributes.add $ classChasmTrigger
		)
		if trigger_type.state == 3 then(
			--Tutorial Trigger
			custAttributes.add $ classTutorialTrigger
		)
		if trigger_type.state == 4 then(
			--Cinematic Transition
			MessageBox "Not implemented yet"
		)
		if trigger_type.state == 5 then(
			--Call Trigger
			custAttributes.add $ classCallTrigger
		)
		
	)
		
	function changeTriggerSize = (
		if $ == undefined then return undefined
		if classof $ != Box and $.layer.name != "triggers" then (
			MessageBox "Selected object is not a Trigger!"
			return undefined
		)
		$.length = l.value
		$.width = w.value
		$.height = h.value
	)
	
	on selection_size pressed do (
		if $ == undefined then (
			MessageBox "You MUST select an object!"
			return undefined
		)
		if classof $ != Box and $.layer.name != "triggers" then (
			MessageBox "Selected object is not a Trigger!"
			return undefined
		)
		--local size = $.boxsize
		l.value = $.length
		w.value = $.width
		h.value = $.height
	)
	
	on reset_size pressed do (
		l.value = 10
		w.value = 10
		h.value = 10
	)

	on l changed state do
	(
		changeTriggerSize()
	)

	on w changed state do
	(
		changeTriggerSize()
	)

	on h changed state do
	(
		changeTriggerSize()
	)
)