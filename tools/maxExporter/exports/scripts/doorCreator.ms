rollout door_creator "Door Creator" 
(
	group "Creator" (
		edittext name "Name:" labelontop:true
		label pos "Position:" align:#left
		spinner x "X:" align:#center width:60 height:15 range:[-1000000,1000000,0]
		spinner y "Y:" align:#center width:60 height:15 range:[-1000000,1000000,0]
		spinner z "Z:" align:#center width:60 height:15 range:[-1000000,1000000,0]
		label type "Type:" align:#left
		radiobuttons door_type align:#left labels:#("Player Go-To","Enemies", "Puzzle", "Timed") columns:2
		label laber_prefab "Door Prefab:" align:#left
		radiobuttons door_prefab align:#left labels:#("Vertical Bars","Vertical Gate", "Horizontal Gate") columns:1

		button create "Create Door" align:#center width:100 height:20
	)

	on create pressed do
	(
		local prefab = ""
		if name.text == "" then (
			MessageBox "Warning: Dummy MUST have a name!"
			return undefined
		)else(
			obj = execute ("$" + name.text)
			if obj != undefined then (
				MessageBox "Warning: Duplicate name!"
			return undefined
			)
			Dummy pos:[x.value,y.value,z.value] isSelected:on boxsize:[4,1,8]
			$.name = name.text
			objXRefMgr.mergeTransforms = true
			if door_prefab.state == 1 then(
				local x_ref_obj = xrefs.addNewXRefObject "c:/exports/max_files/SwrArchBars.max" "SwrArchBars" modifiers:#drop dupMtlNameAction:#useScene 
				prefab = "data/prefabs/doors/verticalDoor.json"
			)else	if door_prefab.state == 2 then(
				local x_ref_obj = xrefs.addNewXRefObject "c:/exports/max_files/gate01.max" "gate_low" modifiers:#drop dupMtlNameAction:#useScene 
				prefab = "data/prefabs/props/gate01.json"
			)else	if door_prefab.state == 3 then(
				local x_ref_obj = xrefs.addNewXRefObject "c:/exports/max_files/Cst_Bridge_Door.max" "Cst_Door_Bridge" modifiers:#drop dupMtlNameAction:#useScene 
				prefab = "data/prefabs/props/Cst_Bridge_Door.json"
			)
			x_ref_obj.pos = $.pos
			append $.children x_ref_obj
			x_ref_obj.transform.controller = prs()
			freeze x_ref_obj
			$.boxsize = $.children[1].max - $.children[1].min
		)	
		if door_type.state == 1 then (
			--player_goto
			custAttributes.add $ classDoorGoto
			$.prefab = prefab
		)
		if door_type.state == 2 then (
			--enemies
			custAttributes.add $ classDoorEnemies
			$.prefab = prefab
		)
		
		if door_type.state == 3 then (
			--Puzzle
			custAttributes.add $ classDoorPuzzle
			$.prefab = prefab
		)
		
		if door_type.state == 4 then (
			--Timed
			custAttributes.add $ classDoorTimed 
			$.prefab = prefab
		)
	)
)