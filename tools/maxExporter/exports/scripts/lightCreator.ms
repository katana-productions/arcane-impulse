rollout light_creator "Light Creator" 
(
	edittext name "Name:" labelontop:true
	label pos "Position:" align:#left
	spinner x "X:" align:#center width:60 height:15 range:[-1000000,1000000,0]
	spinner y "Y:" align:#center width:60 height:15 range:[-1000000,1000000,0]
	spinner z "Z:" align:#center width:60 height:15 range:[-1000000,1000000,0]
	
	group "Light Parameters" (
		checkbox shadows "Cast Shadows?"
		label int_label "Intensity:" align:#left
		spinner intensity type:#float align:#center width:60 height:15 range:[0,1000000,20]
		label rad_label "Radius:" align:#left
		spinner radius type:#float align:#center width:60 height:15 range:[0,1000000,30]
		label color_label "Color:" align:#left
		colorpicker color_pick align:#center width:60 color:white
		label offset "Offset:" align:#left
		spinner offsetX "X:" align:#center width:60 height:15 range:[-1000000,1000000,0]
		spinner offsetY"Y:" align:#center width:60 height:15 range:[-1000000,1000000,0]
		spinner offsetZ "Z:" align:#center width:60 height:15 range:[-1000000,1000000,0]

	)
	
	group "Flicker Options" (
		checkbox flicker_check "Flicker?" align:#center 
		spinner smoothness_spinner "Smoothness:" type:#float align:#center range:[0,1000000,0] visible:false
		spinner min_int "Min. Intensity:" type:#float align:#center range:[0,1000000,0] visible:false
		spinner max_int "Max. Intensity:" type:#float align:#center range:[0,1000000,0] visible:false
		spinner min_rad "Min. Radius:" type:#float align:#center range:[0,1000000,0] visible:false
		spinner max_rad "Max. Radius:" type:#float align:#center range:[0,1000000,0] visible:false
	)
	
	button create "Create Light" align:#center width:100 height:20

	on create pressed do
	(
		if name.text == "" then (
			MessageBox "Warning: Light MUST have a name!"
			return undefined
		)else(
			obj = execute ("$" + name.text)
			if obj != undefined then (
				MessageBox "Warning: Duplicate name!"
			return undefined
			)
			Dummy pos:[x.value,y.value,z.value] isSelected:on boxsize:[2,2,2]
			$.name = name.text
			$.wirecolor = (color 255 0 0)
		)			
		
		custAttributes.add $ classLight	
		$.light_intensity = intensity.value
		$.light_radius = radius.value
		$.light_color = color_pick.color
		$.cast_shadows = shadows.checked
		$.offset_x = offsetX.value
		$.offset_y = offsetY.value
		$.offset_z = offsetZ.value
		$.flicker = flicker_check.checked
		$.smoothness = smoothness_spinner.value
		$.min_intensity = min_int.value
		$.max_intensity = max_int.value
		$.min_radius = min_rad.value
		$.max_radius = max_rad.value
		
		x.value = 0
		y.value = 0
		z.value = 0
		
		MessageBox "Light Created!" beep:false
	)
	
	on flicker_check changed v do (
		if v then(
			smoothness_spinner.visible = true
			min_int.visible = true
			max_int.visible = true
			min_rad.visible = true
			max_rad.visible = true
		)else(
			smoothness_spinner.visible = false
			min_int.visible = false
			max_int.visible = false
			min_rad.visible = false
			max_rad.visible = false
		)
	)
)