rollout spawn_creator "Player Spawn Point" 
(
	label pos "Position:" align:#left
	spinner x "X:" align:#center width:60 height:15 range:[-1000000,1000000,0]
	spinner y "Y:" align:#center width:60 height:15 range:[-1000000,1000000,0]
	spinner z "Z:" align:#center width:60 height:15 range:[-1000000,1000000,0]
	button create "Create Spawn Point" align:#center width:150 height:20
	
	on create pressed do
	(
		obj = execute ("$PlayerSpawnPoint")
		if obj != undefined then (
			MessageBox "Warning: Spawn Point already exists!"
			return undefined
		)
		Dummy pos:[x.value,y.value,z.value] isSelected:on boxsize:[2,2,2]
		$.name = "PlayerSpawnPoint"	
		custAttributes.add $ classSpawnData
	)
	
	on spawn_creator open do
	(
		obj = execute ("$PlayerSpawnPoint")
		if obj != undefined then (
			MessageBox "Warning: Spawn Point already exists!"
		)
	)
)