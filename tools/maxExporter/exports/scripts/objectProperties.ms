rollout object_properties "Object Properties" 
(
	group "Properties" (
		checkbox interactable "Interactable" checked:true
		checkbox dynamic "Dynamic"
		checkbox kinematic "Kinematic"
		checkbox not_instantiable "Not Instantiable (XRefs only)"
		spinner density "Density:" align:#left width:100 height:15 range:[0,1000,1]
		spinner mass "Mass:" align:#left width:100 height:15 range:[0,1000,1]
		spinner linear_drag "Linear Drag:" align:#left width:100 height:15 range:[0,1000,0.5]
		spinner angular_drag "Angular Drag:" align:#left width:100 height:15 range:[0,1000,0.25]
		dropDownList material_type "Material Type:" labelOnTop:true width:151 height:40 items:#("0all", "sticky", "superSticky", "normal", "object", "ice", "rubber", "wood", "rock") align:#left
		dropDownList group_dropdown "Group" labelOnTop:true width:151 height:40 items:#("world", "enemy", "objects", "trigger") align:#left
		dropDownList mask_dropdown "Mask:" labelOnTop:true width:151 height:40 items:#("world", "enemy", "objects", "trigger") align:#left
		spinner glow_amount "Glow Amount:" align:#left width:100 height:15 range:[0,2500,0]
		spinner rim_factor "Rim Factor:" align:#left width:100 height:15 range:[0,1,0]
		spinner specular_factor "Specular Factor:" align:#left width:100 height:15 range:[0,1,0]
		checkbox mesh_color "Use Mesh Color?"
	)
	
	button save_btn "Save" align:#center width:100 height:20

	function get_properties = (
		if $ == undefined then (
			MessageBox "You MUST select an object!"
			return undefined
		)
		if $selection.count > 1 then (
			MessageBox "Multiple objects selected! Please select only one"
			return undefined
		)
		
		local tag = getUserProp $ "tag"
		if tag != undefined then (
			if tag == "Interactable" then interactable.checked = true
			else interactable.checked = false
		)else(
			--Default value
			interactable.checked = true
		)

		if (getUserProp $ "dynamic") == true then (
			dynamic.checked = true
		)else(
			--Default value
			dynamic.checked = false
		)
		
		if (getUserProp $ "kinematic") == true then (
			kinematic.checked = true
		)else(
			--Default value
			kinematic.checked = false
		)
		
		if (getUserProp $ "not_instantiable") == true then (
			not_instantiable.checked = true
		)else(
			--Default value
			not_instantiable.checked = false
		)
		
		local dens = getUserProp $ "density"
		if dens != undefined then (
			density.value = dens
		)else(
			--Default value
			density.value = 1
		)
		
		local ms = getUserProp $ "mass"
		if ms != undefined then (
			mass.value = ms
		)else(
			--Default value
			mass.value = 1
		)
		
		local ld = getUserProp $ "linearDrag"
		if ld != undefined then (
			linear_drag.value = ld
		)else(
			--Default value
			linear_drag.value = 0.5
		)
		
		local ad = getUserProp $ "angularDrag"
		if ad != undefined then (
			angular_drag.value = ad
		)else(
			--Default value
			angular_drag.value = 0.25
		)
		
		local mt = getUserProp $ "materialType"
		if mt != undefined then (
			material_type.selected = mt
		)else(
			--Default value
			material_type.selected = "world"
		)
		
		local gp = getUserProp $ "group"
		if gp != undefined then (
			group_dropdown.selected = gp
		)else(
			--Default value
			group_dropdown.selected = "world"
		)
		
		local mk = getUserProp $ "mask"
		if mk != undefined then (
			mask_dropdown.selected = mk
		)else(
			--Default value
			mask_dropdown.selected = "world"
		)
		
		local ga = getUserProp $ "glowAmount"
		if ga != undefined then (
			glow_amount.value = ga
		)else(
			--Default value
			glow_amount.value = 0
		)
		
		local rf = getUserProp $ "rimFactor"
		if rf != undefined then (
			rim_factor.value = rf
		)else(
			--Default value
			rim_factor.value = 0
		)
		
		local sf = getUserProp $ "specularFactor"
		if sf != undefined then (
			specular_factor.value = sf
		)else(
			--Default value
			specular_factor.value = 0
		)
		
		if (getUserProp $ "meshColor") == true then (
			mesh_color.checked = true
		)else(
			--Default value
			mesh_color.checked = false
		)
	)
	
	on save_btn pressed do
	(
		if interactable.checked then setUserProp $ "tag" "Interactable"
		else setUserProp $ "tag" "No Interactable"
		if dynamic.checked then setUserProp $ "dynamic" true
		else setUserProp $ "dynamic" false
		if kinematic.checked then setUserProp $ "kinematic" true
		else setUserProp $ "kinematic" false
		if not_instantiable.checked then setUserProp $ "not_instantiable" true
		else setUserProp $ "not_instantiable" false
		setUserProp $ "density" density.value
		setUserProp $ "mass" mass.value
		setUserProp $ "linearDrag" linear_drag.value
		setUserProp $ "angularDrag" angular_drag.value
		setUserProp $ "materialType" material_type.selected
		setUserProp $ "group" group_dropdown.selected
		setUserProp $ "mask" mask_dropdown.selected
		setUserProp $ "glowAmount" glow_amount.value
		setUserProp $ "rimFactor" rim_factor.value
		setUserProp $ "specularFactor" specular_factor.value
		MessageBox "Properties Saved!" beep:false
		if mesh_color.checked then setUserProp $ "meshColor" true
		else setUserProp $ "meshColor" false
	)
	
	/*on selection_props pressed do (
		get_properties()
	)*/
	
	on object_properties open do(
		get_properties()
		callbacks.addscript #SelectionSetChanged "object_properties.get_properties()"
	)
	on object_properties close do(
		callbacks.removescripts #SelectionSetChanged
	)
	
)