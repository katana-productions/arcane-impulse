rollout particle_creator "Particle Creator" 
(
	group "Creator" (
		edittext name "Name:" labelontop:true
		label pos "Position:" align:#left
		spinner x "X:" align:#center width:60 height:15 range:[-1000000,1000000,0]
		spinner y "Y:" align:#center width:60 height:15 range:[-1000000,1000000,0]
		spinner z "Z:" align:#center width:60 height:15 range:[-1000000,1000000,0]
		dropDownList particle_list "Particle:" width:151 height:40 align:#left
		button create "Create Particle" align:#center width:100 height:20
	)
	group "Options" (
		spinner l "Length:" align:#center width:80 height:15 range:[-1000000,1000000,10]
		spinner w "Width:" align:#center width:80 height:15 range:[-1000000,1000000,10]
		spinner h "Height:" align:#center width:80 height:15 range:[-1000000,1000000,10]
		button selection_size "Get Selection Size"
		button reset_size "Reset Sizes"
	)

	on create pressed do
	(
		if name.text == "" then (
			MessageBox "Warning: Dummy MUST have a name!"
			return undefined
		)
		obj = execute ("$" + name.text)
		if obj != undefined then (
			MessageBox "Warning: Duplicate name!"
			return undefined
		)
		Dummy pos:[x.value,y.value,z.value] isSelected:on boxsize:[l.value, w.value, h.value]
		$.name = name.text		
		custAttributes.add $ classParticleData
		$.particle = particle_list.selected
		$.particle_number = particle_list.selection
		x.value = 0
		y.value = 0
		z.value = 0
			
	)
		
	function changeDummySize = (
		if $ == undefined then return undefined
		if classof $ != Dummy then (
			MessageBox "Selected object is not a Dummy, dummy!"
			return undefined
		)
		$.boxsize=[l.value, w.value, h.value]
	)
	
	on selection_size pressed do (
		if $ == undefined then (
			MessageBox "You MUST select an object!"
			return undefined
		)
		if classof $ != Dummy then (
			MessageBox "Selected object is not a Dummy, dummy!"
			return undefined
		)
		local size = $.boxsize
		l.value = size.x
		w.value = size.y
		h.value = size.z
	)
	
	on reset_size pressed do (
		l.value = 10
		w.value = 10
		h.value = 10
	)

	on l changed state do
	(
		changeDummySize()
	)

	on w changed state do
	(
		changeDummySize()
	)

	on h changed state do
	(
		changeDummySize()
	)
	
	on particle_creator open do (
		part_array = #()
		part_str = ""
		fs = openFile "C:/exports/particles.txt"
		while not eof fs do(
		   part_str = readline fs
		   append part_array part_str
		)
		close fs
		particle_list.items = part_array
	)
)