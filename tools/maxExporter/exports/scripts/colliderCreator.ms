fn attachObjs objArr = (
	j = 1
	while objArr.count > 1 do
	(				
		if classof objArr[j] != Editable_Poly then converttopoly objArr[j]
			
		if classof objArr[j+1] != Dummy and classof objArr[j] != Dummy then(
			polyop.attach objArr[j] objArr[j+1]
		)else(
			obj = execute ("$" + objArr[j+1].name)
			if obj.children.count > 0 then (
				delete obj.children[1]
			)
			delete obj
		)
		deleteItem objArr (j+1)
			
		j += 1
		
		if (j + 1) > objArr.count then j = 1
	)
)

fn groupTrimesh objs = (
	maxOps.cloneNodes objs newNodes:&result
	result[1].name = objs[1].name + "_collider"
	append objs[1].children result[1]
	result[1].xray = true
	
	if result.count > 1 then attachObjs result
	else converttopoly result[1]
)

rollout collider_creator "Add Collider" 
(
	group "Creator" (
		radiobuttons collider_type align:#center labels:#("Box", "Sphere", "Capsule","Trimesh","Convex","Full Trimesh") columns:2
		checkbox offset "Offset?"
		button create "Add Collider" align:#center width:100 height:20
	)
	
	on collider_type changed p do (
		if p == 1 then(
			offset.visible = true
		)else(
			offset.visible = false
		)
	)

	on create pressed do
	(
		if $ == undefined and collider_type.state != 6 then (
			MessageBox "Warning: You MUST select an object!"
			return undefined
		)
		if collider_type.state != 6 then(
			local objMax = $.max
			local objMin = $.min
		)else(
			groupTrimesh (geometry as array)
			MessageBox "Collider created! You may need to adjust it manually" beep:false
		)
		
		if collider_type.state == 1 then (
			--Box Collider
			if classof $ == box then (
				obj = copy $
			)else(
				bb = objMax - objMin
				obj = box width:bb[1] length:bb[2] height:bb[3]
				CenterPivot obj
				obj.transform = $.transform
				obj.pos = $.center
				obj.pivot = $.pivot
			)
			obj.name = $.name + "_collider"
			append $.children obj
			obj.xray = true
			if offset == 1 then(
				setUserProp $ "offset" true
			)else(
				setUserProp $ "offset" false
			)
			MessageBox "Collider created! You may need to adjust it manually" beep:false
		)
		
		if collider_type.state == 2 then (
			--Sphere Collider
			if classof $ == sphere then (
				obj = copy $
			)else(
				bb = objMax - objMin
				rad = amax #(bb[1],bb[2],bb[3])
				rad = rad/1.4
				obj = sphere radius:rad
				CenterPivot obj
				obj.transform = $.transform
				obj.pos = $.center
				obj.pivot = $.pivot
			)
			obj.name = $.name + "_collider"
			append $.children obj
			obj.xray = true
			MessageBox "Collider created! You may need to adjust it manually" beep:false
		)
		
		if collider_type.state == 3 then (
			--Capsule Collider
			MessageBox "Still not implemented!"
		)
		
		if collider_type.state == 4 then (			
			--Trimesh Collider
			with undo (false) (
				groupTrimesh (Selection as array)
				MessageBox "Collider created! You may need to adjust it manually" beep:false
			)
		)
		
		if collider_type.state == 5 then (
			local cvx_hull = nvpx.CreateConvexHull $.mesh 100 0
			local cloned_mesh = box name:"foo"
			local conv_obj = convertToMesh cloned_mesh
			cloned_mesh.mesh = cvx_hull
			CenterPivot cloned_mesh
			cloned_mesh.rotation =$.rotation
			cloned_mesh.pos = $.pos
			cloned_mesh.parent = $
			cloned_mesh.name = $.name + "_collider"
			cloned_mesh.xray = true
			setUserProp $ "convex" true
			--cloned_mesh.xray = true
			MessageBox "Collider created! You may need to adjust it manually" beep:false
		)
	)
)