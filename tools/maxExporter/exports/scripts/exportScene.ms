clearListener()
gc()

fn isCollider obj = (
	return obj != undefined and (findString obj.name "collider" != undefined ) and obj.parent != null
)

fn isTrigger obj = (
	return obj != undefined and obj.layer.name == "triggers"
)

fn exportCurve obj str_path = (

	local maxLocalToWorld = obj.transform
	local maxToMCV = rotateXMatrix  -90
	local maxWorldToLocal = inverse obj.transform
	local maxWorldToLocalMCV = maxWorldToLocal * maxToMCV
	total_points = #()
	
	fs = TJsonFormatter()
	fs.begin str_path
	/*fs.beginObj()
	fs.writeKeyValue "type" "catmull-rom"
	fs.writeComma()*/
	
	-- Retrieve the knots
	for s = 1 to (numsplines obj) do
	(
		for k = 1 to (numknots obj s) do 
		(
			local p_point = getKnotPoint obj s k
			p_point = p_point * maxToMCV
			append total_points (p_point[1] as string + " " + p_point[2] as string + " " + p_point[3] as string)
			if k == 1 or k == (numknots obj s) then append total_points (p_point[1] as string + " " + p_point[2] as string + " " + p_point[3] as string)
		)
	)
	
	-- Export the knots
	--fs.writeKey "knots"
	fs.arrayOfStrings total_points
	
	--fs.endObj()
	fs.end()	
)
fn isConvex obj =
(
	local convex = true
	
	-- set the selection level to vertex
	subObjectLevel = 1
	
	-- selects the concave vertex
	PolyToolsSelect.ConvexConcave 0.0 1
	
	if (obj.selectedVerts.count > 0) then
		convex = false
	
	-- back to object selection level
	subobjectLevel = 0
	
	return convex
)
----------------------------------------------------------
struct TSceneExporter (

	fs = TJsonFormatter(), 
	project_path = "c:/exports/",
	base_path = "data/",
	current_scene_name,
	scenes_path = base_path + "scenes/",
	mesh_path = base_path + "meshes/",
	mats_path = base_path + "materials/",
	curve_path = base_path + "curves/",
	
	fn exportCompName obj = (
		fs.writeKey "name"
		fs.beginObj()
		fs.writeKeyValue "entityName" obj.name
		fs.writeComma()
		local tag = getUserProp obj "tag"
		if tag != undefined then (
			fs.writeKeyValue "tag" tag
		)else(
			local prefab = custAttributes.get obj classPrefabData
			local puzzle_prefab= custAttributes.get obj classPuzzlePrefab
			if (classof obj != Dummy or prefab != undefined or puzzle_prefab != undefined) and (isTrigger obj) == false then (
				fs.writeKeyValue "tag" "Interactable"
			)else(
				if classof obj == Dummy and obj.children[1] != undefined then(
					fs.writeKeyValue "tag" "Interactable"
				)else(
					fs.writeKeyValue "tag" "No Interactable"
				)
			)
			
		)
		fs.endObj()
		
	),
	
	fn isValidName aname = (
		return findString aname " " == undefined 
	),

	----------------------------------------------------------
	fn exportTransform obj = (
		fs.writeComma()
		fs.writeKey "transform" 
		
		local max2mcv = rotateXMatrix -90
		local mcv2max = rotateXMatrix 90
		local mcv_position = obj.transform.position * max2mcv
		
		-- From mcv, we will go to max, apply the max transform and go back to mcv coord system
		local mcv_transform = mcv2max * obj.transform * max2mcv

		-- Take just the rotation as quaterion
		local mcv_quat = mcv_transform.rotationPart as quat
		
		fs.beginObj()
			fs.writeKeyValue "pos" mcv_position
			fs.writeComma()
			fs.writeKeyValue "rotation" mcv_quat
		fs.endObj()
	),
		
	----------------------------------------------------------
	fn exportEditableMesh obj = (
		
		fs.writeComma()
		fs.writeKey "render" 
		fs.beginObj()
		local mesh_name = mesh_path + current_scene_name + "/" + current_scene_name + "_" + obj.name + ".mesh"
		/*if classof obj == XRefObject then(
			fs.writeKeyValue "instanced" true
			fs.writeComma()
		)*/
		--local texture_name = mats_path + obj.name
		fs.writeKeyValue "mesh" mesh_name
		fs.writeComma()
		if (getUserProp obj "meshColor") == true then (
			fs.writeKeyValue "color" obj.wireColor
		)else(
			--Default value
			fs.writeKeyValue "color" (color 255 255 255)
		)
		
		-- Export the real mesh
		local full_mesh_filename = project_path + mesh_name
		format "full_mesh_filename is %\n" full_mesh_filename
		exportMesh obj full_mesh_filename undefined
		
		-- Export material(s)
		local mat = obj.material
		if mat == undefined then (
			throw ("Obj " + obj.name + " does NOT have a material")
			fs.end()
		)
		
		local full_mat_path = mats_path + current_scene_name + "/"
		local me = TMaterialExporter project_path:project_path base_path:base_path
		local exported_materials = me.exportMaterial mat full_mat_path obj
		
		fs.writeComma()
		fs.writeKey "materials"
		fs.arrayOfStrings exported_materials

		local ga = getUserProp obj "glowAmount"
		if ga != undefined then (
			fs.writeComma()
			fs.writeKeyValue "glowAmount" ga
		)
		
		local rf = getUserProp obj "rimFactor"
		if rf != undefined then (
			fs.writeComma()
			fs.writeKeyValue "rimFactor" rf
		)
		
		local sf = getUserProp obj "specularFactor"
		if sf != undefined then (
			fs.writeComma()
			fs.writeKeyValue "specularFactor" sf
		)		
		
		fs.endObj()
	),
	
	-- ----------------------------------------------------------
	fn exportCompShape obj = (
	
		if classof obj == XRefObject then(
			the_obj = obj.GetSourceObject true
		)else(
			the_obj = copy obj
		)
		--format "Clase: %\n" (classof the_obj)
				
		fs.beginObj()
		
		if classof the_obj == Sphere or classof the_obj == GeoSphere then (
			fs.writeKeyValue "shape" "sphere"
			fs.writeComma()
			fs.writeKeyValue "radius" obj.radius
			
		) else if classof the_obj == Box then (
			fs.writeKeyValue "shape" "box"
			fs.writeComma()
			local half_size =  ( [abs obj.width, abs obj.height, abs obj.length] * 0.5)
			-- Warning, order might be incorrect!!!
			fs.writeKeyValue "half_size" half_size
			--Offset
			if obj.parent != undefined then (
				local offset = getUserProp obj.parent "offset"
				if offset == undefined then offset = false
				if offset then (
					fs.writeComma()
					fs.writeKey "offset"
					fs.beginObj()
						fs.writeKeyValue "pos" [0, half_size.y, 0]
					fs.endObj()
				)
			)
			
		) else if classof the_obj == Editable_Mesh or classof the_obj == Editable_Poly then (
			if obj.parent != undefined then(
				local convexity = getUserProp obj.parent "convex"
				if convexity == undefined then convexity = false
				--convexity = isConvex obj
				if convexity then(
					fs.writeKeyValue "shape" "convex"
				)else(
					fs.writeKeyValue "shape" "trimesh"
				)
				fs.writeComma()
			)
			-- Warning, order might be incorrect!!!
			local mesh_name = mesh_path + current_scene_name + "/" + current_scene_name + "_" + obj.parent.name + ".col_mesh"
			local full_mesh_filename = project_path + mesh_name
			exportMesh obj full_mesh_filename "Pos"
			
			fs.writeKeyValue "collision_mesh" mesh_name
		)
		
		if classof obj != XRefObject then(
			delete the_obj
		)
			
		if obj.layer.name == "triggers" then (
			fs.writeComma()
			fs.writeKeyValue "trigger" true
			fs.writeComma()
			fs.writeKeyValue "group" "triggers"
		)
		
		if obj.parent != undefined then(
			local materialType = getUserProp obj.parent "material"
			if materialType != undefined then (
				fs.writeComma()
				fs.writeKeyValue "material" materialType
			)
			local px_group = getUserProp obj.parent "group"
			if px_group != undefined then (
				fs.writeComma()
				fs.writeKeyValue "group" px_group
			)else(
				fs.writeComma()
				fs.writeKeyValue "group" "all"
			)
			
			local px_mask = getUserProp obj.parent "mask"
			if px_mask != undefined then (
				fs.writeComma()
				fs.writeKeyValue "mask" px_mask
			)else(
				fs.writeComma()
				fs.writeKeyValue "mask" "all"
			)
		)
		
		fs.endObj()
	
	),
	
	fn exportCompCollider obj candidates = (
		
		fs.writeComma()
		fs.writeKey "collider" 
		fs.beginObj()
		
		fs.writeKey "shapes"
		fs.beginArray()
			local n = 0
			for child in candidates do (
				if n > 0 then fs.writeComma()
				exportCompShape child
				n = n + 1
			)
		fs.endArray()

		-- Add it in the user properties panel of max:     density = 10
		local density = getUserProp obj "density"
		if density != undefined then (
			fs.writeComma()
			fs.writeKeyValue "density" density
		)else(
			fs.writeComma()
			fs.writeKeyValue "density" 1
		)
		
		local mass = getUserProp obj "mass"
		if mass != undefined then (
			fs.writeComma()
			fs.writeKeyValue "mass" mass
		)else(
		--Default value
			fs.writeComma()
			fs.writeKeyValue "mass" 1
		)
			
		local linearDrag = getUserProp obj "linearDrag"
		if linearDrag != undefined then (
			fs.writeComma()
			fs.writeKeyValue "linearDrag" linearDrag
		)else(
		--Default value
			fs.writeComma()
			fs.writeKeyValue "linearDrag" 0.5
		)
		
		local angularDrag = getUserProp obj "angularDrag"
		if angularDrag != undefined then (
			fs.writeComma()
			fs.writeKeyValue "angularDrag" angularDrag
		)else(
		--Default value
			fs.writeComma()
			fs.writeKeyValue "angularDrag" 0.25
		)
		
		local is_dynamic = getUserProp obj "dynamic"
		if is_dynamic != true then (
			fs.writeComma()
			fs.writeKeyValue "dynamic" false
		)else(
			fs.writeComma()
			fs.writeKeyValue "dynamic" true
		)
		
		local is_kinematic = getUserProp obj "kinematic"
		if is_kinematic != true then (
			fs.writeComma()
			fs.writeKeyValue "kinematic" false
		)else(
			fs.writeComma()
			fs.writeKeyValue "kinematic" true
		)

		--In this case, we put the property by default
		--local contCollision = getUserProp obj "continuous_collision"
		--if contCollision != undefined then (
			--fs.writeComma()
			--fs.writeKeyValue "continuous_collision" true
		--)
		fs.endObj()
	),
	
	
	fn exportChildrenColliders obj = (
		
		local candidates = #()
		for child in obj.children do (
			if isCollider child or isTrigger child then append candidates child
		)
		
		if isTrigger obj then candidates = #(obj)
		
		if candidates.count == 0 then return undefined
		
		format "Candidates are %\n" candidates
		--return true
		
		exportCompCollider obj candidates
	),
	
	----------------------------------------------------------
	fn exportEntity obj = (
		fs.beginObj()
		fs.writeKey "entity"
			fs.beginObj()
			exportCompName obj
			exportTransform obj
		
			format "Exporting custom attributes\n"
			local nCA = custAttributes.count obj
			for idx = 1 to nCA do (
				local ca_data = custAttributes.get obj idx
				ca_data.exportAsComponent fs
			)
			
			format "Exporting colliders\n"
			exportChildrenColliders obj
			
			--Absolute AABB
			fs.writeComma()
			fs.writeKey "abs_aabb" 
			fs.beginObj()
			fs.endObj()
			
			--Local AABB
			local is_dynamic = getUserProp obj "dynamic"
			if is_dynamic != undefined then (
				fs.writeComma()
				fs.writeKey "local_aabb" 
				fs.beginObj()
					local bb = (obj.max - obj.min)
					local half_size =  ( [abs bb[1], abs bb[2], abs bb[3]] * 0.5)
					-- Warning, order might be incorrect!!!
					fs.writeKeyValue "half_size" half_size
				fs.endObj()
			)
		
			local prefab= custAttributes.get obj classPrefabData
			if prefab == undefined then (
				if not isTrigger obj then (
				--if classof obj == Camera then exportCamera obj
					if canConvertTo obj Editable_mesh then exportEditableMesh obj
				--else (
				--	format "Warning. Don't kwow how to export obj % of class %\n" obj.name ((classof obj) as string)
				)
			)
			fs.endObj()
		fs.endObj()
	),
	
	fn exportPrefabMesh obj pre prefab_id = (
		pre.writeKey "render" 
		pre.beginObj()
		local mesh_name = mesh_path + current_scene_name + "/" + current_scene_name + "_" + obj.name + ".mesh"
		pre.writeKeyValue "instanciable" true
		pre.writeComma()
		pre.writeKeyValue "key" prefab_id
		pre.writeComma()
		--local texture_name = mats_path + obj.name
		pre.writeKeyValue "mesh" mesh_name
		pre.writeComma()
		pre.writeKeyValue "color" (color 255 255 255)
		
		-- Export the real mesh
		local full_mesh_filename = project_path + mesh_name
		format "full_mesh_filename is %\n" full_mesh_filename
		exportMesh obj full_mesh_filename undefined
		
		-- Export material(s)
		local mat = obj.material
		if mat == undefined then (
			throw ("Obj " + obj.name + " does NOT have a material")
			fs.end()
		)
		
		local full_mat_path = mats_path + current_scene_name + "/"
		local me = TMaterialExporter project_path:project_path base_path:base_path
		local exported_materials = me.exportMaterial mat full_mat_path obj
		
		pre.writeComma()
		pre.writeKey "materials"
		pre.arrayOfStrings exported_materials
		
		pre.endObj()
	),
	
	fn exportXRefPrefab obj prefab prefab_name prefab_id = (
		pre = TJsonFormatter()
		
		pre.begin prefab
		pre.beginArray()
			pre.beginObj()
				pre.writeKey "entity"
					pre.beginObj()
						pre.writeKeyValue "name" prefab_name
						pre.writeComma()
						pre.writeKey "transform"
						pre.beginObj()
						pre.endObj()
						pre.writeComma()
						exportPrefabMesh obj pre prefab_id
					pre.endObj()
			pre.endObj()
		pre.endArray()
		pre.end()
		
	),
	
	fn exportEntityWithPrefab obj prefab = (
		fs.beginObj()
		fs.writeKey "entity"
			fs.beginObj()
			exportCompName obj
			exportTransform obj
			local nCA = custAttributes.count obj
			for idx = 1 to nCA do (
				local ca_data = custAttributes.get obj idx
				ca_data.exportAsComponent fs
			)
			fs.writeComma()
			fs.writeKeyValue "prefab" prefab
			exportChildrenColliders obj
			fs.endObj()
		fs.endObj()
	),
		
	fn exportAll = (
		-- Decide output filename based on .max filename
		current_scene_name = getFilenameFile maxFileName
		local sceneNewDir = project_path + scenes_path + current_scene_name +"/" 
		--Delete all files before doing anything
		local scene_files = getFiles (sceneNewDir+"*")
		if scene_files.count > 0 then(
			for f in scene_files do deleteFile f
		)else(
			makeDir sceneNewDir
		)
				
		local full_path = sceneNewDir + current_scene_name + ".json"
		format "Exporting to % %\n" full_path  current_scene_name
		
		--Create scene folder for meshes, materials and textures
		local meshNewDir = project_path + mesh_path + current_scene_name
		makeDir meshNewDir
		
		local materialNewDir = project_path + mats_path + current_scene_name
		makeDir materialNewDir
		
		local textureNewDir = project_path + base_path + "textures/" + current_scene_name
		makeDir textureNewDir
		
		local curveNewDir = project_path + curve_path + current_scene_name
		makeDir curveNewDir
		
		/*local instanced_meshes = #()
		local instanced_objects = #()*/
		
		local prefab_meshes = #()
		local prefab_materials = #()
		local prefab_names = #()
		
		fs.begin full_path
		fs.beginArray()
		
		local nitems = 0
		for obj in $* do (
			if isCollider obj then continue
			if (classof obj) == XRefObject and obj.parent != undefined then continue
			if (classof obj) == Line then (
				local full_curve_path = project_path + curve_path + current_scene_name + "/" + obj.name + ".curve"
				exportCurve obj full_curve_path
				continue
			)
			if nitems > 0 then fs.writeComma()
			/*if (classof obj) == XRefObject then(
				if (appendIfUnique instanced_meshes obj.currentObjectName) then(
					--exportInstancedEntity obj xref_name
					--nitems = nitems + 1
					append instanced_objects #(obj)
				)else(
					
				)
			)else(*/
			local not_instantiable = (getUserProp obj "not_instantiable")
			if not_instantiable == undefined then (
				not_instantiable = false
			)
			
			if (classof obj) == XRefObject and obj.parent == undefined and not_instantiable != true then(
				local mesh_name = obj.currentObjectName
				local mat = obj.material
				if mat == undefined then(
					throw ("Obj " + obj.name + " does NOT have a material")
					fs.end()
					return false
				)
				if classof mat == XRef_Material then(
					mat = mat.GetSrcItem()
				)
				
				local mat_name = mat.name
				local prefab_name = mesh_name+ "_"+mat_name
								
				local prefab_id = findItem prefab_names prefab_name
				--local mat_id = findItem prefab_materials material_name
				local prefab = sceneNewDir + prefab_name + ".json"
				if prefab_id == 0 then(
					--Prefab doesn't exist for this mesh/material pair, we have to create it
					append prefab_meshes mesh_name
					append prefab_materials mat_name
					append prefab_names prefab_name
					
					prefab_id = prefab_names.count
					
					exportXRefPrefab obj prefab prefab_name prefab_id
				)
				--Prefab already exists and it is with the same mesh/material pair
				--local prefab = sceneNewDir + prefab_meshes[prefab_id] + "_"+ prefab_materials[prefab_id] + ".json"
				exportEntityWithPrefab obj (scenes_path + current_scene_name +"/"+prefab_name + ".json")
				
				nitems = nitems + 1				
			)else(
				if (classof obj) != XRefObject or not_instantiable == true then (
					exportEntity obj
					nitems = nitems + 1
				)
			)
		)
		
		fs.endArray()
		fs.end()
		
	)
	
	
)
	
--exporter = TSceneExporter()
--exporter.exportAll()
