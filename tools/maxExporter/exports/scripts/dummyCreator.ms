rollout dummy_creator "Dummy Creator/Resizer" 
(
	group "Creator" (
		edittext name "Name:" labelontop:true
		label pos "Position:" align:#left
		spinner x "X:" align:#center width:60 height:15 range:[-1000000,1000000,0]
		spinner y "Y:" align:#center width:60 height:15 range:[-1000000,1000000,0]
		spinner z "Z:" align:#center width:60 height:15 range:[-1000000,1000000,0]
		label prefab "Prefab:" align:#left
		radiobuttons prefab_type align:#left labels:#("None","Prefab", "Puzzle Comp.","Ent. Destroy") columns:2
		dropDownList prefab_list "Prefab List" width:150 height:20 pos:[15,200] align:#center visible:false
		dropDownList puzzle_prefab_list "Component List" width:151 height:40 pos:[15,200]
			items:#("Default Opening Door","Custom Opening Door","Default Closing Door","Custom Closing Door",
				"Cross Cube","Circle Cube","Triangle Cube","Custom Key","Cross Trigger","Circle Trigger",
				"Triangle Trigger","Custom Activation Trigger","Default Closing Trigger","Custom Closing Trigger") 
				align:#left visible:false
		button create "Create Dummy" align:#center width:100 height:20
	)
	group "Options" (
		spinner l "Length:" align:#center width:80 height:15 range:[-1000000,1000000,10]
		spinner w "Width:" align:#center width:80 height:15 range:[-1000000,1000000,10]
		spinner h "Height:" align:#center width:80 height:15 range:[-1000000,1000000,10]
		button selection_size "Get Selection Size"
		button reset_size "Reset Sizes"
	)
	
	on dummy_creator open do
		(
			pref_array = #()
			pref_str = ""
			fs = openFile "C:/exports/scripts/prefab_names.txt"
			while not eof fs do(
			   pref_str = readline fs
			   append pref_array pref_str
			)
			close fs
			prefab_list.items = pref_array
		)

	on create pressed do
	(
		if name.text == "" then (
			MessageBox "Warning: Dummy MUST have a name!"
			return undefined
		)else(
			obj = execute ("$" + name.text)
			if obj != undefined then (
				MessageBox "Warning: Duplicate name!"
			return undefined
			)
			Dummy pos:[x.value,y.value,z.value] isSelected:on boxsize:[l.value, w.value, h.value]
			$.name = name.text
		)			
		if prefab_type.state == 2 then(
			custAttributes.add $ classPrefabData
			local hasMesh = showPrefabMesh $ prefab_list.selection
			$.prefab = prefab_list.selection
			if hasMesh then $.boxsize = $.children[1].max - $.children[1].min
		)
		if prefab_type.state == 3 then(
			custAttributes.add $ classPuzzlePrefab
			local hasMesh = showPuzzlePrefabMesh $ puzzle_prefab_list.selection
			$.puzzle_prefab = puzzle_prefab_list.selection
			if hasMesh then $.boxsize = $.children[1].max - $.children[1].min
		)
		if prefab_type.state == 4 then(
			custAttributes.add $ classEntitiesDestroy
		)
		x.value = 0
		y.value = 0
		z.value = 0
	)
	
	on prefab_type changed p do (
		if p == 1 or p == 4 then(
			prefab_list.visible = false
			puzzle_prefab_list.visible = false
		)
		if p == 2 then(
			prefab_list.visible = true
			puzzle_prefab_list.visible = false
		)
		if p == 3 then(
			prefab_list.visible = false
			puzzle_prefab_list.visible = true
		)
	)
	
	function changeDummySize = (
		if $ == undefined then return undefined
		if classof $ != Dummy then (
			MessageBox "Selected object is not a Dummy, dummy!"
			return undefined
		)
		$.boxsize=[l.value, w.value, h.value]
	)
	
	on selection_size pressed do (
		if $ == undefined then (
			MessageBox "You MUST select an object!"
			return undefined
		)
		if classof $ != Dummy then (
			MessageBox "Selected object is not a Dummy, dummy!"
			return undefined
		)
		local size = $.boxsize
		l.value = size.x
		w.value = size.y
		h.value = size.z
	)
	
	on reset_size pressed do (
		l.value = 10
		w.value = 10
		h.value = 10
	)

	on l changed state do
	(
		changeDummySize()
	)

	on w changed state do
	(
		changeDummySize()
	)

	on h changed state do
	(
		changeDummySize()
	)
)