clearListener()

struct  TAnimationsExporter (
	project_path = "c:/exports/data/",
	anim_name = "unknown",			-- smurf
	anim_root_path,					-- "C:/../bin/data/skeletons/smurf/,
	anim_file, 						-- "C:/../bin/data/skeletons/smurf/smurf.csf"
	
	fn setAnimName new_name = (
		anim_name = new_name
		anim_root_path = project_path + "animations/" + anim_name + "/"
		makedir anim_root_path 
		anim_file = anim_root_path + anim_name + ".rigid_anim"
	),

	fn exportAnim obj = (
		local pos_controller = obj.position.controller
		local keys = pos_controller.keys
		local num_keys = numKeys pos_controller
		local total_frames = keys[num_keys]
		format "Frames: %\n" total_frames
		
	)
	
)

gc()
ae = TAnimationsExporter()
ae.exportAnim $