global properties_rollout
global dummy_rollout
global collider_rollout
global snap_rollout
global trigger_rollout
global door_rollout
global light_rollout
global cannon_rollout
global spawn_rollout
global particle_rollout
filein "objectProperties.ms"
filein "dummyCreator.ms"
filein "colliderCreator.ms"
filein "customSnapTool.ms"
filein "triggerCreator.ms"
filein "lightCreator.ms"
filein "doorCreator.ms"
filein "cannonCreator.ms"
filein "spawnCreator.ms"
filein "particleCreator.ms"
filein "LBTools_Helper_Manager.ms"

utility MyMCVExporter "Arcane Impulse Utility"
(
	label version_label "Version 2.8"
	button help "Ask for Help!"
	
	fn randomizeFloor obj def:0 = (
		format "obj_name: %\n" obj.name
		local n
		local rot = [0,90,180,270]
		if def == 0 then (
			n = random 1 4
			format "parent rotation:%\n" rot[n]
		)else(
			n = def
			format "child rotation:%\n" rot[n]
		)
		format "transform: %\n" obj.transform
		obj.transform = (rotateZmatrix rot[n]) * obj.transform
		/*if obj.children.count >= 1 then (
			for i = 1 to obj.children.count do(
				randomizeFloor obj.children[i] def:n
			)
		)*/
	)
		
	rollout exporter "Arcane Impulse Utility"
	(
		group "Exporters" (
			button btn_scene "Export Scene" width:100
			button btn_mesh "Export Mesh" width:100
			button btn_skeleton "Export Skeleton & Meshes" width:140
			button btn_skel_anims "Export Animation" width:100 tooltip:"Uses max animation range and frame rate to specify the range to export"
		)
		group "Prefabs / Tools" (
			button btn_dummy "Dummy Creator/Resizer" width:140 tooltip:"Allows creation of dummys or resizing of existing ones"
			--button btn_prefab "Add Prefab" width:100 tooltip:"Adds prefab to selected objects"
			button btn_collider "Collider Creator" width:100 tooltip:"Allows adding a collider to an object"
			button btn_trigger "Trigger Creator" width:100
			button btn_particle "Particle Creator" width:100
			button btn_snap "Snap Tool" width:100
			button btn_floor "Floor Randomizer" width:100
		)
		group "Pivot / XForm"(
			button btn_center_pivot "Center Pivot" width:100
			button btn_reset_pivot "Reset Pivot" width:100
			button btn_attach "Attach All" width:100 tooltip:"Attach ALL objects in the scene together"
			button btn_reset_xform "Reset XForm" width:100
		)
		group "Components/Other"(
			button btn_spawn_point "Player Spawn Point" width:140
			button btn_waypoints "Waypoints" width:100
			button btn_lbtools "LBTools Helpers" width:100
			button btn_xrefselect "XRef Selector" width:100
			button btn_particle_mat "Make Particle Material" width:140
			button btn_obj_props "Object Properties" width:100
		)
		
		on btn_scene pressed do
		(
			try (
				gc()
				local exporter = TSceneExporter()
				exporter.exportAll()	
				MessageBox "All exported OK" beep:false
			) catch (
				MessageBox ("Error Exporting:\n" + getCurrentException())
				gc()
			)
		)
		
		-- Exports the current selection mesh to file
		on btn_mesh pressed do
		(
			
			try (
				gc()
				local ofilename = undefined
				exportMesh $ ofilename undefined
				MessageBox "Single mesh exported OK" beep:false
			) catch (
				MessageBox ("Error Exporting Single Mesh:\n" + getCurrentException())
				gc()
			)
		)
		
		-- Exports the current selection mesh to file
		on btn_skeleton pressed do
		(
			
			try (
				gc()
				local se = TSkeletonsExporter()
				se.exportSkelAndMeshes()
				MessageBox "Skeleton And Meshes exported OK" beep:false
			) catch (
				MessageBox ("Error Exporting Skeleton:\n" + getCurrentException())
				gc()
			)
		)
		
		-- Exports the current selection mesh to file
		on btn_skel_anims pressed do
		(
			
			try (
				gc()
				local se = TSkeletonsExporter()
				se.exportAnim()
				MessageBox "Skeleton Animation exported OK" beep:false
			) catch (
				MessageBox ("Error Exporting Skeleton Animation:\n" + getCurrentException())
				gc()
			)
		)
		
		on btn_dummy pressed do
		(
			dummy_rollout = newRolloutFloater "Dummy Creator/Resizer" 200 440
			addRollout dummy_creator dummy_rollout
		)
		
		-- Exports the current selection mesh to file
		/*on btn_prefab pressed do
		(
			if $ == undefined then(
				MessageBox "Warning: You MUST select an object!"
				return undefined
			)
			custAttributes.add $ classPrefabData
			MessageBox "Prefab selector added to object/s" beep:false
		)*/
		
		on btn_collider pressed do
		(
			collider_rollout = newRolloutFloater "Collider Creator" 200 400
			addRollout collider_creator collider_rollout
		)
		
		on btn_trigger pressed do
		(
			trigger_rollout = newRolloutFloater "Trigger Creator" 200 440
			addRollout trigger_creator trigger_rollout
		)
		
		on btn_particle pressed do
		(
			particle_rollout = newRolloutFloater "Particle Creator" 200 400
			addRollout particle_creator particle_rollout
		)
		
		on btn_attach pressed do
		(
			if queryBox "Are you sure you want to attach all objects?" then (
				objArr = $*
				j = 1
				count = objArr.count
				
				while objArr.count > 1 do
				(				
					if classof objArr[j] != Editable_Poly then converttopoly objArr[j]
						
					polyop.attach objArr[j] objArr[j+1]
					--deleteItem objArr (j+1)
						
					j += 1
						
					if (j + 1) > objArr.count then j = 1
					
				)
				messageBox "All objects attached!" beep:false
			)
		)
		
		on btn_snap pressed do
		(
			snap_rollout = newRolloutFloater "Snap Tool" 200 190
			addRollout snap_tool snap_rollout
		)
		
		on btn_floor pressed do
		(
			if $selection == undefined then return undefined
			for obj in $selection do (
				randomizeFloor obj
			)
		)
		
		on btn_lbtools pressed do
		(
			g_LBTools_HM.f_helperManager()
		)
		
		on btn_center_pivot pressed do
		(
			if $ == undefined then(
				MessageBox "Warning: You MUST select an object!"
				return undefined
			)
			CenterPivot $
		)
		
		on btn_reset_pivot pressed do
		(
			if $ == undefined then(
				MessageBox "Warning: You MUST select an object!"
				return undefined
			)
			ResetPivot $
		)
		
		on btn_reset_xform pressed do
		(
			if $ == undefined then(
				MessageBox "Warning: You MUST select an object!"
				return undefined
			)
			ResetXForm $
		)
		
		on btn_spawn_point pressed do
		(
			spawn_rollout = newRolloutFloater "Player Spawn Point" 200 135
			addRollout spawn_creator spawn_rollout
		)
		
		on btn_waypoints pressed do
		(
			if $ == undefined then(
				MessageBox "Warning: You MUST select an object!"
				return undefined
			)
			custAttributes.add $ classWaypoint
			MessageBox "Waypoint Component added to object" beep:false
		)
				
		on btn_xrefselect pressed do
		(
			if $ == undefined then(
				MessageBox "Warning: You MUST select an object!"
				return undefined
			)
			if classof $ != XRefObject then (
				MessageBox "Warning: Object is NOT an XRef!"
				return undefined
			)
			custAttributes.add $ classXRefSelect
			MessageBox "XRef Selector added to object" beep:false
		)
		
		on btn_particle_mat pressed do
		(
			if $ == undefined then(
				MessageBox "Warning: You MUST select an object!"
				return undefined
			)
			custAttributes.add $.material classParticleMaterial
			MessageBox "Object material is now particle material!" beep:false
		)
		
		on btn_obj_props pressed do
		(
			properties_rollout = newRolloutFloater "Object Properties" 200 460
			addRollout object_properties properties_rollout
		)
		
	) -- end rollout creator
	
	rollout puzzles "Puzzle/Scenery Configurator"
	(
		group "Lighting" (
			button btn_light_add "Add Light to Object" width:140
			button btn_light_create "Light Creator" width:100
		)
		
		group "Puzzles" (
			/*button btn_puzzle_prefab "Add Puzzle Component" width:140
			button btn_a2b_puzzle_controller "Add A2B Puzzle Controller" width:140
			button btn_baby_puzzle_controller "Add Baby Puzzle Controller" width:140*/
			
			button btn_puzzle_trigger "Make Puzzle Trigger" width:140
		)
		
		group "Scenery" (
			button btn_moving_platform "Moving Platform" width:100
			button btn_timed_platform "Timed Platform" width:100
			button btn_cannon "Cannon Creator" width:100
		)
		
		group "Doors" (
			button btn_door_create "Door Creator" width:100
		)
		
		on btn_light_add pressed do
		(
			if $ == undefined then return undefined
			custAttributes.add $ classLight
			MessageBox "Light component added to object/s" beep:false
		)
		
		on btn_light_create pressed do
		(
			light_rollout = newRolloutFloater "Light Creator" 200 565
			addRollout light_creator light_rollout
		)
		
		on btn_door_create pressed do
		(
			door_rollout = newRolloutFloater "Door Creator" 200 320
			addRollout door_creator door_rollout
		)		
				
		/*on btn_puzzle_prefab pressed do
		(
			if $ == undefined then return undefined
			custAttributes.add $ classPuzzlePrefab
			MessageBox "Puzzle component selector added to object/s" beep:false
		)
		on btn_a2b_puzzle_controller pressed do
		(
			if $ == undefined then return undefined
			custAttributes.add $ classA2BPuzzleController
			MessageBox "Object is now Simple Puzzle Controller. PLEASE enter all data" beep:false
		)
		on btn_baby_puzzle_controller pressed do
		(
			if $ == undefined then return undefined
			custAttributes.add $ classBabyPuzzleController
			MessageBox "Object is now Baby Puzzle Controller. PLEASE enter all data" beep:false
		)*/
		
		on btn_puzzle_trigger pressed do
		(
			if $ == undefined then return undefined
			custAttributes.add $ classPuzzleTrigger
			MessageBox "Object is now Puzzle Trigger. PLEASE enter all data" beep:false
		)
		
		on btn_moving_platform pressed do
		(
			if $ == undefined then return undefined
			custAttributes.add $ classMovingPlatform
			MessageBox "Object is now Moving Platform. PLEASE enter all data" beep:false
		)
		
		on btn_timed_platform pressed do
		(
			if $ == undefined then return undefined
			custAttributes.add $ classTimedPlatform
			MessageBox "Object is now Timed Platform. PLEASE enter all data" beep:false
		)
		
		on btn_cannon pressed do
		(
			cannon_rollout = newRolloutFloater "Cannon Creator" 200 500
			addRollout cannon_creator cannon_rollout
		)
		
	) -- end rollout creator
		
	-- ...
	on MyMCVExporter open do
	(
		MessageBox "Please save before using any of these tools!" beep:false
		try(closeRolloutFloater dummy_rollout)catch()
		try(closeRolloutFloater collider_rollout)catch()
		try(closeRolloutFloater snap_rollout)catch()
		try(closeRolloutFloater trigger_rollout)catch()
		try(closeRolloutFloater door_rollout)catch()
		try(closeRolloutFloater light_rollout)catch()
		try(closeRolloutFloater properties_rollout)catch()
		try(closeRolloutFloater cannon_rollout)catch()
		try(closeRolloutFloater spawn_rollout)catch()
		addRollout exporter
		addRollout puzzles
	) 
	on MyMCVExporter close do
	(
		try(closeRolloutFloater dummy_rollout)catch()
		try(closeRolloutFloater collider_rollout)catch()
		try(closeRolloutFloater snap_rollout)catch()
		try(closeRolloutFloater trigger_rollout)catch()
		try(closeRolloutFloater door_rollout)catch()
		try(closeRolloutFloater light_rollout)catch()
		try(closeRolloutFloater properties_rollout)catch()
		try(closeRolloutFloater cannon_rollout)catch()
		try(closeRolloutFloater spawn_rollout)catch()
		removeRollout exporter
		removeRollout puzzles
	) 
	
	on help pressed do(
		t = getLocalTime()
		seed t[8]
		n = random 1 6
		case n of
		(
			1: ShellLaunch "https://www.youtube.com/watch?v=cUN9sX86ZwA" ""
			2: ShellLaunch "https://www.youtube.com/watch?v=L38Z0YNJfCI" ""
			3: ShellLaunch "https://www.youtube.com/watch?v=2Q_ZzBGPdqE" ""
			4: ShellLaunch "https://www.youtube.com/watch?v=GKfAmJK2yO8" ""
			5: ShellLaunch "https://youtu.be/4WM5kyEVneg?t=41" ""
			6: ShellLaunch "https://www.youtube.com/watch?v=z3exEEH86ek" ""
		)
	)
) -- end utility MyUtil 