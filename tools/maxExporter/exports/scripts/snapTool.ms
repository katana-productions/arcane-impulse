try destroyDialog positionSnap catch()
rollout positionSnap "Position Snap" width:125
(
	group "object"(
	spinner spnSnapMult "Unit: " range:[0,1e6,100] fieldWidth:65 type:#worldUnits align:#left
	checkButton chbSnap "SNAP Object" width:100 height:25 align:#left
	)
	group "vertex"(
	spinner spnSnapMultVert "Unit: " range:[0,1e6,100] fieldWidth:65 type:#worldUnits align:#left
	checkButton chbSnapVert "SNAP Vertex" width:100 height:25 align:#left
)
	
--  Object Snap
	fn roundNearest nr mult =
	(
		if nr < 0 do mult *= -1
		nr + mult/2 - 1 - mod (nr + mult/2 - 1) mult
	)
	mapped fn correctPos obj =
		obj.pos = [roundNearest obj.pos.x spnSnapMult.value,
				   roundNearest obj.pos.y spnSnapMult.value,
				   roundNearest obj.pos.z spnSnapMult.value]
	fn redefineTransformHandlers sel =
	(
		deleteAllChangeHandlers id:#autoSnapTransform
		when transform sel changes id:#autoSnapTransform obj do correctPos obj
	)
	fn initCallbacks =
	(
	
		callbacks.removeScripts id:#autoSnapTransform
		callbacks.addScript #selectionSetChanged "positionSnap.redefineTransformHandlers selection" id:#autoSnapTransform
		
		
		redefineTransformHandlers selection
	
	)
	fn removeHandlersAndCallbacks =
	(
		
		 callbacks.removeScripts id:#autoSnapTransform
		 deleteAllChangeHandlers id:#autoSnapTransform
	
	)
	
	
-- Vertex Snap	
	
	local getPolyVert = polyOp.getVert
	local setPolyVert = polyOp.setVert
	local getPolyVertSel = polyOp.getVertSelection
	local getPolyEdgeSel = polyOp.getEdgeSelection
	local getPolyFaceSel = polyOp.getFaceSelection
	local getVertsByEdge = polyOp.getVertsUsingEdge
	local getVertsByFace = polyOp.getVertsUsingFace
	
	
	fn roundVert vert val =
	(
		vert.x = roundNearest vert.x val
		vert.y = roundNearest vert.y val
		vert.z = roundNearest vert.z val
		vert
	)
	fn setRoundedVerts obj =
	(
		local snapVal = spnSnapMultVert.value
		local verts = case subObjectLevel of
		(
			1 : getPolyVertSel obj
			2 : getVertsByEdge obj (getPolyEdgeSel obj)
			3 : getVertsByEdge obj (getPolyEdgeSel obj)
			4 : getVertsByFace obj (getPolyFaceSel obj)
			5 : getVertsByFace obj (getPolyFaceSel obj)
			default : #{}
		)
		setPolyVert obj verts (for v in verts collect roundVert (getPolyVert obj v) snapVal)
		setNeedsRedraw()
	)
	fn redefineGeometryHandlers sel =
	(
		deleteAllChangeHandlers id:#autoSnapGeometry
		when geometry sel changes id:#autoSnapGeometry handleAt:#redrawViews obj do setRoundedVerts obj
	)
	
		fn removeHandlers =
		deleteAllChangeHandlers id:#autoSnapGeometry
	
	-- interface
	on positionSnap open do
	(	removeHandlersAndCallbacks()
	removeHandlers()
	)
	on chbSnap changed state do
		if state then initCallbacks()
		else removeHandlersAndCallbacks()
	
	on chbSnapVert changed state do
	if state then redefineGeometryHandlers (selection[1])
		else removeHandlers()
	
	on positionSnap close do
			(	removeHandlersAndCallbacks()
	removeHandlers()
	)
)
createDialog positionSnap