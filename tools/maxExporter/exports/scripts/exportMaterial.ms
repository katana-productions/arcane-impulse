
-- ----------------------------------------------------------
-- ----------------------------------------------------------
-- ----------------------------------------------------------
struct TMaterialExporter (
	
	fs = TJsonFormatter(), 
	project_path,                 -- Asigned in the ctor 
	base_path,                 -- Asigned in the ctor 	
	
	fn exportMap map alias default_value = (
		current_scene_name = getFilenameFile maxFileName
		
		if map == undefined then (
			local json_filename = base_path + "textures/" + default_value + ".dds"
			fs.writeKeyValue alias json_filename
			return false
		)
		
		-- "C:\mcv\code\bin\data\textures\bricks.DDS"

		-- "C:\users\pep\desktop\download\bricks.dds"
		local map_filename = map.filename
		
		-- bricks
		local base_name = getFilenameFile map_filename
		
		-- data\textures\bricks.dds"
		local json_filename = base_path + "textures/" + current_scene_name + "/" + base_name + ".dds"
		
		-- 
		local ofull_path = project_path + json_filename
		
		-- Check if ofull_path exists
		if not doesFileExist ofull_path then (
			format "We should copy from % to %\n" map_filename ofull_path
			copyFile map_filename ofull_path
		)
		
		fs.writeKeyValue alias json_filename
	),
	
	fn isValidName aname = (
		-- Add no �, accents, etc.
		return findString aname " " == undefined 
	),
	
	-- Exports a single std material to a json format
	fn exportStdMaterial mat mat_name obj is_xref = (
		
		format "Exporting material % % %\n" mat_name mat (classof mat as string)
		
		if not (isValidName mat.name) then (
			fs.end()
			throw ("Obj " + obj.name + " has a material with an invalid name " + mat.name )
		)	
		
		fs.begin (project_path + mat_name )
		fs.beginObj()
			--This value will later be changed by the default one
			local not_instantiable = (getUserProp obj "not_instantiable")
			if not_instantiable == undefined then (
				not_instantiable = false
			)
			if is_xref and not_instantiable != true then (
				fs.writeKeyValue "technique" "objs_culled_by_gpu.tech"
				fs.writeComma()
			)
			local particle = custAttributes.get mat classParticleMaterial
			if particle != undefined then (
				fs.writeKeyValue "technique" "particles_flow_num_instances.tech"
				fs.writeComma()
				fs.writeKeyValue "cast_shadows" false
				fs.writeComma()
				fs.writeKeyValue "category" "transparent"
			)else(
				fs.writeKeyValue "cast_shadows" true
			)
			fs.writeComma()
			fs.writeKey "textures" 
			fs.beginObj()
				
			if classof mat == Standardmaterial then (
				exportMap mat.diffusemap "albedo" "white"
			    if(mat.bumpMap != undefined) then (
					fs.writeComma()
					if isKindOf (mat.bumpMap) Normal_Bump then(
						if mat.bumpMap.normal_map != undefined then(
							exportMap mat.bumpMap.normal_map "normal" "null_normal"
						)else if mat.bumpMap.bump_map != undefined then(
							exportMap mat.bumpMap.bump_map "normal" "null_normal"
						)
					)else(
						exportMap undefined "normal" "null_normal"
					)
				)
				-- ... other maps..
			)
		
			fs.endObj()
		fs.endObj()
		fs.end()
	),
	
	-- Will return an array of all the materials names used by obj and exported by us
	fn exportMaterial mat base_name obj = (
		
		local exported_materials = #()
		local is_xref = false
		--format "Material: %\n" (classof mat)
		
		if classof mat == XRef_Material then(
			mat = mat.GetSrcItem()
		)
		if classof obj == XRefObject then is_xref = true
		
		if classof mat == StandardMaterial then (
			local mat_name = base_name + mat.name + ".material"
			local obj_copy = obj
			if classof obj != Editable_mesh or classof obj != Editable_poly then (
				obj_copy = copy obj
				convertToMesh obj_copy
			)
			local materials_of_mesh = getMaterialsUsedByMesh obj_copy
			for idx = 1 to materials_of_mesh.count do (
				if materials_of_mesh[idx] == undefined then continue
				append exported_materials mat_name
			)
			exportStdMaterial mat mat_name obj_copy is_xref
			if obj_copy != obj then delete obj_copy
			
		) else if classof mat == MultiMaterial then (
			local multi_mat = mat
			
			local materials_of_mesh = getMaterialsUsedByMesh obj
			for mat_idx = 1 to materials_of_mesh.count do (
				if materials_of_mesh[ mat_idx ] == undefined then continue
				local mat_of_mesh = multi_mat[ mat_idx ]
				
				if mat_of_mesh == undefined then throw ("Mesh " + obj.name + " is using a multimaterial in slot " + (mat_idx as string)+ " but the multimat does not have this submat")
				if classof mat_of_mesh != StandardMaterial then throw ("Mesh " + obj.name + " is using a multimaterial in slot " + (mat_idx as string) + " but the multimat in this slot is not a stdMaterial")
				
				local mat_name = base_name + mat_of_mesh.name + ".material"
				append exported_materials mat_name
				exportStdMaterial mat_of_mesh mat_name obj is_xref
			)
		)
		
		return exported_materials
	)

	
)

--gc()
--me = TMaterialExporter project_path:"c:/code/engine/bin/" base_path:"data/"
--me.exportMaterial $.mat "data/materials/" $
