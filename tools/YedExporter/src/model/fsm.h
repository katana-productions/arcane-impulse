class CFSM
{
public:
  void loadXML(const std::string& filename);
  void saveJSON(const std::string& filename);

  // data model
  struct TState
  {
    std::string id;
    std::string name;
    std::string params;
  };
  struct TTransition
  {
    std::string source;
    std::string target;
    std::string params;
  };
  struct TVariable
  {
    std::string name;
    std::string params;
  };

  std::vector<TState> states;
  std::vector<TTransition> transitions;
  std::vector<TVariable> variables;

  TState* getStateById(const std::string& id);
};
